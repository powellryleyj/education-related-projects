# enumeration of database attributes
class DatabaseAttributes:
    ID = 'id'

    class BeerAttributes:
        BEER_NAME = 'beerName'
        ABV = 'abv'
        TYPE_NAME = 'typeName'
        BEER_LOGO = 'beerLogo'

    class BreweryAttributes:
        BREWERY_NAME = 'breweryName'
        BREWERY_LOGO = 'breweryLogo'

    class TypesAttributes:
        TYPE = 'typeName'

    class NotesAttributes:
        NOTE = 'note'

    class OnTapAttributes:
        SENSOR_ID = 'sensorId'
        VOLUME_RECORD = 'volumeRecord'
        INITIAL_VOLUME = 'initialVolume'
