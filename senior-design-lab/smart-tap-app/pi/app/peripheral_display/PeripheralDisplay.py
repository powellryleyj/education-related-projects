# -*- coding: utf-8 -*-

import platform as plat
from distutils import dist

from PyQt5 import QtGui
from PyQt5.QtCore import Q_ENUMS, Qt
from PyQt5.QtGui import QPixmap, QImage
from PyQt5.QtWidgets import QTableWidget, QTableWidgetItem, QHeaderView, QProgressBar

from app.controllers.PeripheralDisplayController import PeripheralDisplayController
from app.peripheral_display.DatabaseAttributes import DatabaseAttributes
from app.peripheral_display.DisplayRefresh import DisplayRefresh
from app.resources.ConfigEnumerations import ConfigEnumerations
from storedProcedures.Helpers import DEFAULT_BREWERY_LOGO, DEFAULT_BEER_LOGO


class PeripheralDisplay(QTableWidget):
    """Class that controls logic to display to external monitor and update itself"""

    def __init__(self, ctrl: PeripheralDisplayController, settings: {}):
        super().__init__()
        Q_ENUMS(ConfigEnumerations)
        Q_ENUMS(DatabaseAttributes)
        Q_ENUMS(TableHeaders)

        self.controller = ctrl
        self.file_paths = settings[ConfigEnumerations.FILE_PATHS]
        self.display_settings = settings[ConfigEnumerations.DISPLAY_SETTINGS]
        self.column_headers = settings[ConfigEnumerations.COLUMN_HEADERS]
        self.display_refresh = DisplayRefresh(widget=self, interval=int(self.display_settings[ConfigEnumerations.DisplaySettings.REFRESH_RATE]))

        self.header_string_list = []

    # starts the background refresh of the display
    def start_background_refresh(self):
        self.display_refresh.start()

    # stops the background refresh of the display
    def stop_background_refresh(self):
        self.display_refresh.stop()

    # this function will apply settings for the main primary container (PeripheralDisplay class)
    def apply_display_settings(self):
        self.setWindowTitle('Beer Menu Display v1.0')

        operating_system = plat.system()
        if operating_system == 'Windows':
            self.setWindowIcon(QtGui.QIcon(self.file_paths[ConfigEnumerations.FilePaths.APPLICATION_ICON_WINDOWS]))
        elif operating_system == 'Linux':
            self.setWindowIcon(QtGui.QIcon(self.file_paths[ConfigEnumerations.FilePaths.APPLICATION_ICON_PI]))
        else:
            print('Could not determine OS.')
            exit(-1)

        self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.verticalHeader().setSectionResizeMode(QHeaderView.Stretch)

        self.display_refresh.set_interval(int(self.display_settings[ConfigEnumerations.DisplaySettings.REFRESH_RATE]))

    # this function will apply settings for the table containing the database entries
    def apply_table_settings(self):

        # set maximum ROWxCOLUMNS
        self.setRowCount(int(self.display_settings[ConfigEnumerations.DisplaySettings.MAX_NUMBER_OF_ROWS]))
        self.setColumnCount(int(self.display_settings[ConfigEnumerations.DisplaySettings.MAX_NUMBER_OF_COLS]))

        # toggle whether the grid lines are shown on the table
        self.setShowGrid(bool(self.display_settings[ConfigEnumerations.DisplaySettings.SHOW_GRID]))

        # hide row headers
        self.verticalHeader().hide()

        # weights are unused as of the moment, but would be used in the event we
        # decided to allow the user to change the order dynamically
        setting_to_column_associations = dict([
            (ConfigEnumerations.ColumnHeaders.BEER_LOGO, {'name': TableHeaders.BEER_LOGO, 'weight': 0}),
            (ConfigEnumerations.ColumnHeaders.BEER_NAME, {'name': TableHeaders.BEER_NAME, 'weight': 1}),
            (ConfigEnumerations.ColumnHeaders.BREWERY_LOGO, {'name': TableHeaders.BREWERY_LOGO, 'weight': 2}),
            (ConfigEnumerations.ColumnHeaders.BREWERY_NAME, {'name': TableHeaders.BREWERY_NAME, 'weight': 3}),
            (ConfigEnumerations.ColumnHeaders.BEER_NOTES, {'name': TableHeaders.BEER_NOTES, 'weight': 4}),
            (ConfigEnumerations.ColumnHeaders.ABV, {'name': TableHeaders.ABV, 'weight': 5})
        ])

        # clear anything in header list
        self.header_string_list.clear()

        # add headers that are enabled via config.ini
        for i in setting_to_column_associations.keys():
            if dist.strtobool(self.column_headers[str(i)]):
                self.header_string_list.append(setting_to_column_associations[i]['name'])

        # add volume header iff enabled via config.ini
        beer_filter = int(self.display_settings[ConfigEnumerations.DisplaySettings.BEER_ON_TAP])
        if beer_filter == 1:
            self.header_string_list.append(TableHeaders.VOLUME)

        # set the headers to the list created
        self.setHorizontalHeaderLabels(self.header_string_list)

    # this function will fill the table out with data pulled from the database
    def display_table_contents(self):
        beer_filter = int(self.display_settings[ConfigEnumerations.DisplaySettings.BEER_ON_TAP])
        beer_list = None
        if beer_filter == 0:
            beer_list = self.controller.get_beer_list()
        elif beer_filter == 1:
            beer_list = self.controller.get_beer_on_tap_list()
        else:
            print('Invalid beer_on_tap setting.')
            exit(-1)

        # print(beer_list)
        column_header_to_database_attributes = dict([
            (TableHeaders.BEER_LOGO, DatabaseAttributes.BeerAttributes.BEER_LOGO),
            (TableHeaders.BEER_NAME, DatabaseAttributes.BeerAttributes.BEER_NAME),
            (TableHeaders.BREWERY_LOGO, DatabaseAttributes.BreweryAttributes.BREWERY_LOGO),
            (TableHeaders.BREWERY_NAME, DatabaseAttributes.BreweryAttributes.BREWERY_NAME),
            (TableHeaders.BEER_NOTES, DatabaseAttributes.BeerAttributes.TYPE_NAME),
            (TableHeaders.ABV, DatabaseAttributes.BeerAttributes.ABV)
        ])

        row_attributes = []
        for i in column_header_to_database_attributes.keys():
            if self.header_string_list.__contains__(i):
                row_attributes.append(column_header_to_database_attributes[i])

        # print(row_attributes)
        for i in range(0, len(beer_list)):
            for j in range(0, len(row_attributes)):
                if row_attributes[j] == DatabaseAttributes.BeerAttributes.BEER_LOGO\
                        or row_attributes[j] == DatabaseAttributes.BreweryAttributes.BREWERY_LOGO:
                    image = QImage(str(beer_list[i][row_attributes[j]]))
                    if image.isNull() and row_attributes[j] == DatabaseAttributes.BeerAttributes.BEER_LOGO:
                        image = QImage(DEFAULT_BEER_LOGO)
                    elif image.isNull() and row_attributes[j] == DatabaseAttributes.BreweryAttributes.BREWERY_LOGO:
                        image = QImage(DEFAULT_BREWERY_LOGO)
                    image_item = QTableWidgetItem()
                    image_item.setTextAlignment(Qt.AlignCenter)
                    image_item.setData(Qt.DecorationRole, QPixmap.fromImage(image).scaled(256, 256, Qt.KeepAspectRatio, Qt.SmoothTransformation))
                    self.setItem(i, j, image_item)
                else:
                    row_item =  QTableWidgetItem(str(beer_list[i][row_attributes[j]]))
                    row_item.setTextAlignment(Qt.AlignCenter)
                    self.setItem(i, j, row_item)
            if beer_filter == 1:
                volume_bar = QProgressBar()
                volume_bar.setOrientation(Qt.Horizontal)
                volume_bar.setRange(0, 100)
                volume_bar.setValue(int(beer_list[i][DatabaseAttributes.OnTapAttributes.INITIAL_VOLUME]) - int(beer_list[i][DatabaseAttributes.OnTapAttributes.VOLUME_RECORD]))
                self.setCellWidget(i, len(row_attributes), volume_bar)

        # hide un-used rows
        if len(beer_list) < int(self.display_settings[ConfigEnumerations.DisplaySettings.MAX_NUMBER_OF_ROWS]):
            for i in range(0, len(beer_list)):
                self.setRowHidden(i, False)
            for i in range(len(beer_list), int(self.display_settings[ConfigEnumerations.DisplaySettings.MAX_NUMBER_OF_ROWS])):
                self.setRowHidden(i, True)

        # hide un-used columns
        if len(row_attributes) < int(self.display_settings[ConfigEnumerations.DisplaySettings.MAX_NUMBER_OF_COLS]):
            for j in range(0, len(row_attributes) + beer_filter):
                self.setColumnHidden(j, False)
            for j in range(len(row_attributes) + beer_filter, int(self.display_settings[ConfigEnumerations.DisplaySettings.MAX_NUMBER_OF_COLS])):
                self.setColumnHidden(j, True)

    def update_config_settings(self, config):
        self.file_paths = config[ConfigEnumerations.FILE_PATHS]
        self.display_settings = config[ConfigEnumerations.DISPLAY_SETTINGS]
        self.column_headers = config[ConfigEnumerations.COLUMN_HEADERS]

    def refresh(self, config=None, settings_changed=False):
        if settings_changed:
            self.update_config_settings(config)
            self.clear()
            self.apply_display_settings()
            self.apply_table_settings()
        # apply other refresh parts
        self.display_table_contents()


class TableHeaders:
    BEER_LOGO = 'Beer Logo'
    BEER_NAME = 'Beer Name'
    BREWERY_LOGO = 'Brewery Logo'
    BREWERY_NAME = 'Brewery Name'
    BEER_NOTES = 'Beer Notes'
    ABV = 'ABV'
    VOLUME = 'Volume'
