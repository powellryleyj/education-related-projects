# -*- coding: utf-8 -*-

import threading
import time


# class that will be used for background refresh of the PeripheralDisplay and it's child components
class DisplayRefresh:

    # creates a thread tied to a specific QWidget and will run at a specified refresh interval (seconds in integer)
    # defaults to 300 seconds (or 5 minutes)
    def __init__(self, widget, interval=300):
        self.interval = interval
        self.widget = widget
        self.running = True

        self.thread = threading.Thread(target=self.run, args=())
        self.thread.daemon = True

    # function that will start the background refresh
    def start(self):
        self.thread.start()

    # sets the time interval for how often the application will refresh
    def set_interval(self, interval=300):
        self.interval = interval

    # function that will run 'forever' until the program is manually terminated
    def run(self):
        while self.running:
            self.widget.refresh()

            time.sleep(self.interval)

    # function that will stop the background refresh
    def stop(self):
        self.running = False
