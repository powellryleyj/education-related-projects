#!/usr/bin/python
# -*- coding: utf-8 -*-

from PyQt5.QtWidgets import *
from PyQt5 import QtCore


class PageView(QWidget):
    gotoSignal = QtCore.pyqtSignal(str)

    def goto(self, name: str):
        self.gotoSignal.emit(name)
