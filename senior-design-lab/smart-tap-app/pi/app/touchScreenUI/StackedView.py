#!/usr/bin/python
# -*- coding: utf-8 -*-

import platform as plat

from PyQt5.QtCore import pyqtSlot, Q_ENUMS
from PyQt5.QtWidgets import *

from app.resources.ConfigEnumerations import *
from .CreateBeerView import CreateBeerView
from .HomeView import HomeView
from .ListBeerView import ListBeerView
from .PageView import PageView
from .SettingsPage import SettingsPage
from ..peripheral_display.PeripheralDisplay import PeripheralDisplay


class StackedView(QWidget):
    def __init__(self, parent, controllers: {}, settings: {}, display: PeripheralDisplay):
        super(QWidget, self).__init__(parent)
        Q_ENUMS(ConfigEnumerations)
        self.controllers = controllers
        self.settings = settings
        self.layout = QVBoxLayout(self)
        self.btn_bar = QHBoxLayout()
        self.stack = QStackedWidget()
        self.m_pages = {}

        self.build_btns()
        self.build_stack(display=display)

        self.layout.addLayout(self.btn_bar)
        self.layout.addWidget(self.stack)

        self.goto('home')

    def build_stack(self, display: PeripheralDisplay):
        self.register(HomeView(), 'home')
        self.register(ListBeerView(self.controllers['list_beer_controller']), 'listBeer')
        self.register(CreateBeerView(self.controllers['create_beer_controller']), 'createBeer')
        operating_system = plat.system()
        if operating_system == 'Windows':
            self.register(SettingsPage(
                config_file_path=self.settings[ConfigEnumerations.FILE_PATHS][ConfigEnumerations.FilePaths.CONFIG_WINDOWS],
                peripheral_display=display), name='settings')
        elif operating_system == 'Linux':
            self.register(SettingsPage(
                config_file_path=self.settings[ConfigEnumerations.FILE_PATHS][ConfigEnumerations.FilePaths.CONFIG_PI],
                peripheral_display=display), name='settings')

    def build_btns(self):
        home_btn = QPushButton('Home')
        create_beer_btn = QPushButton('Create Beer')
        beer_list_btn = QPushButton('Beer List')
        settings_btn = QPushButton('Settings')

        home_btn.clicked.connect(lambda _: self.goto('home'))
        create_beer_btn.clicked.connect(lambda _: self.goto('createBeer'))
        beer_list_btn.clicked.connect(lambda _: self.goto('listBeer'))
        settings_btn.clicked.connect(lambda _: self.goto('settings'))

        self.btn_bar.addWidget(home_btn)
        self.btn_bar.addWidget(create_beer_btn)
        self.btn_bar.addWidget(beer_list_btn)
        self.btn_bar.addWidget(settings_btn)

    def register(self, widget, name):
        self.m_pages[name] = widget
        self.stack.addWidget(widget)
        if isinstance(widget, PageView):
            widget.gotoSignal.connect(self.goto)

    @pyqtSlot(str)
    def goto(self, name):
        if isinstance(self.stack.currentWidget(), SettingsPage) and name is not 'settings':
            self.stack.currentWidget().leave_settings_page()
        if name in self.m_pages:
            widget = self.m_pages[name]
            self.stack.setCurrentWidget(widget)
            self.setWindowTitle(widget.windowTitle())
