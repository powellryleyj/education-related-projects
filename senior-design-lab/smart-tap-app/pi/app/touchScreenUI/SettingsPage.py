#!/usr/bin/python
# -*- coding: utf-8 -*-

import configparser as config_parser
import distutils.util as dist
import os as os

from PyQt5.QtCore import Q_ENUMS, Qt
from PyQt5.QtWidgets import QTabWidget, QWidget, QGridLayout, QLabel, QSlider, QMessageBox, QTabBar, QRadioButton

from app.peripheral_display.PeripheralDisplay import PeripheralDisplay
from app.resources.ConfigEnumerations import ConfigEnumerations
from app.resources.QTabButton import QTabButton
from app.resources.QToggleSwitch import QToggleSwitch
# NOTE: This class should be created every time the page is navigated too and destroyed when leaving.
from app.touchScreenUI.PageView import PageView


class SettingsPage(PageView):

    # TODO: figure out how to cause settings update to touch screen display
    def __init__(self, config_file_path, peripheral_display: PeripheralDisplay, parent=None):
        super().__init__()
        Q_ENUMS(ConfigEnumerations)
        # self.setWindowTitle("Application Settings")
        self.tab_widget = QTabWidget(parent=self)
        self.config_file_path = config_file_path
        self.config = None
        self.read_config_settings()

        # self.home_button = QTabButton("Home")
        # self.home_button.clicked.connect(self.home_button_pushed)

        self.save_button = QTabButton("Save")
        self.save_button.clicked.connect(self.save_settings)

        self.config_changed = False

        self.peripheral_display = peripheral_display

        self.menu_settings_tab = QWidget()
        self.display_settings_tab = QWidget()

        self.display_objects = self.create_display_settings_tab()
        self.menu_objects = self.create_menu_settings_tab()

        self.tab_widget.insertTab(0, self.menu_settings_tab, "Touch Screen Settings")
        self.tab_widget.insertTab(1, self.display_settings_tab, "Display Settings")
        self.tab_widget.tabBar().insertTab(2, "")
        self.tab_widget.tabBar().setTabEnabled(2, False)
        self.tab_widget.tabBar().setTabButton(2, QTabBar.RightSide, self.save_button)
        self.tab_widget.setTabsClosable(False)

        self.tab_widget.show()
        grid = QGridLayout(self)
        grid.addWidget(self.tab_widget)
        self.setLayout(grid)

    # Event Handler for when the Home Button is pushed
    def leave_settings_page(self):
        if self.config_changed:
            self.unsaved_settings_warning()

    # This function will read the config file settings
    def read_config_settings(self):
        config_file = os.path.relpath(self.config_file_path, os.curdir)
        config = config_parser.ConfigParser(comment_prefixes=('#', ';'), delimiters=(':', '='))
        config.read(config_file)
        self.config = config

    # TODO: write this
    # This function will create the content based on the touch screen display menu settings
    def create_menu_settings_tab(self):
        layout = QGridLayout(self.menu_settings_tab)
        layout.addWidget(QLabel('Coming Soon...'))
        return dict([])

    # TODO: clean this up - lots of duplicate code
    # This function will create the content based on the peripheral display menu settings
    def create_display_settings_tab(self):
        display_settings = self.config[ConfigEnumerations.DISPLAY_SETTINGS]
        column_headers = self.config[ConfigEnumerations.COLUMN_HEADERS]
        layout = QGridLayout(self.display_settings_tab)

        # Create toggle switches
        beer_logo_slider = QToggleSwitch()
        beer_logo_slider.setChecked(dist.strtobool(column_headers[ConfigEnumerations.ColumnHeaders.BEER_LOGO]))
        beer_logo_slider.clicked.connect(slot=self.toggle_switch_changed)

        beer_name_slider = QToggleSwitch()
        beer_name_slider.setChecked(dist.strtobool(column_headers[ConfigEnumerations.ColumnHeaders.BEER_NAME]))
        beer_name_slider.clicked.connect(slot=self.toggle_switch_changed)

        brewery_logo_slider = QToggleSwitch()
        brewery_logo_slider.setChecked(dist.strtobool(column_headers[ConfigEnumerations.ColumnHeaders.BREWERY_LOGO]))
        brewery_logo_slider.clicked.connect(slot=self.toggle_switch_changed)

        brewery_name_slider = QToggleSwitch()
        brewery_name_slider.setChecked(dist.strtobool(column_headers[ConfigEnumerations.ColumnHeaders.BREWERY_NAME]))
        brewery_name_slider.clicked.connect(slot=self.toggle_switch_changed)

        beer_notes_slider = QToggleSwitch()
        beer_notes_slider.setChecked(dist.strtobool(column_headers[ConfigEnumerations.ColumnHeaders.BEER_NOTES]))
        beer_notes_slider.clicked.connect(slot=self.toggle_switch_changed)

        abv_slider = QToggleSwitch()
        abv_slider.setChecked(dist.strtobool(column_headers[ConfigEnumerations.ColumnHeaders.ABV]))
        abv_slider.clicked.connect(slot=self.toggle_switch_changed)

        carousel_slider = QToggleSwitch()
        carousel_slider.setChecked(dist.strtobool(display_settings[ConfigEnumerations.DisplaySettings.CAROUSEL]))
        carousel_slider.clicked.connect(slot=self.toggle_switch_changed)

        show_grid_slider = QToggleSwitch()
        show_grid_slider.setChecked(dist.strtobool(display_settings[ConfigEnumerations.DisplaySettings.SHOW_GRID]))
        show_grid_slider.clicked.connect(slot=self.toggle_switch_changed)

        # create sliders
        max_rows_slider = QSlider(orientation=Qt.Horizontal)
        max_rows_slider.setTickPosition(QSlider.TicksBelow)
        max_rows_slider.setTickInterval(1)
        max_rows_slider.setSingleStep(1)
        max_rows_slider.setMinimum(1)
        max_rows_slider.setMaximum(10)
        max_rows_slider.setValue(int(display_settings[ConfigEnumerations.DisplaySettings.MAX_NUMBER_OF_ROWS]))
        max_rows_slider.valueChanged.connect(slot=self.slider_value_changed)

        min_rows_slider = QSlider(orientation=Qt.Horizontal)
        min_rows_slider.setTickPosition(QSlider.TicksBelow)
        min_rows_slider.setTickInterval(1)
        min_rows_slider.setSingleStep(1)
        min_rows_slider.setMinimum(1)
        min_rows_slider.setMaximum(10)
        min_rows_slider.setValue(int(display_settings[ConfigEnumerations.DisplaySettings.MIN_NUMBER_OF_ROWS]))
        min_rows_slider.valueChanged.connect(slot=self.slider_value_changed)

        max_cols_slider = QSlider(orientation=Qt.Horizontal)
        max_cols_slider.setTickPosition(QSlider.TicksBelow)
        max_cols_slider.setTickInterval(1)
        max_cols_slider.setSingleStep(1)
        max_cols_slider.setMinimum(1)
        max_cols_slider.setMaximum(10)
        max_cols_slider.setValue(int(display_settings[ConfigEnumerations.DisplaySettings.MAX_NUMBER_OF_COLS]))
        max_cols_slider.valueChanged.connect(slot=self.slider_value_changed)

        min_cols_slider = QSlider(orientation=Qt.Horizontal)
        min_cols_slider.setTickPosition(QSlider.TicksBelow)
        min_cols_slider.setTickInterval(1)
        min_cols_slider.setSingleStep(1)
        min_cols_slider.setMinimum(1)
        min_cols_slider.setMaximum(10)
        min_cols_slider.setValue(int(display_settings[ConfigEnumerations.DisplaySettings.MIN_NUMBER_OF_COLS]))
        min_cols_slider.valueChanged.connect(slot=self.slider_value_changed)

        # This slider is unique in that each tick value is representative of 30 seconds.
        # Meaning a minimum of 30 seconds, and a maximum of 600 seconds
        refresh_rate_slider = QSlider(orientation=Qt.Horizontal)
        refresh_rate_slider.setTickPosition(QSlider.TicksBelow)
        refresh_rate_slider.setMinimum(1)
        refresh_rate_slider.setMaximum(20)
        refresh_rate_slider.setTickInterval(1)
        refresh_rate_slider.setSingleStep(1)
        refresh_rate_slider.setValue(int(display_settings[ConfigEnumerations.DisplaySettings.REFRESH_RATE]) / 30)
        refresh_rate_slider.valueChanged.connect(slot=self.slider_value_changed)

        # create radio buttons for choosing which filter to place on beers
        on_tap_only = QRadioButton("On Tap Only")
        all_beer = QRadioButton("All Beers")
        beer_filter = int(display_settings[ConfigEnumerations.DisplaySettings.BEER_ON_TAP])
        if beer_filter == 0:
            all_beer.setChecked(True)
            on_tap_only.setChecked(False)
        else:
            all_beer.setChecked(False)
            on_tap_only.setChecked(True)

        on_tap_only.toggled.connect(lambda: self.radio_toggled(on_tap_only))
        all_beer.toggled.connect(lambda: self.radio_toggled(all_beer))

        # add to layout
        # add toggle switches
        layout.addWidget(QLabel(text='Beer Logo'), 0, 0)
        layout.addWidget(beer_logo_slider, 0, 1)

        layout.addWidget(QLabel(text='Beer Name'), 1, 0)
        layout.addWidget(beer_name_slider, 1, 1)

        layout.addWidget(QLabel(text='Brewery Logo'), 2, 0)
        layout.addWidget(brewery_logo_slider, 2, 1)

        layout.addWidget(QLabel(text='Brewery Name'), 3, 0)
        layout.addWidget(brewery_name_slider, 3, 1)

        layout.addWidget(QLabel(text='Beer Notes'), 4, 0)
        layout.addWidget(beer_notes_slider, 4, 1)

        layout.addWidget(QLabel(text='ABV'), 5, 0)
        layout.addWidget(abv_slider, 5, 1)

        layout.addWidget(QLabel(text='Carousel'), 6, 0)
        layout.addWidget(carousel_slider, 6, 1)

        layout.addWidget(QLabel(text='Show Grid'), 7, 0)
        layout.addWidget(show_grid_slider, 7, 1)

        # add radio buttons
        layout.addWidget(QLabel(text="Filter Beers Shown"), 8, 0)
        layout.addWidget(all_beer, 8, 1)
        layout.addWidget(on_tap_only, 8, 2)

        # TODO: consider adding spin-boxes above sliders to show value set?
        # add sliders
        layout.addWidget(QLabel(text='Maximum Number of Rows'), 9, 0)
        layout.addWidget(max_rows_slider, 9, 1)

        layout.addWidget(QLabel(text='Minimum Number of Rows'), 10, 0)
        layout.addWidget(min_rows_slider, 10, 1)

        layout.addWidget(QLabel(text='Maximum Number of Columns'), 11, 0)
        layout.addWidget(max_cols_slider, 11, 1)

        layout.addWidget(QLabel(text='Minimum Number of Columns'), 12, 0)
        layout.addWidget(min_cols_slider, 12, 1)

        layout.addWidget(QLabel(text='Display Refresh Rate'), 13, 0)
        layout.addWidget(refresh_rate_slider, 13, 1)

        return dict([(ConfigEnumerations.ColumnHeaders.BEER_LOGO, beer_logo_slider),
                     (ConfigEnumerations.ColumnHeaders.BEER_NAME, beer_name_slider),
                     (ConfigEnumerations.ColumnHeaders.BREWERY_NAME, brewery_name_slider),
                     (ConfigEnumerations.ColumnHeaders.BREWERY_LOGO, brewery_logo_slider),
                     (ConfigEnumerations.ColumnHeaders.BEER_NOTES, beer_notes_slider),
                     (ConfigEnumerations.ColumnHeaders.ABV, abv_slider),
                     (ConfigEnumerations.DisplaySettings.MAX_NUMBER_OF_ROWS, max_rows_slider),
                     (ConfigEnumerations.DisplaySettings.MIN_NUMBER_OF_ROWS, min_rows_slider),
                     (ConfigEnumerations.DisplaySettings.MAX_NUMBER_OF_COLS, max_cols_slider),
                     (ConfigEnumerations.DisplaySettings.MIN_NUMBER_OF_COLS, min_cols_slider),
                     (ConfigEnumerations.DisplaySettings.CAROUSEL, carousel_slider),
                     (ConfigEnumerations.DisplaySettings.SHOW_GRID, show_grid_slider),
                     (ConfigEnumerations.DisplaySettings.REFRESH_RATE, refresh_rate_slider),
                     (ConfigEnumerations.DisplaySettings.BEER_ON_TAP, beer_filter)])

    # event handler to change how beer are filtered on the display
    # the value assigned to BEER_ON_TAP is heavily coupled in the PeripheralDisplay
    # and likely will break things if any other value than 0 or 1
    def radio_toggled(self, radio):
        self.config_changed = True
        if radio.text() == 'On Tap Only' and radio.isChecked():
            # print('on tap')
            self.display_objects[ConfigEnumerations.DisplaySettings.BEER_ON_TAP] = 1
        elif radio.text() == 'All Beers' and radio.isChecked():
            # print('all beer')
            self.display_objects[ConfigEnumerations.DisplaySettings.BEER_ON_TAP] = 0

    # event handler to change truth value of config_changed
    def toggle_switch_changed(self):
        self.config_changed = True
        # print('toggle')

    # TODO: link spinbox to slider?
    # event handler to change truth value of config_changed and update spinbox of associated slider
    def slider_value_changed(self):
        self.config_changed = True
        # display_object_name = None
        # print(self.sender().value() * 30)
        # for i in self.display_objects:
        #     if self.sender() == self.display_objects[i]:
        #         display_object_name = i
        # print(display_object_name)

    # This function will display a warning message if navigating from this page with any un-saved settings
    def unsaved_settings_warning(self):
        if self.config_changed:
            msg = QMessageBox()
            msg.setWindowTitle('Warning: Unsaved Settings!')
            msg.setIcon(QMessageBox.Warning)
            msg.setText('There are changed settings that are not saved. Do you want to save?')
            msg.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
            ret = msg.exec_()
            if ret == QMessageBox.Yes:
                self.save_settings()
            else:
                self.revert_changes()

    # this function will reset all display setting and menu setting objects
    # back to their original states held by config.ini
    def revert_changes(self):
        # TODO: revert changes to any menu settings
        # for i in self.config[ConfigEnumerations.MENU_SETTINGS]:

        for j in self.config[ConfigEnumerations.COLUMN_HEADERS]:
            if isinstance(self.display_objects[j], QToggleSwitch):
                self.display_objects[j].setChecked(dist.strtobool(self.config[ConfigEnumerations.COLUMN_HEADERS][j]))
            elif isinstance(self.display_objects[j], QSlider):
                self.display_objects[j].setValue(int(self.config[ConfigEnumerations.COLUMN_HEADERS][j]))

        for k in self.config[ConfigEnumerations.DISPLAY_SETTINGS]:
            if isinstance(self.display_objects[k], QToggleSwitch):
                self.display_objects[k].setChecked(dist.strtobool(self.config[ConfigEnumerations.DISPLAY_SETTINGS][k]))
            elif isinstance(self.display_objects[k], QSlider):
                self.display_objects[k].setValue(int(self.config[ConfigEnumerations.DISPLAY_SETTINGS][k]))
            elif k == ConfigEnumerations.DisplaySettings.BEER_ON_TAP:
                self.display_objects[k] = int(self.config[ConfigEnumerations.DISPLAY_SETTINGS][k])

        self.config_changed = False

    # This function will write any changes made to settings to the config file
    def save_settings(self):
        self.config_changed = False

        # get path to config file
        config_file_path = os.path.relpath(self.config_file_path, os.curdir)
        config_file = open(config_file_path, 'w')

        # create ConfigParser object
        config = config_parser.ConfigParser(comment_prefixes=('#', ';'), delimiters=(':', '='))

        # re-write File Paths
        config.add_section(ConfigEnumerations.FILE_PATHS)
        for i in self.config[ConfigEnumerations.FILE_PATHS]:
            # print(self.config[ConfigEnumerations.FILE_PATHS][i])
            config.set(ConfigEnumerations.FILE_PATHS, i, self.config[ConfigEnumerations.FILE_PATHS][i])

        # re-write Menu Settings
        config.add_section(ConfigEnumerations.MENU_SETTINGS)
        # TODO: add menu settings
        # for j in self.config[ConfigEnumerations.MENU_SETTINGS]:
        # config.set(ConfigEnumerations.MENU_SETTINGS, j, self.menu_objects[j])

        # re-write Column Header Settings
        config.add_section(ConfigEnumerations.COLUMN_HEADERS)
        for l in self.config[ConfigEnumerations.COLUMN_HEADERS]:
            if isinstance(self.display_objects[l], QToggleSwitch):
                val = self.display_objects[l].isChecked()
                config.set(ConfigEnumerations.COLUMN_HEADERS, l, str(val))
            else:
                # warnings.warn_explicit("A unhandled display object was read from", RuntimeWarning, SettingsPage)
                print("SettingsPage tried to read from an unhandled object")

        # re-write Display Settings
        config.add_section(ConfigEnumerations.DISPLAY_SETTINGS)
        for k in self.config[ConfigEnumerations.DISPLAY_SETTINGS]:
            if isinstance(self.display_objects[k], QSlider):
                multiplier = 1
                if self.display_objects[k] == self.display_objects[ConfigEnumerations.DisplaySettings.REFRESH_RATE]:
                    multiplier = 30
                val = self.display_objects[k].value() * multiplier
                # print(val)
                config.set(ConfigEnumerations.DISPLAY_SETTINGS, k, str(val))
            elif isinstance(self.display_objects[k], QToggleSwitch):
                val = self.display_objects[k].isChecked()
                # print(val)
                config.set(ConfigEnumerations.DISPLAY_SETTINGS, k, str(val))
            elif k == ConfigEnumerations.DisplaySettings.BEER_ON_TAP:
                config.set(ConfigEnumerations.DISPLAY_SETTINGS, k, str(self.display_objects[ConfigEnumerations.DisplaySettings.BEER_ON_TAP]))
            else:
                # warnings.warn_explicit("A unhandled display object was read from", RuntimeWarning, SettingsPage)
                print("SettingsPage tried to read from an unhandled object")

        config.write(config_file, space_around_delimiters=True)
        config_file.close()

        self.notify_peripheral_display()
        self.notify_touch_screen()

    # This function will notify the peripheral display that changes have been made to the config file
    def notify_peripheral_display(self):
        self.read_config_settings()
        self.peripheral_display.refresh(config=self.config, settings_changed=True)

    # TODO: write this
    # This function will notify the touch screen display that changes have been made to the config file
    def notify_touch_screen(self):
        pass
        # print(1)
