#!/usr/bin/python
# -*- coding: utf-8 -*-

from PyQt5.Qt import *
from PyQt5.QtWidgets import *

from app.controllers.CreateBeerController import CreateBeerController
from .PageView import PageView


# TODO: modify this to allow editing by populating with beer selected from list view
class CreateBeerView(PageView):

    def __init__(
            self,
            create_beer_ctrl: CreateBeerController):
        super().__init__()
        self.create_beer_controller = create_beer_ctrl
        self.brewer_var = ''
        self.notes_var = []
        self.beer_type_var = ''
        self.abv_var = ''
        self.breweries = list(self.create_beer_controller.get_breweries())
        self.notes = list(self.create_beer_controller.get_notes(return_tuple=True))
        self.beer_types = list(self.create_beer_controller.get_beer_types())

        self.abv_val = None
        self.beer_name = None
        self.brewery_box = None
        self.type_box = None
        self.notes_initial_list = None
        self.notes_final_list = None

        self.init_view()

    def init_view(self):
        h_box_main = QHBoxLayout(self)
        column_1 = QVBoxLayout(self)
        column_2 = QVBoxLayout(self)
        data_group = QGroupBox('Beer Data:', self)
        notes_group = QGroupBox('Notes:', self)

        import_btn = QPushButton('Import List')
        save_button = QPushButton('Save')

        # connect buttons
        import_btn.clicked.connect(self.import_from_file)
        save_button.clicked.connect(self.do_submit)

        data_group.setLayout(self.create_data_widget())
        notes_group.setLayout(self.create_notes_widget())

        column_1.addWidget(import_btn)
        column_1.addWidget(data_group)
        column_2.addWidget(save_button, Qt.AlignCenter)
        column_2.addWidget(notes_group, Qt.AlignCenter)
        h_box_main.addLayout(column_1)
        h_box_main.addLayout(column_2)

        self.setLayout(h_box_main)

    def create_data_widget(self):
        grid_box = QGridLayout(self)
        # create widgets
        beer_lbl = QLabel('Name:')
        self.beer_name = QLineEdit(self)
        brewery_lbl = QLabel('Brewery:')
        self.brewery_box = QComboBox()
        type_lbl = QLabel('Type:')
        self.type_box = QComboBox()
        abv_lbl = QLabel('ABV:')
        self.abv_val = QLineEdit()

        # set functionality for widgets
        self.brewery_box.addItems(self.breweries)
        self.brewery_box.setEditable(True)
        self.type_box.addItems(self.beer_types)
        self.type_box.setEditable(True)
        self.abv_val.setInputMask('9.99')

        grid_box.addWidget(beer_lbl, 1, 0, Qt.AlignRight)
        grid_box.addWidget(brewery_lbl, 2, 0, Qt.AlignRight)
        grid_box.addWidget(type_lbl, 3, 0, Qt.AlignRight)
        grid_box.addWidget(abv_lbl, 4, 0, Qt.AlignRight)
        grid_box.addWidget(self.beer_name, 1, 1, 1, 1, Qt.AlignLeft)
        grid_box.addWidget(self.brewery_box, 2, 1, Qt.AlignLeft)
        grid_box.addWidget(self.type_box, 3, 1, Qt.AlignLeft)
        grid_box.addWidget(self.abv_val, 4, 1, Qt.AlignLeft)
        # TODO: figure out how to add stretch to grid
        #   also enlargen the size of the objects
        return grid_box

    # noinspection PyAttributeOutsideInit
    def create_notes_widget(self):
        self.notes_initial_list = QListWidget()
        self.notes_final_list = QListWidget()
        self.notes_initial_list.addItems(self.notes)
        h_box = QHBoxLayout(self)
        v_box = QVBoxLayout(self)

        self.btn_to_selected = QPushButton(">>")
        self.btn_move_to_available = QPushButton(">")
        self.btn_move_to_selected = QPushButton("<")
        self.btn_to_available = QPushButton("<<")

        v_box.addStretch()
        v_box.addWidget(self.btn_to_selected)
        v_box.addWidget(self.btn_move_to_available)
        v_box.addWidget(self.btn_move_to_selected)
        v_box.addWidget(self.btn_to_available)
        v_box.addStretch()

        self.notes_initial_list.itemSelectionChanged.connect(self.update_buttons_status)
        self.notes_final_list.itemSelectionChanged.connect(self.update_buttons_status)
        self.btn_to_selected.clicked.connect(self.btn_to_selected_clicked)
        self.btn_move_to_available.clicked.connect(self.btn_move_to_available_clicked)
        self.btn_move_to_selected.clicked.connect(self.btn_move_to_selected_clicked)
        self.btn_to_available.clicked.connect(self.btn_to_available_clicked)

        h_box.addWidget(self.notes_initial_list)
        h_box.addLayout(v_box)
        h_box.addWidget(self.notes_final_list)

        return h_box

    def get_selected_notes(self):
        results = []
        for i in range(self.notes_final_list.count()):
            results.append(self.notes_final_list.item(i).text())
        return results

    def do_submit(self):
        self.create_beer_controller.create_individual_beer(
            self.beer_name.text(),
            self.brewery_box.currentText(),
            self.get_selected_notes(),
            self.type_box.currentText(),
            float(self.abv_val.text()))
        self.goto('home')

    def import_from_file(self):
        file_name = QFileDialog.getOpenFileName(filter='*.csv')
        self.create_beer_controller.create_multiple_beer(file_name[0])
        self.goto('listBeer')

    @pyqtSlot()
    def update_buttons_status(self):
        self.btn_move_to_available.setDisabled(not bool(self.notes_initial_list.selectedItems())
                                               or self.notes_final_list.currentRow() == 0)
        self.btn_move_to_selected.setDisabled(not bool(self.notes_final_list.selectedItems()))

    @pyqtSlot()
    def btn_move_to_available_clicked(self):
        self.notes_final_list.addItem(self.notes_initial_list.takeItem(self.notes_initial_list.currentRow()))

    @pyqtSlot()
    def btn_move_to_selected_clicked(self):
        self.notes_initial_list.addItem(self.notes_final_list.takeItem(self.notes_final_list.currentRow()))

    @pyqtSlot()
    def btn_to_available_clicked(self):
        while self.notes_final_list.count() > 0:
            self.notes_initial_list.addItem(self.notes_final_list.takeItem(0))

    @pyqtSlot()
    def btn_to_selected_clicked(self):
        while self.notes_initial_list.count() > 0:
            self.notes_final_list.addItem(self.notes_initial_list.takeItem(0))
