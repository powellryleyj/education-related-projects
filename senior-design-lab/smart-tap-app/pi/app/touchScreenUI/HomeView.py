#!/usr/bin/python
# -*- coding: utf-8 -*-

from PyQt5.QtWidgets import *
from .PageView import PageView


class HomeView(PageView):
    """Frame that will be shown on start-up and is considered the home page of the application"""

    def __init__(self):
        super().__init__()
        self.init_view()

    def init_view(self):
        v_box = QVBoxLayout(self)
        label = QLabel('Welcome to the Smart Beer Tap.  Here you can store, manage and track beer that you serve.')
        v_box.addWidget(label)
        self.setLayout(v_box)
