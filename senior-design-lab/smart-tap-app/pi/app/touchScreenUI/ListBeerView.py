#!/usr/bin/python
# -*- coding: utf-8 -*-

from PyQt5.QtWidgets import *
from app.controllers.ListBeerController import ListBeerController
from .PageView import PageView


class ListBeerView(PageView):
    """Frame that will be shown on start-up and is considered the home page of the application"""

    def __init__(self, controller: ListBeerController):
        super().__init__()
        self.controller = controller
        self.beer = self.controller.get_beer_list()
        self.table = None
        self.init_view()

    def init_view(self):
        v_box = QVBoxLayout(self)
        self.table = QTableWidget(self)

        self.table.setColumnCount(4)
        self.table.setHorizontalHeaderLabels(['Name', 'Brewery', 'Type', 'ABV'])
        self.table.setRowCount(len(self.beer))
        self.table.verticalHeader().hide()

        for idx, b in enumerate(self.beer):
            self.table.setItem(idx, 0, QTableWidgetItem(b['beerName']))
            self.table.setItem(idx, 1, QTableWidgetItem(b['breweryName']))
            self.table.setItem(idx, 2, QTableWidgetItem(b['typeName']))
            self.table.setItem(idx, 3, QTableWidgetItem(str(b['abv'])))

        edit_btn = QPushButton('Edit Beer')
        v_box.addWidget(self.table)
        v_box.addWidget(edit_btn)
        self.table.show()
        self.setLayout(v_box)

#     def update_table(self):
#         for i in self.tree_view.get_children():
#             self.tree_view.delete(i)
#         self.create_table(self.tree_view)
#         self.load_table(self.tree_view)
