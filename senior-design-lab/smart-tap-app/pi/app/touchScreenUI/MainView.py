#!/usr/bin/python
# -*- coding: utf-8 -*-

import platform as plat

from PyQt5 import QtGui
from PyQt5.QtCore import Q_ENUMS
from PyQt5.QtWidgets import *

from .StackedView import StackedView
from ..peripheral_display.PeripheralDisplay import PeripheralDisplay
from ..resources.ConfigEnumerations import ConfigEnumerations


class MainView(QMainWindow):
    """Controller and Root for the Brewery Menu Application"""

    def __init__(self, controllers: {}, settings: {}, display: PeripheralDisplay, flags=None, parent=None, *args, **kwargs):
        super().__init__(flags, *args, **kwargs)
        Q_ENUMS(ConfigEnumerations)
        self.setWindowTitle('Brewery Menu v1.0')
        # self.setGeometry(10, 10, 800, 480)

        operating_system = plat.system()
        if operating_system == 'Windows':
            self.setWindowIcon(QtGui.QIcon(settings[ConfigEnumerations.FILE_PATHS][ConfigEnumerations.FilePaths.APPLICATION_ICON_WINDOWS]))
        elif operating_system == 'Linux':
            self.setWindowIcon(QtGui.QIcon(settings[ConfigEnumerations.FILE_PATHS][ConfigEnumerations.FilePaths.APPLICATION_ICON_PI]))
        else:
            print('Could not determine OS.')
            exit(-1)

        self.stack = StackedView(self, controllers, settings=settings, display=display)
        self.setCentralWidget(self.stack)
