import pathlib

from PIL import Image


def image_conversion(input_file_path):
    icon_sizes = [(32, 32), (64, 64)]
    print(input_file_path)
    path = pathlib.Path(input_file_path)
    if path.exists() and path.is_file():
        image = Image.open(input_file_path)
        print("image opened")
        new_filename = path.with_suffix('.ico')
        print(new_filename)
        image.save(new_filename, sizes=icon_sizes)
        print('new image saved')
    else:
        print('File could not be found.')
        return None
