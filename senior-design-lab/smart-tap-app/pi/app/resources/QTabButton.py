#!/usr/bin/python
# -*- coding: utf-8 -*-

from PyQt5 import QtWidgets


class QTabButton(QtWidgets.QWidget):

    def __init__(self, button_name: str):
        super(QTabButton, self).__init__()
        self.button_home = QtWidgets.QPushButton(button_name)
        self.layout = QtWidgets.QVBoxLayout()
        self.layout.setSpacing(0)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.addWidget(self.button_home)
        self.setLayout(self.layout)
        self.clicked = self.button_home.clicked

    def connect(self, a0):
        self.button_home.clicked.connect(a0)
