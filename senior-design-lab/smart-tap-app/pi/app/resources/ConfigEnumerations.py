class ConfigEnumerations:
    DISPLAY_SETTINGS = 'Display Settings'
    MENU_SETTINGS = 'Menu Settings'
    FILE_PATHS = 'File Paths'
    COLUMN_HEADERS = 'Column Headers'

    class FilePaths:
        DATABASE_NAME = 'database_name'
        DATABASE_PI_DIR = 'database_pi_dir'
        DATABASE_WINDOWS_DIR = 'database_windows_dir'
        APPLICATION_ICON_WINDOWS = 'application_icon_windows'
        APPLICATION_ICON_PI = 'application_icon_pi'
        CONFIG_WINDOWS = 'config_file_windows'
        CONFIG_PI = 'config_file_pi'

    class MenuSettings:
        # add stuff here
        MENU = 'menu'

    class ColumnHeaders:
        BEER_LOGO = 'beer_logo'
        BEER_NAME = 'beer_name'
        BREWERY_LOGO = 'brewery_logo'
        BREWERY_NAME = 'brewery_name'
        BEER_NOTES = 'beer_notes'
        ABV = 'abv'

    class DisplaySettings:
        BEER_ON_TAP = 'beer_on_tap'
        MAX_NUMBER_OF_ROWS = 'maximum_number_of_rows'
        MIN_NUMBER_OF_ROWS = 'minimum_number_of_rows'
        MAX_NUMBER_OF_COLS = 'maximum_number_of_columns'
        MIN_NUMBER_OF_COLS = 'minimum_number_of_columns'
        CAROUSEL = 'carousel'
        SHOW_GRID = 'show_grid'
        REFRESH_RATE = 'refresh_rate'
