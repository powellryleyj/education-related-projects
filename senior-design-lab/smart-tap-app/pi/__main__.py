#!/usr/bin/python
# -*- coding: utf-8 -*-

import configparser as config_parser
import os as os
import platform as plat
import sys

from PyQt5.QtGui import QScreen
from PyQt5.QtWidgets import QMessageBox, QApplication

from app.controllers.CreateBeerController import CreateBeerController
from app.controllers.ListBeerController import ListBeerController
from app.controllers.PeripheralDisplayController import PeripheralDisplayController
from app.peripheral_display.PeripheralDisplay import PeripheralDisplay
from app.touchScreenUI.MainView import MainView


def check_monitors():
    monitors = QApplication.screens()
    while len(monitors) < 2:
        ret = check_monitors_popup()
        if ret == QMessageBox.Retry:
            monitors = QApplication.screens()
        elif ret == QMessageBox.Ignore:
            monitors = [None, None]
    return monitors


def check_monitors_popup():
    msg = QMessageBox()
    msg.setWindowTitle('Warning! No Display Detected')
    msg.setIcon(QMessageBox.Warning)
    msg.setText('There is no device plugged into the HDMI port. Please attach a device.\n\rIgnoring this message will not allow the application to auto-place itself.')
    msg.setStandardButtons(QMessageBox.Retry | QMessageBox.Ignore)
    return msg.exec_()


def start_touch_screen(position: QScreen, settings: {}, db_loc: str, db_name: str, display: PeripheralDisplay):
    controllers = {
        'create_beer_controller': CreateBeerController(db_loc=db_loc, db_name=db_name),
        'list_beer_controller': ListBeerController(db_loc=db_loc, db_name=db_name),
    }
    gui = MainView(controllers=controllers, settings=settings, display=display, flags=None)
    if position is not None:
        gui.move(position.availableGeometry().x(), position.availableGeometry().y())
    gui.showMaximized()
    return gui


# starts the peripheral display component at the given location in the form of 'WIDTHxHEIGHT+PADX+PADY'
def start_peripheral_display(position: QScreen, settings: {}, db_loc: str, db_name: str):
    display = PeripheralDisplay(ctrl=PeripheralDisplayController(db_loc=db_loc, db_name=db_name), settings=settings)
    if position is not None:
        display.move(position.availableGeometry().x(), position.availableGeometry().y())
    display.apply_display_settings()
    display.apply_table_settings()
    display.display_table_contents()
    display.start_background_refresh()
    display.showMaximized()
    return display


def main():
    config_file = os.path.relpath('config/config.ini', os.curdir)
    config = config_parser.ConfigParser(comment_prefixes=('#', ';'), delimiters=(':', '='))
    config.read(config_file)

    db_name = config['File Paths']['database_name']

    # determine path to BeerTapSystem.db
    operating_system = plat.system()
    db_loc = None
    if operating_system == 'Windows':
        db_loc = config['File Paths']['database_windows_dir']
    elif operating_system == 'Linux':
        db_loc = config['File Paths']['database_pi_dir']  # used in pi environment
    else:
        print('Could not determine OS.')
        exit(-1)

    # create the PyQt application
    app = QApplication(sys.argv)

    # check for positioning of touch screen and peripheral display
    screens = check_monitors()

    # following line is temporary until it is determined whether multi-threading is needed
    display = start_peripheral_display(position=screens[1], settings=config, db_name=db_name, db_loc=db_loc)
    gui = start_touch_screen(position=screens[0], settings=config, db_name=db_name, db_loc=db_loc, display=display)

    # this line is needed to keep application running until it is closed by the user
    # the entire system will exit upon closing open widgets
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
