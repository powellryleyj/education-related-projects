// Load in the libraries
#include <SPI.h>
#include <RF24.h>

// Set the CE & CSN pins
#define CE_PIN   7
#define CSN_PIN 8
#define Sensor 5

// This is the address used to send/receive
const byte rxAddr[6] = "00001";
int numberCount = 0;
int prevNumber = 0;
int savedCount = 0;
// Create a Radio
RF24 radio(CE_PIN, CSN_PIN); 

void setup() {
  
  // Start up the Serial connection
  while (!Serial);
  Serial.begin(9600);
  
  // Start the Radio!
  radio.begin();
  
  // Power setting. Due to likelihood of close proximity of the devices, set as RF24_PA_MIN (RF24_PA_MAX is default)
  radio.setPALevel(RF24_PA_MAX); // RF24_PA_MIN, RF24_PA_LOW, RF24_PA_HIGH, RF24_PA_MAX
  
  // Slower data rate for better range
  //radio.setDataRate( RF24_250KBPS ); // RF24_250KBPS, RF24_1MBPS, RF24_2MBPS
  
  // Number of retries and set tx/rx address
  //radio.setRetries(15, 15);
  radio.openWritingPipe(rxAddr);

// Stop listening, so we can send!
  radio.stopListening();
}

void loop() {

    int number = digitalRead(Sensor);
    if(number == 0 && prevNumber == 1){
      numberCount++;
      savedCount++;
     }
    if(number == 1 && prevNumber == 0){
      numberCount++;
      savedCount++;
    }
    
    if(numberCount==100){
      String count = String(savedCount);
      String str = "";
      str.concat(savedCount);
      int str_len = str.length() + 1;
      char char_array[str_len];
      str.toCharArray(char_array,str.length()+1);
      radio.write(&char_array, sizeof(char_array));
      Serial.println(savedCount);
      numberCount = 0;
    }
    prevNumber = number; 
}
