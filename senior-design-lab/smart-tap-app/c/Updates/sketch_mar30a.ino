#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <RF24Network.h>

// define the pins
#define CE_PIN   7
#define CSN_PIN 8

//SoftwareSerial BTserial(2, 3);
// Create a Radio
RF24 radio(CE_PIN, CSN_PIN); 
RF24Network network(radio); 
const uint16_t hub_node = 01;   // Address of our node in Octal format ( 04,031, etc)
const uint16_t beer_node_1 = 02;
const uint16_t beer_node_2 = 03;
const uint16_t beer_node_3 = 04;

void setup() {
  Serial.begin(9600);
  radio.begin();
  network.begin(90, hub_node);  //(channel, node address)
  radio.setDataRate(RF24_2MBPS);
}
void loop() {
  network.update();
  while ( network.available() ) {     // Is there any incoming data?
    RF24NetworkHeader header;
    char incomingData[32] ="";
    network.read(header, &incomingData, sizeof(incomingData)); // Read the incoming data
    Serial.println(incomingData); 
  } 
  if(Serial.avaliable()>0){
    int resetCheck = Serial.parseInt();
    if(resetCheck = 11){
      char char_array[32] = "Reset"
      RF24NetworkHeader header2(beer_node_1);     
      bool ok = network.write(header2, &char_array, sizeof(char_array,));
    }else if (resetCheck == 22){
      char char_array[32] = "Reset"
      RF24NetworkHeader header3(beer_node_2);     
      bool ok = network.write(header2, &char_array, sizeof(char_array,));
    }
  }
}
