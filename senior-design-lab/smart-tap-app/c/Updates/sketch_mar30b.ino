#include <RF24Network.h>
#include <RF24Network_config.h>
#include <Sync.h>

// Load in the libraries
#include <SPI.h>
#include <RF24.h>

// Set the CE & CSN pins
#define CE_PIN   7
#define CSN_PIN 8
#define Sensor 5

RF24 radio(CE_PIN, CSN_PIN); 
RF24Network network(radio);
// This is the address used to send/receive
const uint16_t beer_node_1 = 02;
const uint16_t hub_node = 0;

int numberCount = 0;
int prevNumber = 0;
int savedCount = 0;
// Create a Radio


void setup() {
  
  // Start up the Serial connection
  while (!Serial);
  Serial.begin(9600);
   
  // Start the Radio!
  radio.begin();
  network.begin(90,beer_node_1);
  radio.setDataRate(RF24_2MBPS);
}

void loop() {
    network.update();
    while(network.available()){
      RF24NetworkHeader header;
      char incomingData[32] ="";
      network.read(header, &incomingData, sizeof(incomingData)); // Read the incoming data
      if(strcmp(incomingData,"Reset"){
        savedCount =0;     
      }
    }
    int number = digitalRead(Sensor);
    if(number == 0 && prevNumber == 1){
      numberCount++;
      savedCount++;
     }
    if(number == 1 && prevNumber == 0){
      numberCount++;
      savedCount++;
    }
    
    if(numberCount==100){
      String count = String(savedCount);
      String str = "Keg 1 ";
      str.concat(savedCount);
      int str_len = str.length() + 1;
      char char_array[str_len];
      str.toCharArray(char_array,str.length()+1);
      RF24NetworkHeader header2(hub_node);
      bool ok = network.write(header2,&char_array, sizeof(char_array));
      //radio.write(&char_array, sizeof(char_array));
      Serial.println(savedCount);
      numberCount = 0;
    }
    prevNumber = number; 
}
