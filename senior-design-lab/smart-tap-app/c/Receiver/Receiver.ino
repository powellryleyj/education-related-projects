//#include <SoftwareSerial.h>

#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

// define the pins
#define CE_PIN   7
#define CSN_PIN 8

//SoftwareSerial BTserial(2, 3);
// Create a Radio
RF24 radio(CE_PIN, CSN_PIN); 

// The tx/rx address
const byte rxAddr[6] = "00001";

int ledPin = 13;
void setup() {
  // Start the serial
  Serial.begin(9600);  //  baud rate 9600 for the serial Bluetooth communication
 // while(!Serial);
 // Serial.println("NRF24L01P Receiver Starting...");
  
  // Start the radio, again set to min & slow as I'm guessing while testing theire really close to each other
  radio.begin();
  
  //radio.setDataRate( RF24_250KBPS ); // RF24_250KBPS, RF24_1MBPS, RF24_2MBPS
  
  // Set the reading pipe and start listening
  radio.openReadingPipe(0, rxAddr);
  radio.setPALevel(RF24_PA_MIN);   // RF24_PA_MIN ,RF24_PA_LOW, RF24_PA_HIGH, RF24_PA_MAX
  radio.startListening();
}
void loop() {
  int count = receiver();
  if(count>0){
    bluetooth(count);
  }
}

void bluetooth(int count){
  Serial.println(String(count));
}

int receiver(){
  if (radio.available()) {
   char text[32] = "";
    radio.read(&text, sizeof(text));
    int data = atoi(text);   
    return data;
  }
  return -1;
}
