require_relative '../test_helper'

class ProblemTest < ActiveSupport::TestCase
  # start validation tests
  test 'should not save without description, assignment_type, solution_file, started_at, due_at, or closed_at' do
    p = Problem.new
    assert_not p.save

    p.description = 'This is the description'
    assert_not p.save

    p.assignment_type = Problem::CLASS_DIAGRAM_TYPE
    assert_not p.save

    p.solution_file = 'solution_file.xmi'
    assert_not p.save

    p.started_at = Time.now
    assert_not p.save

    p.due_at = (Time.now + Problem::DAY)
    assert_not p.save

    p.closed_at = (Time.now + (2 * Problem::DAY))
    assert p.save
  end

  test 'problem name should be unique' do
    p = Problem.new(description: 'This is the description',
                    assignment_type: Problem::CLASS_DIAGRAM_TYPE,
                    solution_file: 'solution_file.xmi',
                    started_at: Time.now,
                    due_at: (Time.now + Problem::DAY),
                    closed_at: (Time.now + (2 * Problem::DAY)))

    # The following line is due to the fact that two sample problems are loaded into the
    # database via seeds.rb
    p.name = 'Book Reader'
    assert_not p.save

    # (2/16/19) powellr - Removed this assertion since we no longer check that a problem name has a maximum of 20 characters
    # p.name = 'This is an absurdly long problem name that can not be saved because it is so long'
    # assert_not p.save

    p.name = 'Unique Name'
    assert p.save
  end

  test 'problems should only be of the defined diagram types' do
    # Defined values are 0, 1, and 2
    p = Problem.new(description: 'This is the description',
                    solution_file: 'solution_file.xmi',
                    started_at: Time.now,
                    due_at: (Time.now + Problem::DAY),
                    closed_at: (Time.now + (2 * Problem::DAY)))
    p.assignment_type = -1
    assert_not p.save

    p.assignment_type = 3
    assert_not p.save

    p.assignment_type = Problem::CLASS_DIAGRAM_TYPE
    assert p.save

    p.assignment_type = Problem::SEQUENCE_DIAGRAM_TYPE
    assert p.save

    p.assignment_type = Problem::HIGH_LEVEL_DIAGRAM_TYPE
    assert p.save
  end

  test 'solution files should be of format .xmi or .mdl' do
    p = Problem.new(description: 'This is the description',
                    started_at: Time.now,
                    due_at: (Time.now + Problem::DAY),
                    closed_at: (Time.now + (2 * Problem::DAY)))

    accepted_formats = %w[.xmi .XMI .mdl .MDL]

    p.assignment_type = Problem::CLASS_DIAGRAM_TYPE
    p.solution_file = 'file.eap'
    assert_not p.save

    accepted_formats.each do |format|
      p.solution_file = 'file' + format
      assert p.save
    end

    p.assignment_type = Problem::SEQUENCE_DIAGRAM_TYPE
    p.solution_file = 'file.eap'
    assert_not p.save

    accepted_formats.each do |format|
      p.solution_file = 'file' + format
      assert p.save
    end

    p.assignment_type = Problem::HIGH_LEVEL_DIAGRAM_TYPE
    p.solution_file = 'file.eap'
    assert_not p.save

    accepted_formats.each do |format|
      p.solution_file = 'file' + format
      assert p.save
    end
  end

  test 'problems should be due after the start time and closed at or after the due date' do
    p = Problem.new(description: 'This is the description',
                    assignment_type: Problem::CLASS_DIAGRAM_TYPE,
                    solution_file: 'solution_file.xmi')

    time_zero = Time.now
    time_one = (Time.now + Problem::DAY)
    time_two = (Time.now + (2 * Problem::DAY))
    time_three = (Time.now + (3 * Problem::DAY))

    # check that an expected order of dates works
    p.started_at = time_one
    p.due_at = time_two
    p.closed_at = time_three
    assert p.save

    # check that due date CANNOT be before start date
    p.due_at = time_zero
    assert_not p.save

    # check that due date CAN be the same day as start date
    p.due_at = time_one
    assert p.save

    # check that closed date CANNOT be before due date
    p.due_at = time_two
    p.closed_at = time_one
    assert_not p.save

    # check that closed date CAN be the same day as due date
    p.closed_at = time_two
    assert p.save

    # check that closed date CANNOT be before start date
    p.closed_at = time_zero
    assert_not p.save

    # set closed date back to original expectation to re-check
    p.closed_at = time_three
    assert p.save
  end
  # end validation tests

  test 'open? should return true iff the current time is between start and closed time, inclusive' do
    p_open = Problem.new(name: 'P_OPEN',
                         description: 'Problem is open.',
                         assignment_type: Problem::CLASS_DIAGRAM_TYPE,
                         solution_file: 'file.xmi',
                         started_at: (Time.now - Problem::DAY),
                         due_at: (Time.now + Problem::DAY),
                         closed_at: Time.new(2100, 1, 1))

    p_close = Problem.new(name: 'P_CLOSE',
                          description: 'Problem is closed.',
                          assignment_type: Problem::CLASS_DIAGRAM_TYPE,
                          solution_file: 'file.xmi',
                          started_at: (Time.now - (2 * Problem::DAY)),
                          due_at: (Time.now - Problem::DAY),
                          closed_at: (Time.now - Problem::DAY))

    current_time = Time.now

    assert p_open.open?(current_time)

    assert_not p_close.open?(current_time)
  end

  test 'closed? should return true if open? returns false' do
    p_open = Problem.new(name: 'P_OPEN',
                         description: 'Problem is open.',
                         assignment_type: Problem::CLASS_DIAGRAM_TYPE,
                         solution_file: 'file.xmi',
                         started_at: (Time.now - Problem::DAY),
                         due_at: (Time.now + Problem::DAY),
                         closed_at: Time.new(2100, 1, 1))

    p_close = Problem.new(name: 'P_CLOSE',
                          description: 'Problem is closed.',
                          assignment_type: Problem::CLASS_DIAGRAM_TYPE,
                          solution_file: 'file.xmi',
                          started_at: (Time.now - (2 * Problem::DAY)),
                          due_at: (Time.now - Problem::DAY),
                          closed_at: (Time.now - Problem::DAY))

    current_time = Time.now

    assert_not p_open.closed?(current_time)

    assert p_close.closed?(current_time)
  end

  test 'all_open should return all problems with a closed date on or after the current date' do
    p_open = Problem.create(name: 'P_OPEN',
                            description: 'Problem is open.',
                            assignment_type: Problem::CLASS_DIAGRAM_TYPE,
                            solution_file: 'file.xmi',
                            started_at: (Time.now - Problem::DAY),
                            due_at: (Time.now + Problem::DAY),
                            closed_at: Time.new(2100, 1, 1))

    p_close = Problem.create(name: 'P_CLOSE',
                             description: 'Problem is closed.',
                             assignment_type: Problem::CLASS_DIAGRAM_TYPE,
                             solution_file: 'file.xmi',
                             started_at: (Time.now - (2 * Problem::DAY)),
                             due_at: (Time.now - Problem::DAY),
                             closed_at: (Time.now - Problem::DAY))

    current_time = Time.now

    open_problems = Problem.all_open(current_time)

    assert_includes open_problems, p_open

    assert_not_includes open_problems, p_close
  end

  test 'this_type should return all problems of a particular assignment_type' do
    snow_plow = Problem.where(name: 'Snow Plow Routes').take
    book_reader = Problem.where(name: 'Book Reader').take

    p_sequence = Problem.create(name: 'P_SEQUENCE',
                                description: 'Sequence Diagram',
                                assignment_type: Problem::SEQUENCE_DIAGRAM_TYPE,
                                solution_file: 'solution_file.xmi',
                                started_at: Time.now,
                                due_at: (Time.now + Problem::DAY),
                                closed_at: (Time.now + (2 * Problem::DAY)))

    p_high_level = Problem.create(name: 'P_HIGH_LEVEL',
                                  description: 'High Level Diagram',
                                  assignment_type: Problem::HIGH_LEVEL_DIAGRAM_TYPE,
                                  solution_file: 'solution_file.xmi',
                                  started_at: Time.now,
                                  due_at: (Time.now + Problem::DAY),
                                  closed_at: (Time.now + (2 * Problem::DAY)))

    class_diagrams = Problem.this_type(Problem::CLASS_DIAGRAM_TYPE)
    assert_includes class_diagrams, snow_plow
    assert_includes class_diagrams, book_reader
    assert_not_includes class_diagrams, p_sequence
    assert_not_includes class_diagrams, p_high_level

    sequence_diagrams = Problem.this_type(Problem::SEQUENCE_DIAGRAM_TYPE)
    assert_not_includes sequence_diagrams, snow_plow
    assert_not_includes sequence_diagrams, book_reader
    assert_includes sequence_diagrams, p_sequence
    assert_not_includes sequence_diagrams, p_high_level

    high_level_diagrams = Problem.this_type(Problem::HIGH_LEVEL_DIAGRAM_TYPE)
    assert_not_includes high_level_diagrams, snow_plow
    assert_not_includes high_level_diagrams, book_reader
    assert_not_includes high_level_diagrams, p_sequence
    assert_includes high_level_diagrams, p_high_level

    # The following checks that an exception is raised on an undefined assignment_type
    assert_raise ArgumentError do
      Problem.this_type(10)
    end
  end

  test 'all_for_instructor should return all problems created by a particular instructor' do
    hasker = accounts(:hasker)
    generic_instructor = accounts(:generic_instructor)

    snow_plow = Problem.where(name: 'Snow Plow Routes').take
    book_reader = Problem.where(name: 'Book Reader').take

    p_generic = Problem.create(name: 'test',
                               description: 'This is for the generic_instructor account.',
                               assignment_type: Problem::SEQUENCE_DIAGRAM_TYPE,
                               solution_file: 'solution_file.xmi',
                               started_at: Time.now,
                               due_at: (Time.now + Problem::DAY),
                               closed_at: (Time.now + (2 * Problem::DAY)),
                               instructor_id: generic_instructor.id)

    hasker_problems = Problem.all_for_instructor(hasker.id)
    assert_includes hasker_problems, snow_plow
    assert_includes hasker_problems, book_reader
    assert_not_includes hasker_problems, p_generic

    generic_instructor_problems = Problem.all_for_instructor(generic_instructor.id)
    assert_not_includes generic_instructor_problems, snow_plow
    assert_not_includes generic_instructor_problems, book_reader
    assert_includes generic_instructor_problems, p_generic

    # the following should be empty for two reasons
    # 1. there should be no problems associated with the account id
    # 2. incidentally is a student account so it should not have any problems created
    # since the function does not check for the account_type is passed only an integer
    should_be_empty = Problem.all_for_instructor(accounts(:generic_student).id)
    assert_empty should_be_empty
  end

  test 'path should return the directory pathway to the solution file' do
    snow_plow = Problem.where(name: 'Snow Plow Routes').take
    expected_path = 'soln/test/hasker@msoe.edu/snow-plow-soln.mdl'

    # check the expected based on the seeds.rb default
    assert_equal expected_path, snow_plow.path

    # temporarily store the current solution file and change the solution file
    temp = snow_plow.solution_file
    snow_plow.solution_file = 'file.xmi'
    snow_plow.save

    # check the expected no longer matches
    assert_not_equal expected_path, snow_plow.path

    # put the correct solution file back
    snow_plow.solution_file = temp
    snow_plow.save

    # check that the correct solution file is back in place
    assert_equal expected_path, snow_plow.path
  end

  test 'display_name should return the problem name with assignment_type appended to it' do
    expected_class = 'Snow Plow Routes (Class Diagram)' # using the default Snow Plow problem
    expected_sequence = 'P_SEQUENCE (Sequence Diagram)'
    expected_high_level = 'P_HIGH_LEVEL (High-Level Diagram)'

    snow_plow = Problem.where(name: 'Snow Plow Routes').take

    p_sequence = Problem.create(name: 'P_SEQUENCE',
                                description: 'Sequence Diagram',
                                assignment_type: Problem::SEQUENCE_DIAGRAM_TYPE,
                                solution_file: 'solution_file.xmi',
                                started_at: Time.now,
                                due_at: (Time.now + Problem::DAY),
                                closed_at: (Time.now + (2 * Problem::DAY)))

    p_high_level = Problem.create(name: 'P_HIGH_LEVEL',
                                  description: 'High Level Diagram',
                                  assignment_type: Problem::HIGH_LEVEL_DIAGRAM_TYPE,
                                  solution_file: 'solution_file.xmi',
                                  started_at: Time.now,
                                  due_at: (Time.now + Problem::DAY),
                                  closed_at: (Time.now + (2 * Problem::DAY)))

    assert_equal expected_class, snow_plow.display_name

    assert_equal expected_sequence, p_sequence.display_name

    assert_equal expected_high_level, p_high_level.display_name
  end

  test 'filename should return a new name for associated solution_file' do
    snow_plow = Problem.where(name: 'Snow Plow Routes').take

    expected_filename = 'new_name.mdl'

    assert_equal expected_filename, Problem.filename(snow_plow.solution_file, 'new name')

    expected_filename = 'ANOTHER_name.mdl'

    assert_equal expected_filename, Problem.filename(snow_plow.solution_file, 'ANOTHER name')
  end

  # TODO: create_from_form_data testing
  # TODO: save_form_data testing
  # TODO: no_error_submissions testing
  # TODO: no_error_tickets testing

  # (1/27/19) powellr - the following have been preserved for reference
  # test 'should not save without a description' do
  #   problem = Problem.new
  #   problem.solution_file = 'zzz.mdl'
  #   problem.started_at = '20140101'
  #   problem.due_at = '20140101'
  #   problem.closed_at = '20140101'
  #   problem.assignment_type = Problem::CLASS_DIAGRAM_TYPE
  #   assert_not problem.save, 'Saved problem without a description'
  # end
  #
  # test 'should not save without a started at date' do
  #   problem = Problem.new
  #   problem.description = 'asdf'
  #   problem.solution_file = 'zzz.mdl'
  #   problem.due_at = '20140101'
  #   problem.closed_at = '20140101'
  #   problem.assignment_type = Problem::CLASS_DIAGRAM_TYPE
  #   assert_not problem.save, 'Saved problem without a start date'
  # end
  #
  # test 'should not save without a close date' do
  #   problem = Problem.new
  #   problem.description = 'asdf'
  #   problem.solution_file = 'zzz.mdl'
  #   problem.started_at = '20140101'
  #   problem.due_at = '20140101'
  #   problem.assignment_type = Problem::CLASS_DIAGRAM_TYPE
  #   assert_not problem.save, 'Saved problem without a close date'
  # end
  #
  # test 'should not save when name is longer than 20 characters' do
  #   problem = Problem.new
  #   problem.name = '12345678912345678912345'
  #   problem.description = 'asdf'
  #   problem.solution_file = 'zzz.mdl'
  #   problem.started_at = '20140101'
  #   problem.closed_at = '20140101'
  #   problem.due_at = '20140101'
  #   problem.assignment_type = Problem::CLASS_DIAGRAM_TYPE
  #   assert_not problem.save, 'Saved problem with name longer than 20 characters'
  # end
  #
  # test 'should not save when name is not unique' do
  #   problem = Problem.new
  #   problem.name = 'First Problem'
  #   problem.description = 'asdf'
  #   problem.solution_file = 'zzz.mdl'
  #   problem.started_at = '20140101'
  #   problem.closed_at = '20140101'
  #   problem.due_at = '20140101'
  #   problem.assignment_type = Problem::CLASS_DIAGRAM_TYPE
  #   assert_not problem.save, 'Saved problem with duplicate name. "First Problem" used in fixture'
  # end
  #
  # test 'should not save without a due date' do
  #   problem = Problem.new
  #   problem.description = 'asdf'
  #   problem.solution_file = 'zzz.mdl'
  #   problem.started_at = '20140101'
  #   problem.closed_at = '20140101'
  #   problem.assignment_type = Problem::CLASS_DIAGRAM_TYPE
  #   assert_not problem.save, 'Saved problem without a due date'
  # end
  #
  # test 'should not save without a solution file' do
  #   problem = Problem.new
  #   problem.started_at = '20140101'
  #   problem.due_at = '20140101'
  #   problem.closed_at = '20140101'
  #   problem.description = 'description'
  #   problem.assignment_type = Problem::CLASS_DIAGRAM_TYPE
  #   assert_not problem.save, 'Saved problem without a solution file'
  # end
  #
  # test 'should not save when solution file ends with bad extension' do
  #   problem = Problem.new
  #   problem.solution_file = 'something.something'
  #   problem.started_at = '20140101'
  #   problem.due_at = '20140101'
  #   problem.closed_at = '20140101'
  #   problem.description = 'description'
  #   problem.assignment_type = Problem::CLASS_DIAGRAM_TYPE
  #   assert_not problem.save, 'Saved problem without mdl or xmi extension'
  # end
  #
  # test 'should save when all fields are correct with an mdl file' do
  #   problem = Problem.new
  #   problem.solution_file = 'something.mdl'
  #   problem.started_at = '20140101'
  #   problem.due_at = '20140101'
  #   problem.closed_at = '20140101'
  #   problem.description = 'description'
  #   problem.assignment_type = Problem::CLASS_DIAGRAM_TYPE
  #   assert problem.save
  # end
  #
  # test 'should save when all fields are correct with an xmi file' do
  #   problem = Problem.new
  #   problem.solution_file = 'something.xmi'
  #   problem.started_at = '20140101'
  #   problem.due_at = '20140101'
  #   problem.closed_at = '20140101'
  #   problem.description = 'description'
  #   problem.assignment_type = Problem::CLASS_DIAGRAM_TYPE
  #   assert problem.save
  # end
  #
  # test 'should not save when due date is before start date' do
  #   problem = Problem.new
  #   problem.solution_file = 'something.xmi'
  #   problem.started_at = '20140101'
  #   problem.due_at = '20130101'
  #   problem.closed_at = '20140101'
  #   problem.description = 'description'
  #   problem.assignment_type = Problem::CLASS_DIAGRAM_TYPE
  #   assert_not problem.save
  # end
  #
  # test 'should not save when close date is before start date' do
  #   problem = Problem.new
  #   problem.solution_file = 'something.xmi'
  #   problem.started_at = '20140101'
  #   problem.due_at = '20140101'
  #   problem.closed_at = '20130101'
  #   problem.description = 'description'
  #   problem.assignment_type = Problem::CLASS_DIAGRAM_TYPE
  #   assert_not problem.save
  # end
  #
  # test 'should not save when diagram type is not specified' do
  #   problem = Problem.new
  #   problem.solution_file = 'something.xmi'
  #   problem.started_at = '20140101'
  #   problem.due_at = '20140101'
  #   problem.closed_at = '20140101'
  #   problem.description = 'description'
  #   assert_not problem.valid?
  # end
  #
  # test 'should not save when diagram type is not valid' do
  #   problem = Problem.new
  #   problem.solution_file = 'something.xmi'
  #   problem.started_at = '20140101'
  #   problem.due_at = '20140101'
  #   problem.closed_at = '20140101'
  #   problem.description = 'description'
  #   problem.assignment_type = -1
  #   assert_not problem.valid?
  # end
end
