require_relative '../test_helper'

class AccountTest < ActiveSupport::TestCase

  test 'best name returns both given and sur name' do
    assert_equal 'Ryley Powell', accounts(:ryley_powell).best_name
  end

  test 'best name returns only given name' do
    ryley = Account.new given_name: 'Ryley'
    assert_equal 'Ryley', ryley.best_name
  end

  test 'best name returns only sur name' do
    powell = Account.new surname: 'Powell'
    assert_equal 'Powell', powell.best_name
  end

  # (1/19/19) powellr - it seems as if accounts can not be created without given name or surname
  # test 'best name returns email if missing both given and sur name' do
  #   powellr = Account.new email_address: 'powellr@msoe.edu'
  #   assert_equal 'powellr@msoe.edu', powellr.best_name
  # end
  #
  # test 'best name returns unknown if missing given name, surname, and email' do
  #   unknown = Account.new
  #   assert_equal 'unknown', unknown.best_name
  # end

  test 'accounts should be able to change passwords' do
    accounts(:ryley_powell).change_password 'new_pass'
    assert_equal 'new_pass', accounts(:ryley_powell).password_digest
    assert accounts(:ryley_powell).authenticate 'new_pass'
  end

  test 'generic_instructor should have account_type INSTRUCTOR' do
    assert accounts(:generic_instructor).instructor?
    assert_not accounts(:generic_instructor).student?
  end

  test 'generic_student should have account_type STUDENT' do
    assert accounts(:generic_student).student?
    assert_not accounts(:generic_student).instructor?
  end

  test 'an account should of class instructor or student' do
    assert_instance_of Instructor, accounts(:generic_instructor).child
    assert_instance_of Student, accounts(:generic_student).child
  end
end
