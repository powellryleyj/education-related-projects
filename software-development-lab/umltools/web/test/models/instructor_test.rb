require_relative '../test_helper'

class InstructorTest < ActiveSupport::TestCase
  test 'parent should return the instructor as an instance of account' do
    instructor = Instructor.new(given_name: 'Test_Instructor',
                                   surname: 'Tester')
    assert_instance_of Instructor, instructor
    assert_instance_of Account, instructor.parent
  end
end
