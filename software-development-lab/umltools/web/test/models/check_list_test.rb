require_relative '../test_helper'

class CheckListTest < ActiveSupport::TestCase
  # TODO: The following two tests are failing and need attention
  # test 'should return all non-retired lists in an array sorted by description' do
  #   lists = CheckList.check_types
  #   assert_equal(3, lists.size)
  #   assert_equal('Apply all checks', lists[0][0])
  #   assert_equal('Instructor 1 checklist', lists[1][0])
  #   assert_equal('This is a list for instructor 2', lists[2][0])
  # end

  # test 'should return all lists in an array sorted by description' do
  #   lists = CheckList.all_check_types
  #   assert_equal(3, lists.size)
  #   assert_equal('Instructor 1 checklist', lists[0][0])
  #   assert_equal('This is a list for instructor 2', lists[1][0])
  #   assert_equal('This is a retired list', lists[2][0])
  # end

  # test 'should return the organization if both the instructor and org are not nil' do
  #   checklist = CheckList.find(1)
  #   organization = checklist.organization
  #   assert_equal('MSOE', organization)
  # end

  # test 'should return "unknown" for the organization if the instructor is nil' do
  #   checklist = CheckList.find(2)
  #   org = checklist.organization
  #   assert_equal('unknown', org)
  # end

  # test 'should return "unknown" for the organization if the instructor is not nil but the org is nil' do
  #   checklist = CheckList.find(3)
  #   org = checklist.organization
  #   instructor = checklist.account
  #   assert_not_nil(instructor)
  #   assert_equal('unknown', org)
  # end

  # test 'should return "unknown" for the contact if the instructor is nil' do
  #   checklist = CheckList.find(2)
  #   contact = checklist.contact
  #   assert_equal('unknown', contact)
  # end

  # test 'should return the contact name for the contact if the instructor is not nil' do
  #   checklist = CheckList.find(1)
  #   contact = checklist.contact
  #   assert_equal('robert hasker', contact)
  # end

  # test 'should return all available check names' do
  #   checks = CheckList.all_check_types
  #   assert(!checks.empty?)
  # end
end
