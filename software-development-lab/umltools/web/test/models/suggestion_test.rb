require_relative '../test_helper'

# (12/9/18) powellr - This test has been renamed to SuggestionTest to match
# the renaming of Warning -> Suggestion. More detail can be found in
# suggestion.rb
#
# rwh, March 2016: this used to assume the separator was '.',
#      but it is currently ';'
#

class SuggestionTest < ActiveSupport::TestCase
  test 'should setup suggestions based on ";" separated string' do
    input = 'MISSING_HIGH_LEVEL;Missing noun/verb;This is a test message.;7'
    result = Suggestion.setup(input)
    assert_equal('MISSING_HIGH_LEVEL', result.explanation.name)
    assert_equal('This is a test message.', result.message)
    assert_equal('MISSING_HIGH_LEVEL', result.category)
    assert_equal('Missing noun/verb', result.detailed_issue)
    assert_equal('7', result.element_id)
  end

  test 'should perform comparisons based on precedence' do
    higher = Suggestion.setup('MESSAGE_TYPES;a;This has a precedence of 3')
    lower = Suggestion.setup('GUARD_CONDITIONS;b;This has precedence of 4')
    assert_equal(-1, lower <=> higher)
    assert_equal(1, higher <=> lower)
  end

  test 'should perform equality comparisons based on name' do
    firstEqual = Suggestion.setup('INDEX;This has a precedence of 3;a')
    secondEqual = Suggestion.setup('MESSAGE_TYPES;This has a precedence of 3;b')
    assert_equal(-1, firstEqual <=> secondEqual)
  end

  test 'should map strings to suggestions if the input is not empty' do
    input = ['MESSAGE_TYPES;a;This has a precedence of 3', 'GUARD_CONDITIONS;b;This has precedence of 4', 'MISSING_HIGH_LEVEL;Missing noun/verb;This is a test message.']
    result = Suggestion.setup_suggestions(input)
    assert_equal(Suggestion.setup('MESSAGE_TYPES;a;This has a precedence of 3'), result[0])
    assert_equal(Suggestion.setup('GUARD_CONDITIONS;b;This has precedence of 4'), result[1])
    assert_equal(Suggestion.setup('MISSING_HIGH_LEVEL;Missing noun/verb;This is a test message.'), result[2])
  end

  test 'should be able to look up suggestions by message' do
    suggestion = Suggestion.setup('MESSAGE_TYPES;a;This has a precedence of 3')
    assert_equal(Suggestion.lookup_message('This has a precedence of 3'), suggestion)
  end

  # (1/28/19) powellr - The following has been preserved for reference
  # test 'should setup suggestion based on ";" separated string' do
  #   input = 'Explanation1;Subname;Message'
  #   result = Suggestion.setup(input)
  #   assert_equal('Explanation1', result.explanation.name)
  #   assert_equal('Message', result.message)
  #   assert_equal('Explanation1', result.category)
  #   assert_equal('Subname', result.detailed_issue)
  # end
  #
  # test 'should perform less than comparisons based on precedence' do
  #   lower = Suggestion.setup('Explanation2;a;a')
  #   higher = Suggestion.setup('Explanation1;b;b')
  #   assert_equal(-1, lower <=> higher)
  # end
  #
  # test 'should perform greater than comparisons based on precedence' do
  #   lower = Suggestion.setup('Explanation2;a;a')
  #   higher = Suggestion.setup('Explanation1;b;b')
  #   assert_equal(1, higher <=> lower)
  # end
  #
  # test 'should perform equality comparisons based on name' do
  #   firstEqual = Suggestion.setup('Explanation4;a;a')
  #   secondEqual = Suggestion.setup('Explanation1;b;b')
  #   assert_equal(-1, firstEqual <=> secondEqual)
  # end
  #
  # test 'should map strings to suggestions if the input is not empty' do
  #   input = 'Explanation1;Subname;Message'
  #   result = Suggestion.setup_suggestions([input])
  #   assert_equal(Suggestion.setup(input), result[0])
  # end
  #
end
