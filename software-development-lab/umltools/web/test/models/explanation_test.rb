require_relative '../test_helper'

class ExplanationTest < ActiveSupport::TestCase
  test 'detailed_issue_list should return only related issues' do
    # Using 'BAD_MODEL', expecting 'petal_error', 'pdiff_solution_error', 'submission_petal_error'
    # setup
    expected_issues = %w[petal_error pdiff_solution_error submission_petal_error]
    unrelated_issues = %w[reference_to_missing_items role_name_at_source]
    # end setup

    result = Explanation.where(name: 'BAD_MODEL').take.detailed_issue_list

    expected_issues.each do |issue|
      assert_includes result, issue
    end

    unrelated_issues.each do |issue|
      assert_not_includes result, issue
    end
  end

  test 'umlint_explanations should not include any explanations that have a diff or sdiff diagram type' do
    umlint_explanations = Explanation.umlint_explanations
    excluded_explanations = Explanation.where(diagram: %w[diff sdiff])

    excluded_explanations.each do |expl|
      assert_not_includes umlint_explanations, expl
    end
  end

  test 'selectable_umlint_explanations should exclude special explanations' do
    selectable_umlint_explanations = Explanation.selectable_umlint_explanations
    Explanation::SPECIAL_EXPLANATIONS.each do |issue|
      assert_not_includes selectable_umlint_explanations, issue
    end
  end

  test 'selectable_umlint_sequence_explanations should include only explanations associated with all or sequence diagram types' do
    selectable_umlint_sequence_explanations = Explanation.selectable_umlint_sequence_explanations
    excluded_explanations = Explanation.where.not(diagram: %w[all sequence])

    excluded_explanations.each do |expl|
      assert_not_includes selectable_umlint_sequence_explanations, expl
    end
  end

  test 'umlgrader_explanations should contain only explanations of type diff' do
    umlgrader_explanations = Explanation.umlgrader_explanations
    excluded_explanations = Explanation.where.not(diagram: 'diff')

    excluded_explanations.each do |_expl|
      assert_not_includes umlgrader_explanations, excluded_explanations
    end
  end

  test 'umlgrader_sequence_explanations should contain only explanations of type sdiff' do
    umlgrader_sequence_diagrams = Explanation.umlgrader_sequence_explanations
    excluded_explanations = Explanation.where.not(diagram: 'sdiff')

    excluded_explanations.each do |expl|
      assert_not_includes umlgrader_sequence_diagrams, expl
    end
  end

  test 'selectable_umlgrader_explanations should contain only diff and sdiff explanations' do
    selectable_umlgrader_explanations = Explanation.selectable_umlgrader_explanations
    excluded_explanations = Explanation.where.not(diagram: %w[diff sdiff])

    excluded_explanations.each do |expl|
      assert_not_includes selectable_umlgrader_explanations, expl
    end
  end

  test 'sort should return a new array with precedence order from high-to-low' do
    # setup arrays using the first three explanations
    expected_order = [Explanation.where(name: 'BAD_MODEL').take,
                      Explanation.where(name: 'UNKNOWN').take,
                      Explanation.where(name: 'BAD_ASSOC_ROLE').take]

    shuffled_order = Explanation.first(3).to_a

    # sort 'shuffled_order' and store as 'sorted'
    sorted = Explanation.sort(shuffled_order)

    # compare the equality of the explanations of 'expected_order' and 'sorted' and collect within 'result' array
    result = expected_order.zip(sorted).collect do |expected, actual|
      expected.name == actual.name
    end

    # assert each element of 'result' is true
    result.each do |r|
      assert r
    end
  end

  test 'sort! should modify the array in place with precedence order from high-to-low' do
    # setup arrays using the first three explanations
    expected_order = [Explanation.where(name: 'BAD_MODEL').take,
                      Explanation.where(name: 'UNKNOWN').take,
                      Explanation.where(name: 'BAD_ASSOC_ROLE').take]

    shuffled_order = Explanation.first(3).to_a

    # sort 'shuffled_order' in place
    Explanation.sort!(shuffled_order)

    # compare the equality of the explanations of 'expected_order' and 'shuffled_order'
    result = expected_order.zip(shuffled_order).collect do |expected, actual|
      expected.name == actual.name
    end

    # assert each element of 'result' is true
    result.each do |r|
      assert r
    end
  end

  test 'reverse_sort should return an array with precedence order from low-to-high' do
    # setup arrays using the first three explanations
    expected_order = [Explanation.where(name: 'BAD_ASSOC_ROLE').take,
                      Explanation.where(name: 'BAD_MODEL').take,
                      Explanation.where(name: 'UNKNOWN').take]

    shuffled_order = Explanation.first(3).to_a

    # reverse_sort 'shuffled_order' and store as 'sorted'
    sorted = Explanation.reverse_sort(shuffled_order)

    # compare the equality of the explanations of 'expected_order' and 'sorted' and collect within 'result' array
    result = expected_order.zip(sorted).collect do |expected, actual|
      expected.name == actual.name
    end

    # assert each element of 'result' is true
    result.each do |r|
      assert r
    end
  end

  test 'reverse_sort! should modify the array in place with precedence order from low-to-high' do
    # setup arrays using the first three explanations
    expected_order = [Explanation.where(name: 'BAD_ASSOC_ROLE').take,
                      Explanation.where(name: 'BAD_MODEL').take,
                      Explanation.where(name: 'UNKNOWN').take]

    shuffled_order = Explanation.first(3).to_a

    # sort 'shuffled_order' in place
    Explanation.reverse_sort!(shuffled_order)

    # compare the equality of the explanations of 'expected_order' and 'shuffled_order'
    result = expected_order.zip(shuffled_order).collect do |expected, actual|
      expected.name == actual.name
    end

    # assert each element of 'result' is true
    result.each do |r|
      assert r
    end
  end

  # (1/19/19) powellr - everything below has been preserved for reference
  # test 'should return all explanations' do
  #   explanations = Explanation.all_explanations
  #   assert(explanations.size > 0)
  # end
  #
  # test 'should return all detailed issues' do
  #   explanation = Explanation.new
  #   explanation.detailed_issues = 'a,b,c'
  #   detailed_issues_list = explanation.detailed_issue_list
  #   assert(detailed_issues_list.include?('a'))
  #   assert(detailed_issues_list.include?('b'))
  #   assert(detailed_issues_list.include?('c'))
  # end
  #
  # test 'should return explanations not marked as diff for umlint' do
  #   explanations = Explanation.umlint_explanations
  #   assert_not_empty(explanations.select{|e| e.id == 2})
  # end
  #
  # test 'should return explanations marked as diff for umlgrader' do
  #   explanations = Explanation.umlgrader_explanations
  #   assert_not_empty(explanations.select{|e| e.id == 1})
  # end
  #
  # test 'should sort by precedence' do
  #   sorted = Explanation.sort(Explanation.all_explanations.to_a)
  #   assert(sorted[0].precedence > sorted[1].precedence)
  # end
  #
  # test 'should sort by precedence in reverse' do
  #   explanations = Explanation.reverse_sort(Explanation.all_explanations.to_a)
  #   assert(explanations[0].precedence <= explanations[1].precedence)
  #   assert(explanations[1].precedence <= explanations[2].precedence)
  # end
end
