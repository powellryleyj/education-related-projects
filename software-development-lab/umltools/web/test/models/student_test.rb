require_relative '../test_helper'

class StudentTest < ActiveSupport::TestCase
  test 'parent should return the student as an instance of account' do
    student = Student.new(given_name: 'Test_Student',
                          surname: 'Tester')
    assert_instance_of Student, student
    assert_instance_of Account, student.parent
  end
end
