require_relative '../test_helper'

class CheckItemTest < ActiveSupport::TestCase
  # (1/19/19) powellr - Note the next two tests dealing with "detailed_items" mention "explanations",
  # this is due to the fact that "explanations" are categories for CheckItems, and a CheckItem
  # is merely a data structure for "explanations"
  test 'detailed_items should return an empty array for explanations that do not exist' do
    check_item = CheckItem.new category: 'DNE', issue: 'DNE'
    detailed_items_array = check_item.detailed_items
    assert_empty detailed_items_array
  end

  test 'detailed_items should return all related issues for a particular explanation' do
    # using the "MESSAGE_MISMATCH" explanation
    # expects the following: [missing, extra, mislabeled]
    detailed_items_array = CheckItem.where(category: 'MESSAGE_MISMATCH').first.detailed_items
    assert_includes detailed_items_array, :missing
    assert_includes detailed_items_array, :extra
    assert_includes detailed_items_array, :mislabeled
    # next line checks that an unrelated issue has not been included
    assert_not_includes detailed_items_array, :reversed
  end

  test 'get should create a new CheckItem if it does not already exist' do
    check_item = CheckItem.get(Explanation.first, 'DNE')
    assert_includes CheckItem.all, check_item
  end

  test 'get should return the exact CheckItem if it already exists' do
    # using "BAD_MODEL" with sub-issue "petal_error"
    using_table_query = CheckItem.where(category: 'BAD_MODEL', issue: 'petal_error').take
    using_get = CheckItem.get(Explanation.where(name: 'BAD_MODEL').take, 'petal_error')
    assert_equal using_table_query, using_get
  end

  test 'get_list should return all CheckItems specified' do
    # using "COMPLEXATTR" with sub-issues "no_collections", "no_complex_attributes", but not "non_simple_attributes"
    # setup
    explanation = Explanation.where(name: 'COMPLEXATTR').take
    no_collections = 'no_collections'
    no_complex_attributes = 'no_complex_attributes'
    non_simple_attributes = 'non_simple_attributes'

    checks_to_include = [no_collections, no_complex_attributes]

    included_one = CheckItem.get(explanation, no_collections)
    included_two = CheckItem.get(explanation, no_complex_attributes)
    not_included = CheckItem.get(explanation, non_simple_attributes)
    # end setup

    result = CheckItem.get_list(explanation, checks_to_include)

    assert_includes result, included_one
    assert_includes result, included_two
    assert_not_includes result, not_included
  end

  test 'get_list_complement should return all CheckItems that are not listed' do
    # using "COMPLEXATTR" with sub-issues "no_collections", "no_complex_attributes", but not "non_simple_attributes"
    # setup
    explanation = Explanation.where(name: 'COMPLEXATTR').take
    no_collections = 'no_collections'
    no_complex_attributes = 'no_complex_attributes'
    non_simple_attributes = 'non_simple_attributes'

    checks_to_exclude = [non_simple_attributes]

    included_one = CheckItem.get(explanation, no_collections)
    included_two = CheckItem.get(explanation, no_complex_attributes)
    not_included = CheckItem.get(explanation, non_simple_attributes)
    # end setup

    result = CheckItem.get_list_complement(explanation, checks_to_exclude)

    assert_includes result, included_one
    assert_includes result, included_two
    assert_not_includes result, not_included
  end

  # (1/19/19) powellr - everything below has been preserved for reference
  # test 'should get the specified CheckItem object if it exists' do
  #   issue = 'OMG SOMETHING IS WRONG'
  #   explanation = Explanation.new
  #   explanation.id = 1
  #   result = CheckItem.get(explanation, issue)
  #   assert_equal('Category1', result.category)
  # end
  #
  # test 'should get a new CheckItem object if it does not exist' do
  #   issue = 'Not an existing issue'
  #   explanation = Explanation.new
  #   explanation.id = 1000000000
  #   explanation.name = 'New Explanation'
  #   result = CheckItem.get(explanation, issue)
  #   assert_equal(explanation.name, result.category)
  #   assert_equal(issue, result.issue)
  #   assert_equal(explanation.id, result.explanation_id)
  # end
  #
  # test 'should get CheckItem for all in issue list' do
  #   explanation = Explanation.new
  #   explanation.detailed_issues = 'OMG SOMETHING IS WRONG'
  #   explanation.id = 1
  #   issues = ['OMG SOMETHING IS WRONG', 'This has an issue']
  #   results = CheckItem.get_list(explanation, issues)
  #   assert_equal(1, results.size)
  #   assert_equal('OMG SOMETHING IS WRONG', results[0].issue)
  # end
  #
  # test 'should return the issues in issue list that are not found' do
  #   explanation = Explanation.new
  #   explanation.detailed_issues = 'OMG SOMETHING IS WRONG'
  #   explanation.id = 1
  #   issues = ['OMG SOMETHING IS WRONG', 'This has an issue']
  #   results = CheckItem.get_list_complement(explanation, issues)
  #   assert_equal(1, results.size)
  #   assert_equal('OMG SOMETHING IS WRONG', results[0].issue)
  # end
  #
  # test 'should return detailed issues if the explanations name matches the category' do
  #   explanation = Explanation.new
  #   explanation.detailed_issues = 'OMG SOMETHING IS WRONG'
  #   explanation.id = 1
  #   check = CheckItem.get(explanation, 'OMG SOMETHING IS WRONG')
  #   detailed_items = check.detailed_items
  #   assert_equal(1, detailed_items.size)
  #   assert_equal(:issue1, detailed_items[0])
  # end
  #
  # test 'should not return detailed issues if the explanations name does not match the category' do
  #   explanation = Explanation.new
  #   explanation.id = 1
  #   check = CheckItem.get(explanation, 'This has an issue')
  #   detailed_items = check.detailed_items
  #   assert_equal(0, detailed_items.size)
  # end
end
