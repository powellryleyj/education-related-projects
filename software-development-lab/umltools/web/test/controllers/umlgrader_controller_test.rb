require_relative '../test_helper'
require File.join(File.dirname(__FILE__), '..', '..', 'app', 'controllers', 'umlgrader_controller')

class UmlgraderControllerTest < ActionController::TestCase
  # for grader actions requiring login (from umlint) see integration test
  #    manage_assignments_test.rb

  test 'should get index' do
    get :index
    assert_response :success
    assert_not_nil assigns(:problem_options)
  end

  # testing get_results
  test 'should error with no problem selected' do
    get :results
    assert_response :redirect
    assert_equal 'Please select a problem to grade', flash[:error]
  end

  # # make sure eap files are blocked
  test 'EAP File submission blocked' do
    test_eap = File.join(File.dirname(__FILE__), '..', 'lib/resources/eap/test.eap')
    file = Rack::Test::UploadedFile.new(test_eap, 'application/octet-stream')
    open_problem = Problem.where(name: 'Snow Plow Routes').take

    post :results, params: { assignment: open_problem.id, model_file: [file] }

    assert_response :redirect
    assert_equal 'must have file extension .mdl or .xmi (Attempted to save as test.eap)', flash[:error]
  end

  test 'should error with problem closed' do
    # prepare a file to upload
    test_xmi = File.join(File.dirname(__FILE__), '..', 'lib/resources/xmi/ea/vehicle_ea2.xmi')
    file = Rack::Test::UploadedFile.new(test_xmi, 'application/xml')

    p_close = Problem.create(name: 'P_CLOSE',
                             description: 'Problem is closed.',
                             assignment_type: Problem::CLASS_DIAGRAM_TYPE,
                             solution_file: 'file.xmi',
                             started_at: (Time.now - (2 * Problem::DAY)),
                             due_at: (Time.now - Problem::DAY),
                             closed_at: (Time.now - Problem::DAY))

    post :results, params: { assignment: p_close.id, model_file: [file] }

    assert_response :redirect
    assert_match /Sorry, that problem closed at .*/, flash[:error]
  end

  test 'should error with no file selected to grade' do
    open_problem = Problem.where(name: 'Snow Plow Routes').take

    post :results, params: { assignment: open_problem.id }

    assert_response :redirect
    assert_equal 'Please select a file to grade', flash[:error]
  end

  # The below test is commented out because it creates an exception due to the file
  # that is placed inside the POST. The goal is to POST the file to the controller so that it may
  # create a Solution. This has yet to be successful, but I believe that the test is close to
  # being complete. It just needs a little more work in order to successfully POST the file.

  # test "should create solution" do
  #   test_xmi = File.join(File.dirname(__FILE__), '..', 'lib/resources/xmi/ea/vehicle_ea2.xmi')
  #   file = Rack::Test::UploadedFile.new(test_xmi, "application/xml")
  #
  #   post :restults, {
  #       assignment: problems(:open_problem).id.to_s, model_file: file
  #   }
  #
  #   assert_response :redirect
  #   assert Solution.where(:orig_name => 'vehicle_ea2').exists?
  # end

  test 'should create instructor' do
    post :create_instructor, params: { 'change_password' => 'pass',
                                       'confirm_password' => 'pass',
                                       'email' => 'test@msoe.edu',
                                       'first_name' => 'Jon',
                                       'last_name' => 'Doe',
                                       'organization' => 'MSOE' },
                             session: { account_id: accounts(:hasker).id }

    assert_response :redirect
    assert_equal "Instructor 'test@msoe.edu' has been created!", flash[:notice]
    assert Instructor.where('given_name' => 'Jon').exists?, 'Jon not found'
  end

  test 'should modify instructor' do
    post :modify_instructor, params: { 'current_password' => 'hasker123',
                                       'commit' => 'Save',
                                       'change_password' => 'instructor',
                                       'confirm_password' => 'instructor',
                                       'email' => 'i@msoe.edu',
                                       'first_name' => 'New',
                                       'last_name' => 'Instructor',
                                       'organization' => 'New Org' },
                             session: { account_id: accounts(:hasker).id }

    assert_response :redirect
    assert_equal "Instructor 'i@msoe.edu' has been modified!", flash[:notice]
    assert Instructor.where('given_name' => 'New').exists?, 'no modified instructor'
  end
end
