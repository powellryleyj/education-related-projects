#!/usr/bin/env ruby
# t1: testing UmlTools::SExpression reading

require_relative 'test_base'

class Test0SExpr <  Test::Unit::TestCase
  include TestBase

  def setup
    # nothing
  end

  def teardown
    # nothing
  end

  def test_A_tokenize
    x = UmlTools::SExpr.tokenize('(hi)')
    assert_equal(%w{( hi )}, x)
    x = UmlTools::SExpr.tokenize('((this)0.5   12.3')
    assert_equal(%w{( ( this ) 0.5 12.3}, x)
    x = UmlTools::SExpr.tokenize('"(oop" ")" s ())')
    assert_equal(%w{"(oop" ")" s ( ) )}, x)
    x = UmlTools::SExpr.tokenize('"(alone)"')
    assert_equal(%w("(alone)"), x)
    x = UmlTools::SExpr.tokenize('((out) "hi" 0.923 "(there)")')
    assert_equal(%w{( ( out ) "hi" 0.923 "(there)" )}, x)
    x = UmlTools::SExpr.tokenize('		location   	(1807, 1478)')
    assert_equal(%w{location ( 1807, 1478 )}, x)
  end

  def test_B_read
    x = UmlTools::SExpr.read('(this that)')
    assert_equal('[this, that]', x.to_s)
    x = UmlTools::SExpr.read('(this that) other')
    assert_equal('[[this, that], other]', x.to_s)
    x = UmlTools::SExpr.read('(this that) (other) who "not me" (end)')
    assert_equal('[[this, that], [other], who, not me, [end]]', x.to_s)
    x = UmlTools::SExpr.read('((out) "hi" 0.923 "(there)")')
    assert_equal('[[out], hi, 0.923, (there)]', x.to_s)
    x = UmlTools::SExpr.read('		location   	(1807, 1478)')
    assert_equal('[location, [1807,, 1478]]', x.to_s)
  end

  def test_C_read_petal_small_file
    s = <<HERE
(object Petal
    version    	50
    _written   	"Rose 2006.0.0.060314"
    charSet    	0)

(object Design "Logical View"
    is_loaded  	TRUE
    attributes 	(list Attribute_Set
	(object Attribute
	    tool       	"Java"
	    name       	"UserDefineTagName1"
	    value      	""))
   )
HERE
    x = UmlTools::SExpr.read(s)
    assert_equal('[[object, Petal, version, 50, _written, Rose 2006.0.0.060314, charSet, 0], [object, Design, Logical View, is_loaded, TRUE, attributes, [list, Attribute_Set, [object, Attribute, tool, Java, name, UserDefineTagName1, value, ]]]]',
                 x.to_s)
    assert_equal('50',
                 x.first.find_by_path(%w(version)).to_s)
    assert_equal('[list, Attribute_Set, [object, Attribute, tool, Java, name, UserDefineTagName1, value, ]]',
                 x.second.find_by_path(%w(attributes)).to_s)
  end

  def test_D_select
    x = UmlTools::SExpr.read('(this that)')
    y = x.select { |a| a.atom? && a.head[0] == ?t }
    assert(y.class == Array)
    assert_equal(%w(this that), y.map {|w| w.to_s})

    y = x.select { |a| a.atom? && a.head[2] == ?a }
    assert_equal(%w(that), y.map {|w| w.to_s})

    x = UmlTools::SExpr.read('(this that) (other) who "not me" (end)')
    y = x.select { |s| s.list? && s.items.length == 1 }
    assert_equal(%w([other] [end]), y.map {|w| w.to_s})
  end

  def test_E_bad_input
    msg = nil
    assert_nothing_thrown do
      begin
        UmlTools::SExpr.read('(power')
      rescue RuntimeError => e
        msg = e.message
      end
    end
    assert_equal('Unexpected nil atom', msg)
  end
end

