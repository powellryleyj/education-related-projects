require_relative '../test_base'

# version of DelayedModel that automatically clones its file
class ComparisonModel < DelayedModel
  def initialize(filename)
    @file = filename
  end
  def mdl
    if @file
      @mdl = UmlTools::Model::FullModel.load(File.join($comparisons_directory, @file))
      @file = nil
    end
    @mdl.deep_clone
  end
end

class TestPDiff < Test::Unit::TestCase
  include TestBase
  # NOTE:
  #   must clone these before doing diffs since the tests are destructive!
  #
  @@plow_soln = ComparisonModel.new 'plow_soln.mdl'
  @@plow_alt  = ComparisonModel.new 'plow_alt_soln.mdl'
  @@odd_soln  = ComparisonModel.new 'odd_soln.mdl'
  @@miss1     = ComparisonModel.new 'miss1.mdl'
  @@miss2     = ComparisonModel.new 'miss2.mdl'
  @@miss3     = ComparisonModel.new 'miss3.mdl'
  @@miss4     = ComparisonModel.new 'miss4.mdl'
  @@miss5     = ComparisonModel.new 'miss5.mdl'
  @@miss6     = ComparisonModel.new 'miss6.mdl'
  @@no_err    = ComparisonModel.new 'no_plow_errors.mdl'
  @@book      = ComparisonModel.new 'bookreader-soln.mdl'
  @@bad_book1 = ComparisonModel.new 'bookreader-miss1.mdl'

  def setup
    # ensure if this gets set for some test, it's cleared for the next
    $explicit_errors = false
  end

  protected

  def assert_equal_differences(expected_msg_text, actual_issues)
    expected = expected_msg_text.split("\n").sort
    msgs = actual_issues.map { |x| x.message }
    msgs.sort!
    assert_equal_sequences(expected, msgs)
  end

  public

  def test_miss_1
    soln  = @@plow_soln.mdl
    miss1 = @@miss1.mdl
    pd = UmlTools::LogicalDiff.new(soln, miss1)
    expected = <<HERE
Class Route should be associated with Segment instead of Street
Class Street has an unexpected association with Route
Class Street is missing 3 associations
Missing class: Segment
HERE
    assert_equal_differences(expected, pd.errors)
  end

  def test_miss_2
    soln  = @@plow_soln.mdl
    miss2 = @@miss2.mdl
    pd = UmlTools::LogicalDiff.new(soln, miss2)
    expected = <<HERE
Association between Segment and Street with name end appears to be reversed
Class DumpTruck has an unexpected association with Truck
Class DumpTruck should be inherited from another class
Class SnowPlow has an unexpected association with Truck
Class SnowPlow should be inherited from another class
Class Truck has unexpected associations with DumpTruck, SnowPlow
HERE
    assert_equal_differences(expected, pd.errors)
  end

  def test_miss_3
    soln  = @@plow_soln.mdl
    miss3 = @@miss3.mdl
    pd = UmlTools::LogicalDiff.new(soln, miss3)
    expected = <<HERE
Class Driver should be associated with Route instead of Path
Class Segment is missing 2 associations
Class Street should be associated with Segment instead of Path
Classes that should not be in the diagram: Dumper, Path, Snow_Truck
Error in multiplicity for role Driver in association with Truck
Error in multiplicity for role Truck in association with Driver
Incorrect role name for role Street in association with Segment
Missing attributes in class Truck
Missing classes: DumpTruck, Route, SnowPlow
Missing operations in class Truck
Unexpected attributes in class Driver
Unexpected attributes in class Street
Unexpected operations in class Truck
HERE
    assert_equal_differences(expected, pd.errors)

    expected = <<HERE
Class Driver should be associated with Route instead of Path
Class Segment is missing 2 associations
Class Street should be associated with Segment instead of Path
Classes that should not be in the diagram: Dumper, Path, Snow_Truck
Error in multiplicity for role Driver in association with Truck (none given; expected 1)
Error in multiplicity for role Truck in association with Driver (none given; expected 1)
Incorrect role name for role Street in association with Segment (no name given; expected start)
Missing attributes in class Truck: number
Missing classes: DumpTruck, Route, SnowPlow
Missing operations in class Truck: plow
Unexpected attributes in class Driver: name
Unexpected attributes in class Street: length
Unexpected operations in class Truck: push
HERE
    $explicit_errors = true
    assert_equal_differences(expected, pd.errors)
  end

  def test_miss_4
    soln  = @@plow_soln.mdl
    miss4 = @@miss4.mdl
    pd = UmlTools::LogicalDiff.new(soln, miss4)
    expected = <<HERE
Class Segment is missing 2 associations
Class Street is missing 2 associations
Error in multiplicity for role Route in association with Driver
Error in multiplicity for role Segment in association with Route
Error in multiplicity for role Segment in association with Street
Error in multiplicity for role Truck in association with Driver
Missing operations in class Truck
HERE
    assert_equal_differences(expected, pd.errors)
  end

  def test_miss_5
    soln  = @@plow_soln.mdl
    miss5 = @@miss5.mdl
    pd = UmlTools::LogicalDiff.new(soln, miss5)
    expected = <<HERE
Class Driver has the right number of associations, but should be associated with Route, Truck instead of Segment, Snow
Class DumpTruck is inherited from the wrong class
Class Route is missing an association
Class Segment should be associated with Street instead of Driver
Class Snow should not be in the diagram
Class SnowPlow should be inherited from just one class
Class Street is missing an association
Class Street should not be inherited from another class
Class Truck is missing an association
Missing attributes in class Street
Unexpected attributes in class Truck
Unexpected operations in class Driver
HERE
    assert_equal_differences(expected, pd.errors)
  end

  def test_miss_6
    soln  = @@plow_soln.mdl
    miss6 = @@miss6.mdl
    pd = UmlTools::LogicalDiff.new(soln, miss6)
    expected = <<HERE
Association between Segment and Street has a name, end, that should be a role (at one of the ends)
Association between Segment and Street has a name, end, that should be a role (at one of the ends)
Association between Segment and Street with name start appears to be reversed
Error in multiplicity for role Segment in association with Street
Error in multiplicity for role Segment in association with Street
Error in multiplicity for role Street in association with Segment
Error in multiplicity for role Street in association with Segment
Missing operations in class Truck
HERE
    assert_equal_differences(expected, pd.errors)
  end

  def test_alt_1
    soln  = @@plow_alt.mdl
    miss1 = @@miss1.mdl
    pd = UmlTools::LogicalDiff.new(soln, miss1)
    expected = <<HERE
Class Route should be associated with Segment instead of Street
Class Street has an unexpected association with Route
Class Street is missing 3 associations
Missing class: Segment
HERE
    assert_equal_differences(expected, pd.errors)
  end

  def test_alt_2
    soln  = @@plow_alt.mdl
    miss2 = @@miss2.mdl
    pd = UmlTools::LogicalDiff.new(soln, miss2)
    expected = <<HERE
Association between Segment and Street with name end appears to be reversed
Class DumpTruck has an unexpected association with Truck
Class DumpTruck should be inherited from another class
Class SnowPlow has an unexpected association with Truck
Class SnowPlow should be inherited from another class
Class Truck has unexpected associations with DumpTruck, SnowPlow
Incorrect role name for role Segment in association with Street
HERE
    assert_equal_differences(expected, pd.errors)
  end

  def test_alt_3
    soln  = @@plow_alt.mdl
    miss3 = @@miss3.mdl
    pd = UmlTools::LogicalDiff.new(soln, miss3)
    expected = <<HERE
Class Path should be associated with Segment instead of Street
Class Segment is missing 2 associations
Class Snow_Truck is inherited from the wrong class
Class Street should be associated with Segment instead of Path
Error in multiplicity for role Driver in association with Truck (none given; expected 1)
Error in multiplicity for role Truck in association with Driver (none given; expected 1)
Incorrect role name for role Street in association with Segment (no name given; expected start)
Missing attributes in class Truck: number
Unexpected attributes in class Driver: name
Unexpected attributes in class Path: total_distance
Unexpected attributes in class Street: length
HERE
    $explicit_errors = true
    assert_equal_differences(expected, pd.errors)
  end

  def test_alt_4
    soln  = @@plow_alt.mdl
    miss4 = @@miss4.mdl
    pd = UmlTools::LogicalDiff.new(soln, miss4)
    expected = <<HERE
Class Segment is missing 2 associations
Class Street is missing 2 associations
Error in multiplicity for role Segment in association with Route
Error in multiplicity for role Segment in association with Street
Error in multiplicity for role Truck in association with Driver
Incorrect role name for role Segment in association with Street
Missing operations in class Truck
HERE
    assert_equal_differences(expected, pd.errors)
  end

  def test_plow_alt_soln_comparison
    alt_soln  = @@plow_alt.mdl
    plow_soln = @@plow_soln.mdl
    # checking the other direction adds nothing
    pd = UmlTools::LogicalDiff.new(alt_soln, plow_soln)
    expected = <<HERE
Incorrect role name for role Segment in association with Street
HERE
    assert_equal_differences(expected, pd.errors)
  end

  def test_odd_5
    soln  = @@odd_soln.mdl
    miss5 = @@miss5.mdl
    pd = UmlTools::LogicalDiff.new(soln, miss5)
    expected = <<HERE
Class Driver has the right number of associations, but should be associated with Route, Truck instead of Segment, Snow
Class DumpTruck is inherited from the wrong class
Class Route is missing an association
Class Segment should be associated with Street instead of Driver
Class Snow should not be in the diagram
Class Street is missing an association
Class Street should not be inherited from another class
Class Truck is missing an association
Missing attributes in class Street
Number of super classes for SnowPlow does not match the expected number, 3
Unexpected attributes in class Truck
Unexpected operations in class Driver
HERE
    assert_equal_differences(expected, pd.errors)
  end

  # checks case in which role names have to match association names for the
  #   right message
  def test_book_miss_1
    soln  = @@book.mdl
    miss1 = @@bad_book1.mdl
    pd = UmlTools::LogicalDiff.new(soln, miss1)
    expected = <<HERE
Association between Page and Display has a name, current, that should be a role (at one of the ends)
Association between Page and Display has a name, current, that should be a role (at one of the ends)
Association between Page and Display has a name, next, that should be a role (at one of the ends)
Association between Page and Display has a name, next, that should be a role (at one of the ends)
Association between Page and Display has a name, previous, that should be a role (at one of the ends)
Association between Page and Display has a name, previous, that should be a role (at one of the ends)
HERE
    assert_equal_differences(expected, pd.errors)
  end

  def test_identical_match
    alt_soln = @@plow_alt.mdl
    alt_copy = @@plow_alt.mdl
    pd = UmlTools::LogicalDiff.new(alt_soln, alt_copy)
    assert_equal([], pd.errors)
  end

  def test_no_errors_comparison
    alt_soln  = @@plow_alt.mdl
    no_errors = @@no_err.mdl
    pd = UmlTools::LogicalDiff.new(alt_soln, no_errors)
    assert_equal([], pd.errors)
  end

  # coverage
  def test_element
    e = UmlTools::Model::Element.new(UmlTools::SAtom.new('fake elt'), UmlTools::SAtom.new('101'), nil, nil)
    assert_equal('fake elt', e.match_name)
    assert_equal('element fake elt', e.match_ident)
  end
end
