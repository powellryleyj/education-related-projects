# require_relative '../test_base'
#
# TODO: Flexmock
class TestSequenceDiff < Test::Unit::TestCase
  def create_model(lifeline_names: [], messages: [], lifelines: [])
    if lifelines.empty?
      lifelines = lifeline_names.map { |name| UmlTools::Model::Sequence::Lifeline.new 0, '', name, '', nil, []}
    end
    return UmlTools::Model::Sequence::FullModel.new lifelines, messages
  end

  must 'have no missing or extra lifelines when the submission and target lifelines all match' do
    submitted = create_model lifeline_names: ['Lifeline']
    correct = create_model lifeline_names: ['Lifeline']

    diff = UmlTools::SequenceDiff.new correct, submitted

    assert_true diff.missing_lifeline_names.empty?
    assert_true diff.extra_lifeline_names.empty?
  end

  must 'have a missing lifeline issue when the submission is missing one that the target has' do
    submitted = create_model
    correct = create_model lifeline_names: ['Lifeline']

    diff = UmlTools::SequenceDiff.new correct, submitted

    assert_equal ['Lifeline'], diff.missing_lifeline_names.map(&:message)
  end

  must 'have an extra lifeline issue when the submission is missing one that the target has' do
    submitted = create_model lifeline_names: ['ExtraLifeline']
    correct = create_model

    diff = UmlTools::SequenceDiff.new correct, submitted

    assert_equal ['ExtraLifeline'], diff.extra_lifeline_names.map(&:message)
  end

  must 'have an incorrect lifeline issue when the submission has an incorrect type compared to the target' do
    lifelines = [UmlTools::Model::Sequence::Lifeline.new(0, 'Type1', 'Lifeline', '', nil, []),
                 UmlTools::Model::Sequence::Lifeline.new(1, 'CorrectType', 'Lifeline', '', nil, [])]

    submitted = create_model lifelines: [lifelines[0]]
    correct = create_model lifelines: [lifelines[1]]

    diff = UmlTools::SequenceDiff.new correct, submitted

    assert_equal ['Lifeline (should be CorrectType)'],
                 diff.wrong_lifeline_types.map(&:message)
  end

  must 'not have an incorrect lifeline issue when the submission has an incorrect type compared to the target' do
    lifelines = [UmlTools::Model::Sequence::Lifeline.new(0, 'Type', 'Lifeline', '', nil, []),
                 UmlTools::Model::Sequence::Lifeline.new(1, 'Type', 'Lifeline', '', nil, [])]

    submitted = create_model lifelines: [lifelines[0]]
    correct = create_model lifelines: [lifelines[1]]

    diff = UmlTools::SequenceDiff.new correct, submitted

    assert_true diff.wrong_lifeline_types.empty?
  end

  must 'discover missing messages when the submission is missing one that the target has' do
    lifelines = [UmlTools::Model::Sequence::Lifeline.new(0, '', 'Lifeline1', '', nil, []),
                 UmlTools::Model::Sequence::Lifeline.new(1, '', 'Lifeline2', '', nil, [])]
    message_arg = UmlTools::Model::Sequence::MessageArgument.new 0, 'MessageArg', 'String'
    messages = [UmlTools::Model::Sequence::Message.new(0, 'Message', '', '', lifelines[0], lifelines[1], [message_arg], [])]

    submitted = create_model
    correct = create_model lifelines: lifelines, messages: messages

    diff = UmlTools::SequenceDiff.new correct, submitted

    assert_equal ['Lifeline1 to Lifeline2'], diff.missing_messages.map(&:message)
  end

  must 'match lifelines when words are swapped' do
    submitted = create_model lifeline_names: ['LifeLine']
    correct = create_model lifeline_names: ['LineLife']

    diff = UmlTools::SequenceDiff.new correct, submitted

    assert_equal 0, diff.extra_lifeline_names.size
    assert_equal 0, diff.missing_lifeline_names.size
  end

  must 'discover extra messages when the submission is missing one that the target has' do
    lifelines = [UmlTools::Model::Sequence::Lifeline.new(0, '', 'Lifeline1', '', nil, []),
                 UmlTools::Model::Sequence::Lifeline.new(1, '', 'Lifeline2', '', nil, [])]
    message_arg = UmlTools::Model::Sequence::MessageArgument.new 0, 'MessageArg', 'String'
    messages = [UmlTools::Model::Sequence::Message.new(0, 'Message', '', '', lifelines[0], lifelines[1], [message_arg], [])]

    submitted = create_model lifelines: lifelines, messages: messages
    correct = create_model

    diff = UmlTools::SequenceDiff.new correct, submitted

    assert_equal ['Lifeline1 to Lifeline2'], diff.extra_messages.map(&:message)
  end

  must 'discover mislabeled messages when the submission has a message matching the target but the method signature is wrong' do
    lifelines = [UmlTools::Model::Sequence::Lifeline.new(0, '', 'Lifeline1', '', nil, []),
                 UmlTools::Model::Sequence::Lifeline.new(1, '', 'Lifeline2', '', nil, [])]
    message_arg = UmlTools::Model::Sequence::MessageArgument.new 0, 'MessageArg', 'String'

    submitted = create_model lifelines: lifelines, messages: [UmlTools::Model::Sequence::Message.new(0, 'Message', '', '', lifelines[0], lifelines[1], [message_arg], [])]
    correct = create_model lifelines: lifelines, messages: [UmlTools::Model::Sequence::Message.new(0, 'Message2', '', '', lifelines[0], lifelines[1], [message_arg], [])]

    diff = UmlTools::SequenceDiff.new correct, submitted

    assert_equal ['Lifeline1 to Lifeline2'],
                 diff.mislabeled_messages.map(&:message)
  end

  must 'not discover mislabeled or extra messages when the submission has the correct messages' do
    lifelines = [UmlTools::Model::Sequence::Lifeline.new(0, '', 'Lifeline1', '', nil, []),
                 UmlTools::Model::Sequence::Lifeline.new(1, '', 'Lifeline2', '', nil, [])]
    message_arg = UmlTools::Model::Sequence::MessageArgument.new 0, 'MessageArg', 'String'
    messages = [UmlTools::Model::Sequence::Message.new(0, 'Message', '', '', lifelines[0], lifelines[1], [message_arg], [])]

    submitted = create_model lifelines: lifelines, messages: messages+messages
    correct = create_model lifelines: lifelines, messages: messages+messages

    diff = UmlTools::SequenceDiff.new correct, submitted

    assert_true diff.missing_messages.empty?
    assert_true diff.extra_messages.empty?
  end

  must 'discover missing & extra message arguments when the submission values do not match the target' do
    lifelines = [UmlTools::Model::Sequence::Lifeline.new(0, '', 'Lifeline1', '', nil, []),
                 UmlTools::Model::Sequence::Lifeline.new(1, '', 'Lifeline2', '', nil, [])]
    message_arg = UmlTools::Model::Sequence::MessageArgument.new 0, 'MessageAr', 'String'
    message_arg2 = UmlTools::Model::Sequence::MessageArgument.new 0, 'ArgMessage', 'String'

    submitted = create_model lifelines: lifelines,
                                  messages: [UmlTools::Model::Sequence::Message.new(0, 'Message', '', '', lifelines[0], lifelines[1], [message_arg], [])]
    correct = create_model lifelines: lifelines,
                                messages: [UmlTools::Model::Sequence::Message.new(0, 'Message', '', '', lifelines[0], lifelines[1], [message_arg2], [])]

    diff = UmlTools::SequenceDiff.new correct, submitted

    # TODO (rwh, sep 2016): understand why don't need .map(&:message) in following - possible inconsistency in design?
    assert_equal ['ArgMessage:String from Lifeline1 to Lifeline2'],
                 diff.missing_arguments

    assert_equal ['MessageAr:String from Lifeline1 to Lifeline2'],
                 diff.extra_arguments.map(&:message)

    # Check with swapped words
    message_arg = UmlTools::Model::Sequence::MessageArgument.new 0, 'MessageArg', 'String'
    message_arg2 = UmlTools::Model::Sequence::MessageArgument.new 0, 'ArgMessage', 'String'

    submitted = create_model lifelines: lifelines, messages: [UmlTools::Model::Sequence::Message.new(0, 'Message', '', '', lifelines[0], lifelines[1], [message_arg], [])]
    correct = create_model lifelines: lifelines, messages: [UmlTools::Model::Sequence::Message.new(0, 'Message', '', '', lifelines[0], lifelines[1], [message_arg2], [])]

    diff = UmlTools::SequenceDiff.new correct, submitted

    assert_equal 0, diff.extra_arguments.size
    assert_equal 0, diff.missing_arguments.size
    assert_equal 0, diff.invalid_argument_types.size

    # Check with correct values
    submitted = create_model lifelines: lifelines, messages: [UmlTools::Model::Sequence::Message.new(0, 'Message', '', '', lifelines[0], lifelines[1], [message_arg], [])]
    correct = create_model lifelines: lifelines, messages: [UmlTools::Model::Sequence::Message.new(0, 'Message', '', '', lifelines[0], lifelines[1], [message_arg], [])]

    diff = UmlTools::SequenceDiff.new correct, submitted

    assert_equal 0, diff.extra_arguments.size
    assert_equal 0, diff.missing_arguments.size
    assert_equal 0, diff.invalid_argument_types.size
  end

  must 'discover mismatching argument types for message arguments that the submission has' do
    lifelines = [UmlTools::Model::Sequence::Lifeline.new(0, '', 'Lifeline1', '', nil, []),
                 UmlTools::Model::Sequence::Lifeline.new(1, '', 'Lifeline2', '', nil, [])]
    message_arg = UmlTools::Model::Sequence::MessageArgument.new 0, 'MessageArg', 'String'
    message_arg2 = UmlTools::Model::Sequence::MessageArgument.new 0, 'MessageArg', 'Integer'

    submitted = create_model lifelines: lifelines, messages: [UmlTools::Model::Sequence::Message.new(0, 'Message', '', '', lifelines[0], lifelines[1], [message_arg], [])]
    correct = create_model lifelines: lifelines, messages: [UmlTools::Model::Sequence::Message.new(0, 'Message', '', '', lifelines[0], lifelines[1], [message_arg2], [])]

    diff = UmlTools::SequenceDiff.new correct, submitted

    assert_equal ['MessageArg:String (should be Integer) from Lifeline1 to Lifeline2'],
                 diff.invalid_argument_types.map(&:message)
  end

  must 'catch when message arguments are out of order' do
    lifelines = [UmlTools::Model::Sequence::Lifeline.new(0, '', 'Lifeline1', '', nil, []),
                 UmlTools::Model::Sequence::Lifeline.new(1, '', 'Lifeline2', '', nil, [])]
    message_arg = UmlTools::Model::Sequence::MessageArgument.new 0, 'TheLamerArg', 'String'
    message_arg2 = UmlTools::Model::Sequence::MessageArgument.new 0, 'TheCoolerArg', 'Integer'

    submitted = create_model lifelines: lifelines,
                                  messages: [UmlTools::Model::Sequence::Message.new(0, 'Message', '', '', lifelines[0], lifelines[1], [message_arg, message_arg2], [])]
    correct = create_model lifelines: lifelines,
                                messages: [UmlTools::Model::Sequence::Message.new(0, 'Message', '', '', lifelines[0], lifelines[1], [message_arg2, message_arg], [])]

    diff = UmlTools::SequenceDiff.new correct, submitted

    assert_equal 2, diff.out_of_position_arguments.size
    assert_equal ['Argument in position 0 from Lifeline1 to Lifeline2 is out of place! Should be TheCoolerArg',
                  'Argument in position 1 from Lifeline1 to Lifeline2 is out of place! Should be TheLamerArg'],
                 diff.out_of_position_arguments.map { |x| x.message }
  end

  must 'take keywords from lifelines into account when comparing names' do
    lifelines = [UmlTools::Model::Sequence::Lifeline.new(0, '', 'catSwag', '', nil, []),
                 UmlTools::Model::Sequence::Lifeline.new(1, '', 'Lifeline2', '', nil, ['SwagCat'])]
    submitted = create_model lifelines: lifelines[0..0]
    correct = create_model lifelines: lifelines[1..1]

    diff = UmlTools::SequenceDiff.new correct, submitted

    assert_equal 0, diff.extra_lifeline_names.size
    assert_equal 0, diff.missing_lifeline_names.size
  end

  must 'take keywords from messages into account when comparing names' do
    lifelines = [UmlTools::Model::Sequence::Lifeline.new(0, '', 'Lifeline1', '', nil, []),
                 UmlTools::Model::Sequence::Lifeline.new(1, '', 'Lifeline2', '', nil, [])]

    submitted = create_model lifelines: lifelines, messages: [UmlTools::Model::Sequence::Message.new(0, 'Dog', '', '', lifelines[0], lifelines[1], [], [])]
    correct = create_model lifelines: lifelines,
                                messages: [UmlTools::Model::Sequence::Message.new(0, 'Cat', '', '', lifelines[0], lifelines[1], [], ['dog'])]

    diff = UmlTools::SequenceDiff.new correct, submitted

    assert_equal 0, diff.missing_messages.size
    assert_equal 0, diff.extra_messages.size
  end

  # # Accessor methods
  # must 'compare lifelines when missing_lifeline_names is nil and accessed' do
  #   diff = UmlTools::SequenceDiff.new create_model, create_model
  #   diff = diff
  #   diff.should_receive(:compare_lifelines).times 1
  #   diff.missing_lifeline_names
  # end
  #
  # must 'compare lifelines when extra_lifeline_names is nil and accessed' do
  #   diff = UmlTools::SequenceDiff.new create_model, create_model
  #   diff = diff
  #   diff.should_receive(:compare_lifelines).times 1
  #   diff.extra_lifeline_names
  # end
  #
  # must 'compare messages when missing_messages is nil and accessed' do
  #   diff = UmlTools::SequenceDiff.new create_model, create_model
  #   diff = diff
  #   diff.should_receive(:compare_messages).times 1
  #   diff.missing_messages
  # end
  #
  # must 'compare messages when extra_messages is nil and accessed' do
  #   diff = UmlTools::SequenceDiff.new create_model, create_model
  #   diff = diff
  #   diff.should_receive(:compare_messages).times 1
  #   diff.extra_messages
  # end
  #
  # must 'compare messages when mislabeled_messages is nil and accessed' do
  #   diff = UmlTools::SequenceDiff.new create_model, create_model
  #   diff = diff
  #   diff.should_receive(:compare_messages).times 1
  #   diff.mislabeled_messages
  # end
  #
  # must 'compare messages when missing_arguments is nil and accessed' do
  #   diff = UmlTools::SequenceDiff.new create_model, create_model
  #   diff = diff
  #   diff.should_receive(:compare_messages).times 1
  #   diff.missing_arguments
  # end
  #
  # must 'compare messages when extra_arguments is nil and accessed' do
  #   diff = UmlTools::SequenceDiff.new create_model, create_model
  #   diff = diff
  #   diff.should_receive(:compare_messages).times 1
  #   diff.extra_arguments
  # end
  #
  # must 'compare messages when invalid_argument_types is nil and accessed' do
  #   diff = UmlTools::SequenceDiff.new create_model, create_model
  #   diff = diff
  #   diff.should_receive(:compare_messages).times 1
  #   diff.invalid_argument_types
  # end
end
