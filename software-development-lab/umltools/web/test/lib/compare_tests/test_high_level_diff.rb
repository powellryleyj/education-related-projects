# #
# # test checks based on looking at problem description
# #
# # rwh, sep 2016: I'm not convinced this is fully developed
# # TODO: (rwh sep 2016) investigate completeness of high level problem check feature
# #

require_relative '../test_base'

class TestHighLevelDiff < Test::Unit::TestCase
  def create_seq(lifeline_names: [], messages: [], lifelines: [])
    if lifelines.empty?
      lifelines = lifeline_names.map { |name| UmlTools::Model::Sequence::Lifeline.new(0, '', name, '', [])}
    end
    UmlTools::Model::Sequence::FullModel.new lifelines, messages
  end

  def create_cls(name, attrs, methods)
    attrs = attrs.map { |a| UmlTools::Model::Attribute.new a, 'id', nil, nil, nil, nil }
    methods = methods.map { |m| UmlTools::Model::Method.new m, 'id', nil, nil, nil }
    cls = UmlTools::Model::MClass.new name, 'id', nil, nil, nil, attrs, methods, nil
    logical = UmlTools::Model::LogicalModel.new [cls], []
    use_case = UmlTools::Model::UseCaseModel.new [], [], []
    UmlTools::Model::FullModel.new :model, use_case, logical
  end

  # TESTS

  must 'not complain about missing nouns in sequence diagrams' do
    nouns = 'Fish|Cat|Dog'
    submitted = create_seq lifeline_names: %w(Fish Dog Cat)
    diff = UmlTools::HighLevelDiff.new submitted, nouns, ''
    assert_equal 0, diff.errors.size
  end

  must 'complain about missing nouns in sequence diagrams' do
    nouns = 'Fish|Cat|Dog'
    submitted = create_seq lifeline_names: %w(Fish Cat)
    diff = UmlTools::HighLevelDiff.new submitted, nouns, ''
    assert_equal 1, diff.errors.size
  end

  must 'not complain about missing nouns in sequence diagrams when capitalization doesnt match' do
    nouns = 'Fish|Cat|Dog'
    submitted = create_seq lifeline_names: %w(FISH cat doG)
    diff = UmlTools::HighLevelDiff.new submitted, nouns, ''
    assert_equal 0, diff.errors.size
  end

  must 'complain about missing verbs in sequence diagrams' do
    verbs = 'Cat|Dog'
    submitted = create_seq messages: [UmlTools::Model::Sequence::Message.new(0, 'Cat', '', '', nil, nil, [], [])]
    diff = UmlTools::HighLevelDiff.new submitted, '', verbs
    assert_equal 1, diff.errors.size
  end

  must 'not complain about missing verbs in sequence diagrams when capitalization doesnt match' do
    verbs = 'Fish|Dog'
    submitted = create_seq messages: [
                                      UmlTools::Model::Sequence::Message.new(0, 'FiSh', '', '', nil, nil, [], []),
                                      UmlTools::Model::Sequence::Message.new(0, 'doG', '', '', nil, nil, [], [])]
    diff = UmlTools::HighLevelDiff.new submitted, '', verbs
    assert_equal 0, diff.errors.size
  end

  must 'complain about missing nouns in class diagrams' do
    nouns = 'Fish|Goat|Whale'
    submitted = create_cls 'Fish', ['Goat'], []
    diff = UmlTools::HighLevelDiff.new submitted, nouns, ''
    assert_equal 1, diff.errors.size
  end

  must 'complain about missing verbs in class diagrams' do
    verbs = 'KillTheGoat|MilkTheGoat'
    submitted = create_cls 'Class', [], ['KillTheGoat']
    diff = UmlTools::HighLevelDiff.new submitted, '', verbs
    assert_equal 1, diff.errors.size
  end

  must 'not complain about missing nouns in class diagrams when capitalization doesnt match' do
    nouns = 'GoaT|Cat'
    submitted = create_cls 'GOAT', ['caT'], []
    diff = UmlTools::HighLevelDiff.new submitted, nouns, ''
    assert_equal 0, diff.errors.size
  end

  must 'not complain about missing verbs in class diagrams when capitalization doesnt match' do
    verbs = 'TestTest'
    submitted = create_cls  'Class', [], ['teStTEsT']
    diff = UmlTools::HighLevelDiff.new submitted, '', verbs
    assert_equal 0, diff.errors.size
  end
end
