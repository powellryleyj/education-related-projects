# require_relative '../test_base'
#
# TODO: Flexmock
# class TestStateDiff < Test::Unit::TestCase
#
#   def create_mock_model(state_names = [], transitions = [])
#     states = state_names.map { |name| UmlTools::Model::State.new(name, 'Id', nil, nil, nil, nil) }
#     return flexmock(:state => UmlTools::Model::StateModel.new(states, transitions, [], []))
#   end
#
#   def create_state(name)
#     UmlTools::Model::State.new(name, 'ID-' + name, nil, nil, nil, nil)
#   end
#
#   def create_transition(source_name, target_name, trigger=[], guard=nil, effect=nil)
#     transition = UmlTools::Model::Transition.new(nil, nil, nil, nil, guard, effect, trigger)
#     transition.source_node = create_state(source_name)
#     transition.target_node = create_state(target_name)
#
#     return transition
#   end
#
#   def create_trigger(name, id=nil, stereotype=nil, doc=nil, type=nil, expression=nil)
#     UmlTools::Model::Trigger.new(name, id, stereotype, doc, type, expression)
#   end
#
#   must 'compare states when extra_states is nil and is accessed' do
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     mock_diff = flexmock(diff)
#     mock_diff.should_receive(:compare_states).times(1)
#     diff.extra_states
#   end
#
#   must 'compare states when missing_states is nil and is accessed' do
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     mock_diff = flexmock(diff)
#     mock_diff.should_receive(:compare_states).times(1)
#     diff.missing_states
#   end
#
#   must 'compare transitions when extra_transitions is nil and is accessed' do
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     mock_diff = flexmock(diff)
#     mock_diff.should_receive(:compare_transitions).times(1)
#     diff.extra_transitions
#   end
#
#   must 'compare transitions when missing_transitions is nil and is accessed' do
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     mock_diff = flexmock(diff)
#     mock_diff.should_receive(:compare_transitions).times(1)
#     diff.missing_transitions
#   end
#
#   must 'compare transitions when reversed_transitions is nil and is accessed' do
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     mock_diff = flexmock(diff)
#     mock_diff.should_receive(:compare_transitions).times(1)
#     diff.reversed_transitions
#   end
#
#   must 'compare transitions when incorrect_transitions is nil and is accessed' do
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     mock_diff = flexmock(diff)
#     mock_diff.should_receive(:compare_transitions).times(1)
#     diff.incorrect_transitions
#   end
#
#   must 'compare final/initial nodes when final_correct_minus_submitted is nil and is accessed' do
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     mock_diff = flexmock(diff)
#     mock_diff.should_receive(:compare_final_initial_nodes).times(1)
#     diff.final_correct_minus_submitted
#   end
#
#   must 'compare final/initial nodes when initial_correct_minus_submitted is nil and is accessed' do
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     mock_diff = flexmock(diff)
#     mock_diff.should_receive(:compare_final_initial_nodes).times(1)
#     diff.initial_correct_minus_submitted
#   end
#
#   must 'have a single extra state when submitted has a state that correct does not' do
#     expected_extra = 1
#
#     submitted = create_mock_model(['State1', 'State2'])
#     correct = create_mock_model(['State2'])
#
#     diff = UmlTools::StateDiff.new(correct, submitted)
#
#     assert_equal(expected_extra, diff.extra_states.size)
#     assert_equal('State1', diff.extra_states[0].name)
#   end
#
#   must 'have no extra states when submitted and correct have exact same states' do
#     expected_extra = 0
#
#     submitted = create_mock_model(['State1', 'State2'])
#     correct = create_mock_model(['State1', 'State2'])
#
#     diff = UmlTools::StateDiff.new(submitted, correct)
#
#     assert_equal(expected_extra, diff.extra_states.size)
#   end
#
#   must 'have a single missing state when correct has a state that correct does not' do
#     expected_missing = 1
#
#     submitted = create_mock_model(['State2'])
#     correct = create_mock_model(['State2', 'State3'])
#
#     diff = UmlTools::StateDiff.new(correct, submitted)
#
#     assert_equal(expected_missing, diff.missing_states.size)
#     assert_equal('State3', diff.missing_states[0].name)
#   end
#
#   must 'have no missing states when submitted and correct have exact same states' do
#     expected_missing = 0
#
#     submitted = create_mock_model(['State1', 'State2'])
#     correct = create_mock_model(['State1', 'State2'])
#
#     diff = UmlTools::StateDiff.new(submitted, correct)
#
#     assert_equal(expected_missing, diff.missing_states.size)
#   end
#
#   # Expected States:      State1, State2, State3
#   # Actual States:        State1, State2, State3
#   # Expected Transitions: State1 -> State2
#   # Actual Transitions:   State1 -> State2, State2 -> State3
#   #
#   # Expected Result:      extra transition State2 -> State3
#   must 'have a single extra transitions when submitted has a transition that correct does not' do
#     expected_extra = 1
#
#     submitted_transitions = [create_transition('State1', 'State2'), create_transition('State2', 'State3')]
#     correct_transitions = [create_transition('State1', 'State2')]
#
#     submitted = create_mock_model(['State1', 'State2', 'State3'], submitted_transitions)
#     correct = create_mock_model(['State1', 'State2', 'State3'], correct_transitions)
#
#     diff = UmlTools::StateDiff.new(correct, submitted)
#
#     assert_equal(expected_extra, diff.extra_transitions.size)
#     assert_equal('State2', diff.extra_transitions[0].source_node.name)
#     assert_equal('State3', diff.extra_transitions[0].target_node.name)
#   end
#
#   # Expected States:      State1, State2
#   # Actual States:        State1, State2, State3
#   # Expected Transitions: State1 -> State2
#   # Actual Transitions:   State1 -> State2, State2 -> State3
#   #
#   # Expected Result:      no extra transitions (State2 -> State3 is related to an extra state
#   #                                             so we don't count it)
#   must 'not count transitions to an extra state as extra' do
#     expected_extra = 0
#
#     submitted_transitions = [create_transition('State1', 'State2'), create_transition('State2', 'State3')]
#     correct_transitions = [create_transition('State1', 'State2')]
#
#     submitted = create_mock_model(['State1', 'State2', 'State3'], submitted_transitions)
#     correct = create_mock_model(['State1', 'State2'], correct_transitions)
#
#     diff = UmlTools::StateDiff.new(submitted, correct)
#
#     assert_equal(expected_extra, diff.extra_transitions.size)
#   end
#
#   # Expected States:      State1, State2
#   # Actual States:        State0, State1, State2
#   # Expected Transitions: State1 -> State2
#   # Actual Transitions:   State1 -> State2, State0 -> State1
#   #
#   # Expected Result:      no extra transitions (State0 -> State1 is related to an extra state
#   #                                             so we don't count it)
#   must 'not count transitions from an extra state as extra' do
#     expected_extra = 0
#
#     submitted_transitions = [create_transition('State1', 'State2'), create_transition('State0', 'State1')]
#     correct_transitions = [create_transition('State1', 'State2')]
#
#     submitted = create_mock_model(['State0', 'State1', 'State2'], submitted_transitions)
#     correct = create_mock_model(['State1', 'State2'], correct_transitions)
#
#     diff = UmlTools::StateDiff.new(submitted, correct)
#
#     assert_equal(expected_extra, diff.extra_transitions.size)
#   end
#
#   # Expected States:      State1, State2, State3
#   # Actual States:        State1, State2, State3
#   # Expected Transitions: State1 -> State2, State2 -> State3
#   # Actual Transitions:   State1 -> State2
#   #
#   # Expected Result:      missing transition State2 -> State3
#   must 'have a single missing transitions when correct has a transition that submitted does not' do
#     expected_missing = 1
#
#     submitted_transitions = [create_transition('State1', 'State2')]
#     correct_transitions = [create_transition('State1', 'State2'), create_transition('State2', 'State3')]
#
#     submitted = create_mock_model(['State1', 'State2', 'State3'], submitted_transitions)
#     correct = create_mock_model(['State1', 'State2', 'State3'], correct_transitions)
#
#     diff = UmlTools::StateDiff.new(correct, submitted)
#
#     assert_equal(expected_missing, diff.missing_transitions.size)
#     assert_equal('State2', diff.missing_transitions[0].source_node.name)
#     assert_equal('State3', diff.missing_transitions[0].target_node.name)
#   end
#
#   # Expected States:      State1, State2, State3
#   # Actual States:        State1, State2,
#   # Expected Transitions: State1 -> State2, State2 -> State3
#   # Actual Transitions:   State1 -> State2
#   #
#   # Expected Result:      no missing transitions (State2 -> State3 is related to an missing state
#   #                                               so we don't count it)
#   must 'not count transitions to a missing state as missing' do
#     expected_missing = 0
#
#     submitted_transitions = [create_transition('State1', 'State2')]
#     correct_transitions = [create_transition('State1', 'State2'), create_transition('State2', 'State3')]
#
#     submitted = create_mock_model(['State1', 'State2'], submitted_transitions)
#     correct = create_mock_model(['State1', 'State2', 'State3'], correct_transitions)
#
#     diff = UmlTools::StateDiff.new(submitted, correct)
#
#     assert_equal(expected_missing, diff.missing_transitions.size)
#   end
#
#   # Expected States:      State1, State2, State3
#   # Actual States:        State2, State3,
#   # Expected Transitions: State1 -> State2, State2 -> State3
#   # Actual Transitions:   State2 -> State3
#   #
#   # Expected Result:      no missing transitions (State2 -> State3 is related to an missing state
#   #                                               so we don't count it)
#   must 'not count transitions from a missing state as missing' do
#     expected_missing = 0
#
#     submitted_transitions = [create_transition('State2', 'State3')]
#     correct_transitions = [create_transition('State1', 'State2'), create_transition('State2', 'State3')]
#
#     submitted = create_mock_model(['State2', 'State3'], submitted_transitions)
#     correct = create_mock_model(['State1', 'State2', 'State3'], correct_transitions)
#
#     diff = UmlTools::StateDiff.new(submitted, correct)
#
#     assert_equal(expected_missing, diff.missing_transitions.size)
#   end
#
#   # Expected States:      State1, State2
#   # Actual States:        State1, State2
#   # Expected Transitions: State1 -> State2
#   # Actual Transitions:   State2 -> State1
#   #
#   # Expected Result:      reversed transition State2 -> State1
#   must 'have one reversed transition when submitted has a backwards transition' do
#     expected_reversed = 1
#     expected_extra = 0
#     expected_missing = 0
#
#     submitted_transitions = [create_transition('State2', 'State1')]
#     correct_transitions = [create_transition('State1', 'State2')]
#
#     submitted = create_mock_model(['State1', 'State2'], submitted_transitions)
#     correct = create_mock_model(['State1', 'State2'], correct_transitions)
#
#     diff = UmlTools::StateDiff.new(correct, submitted)
#
#     assert_equal(expected_reversed, diff.reversed_transitions.size)
#     assert_equal('State2', diff.reversed_transitions[0].source_node.name)
#     assert_equal('State1', diff.reversed_transitions[0].target_node.name)
#
#     # make sure the reversed transition doesn't show up as missing or extra
#     assert_equal(expected_extra, diff.extra_transitions.size)
#     assert_equal(expected_missing, diff.missing_transitions.size)
#   end
#
#   # Expected States:      State1, State2, State3
#   # Actual States:        State1, State2, State3
#   # Expected Transitions: State1 -> State2, State2 -> State3
#   # Actual Transitions:   State1 -> State2, State2 -> State3
#   #
#   # Expected Result:      no extra or missing transitions
#   must 'have no extra or missing transitions when submitted and correct have same transitions' do
#     expected_extra = 0
#     expected_missing = 0
#
#     submitted_transitions = [create_transition('State1', 'State2'), create_transition('State2', 'State3')]
#     correct_transitions = [create_transition('State1', 'State2'), create_transition('State2', 'State3')]
#
#     submitted = create_mock_model(['State1', 'State2', 'State3'], submitted_transitions)
#     correct = create_mock_model(['State1', 'State2', 'State3'], correct_transitions)
#
#     diff = UmlTools::StateDiff.new(submitted, correct)
#
#     assert_equal(expected_extra, diff.extra_transitions.size)
#     assert_equal(expected_missing, diff.missing_transitions.size)
#   end
#
#   # Expected States:      State1, State2
#   # Actual States:        State1, State2
#   # Expected Transitions: State1 -> State2 (trigger)
#   # Actual Transitions:   State1 -> State2
#   #
#   # Expected Result:      incorrect transition State1 -> State2
#   must 'have incorrect transition if submitted trigger does not match correct trigger' do
#     expected_extra = 0
#     expected_missing = 0
#     expected_incorrect = 1
#
#     submitted_transitions = [create_transition('State1', 'State2')]
#     correct_transitions = [create_transition('State1', 'State2', [create_trigger('some transition')])]
#
#     submitted = create_mock_model(['State1', 'State2'], submitted_transitions)
#     correct = create_mock_model(['State1', 'State2'], correct_transitions)
#
#     diff = UmlTools::StateDiff.new(submitted, correct)
#
#     assert_equal(expected_incorrect, diff.incorrect_transitions.size)
#     assert_equal('State1', diff.incorrect_transitions[0].source_node.name)
#     assert_equal('State2', diff.incorrect_transitions[0].target_node.name)
#
#     # make sure the incorrect transition doesn't show up as missing or extra
#     assert_equal(expected_extra, diff.extra_transitions.size)
#     assert_equal(expected_missing, diff.missing_transitions.size)
#   end
#
#   # Expected States:      State1, State2
#   # Actual States:        State1, State2
#   # Expected Transitions: State1 -> State2 (guard)
#   # Actual Transitions:   State1 -> State2
#   #
#   # Expected Result:      incorrect transition State1 -> State2
#   must 'have incorrect transition if submitted guard does not match correct guard' do
#     expected_extra = 0
#     expected_missing = 0
#     expected_incorrect = 1
#
#     submitted_transitions = [create_transition('State1', 'State2')]
#     correct_transitions = [create_transition('State1', 'State2', [], 'guard')]
#
#     submitted = create_mock_model(['State1', 'State2'], submitted_transitions)
#     correct = create_mock_model(['State1', 'State2'], correct_transitions)
#
#     diff = UmlTools::StateDiff.new(submitted, correct)
#
#     assert_equal(expected_incorrect, diff.incorrect_transitions.size)
#     assert_equal('State1', diff.incorrect_transitions[0].source_node.name)
#     assert_equal('State2', diff.incorrect_transitions[0].target_node.name)
#
#     # make sure the incorrect transition doesn't show up as missing or extra
#     assert_equal(expected_extra, diff.extra_transitions.size)
#     assert_equal(expected_missing, diff.missing_transitions.size)
#   end
#
#   # Expected States:      State1, State2, State3, State4
#   # Actual States:        State1, State2, State3, State4
#   # Expected Transitions: State1 -> State2, State1 -> State3, State2 -> State4, State3 -> State2,
#   #                       State3 -> State1 (trigger), State4 -> State3
#   # Actual Transitions:   State1 -> State2, State1 -> State3, State2 -> State4, State2 -> State3,
#   #                       State3 -> State1, State2 -> State1
#   #
#   # Expected Result:      extra transition State2 -> State1
#   #                       missing transition State4 -> State3
#   #                       reversed transition State2 -> State3
#   #                       incorrect transition State3 -> State1
#   must 'have multiple types of errors if they are all present' do
#     expected = 1
#
#     correct_transitions = [create_transition('State1', 'State2'), create_transition('State1', 'State3'),
#                              create_transition('State2', 'State4'), create_transition('State3', 'State2'),
#                              create_transition('State3', 'State1', [create_trigger('trigger')]),
#                              create_transition('State4', 'State3')]
#     submitted_transitions = [create_transition('State1', 'State2'), create_transition('State1', 'State3'),
#                            create_transition('State2', 'State4'), create_transition('State2', 'State3'),
#                            create_transition('State3', 'State1'), create_transition('State2', 'State1')]
#
#     submitted = create_mock_model(['State1', 'State2', 'State3', 'State4'], submitted_transitions)
#     correct = create_mock_model(['State1', 'State2', 'State3', 'State4'], correct_transitions)
#
#     diff = UmlTools::StateDiff.new(correct, submitted)
#
#     # check extra transitions
#     assert_equal(expected, diff.extra_transitions.size)
#     assert_equal('State2', diff.extra_transitions[0].source_node.name)
#     assert_equal('State1', diff.extra_transitions[0].target_node.name)
#
#     # check missing transitions
#     assert_equal(expected, diff.missing_transitions.size)
#     assert_equal('State4', diff.missing_transitions[0].source_node.name)
#     assert_equal('State3', diff.missing_transitions[0].target_node.name)
#
#     # check reversed transitions
#     assert_equal(expected, diff.reversed_transitions.size)
#     assert_equal('State2', diff.reversed_transitions[0].source_node.name)
#     assert_equal('State3', diff.reversed_transitions[0].target_node.name)
#
#     assert_equal(expected, diff.incorrect_transitions.size)
#     assert_equal('State3', diff.incorrect_transitions[0].source_node.name)
#     assert_equal('State1', diff.incorrect_transitions[0].target_node.name)
#   end
#
#   # Expected States:      State1, State2
#   # Actual States:        State1, State2
#   # Expected Transitions: State1 -> State2 (effect)
#   # Actual Transitions:   State1 -> State2
#   #
#   # Expected Result:      incorrect transition State1 -> State2
#   must 'have incorrect transition if submitted effect does not match correct effect' do
#     expected_extra = 0
#     expected_missing = 0
#     expected_incorrect = 1
#
#     submitted_transitions = [create_transition('State1', 'State2')]
#     correct_transitions = [create_transition('State1', 'State2', [], nil, 'effect')]
#
#     submitted = create_mock_model(['State1', 'State2'], submitted_transitions)
#     correct = create_mock_model(['State1', 'State2'], correct_transitions)
#
#     diff = UmlTools::StateDiff.new(submitted, correct)
#
#     assert_equal(expected_incorrect, diff.incorrect_transitions.size)
#     assert_equal('State1', diff.incorrect_transitions[0].source_node.name)
#     assert_equal('State2', diff.incorrect_transitions[0].target_node.name)
#
#     # make sure the incorrect transition doesn't show up as missing or extra
#     assert_equal(expected_extra, diff.extra_transitions.size)
#     assert_equal(expected_missing, diff.missing_transitions.size)
#   end
#
#   must 'have 0 for final node differences when comparing models with same number of nodes' do
#     correct_state = UmlTools::Model::StateModel.new([], [], [], %w(finalNode1 finalNode2))
#     correct = flexmock(:state => correct_state)
#     submitted_state = UmlTools::Model::StateModel.new([], [], [], %w(finalNode1 finalNode2))
#     submitted = flexmock(:state => submitted_state)
#     diff = UmlTools::StateDiff.new(submitted, correct)
#     diff.compare_final_initial_nodes
#     expected = 0
#     assert_equal(expected, diff.final_correct_minus_submitted)
#   end
#
#   must 'have 1 for final node differences when comparing models with one more node in correct' do
#     correct_state = UmlTools::Model::StateModel.new([], [], [], %w(finalNode1 finalNode2 finalNode3))
#     correct = flexmock(:state => correct_state)
#     submitted_state = UmlTools::Model::StateModel.new([], [], [], %w(finalNode1 finalNode2))
#     submitted = flexmock(:state => submitted_state)
#     diff = UmlTools::StateDiff.new(correct, submitted)
#     diff.compare_final_initial_nodes
#     expected = 1
#     assert_equal(expected, diff.final_correct_minus_submitted)
#   end
#
#
#   must 'have -2 for initial node differences when comparing models with two more nodes in submitted' do
#     correct_state = UmlTools::Model::StateModel.new([], [], %w(initialNode), [])
#     correct = flexmock(:state => correct_state)
#     submitted_state = UmlTools::Model::StateModel.new([], [], %w(initialNode, initialNode2, initialNode3), [])
#     submitted = flexmock(:state => submitted_state)
#     diff = UmlTools::StateDiff.new(correct, submitted)
#     diff.compare_final_initial_nodes
#     expected = -2
#     assert_equal(expected, diff.initial_correct_minus_submitted)
#
#   end
#
#   # Tests the match_state_by_name method with matching names
#   def test_match_state_by_name_exact_name_match
#     correct_state = UmlTools::Model::State.new('red light', 'Id', nil,nil,nil,nil)
#     submitted_state = UmlTools::Model::State.new('red light', 'Id', nil,nil,nil,nil)
#     diff = UmlTools::StateDiff.new(create_mock_model,create_mock_model)
#
#     assert(diff.match_state_by_name?(submitted_state, correct_state),
#            "Submitted state (name: #{submitted_state.name}) expected to matched correct state (name: #{correct_state.name}, doc: #{correct_state.doc})")
#   end
#
#   # Tests the match_state_by_name method with a correct name that partially
#   # matches the submitted name
#   def test_match_state_by_name_partial_name_match
#     correct_state = UmlTools::Model::State.new('red', 'Id', nil,nil,nil,nil)
#     submitted_state = UmlTools::Model::State.new('red light', 'Id', nil,nil,nil,nil)
#     diff = UmlTools::StateDiff.new(create_mock_model,create_mock_model)
#
#     assert(diff.match_state_by_name?(submitted_state, correct_state),
#            "Submitted state (name: #{submitted_state.name}) expected to matched correct state (name: #{correct_state.name}, doc: #{correct_state.doc})")
#   end
#
#   # Tests the match_state_by_name method with a submitted name that partially
#   # matches the correct name
#   def test_match_state_by_name_partial_name_mismatch
#     correct_state = UmlTools::Model::State.new('red light', 'Id', nil,nil,nil,nil)
#     submitted_state = UmlTools::Model::State.new('red', 'Id', nil,nil,nil,nil)
#     diff = UmlTools::StateDiff.new(create_mock_model,create_mock_model)
#
#     assert(!diff.match_state_by_name?(submitted_state, correct_state),
#            "Submitted state (name: #{submitted_state.name}) not expected to matched correct state (name: #{correct_state.name}, doc: #{correct_state.doc})")
#   end
#
#   # Tests the match_state_by_name method with a submitted name that only
#   # partially matches the correct name, but matches a keyword
#   def test_match_state_by_name_exact_keyword_match
#     correct_state = UmlTools::Model::State.new('red light', 'Id', nil,'green, red, stop',nil,nil)
#     submitted_state = UmlTools::Model::State.new('red', 'Id', nil,nil,nil,nil)
#     diff = UmlTools::StateDiff.new(create_mock_model,create_mock_model)
#
#     assert(diff.match_state_by_name?(submitted_state, correct_state),
#            "Submitted state (name: #{submitted_state.name}) expected to matched correct state (name: #{correct_state.name}, doc: #{correct_state.doc})")
#   end
#
#   # Tests the match_state_by_name method with a keyword that partially
#   # matches the submitted name
#   def test_match_state_by_name_partial_keyword_match
#     correct_state = UmlTools::Model::State.new('stop', 'Id', nil,'halt, red',nil,nil)
#     submitted_state = UmlTools::Model::State.new('red light', 'Id', nil,nil,nil,nil)
#     diff = UmlTools::StateDiff.new(create_mock_model,create_mock_model)
#
#     assert(diff.match_state_by_name?(submitted_state, correct_state),
#            "Submitted state (name: #{submitted_state.name}) expected to matched correct state (name: #{correct_state.name}, doc: #{correct_state.doc})")
#   end
#
#   # Tests the match_state_by_name method with a submitted name that
#   # partially matches a keyword
#   def test_match_state_by_name_partial_keyword_mismatch
#     correct_state = UmlTools::Model::State.new('light', 'Id', nil,'halt, red light, stop',nil,nil)
#     submitted_state = UmlTools::Model::State.new('red', 'Id', nil,nil,nil,nil)
#     diff = UmlTools::StateDiff.new(create_mock_model,create_mock_model)
#
#     assert(!diff.match_state_by_name?(submitted_state, correct_state),
#            "Submitted state (name: #{submitted_state.name}) not expected to matched correct state (name: #{correct_state.name}, doc: #{correct_state.doc})")
#   end
#
#   must 'not match if the correct state name is concatenated and cannot be separated by case' do
#     correct_state = UmlTools::Model::State.new('redlight', 'Id', nil,nil,nil,nil)
#     submitted_state = UmlTools::Model::State.new('red light', 'Id', nil,nil,nil,nil)
#     diff = UmlTools::StateDiff.new(create_mock_model,create_mock_model)
#
#     assert(!diff.match_state_by_name?(submitted_state, correct_state),
#            "Submitted state (name: #{submitted_state.name}) not expected to matched correct state (name: #{correct_state.name}, doc: #{correct_state.doc})")
#   end
#
#   must 'match if the correct state name is concatenated but can be separated by case' do
#     correct_state = UmlTools::Model::State.new('RedLight', 'Id', nil,nil,nil,nil)
#     submitted_state = UmlTools::Model::State.new('red light', 'Id', nil,nil,nil,nil)
#     diff = UmlTools::StateDiff.new(create_mock_model,create_mock_model)
#
#     assert(diff.match_state_by_name?(submitted_state, correct_state),
#            "Submitted state (name: #{submitted_state.name}) expected to matched correct state (name: #{correct_state.name}, doc: #{correct_state.doc})")
#   end
#
#   must 'match if the correct state name is separated by underscores' do
#     correct_state = UmlTools::Model::State.new('red_light', 'Id', nil,nil,nil,nil)
#     submitted_state = UmlTools::Model::State.new('red light', 'Id', nil,nil,nil,nil)
#     diff = UmlTools::StateDiff.new(create_mock_model,create_mock_model)
#
#     assert(diff.match_state_by_name?(submitted_state, correct_state),
#            "Submitted state (name: #{submitted_state.name}) expected to matched correct state (name: #{correct_state.name}, doc: #{correct_state.doc})")
#   end
#
#   must 'match if the submitted state name is concatenated but can be separated by case' do
#     correct_state = UmlTools::Model::State.new('Red Light', 'Id', nil,nil,nil,nil)
#     submitted_state = UmlTools::Model::State.new('RedLight', 'Id', nil,nil,nil,nil)
#     diff = UmlTools::StateDiff.new(create_mock_model,create_mock_model)
#
#     assert(diff.match_state_by_name?(submitted_state, correct_state),
#            "Submitted state (name: #{submitted_state.name}) expected to matched correct state (name: #{correct_state.name}, doc: #{correct_state.doc})")
#   end
#
#   must 'match if the submitted state name is separated by underscores' do
#     correct_state = UmlTools::Model::State.new('Red Light', 'Id', nil,nil,nil,nil)
#     submitted_state = UmlTools::Model::State.new('red_light', 'Id', nil,nil,nil,nil)
#     diff = UmlTools::StateDiff.new(create_mock_model,create_mock_model)
#
#     assert(diff.match_state_by_name?(submitted_state, correct_state),
#            "Submitted state (name: #{submitted_state.name}) expected to matched correct state (name: #{correct_state.name}, doc: #{correct_state.doc})")
#   end
#
#   must 'not match if the submitted state name is concatenated but cannot be separated by case' do
#     correct_state = UmlTools::Model::State.new('Red Light', 'Id', nil,nil,nil,nil)
#     submitted_state = UmlTools::Model::State.new('redlight', 'Id', nil,nil,nil,nil)
#     diff = UmlTools::StateDiff.new(create_mock_model,create_mock_model)
#
#     assert(!diff.match_state_by_name?(submitted_state, correct_state),
#            "Submitted state (name: #{submitted_state.name}) not expected to matched correct state (name: #{correct_state.name}, doc: #{correct_state.doc})")
#   end
#
#   must 'not match if the compared objects are not of the same type' do
#     correct_state = UmlTools::Model::State.new('Red Light', 'Id', nil,nil,nil,nil)
#     submitted_inital_node = UmlTools::Model::InitialNode.new('', 'Id', nil,nil,nil)
#     diff = UmlTools::StateDiff.new(create_mock_model,create_mock_model)
#
#     assert(!diff.match_state_by_name?(submitted_inital_node, correct_state),
#            "Submitted state (name: #{submitted_inital_node.name}) not expected to matched correct state (name: #{correct_state.name}, doc: #{correct_state.doc})")
#   end
#
#   must 'add an error when there is an extra state present' do
#     expected_errors = 1
#     expected_error = :extra_states_present
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     extra_states = [UmlTools::Model::State.new('Name', 'Id', nil, nil, nil, nil)]
#     diff_mock = flexmock(diff, :extra_states=>extra_states)
#     errors = diff_mock.errors
#     assert_equal(expected_errors, errors.size)
#     assert_equal(expected_error, errors[0].particular_problem)
#   end
#
#   must 'add an error when there is a missing state' do
#     expected_errors = 1
#     expected_error = :states_missing
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     missing_states = [UmlTools::Model::State.new('Name', 'Id', nil, nil, nil, nil)]
#     diff_mock = flexmock(diff, :missing_states=>missing_states)
#     errors = diff_mock.errors
#     assert_equal(expected_errors, errors.size)
#     assert_equal(expected_error, errors[0].particular_problem)
#   end
#
#   must 'add an error when there is an extra transition present' do
#     expected_errors = 1
#     expected_error = :extra_transitions_present
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     extra_transitions = [create_transition('State1', 'State2')]
#     diff_mock = flexmock(diff, :extra_transitions=>extra_transitions)
#     errors = diff_mock.errors
#     assert_equal(expected_errors, errors.size)
#     assert_equal(expected_error, errors[0].particular_problem)
#   end
#
#   must 'add an error when there is a missing transition' do
#     expected_errors = 1
#     expected_error = :transition_missing
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     missing_transitions = [create_transition('State1', 'State2')]
#     diff_mock = flexmock(diff, :missing_transitions=>missing_transitions)
#     errors = diff_mock.errors
#     assert_equal(expected_errors, errors.size)
#     assert_equal(expected_error, errors[0].particular_problem)
#   end
#
#   must 'add an error when there is a reversed transition' do
#     expected_errors = 1
#     expected_error = :transition_reversed
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     reversed_transitions = [create_transition('State2', 'State1')]
#     diff_mock = flexmock(diff, :reversed_transitions=>reversed_transitions)
#     errors = diff_mock.errors
#     assert_equal(expected_errors, errors.size)
#     assert_equal(expected_error, errors[0].particular_problem)
#   end
#
#   must 'add an error when there is an incorrect transition' do
#     expected_errors = 1
#     expected_error = :transition_incorrect
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     incorrect_transitions = [create_transition('State1', 'State2', ['trigger'])]
#     diff_mock = flexmock(diff, :incorrect_transitions=>incorrect_transitions)
#     errors = diff_mock.errors
#     assert_equal(expected_errors, errors.size)
#     assert_equal(expected_error, errors[0].particular_problem)
#   end
#
#   must 'add an error when there is an extra final node' do
#     expected_errors = 1
#     expected_error = :extra_final
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     final_correct_minus_submitted = -1
#     diff_mock = flexmock(diff, :final_correct_minus_submitted=>final_correct_minus_submitted)
#     errors = diff_mock.errors
#     assert_equal(expected_errors, errors.size)
#     assert_equal(expected_error, errors[0].particular_problem)
#   end
#
#   must 'add an error when there is a missing final node' do
#     expected_errors = 1
#     expected_error = :missing_final
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     final_correct_minus_submitted = 1
#     diff_mock = flexmock(diff, :final_correct_minus_submitted=>final_correct_minus_submitted)
#     errors = diff_mock.errors
#     assert_equal(expected_errors, errors.size)
#     assert_equal(expected_error, errors[0].particular_problem)
#   end
#
#   must 'add an error when there is an extra initial node' do
#     expected_errors = 1
#     expected_error = :extra_initial
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     initial_correct_minus_submitted = -1
#     diff_mock = flexmock(diff, :initial_correct_minus_submitted=>initial_correct_minus_submitted)
#     errors = diff_mock.errors
#     assert_equal(expected_errors, errors.size)
#     assert_equal(expected_error, errors[0].particular_problem)
#   end
#
#   must 'add an error when there is a missing initial node' do
#     expected_errors = 1
#     expected_error = :missing_initial
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     initial_correct_minus_submitted = 1
#     diff_mock = flexmock(diff, :initial_correct_minus_submitted=>initial_correct_minus_submitted)
#     errors = diff_mock.errors
#     assert_equal(expected_errors, errors.size)
#     assert_equal(expected_error, errors[0].particular_problem)
#   end
#
#   # Test the get_individual_identifiers method to ensure it splits
#   # by case, underscore, and spaces correctly.
#   def test_get_individual_identifiers
#     expected = ['abc', 'def', 'ghi', 'jkl']
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     result = diff.get_individual_identifiers('Abc_DefGhi jkl')
#     assert_equal(expected, result)
#   end
#
#   must 'split on | unless it is escaped using backslash' do
#     expected = ['abc', 'def || ghi']
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     result = diff.separate_and_escape('abc | def \|\| ghi')
#     assert_equal(expected, result)
#   end
#
#   must 'match by value when it is an exact match' do
#     submitted = 'a b c'
#     possible_values = ['a b c']
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     assert(diff.match_by_value(submitted, possible_values), 'The submitted value should have been an exact match')
#   end
#
#   must 'match by value when the submitted has extra values' do
#     submitted = 'a b c'
#     possible_values = ['a b']
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     assert(diff.match_by_value(submitted, possible_values), 'The submitted value should have matched partially')
#   end
#
#   must 'match by value when the submitted matches one of the possible values' do
#     submitted = 'a b c'
#     possible_values = ['a b', 'd e']
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     assert(diff.match_by_value(submitted, possible_values),
#            'The submitted value should have matched one of the possible values')
#   end
#
#   must 'not match by value when the submitted does not match any of the possible values' do
#     submitted = 'a b c'
#     possible_values = ['d e f', 'g h i']
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     assert(!diff.match_by_value(submitted, possible_values),
#            'The submitted value should not have matched any of the possible values')
#   end
#
#   must 'not match by value when the submitted only partially matches the possible value' do
#     submitted = 'a b c'
#     possible_values = ['b c d']
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     assert(!diff.match_by_value(submitted, possible_values),
#            'The submitted value should not have completely matched the possible value')
#   end
#
#   must 'match guard if neither have a guard' do
#     submitted = create_transition('State1', 'State2', [], nil, nil)
#     correct = create_transition('State1', 'State2', [], nil, nil)
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     result = diff.do_guards_match?(submitted, correct)
#     assert(result, 'The guards should have matched since neither transitions had guards')
#   end
#
#   must 'match guard if the values are a match' do
#     submitted = create_transition('State1', 'State2', [], 'guard', nil)
#     correct = create_transition('State1', 'State2', [], 'guard | guard2', nil)
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     result = diff.do_guards_match?(submitted, correct)
#     assert(result, 'The guard on the submitted transition should have matched one of the correct transitions guard')
#   end
#
#   must 'not match guard if the submitted guard does not contain any of the correct guards values' do
#     submitted = create_transition('State1', 'State2', [], 'guard', nil)
#     correct = create_transition('State1', 'State2', [], 'guard1 | guard2', nil)
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     result = diff.do_guards_match?(submitted, correct)
#     assert(!result,
#            'The guard on the submitted transition should not have matched any of the correct transitions guards')
#   end
#
#   must 'match guard regardless of value if the correct transitions guard is a wildcard character' do
#     submitted = create_transition('State1', 'State2', [], 'guard', nil)
#     correct = create_transition('State1', 'State2', [], '*', nil)
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     result = diff.do_guards_match?(submitted, correct)
#     assert(result, 'The guard on the submitted transition should have matched regardless of value')
#   end
#
#   must 'not match guard if the correct transitions guard is a wildcard character but the submitted is nil' do
#     submitted = create_transition('State1', 'State2', [], nil, nil)
#     correct = create_transition('State1', 'State2', [], '*', nil)
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     result = diff.do_guards_match?(submitted, correct)
#     assert(!result, 'The guard on the submitted transition should have matched one of the correct transitions guard')
#   end
#
#   must 'match effect if neither have an effect' do
#     submitted = create_transition('State1', 'State2', [], nil, nil)
#     correct = create_transition('State1', 'State2', [], nil, nil)
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     result = diff.do_effects_match?(submitted, correct)
#     assert(result, 'The effects should match since neither had an effect')
#   end
#
#   must 'match effect if the values are a match' do
#     submitted = create_transition('State1', 'State2', [], nil, 'effect')
#     correct = create_transition('State1', 'State2', [], nil, 'effect | effect2')
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     result = diff.do_effects_match?(submitted, correct)
#     assert(result, 'The effect on the submitted transition should have matched one of the correct transitions effects')
#   end
#
#   must 'not match effect if the submitted effect does not contain any of the correct effects values' do
#     submitted = create_transition('State1', 'State2', [], nil, 'effect')
#     correct = create_transition('State1', 'State2', [], nil, 'effect1 | effect2')
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     result = diff.do_effects_match?(submitted, correct)
#     assert(!result,
#            'The effect on the submitted transition should not have matched any of the correct transitions effects')
#   end
#
#   must 'match effect regardless of value if the correct transitions effect is a wildcard character' do
#     submitted = create_transition('State1', 'State2', [], nil, 'effect')
#     correct = create_transition('State1', 'State2', [], nil, '*')
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     result = diff.do_effects_match?(submitted, correct)
#     assert(result, 'The effect on the submitted transition should have matched regardless of value')
#   end
#
#   must 'not match effect if the correct transitions effect is a wildcard character but the submitted is nil' do
#     submitted = create_transition('State1', 'State2', [], nil, nil)
#     correct = create_transition('State1', 'State2', [], nil, '*')
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     result = diff.do_effects_match?(submitted, correct)
#     assert(!result, 'The effect on the submitted transition should have matched one of the correct transitions effects')
#   end
#
#   must 'match triggers if neither the submitted nor the correct transitions have triggers' do
#     submitted = create_transition('State1', 'State2', [], nil, nil)
#     correct = create_transition('State1', 'State2', [], nil, nil)
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     result = diff.do_triggers_match?(submitted, correct)
#     assert(result, 'The triggers should have matched since neither had any triggers')
#   end
#
#   must 'not match triggers if the submitted has no triggers but correct expects one' do
#     submitted = create_transition('State1', 'State2', [], nil, nil)
#     correct = create_transition('State1', 'State2', [create_trigger('trig1 | trig2'), create_trigger('trig3')], nil, nil)
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     result = diff.do_triggers_match?(submitted, correct)
#     assert(!result, 'The triggers should not have matched since there were no submitted triggers')
#   end
#
#   must 'match triggers if the submitted trigger matches at least one of the correct triggers' do
#     submitted = create_transition('State1', 'State2', [create_trigger('trig1')], nil, nil)
#     correct = create_transition('State1', 'State2', [create_trigger('trig1 | trig2'), create_trigger('trig3')], nil, nil)
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     result = diff.do_triggers_match?(submitted, correct)
#     assert(result, 'The triggers should have matched since the submitted trigger matched one of the correct triggers')
#   end
#
#   must 'not match triggers if the submitted trigger does not match at least one of the correct triggers' do
#     submitted = create_transition('State1', 'State2', [create_trigger('trig')], nil, nil)
#     correct = create_transition('State1', 'State2', [create_trigger('trig1 | trig2')], nil, nil)
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     result = diff.do_triggers_match?(submitted, correct)
#     assert(!result,
#            'The triggers should not have matched since the submitted diagram did not match any of the correct triggers')
#   end
#
#   must 'match trigger regardless of value if at least one of the correct transitions triggers is a wildcard character' do
#     submitted = create_transition('State1', 'State2', [create_trigger('wildcard')], nil, nil)
#     correct = create_transition('State1', 'State2', [create_trigger('trig1 | trig2'), create_trigger('*')], nil, nil)
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     result = diff.do_triggers_match?(submitted, correct)
#     assert(result, 'The triggers should have matched since the correct transitions triggers contained a wildcard')
#   end
#
#   must 'not match triggers if the correct transitions trigger is a wildcard but the submitted is empty' do
#     submitted = create_transition('State1', 'State2', [], nil, nil)
#     correct = create_transition('State1', 'State2', [create_trigger('trig1 | trig2'), create_trigger('*')], nil, nil)
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     result = diff.do_triggers_match?(submitted, correct)
#     assert(!result, 'The triggers should not have matched since the submitted transitions trigger array was empty')
#   end
#
#   must 'not match triggers unless every submitted transition trigger has a match' do
#     submitted = create_transition('State1', 'State2', [create_trigger('trig1'), create_trigger('trig2')], nil, nil)
#     correct = create_transition('State1', 'State2', [create_trigger('trig1 | trig3')], nil, nil)
#     diff = UmlTools::StateDiff.new(create_mock_model, create_mock_model)
#     result = diff.do_triggers_match?(submitted, correct)
#     assert(!result, 'The triggers should not have matched since only one of two triggers had a match')
#   end
# end
