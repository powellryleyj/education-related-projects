#!/usr/bin/env ruby
# test_pdiff: testing pdiff utility

require_relative 'test_base'

class TestDiff < Test::Unit::TestCase
  # test running from command line
  def test_pdiff_main
    old_stdout   = $stdout

    $stdout = (help_output = StringIO.new)
    help_return = UmlTools.pdiff_main %w(--help)

    # note: following test sets options that will have to be unset
    $stdout = (nodiff_output = StringIO.new)
    nodiff_return = UmlTools.pdiff_main ['-d', File.join($comparisons_directory, 'bookreader-soln.mdl'),
                                File.join($comparisons_directory, 'bookreader-soln.mdl')]
    assert $debug_pdiff
    # reset options
    $debug_pdiff = false
    assert !$debug_pdiff

    $stdout = (cmp_output = StringIO.new)
    cmp_return = UmlTools.pdiff_main [File.join($comparisons_directory, 'bookreader-soln.mdl'),
                             File.join($comparisons_directory, 'bookreader-miss1.mdl')]

    $stdout = (badsoln_output = StringIO.new)
    petal_file = File.join($lib_directory, 'model.rb')
    badsoln_return = UmlTools.pdiff_main [petal_file,
                                 File.join($comparisons_directory, 'bookreader-soln.mdl')]
    $stdout = (badsub_output = StringIO.new)
    badsub_return = UmlTools.pdiff_main [File.join($comparisons_directory, 'bookreader-soln.mdl'),
                                petal_file]

    assert !UmlTools::Issue.echo?
    $stdout = (badsoln_issue_output = StringIO.new)
    badsoln_issue_return =
      UmlTools.pdiff_main ['-i', petal_file,
                  File.join($comparisons_directory, 'bookreader-soln.mdl')]
    $stdout = (badsub_issue_output = StringIO.new)
    badsub_issue_return =
      UmlTools.pdiff_main ['-i', File.join($comparisons_directory, 'bookreader-soln.mdl'),
                  petal_file]
    assert UmlTools::Issue.echo?
    # reset options
    UmlTools::Issue.set_echo_ids(false)
    assert !UmlTools::Issue.echo?

#     $stdout = (error_output = StringIO.new)
#     error_return = UmlTools.pdiff_main ['model.rb']

    $stdout = old_stdout

    assert_equal 1, help_return
    assert_equal 'Usage:', help_output.string.split[0]

    assert_equal 0, nodiff_return
    assert_equal 'No differences identified, but be sure to review the model carefully',
                 nodiff_output.string.chomp

    assert_equal 0, cmp_return
    assert_equal 'Association', cmp_output.string.split[0]

    assert_equal 3, badsoln_return

    assert_equal "Error: official solution (#{petal_file}) does not appear to contain a UML model; get help",
                 badsoln_output.string.chomp

    assert_equal 1, badsub_return
    assert_equal "Error: submitted solution (#{petal_file}) does not appear to contain a UML model",
                 badsub_output.string.chomp

    assert_equal 3, badsoln_issue_return
    assert_equal "BAD_MODEL.pdiff_solution_error.Error: official solution (#{petal_file}) does not appear to contain a UML model; get help",
                 badsoln_issue_output.string.chomp

    assert_equal 1, badsub_issue_return
    assert_equal "BAD_MODEL.submission_petal_error.Error: submitted solution (#{petal_file}) does not appear to contain a UML model",
                 badsub_issue_output.string.chomp

#     assert_equal 'Error: model.rb does not appear to contain a UML model',
#                  error_output.chomp
#     assert_equal 2, error_return
  end

  # test running from command line with command line arguments
  def test_diff_cmd_line_arguments
    old_stdout   = $stdout

    # try to load non-checklist; -e and -v coverage along for the ride
    #   (that is, check explicit_errors before and after to confirm
    #   that specifying -e changes it)
    assert !$explicit_errors
    $stdout = (chklist_output = StringIO.new)
    petal_file = File.join($lib_directory,'umlTools', 'model', 'model.rb')
    chklist_return = UmlTools.pdiff_main ['-e', '-v', '-c', petal_file,
                                 File.join($comparisons_directory, 'bookreader-soln.mdl'),
                                 File.join($comparisons_directory, 'bookreader-miss1.mdl')]
    assert $explicit_errors
    $explicit_errors = false

    $stdout = old_stdout

    assert_equal 2, chklist_return
    expected = "Error: checklist file #{petal_file} has invalid format"
    actual = chklist_output.string.chomp
    assert_equal expected, actual

    $stdout = (chklist_output = StringIO.new)
    chklist_return = UmlTools.pdiff_main ['-e', '-c', 'NO_SUCH_FILE',
                                 File.join($comparisons_directory, 'bookreader-soln.mdl'),
                                 File.join($comparisons_directory, 'bookreader-miss1.mdl')]

    $stdout = old_stdout

    assert_equal 2, chklist_return
    expected = 'Error: No such checklist file: NO_SUCH_FILE'
    actual = chklist_output.string.chomp
    assert_equal expected, actual
  end

  # Test UmlTools.pdiff_main
  def test_pdiff_main_state_diagrams
    $explicit_errors = false

    old_stdout = $stdout
    $stdout = (output = StringIO.new)

    result = UmlTools.pdiff_main [File.join($comparisons_directory, 'state-xmi-target.xmi'),
                         File.join($comparisons_directory, 'state-xmi-submitted.xmi')]

    $stdout = old_stdout

    assert(output.string.include?("Extra state(s)"))
    assert(output.string.include?("Missing state(s)"))
    assert(output.string.include?("Transition(s) with incorrect trigger, guard, or effect"))
    assert(output.string.include?("Extra transition(s)"))
    assert(output.string.include?("Extra final node(s)"))
  end

  def test_pdiff_main_state_diagrams_other_errors
    $explicit_errors = false

    old_stdout = $stdout
    $stdout = (output = StringIO.new)

    result = UmlTools.pdiff_main [File.join($comparisons_directory, 'state-xmi-target.xmi'),
                         File.join($comparisons_directory, 'state-xmi-submitted2.xmi')]

    $stdout = old_stdout

    assert(output.string.include?("Missing transition(s)"))
    assert(output.string.include?("Reversed transition(s)"))
  end

  def test_pdiff_main_state_no_errors
    old_stdout = $stdout

    $stdout = (output = StringIO.new)
    UmlTools.pdiff_main [File.join($comparisons_directory, 'state-xmi-target.xmi'),
                File.join($comparisons_directory, 'state-xmi-target.xmi')]

    $stdout = old_stdout

    assert_equal('No differences identified, but be sure to review the model carefully',
                 output.string.chomp)
  end

  def test_pdiff_main_state_and_class
    $explicit_errors = false

    old_stdout = $stdout
    $stdout = (output = StringIO.new)

    result = UmlTools.pdiff_main [File.join($comparisons_directory, 'state-xmi-target.xmi'),
                         File.join($comparisons_directory, 'state-xmi-submitted3.xmi')]

    $stdout = old_stdout

    assert(output.string.include?("Classes that should not be in the diagram: Class1, Class2"))
    assert(output.string.include?("Extra state(s)"))
    assert(output.string.include?("Missing state(s)"))
  end

  # TODO (rwh, 2016): move this test to an appropriate location!!!
  #   (this introduces a dependency on the Problem class in the model)
  # def test_pdiff_add_serialized
  #   old_stdout = $stdout
  #   $stdout = (output = StringIO.new)

  #   p = Problem.first
  #   p.update serialized_model: ''

  #   assert_empty p.serialized_model
  #   result = UmlTools.pdiff_main ['-p', p.id,
  #                                 File.join($comparisons_directory, 'StudentSequenceDiagram.xmi'),
  #                                 File.join($comparisons_directory, 'StudentSequenceDiagram.xmi')]

  #   $stdout = old_stdout

  #   assert_not_empty Problem.find(p.id).serialized_model
  # end
end
