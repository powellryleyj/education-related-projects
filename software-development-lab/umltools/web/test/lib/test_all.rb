#!/usr/bin/env ruby
# all: run all tests for umlint project
#
# usage:
#       ruby test_all.rb
# TODO (rwh, sep 2016): get ruby -w test_all.rb to work

require_relative 'test_sexpr'
require_relative 'test_utils'
require_relative 'class_diagram_parsing_tests/test_method'
require_relative 'class_diagram_parsing_tests/test_m_class'
require_relative 'class_diagram_parsing_tests/test_generalization_relationship'
require_relative 'class_diagram_parsing_tests/test_connector'
require_relative 'class_diagram_parsing_tests/test_logical_model'
require_relative 'class_diagram_parsing_tests/test_attribute'
require_relative 'class_diagram_parsing_tests/test_association'
require_relative 'state_diagram_parsing_tests/test_initial_node'
require_relative 'state_diagram_parsing_tests/test_final_node'
require_relative 'state_diagram_parsing_tests/test_transition'
require_relative 'state_diagram_parsing_tests/test_state'
require_relative 'state_diagram_parsing_tests/test_state_model'
require_relative 'class_diagram_parsing_tests/sequence/test_full_model.rb'
require_relative 'integration_tests/test_ea'
require_relative 'integration_tests/test_full_petal'
require_relative 'integration_tests/test_model'
require_relative 'integration_tests/test_ck_tests'
require_relative 'integration_tests/test_seq_ck_tests'
require_relative 'integration_tests/test_runs'
require_relative 'integration_tests/test_pstats'
require_relative 'compare_tests/test_logical_diff'
require_relative 'compare_tests/test_state_diff'
require_relative 'compare_tests/test_sequence_diff'
require_relative 'compare_tests/test_high_level_diff'
require_relative 'test_diff'

class TestAll < Test::Unit::TestCase
end
