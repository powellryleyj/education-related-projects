#!/usr/bin/env ruby
# test_checks.rb: base class for tests of all checkers

require_relative 'test_base'

module BaseModelTests

  @@min    = DelayedModelFile.new 'min-bad.mdl'
  @@e2     = DelayedModelFile.new 'errors2.mdl'
  @@e3     = DelayedModelFile.new 'errors3.mdl'
  @@e4     = DelayedModelFile.new 'errors4.mdl'
  @@e5     = DelayedModelFile.new 'errors5.mdl'
  @@e6     = DelayedModelFile.new 'errors6.mdl'
  @@e7     = DelayedModelFile.new 'errors7.mdl'
  @@noerr  = DelayedModelFile.new 'non-errors.mdl'
  @@uwpc   = DelayedModelFile.new 'uwpclasses.mdl'
  @@uc_sim = DelayedModelFile.new 'uc-simple.mdl'
  @@uc_e1  = DelayedModelFile.new 'uc-errors1.mdl'
  @@uc_e2  = DelayedModelFile.new 'uc-errors2.mdl'
  @@uc_e3  = DelayedModelFile.new 'uc-errors3.mdl'
  @@clean  = DelayedModelFile.new 'clean.mdl'
  @@xmi_f  = DelayedModelFile.new File.join('xmi', 'feature_test.xmi')

  # test that always succeeds - required by Test::Unit
  def test_base
    m = UmlTools::Check::ModelChecker.new(@@min.mdl)
    assert_equal([], m.check)
  end

  protected

  def assert_equal_issues(expected_text, actual_issues)
    expected_lines = expected_text.split("\n").sort
    actual_lines = actual_issues.map! { |issue| issue.message }.sort
    assert_equal_sequences(expected_lines, actual_lines)
  end

end
