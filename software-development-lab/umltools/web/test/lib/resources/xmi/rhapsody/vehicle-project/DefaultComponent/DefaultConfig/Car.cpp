/********************************************************************
	Rhapsody	: 7.6 
	Login		: CSSE
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Car
//!	Generated Date	: Tue, 1, Jan 2013  
	File Path	: DefaultComponent\DefaultConfig\Car.cpp
*********************************************************************/

//## auto_generated
#include "Car.h"
//## auto_generated
#include "Engine.h"
//## auto_generated
#include "Seat.h"
//## package Default

//## class Car
Car::Car() {
}

Car::~Car() {
}

void Car::change_oil() {
    //#[ operation change_oil()
    //#]
}

string Car::getModel() const {
    return model;
}

void Car::setModel(string p_model) {
    model = p_model;
}

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Car.cpp
*********************************************************************/
