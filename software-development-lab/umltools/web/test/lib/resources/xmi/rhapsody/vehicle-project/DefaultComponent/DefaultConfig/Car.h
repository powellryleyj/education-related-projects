/*********************************************************************
	Rhapsody	: 7.6 
	Login		: CSSE
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Car
//!	Generated Date	: Tue, 1, Jan 2013  
	File Path	: DefaultComponent\DefaultConfig\Car.h
*********************************************************************/

#ifndef Car_H
#define Car_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include "Default.h"
//## class Car
#include "Vehicle.h"
//## auto_generated
class Engine;

//## auto_generated
class Seat;

//## package Default

//## class Car
// Represents a car as a type of vehicle
// Pretty simple: can just change the oil.
class Car : public Vehicle {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    Car();
    
    //## auto_generated
    ~Car();
    
    ////    Operations    ////
    
    //## operation change_oil()
    void change_oil();
    
    ////    Additional operations    ////
    
    //## auto_generated
    string getModel() const;
    
    //## auto_generated
    void setModel(string p_model);
    
    ////    Attributes    ////

protected :

    string model;		//## attribute model
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Car.h
*********************************************************************/
