#!/usr/bin/env ruby
# t1: testing UmlTools::SExpression reading

require_relative '../test_base'

class Test1FullPetal < Test::Unit::TestCase
    include TestBase

  @@min_bad = <<HERE

(object Petal
    version    	50
    _written   	"Rose 2006.0.0.060314"
    charSet    	0)

(object Design "Logical View"
    is_unit    	TRUE
    is_loaded  	TRUE
    quid       	"4B4090840203"
    enforceClosureAutoLoad 	FALSE
    defaults   	(object defaults
	rightMargin 	0.250000
	leftMargin 	0.250000
	topMargin  	0.250000
	bottomMargin 	0.500000
	pageOverlap 	0.250000
	clipIconLabels 	TRUE
	autoResize 	TRUE
	snapToGrid 	TRUE
	gridX      	0
	gridY      	0
	defaultFont 	(object Font
	    size       	10
	    face       	"Arial"
	    bold       	FALSE
	    italics    	FALSE
	    underline  	FALSE
	    strike     	FALSE
	    color      	0
	    default_color 	TRUE)
	showMessageNum 	3
	showClassOfObject 	TRUE
	notation   	"Unified")
    root_usecase_package 	(object Class_Category "Use Case View"
	quid       	"4B4090840205"
	exportControl 	"Public"
	global     	TRUE
	logical_models 	(list unit_reference_list)
	logical_presentations 	(list unit_reference_list
	    (object UseCaseDiagram "Main"
		quid       	"4B40909B0187"
		title      	"Main"
		zoom       	100
		max_height 	28350
		max_width  	21600
		origin_x   	0
		origin_y   	0
		items      	(list diagram_item_list))))
    root_category 	(object Class_Category "Logical View"
	quid       	"4B4090840204"
	exportControl 	"Public"
	global     	TRUE
	subsystem  	"Component View"
	quidu      	"4B4090840206"
	logical_models 	(list unit_reference_list
	    (object Class "BaseClass"
		quid       	"4B4090A90251"
		operations 	(list Operations
		    (object Operation "op1"
			quid       	"4B4090F901A5"
			concurrency 	"Sequential"
			opExportControl 	"Public"
			uid        	0)
		    (object Operation "op2"
			quid       	"4B4090FB003E"
			concurrency 	"Sequential"
			opExportControl 	"Public"
			uid        	0))
		class_attributes 	(list class_attribute_list
		    (object ClassAttribute "num1"
			quid       	"4B4090F1004E")
		    (object ClassAttribute "stuff"
			quid       	"4B40912E000F")))
	    (object Class "SubClass"
		quid       	"4B4090E40222"
		superclasses 	(list inheritance_relationship_list
		    (object Inheritance_Relationship
			quid       	"4B40910101C5"
			supplier   	"Logical View::BaseClass"
			quidu      	"4B4090A90251")
		    (object Inheritance_Relationship
			quid       	"4B409273000F"
			supplier   	"Logical View::AltBase"
			quidu      	"4B4092680280")))
	    (object Class "Stuff"
		quid       	"4B4091100242"
		class_attributes 	(list class_attribute_list
		    (object ClassAttribute "nums[]"
			quid       	"4B4091740000")))
	    (object Class "AltBase"
		quid       	"4B4092680280")
	    (object Association "$UNNAMED$0"
		quid       	"4B4091320213"
		roles      	(list role_list
		    (object Role "$UNNAMED$1"
			quid       	"4B409132034B"
			supplier   	"Logical View::Stuff"
			quidu      	"4B4091100242"
			client_cardinality 	(value cardinality "1")
			Containment 	"By Value"
			is_navigable 	TRUE)
		    (object Role "$UNNAMED$2"
			quid       	"4B409132034D"
			supplier   	"Logical View::BaseClass"
			quidu      	"4B4090A90251"
			client_cardinality 	(value cardinality "*")
			is_aggregate 	TRUE))))
	logical_presentations 	(list unit_reference_list
	    (object ClassDiagram "Main"
		quid       	"4B40909B0196"
		title      	"Main"
		zoom       	100
		max_height 	28350
		max_width  	21600
		origin_x   	0
		origin_y   	38
		items      	(list diagram_item_list
		    (object ClassView "Class" "Logical View::SubClass" @1
			ShowCompartmentStereotypes 	TRUE
			IncludeAttribute 	TRUE
			IncludeOperation 	TRUE
			location   	(301, 653)
			font       	(object Font
			    size       	10
			    face       	"Arial"
			    bold       	FALSE
			    italics    	FALSE
			    underline  	FALSE
			    strike     	FALSE
			    color      	0
			    default_color 	TRUE)
			label      	(object ItemLabel
			    Parent_View 	@1
			    location   	(190, 602)
			    fill_color 	13434879
			    nlines     	1
			    max_width  	222
			    justify    	0
			    label      	"SubClass")
			icon_style 	"Icon"
			line_color 	3342489
			fill_color 	13434879
			quidu      	"4B4090E40222"
			width      	240
			height     	126
			annotation 	8
			autoResize 	TRUE)
		    (object ClassView "Class" "Logical View::Stuff" @2
			ShowCompartmentStereotypes 	TRUE
			IncludeAttribute 	TRUE
			IncludeOperation 	TRUE
			location   	(768, 290)
			font       	(object Font
			    size       	10
			    face       	"Arial"
			    bold       	FALSE
			    italics    	FALSE
			    underline  	FALSE
			    strike     	FALSE
			    color      	0
			    default_color 	TRUE)
			label      	(object ItemLabel
			    Parent_View 	@2
			    location   	(679, 209)
			    fill_color 	13434879
			    nlines     	1
			    max_width  	178
			    justify    	0
			    label      	"Stuff")
			icon_style 	"Icon"
			line_color 	3342489
			fill_color 	13434879
			quidu      	"4B4091100242"
			compartment 	(object Compartment
			    Parent_View 	@2
			    location   	(679, 270)
			    font       	(object Font
				size       	10
				face       	"Arial"
				bold       	FALSE
				italics    	FALSE
				underline  	FALSE
				strike     	FALSE
				color      	0
				default_color 	TRUE)
			    icon_style 	"Icon"
			    fill_color 	13434879
			    anchor     	2
			    nlines     	2
			    max_width  	184
			    justify    	1)
			width      	196
			height     	186
			annotation 	8
			autoResize 	TRUE)
		    (object ClassView "Class" "Logical View::BaseClass" @3
			ShowCompartmentStereotypes 	TRUE
			IncludeAttribute 	TRUE
			IncludeOperation 	TRUE
			location   	(297, 285)
			font       	(object Font
			    size       	10
			    face       	"Arial"
			    bold       	FALSE
			    italics    	FALSE
			    underline  	FALSE
			    strike     	FALSE
			    color      	0
			    default_color 	TRUE)
			label      	(object ItemLabel
			    Parent_View 	@3
			    location   	(176, 129)
			    fill_color 	13434879
			    nlines     	1
			    max_width  	242
			    justify    	0
			    label      	"BaseClass")
			icon_style 	"Icon"
			line_color 	3342489
			fill_color 	13434879
			quidu      	"4B4090A90251"
			compartment 	(object Compartment
			    Parent_View 	@3
			    location   	(176, 190)
			    font       	(object Font
				size       	10
				face       	"Arial"
				bold       	FALSE
				italics    	FALSE
				underline  	FALSE
				strike     	FALSE
				color      	0
				default_color 	TRUE)
			    icon_style 	"Icon"
			    fill_color 	13434879
			    anchor     	2
			    nlines     	5
			    max_width  	159
			    justify    	1)
			width      	260
			height     	336
			annotation 	8
			autoResize 	TRUE)
		    (object InheritView "" @4
			stereotype 	TRUE
			line_color 	3342489
			quidu      	"4B40910101C5"
			client     	@1
			supplier   	@3
			vertices   	(list Points
			    (300, 589)
			    (298, 453))
			line_style 	0)
		    (object AssociationViewNew "$UNNAMED$0" @5
			location   	(548, 287)
			font       	(object Font
			    size       	10
			    face       	"Arial"
			    bold       	FALSE
			    italics    	FALSE
			    underline  	FALSE
			    strike     	FALSE
			    color      	0
			    default_color 	TRUE)
			stereotype 	TRUE
			line_color 	3342489
			quidu      	"4B4091320213"
			roleview_list 	(list RoleViews
			    (object RoleView "$UNNAMED$1" @6
				Parent_View 	@5
				location   	(251, 2)
				font       	(object Font
				    size       	10
				    face       	"Arial"
				    bold       	FALSE
				    italics    	FALSE
				    underline  	FALSE
				    strike     	FALSE
				    color      	0
				    default_color 	TRUE)
				label      	(object SegLabel @7
				    Parent_View 	@6
				    location   	(645, 247)
				    font       	(object Font
					size       	10
					face       	"Arial"
					bold       	FALSE
					italics    	FALSE
					underline  	FALSE
					strike     	FALSE
					color      	0
					default_color 	TRUE)
				    hidden     	TRUE
				    anchor     	1
				    anchor_loc 	1
				    nlines     	1
				    max_width  	60
				    justify    	0
				    label      	""
				    pctDist    	0.800000
				    height     	42
				    orientation 	0)
				stereotype 	TRUE
				line_color 	3342489
				quidu      	"4B409132034B"
				client     	@5
				supplier   	@2
				vertices   	(list Points
				    (548, 287)
				    (669, 288))
				line_style 	0
				label      	(object SegLabel @8
				    Parent_View 	@6
				    location   	(657, 342)
				    font       	(object Font
					size       	10
					face       	"Arial"
					bold       	FALSE
					italics    	FALSE
					underline  	FALSE
					strike     	FALSE
					color      	0
					default_color 	TRUE)
				    anchor     	2
				    anchor_loc 	1
				    nlines     	1
				    max_width  	15
				    justify    	0
				    label      	"1"
				    pctDist    	0.900000
				    height     	54
				    orientation 	1))
			    (object RoleView "$UNNAMED$2" @9
				Parent_View 	@5
				location   	(251, 2)
				font       	(object Font
				    size       	10
				    face       	"Arial"
				    bold       	FALSE
				    italics    	FALSE
				    underline  	FALSE
				    strike     	FALSE
				    color      	0
				    default_color 	TRUE)
				stereotype 	TRUE
				line_color 	3342489
				quidu      	"4B409132034D"
				client     	@5
				supplier   	@3
				vertices   	(list Points
				    (548, 287)
				    (427, 286))
				line_style 	0
				label      	(object SegLabel @10
				    Parent_View 	@9
				    location   	(439, 340)
				    font       	(object Font
					size       	10
					face       	"Arial"
					bold       	FALSE
					italics    	FALSE
					underline  	FALSE
					strike     	FALSE
					color      	0
					default_color 	TRUE)
				    anchor     	2
				    anchor_loc 	1
				    nlines     	1
				    max_width  	15
				    justify    	0
				    label      	"*"
				    pctDist    	0.900000
				    height     	54
				    orientation 	0))))
		    (object ClassView "Class" "Logical View::AltBase" @11
			ShowCompartmentStereotypes 	TRUE
			IncludeAttribute 	TRUE
			IncludeOperation 	TRUE
			location   	(784, 659)
			font       	(object Font
			    size       	10
			    face       	"Arial"
			    bold       	FALSE
			    italics    	FALSE
			    underline  	FALSE
			    strike     	FALSE
			    color      	0
			    default_color 	TRUE)
			label      	(object ItemLabel
			    Parent_View 	@11
			    location   	(695, 608)
			    fill_color 	13434879
			    nlines     	1
			    max_width  	178
			    justify    	0
			    label      	"AltBase")
			icon_style 	"Icon"
			line_color 	3342489
			fill_color 	13434879
			quidu      	"4B4092680280"
			width      	196
			height     	126
			annotation 	8
			autoResize 	TRUE)
		    (object InheritView "" @12
			stereotype 	TRUE
			line_color 	3342489
			quidu      	"4B409273000F"
			client     	@1
			supplier   	@11
			vertices   	(list Points
			    (421, 654)
			    (685, 657))
			line_style 	0)))))
    root_subsystem 	(object SubSystem "Component View"
	quid       	"4B4090840206"
	physical_models 	(list unit_reference_list)
	physical_presentations 	(list unit_reference_list
	    (object Module_Diagram "Main"
		quid       	"4B40909B0186"
		title      	"Main"
		zoom       	100
		max_height 	28350
		max_width  	21600
		origin_x   	0
		origin_y   	0
		items      	(list diagram_item_list)))
	category   	"Logical View"
	quidu      	"4B4090840204")
    process_structure 	(object Processes
	quid       	"4B4090840207"
	ProcsNDevs 	(list
	    (object Process_Diagram "Deployment View"
		quid       	"4B4090840213"
		title      	"Deployment View"
		zoom       	100
		max_height 	28350
		max_width  	21600
		origin_x   	0
		origin_y   	0
		items      	(list diagram_item_list))))
                )
HERE
  @@min_bad_sexpr = UmlTools::SExpr.read(@@min_bad)

  def test_A_load_full_petal
    target = '[[object, Petal, version, 50, _written, Rose 2006.0.0.060314, charSet, 0], [object, Design, Logical View, is_unit, TRUE, is_loaded, TRUE, quid, 4B4090840203, enforceClosureAutoLoad, FALSE, defaults, [object, defaults, rightMargin, 0.250000, leftMargin, 0.250000, topMargin, 0.250000, bottomMargin, 0.500000, pageOverlap, 0.250000, clipIconLabels, TRUE, autoResize, TRUE, snapToGrid, TRUE, gridX, 0, gridY, 0, defaultFont, [object, Font, size, 10, face, Arial, bold, FALSE, italics, FALSE, underline, FALSE, strike, FALSE, color, 0, default_color, TRUE], showMessageNum, 3, showClassOfObject, TRUE, notation, Unified], root_usecase_package, [object, Class_Category, Use Case View, quid, 4B4090840205, exportControl, Public, global, TRUE, logical_models, [list, unit_reference_list], logical_presentations, [list, unit_reference_list, [object, UseCaseDiagram, Main, quid, 4B40909B0187, title, Main, zoom, 100, max_height, 28350, max_width, 21600, origin_x, 0, origin_y, 0, items, [list, diagram_item_list]]]], root_category, [object, Class_Category, Logical View, quid, 4B4090840204, exportControl, Public, global, TRUE, subsystem, Component View, quidu, 4B4090840206, logical_models, [list, unit_reference_list, [object, Class, BaseClass, quid, 4B4090A90251, operations, [list, Operations, [object, Operation, op1, quid, 4B4090F901A5, concurrency, Sequential, opExportControl, Public, uid, 0], [object, Operation, op2, quid, 4B4090FB003E, concurrency, Sequential, opExportControl, Public, uid, 0]], class_attributes, [list, class_attribute_list, [object, ClassAttribute, num1, quid, 4B4090F1004E], [object, ClassAttribute, stuff, quid, 4B40912E000F]]], [object, Class, SubClass, quid, 4B4090E40222, superclasses, [list, inheritance_relationship_list, [object, Inheritance_Relationship, quid, 4B40910101C5, supplier, Logical View::BaseClass, quidu, 4B4090A90251], [object, Inheritance_Relationship, quid, 4B409273000F, supplier, Logical View::AltBase, quidu, 4B4092680280]]], [object, Class, Stuff, quid, 4B4091100242, class_attributes, [list, class_attribute_list, [object, ClassAttribute, nums[], quid, 4B4091740000]]], [object, Class, AltBase, quid, 4B4092680280], [object, Association, $UNNAMED$0, quid, 4B4091320213, roles, [list, role_list, [object, Role, $UNNAMED$1, quid, 4B409132034B, supplier, Logical View::Stuff, quidu, 4B4091100242, client_cardinality, [value, cardinality, 1], Containment, By Value, is_navigable, TRUE], [object, Role, $UNNAMED$2, quid, 4B409132034D, supplier, Logical View::BaseClass, quidu, 4B4090A90251, client_cardinality, [value, cardinality, *], is_aggregate, TRUE]]]], logical_presentations, [list, unit_reference_list, [object, ClassDiagram, Main, quid, 4B40909B0196, title, Main, zoom, 100, max_height, 28350, max_width, 21600, origin_x, 0, origin_y, 38, items, [list, diagram_item_list, [object, ClassView, Class, Logical View::SubClass, @1, ShowCompartmentStereotypes, TRUE, IncludeAttribute, TRUE, IncludeOperation, TRUE, location, [301,, 653], font, [object, Font, size, 10, face, Arial, bold, FALSE, italics, FALSE, underline, FALSE, strike, FALSE, color, 0, default_color, TRUE], label, [object, ItemLabel, Parent_View, @1, location, [190,, 602], fill_color, 13434879, nlines, 1, max_width, 222, justify, 0, label, SubClass], icon_style, Icon, line_color, 3342489, fill_color, 13434879, quidu, 4B4090E40222, width, 240, height, 126, annotation, 8, autoResize, TRUE], [object, ClassView, Class, Logical View::Stuff, @2, ShowCompartmentStereotypes, TRUE, IncludeAttribute, TRUE, IncludeOperation, TRUE, location, [768,, 290], font, [object, Font, size, 10, face, Arial, bold, FALSE, italics, FALSE, underline, FALSE, strike, FALSE, color, 0, default_color, TRUE], label, [object, ItemLabel, Parent_View, @2, location, [679,, 209], fill_color, 13434879, nlines, 1, max_width, 178, justify, 0, label, Stuff], icon_style, Icon, line_color, 3342489, fill_color, 13434879, quidu, 4B4091100242, compartment, [object, Compartment, Parent_View, @2, location, [679,, 270], font, [object, Font, size, 10, face, Arial, bold, FALSE, italics, FALSE, underline, FALSE, strike, FALSE, color, 0, default_color, TRUE], icon_style, Icon, fill_color, 13434879, anchor, 2, nlines, 2, max_width, 184, justify, 1], width, 196, height, 186, annotation, 8, autoResize, TRUE], [object, ClassView, Class, Logical View::BaseClass, @3, ShowCompartmentStereotypes, TRUE, IncludeAttribute, TRUE, IncludeOperation, TRUE, location, [297,, 285], font, [object, Font, size, 10, face, Arial, bold, FALSE, italics, FALSE, underline, FALSE, strike, FALSE, color, 0, default_color, TRUE], label, [object, ItemLabel, Parent_View, @3, location, [176,, 129], fill_color, 13434879, nlines, 1, max_width, 242, justify, 0, label, BaseClass], icon_style, Icon, line_color, 3342489, fill_color, 13434879, quidu, 4B4090A90251, compartment, [object, Compartment, Parent_View, @3, location, [176,, 190], font, [object, Font, size, 10, face, Arial, bold, FALSE, italics, FALSE, underline, FALSE, strike, FALSE, color, 0, default_color, TRUE], icon_style, Icon, fill_color, 13434879, anchor, 2, nlines, 5, max_width, 159, justify, 1], width, 260, height, 336, annotation, 8, autoResize, TRUE], [object, InheritView, , @4, stereotype, TRUE, line_color, 3342489, quidu, 4B40910101C5, client, @1, supplier, @3, vertices, [list, Points, [300,, 589], [298,, 453]], line_style, 0], [object, AssociationViewNew, $UNNAMED$0, @5, location, [548,, 287], font, [object, Font, size, 10, face, Arial, bold, FALSE, italics, FALSE, underline, FALSE, strike, FALSE, color, 0, default_color, TRUE], stereotype, TRUE, line_color, 3342489, quidu, 4B4091320213, roleview_list, [list, RoleViews, [object, RoleView, $UNNAMED$1, @6, Parent_View, @5, location, [251,, 2], font, [object, Font, size, 10, face, Arial, bold, FALSE, italics, FALSE, underline, FALSE, strike, FALSE, color, 0, default_color, TRUE], label, [object, SegLabel, @7, Parent_View, @6, location, [645,, 247], font, [object, Font, size, 10, face, Arial, bold, FALSE, italics, FALSE, underline, FALSE, strike, FALSE, color, 0, default_color, TRUE], hidden, TRUE, anchor, 1, anchor_loc, 1, nlines, 1, max_width, 60, justify, 0, label, , pctDist, 0.800000, height, 42, orientation, 0], stereotype, TRUE, line_color, 3342489, quidu, 4B409132034B, client, @5, supplier, @2, vertices, [list, Points, [548,, 287], [669,, 288]], line_style, 0, label, [object, SegLabel, @8, Parent_View, @6, location, [657,, 342], font, [object, Font, size, 10, face, Arial, bold, FALSE, italics, FALSE, underline, FALSE, strike, FALSE, color, 0, default_color, TRUE], anchor, 2, anchor_loc, 1, nlines, 1, max_width, 15, justify, 0, label, 1, pctDist, 0.900000, height, 54, orientation, 1]], [object, RoleView, $UNNAMED$2, @9, Parent_View, @5, location, [251,, 2], font, [object, Font, size, 10, face, Arial, bold, FALSE, italics, FALSE, underline, FALSE, strike, FALSE, color, 0, default_color, TRUE], stereotype, TRUE, line_color, 3342489, quidu, 4B409132034D, client, @5, supplier, @3, vertices, [list, Points, [548,, 287], [427,, 286]], line_style, 0, label, [object, SegLabel, @10, Parent_View, @9, location, [439,, 340], font, [object, Font, size, 10, face, Arial, bold, FALSE, italics, FALSE, underline, FALSE, strike, FALSE, color, 0, default_color, TRUE], anchor, 2, anchor_loc, 1, nlines, 1, max_width, 15, justify, 0, label, *, pctDist, 0.900000, height, 54, orientation, 0]]]], [object, ClassView, Class, Logical View::AltBase, @11, ShowCompartmentStereotypes, TRUE, IncludeAttribute, TRUE, IncludeOperation, TRUE, location, [784,, 659], font, [object, Font, size, 10, face, Arial, bold, FALSE, italics, FALSE, underline, FALSE, strike, FALSE, color, 0, default_color, TRUE], label, [object, ItemLabel, Parent_View, @11, location, [695,, 608], fill_color, 13434879, nlines, 1, max_width, 178, justify, 0, label, AltBase], icon_style, Icon, line_color, 3342489, fill_color, 13434879, quidu, 4B4092680280, width, 196, height, 126, annotation, 8, autoResize, TRUE], [object, InheritView, , @12, stereotype, TRUE, line_color, 3342489, quidu, 4B409273000F, client, @1, supplier, @11, vertices, [list, Points, [421,, 654], [685,, 657]], line_style, 0]]]]], root_subsystem, [object, SubSystem, Component View, quid, 4B4090840206, physical_models, [list, unit_reference_list], physical_presentations, [list, unit_reference_list, [object, Module_Diagram, Main, quid, 4B40909B0186, title, Main, zoom, 100, max_height, 28350, max_width, 21600, origin_x, 0, origin_y, 0, items, [list, diagram_item_list]]], category, Logical View, quidu, 4B4090840204], process_structure, [object, Processes, quid, 4B4090840207, ProcsNDevs, [list, [object, Process_Diagram, Deployment View, quid, 4B4090840213, title, Deployment View, zoom, 100, max_height, 28350, max_width, 21600, origin_x, 0, origin_y, 0, items, [list, diagram_item_list]]]]]]'
    assert_equal(target, @@min_bad_sexpr.to_s)
  end

  def test_B_find_by_path
    logical_presentations = @@min_bad_sexpr.second.find_by_path %w(root_category logical_presentations)
    views = logical_presentations.select { |it|
      it.matches?('object', 'ClassView')
    }
    assert_equal(4, views.size)
    view_objs = logical_presentations.select_all { |it|
      it.list? && it.head.name == 'object' && it.second.name =~ /View/
    }
    assert_equal(9, view_objs.size)
    objs = logical_presentations.select_all { |it|
      it.list? && it.head.name == 'object'
    }
    objs = objs.map { |it| it.third.name || it.second.name }
    assert_equal(['Main', 'Class', 'size',
                  'Parent_View', 'Class', 'size', 'Parent_View',
                  'Parent_View', 'size', 'Class', 'size', 'Parent_View',
                  'Parent_View', 'size', '', '$UNNAMED$0',
                  'size', '$UNNAMED$1', 'size', '@7', 'size', '@8', 'size',
                  '$UNNAMED$2', 'size', '@10', 'size', 'Class', 'size',
                  'Parent_View', ''],
                 objs)
  end # test_B_find_by_path

  def test_C_find_all_by_path
    uc_objects = @@min_bad_sexpr.second.find_all_by_path %w(root_usecase_package object)
    uc_names = uc_objects.map { |x| x.head }
    assert_equal(%w(Class_Category UseCaseDiagram), uc_names)

    cd_refs = @@min_bad_sexpr.second.find_all_by_path %w(root_category unit_reference_list)
    cd_names = cd_refs.map { |x| x.third.head }
    assert_equal(%w(BaseClass Main), cd_names)

    cd_refs = @@min_bad_sexpr.second.find_all_by_path %w(root_category logical_presentations ClassView)
    assert_equal(4, cd_refs.size)
  end # test_C_find_all_by_path

  def test_D_sexpr_coverage
    s = UmlTools::SExpr.read('(a b c)')
    assert(!s.is?('x'))
    assert(!s.empty?)
    assert_equal('[a, b, c]', s.to_s)

    a = UmlTools::SExpr.read('a')
    assert_equal('a', a.to_s)
    selected = a.select_all { |x| true }
    assert_equal(%w(a), selected.map { |x| x.to_s })

    assert_nil(UmlTools::SExpr.read(''))
    empty_list = UmlTools::SExpr.read('()')
    assert(empty_list.empty?)
  end

  def test_E_multiline
    text = <<HERE
(object UseCase "Modify Working Schedule"
 quid       	"433DAB2101B5"
 documentation
|Adding sections, removing sections, and highlighting conflicts.
| Scenario A:
|
|
)
HERE
    s = UmlTools::SExpr.read(text)
    doctext = <<HERE
Adding sections, removing sections, and highlighting conflicts.
 Scenario A:

HERE
    assert_equal("[object, UseCase, Modify Working Schedule, quid, 433DAB2101B5, documentation, Adding sections, removing sections, and highlighting conflicts.\n Scenario A:\n\n]",
                 s.to_s)
  end

end
