#!/usr/bin/env ruby
# t2: reading basic logical model data from a model file

require_relative '../test_checks'

class TestPetalRead < Test::Unit::TestCase
  include BaseModelTests
  include TestBase

  def setup
    # ensure not printing ids when dumping unless I want them
    UmlTools::Model::Element.print_ids = false
    UmlTools::Model::Element.print_just_ids = false
  end

  def teardown
    # nothing
  end

  def test_A_dump1
    x = <<HERE
(object Petal
    version    	50
    _written   	"Rose 2006.0.0.060314"
    charSet    	0)
(object Design "Logical View"
    root_category 	(object Class_Category "Logical View"
	quid       	"4B474C15011A"
	exportControl 	"Public"
	quidu      	"4B474C15011C"
	logical_models 	(list unit_reference_list
	    (object Class "SomeClass"
		quid       	"4B474C360157"
		operations 	(list Operations
		    (object Operation "find key"
			quid       	"4B474D480186"
			opExportControl 	"Protected"
			uid        	0))
		class_attributes 	(list class_attribute_list
		    (object ClassAttribute "that stuff"
			quid       	"4B474D35008C"
                        exportControl 	"Public"
                        type            "Double")))
	    (object Class "short"
		quid       	"4B474DB50232")
	    (object Association "$UNNAMED$"
		quid       	"4B474DCC0261"
		roles      	(list role_list
		    (object Role "try"
			quid       	"4B474DCD00AB"
			label      	"try"
			supplier   	"Logical View::short"
			quidu      	"4B474DB50232"
			is_navigable 	TRUE)
		    (object Role "theSomeClass"
			quid       	"4B474DCD00AD"
			label      	"theSomeClass"
			supplier   	"Logical View::SomeClass"
			quidu      	"4B474C360157"
			client_cardinality 	(value cardinality "0..*")))))
        )
    )
HERE
    target = <<HERE
(LogicalModel
  (MClass SomeClass (attributes (attr (. SomeClass that stuff) (type Double) public)) (methods (elt (. SomeClass find key) protected)) hidden)
  (MClass short hidden)
  (association (role short (name try) navigable plain (assoc try theSomeClass)) (role SomeClass (name theSomeClass) 0..* plain (assoc try theSomeClass)) hidden)
)
HERE
    s = UmlTools::SExpr.read(x)
    m = UmlTools::Model::LogicalModel.read(s)
    assert_equal(target.chomp, m.dump.chomp)
    assert_equal(['find key'], m.all_methods.map { |m| m.name })
  end

  def test_B_check_association
    x = <<HERE
(object Petal
    version    	50
    _written   	"Rose 2006.0.0.060314"
    charSet    	0)
(object Design "Logical View"
    root_category 	(object Class_Category "Logical View"
	quid       	"4B474C15011A"
	exportControl 	"Public"
	quidu      	"4B474C15011C"
	logical_models 	(list unit_reference_list
	    (object Class "SomeClass"
		quid       	"4B474C360157"
 	    )
	    (object Class "short"
		quid       	"4B474DB50232")
	    (object Association "this_association"
		quid       	"4B474DCC0261"
		roles      	(list role_list
		    (object Role "try"
			quid       	"4B474DCD00AB"
			label      	"try"
			supplier   	"Logical View::short"
			quidu      	"4B474DB50232"
			is_navigable 	TRUE)
		    (object Role "theSomeClass"
			quid       	"4B474DCD00AD"
			label      	"theSomeClass"
			supplier   	"Logical View::SomeClass"
			quidu      	"4B474C360157"
			client_cardinality 	(value cardinality "0..*")))))
        )
    )
HERE
    target = '(association this_association (role short (name try) navigable plain (assoc this_association try theSomeClass)) (role SomeClass (name theSomeClass) 0..* plain (assoc this_association try theSomeClass)) hidden)'
    s = UmlTools::SExpr.read(x)
    m = UmlTools::Model::LogicalModel.read(s)
    assert_equal(1, m.associations.size)
    assert_equal(target, m.associations[0].dump)
    assert_equal('try/theSomeClass', m.associations[0].displayed_name)

    elts = m.all_named_elements
    elt_names = elts.map { |e| e.name }
    elt_names.sort!
    assert_equal('SomeClass,short,theSomeClass,this_association,try',
                 elt_names.join(','))
  end

  def test_B2_check_containment
    composite_example = <<HERE
(object Petal
    version    	50
    _written   	"Rose 2006.0.0.060314"
    charSet    	0)
(object Design "Logical View"
    root_category 	(object Class_Category "Logical View"
	quid       	"4B474C15011A"
	exportControl 	"Public"
	quidu      	"4B474C15011C"
	logical_models 	(list unit_reference_list
	    (object Class "SomeClass"
		quid       	"4B474C360157"
 	    )
	    (object Class "short"
		quid       	"4B474DB50232")
	    (object Association "this_association"
		quid       	"4B474DCC0261"
		roles      	(list role_list
		    (object Role "try"
			quid       	"4B474DCD00AB"
			label      	"try"
			supplier   	"Logical View::short"
			quidu      	"4B474DB50232"
			is_navigable 	TRUE
                        is_aggregate    TRUE)
		    (object Role "theSomeClass"
			quid       	"4B474DCD00AD"
			label      	"theSomeClass"
			supplier   	"Logical View::SomeClass"
			quidu      	"4B474C360157"
			client_cardinality 	(value cardinality "0..*")
			Containment 	"By Value"
                    )
            )))
        )
    )
HERE
    target = '(association this_association (role short (name try) navigable composite (assoc this_association try theSomeClass)) (role SomeClass (name theSomeClass) 0..* plain (assoc this_association try theSomeClass)) hidden)'
    s = UmlTools::SExpr.read(composite_example)
    m = UmlTools::Model::LogicalModel.read(s)
    assert_equal(1, m.associations.size)
    assert_equal(target, m.associations[0].dump)

    aggregate_example = <<HERE
(object Petal
    version    	50
    _written   	"Rose 2006.0.0.060314"
    charSet    	0)
(object Design "Logical View"
    root_category 	(object Class_Category "Logical View"
	quid       	"4B474C15011A"
	exportControl 	"Public"
	quidu      	"4B474C15011C"
	logical_models 	(list unit_reference_list
	    (object Class "SomeClass"
		quid       	"4B474C360157"
 	    )
	    (object Class "short"
		quid       	"4B474DB50232")
	    (object Association "this_association"
		quid       	"4B474DCC0261"
		roles      	(list role_list
		    (object Role "try"
			quid       	"4B474DCD00AB"
			label      	"try"
			supplier   	"Logical View::short"
			quidu      	"4B474DB50232"
			is_navigable 	TRUE
			Containment 	"By Reference")
		    (object Role "theSomeClass"
			quid       	"4B474DCD00AD"
			label      	"theSomeClass"
			supplier   	"Logical View::SomeClass"
			quidu      	"4B474C360157"
			client_cardinality 	(value cardinality "0..*")
                        is_aggregate    TRUE
                        )
                )))
        )
    )
HERE
    target = '(association this_association (role short (name try) navigable plain (assoc this_association try theSomeClass)) (role SomeClass (name theSomeClass) 0..* aggregate (assoc this_association try theSomeClass)) hidden)'
    s = UmlTools::SExpr.read(aggregate_example)
    m = UmlTools::Model::LogicalModel.read(s)
    assert_equal(1, m.associations.size)
    assert_equal(target, m.associations[0].dump)
  end

  def test_C_check_use_cases
    x = <<HERE

(object Petal
    version    	50
    _written   	"Rose 2006.0.0.060314"
    charSet    	0)

(object Design "Logical View"
    is_unit    	TRUE
    is_loaded  	TRUE
    attributes 	(list Attribute_Set
	(object Attribute
	    tool       	"Java"
	    name       	"IDE"
	    value      	"Internal Editor")
	(object Attribute
	    tool       	"Java"
	    name       	"UserDefineTagName1"
	    value      	"")
	(object Attribute
	    tool       	"Java"
	    name       	"UserDefineTagText1"
	    value      	"")
	(object Attribute
	    tool       	"Java"
	    name       	"UserDefineTagApply1"
	    value      	"")
	(object Attribute
	    tool       	"Java"
	    name       	"UserDefineTagName2"
	    value      	"")
	(object Attribute
	    tool       	"Java"
	    name       	"UserDefineTagText2"
	    value      	"")
	(object Attribute
	    tool       	"Java"
	    name       	"UserDefineTagApply2"
	    value      	"")
	(object Attribute
	    tool       	"Java"
	    name       	"UserDefineTagName3"
	    value      	"")
	(object Attribute
	    tool       	"Java"
	    name       	"UserDefineTagText3"
	    value      	"")
	(object Attribute
	    tool       	"Java"
	    name       	"UserDefineTagApply3"
	    value      	""))
    quid       	"433DAAD100BB"
    enforceClosureAutoLoad 	FALSE
    defaults   	(object defaults
	rightMargin 	0.250000
	leftMargin 	0.250000
	topMargin  	0.250000
	bottomMargin 	0.500000
	pageOverlap 	0.250000
	clipIconLabels 	TRUE
	autoResize 	TRUE
	snapToGrid 	TRUE
	gridX      	0
	gridY      	0
	defaultFont 	(object Font
	    size       	10
	    face       	"Arial"
	    bold       	FALSE
	    italics    	FALSE
	    underline  	FALSE
	    strike     	FALSE
	    color      	0
	    default_color 	TRUE)
	showMessageNum 	3
	showClassOfObject 	TRUE
	notation   	"Unified")
    root_usecase_package 	(object Class_Category "Use Case View"
	quid       	"433DAAD100BD"
	exportControl 	"Public"
	global     	TRUE
	logical_models 	(list unit_reference_list
	    (object Class "User"
		quid       	"433DAB9602CE"
		stereotype 	"Actor")
	    (object UseCase "Modify Working Schedule"
		quid       	"433DAB2101B5"
		documentation
|Adding sections, removing sections, and highlighting conflicts.
| Scenario A:
|
|
		)
	    (object UseCase "Print"
		quid       	"433DAB2B0251"
		documentation
|Printing the schedule.
		)
	    (object UseCase "Control View"
		quid       	"433DAB3100BB"
		documentation
|Actions which control the display such as suppressing closed sections or change window positions.
|
|[Scenarios not yet written for this use case.]
		)
	    (object UseCase "View Weekly Schedule"
		quid       	"433DAB6901F4"
		documentation
|Weekly class schedule display.
|
|Scenario A:
|
|   1. User is viewing a working schedule.
|   2. User selects viewing the weekly schedule from the menus.
|   3. System displays the schedule for Monday through Friday with classes for each hour shown in a separate cell.
|   4. User moves schedule to a different part of the screen to allow viewing the main scheduling window.
		)
	    (object UseCase "Times"
		quid       	"4C33CD31004E")
	    (object Association "$UNNAMED$0"
		quid       	"433DAB9D029F"
		roles      	(list role_list
		    (object Role "$UNNAMED$1"
			quid       	"433DAB9E0119"
			supplier   	"Use Case View::User"
			quidu      	"433DAB9602CE"
			is_navigable 	TRUE)
		    (object Role "$UNNAMED$2"
			quid       	"433DAB9E011B"
			supplier   	"Use Case View::Control View"
			quidu      	"433DAB3100BB"
			is_navigable 	TRUE)))
	    (object Association "$UNNAMED$3"
		quid       	"433DAB9F03B9"
		roles      	(list role_list
		    (object Role "$UNNAMED$4"
			quid       	"433DABA001D4"
			supplier   	"Use Case View::User"
			quidu      	"433DAB9602CE"
			is_navigable 	TRUE)
		    (object Role "$UNNAMED$5"
			quid       	"433DABA001E4"
			supplier   	"Use Case View::Modify Working Schedule"
			quidu      	"433DAB2101B5"
			is_navigable 	TRUE)))
	    (object Association "$UNNAMED$6"
		quid       	"433DABA602BF"
		roles      	(list role_list
		    (object Role "$UNNAMED$7"
			quid       	"433DABA701F4"
			supplier   	"Use Case View::User"
			quidu      	"433DAB9602CE"
			is_navigable 	TRUE)
		    (object Role "$UNNAMED$8"
			quid       	"433DABA701F6"
			supplier   	"Use Case View::View Weekly Schedule"
			quidu      	"433DAB6901F4"
			is_navigable 	TRUE)))
	    (object Association "$UNNAMED$9"
		quid       	"433DABAA01F4"
		roles      	(list role_list
		    (object Role "$UNNAMED$10"
			quid       	"433DABAB0138"
			supplier   	"Use Case View::User"
			quidu      	"433DAB9602CE"
			is_navigable 	TRUE)
		    (object Role "$UNNAMED$11"
			quid       	"433DABAB013A"
			supplier   	"Use Case View::Print"
			quidu      	"433DAB2B0251"
			is_navigable 	TRUE)))
	    (object Association "$UNNAMED$12"
		quid       	"4C33CDCF033C"
		roles      	(list role_list
		    (object Role "$UNNAMED$13"
			quid       	"4C33CDD00196"
			supplier   	"Use Case View::Times"
			quidu      	"4C33CD31004E"
			is_navigable 	TRUE)
		    (object Role "$UNNAMED$14"
			quid       	"4C33CDD001A5"
			supplier   	"Use Case View::User"
			quidu      	"433DAB9602CE")))
	    (object Association "$UNNAMED$15"
		quid       	"4C33CDE4036B"
		roles      	(list role_list
		    (object Role "$UNNAMED$16"
			quid       	"4C33CDE50196"
			supplier   	"Use Case View::User"
			quidu      	"433DAB9602CE"
			is_navigable 	TRUE)
		    (object Role "$UNNAMED$17"
			quid       	"4C33CDE50198"
			supplier   	"Use Case View::View Weekly Schedule"
			quidu      	"433DAB6901F4"))))
	logical_presentations 	(list unit_reference_list
	    (object UseCaseDiagram "Main"
		quid       	"433DAADB0242"
		title      	"Main"
		zoom       	100
		max_height 	28350
		max_width  	21600
		origin_x   	0
		origin_y   	0
		items      	(list diagram_item_list
		    (object UseCaseView "Use Case View::Control View" @1
			location   	(643, 134)
			font       	(object Font
			    size       	10
			    face       	"Arial"
			    bold       	FALSE
			    italics    	FALSE
			    underline  	FALSE
			    strike     	FALSE
			    color      	0
			    default_color 	TRUE)
			label      	(object ItemLabel
			    Parent_View 	@1
			    location   	(643, 272)
			    anchor_loc 	1
			    nlines     	2
			    max_width  	630
			    justify    	0
			    label      	"Control View")
			icon_style 	"Icon"
			line_color 	3342489
			fill_color 	13434879
			quidu      	"433DAB3100BB"
			height     	118)
		    (object UseCaseView "Use Case View::Modify Working Schedule" @2
			location   	(818, 387)
			font       	(object Font
			    size       	10
			    face       	"Arial"
			    bold       	FALSE
			    italics    	FALSE
			    underline  	FALSE
			    strike     	FALSE
			    color      	0
			    default_color 	TRUE)
			label      	(object ItemLabel
			    Parent_View 	@2
			    location   	(818, 525)
			    anchor_loc 	1
			    nlines     	2
			    max_width  	630
			    justify    	0
			    label      	"Modify Working Schedule")
			icon_style 	"Icon"
			line_color 	3342489
			fill_color 	13434879
			quidu      	"433DAB2101B5"
			height     	118)
		    (object UseCaseView "Use Case View::Print" @3
			location   	(700, 909)
			font       	(object Font
			    size       	10
			    face       	"Arial"
			    bold       	FALSE
			    italics    	FALSE
			    underline  	FALSE
			    strike     	FALSE
			    color      	0
			    default_color 	TRUE)
			label      	(object ItemLabel
			    Parent_View 	@3
			    location   	(700, 1047)
			    anchor_loc 	1
			    nlines     	2
			    max_width  	630
			    justify    	0
			    label      	"Print")
			icon_style 	"Icon"
			line_color 	3342489
			fill_color 	13434879
			quidu      	"433DAB2B0251"
			height     	118)
		    (object UseCaseView "Use Case View::Times" @4
			location   	(450, 1050)
			font       	(object Font
			    size       	10
			    face       	"Arial"
			    bold       	FALSE
			    italics    	FALSE
			    underline  	FALSE
			    strike     	FALSE
			    color      	0
			    default_color 	TRUE)
			label      	(object ItemLabel
			    Parent_View 	@4
			    location   	(450, 1188)
			    anchor_loc 	1
			    nlines     	2
			    max_width  	630
			    justify    	0
			    label      	"Times")
			icon_style 	"Icon"
			line_color 	3342489
			fill_color 	13434879
			quidu      	"4C33CD31004E"
			height     	118)
		    (object UseCaseView "Use Case View::View Weekly Schedule" @5
			location   	(812, 650)
			font       	(object Font
			    size       	10
			    face       	"Arial"
			    bold       	FALSE
			    italics    	FALSE
			    underline  	FALSE
			    strike     	FALSE
			    color      	0
			    default_color 	TRUE)
			label      	(object ItemLabel
			    Parent_View 	@5
			    location   	(812, 788)
			    anchor_loc 	1
			    nlines     	2
			    max_width  	630
			    justify    	0
			    label      	"View Weekly Schedule")
			icon_style 	"Icon"
			line_color 	3342489
			fill_color 	13434879
			quidu      	"433DAB6901F4"
			height     	118)
		    (object ClassView "Class" "Use Case View::User" @6
			ShowCompartmentStereotypes 	TRUE
			IncludeAttribute 	TRUE
			IncludeOperation 	TRUE
			location   	(189, 634)
			font       	(object Font
			    size       	10
			    face       	"Arial"
			    bold       	FALSE
			    italics    	FALSE
			    underline  	FALSE
			    strike     	FALSE
			    color      	0
			    default_color 	TRUE)
			label      	(object ItemLabel
			    Parent_View 	@6
			    location   	(189, 794)
			    anchor_loc 	1
			    nlines     	2
			    max_width  	325
			    justify    	0
			    label      	"User")
			icon       	"Actor"
			icon_style 	"Icon"
			line_color 	3342489
			quidu      	"433DAB9602CE"
			annotation 	8
			autoResize 	TRUE)
		    (object AssociationViewNew "$UNNAMED$0" @7
			location   	(384, 416)
			stereotype 	TRUE
			line_color 	3342489
			quidu      	"433DAB9D029F"
			roleview_list 	(list RoleViews
			    (object RoleView "$UNNAMED$1" @8
				Parent_View 	@7
				location   	(-188, 248)
				stereotype 	TRUE
				line_color 	3342489
				quidu      	"433DAB9E0119"
				client     	@7
				supplier   	@6
				vertices   	(list Points
				    (384, 416)
				    (246, 567))
				line_style 	0)
			    (object RoleView "$UNNAMED$2" @9
				Parent_View 	@7
				location   	(-188, 248)
				stereotype 	TRUE
				line_color 	3342489
				quidu      	"433DAB9E011B"
				client     	@7
				supplier   	@1
				vertices   	(list Points
				    (384, 416)
				    (523, 265))
				line_style 	0)))
		    (object AssociationViewNew "$UNNAMED$3" @10
			location   	(413, 542)
			stereotype 	TRUE
			line_color 	3342489
			quidu      	"433DAB9F03B9"
			roleview_list 	(list RoleViews
			    (object RoleView "$UNNAMED$4" @11
				Parent_View 	@10
				location   	(-165, 83)
				stereotype 	TRUE
				line_color 	3342489
				quidu      	"433DABA001D4"
				client     	@10
				supplier   	@6
				vertices   	(list Points
				    (413, 542)
				    (246, 607))
				line_style 	0)
			    (object RoleView "$UNNAMED$5" @12
				Parent_View 	@10
				location   	(-165, 83)
				stereotype 	TRUE
				line_color 	3342489
				quidu      	"433DABA001E4"
				client     	@10
				supplier   	@2
				vertices   	(list Points
				    (413, 542)
				    (581, 478))
				line_style 	0)))
		    (object AssociationViewNew "$UNNAMED$9" @13
			location   	(419, 756)
			stereotype 	TRUE
			line_color 	3342489
			quidu      	"433DABAA01F4"
			roleview_list 	(list RoleViews
			    (object RoleView "$UNNAMED$10" @14
				Parent_View 	@13
				location   	(-162, -287)
				stereotype 	TRUE
				line_color 	3342489
				quidu      	"433DABAB0138"
				client     	@13
				supplier   	@6
				vertices   	(list Points
				    (419, 756)
				    (246, 662))
				line_style 	0)
			    (object RoleView "$UNNAMED$11" @15
				Parent_View 	@13
				location   	(-162, -287)
				stereotype 	TRUE
				line_color 	3342489
				quidu      	"433DABAB013A"
				client     	@13
				supplier   	@3
				vertices   	(list Points
				    (419, 756)
				    (593, 850))
				line_style 	0)))
		    (object AssociationViewNew "$UNNAMED$12" @16
			location   	(324, 851)
			font       	(object Font
			    size       	10
			    face       	"Arial"
			    bold       	FALSE
			    italics    	FALSE
			    underline  	FALSE
			    strike     	FALSE
			    color      	0
			    default_color 	TRUE)
			stereotype 	TRUE
			line_color 	3342489
			quidu      	"4C33CDCF033C"
			roleview_list 	(list RoleViews
			    (object RoleView "$UNNAMED$13" @17
				Parent_View 	@16
				location   	(135, 217)
				stereotype 	TRUE
				line_color 	3342489
				quidu      	"4C33CDD00196"
				client     	@16
				supplier   	@4
				vertices   	(list Points
				    (324, 851)
				    (411, 991))
				line_style 	0)
			    (object RoleView "$UNNAMED$14" @18
				Parent_View 	@16
				location   	(135, 217)
				stereotype 	TRUE
				line_color 	3342489
				quidu      	"4C33CDD001A5"
				client     	@16
				supplier   	@6
				vertices   	(list Points
				    (324, 851)
				    (238, 712))
				line_style 	0)))
		    (object AssociationViewNew "$UNNAMED$15" @19
			location   	(474, 639)
			font       	(object Font
			    size       	10
			    face       	"Arial"
			    bold       	FALSE
			    italics    	FALSE
			    underline  	FALSE
			    strike     	FALSE
			    color      	0
			    default_color 	TRUE)
			stereotype 	TRUE
			line_color 	3342489
			quidu      	"4C33CDE4036B"
			roleview_list 	(list RoleViews
			    (object RoleView "$UNNAMED$16" @20
				Parent_View 	@19
				location   	(-251, -104)
				stereotype 	TRUE
				line_color 	3342489
				quidu      	"4C33CDE50196"
				client     	@19
				supplier   	@6
				vertices   	(list Points
				    (474, 639)
				    (246, 634))
				line_style 	0)
			    (object RoleView "$UNNAMED$17" @21
				Parent_View 	@19
				location   	(-251, -104)
				stereotype 	TRUE
				line_color 	3342489
				quidu      	"4C33CDE50198"
				client     	@19
				supplier   	@5
				vertices   	(list Points
				    (474, 639)
				    (702, 645))
				line_style 	0)))))))
    )
HERE
    target = <<HERE
(UseCaseModel
  (Actor User (stereo actor))
  (UseCase Control View (doc Actions which control the display such as suppressing closed sections or change window positions.

[Scenarios not yet written for this use case.]))
  (UseCase Modify Working Schedule (doc Adding sections, removing sections, and highlighting conflicts.
 Scenario A:

))
  (UseCase Print (doc Printing the schedule.))
  (UseCase Times)
  (UseCase View Weekly Schedule (doc Weekly class schedule display.

Scenario A:

   1. User is viewing a working schedule.
   2. User selects viewing the weekly schedule from the menus.
   3. System displays the schedule for Monday through Friday with classes for each hour shown in a separate cell.
   4. User moves schedule to a different part of the screen to allow viewing the main scheduling window.))
  (association (role User navigable plain) (role Control View navigable plain))
  (association (role Times navigable plain) (role User plain))
  (association (role User navigable plain) (role View Weekly Schedule plain))
  (association (role User navigable plain) (role Modify Working Schedule navigable plain))
  (association (role User navigable plain) (role View Weekly Schedule navigable plain) hidden)
  (association (role User navigable plain) (role Print navigable plain))
)
HERE
    s = UmlTools::SExpr.read(x)
    m = UmlTools::Model::UseCaseModel.read(s)
    assert_equal_lines(target.chomp, m.dump.chomp)

    elts = m.all_named_elements
    elt_names = elts.map { |e| e.name }
    elt_names.sort!
    assert_equal(['User', 'Modify Working Schedule', 'Print', 'Control View',
                  'View Weekly Schedule', 'Times'].sort.join(','),
                 elt_names.join(','))
  end

  def test_D_empty_use_case_model
    assert_equal([], @@min.mdl.uc.roles)
    assert_equal([], @@min.mdl.uc.actors)
    assert_equal([], @@min.mdl.uc.use_cases)
    assert_equal([], @@min.mdl.uc.associations)
    assert_equal([], @@min.mdl.uc.all_elements)
    assert_equal([], @@min.mdl.uc.all_named_elements)
  end

  def test_E_use_case_actors
    actors = @@uwpc.mdl.uc.actors
    assert_equal(%w(User), actors.map { |x| x.name })
    use_cases = @@uwpc.mdl.uc.use_cases.map { |x| x.name }
    assert_equal(['Control View', 'Modify Working Schedule',
                  'View Weekly Schedule', 'Print', 'Times'].sort,
                 use_cases.sort)
  end

  def test_F_use_case_roles
    rs = @@uwpc.mdl.uc.roles.map { |x| x.to_s }
    expected = ['(Role User navigable plain)', '(Role Control View navigable plain)', '(Role User navigable plain)', '(Role Modify Working Schedule navigable plain)', '(Role User navigable plain)', '(Role View Weekly Schedule navigable plain)', '(Role User navigable plain)', '(Role Print navigable plain)', '(Role Times navigable plain)', '(Role User plain)', '(Role User navigable plain)', '(Role View Weekly Schedule plain)']
    assert_equal(expected.sort, rs.sort)
  end

  def test_G_include
    x = <<HERE

(object Petal
    version    	50
    _written   	"Rose 2006.0.803.2813"
    charSet    	0)

(object Design "Logical View"
    is_unit    	TRUE
    is_loaded  	TRUE
    attributes 	(list Attribute_Set
	(object Attribute
	    tool       	"Java"
	    name       	"IDE"
	    value      	"Internal Editor"))
    quid       	"4C97CE7802DE"
    enforceClosureAutoLoad 	FALSE
    defaults   	(object defaults
	rightMargin 	0.250000
	leftMargin 	0.250000
	topMargin  	0.250000
	bottomMargin 	0.500000
	pageOverlap 	0.250000
	clipIconLabels 	TRUE
	autoResize 	TRUE
	snapToGrid 	TRUE
	gridX      	0
	gridY      	0
	defaultFont 	(object Font
	    size       	10
	    face       	"Arial"
	    bold       	FALSE
	    italics    	FALSE
	    underline  	FALSE
	    strike     	FALSE
	    color      	0
	    default_color 	TRUE)
	showMessageNum 	3
	showClassOfObject 	TRUE
	notation   	"Unified")
    root_usecase_package 	(object Class_Category "Use Case View"
	quid       	"4C97CE7802E0"
	exportControl 	"Public"
	global     	TRUE
	logical_models 	(list unit_reference_list
	    (object Class "Bill"
		quid       	"4C97CE81036B"
		stereotype 	"Actor")
	    (object UseCase "EatSupper"
		quid       	"4C97CE860399"
		visible_modules 	(list dependency_list
		    (object Dependency_Relationship
			quid       	"4C97CE9E0119"
			stereotype 	"include"
			supplier   	"Use Case View::EatGreens"
			quidu      	"4C97CE900109")))
	    (object UseCase "EatGreens"
		quid       	"4C97CE900109")
	    (object Association "$UNNAMED$0"
		quid       	"4C97CEB103D8"
		roles      	(list role_list
		    (object Role "$UNNAMED$1"
			quid       	"4C97CEB20280"
			supplier   	"Use Case View::EatSupper"
			quidu      	"4C97CE860399"
			is_navigable 	TRUE)
		    (object Role "$UNNAMED$2"
			quid       	"4C97CEB20282"
			supplier   	"Use Case View::Bill"
			quidu      	"4C97CE81036B"
			is_navigable 	TRUE))))
	logical_presentations 	(list unit_reference_list
	    (object UseCaseDiagram "Main"
		quid       	"4C97CE7802EF"
		title      	"Main"
		zoom       	100
		max_height 	28350
		max_width  	21600
		origin_x   	0
		origin_y   	0
		items      	(list diagram_item_list
		    (object UseCaseView "Use Case View::EatGreens" @1
			location   	(1137, 187)
			font       	(object Font
			    size       	10
			    face       	"Arial"
			    bold       	FALSE
			    italics    	FALSE
			    underline  	FALSE
			    strike     	FALSE
			    color      	0
			    default_color 	TRUE)
			label      	(object ItemLabel
			    Parent_View 	@1
			    location   	(1137, 325)
			    anchor_loc 	1
			    nlines     	2
			    max_width  	630
			    justify    	0
			    label      	"EatGreens")
			icon_style 	"Icon"
			line_color 	3342489
			fill_color 	13434879
			quidu      	"4C97CE900109"
			height     	118)
		    (object ClassView "Class" "Use Case View::Bill" @2
			ShowCompartmentStereotypes 	TRUE
			IncludeAttribute 	TRUE
			IncludeOperation 	TRUE
			location   	(88, 191)
			font       	(object Font
			    size       	10
			    face       	"Arial"
			    bold       	FALSE
			    italics    	FALSE
			    underline  	FALSE
			    strike     	FALSE
			    color      	0
			    default_color 	TRUE)
			label      	(object ItemLabel
			    Parent_View 	@2
			    location   	(88, 351)
			    anchor_loc 	1
			    nlines     	2
			    max_width  	324
			    justify    	0
			    label      	"Bill")
			icon       	"Actor"
			icon_style 	"Icon"
			line_color 	3342489
			quidu      	"4C97CE81036B"
			annotation 	8
			autoResize 	TRUE)
		    (object UseCaseView "Use Case View::EatSupper" @3
			location   	(547, 187)
			font       	(object Font
			    size       	10
			    face       	"Arial"
			    bold       	FALSE
			    italics    	FALSE
			    underline  	FALSE
			    strike     	FALSE
			    color      	0
			    default_color 	TRUE)
			label      	(object ItemLabel
			    Parent_View 	@3
			    location   	(547, 325)
			    anchor_loc 	1
			    nlines     	2
			    max_width  	630
			    justify    	0
			    label      	"EatSupper")
			icon_style 	"Icon"
			line_color 	3342489
			fill_color 	13434879
			quidu      	"4C97CE860399"
			height     	118)
		    (object DependencyView "" @4
			font       	(object Font
			    size       	10
			    face       	"Arial"
			    bold       	FALSE
			    italics    	FALSE
			    underline  	FALSE
			    strike     	FALSE
			    color      	0
			    default_color 	TRUE)
			stereotype 	(object SegLabel @5
			    Parent_View 	@4
			    location   	(836, 129)
			    font       	(object Font
				size       	10
				face       	"Arial"
				bold       	FALSE
				italics    	FALSE
				underline  	FALSE
				strike     	FALSE
				color      	0
				default_color 	TRUE)
			    anchor     	10
			    anchor_loc 	1
			    nlines     	1
			    max_width  	450
			    justify    	0
			    label      	"<<include>>"
			    pctDist    	0.486559
			    height     	58
			    orientation 	0)
			line_color 	3342489
			quidu      	"4C97CE9E0119"
			client     	@3
			supplier   	@1
			vertices   	(list Points
			    (655, 187)
			    (1027, 187))
			line_style 	0)
		    (object AssociationViewNew "$UNNAMED$0" @6
			location   	(290, 186)
			stereotype 	TRUE
			line_color 	3342489
			quidu      	"4C97CEB103D8"
			roleview_list 	(list RoleViews
			    (object RoleView "$UNNAMED$1" @7
				Parent_View 	@6
				location   	(202, -5)
				stereotype 	TRUE
				line_color 	3342489
				quidu      	"4C97CEB20280"
				client     	@6
				supplier   	@3
				vertices   	(list Points
				    (290, 186)
				    (435, 186))
				line_style 	0)
			    (object RoleView "$UNNAMED$2" @8
				Parent_View 	@6
				location   	(202, -5)
				stereotype 	TRUE
				line_color 	3342489
				quidu      	"4C97CEB20282"
				client     	@6
				supplier   	@2
				vertices   	(list Points
				    (290, 186)
				    (145, 187))
				line_style 	0)))))))
    root_category 	(object Class_Category "Logical View"
	quid       	"4C97CE7802DF"
	exportControl 	"Public"
	global     	TRUE
	subsystem  	"Component View"
	quidu      	"4C97CE7802E1"
	logical_models 	(list unit_reference_list)
	logical_presentations 	(list unit_reference_list
	    (object ClassDiagram "Main"
		quid       	"4C97CE7802E5"
		title      	"Main"
		zoom       	100
		max_height 	28350
		max_width  	21600
		origin_x   	0
		origin_y   	0
		items      	(list diagram_item_list))))

    )
HERE
    target = <<HERE
(UseCaseModel
  (Actor Bill (stereo actor))
  (UseCase EatGreens)
  (UseCase EatSupper (uses (dependency (stereo include) EatGreens)))
  (association (role EatSupper navigable plain) (role Bill navigable plain))
)
HERE
    s = UmlTools::SExpr.read(x)
    m = UmlTools::Model::UseCaseModel.read(s)
    assert_equal(target.chomp, m.dump.chomp)

    elts = m.all_named_elements
    elt_names = elts.map { |e| e.name }
    elt_names.sort!
    assert_equal(%w(Bill EatSupper EatGreens).sort.join(','),
                 elt_names.join(','))

  end

  def test_H_load_use_case_model
    expected = <<HERE
(UseCaseModel
  (Actor Bill (extends Person) (stereo actor) (doc b))
  (UseCase EatGreens (doc Pour on butter, apply fork.))
  (UseCase EatSupper (uses (dependency EatUses (stereo include) EatGreens (doc Include eating greens in each supper.))) (doc Sit down, hold fork, dig.))
  (UseCase SendToGarbage (uses (dependency (stereo extend) EatSupper) (dependency (stereo extend) EatGreens)) (doc Decide inedible, drop into sink.))
  (association (role EatSupper navigable plain) (role Bill navigable plain))
)
HERE
    assert_equal(expected.chomp, @@uc_sim.mdl.use_case_model.dump.chomp)
  end

  def test_I_associates
    camel = @@e2.mdl.logical.visible_classes.select { |x|
      x.name == 'CamelClass' }[0]
    assert_not_nil(camel)
    assert_equal([], camel.associates)

    aclass = @@e2.mdl.logical.visible_classes.select { |x|
      x.name == 'Another_Class' }[0]
    assert_not_nil(aclass)
    assert_equal(['role Another_Class in association with Another_Class',
                  'role Another_Class in association with Another_Class',
                  'role Some Class in association with Another_Class'],
                 aclass.associates.map { |a| a.ident }.sort)

    addrstack = @@e4.mdl.logical.visible_classes.select { |x|
      x.name == 'AddressableStack<Integer>' }[0]
    assert_not_nil(addrstack)
    assert_equal('Integer', addrstack.contained_type)
    assert_equal(['role MainGUI in association with AddressableStack<Integer>'],
                 addrstack.associates.map { |a| a.ident })

    gen = addrstack.super_relationships[0]
    assert(gen.generalization_relationship?)
    assert_equal 'generalization_relationship stack_gen', gen.ident
    assert_equal 'stack<int>', gen.supplier_name
    assert_equal '<generalization_relationship stack_gen>', gen.to_s
    assert !UmlTools::Model::Element.print_just_ids
    assert_equal '(subclass_of stack_gen stack<int>)', gen.dump
    UmlTools::Model::Element.print_just_ids = true
    assert_not_nil gen.dump # weak test because dump prints id numbers
    UmlTools::Model::Element.print_just_ids = false

    numset = @@e4.mdl.logical.visible_classes.select { |x|
      x.name == 'Map<Number*>' }[0]
    assert_not_nil(numset)
    assert_equal('Number', numset.contained_type)
    assert_equal(['role MainGUI in association with Map<Number*>',
                  'role Number in association with Map<Number*>'],
                 # note the following is not sorted - order is important
                 #   for the following tests
                 numset.associates.map { |a| a.ident })
    assert(!numset.associates[0].multiple_element_multiplicity?)
    assert(numset.associates[1].multiple_element_multiplicity?)
  end

  def test_J_bad_input
    msg = nil
    s = UmlTools::SExpr.read('(object unrecognizable)')

    assert_nothing_thrown do
      begin
        UmlTools::Model::LogicalModel.read(s)
      rescue RuntimeError => e
        msg = e.message
      end
    end
    assert_equal('Logical model not found', msg)

    assert_nothing_thrown do
      begin
        UmlTools::Model::UseCaseModel.read(s)
      rescue RuntimeError => e
        msg = e.message
      end
    end
    assert_equal('Use case model not found', msg)

    assert_nothing_thrown do
      begin
        petal_file = File.join($lib_directory, 'umlTools', 'model', 'model.rb')
        UmlTools::Model::FullModel.load(petal_file)
      rescue RuntimeError => e
        msg = e.message
      end
    end
    assert_equal('Use case model not found', msg)

    assert_nothing_thrown do
      begin
        UmlTools::Model::FullModel.load('$does$not$exist$')
      rescue RuntimeError => e
        msg = e.message
      end
    end
    assert_equal('No such file: $does$not$exist$', msg)
  end

  def test_I_hidden_and_visible_checks
    assert_equal(%w(done), @@e6.mdl.hidden_elements.map { |x| x.name })
    assert_equal([], @@min.mdl.hidden_elements.map { |x| x.name })
    assert_equal(%w(BeGone NewClass), @@e4.mdl.hidden_elements.map { |x| x.name })
    assert_equal(['BaseClass', 'SubClass', 'Stuff', 'AltBase', nil, nil],
                 @@min.mdl.visible_elements.map { |x| x.name })
  end

  def test_J_full_model_print
    e4_full_model = <<HERE
FullModel[
  UseCaseModel[
  <Actor UNNAMED_USER>
  <UseCase UNNAMED_ACTION>
  <assoc between (Role navigable plain) and (Role navigable plain)>
]
  LogicalModel[
  <MClass stack<int>//(Method push), (Method rotate), (Method pop), (Method iterate), (Method begin), (Method end), (Method push_back)>
  <MClass Map<Number*>//(Method containsKey), (Method add), (Method sort)>
  <MClass MainGUI//(Method readList), (Method updateList), (Method exitProgram)>
  <MClass AddressableStack<Integer> - from stack<int>//(Method operator[])>
  <MClass BeGone//>
  <MClass Number//>
  <MClass NewClass//>
  <MClass btree<Name>//>
  <MClass UNNAMED5//>
  <MClass DatabaseDate/(Attr year), (Attr month), (Attr day)/>
  <assoc between (Role stack<int> navigable plain) and (Role MainGUI plain)>
  <assoc between (Role Map<Number*> (name: Used_numbers) 1 navigable composite) and (Role MainGUI 1 plain)>
  <assoc between (Role AddressableStack<Integer> 2 navigable plain) and (Role MainGUI composite)>
  <assoc between (Role BeGone navigable plain) and (Role MainGUI plain)>
  <assoc between (Role Number * navigable plain) and (Role Map<Number*> 0..1 plain)>
  <assoc between (Role btree<Name> 0..1 navigable plain) and (Role MainGUI composite)>
]
]
HERE
    assert_equal_lines(e4_full_model, @@e4.mdl.to_s)
  end

  def test_K_full_model_dump
    expected = <<HERE
(FullModel
  (UseCaseModel
  (Actor UNNAMED_USER (stereo actor))
  (UseCase UNNAMED_ACTION)
  (association (role navigable plain) (role navigable plain) hidden)
)
  (LogicalModel
  (MClass AddressableStack<Integer> (extends stack<int>) (methods (elt (. AddressableStack<Integer> operator[]) public (doc h))) (doc g))
  (MClass BeGone hidden)
  (MClass DatabaseDate (attributes (attr (. DatabaseDate day) private (doc d)) (attr (. DatabaseDate month) private (doc m)) (attr (. DatabaseDate year) private (doc y))) (doc This is a Utility
class for dates stored in the database))
  (MClass MainGUI (methods (elt (. MainGUI exitProgram) public (doc e)) (elt (. MainGUI readList) public (doc r)) (elt (. MainGUI updateList) public (doc u))) (doc j))
  (MClass Map<Number*> (methods (elt (. Map<Number*> add) public (doc z)) (elt (. Map<Number*> containsKey) public (doc y)) (elt (. Map<Number*> sort) public (doc i))))
  (MClass NewClass hidden)
  (MClass Number (doc Numbers))
  (MClass UNNAMED5)
  (MClass btree<Name> (doc binary search tree))
  (MClass stack<int> (methods (elt (. stack<int> begin) public (doc e)) (elt (. stack<int> end) public) (elt (. stack<int> iterate) public (doc d)) (elt (. stack<int> pop) public) (elt (. stack<int> push) public) (elt (. stack<int> push_back) public) (elt (. stack<int> rotate) public (doc b))))
  (association (role BeGone navigable plain) (role MainGUI plain) hidden)
  (association (role Number * navigable plain) (role Map<Number*> 0..1 plain))
  (association (role btree<Name> 0..1 navigable plain) (role MainGUI composite))
  (association (role stack<int> navigable plain) (role MainGUI plain) hidden)
  (association (role Map<Number*> (name Used_numbers) 1 navigable composite (assoc Used_numbers)) (role MainGUI 1 plain (assoc Used_numbers)))
  (association (role AddressableStack<Integer> 2 navigable plain) (role MainGUI composite))
)
)
HERE
    assert_equal_lines(expected, @@e4.mdl.dump)
  end

  def test_L_load_stdin
    save = $stdin
    names = []
    $stdin = File.open(File.join($test_resource_directory, 'min-bad.mdl'))
    m = UmlTools::Model::FullModel.load(nil)
    names = m.all_named_elements.map { |x| x.name }
    $stdin = save
    assert(names.include? 'Stuff')
    assert(names.include? 'SubClass')
  end

  def test_M_syntax_error
    mdl = UmlTools::Model::FullModel.load(File.join($test_resource_directory, 'syntax-error1.mdl'))
    lines = mdl.dump.split("\n")
    assert(lines.size > 3)
    assert_equal('(FullModel', lines[0].strip)
    assert_equal('(SYNTAX_ERRORS', lines[1].strip)
    assert_equal('unexpected atom for attributes: oops', lines[2].strip)
    assert_equal('reference to missing super: 4BABCE46008C', lines[3].strip)
    assert_equal('unexpected atom for methods: none', lines[4].strip)
    assert_equal('Missing id field for UmlTools::Model::Method)', lines[5].strip)
  end

  def test_N_hidden
    hidden = @@clean.mdl.logical.hidden_associations
    assert_equal(1, hidden.size)
    assert_equal('(association (role List plain) (role Roster plain) hidden)',
                 hidden[0].dump)
    assert(@@uc_e1.mdl.logical.visible_roles.empty?)
  end

  # roughly, obtain coverage for element methods
  def test_O_elements
    assert_equal(nil, UmlTools::Model::Element.read_protection('unknown'))

    generic_elt = UmlTools::Model::Element.new(UmlTools::SAtom.new(''), UmlTools::SAtom.new('5'), nil, nil)
    assert_equal('Unnamed Element', generic_elt.nonempty_name)
    assert_equal(0, generic_elt <=> generic_elt)
    assert_equal('element [unnamed]', generic_elt.ident)
    assert_equal('', generic_elt.dump)
    assert(!generic_elt.use_case?)
    assert(!generic_elt.attribute?)
    assert(!generic_elt.dependency?)

    assert(!UmlTools::Model::Element.print_ids)
    UmlTools::Model::Element.print_ids = true
    assert(UmlTools::Model::Element.print_ids)
    assert(!UmlTools::Model::Element.print_just_ids)
    UmlTools::Model::Element.print_just_ids = true

    usecase = UmlTools::Model::UseCase.new(UmlTools::SAtom.new('some use case'), UmlTools::SAtom.new('323'),
                                 nil, nil, nil, nil)
    assert(usecase.use_case?)
    assert(!usecase.attribute?)
    assert(!usecase.dependency?)
    usecase.find_links_in({})
    assert_equal([], usecase.supers)
    assert_equal([], usecase.uses)
    assert_equal([], usecase.sub_elements)
    assert_equal([], usecase.associates)
    assert_equal('(usecase 323)', usecase.dump)

    act = UmlTools::Model::Actor.new(UmlTools::SAtom.new('an actor'), UmlTools::SAtom.new('008'),
                           nil, nil, nil, nil)
    act.find_links_in({})
    assert(act.actor?)
    assert_equal('(actor 008)', act.dump)
    # purely coverage for an otherwise unused operation:
    assert(!act.generalization_relationship?)

    cls = UmlTools::Model::MClass.new(UmlTools::SAtom.new('a mclass'), UmlTools::SAtom.new('009'),
                            nil, nil, nil, nil, nil, nil)
    cls.find_links_in({})
    assert(cls.class?)
    assert_equal('class a mclass (hidden)', cls.ident)
    assert_equal(nil, cls.contained_type)
    assert_equal('(class 009 )', cls.dump)

    attr = UmlTools::Model::Attribute.new(UmlTools::SAtom.new('some attribute'), UmlTools::SAtom.new('324'),
                                nil, nil, nil, nil)
    assert(!attr.use_case?)
    assert(attr.attribute?)
    assert(!attr.dependency?)
    assert_equal('attribute some attribute', attr.ident)
    assert_equal('(Attr some attribute)', attr.to_s)
    assert_equal('(attr 324)', attr.dump)

    meth = UmlTools::Model::Method.new(UmlTools::SAtom.new('madness'), UmlTools::SAtom.new('010'), nil, nil, nil)
    assert(meth.method?)
    assert_equal('operation madness', meth.ident)
    assert_equal('(elt 010)', meth.dump)

    ass = UmlTools::Model::Association.new(UmlTools::SAtom.new('assoc'), UmlTools::SAtom.new('011'), nil, nil,
                                 UmlTools::Model::Role.new(UmlTools::SAtom.new('role a'),
                                                 UmlTools::SAtom.new('012'),
                                                 UmlTools::SAtom.new('bad_supplier'),
                                                 nil, nil, nil, nil, nil),
                                 UmlTools::Model::Role.new(UmlTools::SAtom.new('role b'),
                                                 UmlTools::SAtom.new('013'),
                                                 nil, nil, nil, nil, nil, nil))
    ass.find_links_in({})
    assert_equal([], ass.a_role.load_errors)
    assert_equal('<assoc assoc between (Role (name: role a) ) and ' +
                     '(Role (name: role b) )>',
                 ass.to_s)
    assert_equal('(assoc assoc role a role b)', ass.abbrev)
    assert_equal('(association 011 (role 012) (role 013))', ass.dump)
    assert_equal 'role a/role b', ass.a_role.full_name
    assert_equal 'role b/role a', ass.b_role.full_name

    noname_ass = UmlTools::Model::Association.new(UmlTools::SAtom.new('abc'), UmlTools::SAtom.new('014'),
                                        nil, nil,
                                        UmlTools::Model::Role.new(UmlTools::SAtom.new(''),
                                                        UmlTools::SAtom.new('015'),
                                                        nil, nil, nil, '1..1',
                                                        nil, nil),
                                        UmlTools::Model::Role.new(UmlTools::SAtom.new(''),
                                                        UmlTools::SAtom.new('016'),
                                                        nil, nil, nil, '0..*',
                                                        nil, nil))

    assert_equal('1..1', noname_ass.a_role.multiplicity.to_s)
    assert_equal('1', noname_ass.a_role.effective_multiplicity)
    assert_equal('0..*', noname_ass.b_role.multiplicity.to_s)
    assert_equal('0..*', noname_ass.b_role.effective_multiplicity)
    assert_equal('(assoc abc)', noname_ass.abbrev)

    dep = UmlTools::Model::Dependency.new(UmlTools::SAtom.new('some dependency'), nil, nil, nil, nil)
    assert_equal('[unknown]', dep.id)
    assert(!dep.use_case?)
    assert(!dep.attribute?)
    assert(dep.dependency?)
    assert_equal('dependency some dependency', dep.ident)
    assert_equal('<dependency some dependency>', dep.to_s)
    assert_equal('(dependency [unknown])', dep.dump)

    roll1 = UmlTools::Model::Role.new(UmlTools::SAtom.new(''), UmlTools::SAtom.new('017'), nil, nil,
                            nil, nil, nil, :aggregate)
    roll1.find_links_in({})
    assert(roll1.aggregate?)
    assert_equal(['role 017 missing supplier'], roll1.load_errors)

    roll2 = UmlTools::Model::Role.new(UmlTools::SAtom.new(''), UmlTools::SAtom.new('018'), nil, nil,
                            UmlTools::SAtom.new('22'), nil, nil, :composite)
    roll2.find_links_in({})
    assert(!roll2.aggregate?)
    assert_equal(['reference to missing supplier: 22'], roll2.load_errors)

    UmlTools::Model::Element.print_just_ids = false
    assert_equal('(UseCase some use case 323  hidden)', usecase.dump)
    assert_equal('(elt madness)', meth.dump)
    assert_equal('(attr some attribute 324 )', attr.dump)
    UmlTools::Model::Element.print_ids = false
    assert_equal('(UseCase some use case hidden)', usecase.dump)
  end

  # test running from command line
  def test_P_main
    assert !$debug_petal, '$debug_petal must be false for test_P_main'

    old_stdout = $stdout
    help_output = StringIO.new
    dump_output = StringIO.new
    error_output = StringIO.new
    $stdout = help_output
    help_return = UmlTools.petal_main %w(--help)
    help_results = $stdout.string
    $stdout = dump_output
    args = %w(-i -I -m)
    args << File.join($test_resource_directory, 'min-bad.mdl')
    dump_return = UmlTools.petal_main args
    dump_results = $stdout.string
    $stdout = error_output
    error_return = UmlTools.petal_main %w(model.rb)
    error_results = $stdout.string
    $stdout = old_stdout

    assert_equal 'Usage:', help_results.split[0]
    assert_equal 1, help_return
    assert_equal '(FullModel', dump_results.split[0]
    assert_equal 0, dump_return
    assert_equal 'Error: model.rb does not appear to contain a UML model',
                 error_results.chomp
    assert_equal 2, error_return
  end

  def test_Q_load_petal
    target = <<HERE
(FullModel
  (UseCaseModel
  (Actor SomeClass (doc SomeClass appearing on use case diagram as an actor))
  (Actor me (uses (dependency (stereo extend) chew wood)))
  (Actor my_clone (extends me) (doc A clone of myself.))
  (UseCase Log In)
  (UseCase chew oak (extends chew wood) (uses (dependency (stereo extend) my_clone) (dependency (stereo extend) Log In) (dependency (stereo extend) my_clone)) (doc Eating a oak tree leads
to long life.))
  (UseCase chew wood (uses (dependency (stereo include) Log In)) (doc act of chewing wood))
)
  (LogicalModel
  (MClass Basement (extends SomeClass) (uses (dependency (stereo extend) OtherClass (doc dependency upon the class OtherClass)) (dependency (stereo extend) actor_in_class_model (doc odd sort of dependency on some random actor))) (methods (elt (. Basement dig_down_op) public) (elt (. Basement private_dig) private) (elt (. Basement protected_dig) protected)))
  (MClass OtherClass (attributes (attr (. OtherClass default_attribute) public) (attr (. OtherClass private_attribute) private) (attr (. OtherClass protected_attribute) protected)))
  (MClass SomeClass)
  (MClass actor_in_class_model (attributes (attr (. actor_in_class_model height) public)))
  (MClass hidden_class)
  (association *_to_1_association (role OtherClass 1 navigable plain (assoc *_to_1_association)) (role SomeClass *  (assoc *_to_1_association)))
  (association 1_to_1_directed_association (role SomeClass 1  (assoc 1_to_1_directed_association)) (role OtherClass 1 navigable plain (assoc 1_to_1_directed_association)))
  (association 1_to_1_undirected_association (role OtherClass 1 navigable  (assoc 1_to_1_undirected_association)) (role SomeClass 1 navigable  (assoc 1_to_1_undirected_association)))
  (association aggregation (role SomeClass navigable  (assoc aggregation)) (role Basement 6 navigable  (assoc aggregation)))
  (association composition (role Basement 1 navigable composite (assoc composition)) (role SomeClass 9 navigable  (assoc composition)))
  (association hidden_association (role OtherClass 1 navigable  (assoc hidden_association)) (role Basement 1 navigable  (assoc hidden_association)))
)
)
HERE
    assert_equal(target.chomp, @@xmi_f.mdl.dump.chomp)
    assert_equal(@@xmi_f.mdl.file_format, :xmi_21)
    assert !@@xmi_f.mdl.format_supports_detailed_multiplicities?
    assert @@min.mdl.format_supports_detailed_multiplicities?
    assert_equal(3, @@xmi_f.mdl.classes_with_roles.length)
  end

  # Integration test for loading a full model with
  # a use case diagram from an EA xmi file.
  def test_xmi_load_ea_usecase
    model = UmlTools::Model::FullModel.load File.join($test_resource_directory, 'xmi', 'ea', 'uc-xmi-2.1.xmi')
    use_case_model = model.uc
    use_cases = use_case_model.use_cases
    use_case_names = use_cases.map { |use_case| use_case.name }
    actors = use_case_model.actor_names
    expected_actors = ['Fred', 'MysteryUser', 'ExtraMysteriousUser', 'WhoAmI?', 'short'].sort
    expected_use_cases = ['Car', 'DoItAgain', 'ElectricCar', 'Sit', 'try'].sort
    assert_equal(:xmi_ea, model.file_format, "Should be xmi_ea file format")
    assert_equal(expected_use_cases, use_case_names, 'Use cases should have expected names')
    assert_equal(expected_actors, actors, 'Actors should have expected names')
    assert_equal(5, use_cases.length, 'Should be 5 use cases on the model')
    assert_equal(6, use_case_model.associations.length, 'Should be 6 associations on the model')
  end

  # Parsing code is not finished, test that what is
  # finished so far does not blow up.
  def test_xmi_load_ea_class
    model = UmlTools::Model::FullModel.load File.join($test_resource_directory, 'xmi', 'ea', 'class-xmi-2.1.xmi')
    logical_model = model.logical
    logical_model.roles
    assert_equal(:xmi_ea, model.file_format, "Should be xmi file format")
    assert logical_model
  end

  # Test reading elements from EA xmi files with empty details_table.
  # Should raise a RuntimeError.
  def test_xmi_ea_missing_details
    element = REXML::Element.new
    element.add_attributes({'name' => 'Name', 'xmi:id' => 'Id'})
    details_table = {'Id' => nil}
    exception = assert_raise(RuntimeError) { UmlTools::Model::MClass.read_from_xmi(element, details_table) }
    assert_equal('Missing details for class Name', exception.message)
    exception = assert_raise(RuntimeError) { UmlTools::Model::Association.read_from_xmi(element, details_table) }
    assert_equal('Missing details for association Id', exception.message)
  end

  must 'raise an error if the details table does not have a matching id for EA dependency element' do
    element = get_xml_element_with_name 'dependency'
    details_table = {:Id => nil}
    exception = assert_raise(RuntimeError) { UmlTools::Model::Dependency.read_from_xmi(element, details_table) }
    assert_equal('Missing details for dependency Id', exception.message)
  end

  def get_xml_element_with_name(name)
    element = REXML::Element.new
    element.add_attributes({'name' => 'Name', 'xmi:id' => 'Id'})
    element.name = name
    element
  end

  # Tests the reading of an attribute from a Rhapsody xmi file
  def test_xmi_21_read_attribute_from_xmi
    parent_element = REXML::Element.new
    parent_element.add_attributes({'name' => 'parent', 'xmi:id' => 'parent_id', 'type' => 'parent_type'})
    properties = REXML::Element.new('properties')
    properties.add_attributes({'type' => 'Type', 'name' => 'properties'})
    stereotype = REXML::Element.new('stereotype')
    stereotype.text = 'Stereotype'
    doc = REXML::Element.new('document')
    doc.text = 'Doc'
    parent_element.add_element(properties)
    parent_element.add_element(stereotype)
    parent_element.add_element(doc)
    attribute = UmlTools::Model::Attribute.read_from_xmi_21(parent_element, {'Id' => nil})
    assert_equal('parent', attribute.name)
    assert_equal('parent_id', attribute.id)
    assert_equal('Doc', attribute.doc)
    assert_equal('stereotype', attribute.stereotype)
    assert_equal('Type', attribute.type)
  end

  # Tests the exception handling in reading associations from a Rhapsody xmi file
  def test_xmi_21_read_association_errors
    element = REXML::Element.new()
    element.add_attribute('xmi:id', 'ID')
    exception = assert_raise(RuntimeError) { association = UmlTools::Model::Association.read_from_xmi_21(element, nil) }
    assert_equal('Missing memberEnd attribute for uml:Association ID', exception.message, 'Exception should have specific message')

    element.add_attribute('memberEnd', 'member_end')
    exception = assert_raise(RuntimeError) { association = UmlTools::Model::Association.read_from_xmi_21(element, nil) }
    assert_equal('Missing memberEnd item for uml:Association ID', exception.message, 'Exception should have specific message')
  end

  # Tests the exception handling in reading roles from a Rhapsody xmi file
  def test_xmi_21_read_role_errors
    element = REXML::Element.new()
    element.add_attributes({'xmi:id' => 'ID', 'xmi:type' => 'not uml:Property'})
    exception = assert_raise(RuntimeError) { UmlTools::Model::Role.read_from_xmi_21(element, nil, nil) }
    assert_equal('Unexpected ownedEnd object ID', exception.message, 'Exception should have specific message')
  end

  # Tests reading a role from a Rhapsody xmi file
  def test_xmi_21_read_role
    element = REXML::Element.new()
    element.add_attributes({'name' => 'role_name', 'aggregation' => 'aggregate', 'xmi:id' => 'ID', 'xmi:type' => 'uml:Property'})
    role = UmlTools::Model::Role.read_from_xmi_21(element, [], {'Id' => nil})
    assert role
    assert_equal('ID', role.id)
    assert !role.navigable?
    assert_equal('role_name', role.name)
    assert_equal(:aggregate, role.assoc_type)
    assert_equal('uml:Property', role.supplier_id)
  end

  # Tests reading cardinality of a role from a Rhapsody xmi file
  def test_xmi_21_read_cardinality
    parent = REXML::Element.new()
    upper = REXML::Element.new('upperValue')
    upper.add_attribute('value', 'upper_value')
    lower = REXML::Element.new('lowerValue')
    lower.add_attribute('value', 'lower_value')
    parent.add_element(upper)
    parent.add_element(lower)
    card = UmlTools::Model::Role.read_cardinality_xmi_21(parent)
    assert_equal('lower_value..upper_value', card, 'Card should be concatenation of multiplicities')
  end

  # Tests reading cardinality of a role from a Rhapsody xmi file with a default lower value
  def test_xmi_21_read_cardinality_default_lower_mult
    parent = REXML::Element.new()
    upper = REXML::Element.new('upperValue')
    upper.add_attribute('value', 'upper_value')
    parent.add_element(upper)
    card = UmlTools::Model::Role.read_cardinality_xmi_21(parent)
    assert_equal('1..upper_value', card, 'Card should be concatenation of multiplicities')
  end

  # Test reading MClass from a Rhapsody xmi file
  def test_xmi_21_read_m_class
    parent = REXML::Element.new()
    parent.add_attributes({'name' => 'Name', 'xmi:id' => 'Id'})
    child = REXML::Element.new('ownedAttribute')
    child.add_attribute('type', 'type_attr')
    profile_element = REXML::Element.new()
    profile_element.add_attribute('base_Class', 'base')
    parent.add_element(child)
    details = {'type_attr' => profile_element}
    m_class = UmlTools::Model::MClass.read_from_xmi_21(parent, Hash.new(), Hash.new(), details)
    assert_equal('Name', m_class.name)
    assert_equal('Id', m_class.id)
    assert m_class.load_errors.empty?
  end

  # Test AssociableElement.dumped_supers with Element.print_just_ids equal
  # to true, should return an empty string.
  def test_dumped_supers_print_just_ids
    UmlTools::Model::Element.print_just_ids = true
    assoc_element = UmlTools::Model::AssociableElement.new('name', 'id', 'stereotype', 'doc', nil, nil)
    retVal = assoc_element.dumped_supers
    assert_equal('', retVal, "Should return empty string if Element.print_just_ids is true")
  end

  # Test reading Association from a Rhapsody xmi file.
  def test_xmi_21_read_association
    element = REXML::Element.new()
    element.add_attributes({'name' => 'Name', 'xmi:id' => 'Id', 'memberEnd' => 'a b'})
    child1 = REXML::Element.new('ownedEnd')
    child1.add_attributes({'xmi:type' => 'uml:Property', 'name' => 'b_role'})
    child2 = REXML::Element.new('ownedEnd')
    child2.add_attributes({'xmi:type' => 'uml:Property', 'name' => 'a_role'})
    element.add_element(child1)
    element.add_element(child2)
    result = UmlTools::Model::Association.read_from_xmi_21(element, {})
    assert_equal('Name', result.name)
    assert_equal('Id', result.id)
    assert_equal('a_role', result.a_role.name)
    assert_equal('b_role', result.b_role.name)
    assert result
  end
end
