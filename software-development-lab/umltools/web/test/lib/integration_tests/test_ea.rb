#!/usr/bin/env ruby

require_relative '../test_base'

class EAIntegrationTests < Test::Unit::TestCase
  include TestBase

  # Basic test of parsing an ea file
  def test_ea_class_diagram
    diagram_file = File.join($test_directory, 'resources', 'xmi', 'ea', 'class-xmi-integration.xmi')
    model = UmlTools::Model::FullModel.load(diagram_file)
    logical = model.logical_model

    assert_equal(1, logical.associations.size)
    assert_equal(5, logical.classes.size)
    assert_equal(1, logical.classes[0].super_relationships.size)
    assert_equal(1, logical.classes[2].super_relationships.size)
    assert_equal(1, logical.classes[3].attributes.size)
    assert_equal(1, logical.classes[3].attributes.size)
    assert(logical.associations[0].a_role)
    assert(logical.associations[0].a_role.single_element_multiplicity?)
    assert(logical.associations[0].b_role.multiple_element_multiplicity?)
  end

  # check that there are no errors when checking a diagram with two
  #   features: String is both an attribute and an object, and
  #   the diagram contains an EA diagram note
  # to remove:
  #Class Unnamed Element is not associated with any other class and may be unnecessary
  #Missing documentation: class Unnamed Element
  def test_simple_diagram_with_note
    old_stdout = $stdout
    $stdout = (output = StringIO.new)

    UmlTools.check_main(['-d', '-c', '[]', 
                         File.join($test_resource_directory, 'xmi', 'ea', 
                                   'simplenote.xmi')])
    none = "No issues identified, but be sure to review the model carefully.\n"
    assert_equal none, output.string

    $stdout = old_stdout
  end

  # Sanity check to make sure UMLint does not blow up when checking EA files. Checks one
  # or more errors in each category of possible errors.
  def test_checks
    old_stdout = $stdout
    $stdout = (output = StringIO.new)

    UmlTools.check_main(['-d', '-c', '[]', File.join($test_resource_directory, 'xmi', 'ea', 'class-xmi-checks.xmi')])
    result = output.string

    assert(result.include?('Class MultInheritance inherits from multiple classes (lowerCase, With Spaces) - generalization arrows may be backwards'))
    assert(result.include?('Missing documentation for class lowerCase members: another_attribute, complexAttribute, spellingEror'))
    assert(result.include?('Class lowerCase should begin with a capital letter'))
    assert(result.include?('Class NoFriends is not associated with any other class and may be unnecessary'))
    assert(result.include?('Role Class2 in association with Class3: use multiplicity \'*\' rather than \'0..*\''))
    assert(result.include?('Unrecognized words in identifiers: eror, mult'))
    assert(result.include?('Attribute complexAttribute in class lowerCase appears to not be simple - replace by an association with the class'))
    assert(result.include?('Avoid mixing naming styles between attributes (using underscores in some names and not others)'))
    assert(result.include?('Class With Spaces contains spaces'))
    assert(result.include?('Invalid operation(s) in standard class List: notPartOfStdList'))
    assert(result.include?('Operation bad$identifier in With Spaces is not a legal identifier'))
    assert(result.include?('Role Class3 in association with Class2: do not use multiplicity 0'))
    assert(result.include?('`int\' is a reserved word in Java and C++ but was used as a name in operation int in With Spaces'))

    $stdout = old_stdout
  end

end
