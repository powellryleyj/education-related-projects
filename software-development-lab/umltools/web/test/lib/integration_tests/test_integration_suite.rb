#!/usr/bin/env ruby
# Testing the Attribute class
require_relative 'test_ea'
require_relative 'test_full_petal'
require_relative 'test_model'
require_relative 'test_runs'
require_relative 'test_ck_tests'
require_relative 'test_pstats'

class IntegrationTestsSuite < Test::Unit::TestCase
end
