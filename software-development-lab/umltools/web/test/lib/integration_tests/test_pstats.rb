#!/usr/bin/env ruby
# test_pstats: test pstats.rb
#   - make this a separate test to allow for future growth in this area

require_relative '../test_checks'

class TestPStats < Test::Unit::TestCase
  include BaseModelTests

  def setup
    # nothing
  end

  def teardown
    # nothing
  end

  def test_stats
    stats = @@uwpc.mdl.logical_model.statistics
    assert_equal 8,  stats.class_count
    assert_equal 29, stats.method_count
    assert_equal 23, stats.attribute_count
    assert_equal 64, stats.complexity

    stats = @@uwpc.mdl.use_case_model.statistics
    assert_equal 1,  stats.actor_count
    assert_equal 5,  stats.use_case_count
    assert_equal 5,  stats.association_count
    assert_equal 16, stats.complexity
  end

  # test running from command line
  def test_pstats_main
    old_stdout   = $stdout
    stats_output = StringIO.new
    $stdout = stats_output
    stats_return = UmlTools.pstats_main(File.join($test_resource_directory, 'uwpclasses.mdl'))
    stats_results = $stdout.string
    $stdout = old_stdout

    assert_equal 0, stats_return
    expected = <<HERE
Use case model size: 1 actors, 5 cases, 5 associations
Use case complexity: 16
Class model size: 8 classes w/ 23 attributes and 29 methods, 12 associations
Class complexity: 64
HERE
    assert_equal expected, stats_results
  end

  def test_pstats_main_for_empty_use_case_model
    old_stdout   = $stdout
    stats_output = StringIO.new
    $stdout = stats_output
    stats_return = UmlTools.pstats_main(File.join($test_resource_directory, 'non-errors.mdl'))
    stats_results = $stdout.string
    $stdout = old_stdout

    assert_equal 0, stats_return
    expected = <<HERE
Use case model: empty
Class model size: 5 classes w/ 10 attributes and 13 methods, 8 associations
Class complexity: 31
HERE
    assert_equal expected, stats_results
  end

  def test_pstats_main_for_empty_logical_model
    old_stdout   = $stdout
    stats_output = StringIO.new
    $stdout = stats_output
    stats_return = UmlTools.pstats_main(File.join($test_resource_directory, 'uc0.mdl'))
    stats_results = $stdout.string
    $stdout = old_stdout

    assert_equal 0, stats_return
    expected = <<HERE
Use case model size: 1 actors, 4 cases, 1 associations
Use case complexity: 7
Class model: empty
HERE
    assert_equal expected, stats_results
  end

end
