#!/usr/bin/env ruby
# testing core umlint output

require_relative '../test_checks'

class TestLogicalModelChecks < Test::Unit::TestCase
  include BaseModelTests
  include TestBase

  def test_ck_associations
    expected = ['Association between Stuff and BaseClass: composition should have multiplicity 1']
    assert_equal(expected.sort,
                 UmlTools.msgs(UmlTools::Check::CheckAssociations.new(@@min.mdl).check).sort)
    expected = ['Association between short and Some Class has a role name at the wrong end',
                'Class CamelClass is not associated with any other class and may be unnecessary']
    assert_equal(expected.sort,
                 UmlTools.msgs(UmlTools::Check::CheckAssociations.new(@@e2.mdl).check).sort)
    expected = ['Association between Worker and Manager has a role name at the wrong end',
                'Association between Worker and Job: composition should have multiplicity 1',
                'Class Manager has a dynamic relationship with Job - documenting these is rarely important',
                'Class Worker has two owners: Job and Manager',
                'Container class vector<> appears to have no association to any elements']
    assert_equal_sequences(expected.sort,
                           UmlTools.msgs(UmlTools::Check::CheckAssociations.new(@@e3.mdl).check).sort)
    assert_equal([], UmlTools::Check::CheckAssociations.new(@@noerr.mdl).check)
    expected = ['Association between DoItNow and MysteryUser is navigable; change to undirected association',
                'Association between DoItAgain and ConfidenceArtist is either marked with a stereotype or has a name; such associations must be unmarked',
                'Association between WierdUser and DoItAgain is either marked with a stereotype or has a name; such associations must be unmarked',
                'Association between WierdUser and DoItNow is either marked with a stereotype or has a name; such associations must be unmarked',
                'Association between User with no name and DoItNow is navigable; change to undirected association',
                'Association some_association is either marked with a stereotype or has a name; such associations must be unmarked',
                'Use case DoNothing is not associated with anything and may be unnecessary',
                'Use case Dont Do It is not associated with anything and may be unnecessary',
                'Plain dependencies such as between DoNothing and ConfidenceArtist should not appear in use case diagrams',
                'Plain dependencies such as between DoNothing and DoItAgain should not appear in use case diagrams',
                'Plain dependencies such as between DoNothing and MysteryUser should not appear in use case diagrams',
                'Plain dependencies such as between User with no name and ConfidenceArtist should not appear in use case diagrams'
               ]
    assert_equal_sequences(expected.sort,
                           UmlTools.msgs(UmlTools::Check::CheckAssociations.new(@@uc_e1.mdl).check).sort)
    expected = ['Association between DoItAgain and Caar is marked as <<extend>> or <<include>>; change to a dependency',
                'Association between DoItAgain and WhoAmI? has multiplicity and should not',
                'Association between DoItAgain and try is marked as <<extend>> or <<include>>; change to a dependency',
                'Association between Fred and DoItAgain has multiplicity and should not',
                'Association between Fred and try is navigable; change to undirected association',
                'Association between try and MysteryUser is navigable; change to undirected association',
                'Dependencies such as between Caar and CloseDoor cannot be named; use <<extend>> and <<include>>',
                'Dependencies such as between break lock and Caar cannot be named; use <<extend>> and <<include>>',
                'Illegal dependency between OpenDoor and Caar; must use either <<extend>> or <<include>>',
                'Invalid association between DoItAgain and Caar: associate use cases to actors only',
                'Invalid association between DoItAgain and try: associate use cases to actors only',
                'Invalid association between Sit and Caar: associate use cases to actors only',
                'Invalid association between WhoAmI? and short: associate use cases to actors only',
                'Use case CloseDoor is not associated with anything and may be unnecessary',
                'Use case ElectricVoiture is not associated with anything and may be unnecessary',
                'Use case OpenDoor is not associated with anything and may be unnecessary',
                'Use case break lock is not associated with anything and may be unnecessary'
               ]
    assert_equal_sequences(expected.sort,
                           UmlTools.msgs(UmlTools::Check::CheckAssociations.new(@@uc_e2.mdl).check).sort)
    # essentially, provide coverage of method
    assert_not_nil(UmlTools::Check::CheckAssociations.simple_types)
  end

  def test_ck_attr_names
    assert_equal([], UmlTools::Check::CheckAttributeNames.new(@@min.mdl).check)
    expected = ['Attribute index in short names an index - replace by a direct association',
                'Attribute some_id in short names an index - replace by a direct association',
                'Attribute the_id_for_camel in short names an index - replace by a direct association',
                'Attribute this stuff in Some Class is public']
    assert_equal_sequences(expected.sort,
                           UmlTools.msgs(UmlTools::Check::CheckAttributeNames.new(@@e2.mdl).check).sort)
    expected = ['Avoid mixing naming styles between attributes (using underscores in some names and not others)',
                'Attribute Manger::Low:Salary in Manager should begin with a lower case letter']
    assert_equal_sequences(expected.sort,
                           UmlTools.msgs(UmlTools::Check::CheckAttributeNames.new(@@e3.mdl).check).sort)
    assert_equal([], UmlTools::Check::CheckAttributeNames.new(@@noerr.mdl).check)
  end

  def test_ck_idents
    # min_mdl used to contain nums[] as an illegal identifier, but this
    #   is now treated as a complex attributes issue
    assert_equal([], UmlTools::Check::CheckIdentifiers.new(@@min.mdl).check)
    expected = ['Class Some Class contains spaces',
                'Attribute this stuff in Some Class contains spaces',
                'Attribute that stuff in Some Class contains spaces',
                'Operation find key in Some Class contains spaces',
                'Role theSome Class from Some Class contains spaces',
                'Operation a&7 in short is not a legal identifier',
                'Operation a* in short is not a legal identifier',
                'Operation b$ in short is not a legal identifier',
                'Operation 3x7 in short is not a legal identifier',
                "`short' is a reserved word in Java and C++ but was used as a name in attribute short in CamelClass",
                "`short' is a reserved word in Java and C++ but was used as a name in class short",
                "`signed' is a reserved word in C++ but was used as a name in association signed",
                "`break' is a reserved word in Java and C++ but was used as a name in operation break in Another_Class",
                "`case' is a reserved word in Java and C++ but was used as a name in operation case in Another_Class",
                "`int' is a reserved word in Java and C++ but was used as a name in attribute int in CamelClass",
                "`extends' is a reserved word in Java but was used as a name in operation extends in CamelClass",
                "`try' is a reserved word in Java and C++ but was used as a name in role try from short",
                'Unrecognized words in identifiers: wierd']
    assert_equal_sequences(expected.sort,
                           UmlTools.msgs(UmlTools::Check::CheckIdentifiers.new(@@e2.mdl).check).sort)
    expected = ['Attribute Manger::Low:Salary in Manager is not a legal identifier',
                'Operation process in Job is a nondescript identifier',
                "`do' is a reserved word in Java and C++ but was used as a name in role do from Worker"]
    assert_equal_sequences(expected.sort,
                           UmlTools.msgs(UmlTools::Check::CheckIdentifiers.new(@@e3.mdl).check).sort)
    assert_equal(['Unrecognized words in identifiers: loc, wierd'],
                 UmlTools.msgs(UmlTools::Check::CheckIdentifiers.new(@@noerr.mdl).check))
    expected = ['Actor User with no name contains spaces',
                'Unrecognized words in identifiers: dont, wierd']
    assert_equal(expected.sort,
                 UmlTools.msgs(UmlTools::Check::CheckIdentifiers.new(@@uc_e1.mdl).check).sort)
    expected = ['Actor Someone Else contains spaces',
                'Actor WhoAmI? is not a legal identifier',
                "`short' is a reserved word in Java and C++ but was used as a name in actor short",
                'Unrecognized words in identifiers: caar, usr, voiture',
                "`try' is a reserved word in Java and C++ but was used as a name in use case try"
                ]
    assert_equal_sequences(expected.sort,
                           UmlTools.msgs(UmlTools::Check::CheckIdentifiers.new(@@uc_e2.mdl).check.sort))

    # test case where spell check not available
    saved_path = ENV['PATH']
    ENV['PATH'] = ''
    assert_equal(['Spell check not available'],
                 UmlTools.msgs(UmlTools::Check::CheckIdentifiers.new(@@noerr.mdl).check))
    ENV['PATH'] = saved_path
  end

  def test_ck_class_names
    expected = []
    assert_equal(expected.sort, UmlTools::Check::CheckClassNames.new(@@min.mdl).check)
    expected = ['Class short should begin with a capital letter',
                'Avoid mixing class naming styles (using underscores in some names and not others)']
    assert_equal_sequences(expected.sort,
                           UmlTools.msgs(UmlTools::Check::CheckClassNames.new(@@e2.mdl).check).sort)
    assert_equal(['Container vector<> has no contents - nothing between < and >'],
                 UmlTools.msgs(UmlTools::Check::CheckClassNames.new(@@e3.mdl).check).sort)
    assert_equal([], UmlTools::Check::CheckClassNames.new(@@noerr.mdl).check)
  end

  def test_ck_complex_attrs
    expected = ['Complex attribute nums[] in class Stuff - replace by an association to a container',
                'Attribute stuff in class BaseClass appears to not be simple - replace by an association with the class'
               ]
    assert_equal_sequences(expected.sort,
                           UmlTools.msgs(UmlTools::Check::CheckComplexAttributes.new(@@min.mdl).check).sort)
    expected = ['Attribute short in class CamelClass appears to not be simple - replace by an association with the class']
    assert_equal(expected.sort,
                 UmlTools.msgs(UmlTools::Check::CheckComplexAttributes.new(@@e2.mdl).check).sort)
    expected = ['Attributes my_job, yours in class Manager are apparently not simple - replace by associations']
    assert_equal(expected.sort,
                 UmlTools.msgs(UmlTools::Check::CheckComplexAttributes.new(@@e3.mdl).check).sort)
    assert_equal([], UmlTools::Check::CheckComplexAttributes.new(@@noerr.mdl).check)
  end

  def test_ck_documentation
    expected = ['Missing documentation: classes BaseClass, SubClass, Stuff, AltBase',
                'Missing documentation for class BaseClass members: num1, stuff, op1, op2',
                'Missing documentation for class Stuff member: nums[]']
    assert_equal_sequences(expected.sort,
                           UmlTools.msgs(UmlTools::Check::CheckDocumentation.new(@@min.mdl).check).sort)
    expected = ['Missing documentation: classes Some Class, Another_Class, CamelClass, short',
                'Missing documentation for class Some Class members: this stuff, that stuff, find key, WierdOp, no_Consistency, isHere',
                'Missing documentation for class Another_Class members: break, case, CamelClass',
                'Missing documentation for class CamelClass members: int, to_float, extends',
                'Missing documentation for class short members: some_id, index, the_id_for_camel, a&7, a*, b$, 3x7']
    assert_equal_sequences(expected.sort,
                           UmlTools.msgs(UmlTools::Check::CheckDocumentation.new(@@e2.mdl).check).sort)
    expected = ['Missing documentation: class Manager',
                'Missing documentation for class Worker members: type, name, myManagerType',
                'Missing documentation for class Manager members: my_job, yours']
    assert_equal_sequences(expected.sort,
                           UmlTools.msgs(UmlTools::Check::CheckDocumentation.new(@@e3.mdl).check).sort)
    # note: no missing documentation any more since map<> is a library class
    assert_equal([], UmlTools.msgs(UmlTools::Check::CheckDocumentation.new(@@noerr.mdl).check))
    assert_equal_sequences(['Missing documentation: use case DoItNow',
                            'Missing documentation: actors User with no name, ExtraMysteriousUser, ConfusedUser, FarOutUser'].sort,
                           UmlTools.msgs(UmlTools::Check::CheckDocumentation.new(@@uc_e1.mdl).check).sort)
  end

  # TODO: Flexmock
  # def test_ck_documentation_without_support
  #   stubbed = flexmock(@@min.mdl, :format_supports_documentation? => false)
  #   checker = UmlTools::Check::CheckDocumentation.new(stubbed)
  #   expected = []
  #   actual = checker.check
  #   assert_equal_sequences(expected,actual)
  # end

  def test_ck_methods
    expected = []
    assert_equal(expected.sort, UmlTools::Check::CheckMethods.new(@@min.mdl).check)
    expected = ['Avoid mixing naming styles between method names (using underscores in some and not others)']
    assert_equal(expected.sort,
                 UmlTools.msgs(UmlTools::Check::CheckMethods.new(@@e2.mdl).check).sort)
    expected = ['Operation processNames in Manager contains a nondescript word'
                ]
    assert_equal_sequences(expected.sort, UmlTools.msgs(UmlTools::Check::CheckMethods.new(@@e3.mdl).check).sort)
    expected = ['Avoid mixing naming styles between method names (using underscores in some and not others)']
    assert_equal(expected.sort,
                 UmlTools.msgs(UmlTools::Check::CheckMethods.new(@@noerr.mdl).check).sort)
  end

  def test_ck_multi_inherit
    expected = ['Class SubClass inherits from multiple classes (BaseClass, AltBase) - generalization arrows may be backwards']
    assert_equal(expected.sort,
                 UmlTools.msgs(UmlTools::Check::CheckMultiInheritance.new(@@min.mdl).check).sort)
    assert_equal([], UmlTools::Check::CheckMultiInheritance.new(@@e2.mdl).check)
    assert_equal([], UmlTools::Check::CheckMultiInheritance.new(@@e3.mdl).check)
    assert_equal([], UmlTools::Check::CheckMultiInheritance.new(@@noerr.mdl).check)
  end

  def test_ck_multiplicities
    assert_equal([], UmlTools::Check::CheckMultiplicities.new(@@min.mdl).check)
    assert_equal([], UmlTools::Check::CheckMultiplicities.new(@@e2.mdl).check)
    assert_equal([], UmlTools::Check::CheckMultiplicities.new(@@e3.mdl).check)
    expected = ['Missing multiplicities: association between SomeClass and map<string<char>, pair<SomeClass,SomeClass*>>, role list<Short*> in association with Short']
    assert_equal(expected.sort,
                 UmlTools.msgs(UmlTools::Check::CheckMultiplicities.new(@@noerr.mdl).check))
  end

  def test_ck_roles
    assert_equal([], UmlTools::Check::CheckRoles.new(@@min.mdl).check)
    expected = ['Role Another_Class in association with Another_Class: do not use multiplicity 0',
                'Role Another_Class in association with Some Class: class Some Class should be a container',
                "Role theSome Class from Some Class: use multiplicity '*' rather than '0..*'"]
    assert_equal(expected.sort, UmlTools.msgs(UmlTools::Check::CheckRoles.new(@@e2.mdl).check).sort)
    assert_equal(['Role managed from Worker: class Manager should be a container',
                  'Role Responsibility in association with Job: class Job should be a container'],
                 UmlTools.msgs(UmlTools::Check::CheckRoles.new(@@e3.mdl).check))
    expected = ["Role SomeClass in association with Short: use multiplicity '*' rather than '0..*'"]
    assert_equal(expected.sort, UmlTools.msgs(UmlTools::Check::CheckRoles.new(@@noerr.mdl).check))
  end

  def test_ck_standard_classes
    assert_equal([], UmlTools::Check::CheckStandardClasses.new(@@e2.mdl).check)
    assert_equal(['Invalid operation(s) in standard class list<Short*>: extended, operator[]'],
                 UmlTools.msgs(UmlTools::Check::CheckStandardClasses.new(@@noerr.mdl).check))
    assert_equal_sequences(['Container Map<Number*> has 1 argument and needs 2',
                            'Invalid operation(s) in standard class Map<Number*>: add, sort',
                            'Invalid operation(s) in standard class stack<int>: begin, end, iterate, push_back, rotate'].sort,
                 UmlTools.msgs(UmlTools::Check::CheckStandardClasses.new(@@e4.mdl).check).sort)
  end

  def test_ck_hidden
    assert_equal([], UmlTools::Check::CheckHiddenElements.new(@@e3.mdl).check)
    assert_equal(['The following classes are not visible in any view: BeGone, NewClass'],
                 UmlTools.msgs(UmlTools::Check::CheckHiddenElements.new(@@e4.mdl).check))
    assert_equal_sequences(['Actors MissingActor, SecondMissingActor are not visible in any view',
                            'Use case DeletedUC1 is not visible in any view'].sort,
                           UmlTools.msgs(UmlTools::Check::CheckHiddenElements.new(@@uc_e2.mdl).check).sort)
  end

end
