#!/usr/bin/env ruby
# test_runs.rb: test output against known cases
#   Also covers running check as if from the command line.
#
# running just one: ruby test_runs.rb -n test_D_e4_mdl_filtered

require_relative '../test_checks'

class TestRuns < Test::Unit::TestCase
  include BaseModelTests
  include TestBase

  # ensure handle bad input files as gracefully as possible
  def test_0_load_bad_file
    $debug_umlint = false
    old_stdout = $stdout
    $stdout = (output = StringIO.new)

    check_rb_file = File.join($lib_directory, 'check.rb')
    assert_nothing_thrown do
      UmlTools.check_main [check_rb_file]
    end
    $stdout = old_stdout
    assert_equal("Error: #{check_rb_file} does not appear to contain a UML model\n", output.string)
  end

  # try to run UmlTools.check_main with a file that doesn't exist
  def test_1_load_nonexistent_file
    old_stdout = $stdout
    $stdout = (output = StringIO.new)

    nonexistent_file = File.join($test_resource_directory, 'nonexistent.mdl')
    return_code = UmlTools.check_main ['-c', nonexistent_file]
    $stdout = old_stdout
    assert_equal(3, return_code)
    assert_equal("Error: No such checklist file: #{nonexistent_file}\n", output.string)
  end

  def test_A_min_mdl
    expected_text = <<END
Association between Stuff and BaseClass: composition should have multiplicity 1
Attribute stuff in class BaseClass appears to not be simple - replace by an association with the class
Class SubClass inherits from multiple classes (BaseClass, AltBase) - generalization arrows may be backwards
Complex attribute nums[] in class Stuff - replace by an association to a container
Missing documentation for class BaseClass members: num1, stuff, op1, op2
Missing documentation for class Stuff member: nums[]
Missing documentation: classes BaseClass, SubClass, Stuff, AltBase
END
    actual_results = UmlTools::Check::AllChecks.run_all_checks(@@min.mdl, false)
    assert_equal_issues(expected_text, actual_results)

  end

  def test_B_e2_mdl
    expected_text = <<END
Association between short and Some Class has a role name at the wrong end
Attribute index in short names an index - replace by a direct association
Attribute short in class CamelClass appears to not be simple - replace by an association with the class
Attribute some_id in short names an index - replace by a direct association
Attribute that stuff in Some Class contains spaces
Attribute the_id_for_camel in short names an index - replace by a direct association
Attribute this stuff in Some Class contains spaces
Attribute this stuff in Some Class is public
Avoid mixing class naming styles (using underscores in some names and not others)
Avoid mixing naming styles between method names (using underscores in some and not others)
Class CamelClass is not associated with any other class and may be unnecessary
Class Some Class contains spaces
Class short should begin with a capital letter
Missing documentation for class Another_Class members: break, case, CamelClass
Missing documentation for class CamelClass members: int, to_float, extends
Missing documentation for class Some Class members: this stuff, that stuff, find key, WierdOp, no_Consistency, isHere
Missing documentation for class short members: some_id, index, the_id_for_camel, a&7, a*, b$, 3x7
Missing documentation: classes Some Class, Another_Class, CamelClass, short
Unrecognized words in identifiers: wierd
Operation 3x7 in short is not a legal identifier
Operation a&7 in short is not a legal identifier
Operation a* in short is not a legal identifier
Operation b$ in short is not a legal identifier
Operation find key in Some Class contains spaces
Role Another_Class in association with Another_Class: do not use multiplicity 0
Role Another_Class in association with Some Class: class Some Class should be a container
Role theSome Class from Some Class contains spaces
Role theSome Class from Some Class: use multiplicity '*' rather than '0..*'
`break' is a reserved word in Java and C++ but was used as a name in operation break in Another_Class
`case' is a reserved word in Java and C++ but was used as a name in operation case in Another_Class
`extends' is a reserved word in Java but was used as a name in operation extends in CamelClass
`int' is a reserved word in Java and C++ but was used as a name in attribute int in CamelClass
`short' is a reserved word in Java and C++ but was used as a name in attribute short in CamelClass
`short' is a reserved word in Java and C++ but was used as a name in class short
`signed' is a reserved word in C++ but was used as a name in association signed
`try' is a reserved word in Java and C++ but was used as a name in role try from short
END
    assert_equal_issues(expected_text,
                        UmlTools::Check::AllChecks.run_all_checks(@@e2.mdl, false))
  end

  def test_C_e3_mdl
    expected_text = <<END
Association between Worker and Job: composition should have multiplicity 1
Association between Worker and Manager has a role name at the wrong end
Attribute Manger::Low:Salary in Manager is not a legal identifier
Attribute Manger::Low:Salary in Manager should begin with a lower case letter
Attributes my_job, yours in class Manager are apparently not simple - replace by associations
Avoid mixing naming styles between attributes (using underscores in some names and not others)
Class Manager has a dynamic relationship with Job - documenting these is rarely important
Class Worker has two owners: Job and Manager
Container class vector<> appears to have no association to any elements
Container vector<> has no contents - nothing between < and >
Missing documentation for class Manager members: my_job, yours
Missing documentation for class Worker members: type, name, myManagerType
Missing documentation: class Manager
Operation process in Job is a nondescript identifier
Operation processNames in Manager contains a nondescript word
Role Responsibility in association with Job: class Job should be a container
Role managed from Worker: class Manager should be a container
`do' is a reserved word in Java and C++ but was used as a name in role do from Worker
END
    assert_equal_issues(expected_text,
                        UmlTools::Check::AllChecks.run_all_checks(@@e3.mdl, false))
  end

  def test_D_e4_mdl
    expected_text = <<END
Actor UNNAMED_USER is not associated with any use case and may be unnecessary
Class UNNAMED5 is not associated with any other class and may be unnecessary
Class btree<Name> should begin with a capital letter
Container Map<Number*> has 1 argument and needs 2
Container class btree<Name> appears to have no association to any elements
Invalid operation(s) in standard class Map<Number*>: add, sort
Invalid operation(s) in standard class stack<int>: begin, end, iterate, push_back, rotate
Missing documentation: actor UNNAMED_USER
Missing documentation: class UNNAMED5
Missing documentation: use case UNNAMED_ACTION
Role Used_numbers from Map<Number*> should begin with a lower case letter
The following classes are not visible in any view: BeGone, NewClass
Use case UNNAMED_ACTION is not associated with anything and may be unnecessary
END
    actual_issues = UmlTools::Check::AllChecks.run_all_checks(@@e4.mdl,false)
    assert_equal_issues(expected_text,actual_issues)
  end

  def test_D_e4_mdl_filtered
    to_exclude = [ UmlTools::ListOfChecks::CheckItem.new(:INVISIBLE, :invisible_classes),
                   UmlTools::ListOfChecks::CheckItem.new(:NODOC, :ALL),
                   UmlTools::ListOfChecks::CheckItem.new(:REQCAPS, :capitalize_class_names),
                   UmlTools::ListOfChecks::CheckItem.new(:REQCAPS, :nothing),
                   UmlTools::ListOfChecks::CheckItem.new(:INVALIDOP, :invalid_op_in_standard_class)]

    cl =  UmlTools::ListOfChecks::ChecksToExclude.new('uwp', 'cs1', 'hasker', '1.2', 'ruby',
                             to_exclude)
    expected_text = <<END
Actor UNNAMED_USER is not associated with any use case and may be unnecessary
Class UNNAMED5 is not associated with any other class and may be unnecessary
Container Map<Number*> has 1 argument and needs 2
Container class btree<Name> appears to have no association to any elements
Role Used_numbers from Map<Number*> should begin with a lower case letter
Use case UNNAMED_ACTION is not associated with anything and may be unnecessary
END
    all_issues    = UmlTools::Check::AllChecks.run_all_checks(@@e4.mdl, false)
    actual_issues = cl.filtered(all_issues)
    assert_equal_issues(expected_text, actual_issues)

    to_include = [ UmlTools::ListOfChecks::CheckItem.new(:MISSINGELT, :container_without_element),
                   UmlTools::ListOfChecks::CheckItem.new(:WIDOWELEMENT, :none),
                   UmlTools::ListOfChecks::CheckItem.new(:INVISIBLE, :invisible_classes),
                   UmlTools::ListOfChecks::CheckItem.new(:NODOC, :ALL)]
    cl =  UmlTools::ListOfChecks::ChecksToInclude.new('uwp', 'cs1', 'hasker', '1.2', 'ruby',
                             to_include)
    expected_text = <<END
Container class btree<Name> appears to have no association to any elements
Missing documentation: actor UNNAMED_USER
Missing documentation: class UNNAMED5
Missing documentation: use case UNNAMED_ACTION
The following classes are not visible in any view: BeGone, NewClass
END
    actual_issues = cl.filtered(all_issues)
    assert_equal_issues(expected_text, actual_issues)
  end

  def test_E_noerr_mdl
    expected_text = <<END
Avoid mixing naming styles between method names (using underscores in some and not others)
Invalid operation(s) in standard class list<Short*>: extended, operator[]
Missing multiplicities: association between SomeClass and map<string<char>, pair<SomeClass,SomeClass*>>, role list<Short*> in association with Short
Unrecognized words in identifiers: loc, wierd
Role SomeClass in association with Short: use multiplicity '*' rather than '0..*'
END
    actual_issues = UmlTools::Check::AllChecks.run_all_checks(@@noerr.mdl, false)
    assert_equal_issues(expected_text,actual_issues)
  end

#   def test_F_uwpc
#     expected_text = <<END
# A single word was used for the use cases Print, Times - include both a verb and object for each
# Association between Times and User is navigable; change to undirected association
# Association between User and View Weekly Schedule is navigable; change to undirected association
# Attribute _course_id in TSection names an index - replace by a direct association
# Attribute _data in TSection contains a nondescript word
# Attribute session_id in TSession names an index - replace by a direct association
# Missing documentation for class TMeetTime members: _hr, _min, hour, min, toICal
# Missing documentation for class TMeeting members: addBUildings, addRoomsIn, conflictsWith, mtgHour, classHours, mtgPlace, timeIsTBA, meetsIn, session, make
# Missing documentation for class TSection members: _course, _class_nbr, _course_id, _seats, _data, _credits, _closed, _section, _title, _instructor, _program, _college, course, courseId, classNbr, seats, filled, credits, suppressCredits, closed, title, setTitle, program
# Missing documentation for class TSession members: descriptor, weeks_in_session, session_id, _portion_of_term, conflictsWith, portionOfTerm, desc, lookup
# Missing documentation for class TSingleMeeting members: _place, _building, _room, _days, times
# Missing documentation for class TTBAMeetingWithInfo member: _info
# Missing documentation: actor User
# Missing documentation: classes TSection, TSession, TMeetTime, TMeeting, TTBAMeeting, TTBAMeetingWithInfo, TSingleMeeting, TDoubleMeeting
# Missing documentation: use case Times
# Missing multiplicity: role _meetsAt from TMeeting
# Unrecognized words in identifiers: desc, lookup, nbr, uildings
# END
#     actual_issues = UmlTools::Check::AllChecks.run_all_checks(@@uwpc.mdl, false)
#     assert_equal_issues(expected_text, actual_issues)
#   end

  def test_G_uc_sim
    expected_text = <<END
Class Force has a dynamic relationship with Power - documenting these is rarely important
Class Force is not associated with any other class and may be unnecessary
Dependencies such as between EatSupper and EatGreens cannot be named; use <<extend>> and <<include>>
END
    actual_issues = UmlTools::Check::AllChecks.run_all_checks(@@uc_sim.mdl, false)
    assert_equal_issues(expected_text, actual_issues)
  end

  def test_H_uc_e1
    expected_text = <<END
Actor User with no name contains spaces
Actors ConfusedUser, FarOutUser are not associated with any use cases and may be unnecessary
Association between DoItAgain and ConfidenceArtist is either marked with a stereotype or has a name; such associations must be unmarked
Association between DoItNow and MysteryUser is navigable; change to undirected association
Association between User with no name and DoItNow is navigable; change to undirected association
Association between WierdUser and DoItAgain is either marked with a stereotype or has a name; such associations must be unmarked
Association between WierdUser and DoItNow is either marked with a stereotype or has a name; such associations must be unmarked
Association some_association is either marked with a stereotype or has a name; such associations must be unmarked
Missing documentation: actors User with no name, ExtraMysteriousUser, ConfusedUser, FarOutUser
Missing documentation: use case DoItNow
Plain dependencies such as between DoNothing and ConfidenceArtist should not appear in use case diagrams
Plain dependencies such as between DoNothing and DoItAgain should not appear in use case diagrams
Plain dependencies such as between DoNothing and MysteryUser should not appear in use case diagrams
Plain dependencies such as between User with no name and ConfidenceArtist should not appear in use case diagrams
Unrecognized words in identifiers: dont, wierd
Use case DoNothing is not associated with anything and may be unnecessary
Use case Dont Do It is not associated with anything and may be unnecessary
END
    actual_issues = UmlTools::Check::AllChecks.run_all_checks(@@uc_e1.mdl, false)
    assert_equal_issues(expected_text,
                        actual_issues)
  end

  def test_I_uc_e2
    expected_text = <<END
A single word was used for the use cases Caar, try, Sit - include both a verb and object for each
Actor Someone Else contains spaces
Actor WhoAmI? is not a legal identifier
Actor short is not associated with any use case and may be unnecessary
Actors MissingActor, SecondMissingActor are not visible in any view
Association between DoItAgain and Caar is marked as <<extend>> or <<include>>; change to a dependency
Association between DoItAgain and WhoAmI? has multiplicity and should not
Association between DoItAgain and try is marked as <<extend>> or <<include>>; change to a dependency
Association between Fred and DoItAgain has multiplicity and should not
Association between Fred and try is navigable; change to undirected association
Association between try and MysteryUser is navigable; change to undirected association
Dependencies such as between Caar and CloseDoor cannot be named; use <<extend>> and <<include>>
Dependencies such as between break lock and Caar cannot be named; use <<extend>> and <<include>>
Generalization relationships between use cases (ElectricVoiture and Caar) is not allowed
Illegal dependency between OpenDoor and Caar; must use either <<extend>> or <<include>>
Invalid association between DoItAgain and Caar: associate use cases to actors only
Invalid association between DoItAgain and try: associate use cases to actors only
Invalid association between Sit and Caar: associate use cases to actors only
Invalid association between WhoAmI? and short: associate use cases to actors only
Missing documentation: use case Sit
Unrecognized words in identifiers: caar, usr, voiture
Use case CloseDoor is not associated with anything and may be unnecessary
Use case DeletedUC1 is not visible in any view
Use case ElectricVoiture is not associated with anything and may be unnecessary
Use case OpenDoor is not associated with anything and may be unnecessary
Use case break lock is not associated with anything and may be unnecessary
\`short' is a reserved word in Java and C++ but was used as a name in actor short
\`try' is a reserved word in Java and C++ but was used as a name in use case try
END
    actual_issues = UmlTools::Check::AllChecks.run_all_checks(@@uc_e2.mdl, false)
    assert_equal_issues(expected_text,actual_issues)
  end

  def test_J_uc_e3
    # note: Actor Baker is not seen as associated with anything because the
    #       buggy association between Baker and Bake has a spurious reference
    #       listed as the a_role.  Thus the message is correct even though
    #       the diagram makes it look like there is a proper association.
    expected_text = <<END
<<extend>> dependency from Bake to FrostCake may be backwards
<<include>> dependency from FoldBatter to Bake may be backwards
<<include>> dependency from PourFlour to MixInFlour may be backwards
A single word was used for the use case Bake - include both a verb and object
Actor Baker is not associated with any use case and may be unnecessary
Association between unknown element and Bake contains references to missing items
Generalization relationships between use cases (BakePie and Bake) is not allowed
Use case FoldBatter is not associated with anything and may be unnecessary
Use case FrostCake is not associated with anything and may be unnecessary
Use case MeltButter is not associated with anything and may be unnecessary
Use case PourFlour is not associated with anything and may be unnecessary
Use case(s) AddFilling, MixInFlour, RollDough more than 3 links from an actor
END
    actual_issues = UmlTools::Check::AllChecks.run_all_checks(@@uc_e3.mdl, false)
    assert_equal_issues(expected_text,actual_issues)
  end

  def test_I_clean
    assert_equal_issues('',
                        UmlTools::Check::AllChecks.run_all_checks(@@clean.mdl, false))
  end

  def test_K_e5
    expected_text = <<END
Actor delete is not visible in any view
Attribute AppleCount in Sack should begin with a lower case letter
Attribute fullFlag in Sack contains a nondescript word
Attributes sack, theCashier in class HamAndCheese are apparently not simple - replace by associations
Avoid mixing naming styles between attributes (using underscores in some names and not others)
Class ApplesOrOranges has 'or' in it - rename or use generalization
Class ApplesOrOranges has dynamic relationships with Worm, Mold - documenting these is rarely important
Class Disease has two owners: Worm and Mold
Class HamAndCheese has 'and' in it - rename or split into two classes
Complex attribute condiments_list in class HamAndCheese - replace by an association to a container
Complex attributes fruit_list, worm_list in class ApplesOrOranges - replace by associations with containers
Complex attributes apples[], _oranges[][] in class ApplesOrOranges - replace by associations with containers
Illegal dependency between Break_down_objects and Stuff sack; must use either <<extend>> or <<include>>
Missing documentation for class ApplesOrOranges member: fruit_list
Missing documentation: use cases Stuff sack, Break_down_objects
Missing multiplicities: role Disease in association with Mold, role Disease in association with Worm
Role ApplesOrOranges in association with Sack: class Sack should be a container
Role HamAndCheese in association with Sack: class Sack should be a container
Role Sack in association with Cashier: actor Cashier should be a container
Use case Break_down_objects is not associated with anything and may be unnecessary
Use cases try, second deleted use case are not visible in any view
\`delete' is a reserved word in C++ but was used as a name in actor delete
\`try' is a reserved word in Java and C++ but was used as a name in use case try
END
    assert_equal_issues(expected_text,
                        UmlTools::Check::AllChecks.run_all_checks(@@e5.mdl, false))
  end

  def test_L_e6
    expected_text = <<END
Association between B and no-such-id contains references to missing items
Use case done is not visible in any view
END
    assert_equal_issues(expected_text,
                        UmlTools::Check::AllChecks.run_all_checks(@@e6.mdl, false))
  end

  def test_M_e7
    expected_text = <<END
A single word was used for the use case Control - include both a verb and object
Association between Bowl and Pet has a role name at the wrong end
Association between FillFoodBowl and MyDog is a duplicated association - remove the extra association
Class FoodBowl is not an actor and should not appear on a use case diagram
Class WaterBowl is not an actor and should not appear on a use case diagram
Use case Control is a nondescript identifier
Use case(s) BuyFood, EatFood, TakeFoodHome more than 3 links from an actor
END
    assert_equal_issues(expected_text,
                        UmlTools::Check::AllChecks.run_all_checks(@@e7.mdl, false))
  end

  # test running from command line
  def test_check_main
    $debug_umlint = false
    old_stdout = $stdout

    $stdout = (help_output = StringIO.new)
    help_return = UmlTools.check_main %w(--help)

    $stdout = (msg_output = StringIO.new)
    min_bad_location = File.join($test_resource_directory, 'min-bad.mdl')
    msg_return = UmlTools.check_main [min_bad_location]

    assert !$debug_umlint
    # note: following test sets options that will have to be unset
    $stdout = (clean_output = StringIO.new)
    clean_mdl_location = File.join($test_resource_directory, 'clean.mdl')
    clean_return  = UmlTools.check_main ['-f', 'xxx.mdl', '-d', clean_mdl_location]
    # reset options
    $debug_umlint = false
    assert !$debug_umlint

    $stdout = (error_output = StringIO.new)
    error_return = UmlTools.check_main %w(model.rb)

    # note: following test sets options that will have to be unset
    assert !UmlTools::Issue.echo?
    assert !$debug_umlint
    $stdout = (chklist_output = StringIO.new)
    petal_file_location = File.join($lib_directory,'umlTools', 'model',  'model.rb')
    chklist_return = UmlTools.check_main ['-c', petal_file_location, '-d' ,'-i', '-v', clean_mdl_location]
    assert UmlTools::Issue.echo?
    assert $debug_umlint
    # reset options
    UmlTools::Issue.set_echo_ids(false)
    $debug_umlint = false
    assert !UmlTools::Issue.echo?
    assert !$debug_umlint


    # verbose output, but bad file
    $stdout = (verbose_output = StringIO.new)
    verbose_return = UmlTools.check_main ['-v', '-i', petal_file_location]
    assert UmlTools::Issue.echo?
    # reset options
    UmlTools::Issue.set_echo_ids(false)
    assert !UmlTools::Issue.echo?

    # slightly bad file, with verbose mode off:
    $stdout = (syntax_output = StringIO.new)
    syntax_error_file_location = File.join($test_resource_directory, 'syntax-error1.mdl')
    syntax_return = UmlTools.check_main [syntax_error_file_location]

    $stdout = old_stdout

    assert_equal 'Usage:', help_output.string.split[0]
    assert_equal 1, help_return

    assert_equal 0, msg_return
    expected = <<HERE
Association between Stuff and BaseClass: composition should have multiplicity 1
Attribute stuff in class BaseClass appears to not be simple - replace by an association with the class
Class SubClass inherits from multiple classes (BaseClass, AltBase) - generalization arrows may be backwards
Complex attribute nums[] in class Stuff - replace by an association to a container
Missing documentation for class BaseClass members: num1, stuff, op1, op2
Missing documentation for class Stuff member: nums[]
Missing documentation: classes BaseClass, SubClass, Stuff, AltBase
HERE
    assert_equal expected, msg_output.string

    assert_equal 2, error_return
    assert_equal 'Error: model.rb does not appear to contain a UML model',
                 error_output.string.chomp

    assert_equal 0, clean_return
    assert_equal 'No issues identified, but be sure to review the model carefully.',
                 clean_output.string.chomp

    assert_equal chklist_return, 3
    assert_equal "Error: checklist file #{petal_file_location} has invalid format",
                 chklist_output.string.chomp

    assert_equal 2, verbose_return
    assert_equal 'BAD_MODEL.petal_error.Error: file does not appear to contain a UML model',
                  verbose_output.string.chomp

    assert_equal 0, syntax_return
    expected_errors = <<HERE
Complex attribute list[] in class Stack - replace by an association to a container
Syntax error in the model: Missing id field for UmlTools::Model::Method
Syntax error in the model: reference to missing super: 4BABCE46008C
Syntax error in the model: unexpected atom for attributes: oops
Syntax error in the model: unexpected atom for methods: none
Unrecognized words in identifiers: calc, shft
HERE
    assert_equal expected_errors, syntax_output.string
  end

  # test running from command line with -v option set
  # Note: this test resets $stderr, so must be careful about adding
  #       tests to this case since any error output will be hard to see
  def test_check_verbose_output
    old_stdout = $stdout
    old_stderr = $stderr

    # verbose output, but bad file
    $stdout = (verbose_output = StringIO.new)
    $stderr = StringIO.new

    # also specify printing metrics for coverage
    clean_file_location = File.join($test_resource_directory, 'clean.mdl')
    verbose_return = UmlTools.check_main ['-v', '-m', clean_file_location]

    $stdout = old_stdout
    $stderr = old_stderr

    expected = <<HERE
No issues identified, but be sure to review the model carefully.
Use case model size: 5 actors, 4 cases, 4 associations
Use case complexity: 17
Class model size: 6 classes w/ 0 attributes and 0 methods, 6 associations
Class complexity: 6
HERE
    assert_equal expected, verbose_output.string
    assert_equal 0, verbose_return
  end

  # TODO: Flexmock
  # must 'provide generic ChecksToExclude when no empty checks are provided in UmlTools.check_main args' do
  #   old_stdout = $stdout
  #   old_stderr = $stderr
  #   $stdout = StringIO.new
  #   $stderr =  StringIO.new
  #
  #   expected =  UmlTools::ListOfChecks::ChecksToExclude.new('empty', 'generic', 'none', '0', '', [])
  #   flexmock( UmlTools::ListOfChecks::ChecksToExclude).new_instances do |actual_instance|
  #     #TODO (rwh, sep 2016): fix following test, but note this may work in later versions of Rails
  #     #assert_equal expected, actual_instance
  #   end
  #
  #   partial_mock = flexmock(UmlTools)
  #   partial_mock.should_receive(:check_results).once.and_return []
  #   clean_mdl_location = File.join($test_resource_directory, 'clean.mdl')
  #   partial_mock.method(:check_main).call ['-c', '[]',clean_mdl_location]
  #
  #   $stdout = old_stdout
  #   $stderr = old_stderr
  #
  # end


  def test_check_silent_output
    old_stdout = $stdout
    old_stderr = $stderr

    # verbose output, but bad file
    $stdout = (silent_output = StringIO.new)
    $stderr =  StringIO.new

    # also specify printing metrics for coverage
    clean_file_location = File.join($test_resource_directory, 'clean.mdl')
    silent_return = UmlTools.check_main ['-s', '-m', clean_file_location]

    $stdout = old_stdout
    $stderr = old_stderr

    expected = <<HERE
Use case model size: 5 actors, 4 cases, 4 associations
Use case complexity: 17
Class model size: 6 classes w/ 0 attributes and 0 methods, 6 associations
Class complexity: 6
HERE
    assert_equal expected, silent_output.string
    assert_equal 0, silent_return
  end

end
