#!/usr/bin/env ruby
# testing core umlint output
require_relative '../test_checks'
require_relative '../../../lib/umlTools/model/sequence/full_model'
require_relative '../../../lib/umlTools/model/sequence/lifeline'
require_relative '../../../lib/umlTools/model/sequence/message'
require_relative '../../../lib/umlTools/model/sequence/message_argument'

class TestSequenceChecks < Test::Unit::TestCase
  include BaseModelTests
  include TestBase

  def blank_model
    # Creates a model with a two lifelines (Sender & Receiver), one message (with one arg)
    # with every string field (besides lifeline name) equal to ''
    sender = UmlTools::Model::Sequence::Lifeline.new 1, '', 'Sender', '', nil, []
    receiver = UmlTools::Model::Sequence::Lifeline.new 2, '', 'Receiver', '', nil, []
    arg = UmlTools::Model::Sequence::MessageArgument.new 1, '', ''
    message = UmlTools::Model::Sequence::Message.new 1, '', '', '', sender, receiver, [arg], []
    lifelines = [sender, receiver]
    UmlTools::Model::Sequence::FullModel.new lifelines, [message]
  end

  def test_ck_message_labels
    expected = ['Message going from Sender to Receiver does not have a method signature',
                'Message going from Sender to Receiver has an unnamed argument']
    # seq_message_labels, seq_argument_labels
    assert_equal(expected.sort,
                 UmlTools.msgs(UmlTools::Check::Sequence::CheckMessageLabels.new(self.blank_model).check).sort)
  end

  def test_ck_message_type
    expected = ['Message going from Sender to Receiver has an argument with no type specified.']
    assert_equal(expected.sort,
                 UmlTools.msgs(UmlTools::Check::Sequence::CheckMessageTypes.new(self.blank_model).check).sort)
  end

  def test_ck_guard_conditions
    model = self.blank_model
    guard = 'tt=1s && tt==12 || (f#s != tt*2) || ((bool_test) && 2>3 && 2<(3-0/1+1) && 2>=3 || 2<=3 && 5%2==1'
    model.messages[0].guard = guard
    pre_msg = 'Message going from Sender to Receiver has an invalid guard condition. '
    expected = [
        pre_msg + "(Guard: #{guard} Error: Number of opening parenthesis does not match number of closing.",
        pre_msg + "(Guard: #{guard}, Invalid portion: tt=1s, Error: Accidental assignment operator, should be ==.",
        pre_msg + "(Guard: #{guard}, Invalid portion: f#s!=tt*2, Error: Invalid characters found, only a-z & A-Z & 0-9 & _ is allowed.",
        pre_msg + "(Guard: #{guard}, Invalid portion: tt=1s, Error: First character of a variable cannot be a number."
    ]
    assert_equal(expected.sort,
                 UmlTools.msgs(UmlTools::Check::Sequence::CheckGuardConditions.new(model).check).sort)
  end

  def test_ck_lifeline_interactions
    actor = UmlTools::Model::Sequence::Lifeline.new 1, 'uml:Actor', 'Actor', '', nil, []
    boundary = UmlTools::Model::Sequence::Lifeline.new 2, '', 'Boundary', 'boundary', nil, []
    controller = UmlTools::Model::Sequence::Lifeline.new 3, '', 'Controller', 'controller', nil, []
    entity = UmlTools::Model::Sequence::Lifeline.new 4, '', 'Entity', 'entity', nil, []
    lifelines = [actor, boundary, controller, entity]
    messages = []
    messages << UmlTools::Model::Sequence::Message.new(1, '', '', '', actor, boundary, [], [])
    messages << UmlTools::Model::Sequence::Message.new(2, '', '', '', boundary, actor, [], [])
    messages << UmlTools::Model::Sequence::Message.new(3, '', '', '', boundary, controller, [], [])
    messages << UmlTools::Model::Sequence::Message.new(4, '', '', '', controller, boundary, [], [])
    messages << UmlTools::Model::Sequence::Message.new(5, '', '', '', controller, entity, [], [])
    messages << UmlTools::Model::Sequence::Message.new(6, '', '', '', entity, controller, [], [])
    model = UmlTools::Model::Sequence::FullModel.new lifelines, messages
    assert_empty(UmlTools.msgs(UmlTools::Check::Sequence::CheckLifelineInteraction.new(model).check))
    messages = []
    messages << UmlTools::Model::Sequence::Message.new(1, '', '', '', actor, controller, [], [])
    messages << UmlTools::Model::Sequence::Message.new(2, '', '', '', actor, entity, [], [])
    messages << UmlTools::Model::Sequence::Message.new(3, '', '', '', boundary, entity, [], [])
    messages << UmlTools::Model::Sequence::Message.new(4, '', '', '', controller, actor, [], [])
    messages << UmlTools::Model::Sequence::Message.new(5, '', '', '', entity, boundary, [], [])
    messages << UmlTools::Model::Sequence::Message.new(6, '', '', '', entity, actor, [], [])
    expected = [
        'Message going from Actor to Controller involves an invalid interaction!',
        'Message going from Actor to Entity involves an invalid interaction!',
        'Message going from Boundary to Entity involves an invalid interaction!',
        'Message going from Controller to Actor involves an invalid interaction!',
        'Message going from Entity to Boundary involves an invalid interaction!',
        'Message going from Entity to Actor involves an invalid interaction!',
    ]
    model = UmlTools::Model::Sequence::FullModel.new lifelines, messages
    assert_equal(expected.sort, UmlTools.msgs(UmlTools::Check::Sequence::CheckLifelineInteraction.new(model).check).sort)
  end
end

