#!/usr/bin/env ruby
# Testing the Role class

require_relative '../test_base'

class TestMethod < Test::Unit::TestCase
  include TestBase

  def setup
    test_role_xmi = <<HERE
  <?xml version="1.0" encoding="windows-1252"?>
  <xmi:XMI xmi:version="2.1" xmlns:uml="http://schema.omg.org/spec/UML/2.1" xmlns:xmi="http://schema.omg.org/spec/XMI/2.1" xmlns:thecustomprofile="http://www.sparxsystems.com/profiles/thecustomprofile/1.0">
    <xmi:Documentation exporter="Enterprise Architect" exporterVersion="6.5"/>
    <uml:Model xmi:type="uml:Model" name="EA_Model" visibility="public">
      <packagedElement xmi:type="uml:Package" xmi:id="EAPK_9EB73FD5_CC34_40f3_AE08_2D1B268C24D3" name="Model" visibility="public">
        <packagedElement xmi:type="uml:Package" xmi:id="EAPK_D87357D7_03EE_4c6c_A279_A7ADFFCB0BA2" name="Class Model" visibility="public">
          <packagedElement xmi:type="uml:Association" xmi:id="EAID_DB58177C_24E2_475c_A8D4_6AE2E97533F9" visibility="public">
            <memberEnd xmi:idref="memberEndId"/>
            <ownedEnd xmi:type="uml:Property" xmi:id="EAID_dst58177C_24E2_475c_A8D4_6AE2E97533F9" visibility="public" association="EAID_DB58177C_24E2_475c_A8D4_6AE2E97533F9" isStatic="false" isReadOnly="false" isDerived="false" isOrdered="false" isUnique="true" isDerivedUnion="false" aggregation="none">
              <type xmi:idref="EAID_E2539076_CFA6_4a02_B07F_CF441924B87A"/>
            </ownedEnd>
            <memberEnd xmi:idref="memberEndId2"/>
            <ownedEnd xmi:type="uml:Property" xmi:id="EAID_src58177C_24E2_475c_A8D4_6AE2E97533F9" visibility="public" association="EAID_DB58177C_24E2_475c_A8D4_6AE2E97533F9" isStatic="false" isReadOnly="false" isDerived="false" isOrdered="false" isUnique="true" isDerivedUnion="false" aggregation="none">
              <type xmi:idref="EAID_112524D5_A4C7_49b2_8C23_0E559D2CC0F4"/>
            </ownedEnd>
          </packagedElement>
        </packagedElement>
      </packagedElement>
    </uml:Model>
    <xmi:Extension extender="Enterprise Architect" extenderID="6.5">
      <connectors>
        <connector xmi:idref="EAID_DB58177C_24E2_475c_A8D4_6AE2E97533F9">
          <source xmi:idref="sourceId">
            <model ea_localid="14" type="Class" name="Super"/>
            <role visibility="Public" stereotype="aStereotype" targetScope="instance"/>
            <type multiplicity="1" aggregation="none" containment="Unspecified"/>
            <constraints/>
            <modifiers isOrdered="false" changeable="none" isNavigable="true"/>
            <style value="Union=0;Derived=0;AllowDuplicates=0;Owned=0;Navigable=Unspecified;alias=roleName;"/>
            <documentation value="This is documentation"/>
            <xrefs/>
            <tags/>
          </source>
          <target xmi:idref="EAID_E2539076_CFA6_4a02_B07F_CF441924B87A">
            <model ea_localid="17" type="Class" name="SuperAssociate"/>
            <role visibility="Public" targetScope="instance"/>
            <type aggregation="none" containment="Unspecified"/>
            <constraints/>
            <modifiers isOrdered="false" changeable="none" isNavigable="false"/>
            <style value="Union=0;Derived=0;AllowDuplicates=0;Owned=0;Navigable=Unspecified;"/>
            <documentation/>
            <xrefs/>
            <tags/>
          </target>
          <model ea_localid="10"/>
          <properties ea_type="Association" direction="Unspecified"/>
          <modifiers isRoot="false" isLeaf="false"/>
          <parameterSubstitutions/>
          <documentation/>
          <appearance linemode="3" linecolor="-1" linewidth="0" seqno="0" headStyle="0" lineStyle="0"/>
          <labels/>
          <extendedProperties virtualInheritance="0"/>
          <style/>
          <xrefs/>
          <tags/>
        </connector>
      </connectors>
    </xmi:Extension>
  </xmi:XMI>
HERE
    doc = REXML::Document.new(test_role_xmi)
    @role_element = doc.elements['//memberEnd[1]']
    @role_details = doc.elements['//source']
  end

  # Tests the read_from_xmi method with no associativity to ensure basic attributes
  # are populated correctly.
  def test_read_from_xmi
    expected_id = 'memberEndId'
    expected_supplier_id = 'sourceId'
    expected_doc = 'This is documentation'
    expected_stereotype = 'astereotype'
    expected_multiplicity = '1'
    expected_associativity = :plain
    expected_navigability = true
    expected_name = 'roleName'

    role = UmlTools::Model::Role.read_from_xmi(@role_element, @role_details)

    assert_equal(expected_id, role.id)
    assert_equal(expected_doc, role.doc)
    assert_equal(expected_supplier_id, role.supplier_id)
    assert_equal(expected_stereotype, role.stereotype)
    assert_equal(expected_multiplicity, role.multiplicity)
    assert_equal(expected_associativity, role.assoc_type)
    assert_equal(expected_navigability, role.navigable?)
    assert_equal(expected_name, role.name)
  end

  # Tests the read_from_xmi method with composite associativity to ensure the
  # assoc_type is set correctly.
  def test_read_from_xmi_composite
    expected_associativity = :composite

    type = @role_details.elements['type']
    if(type)
      type.attributes['aggregation'] = 'composite'
    end

    role = UmlTools::Model::Role.read_from_xmi(@role_element, @role_details)

    assert_equal(expected_associativity, role.assoc_type)
  end

  # Tests the read_from_xmi method with aggregate associativity to ensure
  # the assoc_type is set correctly.
  def test_read_from_xmi_aggregate
    expected_associativity = :aggregate

    type = @role_details.elements['type']
    if(type)
      type.attributes['aggregation'] = 'shared'
    end

    role = UmlTools::Model::Role.read_from_xmi(@role_element, @role_details)

    assert_equal(expected_associativity, role.assoc_type)
  end

  # Tests the ability to set a name on the Role to ensure the attribute
  # is set correctly
  def test_set_name
    expected_name = 'Joe'
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, nil, nil, nil)
    role.name = 'Joe'
    assert_equal(expected_name, role.name)
  end

  # Tests the navigable? method to ensure it returns true when the Role is navigable.
  def test_is_navigable_true
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, nil, true, nil)
    assert(role.navigable?)
  end

  # Tests the has_supplier? method to ensure it returns true if there is a supplier with a name.
  def test_has_supplier
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, 'Id', nil, true, nil)
    supplier = UmlTools::Model::MClass.new('supName', 'Id', nil, nil, nil, nil, nil, nil)
    role.supplier = supplier
    assert(role.has_supplier?)
  end

  # Tests the has_supplier? method to ensure it returns false if there is not supplier on the Role.
  def test_has_supplier_no_name
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, 'Id', nil, true, nil)
    supplier = UmlTools::Model::MClass.new(nil, 'Id', nil, nil, nil, nil, nil, nil)
    role.supplier = supplier
    assert(!role.has_supplier?)
  end

  # Tests supplier_name with a supplier that has a name to ensure it is returned.
  def test_supplier_name_with_name
    expected_name = 'supName'
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, 'Id', nil, true, nil)
    supplier = UmlTools::Model::MClass.new('supName', 'Id', nil, nil, nil, nil, nil, nil)
    role.supplier = supplier
    assert_equal(expected_name, role.supplier_name)
  end

  # Tests supplier_name with a supplier that has no name, but does have an id, to ensure the
  # id is returned instead.
  def test_supplier_name_with_id
    expected_name = 'Id'
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, 'Id', nil, true, nil)
    supplier = UmlTools::Model::MClass.new(nil, 'Id', nil, nil, nil, nil, nil, nil)
    role.supplier = supplier
    assert_equal(expected_name, role.supplier_name)
  end

  # Tests supplier_name without a supplier to ensure 'unknown element' is returned.
  def test_supplier_name_missing
    expected_name = 'unknown element'
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, nil, true, nil)
    assert_equal(expected_name, role.supplier_name)
  end

  # Tests consumer to ensure that the role.consumer.supplier is returned.
  def test_consumer
    consumer = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, nil, nil, nil)
    consumer_supplier = UmlTools::Model::MClass.new('Consumer', nil, nil, nil, nil, nil, nil, nil)
    consumer.supplier = consumer_supplier
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, nil, true, nil)
    role.consumer_role = consumer
    assert_equal(consumer_supplier, role.consumer)
  end

  # Tests consumer_role_has_name? to ensure that it returns true when role.consumer has a name.
  def test_consumer_role_has_name
    consumer = UmlTools::Model::Role.new('Name', nil, nil, nil, nil, nil, nil, nil)
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, nil, true, nil)
    role.consumer_role = consumer
    assert(role.consumer_role_has_name?)
  end

  # Tests consumer_class to ensure it returns the role.consumer.supplier if consumer.supplier
  # exists and is an instance of MClass.
  def test_consumer_class
    consumer = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, nil, nil, nil)
    consumer_supplier = UmlTools::Model::MClass.new('Consumer', nil, nil, nil, nil, nil, nil, nil)
    consumer.supplier = consumer_supplier
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, nil, true, nil)
    role.consumer_role = consumer
    assert_equal(consumer_supplier, role.consumer_class)
  end

  # Tests the full_name method with a Role that has a name to ensure it concatenates
  # the role name and the consumer name.
  def test_full_name_with_name
    expected_full_name = 'RoleName/ConsumerName'
    consumer = UmlTools::Model::Role.new('ConsumerName', nil, nil, nil, nil, nil, nil, nil)
    role = UmlTools::Model::Role.new('RoleName', nil, nil, nil, nil, nil, true, nil)
    role.consumer_role = consumer
    assert_equal(expected_full_name, role.full_name)
  end

  # Tests the full_name method with a Role that does not have a name to ensure it
  # returns the consumer name only.
  def test_full_name_without_name
    expected_full_name = 'ConsumerName'
    consumer = UmlTools::Model::Role.new('ConsumerName', nil, nil, nil, nil, nil, nil, nil)
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, nil, true, nil)
    role.consumer_role = consumer
    assert_equal(expected_full_name, role.full_name)
  end

  # TODO: Flexmock
  # Tests the ident method with  a Role that has a name to ensure the correct string is generated.
  # def test_ident_with_name
  #   expected_ident = 'role name from supName'
  #   role = UmlTools::Model::Role.new('name', nil, nil, nil, nil, nil, true, nil)
  #   flexmock(role, :supplier_name => 'supName')
  #   assert_equal(expected_ident, role.ident)
  # end

  # TODO: Flexmock
  # Tests the ident method with a Role that does not have a name to ensure the correct string is generated.
  # def test_ident_without_name
  #   expected_ident = 'role supName in association with consumerSupName'
  #   role = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, nil, true, nil)
  #   flexmock(role, :supplier_name=>'supName')
  #   mock_consumer = flexmock('supplier_name'=>'consumerSupName')
  #   role.consumer_role = mock_consumer
  #   assert_equal(expected_ident, role.ident)
  # end

  # Tests the missing_multiplicity? method with a composite associativity to ensure it always returns false.
  def test_missing_multiplicity_composite
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, nil, true, :composite)
    assert(!role.missing_multiplicity?)
  end

  # Tests the missing multiplicity? method with a non-composite Role with a nil multiplicity.
  def test_missing_multiplicity_nil_multiplicity
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, nil, true, :plain)
    assert(role.missing_multiplicity?)
  end

  # Tests the missing multiplicity? method with a non-composite Role with a multiplicity of ''.
  def test_missing_multiplicity_empty_multiplicity
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, '', true, :plain)
    assert(role.missing_multiplicity?)
  end

  # Tests the single_element_multiplicity method with a composite role which results in a return value of true.
  def test_single_element_multiplicity_composite
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, nil, true, :composite)
    assert(role.single_element_multiplicity?)
  end

  # Tests the single_element_multiplicity method with a nil multiplicity which results in a return value of false.
  def test_single_element_multiplicity_nil_multiplicity
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, nil, true, :plain)
    assert(!role.single_element_multiplicity?)
  end

  # Tests the single_element_multiplicity method with a singular multiplicity which results in a return value of true.
  def test_single_element_multiplicity_basic_multiplicity
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, '0..1', true, :plain)
    assert(role.single_element_multiplicity?)
  end

  # Tests the single_element_multiplicity method with a mutliple element multiplicity which results in a return
  # value of false.
  def test_single_element_multiplicity_multiple_element
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, '*', true, :plain)
    assert(!role.single_element_multiplicity?)
  end

  # Tests the multiple_element_multiplicity method with a nil multiplicity which results in a return value of false.
  def test_multiple_element_multiplicity_nil_multiplicity
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, nil, true, :plain)
    assert(!role.multiple_element_multiplicity?)
  end

  # Tests the multiple_element_multiplicity method with a singular multiplicity which results in a return value
  # of false.
  def test_multiple_element_multiplicity_single_element
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, '1', true, :plain)
    assert(!role.multiple_element_multiplicity?)
  end

  # Tests the multiple_element_multiplicity method with a multiple element multiplicity which results in a
  # return value of false.
  def test_multiple_element_multiplicity
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, '*', true, :plain)
    assert(role.multiple_element_multiplicity?)
  end

  # Tests the effective_multiplicity method with a one-to-one multiplicity resulting in an effective multiplicity of 1.
  def test_effective_multiplicity_one_to_one
    expected_mult = '1'
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, '1..1', true, :plain)
    assert_equal(expected_mult, role.effective_multiplicity)
  end

  # Tests the effective_multiplicity method with a composite role resulting in an effective multiplicity of 1.
  def test_effective_multiplicity_composite
    expected_mult = '1'
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, nil, true, :composite)
    assert_equal(expected_mult, role.effective_multiplicity)
  end

  # Tests the effective_multiplicity method with a non-one-to-one multiplicity resulting in an effective
  # multiplicity of the original multiplicity.
  def test_effective_multiplicity_multiple_element
    expected_mult = '*'
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, '*', true, :plain)
    assert_equal(expected_mult, role.effective_multiplicity)
  end

  # Tests the effective_multiplicity method with an empty multiplicity resulting in an effective multiplicity of ''.
  def test_effective_multiplicity_empty
    expected_mult = ''
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, '', true, :plain)
    assert_equal(expected_mult, role.effective_multiplicity)
  end

  # Tests the effective_multiplicity method with a nil multiplicity resulting in an effective multiplicity of ''.
  def test_effective_multiplicity_nil
    expected_mult = ''
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, nil, true, :plain)
    assert_equal(expected_mult, role.effective_multiplicity)
  end

  # Tests the aggregate? method to ensure it returns true when the role represents aggregation.
  def test_aggregate
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, nil, true, :aggregate)
    assert(role.aggregate?)
  end

  # Tests the composite? method to ensure it returns true when the role represents composition.
  def test_composite
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, nil, true, :composite)
    assert(role.composite?)
  end

  # Tests the ok_composite_multiplicity? method with a nil multiplicity to ensure it returns true.
  def test_ok_composite_multiplicity_nil
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, nil, true, :composite)
    assert(role.ok_composite_multiplicity?)
  end

  # Tests the ok_composite_multiplicity? method with an empty multiplicity to ensure it returns true.
  def test_ok_composite_multiplicity_empty
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, '', true, :composite)
    assert(role.ok_composite_multiplicity?)
  end

  # Tests the ok_composite_multiplicity? method with a single multiplicity to ensure it returns true.
  def test_ok_composite_multiplicity_one
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, '1', true, :composite)
    assert(role.ok_composite_multiplicity?)
  end

  # Tests the ok_composite_multiplicity? method with a one-to-one multiplicity to ensure it returns true.
  def test_ok_composite_multiplicity_one_to_one
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, '1..1', true, :composite)
    assert(role.ok_composite_multiplicity?)
  end

  # Tests the ok_composite_multiplicity? method with a multiple element multiplicity to ensure it returns false.
  def test_ok_composite_multiplicity_multiple_element
    role = UmlTools::Model::Role.new(nil, nil, nil, nil, nil, '*', true, :composite)
    assert(!role.ok_composite_multiplicity?)
  end

  # Tests the to_s method with valid name and supplier name, and a navigable role to ensure the correct string
  # is returned.
  def test_to_s_with_names_and_navigable
    expected_string = '(Role supName (name: Name) 1..1 navigable composite)'
    role = UmlTools::Model::Role.new('Name', 'Id', nil, nil, nil, '1..1', true, :composite)
    supplier = UmlTools::Model::MClass.new('supName', nil, nil, nil, nil, nil, nil, nil)
    role.supplier = supplier
    assert_equal(expected_string, role.to_s)
  end

  # Tests the to_s method without a valid name and supplier name, and a non-navigable role to ensure the correct
  # string is returned.
  def test_to_s_without_names_and_not_navigable
    expected_string = '(Role * aggregate)'
    role = UmlTools::Model::Role.new(nil, 'Id', nil, nil, nil, '*', false, :aggregate)
    assert_equal(expected_string, role.to_s)
  end

  # TODO: Flexmock
  # Tests the dump method with a valid name and supplier name, and a naviagable role to ensure the correct string
  # is returned.
  # def test_dump_with_names_and_navigable
  #   expected_string = '(role SupName (name Name) 1..1 navigable composite abbreviation (stereo stereotype) (doc Documentation))'
  #   UmlTools::Model::Element.print_just_ids = false
  #   role = UmlTools::Model::Role.new('Name', 'Id', 'Stereotype', 'Documentation', 'SupId', '1..1', true, :composite)
  #   supplier = UmlTools::Model::MClass.new('SupName', 'SupId', nil, nil, nil, nil, nil, nil)
  #   role.container = flexmock('abbrev'=>'abbreviation')
  #   role.supplier = supplier
  #   assert_equal(expected_string, role.dump)
  # end

  # TODO: Flexmock
  # Tests the dump method without a valid name and supplier name, and a non-navigable role to ensure the correct
  # string is returned.
  # def test_dump_without_names_and_not_navigable
  #   expected_string = '(role * aggregate abbreviation (stereo stereotype) (doc Documentation))'
  #   UmlTools::Model::Element.print_just_ids = false
  #   role = UmlTools::Model::Role.new(nil, 'Id', 'Stereotype', 'Documentation', 'SupId', '*', false, :aggregate)
  #   supplier = UmlTools::Model::MClass.new(nil, 'SupId', nil, nil, nil, nil, nil, nil)
  #   role.container = flexmock('abbrev'=>'abbreviation')
  #   role.supplier = supplier
  #   assert_equal(expected_string, role.dump)
  # end

  # Tests the dump method with print_just_ids set to true to ensure the correct string is returned.
  def test_dump_print_just_ids
    expected_string = '(role Id SupId)'
    UmlTools::Model::Element.print_just_ids = true
    role = UmlTools::Model::Role.new('Name', 'Id', 'Stereotype', 'Documentation', 'SupId', '1..1', true, :composite)
    supplier = UmlTools::Model::MClass.new('SupName', 'SupId', nil, nil, nil, nil, nil, nil)
    role.supplier = supplier
    assert_equal(expected_string, role.dump)
  end
end
