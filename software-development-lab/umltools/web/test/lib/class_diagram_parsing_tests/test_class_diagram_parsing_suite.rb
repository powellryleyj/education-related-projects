require_relative 'test_associable_element'
require_relative 'test_association'
require_relative 'test_attribute'
require_relative 'test_connector'
require_relative 'test_generalization_relationship'
require_relative 'test_logical_model'
require_relative 'test_m_class'
require_relative 'test_method'

class TestClassDiagramParsingSuite < Test::Unit::TestCase
end
