#!/usr/bin/env ruby
# Testing the Attribute class
# Currently tests EA Parsing.  For Rhapsody, see test_model.rb

require_relative '../test_base'

class TestAttribute < Test::Unit::TestCase
  include TestBase

  @@attribute_xmi = <<HERE
  <?xml version="1.0" encoding="windows-1252"?>
  <xmi:XMI xmi:version="2.1" xmlns:uml="http://schema.omg.org/spec/UML/2.1" xmlns:xmi="http://schema.omg.org/spec/XMI/2.1" xmlns:thecustomprofile="http://www.sparxsystems.com/profiles/thecustomprofile/1.0">
		<attribute xmi:idref="EAID_1" name="name" scope="Public">
      <documentation value="Doc"/>
      <properties type="int" derived="0" collection="false" duplicates="0" changeability="changeable"/>
      <stereotype stereotype="Stereotype"/>
    </attribute>
  </xmi:XMI>
HERE

  # type, stereotype, documentation, and scope are optional
  @@attribute_xmi_empty_fields = <<HERE
  <?xml version="1.0" encoding="windows-1252"?>
  <xmi:XMI xmi:version="2.1" xmlns:uml="http://schema.omg.org/spec/UML/2.1" xmlns:xmi="http://schema.omg.org/spec/XMI/2.1" xmlns:thecustomprofile="http://www.sparxsystems.com/profiles/thecustomprofile/1.0">
		<attribute xmi:idref="EAID_1" name="name">
      <documentation/>
      <stereotype/>
    </attribute>
  </xmi:XMI>
HERE

  # Tests

  # Test read_from_xmi to ensure basic attributes get populated correctly
  def test_read_from_xmi
    doc = REXML::Document.new(@@attribute_xmi)
    attribute_element = doc.elements().first.elements['attribute']
    attribute = UmlTools::Model::Attribute.read_from_xmi(attribute_element)

    assert_equal('name', attribute.name) #could also test base_name
    assert_equal('EAID_1', attribute.id)
    assert_equal('Doc', attribute.doc)
    assert_equal('stereotype', attribute.stereotype) #gets downcased
    assert_equal('int', attribute.type)
    assert_equal(:public, attribute.protection)
  end

  # Test that exclusion of optional parameters is handled correctly
  def test_read_from_xmi_empty_fields
    doc = REXML::Document.new(@@attribute_xmi_empty_fields)
    attribute_element = doc.elements().first.elements['attribute']
    attribute = UmlTools::Model::Attribute.read_from_xmi(attribute_element)

    assert_nil(attribute.doc)
    assert_nil(attribute.stereotype)
    assert_nil(attribute.type)
    assert_equal(:private, attribute.protection) # defaults to private
  end

  # Test that scopes other than the default ('public') will successfully load
  def test_read_from_xmi_private_method
    doc = REXML::Document.new(@@attribute_xmi_empty_fields)
    attribute_element = doc.elements().first.elements['attribute']
    attribute_element.add_attribute('scope', 'Private')
    attribute = UmlTools::Model::Attribute.read_from_xmi(attribute_element)

    assert_equal(:private, attribute.protection)
  end
end
