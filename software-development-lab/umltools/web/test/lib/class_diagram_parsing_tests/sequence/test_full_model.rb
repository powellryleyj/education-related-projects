require_relative '../../test_base'
require_relative '../../../../lib/umlTools/model/sequence/full_model'

class TestFullModel < Test::Unit::TestCase
  include TestBase


  def setup
   @details_for_element = REXML::Document.new(
       IO.read(File.join(File.dirname(__FILE__), '..', '..', 'resources', 'xmi', 'sequence_diagram.xmi'))
   )
  end

  <<DOC
Message and Lifeline IDs can be found near the top of the XMI file.
The XMI structure including the important lines will be approximately:
<uml:Model>
  <packagedElement>
    <packagedElement>
      <ownedBehavior>
        <lifeline xmi:id="THIS IS THE LIFELINE ID" />
        <message xmi:id="THIS IS THE MESSAGE ID" />
DOC

  must 'read all lifelines with proper arguments' do
    fm = UmlTools::Model::Sequence::FullModel::read_from_xmi(@details_for_element)
    assert_equal fm.lifelines.length, 4
    lifeline = fm.lifelines[0]
    assert_equal lifeline.id, "EAID_AT000000_8C43_4575_A3A1_47D2CC7548E2"
    assert_equal lifeline.type, "uml:Sequence"
    assert_equal lifeline.name, "Shopping List"
    assert_equal lifeline.stereotype, "entity"

    lifeline = fm.lifelines[1]
    assert_equal lifeline.id, "EAID_AT000000_AF56_4c88_9D64_9CE08CF5ABCB"
    assert_equal lifeline.type, "uml:Sequence"
    assert_equal lifeline.name, "Shopping List Controller"
    assert_equal lifeline.stereotype, "boundary"

    lifeline = fm.lifelines[2]
    assert_equal lifeline.id, "EAID_AT000000_7A79_474f_A6DC_2921F932D788"
    assert_equal lifeline.type, "uml:Sequence"
    assert_equal lifeline.name, "Shopping List Screen"
    assert_equal lifeline.stereotype, "control"
    assert_true lifeline.valid_names.include? 'Shopping List Screen'
    assert_true lifeline.valid_names.include? 'Number One'
    assert_true lifeline.valid_names.include? 'Notes'
    assert_true lifeline.valid_names.include? 'Pizza'

    lifeline = fm.lifelines[3]
    assert_equal lifeline.id, "EAID_AT000000_000_3DD5_42d1_A556_C098681014E"
    assert_equal lifeline.type, "uml:Actor"
    assert_equal lifeline.name, "User"
    assert_nil lifeline.stereotype

  end

  must 'handle nil documents gracefully' do
    assert_raise do
      UmlTools::Model::Sequence::FullModel::read_from_xmi(nil)
    end
  end

  must 'read message and its argument with proper properties on all' do
    fm = UmlTools::Model::Sequence::FullModel::read_from_xmi(@details_for_element)

    assert_equal(fm.messages.length, 4)
    message = fm.messages[0]
    assert_equal message.id, "EAID_C9F28A3B_E467_495d_9FB5_6277D04959C4"
    assert_nil message.name
    assert_equal message.message_kind, "complete"
    assert_equal message.message_sort, "createMessage"
    assert_equal message.sender.id, "EAID_AT000000_000_3DD5_42d1_A556_C098681014E"
    assert_equal message.receiver.id, "EAID_AT000000_7A79_474f_A6DC_2921F932D788"
    assert_nil message.guard

    message = fm.messages[1]
    assert_equal message.id, "EAID_0BE6843C_E17D_4a7e_92B7_4B9D90678E18"
    assert_equal message.name, "Marks selected shopping list"
    assert_equal message.message_kind, "complete"
    assert_equal message.message_sort, "synchCall"
    assert_equal message.sender.id, "EAID_AT000000_7A79_474f_A6DC_2921F932D788"
    assert_equal message.receiver.id, "EAID_AT000000_AF56_4c88_9D64_9CE08CF5ABCB"
    assert_equal message.guard, "PumpkinCount = 9"

    assert_equal message.arguments.length, 1
    arg = message.arguments[0]
    assert_equal arg.id, "EAID_AR000000_E17D_4a7e_92B7_4B9D90678E18"
    assert_equal arg.type, "uml:LiteralString"
    assert_equal arg.name, "shoppingListID"

    message = fm.messages[2]
    assert_equal message.id, "EAID_F207A95A_C9BB_4505_A256_FF222971D159"
    assert_equal message.name, "CompareShoppingLists"
    assert_equal message.message_kind, "complete"
    assert_equal message.message_sort, "synchCall"
    assert_equal message.sender.id, "EAID_AT000000_AF56_4c88_9D64_9CE08CF5ABCB"
    assert_equal message.receiver.id, "EAID_AT000000_AF56_4c88_9D64_9CE08CF5ABCB"
    assert_true message.valid_names.include? 'CompareShoppingLists'
    assert_true message.valid_names.include? 'CheckShoppingLists'
    assert_true message.valid_names.include? 'ExtraKeyword'
    assert_nil message.guard

    assert_equal message.arguments.length, 1
    arg = message.arguments[0]
    assert_equal arg.id, "EAID_AR000000_C9BB_4505_A256_FF222971D159"
    assert_equal arg.type, "uml:LiteralString"
    assert_equal arg.name, "shoppingListID"

    message = fm.messages[3]
    assert_equal message.id, "EAID_2EB1E49B_112F_40b4_A040_89F84E1FD64F"
    assert_equal message.name, "Retrieves selected shopping list"
    assert_equal message.message_kind, "complete"
    assert_equal message.message_sort, "synchCall"
    assert_equal message.sender.id, "EAID_AT000000_AF56_4c88_9D64_9CE08CF5ABCB"
    assert_equal message.receiver.id, "EAID_AT000000_8C43_4575_A3A1_47D2CC7548E2"
    assert_nil message.guard

    assert_equal message.arguments.length, 1
    arg = message.arguments[0]
    assert_equal arg.id, "EAID_AR000000_112F_40b4_A040_89F84E1FD64F"
    assert_equal arg.type, "uml:LiteralString"
    assert_equal arg.name, "shoppingListID"
  end

  must 'read location data for lifelines' do
    fm = UmlTools::Model::Sequence::FullModel::read_from_xmi(@details_for_element)

    # Every lifeline is parsed the same way, except for Actors
    # Therefore we only need to check one controller and one actor for correct values
    type1 = fm.lifelines[0] # Entity
    type2 = fm.lifelines[3] # Actor

    assert_equal type1.location.left, 853
    assert_equal type1.location.top, 50
    assert_equal type1.location.right, 903
    assert_equal type1.location.bottom, 322

    assert_equal type2.location.left, 1
    assert_equal type2.location.top, 50
    assert_equal type2.location.right, 91
    assert_equal type2.location.bottom, 322
  end
end
