#!/usr/bin/env ruby
# Testing the Logical Model class

require_relative '../test_base'

class TestLogicalModel < Test::Unit::TestCase
  include TestBase

  @@test_logical_xmi = <<HERE
<?xml version="1.0" encoding="windows-1252"?>
<xmi:XMI xmi:version="2.1" xmlns:uml="http://schema.omg.org/spec/UML/2.1" xmlns:xmi="http://schema.omg.org/spec/XMI/2.1" xmlns:thecustomprofile="http://www.sparxsystems.com/profiles/thecustomprofile/1.0" xmlns:EPProfile="http://www.sparxsystems.com/profiles/EPProfile/1.0">
	<xmi:Documentation exporter="Enterprise Architect" exporterVersion="6.5"/>
	<uml:Model xmi:type="uml:Model" name="EA_Model" visibility="public">
		<packagedElement xmi:type="uml:Package" xmi:id="EAPK_9EB73FD5_CC34_40f3_AE08_2D1B268C24D3" name="Model" visibility="public">
			<packagedElement xmi:type="uml:Package" xmi:id="EAPK_D87357D7_03EE_4c6c_A279_A7ADFFCB0BA2" name="Class Model" visibility="public">
				<packagedElement xmi:type="uml:Class" xmi:id="EAID_7AD2D52A_4DE5_43ff_BC85_8C8584BD46A6" name="DependsUponSub2" visibility="public"/>
				<packagedElement xmi:type="uml:Dependency" xmi:id="EAID_04160351_28C2_4525_8613_DC88405D81AE" visibility="public" supplier="EAID_9A976755_9A9F_4f9d_A009_CD87FF9508BF" client="EAID_7AD2D52A_4DE5_43ff_BC85_8C8584BD46A6"/>
				<packagedElement xmi:type="uml:Class" xmi:id="EAID_F6D101C7_D1B7_4f4c_AE06_8ECA959D2BD7" name="Sub1" visibility="public">
					<generalization xmi:type="uml:Generalization" xmi:id="EAID_8E454E15_8ADF_423b_B20B_B108EF0EC729" general="EAID_112524D5_A4C7_49b2_8C23_0E559D2CC0F4"/>
				</packagedElement>
				<packagedElement xmi:type="uml:Class" xmi:id="EAID_9A976755_9A9F_4f9d_A009_CD87FF9508BF" name="Sub2" visibility="public">
					<ownedAttribute xmi:type="uml:Property" xmi:id="EAID_90014C72_78E9_4732_9B8D_8EBCB539DCB8" name="firstAttribute" visibility="private" isStatic="false" isReadOnly="false" isDerived="false" isOrdered="false" isUnique="true" isDerivedUnion="false">
						<lowerValue xmi:type="uml:LiteralInteger" xmi:id="EAID_LI000001_78E9_4732_9B8D_8EBCB539DCB8" value="1"/>
						<upperValue xmi:type="uml:LiteralInteger" xmi:id="EAID_LI000002_78E9_4732_9B8D_8EBCB539DCB8" value="1"/>
						<type xmi:idref="EAJava_int"/>
					</ownedAttribute>
					<ownedOperation xmi:id="EAID_351DD423_3831_43ee_9C2A_86DCF73FDC9C" name="doSomething" visibility="public" concurrency="sequential">
						<ownedParameter xmi:id="EAID_RT000000_3831_43ee_9C2A_86DCF73FDC9C" name="return" direction="return" type="EAJava_void"/>
					</ownedOperation>
					<ownedOperation xmi:id="EAID_CE53FAA2_E94C_4e29_858F_1870AE2CD5FE" name="returnStuff" visibility="public" concurrency="sequential">
						<ownedParameter xmi:id="EAID_RT000000_E94C_4e29_858F_1870AE2CD5FE" name="return" direction="return" type="EAJava_String"/>
					</ownedOperation>
					<ownedOperation xmi:id="EAID_4B58ED13_82A8_4caa_A591_111EBF804190" name="takeParam" visibility="public" concurrency="sequential">
						<ownedParameter xmi:id="EAID_F078F4BB_595C_4605_B42E_95A6C5B135D4" name="num" direction="in" isStream="false" isException="false" isOrdered="false" isUnique="false" type="EAJava_int"/>
						<ownedParameter xmi:id="EAID_RT000000_82A8_4caa_A591_111EBF804190" name="return" direction="return" type="EAJava_void"/>
					</ownedOperation>
					<generalization xmi:type="uml:Generalization" xmi:id="EAID_9846E417_3848_466e_AA30_F7C461E7FD76" general="EAID_112524D5_A4C7_49b2_8C23_0E559D2CC0F4"/>
				</packagedElement>
				<packagedElement xmi:type="uml:Class" xmi:id="EAID_112524D5_A4C7_49b2_8C23_0E559D2CC0F4" name="Super" visibility="public"/>
				<packagedElement xmi:type="uml:Association" xmi:id="EAID_6881F9D0_3285_4859_8AFB_4AA08C8D0202" visibility="public">
					<memberEnd xmi:idref="EAID_dst81F9D0_3285_4859_8AFB_4AA08C8D0202"/>
					<ownedEnd xmi:type="uml:Property" xmi:id="EAID_dst81F9D0_3285_4859_8AFB_4AA08C8D0202" visibility="public" association="EAID_6881F9D0_3285_4859_8AFB_4AA08C8D0202" isStatic="false" isReadOnly="false" isDerived="false" isOrdered="false" isUnique="true" isDerivedUnion="false" aggregation="none">
						<type xmi:idref="EAID_E2539076_CFA6_4a02_B07F_CF441924B87A"/>
					</ownedEnd>
					<memberEnd xmi:idref="EAID_src81F9D0_3285_4859_8AFB_4AA08C8D0202"/>
					<ownedEnd xmi:type="uml:Property" xmi:id="EAID_src81F9D0_3285_4859_8AFB_4AA08C8D0202" visibility="public" association="EAID_6881F9D0_3285_4859_8AFB_4AA08C8D0202" isStatic="false" isReadOnly="false" isDerived="false" isOrdered="false" isUnique="true" isDerivedUnion="false" aggregation="none">
						<type xmi:idref="EAID_112524D5_A4C7_49b2_8C23_0E559D2CC0F4"/>
					</ownedEnd>
				</packagedElement>
				<packagedElement xmi:type="uml:Association" xmi:id="EAID_A5AE2562_14F1_4adf_8167_9546EEE24797" visibility="public">
					<memberEnd xmi:idref="EAID_dstAE2562_14F1_4adf_8167_9546EEE24797"/>
					<ownedEnd xmi:type="uml:Property" xmi:id="EAID_dstAE2562_14F1_4adf_8167_9546EEE24797" visibility="public" association="EAID_A5AE2562_14F1_4adf_8167_9546EEE24797" isStatic="false" isReadOnly="false" isDerived="false" isOrdered="false" isUnique="true" isDerivedUnion="false" aggregation="none">
						<type xmi:idref="EAID_E2539076_CFA6_4a02_B07F_CF441924B87A"/>
						<lowerValue xmi:type="uml:LiteralUnlimitedNatural" xmi:id="EAID_LI000003__14F1_4adf_8167_9546EEE24797" value="-1"/>
						<upperValue xmi:type="uml:LiteralUnlimitedNatural" xmi:id="EAID_LI000004__14F1_4adf_8167_9546EEE24797" value="-1"/>
					</ownedEnd>
					<memberEnd xmi:idref="EAID_srcAE2562_14F1_4adf_8167_9546EEE24797"/>
					<ownedEnd xmi:type="uml:Property" xmi:id="EAID_srcAE2562_14F1_4adf_8167_9546EEE24797" visibility="public" association="EAID_A5AE2562_14F1_4adf_8167_9546EEE24797" isStatic="false" isReadOnly="false" isDerived="false" isOrdered="false" isUnique="true" isDerivedUnion="false" aggregation="none">
						<type xmi:idref="EAID_112524D5_A4C7_49b2_8C23_0E559D2CC0F4"/>
						<lowerValue xmi:type="uml:LiteralInteger" xmi:id="EAID_LI000005__14F1_4adf_8167_9546EEE24797" value="1"/>
						<upperValue xmi:type="uml:LiteralInteger" xmi:id="EAID_LI000006__14F1_4adf_8167_9546EEE24797" value="1"/>
					</ownedEnd>
				</packagedElement>
				<packagedElement xmi:type="uml:Association" xmi:id="EAID_DB58177C_24E2_475c_A8D4_6AE2E97533F9" visibility="public">
					<memberEnd xmi:idref="EAID_dst58177C_24E2_475c_A8D4_6AE2E97533F9"/>
					<ownedEnd xmi:type="uml:Property" xmi:id="EAID_dst58177C_24E2_475c_A8D4_6AE2E97533F9" visibility="public" association="EAID_DB58177C_24E2_475c_A8D4_6AE2E97533F9" isStatic="false" isReadOnly="false" isDerived="false" isOrdered="false" isUnique="true" isDerivedUnion="false" aggregation="none">
						<type xmi:idref="EAID_E2539076_CFA6_4a02_B07F_CF441924B87A"/>
					</ownedEnd>
					<memberEnd xmi:idref="EAID_src58177C_24E2_475c_A8D4_6AE2E97533F9"/>
					<ownedEnd xmi:type="uml:Property" xmi:id="EAID_src58177C_24E2_475c_A8D4_6AE2E97533F9" visibility="public" association="EAID_DB58177C_24E2_475c_A8D4_6AE2E97533F9" isStatic="false" isReadOnly="false" isDerived="false" isOrdered="false" isUnique="true" isDerivedUnion="false" aggregation="none">
						<type xmi:idref="EAID_112524D5_A4C7_49b2_8C23_0E559D2CC0F4"/>
					</ownedEnd>
				</packagedElement>
				<packagedElement xmi:type="uml:Class" xmi:id="EAID_E2539076_CFA6_4a02_B07F_CF441924B87A" name="SuperAssociate" visibility="public"/>
				<packagedElement xmi:type="uml:Package" xmi:id="EAPK_DD490B9E_81AC_472d_9C3C_D514B13F9511" name="System" visibility="public">
					<packagedElement xmi:type="uml:Class" xmi:id="EAID_A6AF0B2D_4454_46cc_876D_BEAE0B09C5D1" name="Class1" visibility="public">
						<generalization xmi:type="uml:Generalization" xmi:id="EAID_491FA082_0A09_4122_B606_A675C743F6A6" general="EAID_E785473F_F7AC_4212_A002_B7425C474081"/>
					</packagedElement>
					<packagedElement xmi:type="uml:Association" xmi:id="EAID_1788A91E_22C6_4478_9323_F30F8B675E09" visibility="public">
						<memberEnd xmi:idref="EAID_dst88A91E_22C6_4478_9323_F30F8B675E09"/>
						<ownedEnd xmi:type="uml:Property" xmi:id="EAID_dst88A91E_22C6_4478_9323_F30F8B675E09" visibility="public" association="EAID_1788A91E_22C6_4478_9323_F30F8B675E09" isStatic="false" isReadOnly="true" isDerived="false" isOrdered="false" isUnique="true" isDerivedUnion="false" aggregation="none">
							<type xmi:idref="EAID_9F097B87_E1DB_4eca_B240_B551BDAC6E14"/>
						</ownedEnd>
						<memberEnd xmi:idref="EAID_src88A91E_22C6_4478_9323_F30F8B675E09"/>
						<ownedEnd xmi:type="uml:Property" xmi:id="EAID_src88A91E_22C6_4478_9323_F30F8B675E09" visibility="public" association="EAID_1788A91E_22C6_4478_9323_F30F8B675E09" isStatic="false" isReadOnly="true" isDerived="false" isOrdered="false" isUnique="true" isDerivedUnion="false" aggregation="none">
							<type xmi:idref="EAID_A6AF0B2D_4454_46cc_876D_BEAE0B09C5D1"/>
						</ownedEnd>
					</packagedElement>
					<packagedElement xmi:type="uml:Realization" xmi:id="EAID_E6C41DD8_1507_42ae_B839_8234F6E0B66C" visibility="public" supplier="EAID_68E50CB0_8440_4105_8175_BD7599C8F0F7" client="EAID_A6AF0B2D_4454_46cc_876D_BEAE0B09C5D1"/>
					<packagedElement xmi:type="uml:Class" xmi:id="EAID_E785473F_F7AC_4212_A002_B7425C474081" name="Class2" visibility="public"/>
					<packagedElement xmi:type="uml:Class" xmi:id="EAID_9F097B87_E1DB_4eca_B240_B551BDAC6E14" name="Class3" visibility="public"/>
					<packagedElement xmi:type="uml:Interface" xmi:id="EAID_68E50CB0_8440_4105_8175_BD7599C8F0F7" name="Interface1" visibility="public" isAbstract="true"/>
				</packagedElement>
				<packagedElement xmi:type="uml:Package" xmi:id="EAPK_825C8228_B023_4faf_87AA_763EF4F7CABC" name="Frameworks" visibility="public">
					<ownedComment xmi:type="uml:Comment" xmi:id="EAID_A957FC29_CD90_4424_862D_D295A1177859"/>
				</packagedElement>
			</packagedElement>
		</packagedElement>
		<thecustomprofile:stereotype base_Generalization="EAID_8E454E15_8ADF_423b_B20B_B108EF0EC729"/>
		<EPProfile:abstract base_Class="EAID_9A976755_9A9F_4f9d_A009_CD87FF9508BF"/>
		<thecustomprofile:realize base_Realization="EAID_E6C41DD8_1507_42ae_B839_8234F6E0B66C"/>
	</uml:Model>
</xmi:XMI>
HERE

  # TODO: Flexmock
  # Test read_from_xmi to ensure basic attributes are parsed correctly
  # def test_read_from_xmi
  #
  #   doc = REXML::Document.new(@@test_logical_xmi)
  #   model_element = doc.elements['xmi:XMI'].elements['uml:Model']
  #
  #   flexmock(UmlTools::Model::MClass).should_receive(:read_from_xmi).times(5)
  #   flexmock(UmlTools::Model::Association).should_receive(:read_from_xmi).times(3)
  #
  #   model = UmlTools::Model::LogicalModel.read_from_xmi(model_element, Hash.new)
  #
  #   assert_equal(3, model.associations.size)
  #   assert_equal(5, model.classes.size)
  # end

  # Test read_from_xmi with a nil model to ensure an exception is thrown indicating
  # that a logical model could not be found
  def test_read_from_xmi_no_model
    expected_message = 'Logical model not found'
    exception = assert_raise(RuntimeError) { UmlTools::Model::LogicalModel.read_from_xmi(nil, nil) }
    assert_equal(expected_message, exception.message)
  end

  # Test visible_classes to ensure that only visible MClass objects are returned
  def test_visible_classes
    visible_class = UmlTools::Model::MClass.new('visibleClass', 'Id', nil, nil, nil, nil, nil, nil)
    visible_class.visible = true
    invisible_class = UmlTools::Model::MClass.new('invisibleClass', 'Id', nil, nil, nil, nil, nil, nil)
    invisible_class.visible = false
    expected_classes = [visible_class]

    model = UmlTools::Model::LogicalModel.new([visible_class, invisible_class], [])

    assert_equal(expected_classes, model.visible_classes)
  end

  # Test hidden_classes to ensure that only non-visible MClass objects are returned
  def test_hidden_classes
    visible_class = UmlTools::Model::MClass.new('visibleMethod', 'Id', nil, nil, nil, nil, nil, nil)
    visible_class.visible = true
    invisible_class = UmlTools::Model::MClass.new('invisibleMethod', 'Id', nil, nil, nil, nil, nil, nil)
    invisible_class.visible = false
    expected_classes = [invisible_class]

    model = UmlTools::Model::LogicalModel.new([visible_class, invisible_class], [])

    assert_equal(expected_classes, model.hidden_classes)
  end

  # Test all_attributes to ensure that the attributes of all classes are returned
  def test_all_attributes
    class_one_attrs = [UmlTools::Model::Attribute.new('attr1', 'Id', nil, nil, nil, nil),
                       UmlTools::Model::Attribute.new('attr2', 'Id', nil, nil, nil, nil)]
    class_two_attrs = [UmlTools::Model::Attribute.new('attr3', 'Id', nil, nil, nil, nil)]
    class_one = UmlTools::Model::MClass.new('classOne', 'Id', nil, nil, nil, class_one_attrs, nil, nil)
    class_two = UmlTools::Model::MClass.new('classTwo', 'Id', nil, nil, nil, class_two_attrs, nil, nil)
    expected_attrs = class_one_attrs + class_two_attrs

    model = UmlTools::Model::LogicalModel.new([class_one, class_two], [])

    assert_equal(expected_attrs, model.all_attributes)
  end

  # Test all_methods to ensure that the methods of all classes are returned
  def test_all_methods
    class_one_methods = [UmlTools::Model::Method.new('method1', 'Id', nil, nil, nil)]
    class_two_methods = [UmlTools::Model::Method.new('method2', 'Id', nil, nil, nil),
                         UmlTools::Model::Method.new('method3', 'Id', nil, nil, nil)]
    class_one = UmlTools::Model::MClass.new('classOne', 'Id', nil, nil, nil, nil, class_one_methods, nil)
    class_two = UmlTools::Model::MClass.new('classTwo', 'Id', nil, nil, nil, nil, class_two_methods, nil)
    expected_methods = class_one_methods + class_two_methods

    model = UmlTools::Model::LogicalModel.new([class_one, class_two], [])

    assert_equal(expected_methods, model.all_methods)
  end

  # Test all_non_library_methods to ensure that only methods from non-library classes are returned
  def test_all_non_library_methods
    method_one = UmlTools::Model::Method.new('method1', 'Id', nil, nil, nil)
    method_two = UmlTools::Model::Method.new('method2', 'Id', nil, nil, nil)
    library_class = UmlTools::Model::MClass.new('list', 'Id', nil, nil, nil, nil, [method_one], nil)
    non_library_class = UmlTools::Model::MClass.new('notList', 'Id', nil, nil, nil, nil, [method_two], nil)
    expected_methods = [method_two]

    model = UmlTools::Model::LogicalModel.new([library_class, non_library_class], [])

    assert_equal(expected_methods, model.all_non_library_methods)
  end

  # Test roles to ensure that the roles of all associations are returned
  def test_roles
    role_a_one = UmlTools::Model::Role.new('roleAOne', 'Id', nil, nil, nil, nil, nil, nil)
    role_b_one = UmlTools::Model::Role.new('roleBOne', 'Id', nil, nil, nil, nil, nil, nil)
    role_a_two = UmlTools::Model::Role.new('roleATwo', 'Id', nil, nil, nil, nil, nil, nil)
    role_b_two = UmlTools::Model::Role.new('roleBTwo', 'Id', nil, nil, nil, nil, nil, nil)
    association_one = UmlTools::Model::Association.new('assocOne', 'Id', nil, nil, role_a_one, role_b_one)
    association_two = UmlTools::Model::Association.new('assocTwo', 'Id', nil, nil, role_a_two, role_b_two)
    expected_roles = [role_a_one, role_b_one, role_a_two, role_b_two]

    model = UmlTools::Model::LogicalModel.new([], [association_one, association_two])

    assert_equal(expected_roles, model.roles)
  end

  # Test visible_roles to ensure that only roles that are part of visible associations are returned
  def test_visible_roles
    role_a_one = UmlTools::Model::Role.new('roleAOne', 'Id', nil, nil, nil, nil, nil, nil)
    role_b_one = UmlTools::Model::Role.new('roleBOne', 'Id', nil, nil, nil, nil, nil, nil)
    role_a_two = UmlTools::Model::Role.new('roleATwo', 'Id', nil, nil, nil, nil, nil, nil)
    role_b_two = UmlTools::Model::Role.new('roleBTwo', 'Id', nil, nil, nil, nil, nil, nil)
    visible_assocation = UmlTools::Model::Association.new('assocOne', 'Id', nil, nil, role_a_one, role_b_one)
    visible_assocation.visible = true
    invisible_association = UmlTools::Model::Association.new('assocTwo', 'Id', nil, nil, role_a_two, role_b_two)
    invisible_association.visible = false
    expected_roles = [role_a_one, role_b_one]

    model = UmlTools::Model::LogicalModel.new([], [visible_assocation, invisible_association])

    assert_equal(expected_roles, model.visible_roles)
  end

  # Test all_elements to ensure that every single parsed element of the model is returned
  def test_all_elements
    role_a = UmlTools::Model::Role.new('roleA', 'Id', nil, nil, nil, nil, nil, nil)
    role_b = UmlTools::Model::Role.new('roleB', 'Id', nil, nil, nil, nil, nil, nil)
    assoc = UmlTools::Model::Association.new('associationWithRoles', 'Id', nil, nil, role_a, role_b)
    method = UmlTools::Model::Method.new('singleMethod', 'Id', nil, nil, nil)
    attr = UmlTools::Model::Attribute.new('singleAttr', 'Id', nil, nil, nil, nil)
    m_class = UmlTools::Model::MClass.new('classWithMembers', 'Id', nil, nil, nil, [attr], [method], nil)
    expected_elements = [m_class, assoc, attr, method, role_a, role_b]

    model = UmlTools::Model::LogicalModel.new([m_class], [assoc])

    assert_equal(expected_elements, model.all_elements)
  end

  # Test all_named_elements to ensure that only elements with a non-nil name are returned
  def test_all_named_elements
    role_a = UmlTools::Model::Role.new('roleA', 'Id', nil, nil, nil, nil, nil, nil)
    role_b = UmlTools::Model::Role.new(nil, 'Id', nil, nil, nil, nil, nil, nil)
    assoc = UmlTools::Model::Association.new(nil, 'Id', nil, nil, role_a, role_b)
    method = UmlTools::Model::Method.new('singleMethod', 'Id', nil, nil, nil)
    attr = UmlTools::Model::Attribute.new(nil, 'Id', nil, nil, nil, nil)
    m_class = UmlTools::Model::MClass.new('classWithMembers', 'Id', nil, nil, nil, [attr], [method], nil)
    expected_elements = [m_class, method, role_a]

    model = UmlTools::Model::LogicalModel.new([m_class], [assoc])

    assert_equal(expected_elements, model.all_named_elements)
  end

  # Test short_class_names to ensure that template information is stripped from the name
  def test_short_class_names
    classes = [UmlTools::Model::MClass.new('<badName>', 'Id', nil, nil, nil, nil, nil, nil),
               UmlTools::Model::MClass.new('goodName', 'Id', nil, nil, nil, nil, nil, nil)]
    expected_names = ['badName', 'goodName']

    model = UmlTools::Model::LogicalModel.new(classes, [])

    assert_equal(expected_names, model.short_class_names)
  end

  # Test add_associations to ensure that more associations can be added to an existing model
  def test_add_associations
    assoc_one = UmlTools::Model::Association.new('assoc1', 'Id', nil, nil, UmlTools::Model::Role.new(nil, nil, nil, nil, nil, nil, nil, nil),
                                       UmlTools::Model::Role.new(nil, nil, nil, nil, nil, nil, nil, nil))
    assoc_two = UmlTools::Model::Association.new('assoc2', 'Id', nil, nil, UmlTools::Model::Role.new(nil, nil, nil, nil, nil, nil, nil, nil),
                                       UmlTools::Model::Role.new(nil, nil, nil, nil, nil, nil, nil, nil))
    expected_associations = [assoc_one, assoc_two]

    model = UmlTools::Model::LogicalModel.new([], [assoc_one])
    model.add_xmi_associations([assoc_two])

    assert_equal(expected_associations, model.associations)
  end

  # Test to_s to ensure that the correct string representation of the model is returned
  def test_to_s
    expected_string = "LogicalModel[\n  <MClass classWithMembers/(Attr singleAttr)/(Method singleMethod)>\n  " +
                      "<assoc associationWithRoles between (Role (name: roleA) ) and (Role (name: roleB) )>\n]"
    role_a = UmlTools::Model::Role.new('roleA', 'Id', nil, nil, nil, nil, nil, nil)
    role_b = UmlTools::Model::Role.new('roleB', 'Id', nil, nil, nil, nil, nil, nil)
    assoc = UmlTools::Model::Association.new('associationWithRoles', 'Id', nil, nil, role_a, role_b)
    method = UmlTools::Model::Method.new('singleMethod', 'Id', nil, nil, nil)
    attr = UmlTools::Model::Attribute.new('singleAttr', 'Id', nil, nil, nil, nil)
    m_class = UmlTools::Model::MClass.new('classWithMembers', 'Id', nil, nil, nil, [attr], [method], nil)

    model = UmlTools::Model::LogicalModel.new([m_class], [assoc])

    assert_equal(expected_string, model.to_s)
  end

  # TODO: Flexmock
  # Test dump to ensure the correct dumped string is returned
  # def test_dump
  #
  #   expected_string = "(LogicalModel\n  (MClass classWithMembers (attributes (attr singleAttr)) (methods (elt singleMethod)) hidden)\n)"
  #
  #   method = UmlTools::Model::Method.new('singleMethod', 'Id', nil, nil, nil)
  #   attr = UmlTools::Model::Attribute.new('singleAttr', 'Id', nil, nil, nil, nil)
  #   m_class = UmlTools::Model::MClass.new('classWithMembers', 'Id', nil, nil, nil, [attr], [method], nil)
  #   flexmock(UmlTools::Model::Role).should_receive(:dump).and_return('roleName')
  #   UmlTools::Model::Element.print_just_ids = false
  #   UmlTools::Model::Element.print_ids = false
  #
  #   model = UmlTools::Model::LogicalModel.new([m_class], [])
  #   actual = model.dump
  #
  #   assert_equal(expected_string, actual)
  # end
end
