#!/usr/bin/env ruby
# Testing the Method class

require_relative '../test_base'

class TestMethod < Test::Unit::TestCase
  include TestBase

  # Setup XMI

  @@test_method_xmi = <<HERE
  <?xml version="1.0" encoding="windows-1252"?>
  <xmi:XMI xmi:version="2.1" xmlns:uml="http://schema.omg.org/spec/UML/2.1" xmlns:xmi="http://schema.omg.org/spec/XMI/2.1" xmlns:thecustomprofile="http://www.sparxsystems.com/profiles/thecustomprofile/1.0">
		<operation xmi:idref="EAID_351DD423_3831_43ee_9C2A_86DCF73FDC9C" name="doSomething" scope="Public">
			<properties position="0"/>
			<stereotype stereotype='helper'/>
			<model ea_guid="{351DD423-3831-43ee-9C2A-86DCF73FDC9C}" ea_localid="1"/>
			<type type="void" const="false" static="false" isAbstract="false" synchronised="0" concurrency="Sequential" returnarray="0" pure="0" isQuery="false"/>
			<behaviour/>
			<code/>
			<style/>
			<styleex/>
			<documentation value="This is documentation."/>
			<tags/>
			<parameters>
				<parameter xmi:idref="EAID_RETURNID_3831_43ee_9C2A_86DCF73FDC9C" visibility="public">
					<properties pos="0" type="void" const="false" ea_guid="{RETURNID-3831-43ee-9C2A-86DCF73FDC9C}"/>
					<style/>
					<styleex/>
					<documentation/>
					<tags/>
					<xrefs/>
				</parameter>
			</parameters>
			<xrefs/>
		</operation>
  </xmi:XMI>
HERE

  # Tests

  # Test read_from_xmi to ensure basic attributes get populated correctly
  def test_read_from_xmi
    expected_name = 'doSomething'
    expected_id = 'EAID_351DD423_3831_43ee_9C2A_86DCF73FDC9C'
    expected_doc = 'This is documentation.'
    expected_stereotype = 'helper'
    doc = REXML::Document.new(@@test_method_xmi)
    op = doc.elements['//operation']
    method = UmlTools::Model::Method.read_from_xmi(op)
    assert_equal(expected_name, method.name)
    assert_equal(expected_id, method.id)
    assert_equal(expected_doc, method.doc)
    assert_equal(expected_stereotype, method.stereotype)
  end

  # TODO: Flexmock
  # Test that scopes other than the default ('public') will successfully load
  # def test_read_from_xmi_private_method
  #   expected_protection = :private
  #   doc = REXML::Document.new(@@test_method_xmi)
  #   op = doc.elements['//operation']
  #   partial_mock = flexmock(op, 'operation', :attributes => { 'scope'=>'Private'} )
  #   method = UmlTools::Model::Method.read_from_xmi(partial_mock)
  #   assert_equal(expected_protection, method.protection)
  # end

  # Test that when the scope attribute is not present, the default will be 'public'
  def test_read_from_xmi_undefined_scope
    expected_protection = :public
    doc = REXML::Document.new(@@test_method_xmi)
    op = doc.elements['//operation']
    op.attributes.get_attribute('scope').remove
    method = UmlTools::Model::Method.read_from_xmi(op)
    assert_equal(expected_protection, method.protection)
  end

  # Test the to_s method to ensure it formats the string correctly
  def test_to_s
    expected_desc = '(Method doSomething)'
    op = UmlTools::Model::Method.new('doSomething', 'id', 'stereotype', 'doc', 'public')
    desc = op.to_s
    assert_equal(expected_desc, desc)
  end

  # TODO: Flexmock
  # Test the ident method with a non-nil container variable to
  # ensure it formats the string correctly
  # def test_ident_with_container
  #   expected_ident = 'operation doSomething in aContainer'
  #   mock_container = flexmock('name'=>'aContainer')
  #   op = UmlTools::Model::Method.new('doSomething', 'id', 'stereotype', 'doc', 'public')
  #   op.container = mock_container
  #   ident_result = op.ident
  #   assert_equal(expected_ident, ident_result)
  # end

  # Test the ident method with a nil container variable to
  # ensure it formats the string correctly
  def test_ident_with_nil_container
    expected_ident = 'operation doSomething'
    op = UmlTools::Model::Method.new('doSomething', 'id', 'stereotype', 'doc', 'public')
    ident_result = op.ident
    assert_equal(expected_ident, ident_result)
  end

  # Test method? to ensure it returns true
  def test_method
    op = UmlTools::Model::Method.new('doSomething', 'id', 'stereotype', 'doc', 'public')
    assert(op.method?)
  end
end
