require_relative '../test_base'

class TestAssociableElement < Test::Unit::TestCase
  include TestBase

  def setup
    xmi = <<END
<?xml version="1.0" encoding="windows-1252"?>
<xmi:XMI xmi:version="2.1" xmlns:uml="http://schema.omg.org/spec/UML/2.1" xmlns:xmi="http://schema.omg.org/spec/XMI/2.1" xmlns:thecustomprofile="http://www.sparxsystems.com/profiles/thecustomprofile/1.0" xmlns:EPProfile="http://www.sparxsystems.com/profiles/EPProfile/1.0">
<packagedElement xmi:type="uml:Association" xmi:id="AssociationID" name="theName" visibility="public">
	<memberEnd xmi:idref="somesrcID"/>
	<ownedEnd xmi:type="uml:Property" xmi:id="somesrcID" visibility="public" association="AssociationID" isStatic="false" isReadOnly="true" isDerived="false" isOrdered="false" isUnique="true" isDerivedUnion="false" aggregation="none">
	  <type xmi:idref="targetID"/>
	</ownedEnd>
	<memberEnd xmi:idref="somedstID"/>
	<ownedEnd xmi:type="uml:Property" xmi:id="somedstID" visibility="public" association="AssociationID" isStatic="false" isReadOnly="true" isDerived="false" isOrdered="false" isUnique="true" isDerivedUnion="false" aggregation="none">
		<type xmi:idref="sourceID"/>
	</ownedEnd>
</packagedElement>
</xmi:XMI>
END
    details = <<END
<?xml version="1.0" encoding="windows-1252"?>
<xmi:XMI xmi:version="2.1" xmlns:uml="http://schema.omg.org/spec/UML/2.1" xmlns:xmi="http://schema.omg.org/spec/XMI/2.1" xmlns:thecustomprofile="http://www.sparxsystems.com/profiles/thecustomprofile/1.0" xmlns:EPProfile="http://www.sparxsystems.com/profiles/EPProfile/1.0">
<connector xmi:idref="AssociationID">
	<source xmi:idref="sourceID">
	</source>
	<target xmi:idref="targetID">
	</target>
	<properties ea_type="Association" direction="Unspecified" stereotype="stereotype"/>
	<documentation value="This is documentation."/>
</connector>
</xmi:XMI>
END
    @element = REXML::Document.new(xmi).elements['xmi:XMI/packagedElement']
    @details = Hash.new
    @details['AssociationID'] = REXML::Document.new(details).elements['xmi:XMI/connector']

  end

  # TODO: Flexmock
  # def setup_association_mock
  #   source_role = UmlTools::Model::Role.new('source', nil, nil, nil, nil, nil, nil, nil)
  #   target_role = UmlTools::Model::Role.new('target', nil, nil, nil, nil, nil, nil, nil)
  #   flexmock(UmlTools::Model::Role).should_receive(:read_from_xmi).and_return(source_role, target_role)
  #   @association = UmlTools::Model::Association.read_from_xmi(@element, @details)
  # end

  # TODO: Flexmock
  # must 'parse association name' do
  #   setup_association_mock
  #   assert_equal('theName', @association.name, 'Association name returned did not match expected')
  # end
  #
  # TODO: Flexmock
  # must 'parse association id' do
  #   setup_association_mock
  #   assert_equal('AssociationID', @association.id, 'Association id returned did not match expected')
  # end
  #
  # TODO: Flexmock
  # must 'parse association a_role' do
  #   setup_association_mock
  #   assert_equal('source', @association.a_role.name, 'Association a_role returned did not match expected')
  # end
  #
  # TODO: Flexmock
  # must 'parse association b_role' do
  #   setup_association_mock
  #   assert_equal('target', @association.b_role.name, 'Association b_role returned did not match expected')
  # end
  #
  # TODO: Flexmock
  # must 'parse association documentation' do
  #   setup_association_mock
  #   assert_equal('This is documentation.', @association.doc, 'Association documentation returned did not match expected')
  # end
  #
  # TODO: Flexmock
  # must 'parse association stereotype' do
  #   setup_association_mock
  #   assert_equal('stereotype', @association.stereotype, 'Association stereotype returned did not match expected')
  # end

  # TODO: Flexmock
  # must 'call Role with correct arguments' do
  #   source_role = UmlTools::Model::Role.new('source', nil, nil, nil, nil, nil, nil, nil)
  #   target_role = UmlTools::Model::Role.new('target', nil, nil, nil, nil, nil, nil, nil)
  #
  #
  #   source_entry = nil
  #   target_entry = nil
  #
  #   REXML::XPath.match(@element, 'memberEnd').each { |memberEnd|
  #     if memberEnd.attributes['xmi:idref'].include? 'src'
  #       source_entry = memberEnd
  #     else
  #       target_entry = memberEnd
  #     end
  #   }
  #
  #   source_details = @details['AssociationID'].elements['source']
  #   flexmock(UmlTools::Model::Role).should_receive(:read_from_xmi).with(source_entry, source_details).and_return(source_role, target_role).times(1)
  #
  #   target_details = @details['AssociationID'].elements['target']
  #   flexmock(UmlTools::Model::Role).should_receive(:read_from_xmi).with(target_entry, target_details).and_return(source_role, target_role).times(1)
  #
  #   UmlTools::Model::Association.read_from_xmi(@element, @details)
  # end

end
