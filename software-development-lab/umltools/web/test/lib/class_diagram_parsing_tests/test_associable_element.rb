require_relative '../test_base'

class TestAssociableElement < Test::Unit::TestCase
  include TestBase

  def setup
    xmi = <<END
<?xml version="1.0" encoding="windows-1252"?>
<xmi:XMI xmi:version="2.1" xmlns:uml="http://schema.omg.org/spec/UML/2.1" xmlns:xmi="http://schema.omg.org/spec/XMI/2.1" xmlns:thecustomprofile="http://www.sparxsystems.com/profiles/thecustomprofile/1.0" xmlns:EPProfile="http://www.sparxsystems.com/profiles/EPProfile/1.0">
  <element xmi:idref="theStart" xmi:type="uml:Class" name="somethingsomething" scope="public">
    <links>
      <Dependency xmi:id="Dependency1" start="theStart" end="theEnd"/>
      <Dependency xmi:id="Dependency2" start="theStart" end="theEnd2"/>
      <Generalization xmi:id="Generalization1" start="theStart" end="theEnd3"/>
      <Generalization xmi:id="Generalization2" start="theStart" end="theEnd4"/>
    </links>
  </element>
</xmi:XMI>
END
    @details_for_element = REXML::Document.new(xmi).elements['xmi:XMI/element']
  end

  # TODO: Flexmock
  # must 'read two Dependencies and two Generalizations' do
  #   flexmock(UmlTools::Model::GeneralizationRelationship).should_receive(:read_from_xmi).times(2)
  #   flexmock(UmlTools::Model::Dependency).should_receive(:read_from_xmi).times(2)
  #
  #   UmlTools::Model::AssociableElement.read_super_and_uses_from_xmi(@details_for_element, Hash.new)
  # end
  #
  # TODO: Flexmock
  # must 'return generalizations and dependencies in an array of arrays' do
  #   flexmock(UmlTools::Model::GeneralizationRelationship).should_receive(:read_from_xmi).and_return(1, 2)
  #   flexmock(UmlTools::Model::Dependency).should_receive(:read_from_xmi).and_return(3, 4)
  #
  #   results = UmlTools::Model::AssociableElement.read_super_and_uses_from_xmi(@details_for_element, Hash.new)
  #   assert_equal([1, 2], results[0], 'Generalizations returned did not match expected')
  #   assert_equal([3, 4], results[1], 'Dependencies returned did not match expected')
  #
  # end

  must 'use a default name for to_s if name is nil or empty' do
    gen = UmlTools::Model::GeneralizationRelationship.new(nil, 'Id', nil, nil, nil)
    mdl = UmlTools::Model::AssociableElement.new(nil, nil, nil, nil, [gen], nil)
    mdl.supers_as_string.include?('Unnamed Element')
  end

end
