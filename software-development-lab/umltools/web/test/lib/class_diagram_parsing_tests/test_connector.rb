#!/usr/bin/env ruby
# Testing the GeneralizationRelationship class

require_relative '../test_base'

class TestGeneralizationRelationship < Test::Unit::TestCase
  include TestBase

  @@test_someConnector_xmi = <<HERE
  <?xml version="1.0" encoding="windows-1252"?>
  <xmi:XMI xmi:version="2.1" xmlns:uml="http://schema.omg.org/spec/UML/2.1" xmlns:xmi="http://schema.omg.org/spec/XMI/2.1" xmlns:thecustomprofile="http://www.sparxsystems.com/profiles/thecustomprofile/1.0">
    <xmi:Extension extender="Enterprise Architect" extenderID="6.5">
      <elements>
        <element xmi:idref="EAID_F6D101C7_D1B7_4f4c_AE06_8ECA959D2BD7" xmi:type="uml:Class" name="Sub1" scope="public">
          <model package="EAPK_D87357D7_03EE_4c6c_A279_A7ADFFCB0BA2" tpos="0" ea_localid="15" ea_eleType="element"/>
          <properties isSpecification="false" sType="Class" nType="0" scope="public" isRoot="false" isLeaf="false" isAbstract="false" isActive="false"/>
          <project author="hermanm" version="1.0" phase="1.0" created="2014-01-16 10:13:42" modified="2014-01-16 10:13:46" complexity="1" status="Proposed"/>
          <code gentype="Java"/>
          <style appearance="BackColor=-1;BorderColor=-1;BorderWidth=-1;FontColor=-1;VSwimLanes=1;HSwimLanes=1;BorderStyle=0;"/>
          <modelDocument/>
          <tags/>
          <xrefs/>
          <extendedProperties tagged="0" package_name="Class Model"/>
          <links>
            <Generalization xmi:id="EAID_8E454E15_8ADF_423b_B20B_B108EF0EC729" start="EAID_F6D101C7_D1B7_4f4c_AE06_8ECA959D2BD7" end="EAID_112524D5_A4C7_49b2_8C23_0E559D2CC0F4"/>
          </links>
        </element>
      </elements>
      <connectors>
        <connector xmi:idref="EAID_8E454E15_8ADF_423b_B20B_B108EF0EC729" name="gen">
          <source xmi:idref="EAID_F6D101C7_D1B7_4f4c_AE06_8ECA959D2BD7">
            <model ea_localid="15" type="Class" name="Sub1"/>
            <role visibility="Public" targetScope="instance"/>
            <type aggregation="none" containment="Unspecified"/>
            <constraints/>
            <modifiers isOrdered="false" changeable="none" isNavigable="false"/>
            <style value="Union=0;Derived=0;AllowDuplicates=0;"/>
            <documentation/>
            <xrefs/>
            <tags/>
          </source>
          <target xmi:idref="EAID_112524D5_A4C7_49b2_8C23_0E559D2CC0F4">
            <model ea_localid="14" type="Class" name="Super"/>
            <role visibility="Public" targetScope="instance"/>
            <type aggregation="none" containment="Unspecified"/>
            <constraints/>
            <modifiers isOrdered="false" changeable="none" isNavigable="true"/>
            <style value="Union=0;Derived=0;AllowDuplicates=0;"/>
            <documentation/>
            <xrefs/>
            <tags/>
          </target>
          <model ea_localid="6"/>
          <properties ea_type="Generalization" stereotype="stereotype" direction="Source -&gt; Destination"/>
          <parameterSubstitutions/>
          <documentation value="This is documentation"/>
          <appearance linemode="3" linecolor="-1" linewidth="0" seqno="0" headStyle="0" lineStyle="0"/>
          <labels mt="gen"/>
          <extendedProperties virtualInheritance="0"/>
          <style/>
          <xrefs value="$XREFPROP=$XID={6D722C97-BC72-4253-97A8-6FFC870D0C34}$XID;$NAM=Stereotypes$NAM;$TYP=connector property$TYP;$VIS=Public$VIS;$PAR=0$PAR;$DES=@STEREO;Name=stereotype;GUID={EE1EB519-0392-40fd-A4D0-944D235368CE};@ENDSTEREO;$DES;$CLT={8E454E15-8ADF-423b-B20B-B108EF0EC729}$CLT;$SUP=&lt;none&gt;$SUP;$ENDXREF;"/>
          <tags/>
        </connector>
      </connectors>
    </xmi:Extension>
  </xmi:XMI>
HERE

  # Test read_from_xmi to ensure basic attributes are populated correctly
  def test_read_from_xmi
    expected_name = 'gen'
    expected_id = 'EAID_8E454E15_8ADF_423b_B20B_B108EF0EC729'
    expected_stereotype = 'stereotype'
    expected_doc = 'This is documentation'
    doc = REXML::Document.new(@@test_someConnector_xmi)
    element_details = doc.elements['//element'] #doc.elements().first.elements['xmi:Extension'].elements['elements'].elements['element']
    connector_details = doc.elements['//connector'] #doc.elements().first.elements['xmi:Extension'].elements['connectors'].elements['connector']
    details_table = { 'EAID_F6D101C7_D1B7_4f4c_AE06_8ECA959D2BD7'=>element_details, 'EAID_8E454E15_8ADF_423b_B20B_B108EF0EC729'=>connector_details }
    element = element_details.elements['links'].elements['Generalization']
    gen = UmlTools::Model::Connector.read_from_xmi(element, details_table)
    assert_equal(expected_name, gen.name)
    assert_equal(expected_id, gen.id)
    assert_equal(expected_stereotype, gen.stereotype)
    assert_equal(expected_doc, gen.doc)
  end

  def test_read_from_xmi_missing_details
    expected_message = 'Missing details for someConnector ID'
    element = REXML::Element.new
    element.add_attribute('xmi:id', 'ID')
    element.name = 'someConnector'
    exception = assert_raise(RuntimeError) { UmlTools::Model::Connector.read_from_xmi(element, Hash.new) }
    assert_equal(expected_message, exception.message)
  end

  # Test find_links_in to ensure supplier is set based on the supplier_id
  def test_find_links_in
    expected_supplier = 'Supplier'
    supplier_id = 'supID'
    gen = UmlTools::Model::Connector.new(nil, 'Id', nil, nil, supplier_id)
    gen.find_links_in( { 'supID'=>'Supplier' } )
    assert_equal(expected_supplier, gen.supplier)
  end

  # Test find_links_in to ensure that supplier is only set if it is nil
  def test_find_links_in_supplier_not_nil
    expected_supplier = 'Supplier'
    supplier_id = 'supID'
    gen = UmlTools::Model::Connector.new(nil, 'Id', nil, nil, supplier_id)
    gen.find_links_in( { 'supID'=>'Supplier' } )
    gen.find_links_in( { 'supID'=>'NotSupplier' } )
    assert_equal(expected_supplier, gen.supplier)
  end

  # Test supplier_name with a non-nil supplier
  def test_supplier_name_with_supplier
    expected_name = 'supName'
    supplier = UmlTools::Model::MClass.new('supName', 'Id', nil, nil, nil, nil, nil, nil)
    gen = UmlTools::Model::Connector.new(nil, 'Id', nil, nil, 'supID')
    gen.find_links_in( { 'supID'=>supplier } )
    assert_equal(expected_name, gen.supplier_name)
  end

  # Test supplier_name with a nil supplier
  def test_supplier_name_without_supplier
    expected_name = '[unknown]'
    gen = UmlTools::Model::Connector.new(nil, 'Id', nil, nil, nil)
    assert_equal(expected_name, gen.supplier_name)
  end

  # Test to_s with a non-nil stereotype
  def test_to_s_with_stereotype
    gen = UmlTools::Model::Connector.new('genName', 'Id', 'stereotype', nil, nil)
    expected_string = "<#{gen.ident} (stereo stereotype)>"
    assert_equal(expected_string, gen.to_s)
  end

  # Test to_s with a nil stereotype
  def test_to_s_without_stereotype
    gen = UmlTools::Model::Connector.new('genName', 'Id', nil, nil, nil)
    expected_string = "<#{gen.ident}>"
    assert_equal(expected_string, gen.to_s)
  end

  # Test dump with Element.print_just_ids set to true
  def test_dump_with_print_just_ids
    UmlTools::Model::Element.print_just_ids = true
    supplier = UmlTools::Model::MClass.new('supplierName', 'supplierId', nil, nil, nil, nil, nil, nil)
    gen = UmlTools::Model::Connector.new('genName', 'genID', nil, nil, 'supplierId', 'myDump')
    expected_result = '(myDump genID supplierId)'
    gen.find_links_in( { 'supplierId'=>supplier } )
    assert_equal(expected_result, gen.dump)
  end

  # Test dump with Element.print_just_ids set to false, but Element.print_ids set to true
  def test_dump_without_print_just_ids
    UmlTools::Model::Element.print_just_ids = false
    UmlTools::Model::Element.print_ids = true
    supplier = UmlTools::Model::MClass.new('supplierName', 'supplierId', nil, nil, nil, nil, nil, nil)
    gen = UmlTools::Model::Connector.new('genName', 'genId', 'stereotype', 'documentation', 'supplierId', 'myDumpIdentifier')
    expected_result = '(myDumpIdentifier genName genId  (stereo stereotype) supplierName (doc documentation))'
    gen.find_links_in( { 'supplierId'=>supplier } )
    assert_equal(expected_result, gen.dump)
  end

  must 'be able to find supplier with find_links if supplier_id is nil but has a
        name which is the same as supplier name in table' do
    supplier = UmlTools::Model::MClass.new('theName', 'supplierId', nil, nil, nil, nil, nil, nil)
    connector = UmlTools::Model::Connector.new('theName', 'theId', 'theStereotype', 'documentation', nil, 'theDumpIdentifier')
    connector.find_links_in({'supplierId'=>supplier})
    assert_equal(supplier, connector.supplier)
  end


end
