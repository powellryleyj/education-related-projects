#!/usr/bin/env ruby
# Testing the GeneralizationRelationship class

require_relative '../test_base'

class TestGeneralizationRelationship < Test::Unit::TestCase
  include TestBase

  # Test ident with a generalization relationship that has a name
  def test_ident_with_name
    expected_ident = 'generalization_relationship genName'
    gen = UmlTools::Model::GeneralizationRelationship.new('genName', 'Id', nil, nil, nil)
    assert_equal(expected_ident, gen.ident)
  end

  # Test ident with a generalization relationship that does not have a name, but does have a supplier
  def test_ident_with_supplier
    expected_ident = 'generalization_relationship of supName'
    gen = UmlTools::Model::GeneralizationRelationship.new(nil,'Id', nil, nil, 'supplierId')
    supplier = UmlTools::Model::MClass.new('supName', 'Id', nil, nil, nil, nil, nil, nil)
    gen.find_links_in( { 'supplierId'=>supplier } )
    assert_equal(expected_ident, gen.ident)
  end

  # Test ident with a generalization relationship that has neither a name or supplier, but does
  # have a supplier id
  def test_ident_with_supplier_id
    expected_ident = 'generalization_relationship of supID'
    gen = UmlTools::Model::GeneralizationRelationship.new(nil, 'Id', nil, nil, 'supID')
    assert_equal(expected_ident, gen.ident)
  end

  # Test generalization_relationship? to ensure it returns true
  def test_generalization_relationship
    gen = UmlTools::Model::GeneralizationRelationship.new(nil, nil, nil, nil, nil)
    assert(gen.generalization_relationship?)
  end

end
