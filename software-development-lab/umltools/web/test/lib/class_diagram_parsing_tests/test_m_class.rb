#!/usr/bin/env ruby
# Testing the MClass class

require_relative '../test_base'

class TestMClass < Test::Unit::TestCase
  include TestBase

  @@test_mclass_xmi = <<HERE
  <?xml version="1.0" encoding="windows-1252"?>
  <xmi:XMI xmi:version="2.1" xmlns:uml="http://schema.omg.org/spec/UML/2.1" xmlns:xmi="http://schema.omg.org/spec/XMI/2.1" xmlns:thecustomprofile="http://www.sparxsystems.com/profiles/thecustomprofile/1.0">
    <xmi:Documentation exporter="Enterprise Architect" exporterVersion="6.5"/>
    <uml:Model xmi:type="uml:Model" name="EA_Model" visibility="public">
      <packagedElement xmi:type="uml:Package" xmi:id="EAPK_9EB73FD5_CC34_40f3_AE08_2D1B268C24D3" name="Model" visibility="public">
        <packagedElement xmi:type="uml:Package" xmi:id="EAPK_D87357D7_03EE_4c6c_A279_A7ADFFCB0BA2" name="Class Model" visibility="public">
          <packagedElement xmi:type="uml:Class" xmi:id="EAID_9A976755_9A9F_4f9d_A009_CD87FF9508BF" name="Sub2" visibility="public">
            <ownedAttribute xmi:type="uml:Property" xmi:id="EAID_90014C72_78E9_4732_9B8D_8EBCB539DCB8" name="firstAttribute" visibility="private" isStatic="false" isReadOnly="false" isDerived="false" isOrdered="false" isUnique="true" isDerivedUnion="false">
              <lowerValue xmi:type="uml:LiteralInteger" xmi:id="EAID_LI000001_78E9_4732_9B8D_8EBCB539DCB8" value="1"/>
              <upperValue xmi:type="uml:LiteralInteger" xmi:id="EAID_LI000002_78E9_4732_9B8D_8EBCB539DCB8" value="1"/>
              <type xmi:idref="EAJava_int"/>
            </ownedAttribute>
            <ownedOperation xmi:id="EAID_351DD423_3831_43ee_9C2A_86DCF73FDC9C" name="doSomething" visibility="public" concurrency="sequential">
              <ownedParameter xmi:id="EAID_RT000000_3831_43ee_9C2A_86DCF73FDC9C" name="return" direction="return" type="EAJava_void"/>
            </ownedOperation>
            <ownedOperation xmi:id="EAID_CE53FAA2_E94C_4e29_858F_1870AE2CD5FE" name="returnStuff" visibility="public" concurrency="sequential">
              <ownedParameter xmi:id="EAID_RT000000_E94C_4e29_858F_1870AE2CD5FE" name="return" direction="return" type="EAJava_String"/>
            </ownedOperation>
            <ownedOperation xmi:id="EAID_4B58ED13_82A8_4caa_A591_111EBF804190" name="takeParam" visibility="public" concurrency="sequential">
              <ownedParameter xmi:id="EAID_F078F4BB_595C_4605_B42E_95A6C5B135D4" name="num" direction="in" isStream="false" isException="false" isOrdered="false" isUnique="false" type="EAJava_int"/>
              <ownedParameter xmi:id="EAID_RT000000_82A8_4caa_A591_111EBF804190" name="return" direction="return" type="EAJava_void"/>
            </ownedOperation>
            <generalization xmi:type="uml:Generalization" xmi:id="EAID_9846E417_3848_466e_AA30_F7C461E7FD76" general="EAID_112524D5_A4C7_49b2_8C23_0E559D2CC0F4"/>
				  </packagedElement>
        </packagedElement>
      </packagedElement>
    </uml:Model>
    <xmi:Extension extender="Enterprise Architect" extenderID="6.5">
      <elements>
        <element xmi:idref="EAID_9A976755_9A9F_4f9d_A009_CD87FF9508BF" xmi:type="uml:Class" name="Sub2" scope="public">
          <model package="EAPK_D87357D7_03EE_4c6c_A279_A7ADFFCB0BA2" tpos="0" ea_localid="16" ea_eleType="element"/>
          <properties documentation="This is documentation" isSpecification="false" sType="Class" nType="0" scope="public" stereotype="abstract" isRoot="false" isLeaf="false" isAbstract="false" isActive="false"/>
          <project author="hermanm" version="1.0" phase="1.0" created="2014-01-16 10:13:48" modified="2014-01-16 10:13:52" complexity="1" status="Proposed"/>
          <code gentype="Java"/>
          <style appearance="BackColor=-1;BorderColor=-1;BorderWidth=-1;FontColor=-1;VSwimLanes=1;HSwimLanes=1;BorderStyle=0;"/>
          <modelDocument/>
          <tags/>
          <xrefs/>
          <extendedProperties tagged="0" package_name="Class Model"/>
          <attributes>
            <attribute xmi:idref="EAID_90014C72_78E9_4732_9B8D_8EBCB539DCB8" name="firstAttribute" scope="Private">
              <initial/>
              <documentation/>
              <model ea_localid="1" ea_guid="{90014C72-78E9-4732-9B8D-8EBCB539DCB8}"/>
              <properties type="int" derived="0" collection="false" duplicates="0" changeability="changeable"/>
              <coords ordered="0"/>
              <containment containment="Not Specified" position="0"/>
              <stereotype/>
              <bounds lower="1" upper="1"/>
              <options/>
              <style/>
              <styleex value="IsLiteral=0;volatile=0;"/>
              <tags/>
              <xrefs/>
            </attribute>
          </attributes>
          <operations>
            <operation xmi:idref="EAID_351DD423_3831_43ee_9C2A_86DCF73FDC9C" name="doSomething" scope="Public">
              <properties position="0"/>
              <stereotype/>
              <model ea_guid="{351DD423-3831-43ee-9C2A-86DCF73FDC9C}" ea_localid="1"/>
              <type type="void" const="false" static="false" isAbstract="false" synchronised="0" concurrency="Sequential" returnarray="0" pure="0" isQuery="false"/>
              <behaviour/>
              <code/>
              <style/>
              <styleex/>
              <documentation/>
              <tags/>
              <parameters>
                <parameter xmi:idref="EAID_RETURNID_3831_43ee_9C2A_86DCF73FDC9C" visibility="public">
                  <properties pos="0" type="void" const="false" ea_guid="{RETURNID-3831-43ee-9C2A-86DCF73FDC9C}"/>
                  <style/>
                  <styleex/>
                  <documentation/>
                  <tags/>
                  <xrefs/>
                </parameter>
              </parameters>
              <xrefs/>
            </operation>
            <operation xmi:idref="EAID_CE53FAA2_E94C_4e29_858F_1870AE2CD5FE" name="returnStuff" scope="Public">
              <properties position="1"/>
              <stereotype/>
              <model ea_guid="{CE53FAA2-E94C-4e29-858F-1870AE2CD5FE}" ea_localid="3"/>
              <type type="String" const="false" static="false" isAbstract="false" synchronised="0" concurrency="Sequential" returnarray="0" pure="0" isQuery="false"/>
              <behaviour/>
              <code/>
              <style/>
              <styleex/>
              <documentation/>
              <tags/>
              <parameters>
                <parameter xmi:idref="EAID_RETURNID_E94C_4e29_858F_1870AE2CD5FE" visibility="public">
                  <properties pos="0" type="String" const="false" ea_guid="{RETURNID-E94C-4e29-858F-1870AE2CD5FE}"/>
                  <style/>
                  <styleex/>
                  <documentation/>
                  <tags/>
                  <xrefs/>
                </parameter>
              </parameters>
              <xrefs/>
            </operation>
            <operation xmi:idref="EAID_4B58ED13_82A8_4caa_A591_111EBF804190" name="takeParam" scope="Public">
              <properties position="2"/>
              <stereotype/>
              <model ea_guid="{4B58ED13-82A8-4caa-A591-111EBF804190}" ea_localid="2"/>
              <type type="void" const="false" static="false" isAbstract="false" synchronised="0" concurrency="Sequential" returnarray="0" pure="0" isQuery="false"/>
              <behaviour/>
              <code/>
              <style/>
              <styleex/>
              <documentation/>
              <tags/>
              <parameters>
                <parameter xmi:idref="EAID_RETURNID_82A8_4caa_A591_111EBF804190" visibility="public">
                  <properties pos="0" type="void" const="false" ea_guid="{RETURNID-82A8-4caa-A591-111EBF804190}"/>
                  <style/>
                  <styleex/>
                  <documentation/>
                  <tags/>
                  <xrefs/>
                </parameter>
                <parameter xmi:idref="EAID_F078F4BB_595C_4605_B42E_95A6C5B135D4" visibility="public">
                  <properties pos="0" type="int" const="false" ea_guid="{F078F4BB-595C-4605-B42E-95A6C5B135D4}"/>
                  <style/>
                  <styleex/>
                  <documentation/>
                  <tags/>
                  <xrefs/>
                </parameter>
              </parameters>
              <xrefs/>
            </operation>
          </operations>
          <links/>
        </element>
      </elements>
    </xmi:Extension>
  </xmi:XMI>
HERE

  # TODO: Flexmock
  # Test read_from_xmi to ensure that basic properties are correctly populated.
  # Should mock out calls to lower levels of parsing.
  # def test_read_from_xmi
  #   expected_id = 'EAID_9A976755_9A9F_4f9d_A009_CD87FF9508BF'
  #   expected_name = 'Sub2'
  #   expected_doc = 'This is documentation'
  #   expected_stereotype = 'abstract'
  #
  #   doc = REXML::Document.new(@@test_mclass_xmi)
  #   element = doc.elements['//uml:Model/packagedElement/packagedElement/packagedElement']
  #   element_details = doc.elements['//element']
  #   details_table = { 'EAID_9A976755_9A9F_4f9d_A009_CD87FF9508BF'=>element_details }
  #
  #   flexmock(UmlTools::Model::Method).should_receive(:read_from_xmi).times(3)
  #   flexmock(UmlTools::Model::Attribute).should_receive(:read_from_xmi).once
  #
  #   m_class = UmlTools::Model::MClass.read_from_xmi(element, details_table)
  #
  #   assert_equal(expected_id, m_class.id)
  #   assert_equal(expected_name, m_class.name)
  #   assert_equal(expected_doc, m_class.doc)
  #   assert_equal(expected_stereotype, m_class.stereotype)
  #   assert(m_class.load_errors.empty?)
  # end

  # Test read_from_xmi without a matching id in the details_table to ensure
  # an exception is thrown when there are no details for the element
  def test_read_from_xmi_missing_details
    expected_message = 'Missing details for class Dog'
    element = REXML::Element.new
    element.add_attribute('xmi:id', 'ID')
    element.add_attribute('name', 'Dog')
    exception = assert_raise(RuntimeError) { UmlTools::Model::MClass.read_from_xmi(element, Hash.new()) }
    assert_equal(expected_message, exception.message)
  end

  # Test ident with an MClass that is visible
  def test_ident_visible
    expected_ident = 'class className'
    m_class = UmlTools::Model::MClass.new('className', 'Id', nil, nil, nil, nil, nil, nil)
    m_class.visible = true
    assert_equal(expected_ident, m_class.ident)
  end

  # Test ident with an MClass that is not visible
  def test_ident_not_visible
    expected_ident = 'class className (hidden)'
    m_class = UmlTools::Model::MClass.new('className', 'Id', nil, nil, nil, nil, nil, nil)
    m_class.visible = false
    assert_equal(expected_ident, m_class.ident)
  end

  # Test ident with an MClass that does not have a name
  def test_ident_nil_name
    expected_ident = 'class Unnamed Element'
    m_class = UmlTools::Model::MClass.new(nil, 'Id', nil, nil, nil, nil, nil, nil)
    m_class.visible = true
    assert_equal(expected_ident, m_class.ident)
  end

  # Test to_s to ensure that the correct string is returned
  def test_to_s
    expected_result = '<MClass Sub2/(Attr firstAttribute)/(Method doSomething), (Method returnStuff), (Method takeParam)>'

    doc = REXML::Document.new(@@test_mclass_xmi)
    element = doc.elements['//uml:Model/packagedElement/packagedElement/packagedElement']
    element_details = doc.elements['//element']
    details_table = { 'EAID_9A976755_9A9F_4f9d_A009_CD87FF9508BF'=>element_details }

    m_class = UmlTools::Model::MClass.read_from_xmi(element, details_table)

    assert_equal(expected_result, m_class.to_s)
  end

  # Test dump with Element.print_just_ids set to true to ensure that the correct
  # string is returned
  def test_dump_print_just_ids
    expected_result = '(class classId  (attributes (attr attrId)) (methods (elt methodId)))'
    methods = [UmlTools::Model::Method.new('doSomething', 'methodId', nil, nil, nil)]
    attrs = [UmlTools::Model::Attribute.new('holdData', 'attrId', nil, nil, nil, nil)]
    m_class = UmlTools::Model::MClass.new('Class', 'classId', nil, nil, nil, attrs, methods, nil)
    UmlTools::Model::Element.print_just_ids = true
    assert_equal(expected_result, m_class.dump)
  end

  # Test dump with Element.print_just_ids set to false to ensure that the correct
  # string is returned
  def test_dump_without_print_just_ids
    expected_result = '(MClass Dog classId  (stereo stereotype) (attributes (attr holdData attrId )) (methods (elt doSomething)) (doc theDoc) hidden)'
    methods = [UmlTools::Model::Method.new('doSomething', 'methodId', nil, nil, nil)]
    attrs = [UmlTools::Model::Attribute.new('holdData', 'attrId', nil, nil, nil, nil)]
    m_class = UmlTools::Model::MClass.new('Dog', 'classId', 'stereotype', 'theDoc', nil, attrs, methods, nil)
    m_class.visible = false
    UmlTools::Model::Element.print_just_ids = false
    assert_equal(expected_result, m_class.dump)
  end

  # Test all elements to ensure the returned collection includes all necessary elements
  def test_all_elements_nil
    methods = [UmlTools::Model::Method.new('doSomething', 'methodId', nil, nil, nil)]
    attrs = [UmlTools::Model::Attribute.new('holdData', 'attrId', nil, nil, nil, nil)]
    super_relationships = [UmlTools::Model::GeneralizationRelationship.new('genName', 'genId', nil, nil, nil)]
    expected_array = attrs + methods + super_relationships
    m_class = UmlTools::Model::MClass.new('Dog', 'classId', 'stereotype', 'theDoc', super_relationships, attrs, methods, nil)

    assert_equal(expected_array, m_class.all_elements)
  end

  # Test contained_type with a nil name to ensure nil is returned
  def test_contained_type_nil
    expected_result = nil
    m_class = UmlTools::Model::MClass.new(nil, 'Id', nil, nil, nil, nil, nil, nil)
    assert_equal(expected_result, m_class.contained_type)
  end

  # Test contained_type to ensure that all unwanted characters are removed from the class name
  def test_contained_type
    expected_result = 'classA'
    m_class = UmlTools::Model::MClass.new('<class* [A] &19>', 'Id', nil, nil, nil, nil, nil, nil)
    assert_equal(expected_result, m_class.contained_type)
  end

  # Test actor? to ensure that it evaluates to true when the stereotype is 'actor'
  def test_actor_true
    m_class = UmlTools::Model::MClass.new('className', 'Id', 'actor', nil, nil, nil, nil, nil)
    assert(m_class.actor?)
  end

  # Test actor? to ensure that it evaluates to false when the stereotype is not 'actor'
  def test_actor_false
    m_class = UmlTools::Model::MClass.new('className', 'Id', 'notActor', nil, nil, nil, nil, nil)
    assert(!m_class.actor?)
  end

  # Test template? to ensure it evaluates to true when the name is surrounded by '<' and '>'
  def test_template_match
    m_class = UmlTools::Model::MClass.new('<class>', 'Id', nil, nil, nil, nil, nil, nil)
    assert(m_class.template?)
  end

  # Test template? to ensure it evaluates to false when the name is not surrounded by '<' and '>'
  def test_template_no_match
    m_class = UmlTools::Model::MClass.new('<class', 'Id', nil, nil, nil, nil, nil, nil)
    assert(!m_class.template?)
  end

  # Test in_library? to ensure it returns a LibraryClass object if the name of the class
  # is part of a standard library
  def test_in_library_match
    m_class = UmlTools::Model::MClass.new('list', 'Id', nil, nil, nil, nil, nil, nil)
    library = m_class.in_library?
    assert(library)
    assert(library.is_a?(UmlTools::LibraryClass))
  end

  # Test in_library? to ensure it returns a nil if the name of the class
  # is not part of a standard library
  def test_in_library_no_match
    m_class = UmlTools::Model::MClass.new('definitelyNotALibraryClass', 'Id', nil, nil, nil, nil, nil, nil)
    assert(!m_class.in_library?)
  end

  # Test find_links_in to ensure that the container of the attributes and methods
  # are set to the class itself
  def test_find_in_table
    methods = [UmlTools::Model::Method.new('doSomething', 'methodId', nil, nil, nil)]
    attrs = [UmlTools::Model::Attribute.new('holdData', 'attrId', nil, nil, nil, nil)]
    m_class = UmlTools::Model::MClass.new('Dog', 'classId', nil, nil, nil, attrs, methods, nil)
    m_class.find_links_in(Hash.new)
    assert_equal(m_class.attributes[0].container, m_class)
    assert_equal(m_class.methods[0].container, m_class)
  end

  # Test class? to ensure that it returns true
  def test_class
    m_class = UmlTools::Model::MClass.new('Dog', 'classId', nil, nil, nil, [], [], nil)
    assert(m_class.class?)
  end
end
