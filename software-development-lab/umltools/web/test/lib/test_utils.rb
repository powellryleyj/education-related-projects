#!/usr/bin/env ruby
# t3: testing utilities used in checking

require_relative 'test_base'

class TestUtils <  Test::Unit::TestCase
    include TestBase

  def test_A_words_in_identifier
    assert_equal %w(car), UmlTools.words_in_identifier('car')
    assert_equal %w(a), UmlTools.words_in_identifier('a')
    assert_equal %w(C O W), UmlTools.words_in_identifier('COW')
    assert_equal %w(Crash), UmlTools.words_in_identifier('Crash')
    assert_equal %w(Crash), UmlTools.words_in_identifier('_Crash')
    assert_equal %w(a brown cat), UmlTools.words_in_identifier('a_brown_cat')
    assert_equal %w(a Red Dog Tooth), UmlTools.words_in_identifier('aRedDogTooth')
    assert_equal %w(Power To The Children), UmlTools.words_in_identifier('Power_To_TheChildren')
    assert_equal [], UmlTools.words_in_identifier('_')
  end

  def test_B_cap_first
    assert_equal 'Abc', 'abc'.cap_first
    assert_equal 'X', 'x'.cap_first
    assert_equal 'Do it now', 'do it now'.cap_first
    assert_equal '', ''.cap_first
  end

  def test_C_mk_issue
    i = UmlTools::Issue.new(:a, :bc, 'abc', 'elt')
    assert_equal :a, i.problem
    assert_equal :bc, i.particular_problem
    assert_equal 'abc', i.message
    assert_equal 'elt', i.associated_element_id
  end

  def test_C_issue
    UmlTools::Issue.set_echo_ids(false)
    actual = UmlTools::Issue.new(:a, :bc, 'abc', 'elt_id').to_s
    assert_equal 'abc', actual
    assert_equal %w(one two),
                 UmlTools.msgs([UmlTools::Issue.new(:x, :x1, 'one', 'id'), 
                                UmlTools::Issue.new(:y, :y2, 'two', 'id')])
    UmlTools::Issue.set_echo_ids(true)
    assert_equal 'z;uv;abc;elt_id', 
                 UmlTools::Issue.new(:z, :uv, 'abc', 'elt_id').to_s
    assert_equal %w(z;x;abc;elt_id), 
                 UmlTools.msgs([UmlTools::Issue.new(:z, :x, 'abc', 'elt_id')])
    assert_equal %w(x;x1;one;id y;y2;two;id2),
                 UmlTools.msgs([UmlTools::Issue.new(:x, :x1, 'one', 'id'),
                                UmlTools::Issue.new(:y, :y2, 'two', 'id2')])
    UmlTools::Issue.set_echo_ids(false)
    # following feature (UmlTools::Issue.@@echo_problem_ids) currently off -
    #   change if there appears to be a reason to show these:
    assert(!UmlTools::Issue.echo?)
  end

  def test_D_library_class
    assert UmlTools::LibraryClass.in_stl?('map')
    assert !UmlTools::LibraryClass.in_stl?('trash')
    assert UmlTools::LibraryClass.in_java_class_library?('Map')
    assert !UmlTools::LibraryClass.in_java_class_library?('trash')
  end

  def test_include_unnamed_checks
    x = UmlTools::ListOfChecks::CheckItem.new('X', :ALL)
    y = UmlTools::ListOfChecks::CheckItem.new('Y', 'yy')
    z = UmlTools::ListOfChecks::CheckItem.new(:Y,  :zz)
    cl = UmlTools::ListOfChecks::ChecksToExclude.new('testorg', 'testdescr', 'testemail', 'testver', 'testlang',
                                                     [x, y, z])
    # confirm fields
    assert_equal 'testorg',   cl.organization
    assert_equal 'testdescr', cl.description
    assert_equal 'testemail', cl.contact_email_address
    assert_equal 'testver',   cl.tool_version
    assert_equal 'testlang',  cl.language
    # confirm do? behavior
    assert !cl.do?(:X, :arbitrary_not_in_list)
    assert !cl.do?(:X, :again_just_X_listed)
    assert !cl.do?(:Y, :yy)       # item in list explicitely
    assert !cl.do?(:Y, :zz)       # another in list
    assert cl.do?(:Y, :r)         # Y in list, but r is not
    assert cl.do?(:Z, :yy)        # Z not in list, though yy is
    assert cl.do?(:Z, :v)         # neither Z nor v in the list

    cl.exclude(:Y, :ALL)
    assert !cl.do?(:Y, :r)
    cl.exclude(:Z, :v)
    assert !cl.do?(:Z, :v)
    assert  cl.do?(:Z, :yy)
    assert  cl.do?(:New, :x)
    cl.exclude(:New, :x)
    assert !cl.do?(:New, :x)
    cl.exclude(:Z, :v)
    assert  cl.do?(:Z, :u)
    cl.exclude(:Z, :u)
    assert !cl.do?(:Z, :u)
  end

  def test_exclude_unnamed_checks
    x = UmlTools::ListOfChecks::CheckItem.new('X', :ALL)
    y = UmlTools::ListOfChecks::CheckItem.new('Y', 'yy')
    z = UmlTools::ListOfChecks::CheckItem.new(:Y,  :zz)
    cl = UmlTools::ListOfChecks::ChecksToInclude.new('testorg', 'testdescr', 'testemail', 'testver',
                             'testlang', [x, y, z])
    assert cl.do?(:X, :arbitrary_not_in_list)
    assert cl.do?(:X, :again_just_X_listed)
    assert cl.do?(:Y, :yy)       # item in list explicitely
    assert cl.do?(:Y, :zz)       # another in list
    assert !cl.do?(:Y, :r)         # Y in list, but r is not
    assert !cl.do?(:Z, :yy)        # Z not in list, though yy is
    assert !cl.do?(:Z, :v)         # neither Z nor v in the list
  end

  def test_checklist_with_yaml
    x = UmlTools::ListOfChecks::CheckItem.new('X', :ALL)
    y = UmlTools::ListOfChecks::CheckItem.new('Y', 'yy')
    z = UmlTools::ListOfChecks::CheckItem.new(:Y,  :zz)
    cl = UmlTools::ListOfChecks::ChecksToInclude.new('testorg', 'testdescr', 'testemail', 'testver', 'testlang', 
                                                     [x, y, z])
    yaml_version = cl.to_yaml

    new_cl = UmlTools::ListOfChecks.load(yaml_version)
    # confirm fields
    assert_equal 'testorg',   new_cl.organization
    assert_equal 'testdescr', new_cl.description
    assert_equal 'testemail', new_cl.contact_email_address
    assert_equal 'testver',   new_cl.tool_version
    assert_equal 'testlang',  new_cl.language
    # now confirm do? works after unmarshalling
    assert new_cl.do?(:X, :arbitrary_not_in_list)
    assert new_cl.do?(:X, :again_just_X_listed)
    assert new_cl.do?(:Y, :yy)         # item in list explicitely
    assert new_cl.do?(:Y, :zz)         # another in list
    assert !new_cl.do?(:Y, :r)         # Y in list, but r is not
    assert !new_cl.do?(:Z, :yy)        # Z not in list, though yy is
    assert !new_cl.do?(:Z, :v)         # neither Z nor v in the list

    cl.exclude(:Y, :zz)
    assert  cl.do?(:Y, :yy)
    assert !cl.do?(:Y, :zz)
    cl.exclude(:Y, :ALL)
    assert !cl.do?(:Y, :yy)
    assert !cl.do?(:Y, :zz)
  end

  def test_parsing_type_parameters
    assert_equal %w(int char),
                 UmlTools::LibraryClass.type_parameters('map<int, char>')
    assert_nil   UmlTools::LibraryClass.type_parameters('map')
    assert_equal [], UmlTools::LibraryClass.type_parameters('map<>')
    assert_equal ['int', ''], 
                 UmlTools::LibraryClass.type_parameters('map<int,>')
    assert_equal ['', ''], UmlTools::LibraryClass.type_parameters('map<,>')
    assert_equal ['', 'int'], 
                 UmlTools::LibraryClass.type_parameters('map<,int>')
    assert_equal %w(map<char,set<int>>),
                 UmlTools::LibraryClass.type_parameters('vector< map < char,set<int>>>')
    # note: one missing > in following
    assert_equal %w(list<map<char,set<int>>),
                 UmlTools::LibraryClass.type_parameters('vector<list< map < char,set<int>>>')
  end

end
