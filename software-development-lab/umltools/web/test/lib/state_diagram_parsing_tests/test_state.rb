require_relative '../test_base'

class TestState < Test::Unit::TestCase

  # Called before every test method runs. Can be used
  # to set up fixture information.
  def setup
    xmi = <<END
<?xml version="1.0" encoding="windows-1252"?>
<xmi:XMI xmi:version="2.1" xmlns:uml="http://schema.omg.org/spec/UML/2.1" xmlns:xmi="http://schema.omg.org/spec/XMI/2.1">
  <subvertex xmi:type="uml:State" xmi:id="EAID_22E9CFAD_1D05_4e98_88EB_287FDD11CA67" name="State2" visibility="public" isSubmachineState="false">
    <incoming xmi:idref="EAID_E29C159B_42CC_4ce7_9F76_488C3C27A02F"/>
    <incoming xmi:idref="EAID_755B5069_579A_417f_949C_03DE719FF232"/>
    <outgoing xmi:idref="EAID_456C5A5F_D827_4b5f_A598_62EBA1944A89"/>
    <outgoing xmi:idref="EAID_093647BC_010D_47d7_B626_0DBEFA5A8F77"/>
  </subvertex>
</xmi:XMI>
END
    details_xmi = <<END
<?xml version="1.0" encoding="windows-1252"?>
<xmi:XMI xmi:version="2.1" xmlns:uml="http://schema.omg.org/spec/UML/2.1" xmlns:xmi="http://schema.omg.org/spec/XMI/2.1">
  <element xmi:idref="EAID_22E9CFAD_1D05_4e98_88EB_287FDD11CA67" xmi:type="uml:State" name="State2" scope="public">
    <model package="EAPK_D774A1B6_2322_48b8_AB67_7D59DC4AFE0B" tpos="0" ea_localid="28" ea_eleType="element"/>
    <properties documentation="This is documentation for State2. It is very descriptive." isSpecification="false" sType="State" nType="0" scope="public" stereotype="State2 stereotype"/>
    <project author="hermanm" version="1.0" phase="1.0" created="2014-02-03 09:11:16" modified="2014-02-03 09:11:20" complexity="1" status="Proposed"/>
    <code gentype="Java"/>
    <style appearance="BackColor=-1;BorderColor=-1;BorderWidth=-1;FontColor=-1;VSwimLanes=0;HSwimLanes=0;BorderStyle=0;"/>
    <modelDocument/>
    <tags/>
    <xrefs/>
    <extendedProperties tagged="0" package_name="State"/>
    <links>
      <StateFlow xmi:id="EAID_093647BC_010D_47d7_B626_0DBEFA5A8F77" start="EAID_22E9CFAD_1D05_4e98_88EB_287FDD11CA67" end="EAID_44A8E10D_B49F_40d2_9FEB_18E6167CFFD8"/>
      <StateFlow xmi:id="EAID_456C5A5F_D827_4b5f_A598_62EBA1944A89" start="EAID_22E9CFAD_1D05_4e98_88EB_287FDD11CA67" end="EAID_39FA7791_A8A6_4140_BB7D_41DEC94DBDB1"/>
      <StateFlow xmi:id="EAID_755B5069_579A_417f_949C_03DE719FF232" start="EAID_39FA7791_A8A6_4140_BB7D_41DEC94DBDB1" end="EAID_22E9CFAD_1D05_4e98_88EB_287FDD11CA67"/>
      <StateFlow xmi:id="EAID_E29C159B_42CC_4ce7_9F76_488C3C27A02F" start="EAID_C815B498_7ADF_4aec_BCF4_1B218E9152CE" end="EAID_22E9CFAD_1D05_4e98_88EB_287FDD11CA67"/>
    </links>
  </element>
</xmi:XMI>
END
    transitions = [create_transition_with_id('EAID_E29C159B_42CC_4ce7_9F76_488C3C27A02F'),
                   create_transition_with_id('EAID_755B5069_579A_417f_949C_03DE719FF232'),
                   create_transition_with_id('EAID_456C5A5F_D827_4b5f_A598_62EBA1944A89'),
                   create_transition_with_id('EAID_093647BC_010D_47d7_B626_0DBEFA5A8F77')]
    @state_rexml = REXML::Document.new(xmi).elements['xmi:XMI/subvertex[@xmi:type="uml:State"]']
    detail_element = REXML::Document.new(details_xmi).elements['xmi:XMI/element']
    details_table = {@state_rexml.attributes['xmi:id'] => detail_element}
    @state = UmlTools::Model::State.read_from_xmi(@state_rexml, details_table, transitions)
  end

  must 'parse state id' do
    assert_equal('EAID_22E9CFAD_1D05_4e98_88EB_287FDD11CA67', @state.id)
  end

  must 'parse state name' do
    assert_equal('State2', @state.name)
  end

  must 'parse outgoing transition 0 for state' do
    assert_equal('EAID_456C5A5F_D827_4b5f_A598_62EBA1944A89', @state.outgoing_transitions[0].id)
  end

  must 'parse outgoing transition 1 for state' do
    assert_equal('EAID_093647BC_010D_47d7_B626_0DBEFA5A8F77', @state.outgoing_transitions[1].id)
  end

  must 'parse incoming transition 0 for state' do
    assert_equal('EAID_E29C159B_42CC_4ce7_9F76_488C3C27A02F', @state.incoming_transitions[0].id)
  end

  must 'parse incoming transition 1 for state' do
    assert_equal('EAID_755B5069_579A_417f_949C_03DE719FF232', @state.incoming_transitions[1].id)
  end

  must 'parse documentation for state' do
    assert_equal('This is documentation for State2. It is very descriptive.', @state.doc)
  end

  must 'parse stereotype for state' do
    assert_equal('state2 stereotype', @state.stereotype)
  end

  def create_transition_with_id(transition_id)
    UmlTools::Model::Transition.new('', transition_id, nil, nil, nil, nil, nil)
  end

end
