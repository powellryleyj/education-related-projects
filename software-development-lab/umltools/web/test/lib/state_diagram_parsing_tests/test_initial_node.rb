require_relative '../test_base'

class TestInitialNode < Test::Unit::TestCase

  # Called before every test method runs. Can be used
  # to set up fixture information.
  def setup
    xmi = <<END
<?xml version="1.0" encoding="windows-1252"?>
  <xmi:XMI xmi:version="2.1" xmlns:uml="http://schema.omg.org/spec/UML/2.1" xmlns:xmi="http://schema.omg.org/spec/XMI/2.1">
    <subvertex xmi:type="uml:Pseudostate" xmi:id="initial_node_id" name="Start" visibility="public" kind="initial">
			<outgoing xmi:idref="outgoing_id"/>
		</subvertex>
  </xmi:XMI>
END
    details_xmi = <<END
<?xml version="1.0" encoding="windows-1252"?>
  <xmi:XMI xmi:version="2.1" xmlns:uml="http://schema.omg.org/spec/UML/2.1" xmlns:xmi="http://schema.omg.org/spec/XMI/2.1">
    <element xmi:idref="initial_node_id" xmi:type="uml:StateNode" name="Start" scope="public">
		  <model package="EAPK_D774A1B6_2322_48b8_AB67_7D59DC4AFE0B" tpos="0" ea_localid="3" ea_eleType="element"/>
		  <properties documentation="This is an initial node." isSpecification="false" sType="StateNode" nType="3" scope="public" stereotype="initialNode"/>
		  <project author="hermanm" version="1.0" phase="1.0" created="2014-01-30 08:36:12" modified="2014-02-20 09:31:19" complexity="1" status="Proposed"/>
		  <code gentype="&lt;none&gt;"/>
		  <style appearance="BackColor=-1;BorderColor=-1;BorderWidth=-1;FontColor=-1;VSwimLanes=1;HSwimLanes=1;BorderStyle=0;"/>
		  <modelDocument/>
		  <tags/>
		  <xrefs value="$XREFPROP=$XID={1CE81491-04FC-4f01-AF9A-E92DA2EC1337}$XID;$NAM=Stereotypes$NAM;$TYP=element property$TYP;$VIS=Public$VIS;$PAR=0$PAR;$DES=@STEREO;Name=initialNode;GUID={3E8280BB-99D2-4c74-A3F2-A8FE49695BEC};@ENDSTEREO;$DES;$CLT={BE455297-A70A-490d-ACFF-CC30AD0A9EC3}$CLT;$SUP=&lt;none&gt;$SUP;$ENDXREF;"/>
		  <extendedProperties tagged="0" package_name="State"/>
		  <links>
		  	<StateFlow xmi:id="outgoing_id" start="initial_node_id" end="EAID_C815B498_7ADF_4aec_BCF4_1B218E9152CE"/>
		  </links>
	  </element>
  </xmi:XMI>
END
    transitions = [create_transition_with_id('outgoing_id')]
    @initial_node_rexml = REXML::Document.new(xmi).elements['xmi:XMI/subvertex']
    detail_element = REXML::Document.new(details_xmi).elements['xmi:XMI/element']
    details_table = {@initial_node_rexml.attributes['xmi:id'] => detail_element}
    @initial_node = UmlTools::Model::InitialNode.read_from_xmi(@initial_node_rexml, details_table, transitions)
  end

  must 'parse initial node id' do
    assert_equal('initial_node_id', @initial_node.id)
  end

  must 'identify itself as an initial node' do
    assert(@initial_node.initial_node?)
  end

  must 'parse initial node name' do
    assert_equal('Start', @initial_node.name)
  end

  must 'parse initial node outgoing id' do
    assert_equal('outgoing_id', @initial_node.outgoing[0].id)
  end

  must 'parse initial node stereotype' do
    assert_equal('initialnode', @initial_node.stereotype)
  end

  must 'parse initial node documentation' do
    assert_equal('This is an initial node.', @initial_node.doc)
  end

  def create_transition_with_id(transition_id)
    UmlTools::Model::Transition.new('', transition_id, nil, nil, nil, nil, nil)
  end

end
