require_relative '../test_base'

class TestFinalNode < Test::Unit::TestCase
  # Called before every test method runs. Can be used
  # to set up fixture information.
  def setup
    xmi = <<END
<?xml version="1.0" encoding="windows-1252"?>
  <xmi:XMI xmi:version="2.1" xmlns:uml="http://schema.omg.org/spec/UML/2.1" xmlns:xmi="http://schema.omg.org/spec/XMI/2.1">
    <subvertex xmi:type="uml:FinalState" xmi:id="final_node_id" name="Final" visibility="public">
		  <incoming xmi:idref="incoming_id"/>
		</subvertex>
  </xmi:XMI>
END
    details_xmi = <<END
<?xml version="1.0" encoding="windows-1252"?>
  <xmi:XMI xmi:version="2.1" xmlns:uml="http://schema.omg.org/spec/UML/2.1" xmlns:xmi="http://schema.omg.org/spec/XMI/2.1">

    <element xmi:idref="final_node_id" xmi:type="uml:StateNode" name="Final" scope="public">
		  <model package="EAPK_D774A1B6_2322_48b8_AB67_7D59DC4AFE0B" tpos="0" ea_localid="26" ea_eleType="element"/>
		  <properties documentation="This is a final node." isSpecification="false" sType="StateNode" nType="4" scope="public" stereotype="finalNode"/>
		  <project author="hermanm" version="1.0" phase="1.0" created="2014-01-30 09:32:11" modified="2014-02-20 09:31:34" complexity="1" status="Proposed"/>
		  <code gentype="&lt;none&gt;"/>
		  <style appearance="BackColor=-1;BorderColor=-1;BorderWidth=-1;FontColor=-1;VSwimLanes=1;HSwimLanes=1;BorderStyle=0;"/>
		  <modelDocument/>
		  <tags/>
		  <xrefs value="$XREFPROP=$XID={9EF664E3-95EB-4d65-A145-1AFEF791D308}$XID;$NAM=Stereotypes$NAM;$TYP=element property$TYP;$VIS=Public$VIS;$PAR=0$PAR;$DES=@STEREO;Name=finalNode;GUID={D9488054-6615-4cef-8D30-6DDE1AEC4D19};@ENDSTEREO;$DES;$CLT={E3D533CE-571D-4225-913C-AE354E696BA6}$CLT;$SUP=&lt;none&gt;$SUP;$ENDXREF;"/>
		  <extendedProperties tagged="0" package_name="State"/>
		  <links>
		  	<StateFlow xmi:id="incoming_id" start="EAID_44A8E10D_B49F_40d2_9FEB_18E6167CFFD8" end="final_node_id"/>
	    </links>
	  </element>
  </xmi:XMI>
END
    transitions = [create_transition_with_id('incoming_id')]
    @final_node_rexml = REXML::Document.new(xmi).elements['xmi:XMI/subvertex']
    detail_element = REXML::Document.new(details_xmi).elements['xmi:XMI/element']
    details_table = {@final_node_rexml.attributes['xmi:id'] => detail_element}
    @final_node = UmlTools::Model::FinalNode.read_from_xmi(@final_node_rexml, details_table, transitions)
  end

  must 'parse final node id' do
    assert_equal('final_node_id', @final_node.id)
  end

  must 'identify itself as a final node' do
    assert(@final_node.final_node?)
  end

  must 'parse final node name' do
    assert_equal('Final', @final_node.name)
  end

  must 'parse final node incoming id' do
    assert_equal('incoming_id', @final_node.incoming[0].id)
  end

  must 'parse final node stereotype' do
    assert_equal('finalnode', @final_node.stereotype)
  end

  must 'parse final node documentation' do
    assert_equal('This is a final node.', @final_node.doc)
  end

  def create_transition_with_id(transition_id)
    UmlTools::Model::Transition.new('', transition_id, nil, nil, nil, nil, nil)
  end

end
