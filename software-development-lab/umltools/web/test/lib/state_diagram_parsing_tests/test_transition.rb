require_relative '../test_base'

class TestTransition < Test::Unit::TestCase

  # Called before every test method runs. Can be used
  # to set up fixture information.
  def setup
    xmi = <<END
<?xml version="1.0" encoding="windows-1252"?>
<xmi:XMI xmi:version="2.1" xmlns:uml="http://schema.omg.org/spec/UML/2.1" xmlns:xmi="http://schema.omg.org/spec/XMI/2.1">
  <transition xmi:type="uml:Transition" xmi:id="TransitionID" visibility="public" kind="local" source="SourceID" target="TargetID">
    <trigger xmi:idref="EAID_E87134B2_A1BB_42c9_9953_617D6190DB55"/>
		<trigger xmi:idref="EAID_778B4CEA_942A_4f54_9582_A30635CC23FF"/>
    <guard xmi:type="uml:Constraint" xmi:id="EAID_CO000000_579A_417f_949C_03DE719FF232">
      <specification xmi:type="uml:OpaqueExpression" xmi:id="EAID_OE000000_579A_417f_949C_03DE719FF232" body="GuardBody"/>
    </guard>
    <effect xmi:type="uml:OpaqueBehavior" xmi:id="EAID_OB000000_9FA2_427f_B915_D1963B487EA1" body="EffectBody"/>
  </transition>
</xmi:XMI>
END
    details_xmi = <<END
<?xml version="1.0" encoding="windows-1252"?>
<xmi:XMI xmi:version="2.1" xmlns:uml="http://schema.omg.org/spec/UML/2.1" xmlns:xmi="http://schema.omg.org/spec/XMI/2.1">
  <connector xmi:idref="TransitionID">
				<source xmi:idref="SourceID">
				</source>
				<target xmi:idref="TargetID">
				</target>
				<properties ea_type="StateFlow" direction="Source -&gt; Destination" stereotype="Transition stereotype"/>
				<documentation value="This is documentation for a transition."/>
				<extendedProperties virtualInheritance="0" privatedata2="GuardBody"/>
			</connector>
</xmi:XMI>
END
    @transition_rexml = REXML::Document.new(xmi).elements['xmi:XMI/transition']
    detail_element = REXML::Document.new(details_xmi).elements['xmi:XMI/connector']
    details_table = {@transition_rexml.attributes['xmi:id'] => detail_element}
    triggers_hash = Hash['EAID_E87134B2_A1BB_42c9_9953_617D6190DB55' =>
                             UmlTools::Model::Trigger.new('', 'EAID_E87134B2_A1BB_42c9_9953_617D6190DB55', nil, nil, nil, nil),
                         'EAID_778B4CEA_942A_4f54_9582_A30635CC23FF' =>
                             UmlTools::Model::Trigger.new('', 'EAID_778B4CEA_942A_4f54_9582_A30635CC23FF', nil, nil, nil, nil)]
    @transition = UmlTools::Model::Transition.read_from_xmi(@transition_rexml, details_table, triggers_hash)
  end

  must 'parse transition id' do
    assert_equal('TransitionID', @transition.id)
  end

  must 'parse transition trigger ids' do
    assert_equal('EAID_E87134B2_A1BB_42c9_9953_617D6190DB55', @transition.triggers[0].id)
    assert_equal('EAID_778B4CEA_942A_4f54_9582_A30635CC23FF', @transition.triggers[1].id)
  end

  must 'parse transition guard body' do
    assert_equal('GuardBody', @transition.guard)
  end

  must 'parse transition effect body' do
    assert_equal('EffectBody', @transition.effect)
  end

  must 'parse transition documentation' do
    assert_equal('This is documentation for a transition.', @transition.doc)
  end

  must 'parse transition stereotype' do
    assert_equal('transition stereotype', @transition.stereotype)
  end

end
