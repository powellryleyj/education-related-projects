require_relative '../test_base'

class TestStateModel < Test::Unit::TestCase

  # Called before every test method runs. Can be used
  # to set up fixture information.
  def setup
    text = IO.read(File.join($test_resource_directory,'xmi', 'ea', 'state-xmi-2.1.xmi'))
    @state_model_element = REXML::Document.new(text).elements['xmi:XMI/uml:Model']
    @state_model = UmlTools::Model::StateModel.read_xmi_elements(@state_model_element, Hash.new)
  end

  must 'read all transitions for the state diagram' do
    assert_equal(6, @state_model.transitions.length)
  end

  must 'read all states for the state diagram' do
    assert_equal(4, @state_model.states.length)
  end

  must 'read all initial nodes for the state diagram' do
    assert_equal(1, @state_model.initial_nodes.length)
  end

  must 'read all final nodes for the state diagram' do
    assert_equal(1, @state_model.final_nodes.length)
  end

  must 'have the all elements for state model' do
    assert_equal(12, @state_model.all_elements.length)
  end
end
