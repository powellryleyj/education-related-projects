require_relative '../test_base'

class TestTrigger < Test::Unit::TestCase
  # Called before every test method runs. Can be used
  # to set up fixture information.
  def setup
    change_trigger_xmi = <<END
<?xml version="1.0" encoding="windows-1252"?>
<xmi:XMI xmi:version="2.1" xmlns:uml="http://schema.omg.org/spec/UML/2.1" xmlns:xmi="http://schema.omg.org/spec/XMI/2.1">
  <packagedElement xmi:type="uml:Trigger" xmi:id="EAID_F9348A95_3FD6_42aa_9963_80C8F3120D7E" name="ChangeTrigger_Two-Three" visibility="public">
    <event xmi:type="uml:ChangeEvent" xmi:id="EAID_EX000000_3FD6_42aa_9963_80C8F3120D7E">
      <changeExpression symbol="change"/>
    </event>
  </packagedElement>
</xmi:XMI>
END

  time_trigger_xmi = <<END
<?xml version="1.0" encoding="windows-1252"?>
<xmi:XMI xmi:version="2.1" xmlns:uml="http://schema.omg.org/spec/UML/2.1" xmlns:xmi="http://schema.omg.org/spec/XMI/2.1">
  <packagedElement xmi:type="uml:Trigger" xmi:id="EAID_35F6BB73_0E08_4d1d_A244_D52AB89732B7" name="TimeTrigger_One-Two" visibility="public">
    <event xmi:type="uml:TimeEvent" xmi:id="EAID_EX000000_0E08_4d1d_A244_D52AB89732B7">
      <when expr="10s"/>
    </event>
  </packagedElement>
</xmi:XMI>
END

    @change_trigger_rexml = REXML::Document.new(change_trigger_xmi).elements['xmi:XMI/packagedElement']
    @change_trigger = UmlTools::Model::Trigger.read_from_xmi(@change_trigger_rexml, nil)
    @time_trigger_rexml = REXML::Document.new(time_trigger_xmi).elements['xmi:XMI/packagedElement']
    @time_trigger = UmlTools::Model::Trigger.read_from_xmi(@time_trigger_rexml, nil)
  end

  must 'parse trigger id' do
    assert_equal('EAID_F9348A95_3FD6_42aa_9963_80C8F3120D7E', @change_trigger.id)
  end

  must 'parse trigger name' do
    assert_equal('ChangeTrigger_Two-Three', @change_trigger.name)
  end

  must 'parse trigger type' do
    assert_equal('uml:ChangeEvent', @change_trigger.type)
  end

  must 'parse trigger expression for change event' do
    assert_equal('change', @change_trigger.expression)
  end

  must 'parse trigger expression for time event' do
    assert_equal('10s', @time_trigger.expression)
  end
end
