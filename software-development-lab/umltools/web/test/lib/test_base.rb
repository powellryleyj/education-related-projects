#!/usr/bin/env ruby
# test_base: base for all tests

gem 'test-unit'

require_relative '../../lib/uml_tools'

require 'test/unit'
require 'ci/reporter/rake/test_unit_loader'
require 'stringio'
# require 'flexmock/test_unit'
# require 'flexmock'
require 'rexml/document'
require 'yaml'

module TestBase

  protected

  def assert_equal_lines(expected_text, actual_text)
    expected_lines = expected_text.split("\n")
    actual_lines   = actual_text.split("\n")
    assert_equal_sequences(expected_lines, actual_lines)
  end

# currently unused (and untested, though should work fine):
#   def assert_equal_sorted_lines(expected_text, actual_text)
#     expected_lines = expected_text.split("\n").sort
#     actual_lines   = actual_text.split("\n").sort
#     assert_equal_sequences(expected_lines, actual_lines)
#   end

  def assert_equal_sequences(expected, actual)
    in_expected_not_in_actual = expected - actual
    in_actual_not_in_expected = actual - expected
    error_message = ''
    unless in_actual_not_in_expected.empty?
      error_message += "The following items were in the results, but not expected:\n"+
          in_actual_not_in_expected.join(",\n")
    end
    unless in_expected_not_in_actual.empty?
      error_message += "\n" unless error_message.length == 0
      error_message += "The following items were expected, but not in the results:\n" +
          in_expected_not_in_actual.join(",\n")
    end
    unless error_message.length == 0
      flunk error_message
    end
  end

end

$test_directory = File.expand_path File.dirname(__FILE__)
$test_resource_directory = File.join($test_directory, 'resources')
$comparisons_directory = File.join($test_resource_directory, 'comparisons')
$lib_directory = File.expand_path('../../lib', $test_directory)

# used to load models only on demand
class DelayedModel
  attr_reader :mdl
# Uncomment when needed:
#   def initialize(&loader_block)
#     @loader = loader_block
#   end
#   def mdl
#     if @loader
#       @mdl = @loader.call
#       @loader = nil
#     end
#     @mdl
#   end
end

class DelayedModelFile < DelayedModel
  def initialize(filename)
    @file = filename
    @file_path = File.join($test_resource_directory, @file)
  end
  def mdl
    @mdl ||= UmlTools::Model::FullModel.load(@file_path)
  end
end


module Test::Unit
  class TestCase

    def self.must(name, &block)
      test_name = "test_#{name.gsub(/\s+/,'_')}".to_sym
      defined = instance_method(test_name) rescue false
      raise "#{test_name} is already defined in #{self}" if defined
      if block_given?
        define_method(test_name, &block)
      else
        define_method(test_name) do
          flunk "No implementation provided for #{name}"
        end
      end
    end

  end
end
