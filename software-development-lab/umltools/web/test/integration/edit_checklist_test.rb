require_relative '../test_helper'
require_relative 'test_that_requires_login'

class EditChecklistTest < TestThatRequiresLogin
  # test "login and upload checklist file" do
  #   login_as_instructor_account
  #   test_checklist = File.join(File.dirname(__FILE__), '..', 'lib/resources/checklists/AllUmlintAndGraderChecks.json')
  #   file = Rack::Test::UploadedFile.new(test_checklist, "application/json")
  #   file_array = [file]
  #
  #   assert_difference('CheckList.count') do
  #     post '/umlgrader/upload_checklist_file', params:
  #         {
  #           checklist_file: file_array
  #         }
  #   end
  #
  #   assert_response :redirect
  #   assert_equal "Checklist successfully uploaded!", flash[:msg]
  # end
  #
  # test "login and download checklist data" do
  #   login_as_instructor_account
  #   get '/umlgrader/download_checklist_file/3'
  #
  #   assert_response :success
  # end
  #
  # test "login and edit checklist data" do
  #   login_as_instructor_account
  #
  #   post "/umlgrader/do_update_checklist", params:
  #       {
  #          commit: "Save",
  #          checklist_id: 4,
  #          description: "Updated Checklist",
  #          language: "C++",
  #          retired: "0",
  #          include: {
  #            "22": true
  #          },
  #          checkitem: {}
  #       }
  #   assert_response :redirect
  #   assert_nil flash[:error]
  #   assert_equal "Checklist Updated Checklist updated", flash[:notice]
  #   check_list = CheckList.find_by_id(4)
  #   assert_equal "Updated Checklist", check_list.description.chomp
  #   assert_equal "C++", check_list.language.chomp
  #   assert !check_list.retired
  # end
end
