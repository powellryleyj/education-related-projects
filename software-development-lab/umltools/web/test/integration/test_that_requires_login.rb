require_relative '../test_helper'

# TODO: this should be a helper function available to all inheriting tests
class TestThatRequiresLogin < ActionDispatch::IntegrationTest
  protected

  def login_as_instructor_account
    https!
    post '/umlint/do_login', params: { email_address: accounts(:hasker).email_address,
                                       change_password: accounts(:hasker).password_digest }
    follow_redirect!
    https!(false)

    # Suppress depreciation warning. See
    #    http://stackoverflow.com/questions/20361428/rails-i18n-validation-deprecation-warning
    I18n.enforce_available_locales = false
  end
end
