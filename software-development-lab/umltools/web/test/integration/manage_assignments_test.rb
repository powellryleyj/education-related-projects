require_relative '../test_helper'

class ManageAssignmentsTest < TestThatRequiresLogin

  # TODO: This test does not belong here
  # test 'check password' do
  #   i = accounts(:hasker)
  #   assert i.authenticate('hasker123')
  # end
  #
  # TODO: This test does not belong here
  # test 'login and browse site' do
  #   https! # login via https
  #
  #   get '/umlint/login'
  #   assert_response :success
  #
  #   post '/umlint/do_login', params: { email_address: accounts(:hasker).email_address,
  #                                      change_password: accounts(:hasker).password_digest }
  #   follow_redirect!
  #
  #   # expects a path like %2Finedex.html or /index
  #   assert_match /(\/|%2F)index(\.html)?/, path, 'Redirect does not path expected value ' \
  #                                                "\n\tflash[:error]: \"" + (flash[:error] || '') + "\"\n"
  # end

  test 'login and upload class diagram' do
    login_as_instructor_account

    # prepare a file to upload
    test_xmi = File.join(File.dirname(__FILE__), '..', 'lib/resources/xmi/ea/vehicle_ea2.xmi')
    file = Rack::Test::UploadedFile.new(test_xmi, 'application/xml')

    assert_difference('Problem.count') do
      post '/umlgrader/upload_assignment', params:
           {
             name: 'A Name',
             description: 'A description',
             started_at: '01/01/2020',
             due_at: '01/01/2030',
             closed_at: '01/01/2040',
             solution_file: file,
             assignment_type: Problem::CLASS_DIAGRAM_TYPE
           }
    end

    # assert_redirected_to confirm_msg
    assert_response :redirect
    assert_equal 'Problem successfully uploaded', flash[:msg]
  end

  test 'login and upload sequence diagram' do
    login_as_instructor_account

    # prepare a file to upload
    test_xmi = File.join(File.dirname(__FILE__), '..', 'lib/resources/xmi/ea/vehicle_ea2.xmi')
    file = Rack::Test::UploadedFile.new(test_xmi, 'application/xml')

    assert_difference('Problem.count') do
      post '/umlgrader/upload_assignment', params:
           {
             name: 'A Name',
             description: 'A description',
             assignment_type: Problem::SEQUENCE_DIAGRAM_TYPE,
             started_at: '01/01/2020',
             due_at: '01/01/2030',
             closed_at: '01/01/2040',
             solution_file: file
           }
    end

    assert_not_empty Problem.find_by_name('A Name').serialized_model

    assert_response :redirect
    assert_equal 'Problem successfully uploaded', flash[:msg]
  end

  test 'login and upload incomplete assignment' do
    login_as_instructor_account

    # prepare a file to upload
    test_xmi = File.join(File.dirname(__FILE__), '..', 'lib/resources/xmi/ea/vehicle_ea2.xmi')
    file = Rack::Test::UploadedFile.new(test_xmi, 'application/xml')

    post '/umlgrader/upload_assignment', params:
         {
           name: 'Another Name',
           description: '',
           started_at: '01/01/2020',
           due_at: '01/01/2030',
           closed_at: '',
           solution_file: file,
           assignment_type: Problem::CLASS_DIAGRAM_TYPE
         }

    # re-displays create-assignment form
    problem = assigns(:problem) # (1/28/19) powellr - I'm not sure what this does, but the fixture 'assigns' does not exist
    assert_equal(problem.errors.count, 1) # 1 error for missing description
    assert_select('form') # displays the partially filled form
    assert_response :success
  end

  test 'login and create assignment' do
    login_as_instructor_account

    get '/umlgrader/create_assignment'

    assert_response :success

    # assert the form contains all the expected fields (overcomplicated?)
    # left out 'decription' because it's a <textarea> and not an <input>
    input_fields = %w[name started_at due_at closed_at solution_file]
    assert_select('form') do # displays the partially filled form
      input_fields.each do |name|
        assert_select 'input[name=?]', name
      end
    end
  end

  test 'login and manage assignments' do
    login_as_instructor_account

    get '/umlgrader/manage_assignments'

    assert_response :success
    assert_select('tbody') do # displays the partially filled form
      assert_select 'tr', Problem.count + 1 #+1 for the row offering to create a new assignment
    end
  end

  test 'login and delete assignment' do
    login_as_instructor_account # logged in as hasker so following problem should be created via his idea

    p_open = Problem.create(name: 'P_OPEN',
                            description: 'Problem is open.',
                            assignment_type: Problem::CLASS_DIAGRAM_TYPE,
                            solution_file: 'file.xmi',
                            started_at: (Time.now - Problem::DAY),
                            due_at: (Time.now + Problem::DAY),
                            closed_at: Time.new(2100, 1, 1),
                            instructor_id: accounts(:hasker).id)

    assert_difference('Problem.count', -1) do
      delete '/umlgrader/delete_assignment/' + p_open.id.to_s
    end

    assert_response :redirect # redirects to manage_assignments view
  end

  test 'login and modify assignment' do
    login_as_instructor_account

    # load the edit-assignment page
    problem = Problem.where(name: 'Snow Plow Routes').take
    get '/umlgrader/modify_assignment/' + problem.id.to_s

    assert_response :success
    # assert the form contains all the expected fields (overcomplicated?)
    # left out 'decription' because it's a <textarea> and not an <input>
    input_fields = {
      name: problem.name,
      # description: problem.description, # doesn't use an <input> and value is store in innerHTML rather than value attribute
      started_at: problem.started_at.strftime('%m/%d/%Y'),
      due_at: problem.due_at.strftime('%m/%d/%Y'),
      closed_at: problem.closed_at.strftime('%m/%d/%Y')
    }
    assert_select('form') do # displays the partially filled form
      input_fields.each do |name, value|
        assert_select "input[name='#{name}']"
        assert_select "input[value='#{value}']"
      end
    end

    #
    # modify the assignment
    #

    # prepare a file to upload
    test_xmi = File.join(File.dirname(__FILE__), '..', 'lib/resources/xmi/ea/vehicle_ea2.xmi')
    file = Rack::Test::UploadedFile.new(test_xmi, 'application/xml')

    # submit a partially-filled assignment form for updating
    post '/umlgrader/upload_assignment/' + problem.id.to_s, params:
         {
           name: 'Modified Name',
           description: 'Modified Description',
           started_at: '01/01/1999',
           solution_file: file,
           assignment_type: problem.assignment_type
         }
    follow_redirect!
    assert_response :success
    assert_equal 'Problem successfully updated', flash[:notice]
  end

  # TODO: This test is in the wrong place.
  # test 'login and fail to create instructor over mismatched passwords' do
  #   login_as_instructor_account
  #   post '/umlgrader/create_instructor', params:
  #       {
  #         first_name: 'Name',
  #         last_name: 'Last',
  #         change_password: 'test-pass',
  #         confirm_password: 'diff-pass',
  #         email: 'email@email',
  #         organization: 'organization'
  #       }
  #   assert_response :redirect # Passwords don't match
  #   follow_redirect!
  #   assert_equal 'Passwords do not match!', flash[:error]
  # end

  # TODO: This test is in the wrong place
  # test 'login and create instructor' do
  #   login_as_instructor_account
  #   post '/umlgrader/create_instructor', params:
  #        {
  #          first_name: 'FName',
  #          last_name: 'LastNom',
  #          change_password: 'test-pass',
  #          confirm_password: 'test-pass',
  #          email: 'email@email',
  #          organization: 'organization'
  #        }
  #   assert_response :redirect
  #   follow_redirect!
  #   assert_equal "Instructor 'email@email' has been created!", flash[:notice]
  #   assert Account.where(given_name: 'FName').exists?, 'no created instructor'
  #   assert Account.where(email_address: 'email@email').exists?, 'no created instructor'
  #   Account.where(email_address: 'email@email').destroy_all
  # end
end
