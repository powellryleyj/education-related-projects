require_relative '../test_helper'

class ManageGroupsTest < TestThatRequiresLogin
  test "create group" do
    login_as_instructor_account

    # Makes sure that a group is properly created
    post "/umlgrader/create_group", params: {
      name: 'Group 1',
      description: "Group Used For Test"
    }

    assert_response :redirect
    group = Group.find_by(name: 'Group 1')
    assert_equal group.nil?, false
    assert_equal group.retired, false
    assert_equal group.description, "Group Used For Test"
  end

  # Makes sure a group can be properly retired
  test "retire group" do
    login_as_instructor_account
    group = gen_group

    delete "/umlgrader/delete_group/#{group.id}", params: {
      id: group.id
    }

    assert_response :redirect
    group = Group.find_by_id(group.id)
    assert_equal group.nil?, false
    assert_equal group.retired, true
  end

  # Makes sure it can add atleast one student to a group
  test "add student to group" do
    login_as_instructor_account
    group = gen_group
    student = Student.find_by_id(2)

    post "/umlgrader/group_add_students/#{group.id}", params: {
      id: group.id,
      students_import: "#{student.email_address}"
    }

    assert_response :redirect
    assert_equal group.students.include?(student), true
  end

  # Makes sure it can add multiple students to a group at once
  test "add multiple students to group" do
    login_as_instructor_account
    group = gen_group
    student1 = Student.find_by_id(2)
    student2 = Student.find_by_id(3)

    post "/umlgrader/group_add_students/#{group.id}", params: {
      id: group.id,
      students_import: "#{student1.email_address};#{student2.email_address}"
    }

    assert_response :redirect
    assert_equal group.students.include?(student1), true
    assert_equal group.students.include?(student2), true
  end

  # Makes sure it can remove a student from a group
  test "remove student from group" do
    login_as_instructor_account
    group = gen_group
    student = Student.find_by_id(2)
    group_add_students group, [student]

    delete "/umlgrader/delete_group_student/#{group.id}/#{student.id}", params: {
      group: group.id,
      student: student.id
    }

    assert_response :redirect
    assert_equal student.nil?, false
    assert_equal group.students.include?(student), false
  end

  # Mkaes sure it can add a problem to the group successfully
  test "add problem to group" do
    login_as_instructor_account
    group = gen_group
    problem = gen_problem

    post "/umlgrader/group_add_problem/#{group.id}", params: {
      assignment: problem.id
    }

    assert_response :redirect
    assert_equal group.problems.include?(problem), true
  end

  # Makes sure it can remove a problem from the group successfully
  test "remove problem from group" do
    login_as_instructor_account
    group = gen_group
    problem = gen_problem
    group_add_problem group, problem

    delete "/umlgrader/delete_group_problem/#{group.id}/#{problem.id}", params: {
      group: group.id,
      problem: problem.id
    }

    assert_response :redirect
    assert_equal problem.nil?, false
    assert_equal group.problems.include?(problem), false
  end


  # Helper Methods Below
  private

  def gen_group
    post "/umlgrader/create_group", params: {
      name: 'Group C',
      description: "Desc For Group C"
    }

    Group.find_by(name: 'Group C')
  end

  def group_add_students group, students
    emails = ""
    emails += "#{students.shift}"
    students.each { |student|
      emails += ";#{student.email_address}"
    }

    post "/umlgrader/group_add_students/#{group.id}", params: {
      id: group.id,
      students_import: "#{emails}"
    }
  end

  def group_add_problem group, problem
    post "/umlgrader/group_add_problem/#{group.id}", params: {
      assignment: problem.id
    }
  end

  def gen_problem
    login_as_instructor_account

    # prepare a file to upload
    test_xmi = File.join(File.dirname(__FILE__), '..', 'lib/resources/xmi/ea/vehicle_ea2.xmi')
    file = Rack::Test::UploadedFile.new(test_xmi, 'application/xml')

    assert_difference('Problem.count') do
      post '/umlgrader/upload_assignment', params:
           {
             name: 'A Name',
             description: 'A description',
             started_at: '01/01/2020',
             due_at: '01/01/2030',
             closed_at: '01/01/2040',
             solution_file: file,
             assignment_type: Problem::CLASS_DIAGRAM_TYPE
           }
    end

    Problem.find_by(name: 'A Name')
  end

end
