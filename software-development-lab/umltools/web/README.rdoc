Base setup (that doesn't depend on rails):

 1. sudo apt-get install -y apache2

 2. adding mysql; version 1:
        sudo apt-get install -y mysql-server mysql-client libmysqlclient-dev
    alternative version of adding mysql:
        sudo apt-get install -y mysql-server-core
        sudo apt-get install -y mysql-client libmysqlclient-dev
     - Note: must do one or other before installing mysql2

 3. Optional (enables some of the scripts):
        sudo apt-get install -y csh

 4. See https://rvm.io/rvm/install to install rvm
   [as of Jan 2016, this means:]
      gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
      \curl -sSL https://get.rvm.io | bash -s stable --ruby

 5. Need aspell:
    - install from http://aspell.net/
        > also need to install the English library
        > alternatively:
                sudo apt-get install -y aspell
          (this also installs aspell-en for the English language)
    Note for windows systems, must add aspell directory to path.

 6. Set up database (where new-password is your own password)
        mysqladmin -u root password 'new-password'

 6.5 Create the umladmin user:
        sudo adduser umladmin
     The password can be anything reasonable; I used creamcrop on the
     deployment server

 7. Then
        sudo su umladmin
        rvm install 2.4.5
        gem install rails 5.1.6
        gem install bundle      # may not be needed
        git clone umltools to ~
           (the following directions run it out of my home directory)
        cd umltools/web
        bundle install

Running the server:

  0. Start up mysql
         mysqladmin -u root password frostie
         /etc/init.d/mysql start
         sudo /usr/bin/mysql_secure_installation

  1. Create umlintd, umlint_test, umlint databases in mysql

  > some variation on steps 2a-2c seems to have worked, but I got
    lots of errors along the way so it's not quite clear how to
    redo these steps
  2a. execute
        mysqladmin -u umladmin -p create umlint create umlintd create umlint_test
	//powellr: "sudo mysqladmin -u root create umlint create umlintd create umlint_test"
  2b. create new users for mysql
        sudo mysqlaccess -u hasker -p umlint umlint
	//powellr: 'mysqlaccess' not found
        (prompts for password)
  2c. Use the following commands (as mysql administrator) to give user umlint
     access to the databases:
       grant all on umlint.* to 'umlint'@'localhost' identified by 'password';
       grant all on umlintd.* to 'umlint'@'localhost' identified by 'password';
       grant all on umlint_test.* to 'umlint'@'localhost' identified by 'password';

  3. Copy baseline_database.yml to database.yml and set passwords in
     database.yml to info in words
       > note that database.yml is ignored by git

     To support development,
        cd config/environments
        cp baseline_development.rb development.rb
     and review development.rb to check if anything needs updating. There generally
     shouldn't be any changes for deployment on a Linux system, changes are
     more likely for Windows development boxes.

  4. cd to web subdirectory and type
        bundle install
            (note: I used to add the options "--path vendor/bundle",
             but I did not need that in Fall, 2014)

  5. run tests:
        cd web
        > if have not done: cd config; cp sqlite_database.yml database.yml
              (other configs will work for testing, but this requires
              no other setup other than installing sqlite)
        export RAILS_ENV=test
        rake db:create
        rake db:migrate
        rake test:run_all
          >> Alternative: can run them separately
                # test rails side
                rake test
                # test umlgrader, umlint tools by themselves
                rake lib:test
          >> alternative to rake lib:test: cd test/lib; ruby test_all.rb
          >> note: should be able to do "ruby -w test_all.rb", but as of
             Sep 2016 there are a large number of warnings
          >> as of Sept. 2016: teamcity is running rake test:run_all

  6. set up production:
        export RAILS_ENV=production
        bundle exec rake db:create
        bundle exec rake db:migrate
        rake instructor:create
          > system will prompt for an email address and password
            for the primary administration account (primary in the
            sense that this account can create others - the other
            instructor accounts may be able to create more)
          > this code is in lib/tasks/instructor.rake
        bundle exec rake assets:precompile
          > need to redo if change stylesheets, etc
          > if have problems viewing tutorial images, try a shift-reload
          > if no css pages, set
                config.serve_static_assets = true
            in ~/umltools/web/config/environments/production.rb
            [note from before 2016: may want to set this variable back
             to false if get apache2 working]

  6.5 To run checks on a file
         cd web
         ruby lib/umlTools/check.rb

  7. To run a simple test to make sure the core system is running:
       a. rvmsudo rails server -p 80 -e production
          (sudo rails server -p 80 -e production)
          [need sudo to access port 80]
       b. Visit
                http://umltools.msoe.edu:/umlgrader
								//powellr - this grabs the deployed server; visit http://localhost:80
								// to check server is running
       c. select 
                web/test/lib/resources/clean.mdl        -- no errors
       d. web/test/lib/resources/min-bad.mdl (with all checks enabled)
          and should get
              Class SubClass inherits from multiple classes (BaseClass, AltBase) -
                generalization arrows may be backwards
              Association between Stuff and BaseClass: composition should have multiplicity 1
              Attribute stuff in class BaseClass appears to not be simple -
                replace by an association with the class
              Complex attribute nums[] in class Stuff - replace by an association to a container
              Missing documentation: classes BaseClass, SubClass, Stuff, AltBase
              Missing documentation for class Stuff member: nums[]
              Missing documentation: classes BaseClass, SubClass, Stuff, AltBase
          Also check that "more information" gives good results.
          note that resources/gifs contains min-bad.gif
       e. web/test/lib/resources/xmi/min2.xmi (with all checks)
          and should get
              Attribute color in Bottle is public
              Attribute height in Baby is public
              Missing documentation for class Baby member: height
              Missing documentation for class Bottle member: color
              Missing documentation: classes Baby, Bottle
       f. Visit
                http://umltools.msoe.edu:/umlint
          and select
     Finally, shut down this server since will use passenger (below).

  8. Set up server to deploy project:
        gem install passenger
        sudo passenger-install-apache2-module
        sudo apt-get install libapache2-mod-passenger
        sudo a2enmod passenger
                - should get passenger already enabled
        cd /etc/apache2/sites-available
        sudo cp 000-default.conf umltools.conf
        Edit umltools.conf; see
            - see https://www.digitalocean.com/community/tutorials/how-to-deploy-a-rails-app-with-passenger-and-apache-on-ubuntu-14-04
        sudo a2dissite 000-default
        sudo a2ensite umltools
        sudo service apache2 restart
  * see steps below to configure mysql

        -----------------
        aborted version:

        rvmsudo passenger-install-apache2-module
          > per the directions from this tool:
        sudo apt-get install libcurl4-openssl-dev
        sudo apt-get install apache2-threaded-dev
        sudo apt-get install libapr1-dev
        sudo apt-get install libaprutil1-dev
        rvmsudo passenger-install-apache2-module
        rvmsudo passenger-install-apache2-module
        edit /etc/apache2/mods-available/passenger.conf
                add:
   <IfModule mod_passenger.c>
     PassengerRoot /home/umladmin/.rvm/gems/ruby-2.4.5@global/gems/passenger-5.0.7
     PassengerDefaultRuby /home/umladmin/.rvm/gems/ruby-2.4.5/wrappers/ruby
   </IfModule>
        edit /etc/apache2/mods-available/passenger.load
                add:
   LoadModule passenger_module /home/umladmin/.rvm/gems/ruby-2.4.5@global/gems/passenger-5.0.7/buildout/apache2/mod_passenger.so

        sudo service apache2 restart

        -----------------

        Did not do:
                Please add /home/umladmin/.rvm/gems/ruby-2.4.5@global/gems/passenger-5.0.7/bin to PATH.
           remove passenger from
                /home/umladmin/.rvm/rubies/ruby-2.4.5/bin/passenger
                /usr/local/bin/passenger

        https://www.phusionpassenger.com/documentation/Users%20guide%20Apache.html


  6. To run server on port 80 in production environment:
     [** And see above for server tests! **]
      - one time:
                setenv RAILS_ENV production
                [or in bash: RAILS_ENV=production]
                rake db:migrate
      - I'm not sure how often the following needs to be done:
                setenv RAILS_ENV production
                bundle exec rake assets:precompile
      - running server:
                nohup sudo rails server -e production -p 80
            [need sudo so can access port 80]
      - if no css pages, set
                config.serve_static_assets = true
        in ~/umltools/web/config/environments/production.rb
      - may want to set this variable back to false if get apache2
        working

To restart the app:
        sudo passenger-config restart-app /home/umladmin/umltools/web
    or
        sudo passenger-config restart-app /home
    - can leave off app name and it'll prompt with options; hit return
    - writing just /home restarts all in that folder

----------------------------------------------------------------------

Information about using apache2 as the front end:


Deployment on umltools
  sudo apt-get apache2
  sudo gem install passenger
  sudo passenger-install-apache2-module

-- passenger said:
 * To install Curl development headers with SSL support:
   Please run apt-get install libcurl4-openssl-dev or libcurl4-gnutls-dev, whichever you prefer.

 * To install Apache 2 development headers:
   Please install it with apt-get install apache2-threaded-dev

 * To install Apache Portable Runtime (APR) development headers:
   Please install it with apt-get install libapr1-dev

 * To install Apache Portable Runtime Utility (APU) development headers:
   Please install it with apt-get install libaprutil1-dev

If the aforementioned instructions didn't solve your problem, then please take
a look at the Users Guide:
---------------------------

Added by powellr (Ryley Powell)

After 'bundle install' in step 7 of base setup:

Ruby Sass is deprecated and will be unmaintained as of 26 March 2019.

* If you use Sass as a command-line tool, we recommend using Dart Sass, the new
  primary implementation: https://sass-lang.com/install

* If you use Sass as a plug-in for a Ruby web framework, we recommend using the
  sassc gem: https://github.com/sass/sassc-ruby#readme

* For more details, please refer to the Sass blog:
  http://sass.logdown.com/posts/7081811

----------------------------

See:

http://rubyonrails.org/deploy

http://stackoverflow.com/questions/19190773/binding-a-rails-server-to-port-80-on-linux-without-running-it-as-root
Probably want to run nginx or similar web server; config for nginx:

upstream rails_server {
  server localhost:3000;
}

server {
  listen 80;

  location / {
    root /home/deploy_user/rails_app/public;
    try_files $uri @missing;
  }

  location @missing {
    proxy_pass http://rails_server;
    proxy_set_header Host $http_host;
    proxy_redirect off;
  }
}


----------------------------------------------------------------------

# no longer needed; really kept as a warning
        gem install test-unit
        gem install mysql2
        gem install linecache19
        gem install rake
        gem install ruby-debug19
                > if don't install this, will get errors when try to
                  run bundle install in rails project
        sudo script/restart

----------------------------------------------------------------------

TeamCity setup:
  *


----------------------------------------------------------------------

Notes about config changes I made:

  * web/config/environments/production.rb
      - added
          config.assets.precompile << '*.js'
          config.serve_static_assets = true
        These two should be re-evaluated down the road

----------------------------------------------------------------------


Old stuff:

----------------------------------------------------------------------

  4. If desired, back up current umlint database:
        cd server
        script/backup-production
     The file will be placed in log/backup-production....sql
     I check this in to Subversion.
     Alternatively, this can be done by hand:
        mysqldump -u umlint umlint -p > `date '+%F.%H%M%S.dmp'`; gzip *.dmp

  6. If desired, erase all data in umlint database and reload the globals

        cd server
        setenv RAILS_ENV production
        script/reload

     [ old text:
     Can ignore warnings such as

        WARNING: 'require 'rake/rdoctask'' is deprecated.  Please use 'require 'rdoc/task' (in RDoc 2.4.2+)' instead.
    ]

----------------------------------------------------------------------

Printing all tickets that have been used:

 1. Identify problem:
        select problem_id
        Problem.all.map(&:name)
        target = Problem.all.map(&:name)[...]
        p = Problem.find_by_name(target)
 2. List all submission ids:
        ss = Submission.where(:problem_id == p.id).select { |s| s.warnings.size == 0 }
        ss.map(&:ticket)
        Addressing problem of unnamed elements:
           ss = Submission.where(:problem_id == p.id).select { |s|
                 s.warnings.size > 0 &&
                 s.warnings.select { |w| w.message =~ /Unnamed/ }.size == s.warnings.size }

 check_id

        Submission.all.map(&:ticket)
 3.
