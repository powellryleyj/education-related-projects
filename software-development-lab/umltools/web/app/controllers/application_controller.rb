require(File.join(File.dirname(__FILE__), '..', '..', 'lib', 'uml_tools'))

class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :set_base_url # (10/30/18) powellr - changed from before_filter to follow rails-5.1

  def login
    unless need_login?
      redirect_to '/'
      return
    end

  end

  def do_login
    if params[:commit] == 'Cancel'
      redirect_to @base_url
      return
    end

    session[:return_to] ||= request.referrer # sets on the first time the login page is entered

    e = params[:email_address]
    p = params[:change_password]
    if e.nil? || e.empty? || p.nil? || p.empty?
      flash[:error] = 'Please enter an email address and password.'
      redirect_to @base_url + '/login'
      return
    end
    i = Account.find_by(:email_address => e)#Instructor.where(:email_address => e).take
    # print "-----------------------------\nFOUND INSTRUCTOR\n-----------------------------\n"
    begin
      if i.nil?
        flash[:error] = 'Invalid email address.'
        redirect_to @base_url + '/login'
      elsif !i.authenticate(p)
        flash[:error] = 'Invalid email password.'
        redirect_to @base_url + '/login'
      else
        session[:account_id] = i.id
        redirect_to session.delete(:return_to) || '/'
      end
    rescue
      flash[:error] = 'Login failed.'
      redirect_to @base_url + '/login'
    end
  end

  def logout
    session[:account_id] = nil
    @account = nil
    redirect_to @base_url + '/index'
  end

  protected

  # returns true if need to log in, otherwise returns false and
  #  sets @account
  def need_login?
    return true if session[:account_id].nil? || session[:account_id].blank?
    @account = Account.find_by_id(session[:account_id])
    @account.nil?
  end


  def need_instructor_login?
    Instructor.find_by_id(session[:account_id]).nil?
  end

  def set_base_url
    @base_url = ''
  end

end
