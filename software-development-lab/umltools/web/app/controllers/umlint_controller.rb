class UmlintController < ApplicationController
  respond_to :html, :js

  protect_from_forgery :except => [:desktop_results]

  def set_base_url
    @base_url = '/umlint'
  end

  def index
    @checklist_id = session[:checklist] || 0
  end

  def desktop_results
    results
    render :layout => 'api'
  end

  def results

    check_list_id = params[:checklist_id]
    if check_list_id
      session[:checklist] = check_list_id
      @check_list = CheckList.find_by_id check_list_id # nil if no such check list
    end

    file_type_id = params[:file_type_id]

    # otherwise, run checks

    @mdl_file = params[:model_file] && params[:model_file].first
    if @mdl_file.nil? || @mdl_file.original_filename == ""
      flash[:error] = "Please select a file to check"
      redirect_to "/umlint/#{@subset}"
    else
      ip = request.remote_ip
      @sub = Submission.create(@mdl_file, @check_list, file_type_id, ip)
      if @sub.run_checks(params[:generate_diagram]).nil?
        @suggestions = nil
      else
        @sub.reload
        @suggestions = @sub.ordered_suggestions
      end
    end
  end

  def checks
    @explanations = Explanation.umlint_explanations.delete_if { |e|
      e.name == 'UNKNOWN' || e.name == 'SYNTAX'
    }
    Explanation.reverse_sort!(@explanations)
  end

  def view
    # rwh, 2-mar-13: added following 4 lines to support changing
    #   the checklist that was viewed - see the first form_tag in view.html.erb
    if params[:checklist_id]
      @checklist_id = params[:checklist_id].to_i # 0 if checklist_id not set
      session[:checklist] = @checklist_id
    else
      @checklist_id = session[:checklist].to_i || 0
    end
    if @checklist_id != 0
      @check_list = CheckList.find_by_id(@checklist_id)
      if @check_list.nil?
        # return it to 0 so just don't get anything displayed; user can then
        #  try to reselect the bad list and will get the error (this shouldn't
        #  happen anyways)
        @checklist_id = 0
      else
        @included_checks = @check_list.inclusion_map
        @items = Explanation.selectable_umlint_explanations
        Explanation.reverse_sort!(@items)
      end
    end
    # note: _view_checklist partial handles case where @checklist_id is 0
  end

  def view_checklist
    @checklist_id = params[:checklist_id].to_i # 0 if checklist_id not set
    @check_list = CheckList.find_by_id(@checklist_id)
    if @checklist_id == 0
      # partial will take care of ensuring nothing is displayed
      flash[:error] = ''
      @check_list = nil
      render :partial => 'view_checklist'
      return
    elsif @check_list.nil?
      flash[:error] = "Internal error: no checklist with id #{@checklist_id}"
      redirect_to "/umlint/view"
      return
    end
    @included_checks = @check_list.inclusion_map
    @items = Explanation.selectable_umlint_explanations
    Explanation.reverse_sort!(@items)
    render :partial => 'view_checklist'
  end

  def edit
    if need_login?              # sets @account
      session[:return_to] = request.fullpath
      redirect_to '/umlint/login'
      return
    end
    # rwh, 2-mar-13: added following 4 lines to support changing
    #   the checklist being edited - see the first form_tag in edit.html.erb
    if params[:checklist_id]
      @checklist_id = params[:checklist_id].to_i # 0 if checklist_id not set
      session[:checklist] = @checklist_id
    else
      @checklist_id = session[:checklist].to_i || 0
    end
    @check_list   = CheckList.find_by_id(@checklist_id)
    if @check_list
      @included_checks = @check_list.inclusion_map
    else
      @checklist_id = 0 # signal can't find checklist
      # (This could be an error situation, but handle it for now by
      # simply creating a dummy list and handle it later if the
      # user is able to reselect the bad number.)
    end
    if @checklist_id == 0
      # set up empty checklist
      @check_list = CheckList.new
      @check_list.instructor  = @account
      @check_list.description = ''
      @check_list.language    = ''
      @check_list.retired     = false
      # note this is not saved at this point
      @included_checks = @check_list.all_checks_enabled
    end
    # ensure have list of explanations
    @items = Explanation.selectable_umlint_explanations
    Explanation.reverse_sort!(@items)
  end

  def show_checklist_info
    if need_login?      # sets @account
      session[:return_to] = request.fullpath
      redirect_to '/umlint/login'
      return
    end
    @checklist_id = params[:checklist_id]
    @check_list = CheckList.find_by_id(@checklist_id)
    if @check_list
      @included_checks = @check_list.inclusion_map
    else
      @check_list = CheckList.new
      @check_list.instructor  = @account
      @check_list.description = ''
      @check_list.language    = ''
      @check_list.retired     = false
      # note this is not saved at this point
      @included_checks = @check_list.all_checks_enabled
    end
    @items = Explanation.selectable_umlint_explanations
    Explanation.reverse_sort!(@items)
    @read_only = @check_list.instructor != @account
    render :partial => 'edit_checklist'
  end

  def show_suggestion_details
    @suggestion_item_id = params[:item]
    @suggestion_item = @suggestion_item_id && Suggestion.find(@suggestion_item_id)
    respond_to do |format|
      format.js
    end
  end

  def do_update_checklist
    if params[:commit] == 'Cancel'
      redirect_to '/umlint'
      return
    end
    if need_login?      # sets @account
      session[:return_to] = request.fullpath
      redirect_to '/umlint/login'
      return
    end
    # sample params: see CheckList.updated_to
    checklist_id = params[:checklist_id]
    if checklist_id.nil? || checklist_id == '0'
      checklist = CheckList.new
      checklist.is_inclusion_list = true
      checklist.instructor = @account
    elsif (checklist = CheckList.find_by_id checklist_id).nil?
      flash[:error] = "Unknown checklist"
      redirect_to '/umlint/edit'
      return
    elsif checklist.instructor != @account
      flash[:error] = "Cannot modify checklist created by another instructor"
      redirect_to '/umlint/edit'
      return
    end

    isNew = checklist.id.nil?

    if checklist.updated_to(params)
      flash[:notice] = isNew ? "Checklist #{checklist.description} created" : "Checklist #{checklist.description} updated"
      redirect_to '/umlint'
    else
      flash[:error] = "Internal error; checklist could not be updated"
      redirect_to '/umlint/edit'
    end
  end

  def manage_password
    if need_login? # sets @account
      redirect_to '/umlint/login'
    else
      render :view => umlint_manage_password_path
    end
  end

end
