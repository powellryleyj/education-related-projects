require 'zip'
require 'csv'

class UmlgraderController < ApplicationController

  protect_from_forgery :except => [:desktop_results]

  def set_base_url
    @base_url = '/umlgrader'
  end

  # will prompt user for problem and file
  # prompts for problem unless it is set
  def index
    problems = Problem.all_open.to_a
    problems.sort! { |a, b| a.name <=> b.name }
    @problem_options = problems.map { |p| [p.display_name, p.id] }
  end

  def desktop_options
    problems = Problem.all_open.to_a
    problems.sort! { |a, b| a.name <=> b.name }
    problem_options = problems.map { |p| [p.display_name, p.id] }
    checklists = CheckList.check_types
    render :json => {:problems => problem_options, :checklists => checklists}
  end

  def desktop_results
    results
    render :layout => 'api'
  end

  def results
    get_problem
    # && params[:model_file].first: Makes EAP File submission blocked test fail
    @mdl_file = params[:model_file] && params[:model_file].first
    if @problem.nil?
      flash[:error] = "Please select a problem to grade"
      redirect_to "/umlgrader"
    elsif @problem.closed?
      flash[:error] = "Sorry, that problem closed at #{@problem.closed_at}."
      redirect_to "/umlgrader"
    elsif @mdl_file.nil? || @mdl_file.original_filename == ""
      flash[:error] = "Please select a file to grade"
      redirect_to "/umlgrader/#{@problem.id}"
    elsif !@mdl_file.original_filename.match '\.(xmi|mdl|XMI|MDL)$'
      flash[:error] = "must have file extension .mdl or .xmi (Attempted to save as #{@mdl_file.original_filename})"
      redirect_to "/umlgrader"
    else
      # Deletes all previous submissions by this account.
      @problem.submissions.each{|submission|
        if(submission.student_id == session[:account_id])
          submission.destroy
        end
      }

      ip = request.remote_ip
      @sub = Submission.create(@mdl_file, @problem, session[:account_id], ip)
      if @sub.run_checks(params[:generate_diagram]).nil?
        @results = nil
      else
        @sub.reload
        @results = @sub.ordered_suggestions
        @account = Account.find_by_id(session[:account_id])
        if @account.student?
          @account.child.submissions << @sub
          @account.save
        end
      end
    end
  end

  def create_instructor
    if !check_for_instructor
      return
    end
    if request.post?
      if params[:change_password] != params[:confirm_password]
        flash[:error] = 'Passwords do not match!'
        redirect_to umlgrader_create_instructor_path
        return
      end
      if Account.find_by(email_address: params[:email]) != nil
        flash[:error] = 'Email already in use!'
        redirect_to umlgrader_create_instructor_path
        return
      end
      instructor = Account.new
      instructor.email_address = params[:email]
      instructor.password_digest = params[:change_password]
      instructor.given_name = params[:first_name]
      instructor.surname = params[:last_name]
      instructor.organization = params[:organization]
      instructor.account_type = Account::INSTRUCTOR
      saved = instructor.save

      if saved
        flash[:notice] = "Instructor '#{params[:email]}' has been created!"
        redirect_to umlgrader_index_path
      else
        # should probably say why, but at least capture the error
        flash[:notice] = "Error: instructor '#{params[:email]}' not created!"
        redirect_to umlgrader_create_instructor_path
      end
    end
  end

  def modify_instructor
    if !check_for_instructor
      return
    end

    @account = Account.find_by_id(session[:account_id])
    if request.post?
      if @account.password_digest != params[:current_password]
        flash[:error] = 'Incorrect current password!'
        redirect_to umlgrader_modify_instructor_path
        return
      end
      if params[:commit] == 'Delete' # Delete button pressed
        destroy_instructor
      elsif params[:commit] == 'Save' # Save button pressed

        if params[:change_password] != params[:confirm_password]
          flash[:error] = 'Passwords do not match!'
          redirect_to umlgrader_modify_instructor_path
          return
        end
        if @account.email_address != params[:email] &&
           Account.where(:email_address => params[:email]).exists?
          flash[:error] = 'This email address is already in use!'
          redirect_to umlgrader_modify_instructor_path
          return
        end

        @account.email_address = params[:email]

        if !params[:change_password].empty?
          @account.password_digest = params[:change_password]
        end

        @account.given_name = params[:first_name]
        @account.surname = params[:last_name]
        @account.organization = params[:organization]

        @account.save
        flash[:notice] = "Instructor '#{params[:email]}' has been modified!"
        redirect_to umlgrader_index_path
      end
    end
  end

  def destroy_instructor
    @account = Account.find_by_id(session[:account_id])

    unless @account.nil?
      @account.destroy
    end
    flash[:notice] = 'Your instructor account has been deleted!'
    logout
  end


  def destroy
    if !check_for_instructor
      return
    end

    @account = Account.find_by_id(session[:account_id])
    @problem = @account.child.problems.find(params[:id])

    unless @problem.nil?
      Problem.find_by_id(params[:id]).destroy
    end

    redirect_to umlgrader_manage_assignments_path
  end

  def show_grader_results
    @grader_item_id = params[:item]
    @grader_item = @grader_item_id && Suggestion.find(@grader_item_id)
    respond_to do |format|
      format.js
    end
  end

  def checks
    @explanations = Explanation.umlgrader_explanations.delete_if { |e|
      e.name == 'UNKNOWN' || e.name == 'SYNTAX'
    }
    Explanation.reverse_sort!(@explanations)
    @sexplanations = Explanation.reverse_sort Explanation.umlgrader_sequence_explanations
  end

  def view
    @checklists = CheckList.all
    if params[:checklist_id]
      @checklist_id = params[:checklist_id].to_i # 0 if checklist_id not set
      session[:checklist] = @checklist_id
    else
      @checklist_id = session[:checklist].to_i || 0
    end
    if @checklist_id != 0
      @check_list = CheckList.find_by_id @checklist_id
      if @check_list.nil?
        # return it to 0 so just don't get anything displayed; user can then
        #  try to reselect the bad list and will get the error (this s Checkshouldn't
        #  happen anyways)
        @checklist_id = 0
      else
        @included_checks = @check_list.inclusion_map
        @lint_items = Explanation.selectable_umlint_explanations
        @grader_items = Explanation.selectable_umlgrader_explanations
        Explanation.reverse_sort! @lint_items
        Explanation.reverse_sort! @grader_items
      end
    end
  end

  def edit
    if !check_for_instructor
      return
    end
    @account = Account.find_by_id(session[:account_id])
    if params[:commit] == 'Submit'
      upload_checklist_file
      return
    end
    # rwh, 2-mar-13: added following 4 lines to support changing
    #   the checklist being edited - see the first form_tag in edit.html.erb
    if params[:checklist_id]
      @checklist_id = params[:checklist_id].to_i # 0 if checklist_id not set
      session[:checklist] = @checklist_id
    else
      @checklist_id = session[:checklist].to_i || 0
    end
    @check_list   = CheckList.find_by_id(@checklist_id)
    if @check_list
      @included_checks = @check_list.inclusion_map
    else
      @checklist_id = 0 # signal can't find checklist
      # (This could be an error situation, but handle it for now by
      # simply creating a dummy list and handle it later if the
      # user is able to reselect the bad number.)
    end
    if @checklist_id == 0
      # set up empty checklist
      @check_list = CheckList.new
      @check_list.instructor  = @account.child
      @check_list.description = ''
      @check_list.language    = ''
      @check_list.retired     = false
      # note this is not saved at this point
      @included_checks = @check_list.all_checks_enabled
    end
    # ensure have list of explanations
    @read_only = (@check_list.instructor != @account.child)
    @lint_items = Explanation.selectable_umlint_explanations
    @grader_items = Explanation.selectable_umlgrader_explanations
    Explanation.reverse_sort!(@lint_items)
    Explanation.reverse_sort! @grader_items
  end

  def show_checklist_info
    if !check_for_instructor
      return
    end
    @account = Account.find_by_id(session[:account_id])
    @checklist_id = params[:checklist_id]
    @check_list = CheckList.find_by_id(@checklist_id)
    if @check_list
      @included_checks = @check_list.inclusion_map
    else
      @check_list = CheckList.new
      @check_list.instructor  = @account.child
      @check_list.description = ''
      @check_list.language    = ''
      @check_list.retired     = false
      # note this is not saved at this point
      @included_checks = @check_list.all_checks_enabled
    end
    @lint_items = Explanation.selectable_umlint_explanations
    @grader_items = Explanation.selectable_umlgrader_explanations
    Explanation.reverse_sort! @lint_items
    Explanation.reverse_sort! @grader_items
    @read_only = @check_list.instructor != @account.child
    render :partial => 'edit_checklist'
  end

  def download_checklist_file
    if !check_for_instructor
      return
    end
    checklist_id = params[:checklist_id]
    check_list = CheckList.find_by_id(checklist_id)
    filepath = check_list.written_json_string
    send_file(
      "#{filepath}",
      filename: "#{check_list.description}.json",
      type: "application/json",
      disposition: "attachment"
    )
  end

  def upload_checklist_file
    if !check_for_instructor
      return
    end
    @account = Account.find_by_id(session[:account_id])
    @checklist_file = params[:checklist_file] && params[:checklist_file].first
    checklist_id = CheckList.create_from_file(@checklist_file, @account.child)
    if(checklist_id.nil?)
      flash[:msg] = "Error uploading checklist!"
    else
      flash[:msg] = "Checklist successfully uploaded!"
    end
    redirect_to "/umlgrader/edit?checklist_id=#{checklist_id}"
  end

  def do_update_checklist
    if params[:commit] == 'Cancel'
      redirect_to umlgrader_index_path
      return
    end
    if !check_for_instructor
      return
    end
    @account = Account.find_by_id(session[:account_id])
    # sample params: see CheckList.updated_to
    checklist_id = params[:checklist_id]
    if checklist_id.nil? || checklist_id == '0'
      checklist = CheckList.new
      checklist.is_inclusion_list = true
      checklist.instructor = @account.child
    elsif (checklist = CheckList.find_by_id checklist_id).nil?
      flash[:error] = 'Unknown checklist'
      redirect_to umlgrader_edit_path
      return
    elsif checklist.instructor != @account.child
      flash[:error] = 'Cannot modify checklist created by another instructor'
      redirect_to umlgrader_edit_path
      return
    end

    isNew = checklist.id.nil?

    if checklist.updated_to(params)
      flash[:notice] = isNew ? "Checklist #{checklist.description} created" : "Checklist #{checklist.description} updated"
      redirect_to umlgrader_index_path
    else
      flash[:error] = 'Internal error; checklist could not be updated'
      redirect_to umlgrader_edit_path
    end
  end

  def logout
    session[:account_id] = nil
    @account = nil
    redirect_to '/umlgrader/index'
  end

  # display problem form
  def create_assignment
    if !check_for_instructor
      return
    end

    # avoid null errors when view references @problem
    if @problem == nil
      @problem = Problem.new
    end
  end

  def modify_assignment
    if !check_for_instructor
      return
    end

    @account = Account.find_by_id(session[:account_id])
    # @problem != nil when re-displaying a form with errors
    if @problem == nil
      if params.has_key?(:id)
        @problem = @account.child.problems.find(params[:id])
      end
    end

    if @problem == nil
      #TODO: error message instead
      @problem = Problem.new
      redirect_to '/umlgrader/create_assignment'
    end
  end

  # receives problem form & stores a Problem in the db
  def upload_assignment
    if !check_for_instructor
      return
    end

    @account = Account.find_by_id(session[:account_id])
    @problem = Problem.create_from_form_data(problem_params, @account.child)
    if @problem.errors && !@problem.errors.empty?
      render "/umlgrader/create_assignment"
    else
      flash[:title] = "Create Assignment"
      flash[:msg] = "Problem successfully uploaded"
      redirect_to "/umlgrader/manage_assignments"
    end
  end

  # recieves problem form & stores a Problem in the db
  def reupload_assignment
    if !check_for_instructor
      return
    end

    @account = Account.find_by_id(session[:account_id])
    if @problem == nil
      if params.has_key?(:id)
        @problem = @account.child.problems.find(params[:id])
      end
    end

    @problem = Problem.find(params[:id])
    @problem.save_form_data(problem_params, @account.child)
    if @problem.errors && !@problem.errors.empty?
      render "/umlgrader/modify_assignment"
    else
      flash[:notice] = "Problem successfully updated"
      redirect_to umlgrader_manage_assignments_path
    end
  end

  def manage_assignments
    if !check_for_instructor
      return
    end

    @account = Account.find_by_id(session[:account_id])
    #instructor = Instructor.where(:id => session[:instructor_id]).take
    @account_problems = Problem.all_for_instructor(@account.id)

    render "/umlgrader/manage_assignments"
  end

  def view_submissions
    if !check_for_instructor
      return
    end

    @account = Account.find_by_id(session[:account_id])
    if @problem == nil
      if params.has_key?(:id)
        @problem = @account.child.problems.find(params[:id])
      end
    end
    @problem_groups = {}
    @problem.submissions.each  { |submission|
      student = Student.find_by_id(submission.student_id)
      student_group = nil
      # could replace this with an active record call, I couldn't get it to work - bensonja 2/11/2019
      student.groups.each do |group|
        @problem.groups.each do |group2|
          if group.id == group2.id
            student_group = group
          end
        end
      end
      if(student && student_group)
        if(@problem_groups[student_group.id].nil?)
          @problem_groups[student_group.id] = []
        end
        @problem_groups[student_group.id] << submission
      end
    }
  end

  def download_submission_file
    if !check_for_instructor
      return
    end

    @submission = Submission.find_by_id(params[:id])
    send_file(@submission.path,
      filename: @submission.orig_name,
      disposition: "attachment")
  end

  # Zips all submissions for one group on a particular problem into one file and sends it for download
  # https://github.com/rubyzip/rubyzip for reference on how to zip files
  def download_group_submissions
    if !check_for_instructor
      return
    end

    submissions = Problem.find_by_id(params[:id]).submissions
    submissions = submissions.select do |submission|
      submission.student.groups.find_by_id(params[:group_id])
    end

    file_name = "#{Group.find_by_id(params[:group_id]).name}-#{Problem.find_by_id(params[:id]).name}.zip"
    directory_name = "" + params[:group_id]
    zipfile_name = File.join(directory_name, file_name)
    FileUtils.mkdir_p directory_name

    csv_filename = File.join(directory_name, "Table Data.csv")
    # See private section for these method definitions
    create_csv_file(csv_filename, submissions)
    create_zip_file(zipfile_name, csv_filename, submissions)

    File.open(zipfile_name, 'r') do |f|
      send_data f.read, type: "application/zip", filename: File.basename(zipfile_name), disposition: "attachment"
    end

    File.delete(zipfile_name)
    File.delete(csv_filename)
    FileUtils.remove_dir(directory_name)
  end

  def download_file
    send_file File.join(File.dirname(__FILE__), '/../../public/UMTutorial.zip')
  end

  def manage_groups
    if !check_for_instructor
      return
    end
    @account = Account.find_by_id(session[:account_id])
    @groups = (@account.child.groups.where(retired: false).to_a.map {|g| [g.name, g.description, g.id]}).sort! {|a,b| a[0] <=> b[0]}
  end

  def manage_group
    if !check_for_instructor
      return
    end
    #creates account and group variables
    @account = Account.find_by_id(session[:account_id])
    @group = nil
    #get existing group if editing. otherwise, create new group
    if params.has_key?(:id)
      if(params[:id] == "new")
        @group = Group.new
        @group.instructor = @account.child
      else
        @group = @account.child.groups.find_by_id(params[:id])
        #if the group is retired or doesnt exist, go back
        if(@group.retired || @group == nil)
          redirect_to "/umlgrader/manage_groups"
          return
        end
      end
    end
    #get problems for problems section
    problems = Problem.all_open.to_a
    problems.sort! { |a, b| a.name <=> b.name }
    @problem_options = problems.map { |p| [p.display_name, p.id] }
  end

  def group_update_descriptions
    if !check_for_instructor
      return
    end
    #get group to compare to, as we dont wan't duplicates
    group_compare = Instructor.find_by_id(session[:account_id]).groups.where(retired: false).find_by(name: params[:name])
    if(params[:name].nil? || params[:name].blank?)
      # Send error to user: need name
      flash[:error] = 'Group needs a name'
      redirect_to "/umlgrader/manage_group/#{params[:id]}"
      return
    elsif(!group_compare.nil? && group_compare.id != params[:id].to_i)
      # Send error to user: A group already has that name
      flash[:error] = 'A group you own already has that name'
      redirect_to "/umlgrader/manage_group/#{params[:id]}"
      return
    elsif(params[:description].nil? || params[:description].blank?)
      # Send error to user: need description
      flash[:error] = 'Group needs a description'
      redirect_to "/umlgrader/manage_group/#{params[:id]}"
      return
    end

    #get group and update fields if exists
    @group = Instructor.find_by_id(session[:account_id]).groups.find_by_id(params[:id])
    # if group doesnt exist, exit
    if(@group == nil)
      redirect_to "/umlgrader/manage_groups/"
    end
    @group.name = params[:name]
    @group.description = params[:description]
    @group.save
    flash[:success] = "Descriptions Updated!"
    redirect_to "/umlgrader/manage_group/#{@group.id}"
  end

  def create_group
    if !check_for_instructor
      return
    end
    if(params[:name].nil? || params[:name].blank?)
      # Send error to user: need name
      flash[:error] = 'Group needs a name'
      redirect_to "/umlgrader/manage_group/new"
      return
    elsif(Group.all.where(instructor_id: session[:account_id]).where(name: params[:name]).exists?)
      # Send error to user: A group already has that name
      flash[:error] = 'A group you own already has that name'
      redirect_to "/umlgrader/manage_group/new"
      return
    elsif(params[:description].nil? || params[:description].blank?)
      # Send error to user: need description
      flash[:error] = 'Group needs a description'
      redirect_to "/umlgrader/manage_group/new"
      return
    end

    #get instructor and assign new group
    @instructor = Instructor.find_by_id(session[:account_id])

    @group = Group.new
    @group.name = params[:name]
    @group.description = params[:description]
    @group.retired = false
    @group.instructor = @instructor
    @group.save
    @instructor.save

    # redirect_to umlgrader_manage_groups_path
    redirect_to "/umlgrader/manage_group/#{@group.id}"
  end

  def group_add_students
    if !check_for_instructor
      return
    end
    #iterate through emails and add them to the group
    @group = Instructor.find_by_id(session[:account_id]).groups.find_by_id(params[:id])
    if(@group == nil)
      redirect_to "/umlgrader/manage_groups/"
    end
    emails = params[:students_import].split(";")
    incorrect_emails = "Invalid Emails: "
    already_added = "Emails Already Added: "
    emails.each {|email|
      student = Student.find_by(email_address: email)
      if(!student.nil?)
        if(@group.students.include?(student))
          already_added += email + " "
        else
          @group.students << student
          flash[:success] = "Students Added!"
        end
      else
        incorrect_emails += email + " "
      end
    }

    if(incorrect_emails != "Invalid Emails: ")
      flash[:error] = incorrect_emails
    end
    if(already_added != "Emails Already Added: ")
      flash[:notice] = already_added
    end

    redirect_to "/umlgrader/manage_group/#{@group.id}"
  end

  def group_add_problem
    if !check_for_instructor
      return
    end
    #get problem and add it to group
    get_problem
    @group = Instructor.find_by_id(session[:account_id]).groups.find_by_id(params[:id])
    if(@group == nil)
      redirect_to "/umlgrader/manage_groups/"
    end
    if(@group.problems.include?(@problem))
      flash[:notice] = "Problem Already Added: #{@problem.name}"
    else
      @group.problems << @problem
      flash[:success] = "Problem Added!"
    end
    redirect_to "/umlgrader/manage_group/#{@group.id}"
  end

  def delete_group
    if !check_for_instructor
      return
    end
    @group = Group.find_by_id(params[:id])
    if(@group == nil)
      flash[:error] = "Group does not exist"
      return
    else
      @group.retired = true
      @group.save
    end
    redirect_to "/umlgrader/manage_groups"
  end

  def delete_group_student
    if !check_for_instructor
      return
    end
    student = Student.find_by_id(params[:student])
    group = Group.find_by_id(params[:group])
    group.students.delete(student)
    redirect_to "/umlgrader/manage_group/#{group.id}"
  end

  def delete_group_problem
    if need_instructor_login?
      session[:return_to] = request.fullpath
      redirect_to umlint_login_path
      return
    end
    problem = Problem.find_by_id(params[:problem])
    group = Group.find_by_id(params[:group])
    group.problems.delete(problem)
    redirect_to "/umlgrader/manage_group/#{group.id}"
  end

  private

  def create_csv_file (csv_filename, submissions)
    CSV.open(csv_filename, "wb") do |csv|
      csv << ["Student Email", "Filename", "Feedback", "Time Submitted"]
      submissions.each do |submission|
        email = submission.student.email_address
        filename = submission.orig_name
        suggestions = ""
        if(!submission.suggestions.empty?)
          submission.suggestions.each do |suggestion|
            suggestions << suggestion.message
            suggestions << "\n"
          end
        else
          suggestions = "No errors found."
        end
        time = submission.time
        csv << [email, filename, suggestions, time]
      end
    end
  end

  def create_zip_file(zipfile_name, csv_filename, submissions)
    Zip::File.open(zipfile_name, Zip::File::CREATE) do |zipfile|
      submissions.each {|submission|
        if(submission.student_id)
          student_email = Student.find_by_id(submission.student_id).email_address
          zipfile.add(File.join(student_email, File.basename(submission.path)), submission.path)
        end
      }
      zipfile.add(File.basename(csv_filename), csv_filename)
    end
  end

  def check_for_instructor
    if need_instructor_login?
      session[:return_to] = request.fullpath
      redirect_to '/umlint/login'
      return false
    end
    true
  end

  def get_problem
    if params[:assignment]
      session[:assignment] = params[:assignment]
    end
    id = session[:assignment].to_i
    # get problem object, or nil if doesn't exist
    @problem = Problem.find_by_id id
  end

  def problem_params
    params.permit(:name, :description, :started_at, :closed_at, :due_at, :solution_file,
                  :assignment_type, :check_list, :nouns, :verbs, :sub_type)
  end
end
