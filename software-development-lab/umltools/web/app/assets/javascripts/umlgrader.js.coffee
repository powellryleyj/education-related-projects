jQuery ->
  $ ->
    if $('#assignment_type').val() == '2'
      $('#solution_file_type').addClass('hide')
      $('#content_editor').removeClass('hide')
      $('#sub_type').removeClass('hide')
      $('#check_list_row').hide();

  $('#assignment_type').change ->
    if ($(this).val() == '2')
      $('#solution_file_type').addClass('hide')
      $('#content_editor').removeClass('hide')
      $('#sub_type').removeClass('hide')
      $('#check_list_row').hide();
    else
      $('#solution_file_type').removeClass('hide')
      $('#content_editor').addClass('hide')
      $('#sub_type').addClass('hide')
      $('#check_list_row').show();