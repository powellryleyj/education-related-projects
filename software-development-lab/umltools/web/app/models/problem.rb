class Problem < ApplicationRecord
  # fields :author, :closed_at, :description, :due_at, :name, :organization
  # fields :posted_at, :solution_file, :started_at, :instructor_id
  # fields :assignment_type, :check_list, :serialized_model

  has_many :submissions
  has_one :problem_word
  belongs_to :instructor
  has_and_belongs_to_many :groups

  # Assignment types
  CLASS_DIAGRAM_TYPE = 0
  SEQUENCE_DIAGRAM_TYPE = 1
  HIGH_LEVEL_DIAGRAM_TYPE = 2
  DIAGRAM_TYPE_NAME = {
    CLASS_DIAGRAM_TYPE => 'Class',
    SEQUENCE_DIAGRAM_TYPE => 'Sequence',
    HIGH_LEVEL_DIAGRAM_TYPE => 'High-Level'
  }.freeze

  SECOND = 1 # (1/27/19) powellr - useless assignment but is included for consistency
  MINUTE = (SECOND * 60).freeze
  HOUR = (MINUTE * 60).freeze
  DAY = (HOUR * 24).freeze

  validates :name, uniqueness: true, on: :create

  validates :description, presence: true

  validates :assignment_type, presence: true, numericality: {
    only_integer: true, greater_than_or_equal_to: CLASS_DIAGRAM_TYPE, less_than: 3
  }

  validates :solution_file,
            # rwh, March 2016: changed != to == in following (so check does
            #     not apply for high level diagrams)
            # unless: ->(p) { p.assignment_type == HIGH_LEVEL_DIAGRAM_TYPE },
            presence: true, format: { with: /[^\n]+(\.(xmi|mdl|XMI|MDL))/,
                                      message: "must have file extension .mdl or .xmi (Attempted to save as '%{value}')" }

  validates :started_at, presence: true

  validates :due_at, presence: true

  validates :closed_at, presence: true

  # (1/27/19) powellr - note that validation checks down to the second between time-stamps
  # but only the date is used for the website. Future development should include the usage
  # of exact times for more precise control over due/closed times.
  validate :validate_start_due_close_time

  def validate_start_due_close_time
    unless started_at.nil? || due_at.nil? || closed_at.nil?
      errors.add(:due_at, 'must be on or after the start date.') if started_at.to_date > due_at.to_date
      errors.add(:closed_at, ' must be on or after the due date.') if due_at.to_date > closed_at.to_date
    end
  end

  # UML-297: Returns a list of problems matching the specified type
  # (1/27/19) powellr - only use defined enumerations; exception is raised to dissuade 'magic numbers'
  def self.this_type(assignment_type)
    raise ArgumentError, 'assignment_type only have a value on the interval of [0,2]' if assignment_type < 0 || assignment_type >= 3

    Problem.where(assignment_type: assignment_type)
  end

  def self.all_for_instructor(instructor_id)
    Problem.where(instructor_id: instructor_id)
  end

  # returns list of open problems ordered alphabetically
  # TODO: This does not sort alphabetically despite the comment above
  def self.all_open(current_time = Time.now)
    Problem.all.to_a.keep_if do |p|
      p.open?(current_time)
    end
  end

  def open?(current_time = Time.now)
    started_at <= current_time && current_time <= closed_at
  end

  def closed?(current_time = Time.now)
    !open?(current_time)
  end

  def path
    File.join(Rails.configuration.solution_dir, Instructor.find_by_id(instructor_id).email_address, solution_file)
  end

  def nouns
    problem_word.noun_list if problem_word
  end

  def verbs
    problem_word.verb_list if problem_word
  end

  # no null check for instructor
  def self.create_from_form_data(form, instructor)
    problem = Problem.new

    problem.save_form_data(form, instructor)

    problem
  end

  # inserts data from form into problem, if the data is provided
  def save_form_data(form, instructor)
    solution_file = form[:solution_file]

    # TODO: Error checking with ActiveRecord.errors
    self.name = form[:name]
    self.description = form[:description]
    self.description = 'This assignment has no description.' if form[:description].blank?
    self.assignment_type = form[:assignment_type]
    date_format = /\d\d\/\d\d\/\d\d\d\d/
    self.started_at = Time.strptime(form[:started_at], '%m/%d/%Y') if date_format.match form[:started_at]
    self.closed_at = Time.strptime(form[:closed_at], '%m/%d/%Y') if date_format.match form[:closed_at]
    self.due_at = Time.strptime(form[:due_at], '%m/%d/%Y') if date_format.match form[:due_at]

    # name the solution file after the problem name, as problems should have unique names?
    self.solution_file = Problem.filename(solution_file.original_filename, name) if solution_file
    self.instructor_id = instructor.id
    self.posted_at = self.created_at = self.updated_at = Time.now
    self.check_list = form[:check_list]

    if solution_file
      # write the file
      logger.debug ">>>> Rails.configuration = #{Rails.configuration}"
      logger.debug ">>>> Rails.configuration.solution_dir = #{Rails.configuration.solution_dir}"
      FileUtils.mkdir_p File.dirname(path)
      File.open(path, 'wb') { |f| f.write(solution_file.read) }
      self.serialized_model = YAML.dump UmlTools.load_diagram(path, assignment_type)
    end

    save

    if form[:assignment_type].to_i == HIGH_LEVEL_DIAGRAM_TYPE &&
       !(errors && !errors.empty?)
      nouns = form[:nouns].delete("\r").split("\n").join '|'
      verbs = form[:verbs].delete("\r").split("\n").join '|'
      if problem_word
        problem_word.nouns = nouns
        problem_word.verbs = verbs
        problem_word.assignment_type = form[:sub_type].to_i
        problem_word.save
      else
        ProblemWord.create problem_id: id, nouns: nouns, verbs: verbs, assignment_type: form[:sub_type].to_i
      end
    end
    self
  end

  def display_name
    name + ' (' + Problem::DIAGRAM_TYPE_NAME[assignment_type.to_i] + ' Diagram)'
  end

  # (1/27/19) powellr - I'm not really sure what the point of this method is.
  # nothing gets updated.
  def self.filename(original_filename, chosen_name)
    extension = File.extname(original_filename)
    chosen_name.gsub(/[^\w\-]/, '_').squeeze('_') + extension
  end

  def no_error_submissions
    submissions.to_a.select { |x| x.errors.empty? }
  end

  def no_error_tickets
    no_error_submissions.map(&:ticket).sort
  end

# (1/19/19) powellr - everything below this line has been disabled as
# default instantiation has been moved to seeds.rb

    # protected

  # returns instructor for with id 0 (or creates one for hasker if none exists)
#   def self.default_instructor
#     id = Instructor.all.map(&:id).min
#     if id
#       i = Instructor.find(id)
#     else
#       a = Account.new(given_name: 'Rob',
#                       surname: 'Hasker',
#                       organization: 'MSOE',
#                       email_address: 'hasker@msoe.edu')
#       a.save
#       a.set_account_type 1
#       a.change_password 'change_me'
#       i = Instructor.new
#       a.set_subtype i
#       i.save
#       a.reload
#     end
#     i
#   end
#
#   public
#
#   # setup lab examples discussed in papers
#   def self.setup_lab_examples_used_in_papers
#     # plow example - this one actually wasn't a lab
#     descr = <<HERE
# Draw a class diagram modeling a system for snow plows:
#
# * Snow removal drivers have routes and are assigned trucks with which they clear those routes.  Each route is the responsibility of one driver.
# * Trucks have numbers. There are two types of trucks: dump trucks and snow plows. For dump trucks, there is a name of the type of equipment attached.
# * A route is a sequence of street segments to traverse.
# * A street has a name is made up of a sequence of segments, where each segment is defined by the crossing streets that start and end that segment. If the street is a dead-end, the crossing street is NULL.
# * Show classes, associations, multiplicities, attributes, operations, and generalizations.
# HERE
#     p = Problem.where(name: 'snow plow routes')
#                .first_or_create(description: descr,
#                                 solution_file: 'snow-plow-soln.mdl',
#                                 author: default_instructor.best_name,
#                                 organization: default_instructor.organization,
#                                 instructor_id: default_instructor.id,
#                                 posted_at: Time.now.utc,
#                                 started_at: Time.now.utc,
#                                 due_at: 'Dec 31 23:59:59 -0500 2030',
#                                 closed_at: 'Dec 31 23:59:59 -0500 2030',
#                                 assignment_type: CLASS_DIAGRAM_TYPE)
#     p.save
#
#     descr = <<HERE
# Model an electronic book reader which displays pages of books and collections.
# Books have chapters, collections have articles.
#
# * An electronic book reader has a collection of documents, and each document (from the publisher’s point of view) is associated with multiple book readers. Book readers have names.
# * Documents are either books or collections.
# * Books have 1 or more chapters. A book has a title and an author, both strings. A chapter has a number, a title, and the text comprising that chapter. The title and text are both strings.
# * Collections have 1 or more articles, where each article has a title, an author, and the text for that article. A collection also has an editor.
# * The reader has a display object which shows the contents of one “page”. Since the user can control the size of the font on the display, a page can have varying amounts of information on it.
# * The display has a document currently being viewed. Again from the publisher’s point of view, each document is being viewed on zero or more displays.
# * Each display is associated with pages being viewed. Each page object contains formatted text. For efficiency, the display maintains three pages: the current page, the previous page, and the next page. In the case of the current page being the first page of the document, the formatted text entry for the previous page will be empty. Likewise for the last page, where the next formatted text will be empty.
# HERE
#     p = Problem.where(name: 'book reader')
#                .first_or_create(description: descr,
#                                 solution_file: 'book-reader-soln.mdl',
#                                 author: default_instructor.best_name,
#                                 organization: default_instructor.organization,
#                                 instructor_id: default_instructor.id,
#                                 posted_at: Time.now.utc,
#                                 started_at: Time.now.utc,
#                                 due_at: 'Dec 31 23:59:59 -0500 2030',
#                                 closed_at: 'Dec 31 23:59:59 -0500 2030',
#                                 assignment_type: CLASS_DIAGRAM_TYPE)
#   end
#
#   # setup exam exercises discussed in paper
#   def self.exam_examples_used_in_papers
#     # actually, the plow example should be moved to here
#
#     # pet office
#     descr = <<HERE
# Model a veterinarian office for pets:
#
# In a family pet office, each veterinarian handles two kinds of pets: dogs and cats. Each dog or cat has a name, a height and a weight. Dogs have a breed. For each pet there is a single owner, but one owner can have multiple pets. Owners have names and phone numbers. At any one time, certain pets have appointments, so some pets have no appointment, but others have a single appointment. An appointment is for a specific date and time. Each veterinarian also has multiple assistants who have names and identification numbers, and one assistant is assigned to each appointment with a second assistant assigned as backup if needed. Assistants work for just one veterinarian. Draw a class diagram capturing attributes, operations, associations, multiplicities, and generalization.
# HERE
#     p = Problem.where(name: 'family pet office')
#                .first_or_create(description: descr,
#                                 solution_file: 'pet-office-soln.mdl',
#                                 author: default_instructor.best_name,
#                                 organization: default_instructor.organization,
#                                 instructor_id: default_instructor.id,
#                                 posted_at: Time.now.utc,
#                                 started_at: Time.now.utc,
#                                 due_at: 'Dec 31 23:59:59 -0500 2030',
#                                 closed_at: 'Dec 31 23:59:59 -0500 2030',
#                                 assignment_type: CLASS_DIAGRAM_TYPE)
#     p.save
#
#     # course registration system
#     descr = <<HERE
# Model a course registration system:
#
# 1. Each user has a unique username and password. A user also has his/her full name and email stored in the system.
# 2. The user should log in the system before using it, and log out after using it.
# 3. There are two types of users: instructors and students.
# 4. Each instructor has a department. Instructors offer courses.
# 5. Each student has an ID and major. A student can sign up and drop courses.
# 6. A course has its title, course ID and prerequisites. A course may have multiple sections. Each section is assigned a room and capacity.
# 7. Multiple instructors can teach the same course.
# 8. There must be at least one student taking a course.
# HERE
#     p = Problem.where(name: 'course registration system')
#                .first_or_create(description: descr,
#                                 solution_file: 'course-registration-soln.mdl',
#                                 # actually, Yan Shi at UW-Platteville was the author
#                                 author: default_instructor.best_name,
#                                 organization: default_instructor.organization,
#                                 instructor_id: default_instructor.id,
#                                 posted_at: Time.now.utc,
#                                 started_at: Time.now.utc,
#                                 due_at: 'Dec 31 23:59:59 -0500 2030',
#                                 closed_at: 'Dec 31 23:59:59 -0500 2030',
#                                 assignment_type: CLASS_DIAGRAM_TYPE)
#     p.save
#
#     descr = <<HERE
# Draw a diagram for a property management system:
#
# 1. A property has a specific location and the date when it is added to the system. A location is composed of street address, city, state, and zip code.
# 2. There is one owner per each property. The system should keep a record of the owner’s name, phone number and email address. An owner should have at least one property.
# 3. There are two types of properties: for rent and for sale. A rental property should list the monthly rent and whether it is vacant. A property for sale should list its sale price and property tax.
# 4. Each agent has a unique ID in the system. An agent can list multiple properties for sale. She will process the closing if a sale is made. An agent may also manage multiple rental properties. She should collect rents every month.
# HERE
#     p = Problem.where(name: 'property management system')
#                .first_or_create(description: descr,
#                                 solution_file: 'property-management-soln.mdl',
#                                 # actually, Yan Shi at UW-Platteville was the author
#                                 author: default_instructor.best_name,
#                                 organization: default_instructor.organization,
#                                 instructor_id: default_instructor.id,
#                                 posted_at: Time.now.utc,
#                                 started_at: Time.now.utc,
#                                 due_at: 'Dec 31 23:59:59 -0500 2030',
#                                 closed_at: 'Dec 31 23:59:59 -0500 2030',
#                                 assignment_type: CLASS_DIAGRAM_TYPE)
#     p.save
#   end
#
#   # like the plow example with a due date 24 hours into the future
#   def self.setup_test
#     descr = <<HERE
# Snow removal drivers have routes and are assigned trucks with which
#   they clear those routes.  Each route is
#   the responsibility of one driver.
#  Trucks have numbers. There are two types of trucks: dump trucks and
#   snow plows. For dump trucks, there is a name of the type of equipment
#   attached.
#  A route is a sequence of street segments to traverse.
#  A street has a name is made up of a sequence of segments, where each
#   segment is defined by the crossing streets that start and end that
#   segment. If the street is a dead-end, the crossing street is NULL.
# Show classes, associations, multiplicities, attributes, operations, and
# generalizations.
# HERE
#
#     p = Problem.where(name: '24hour snow plow routes')
#                .first_or_create(description: descr,
#                                 solution_file: 'plow_example.mdl',
#                                 author: default_instructor.best_name,
#                                 organization: default_instructor.organization,
#                                 instructor_id: default_instructor.id,
#                                 posted_at: Time.now.utc,
#                                 started_at: Time.now.utc,
#                                 due_at: Time.now + (10 * 3600 * 24),
#                                 closed_at: Time.now.utc + (10 * 3600 * 24),
#                                 assignment_type: CLASS_DIAGRAM_TYPE)
#     p.save
#   end
#
#   def self.setup_car_lab3
#     descr = <<HERE
# A car with doors, an engine, and a transmission. The transmission can
# either be automatic or manual.
# HERE
#     Problem.create(name: 'car',
#                    description: descr,
#                    solution_file: 'car-soln.mdl',
#                    author: default_instructor.best_name,
#                    # actually, done while at UWP
#                    organization: default_instructor.organization,
#                    instructor_id: default_instructor.id,
#                    posted_at: Time.now.utc,
#                    started_at: Time.now.utc,
#                    due_at: 'Dec 31 23:59:59 -0500 2030',
#                    closed_at: 'Dec 31 23:59:59 -0500 2030',
#                    # for actual assignment:
#                    #      :due_at => 'Mar 28 23:59:59 -0500 2011',
#                    #      :closed_at => 'Mar 28 23:59:59 -0500 2011',
#                    assignment_type: CLASS_DIAGRAM_TYPE)
#
#     descr = <<HERE
# An electronic book reader which displays pages of books and collections.
# Books have chapters, collections have articles.
# HERE
#     Problem.create(name: 'book reader',
#                    description: descr,
#                    solution_file: 'bookreader-soln.mdl',
#                    author: default_instructor.best_name,
#                    # actually, done while at UWP
#                    organization: default_instructor.organization,
#                    instructor_id: default_instructor.id,
#                    posted_at: Time.now.utc,
#                    started_at: Time.now.utc,
#                    due_at: Time.now + 365 * 24 * 3600,
#                    closed_at: Time.now + 365 * 24 * 3600,
#                    # for actual assignment:
#                    #      :due_at => 'Mar 28 23:59:59 -0500 2011',
#                    #      :closed_at => 'Mar 28 23:59:59 -0500 2011',
#                    assignment_type: CLASS_DIAGRAM_TYPE)
#   end
#
#   def self.setup_as4
#     descr = <<HERE
#     * A building has a name and zero or more rooms. Each room has a number (such as "206") and a capacity (in seats).
#     * A program has a name (such as "SOFTWARE") and classes, where each class has a catalog number (such as "3330"), an enrollment (number of students taking the class), and a number of sections.
#     * A schedule contains time slots, where each time slot has a day, a start time, and an end time. Each room has a unique schedule.
#     * A time slot may be associated with a class, or the room may be empty at that time. Since classes have multiple sections and occur multiple times during the week, a class can be associated with an arbitrary number of time slots.
#     * In addition to standard projection equipment, whiteboards, and seats, a room provides specialized equipment. Represent equipment as a class with a single attribute called type, and the association from room to equipment has a role name of "provides".
#     * A lab is a type of class with special needs. Associate lab with equipment that is needed and name the items "needs".
# HERE
#     Problem.create(name: 'classrooms',
#                    description: descr,
#                    solution_file: 'as4-classrooms.mdl',
#                    author: default_instructor.best_name,
#                    # actually, done while at UWP
#                    organization: default_instructor.organization,
#                    instructor_id: default_instructor.id,
#                    posted_at: Time.now.utc,
#                    started_at: Time.now.utc,
#                    due_at: 'Mar 28 23:59:59 -0500 2011',
#                    closed_at: 'Mar 28 23:59:59 -0500 2011',
#                    assignment_type: CLASS_DIAGRAM_TYPE)
#   end
#
#   def self.setup_273
#     descr = <<HERE
# A car with doors, an engine, and a transmission. The transmission can
# either be automatic or manual.
# HERE
#     Problem.create(name: 'Cars',
#                    description: descr,
#                    solution_file: '273-car-sample.mdl',
#                    author: default_instructor.best_name,
#                    # actually, done while at UWP
#                    organization: default_instructor.organization,
#                    instructor_id: default_instructor.id,
#                    posted_at: Time.now.utc,
#                    started_at: Time.now.utc,
#                    due_at: 'Dec 31 23:59:59 -0500 2020',
#                    closed_at: 'Dec 31 23:59:59 -0500 2020',
#                    assignment_type: CLASS_DIAGRAM_TYPE)
#
#     descr = <<HERE
# An electronic book reader which supports books and collections.
# Books have chapters, collections have articles.
# HERE
#     Problem.create(name: 'simple book reader',
#                    description: descr,
#                    solution_file: '273-bookreader-sample.mdl',
#                    author: default_instructor.best_name,
#                    # actually, done while at UWP
#                    organization: default_instructor.organization,
#                    instructor_id: default_instructor.id,
#                    posted_at: Time.now.utc,
#                    started_at: Time.now.utc,
#                    due_at: 'Dec 31 23:59:59 -0500 2020',
#                    closed_at: 'Dec 31 23:59:59 -0500 2020',
#                    assignment_type: CLASS_DIAGRAM_TYPE)
#     descr = <<HERE
# A library example with books, patrons, and fines.
# HERE
#     Problem.create(name: 'library/273',
#                    description: descr,
#                    solution_file: '273-library.mdl',
#                    author: default_instructor.best_name,
#                    # actually, done while at UWP
#                    organization: default_instructor.organization,
#                    instructor_id: default_instructor.id,
#                    posted_at: Time.now.utc,
#                    started_at: Time.now.utc,
#                    due_at: 'Apr  6 23:59:59 -0500 2020',
#                    closed_at: 'Apr 10 23:59:59 -0500 2020',
#                    assignment_type: CLASS_DIAGRAM_TYPE)
#     Problem.create(name: 'library/273_s13',
#                    description: 'A library with items, patrons, fines, and a catalog.',
#                    solution_file: '273-library.xmi',
#                    # actually Yan Shi at UWP
#                    author: default_instructor.best_name,
#                    organization: default_instructor.organization,
#                    instructor_id: default_instructor.id,
#                    posted_at: Time.now.utc,
#                    started_at: Time.now.utc,
#                    due_at: 'Apr  13 23:59:59 -0500 2020',
#                    closed_at: 'Apr 15 23:59:59 -0500 2020',
#                    assignment_type: CLASS_DIAGRAM_TYPE)
#
#     Problem.create(name: 'uwrv/273',
#                    description: 'Underwater remote vehicle example',
#                    solution_file: '273-uwrv.mdl',
#                    author: default_instructor.best_name,
#                    # actually at UWP
#                    organization: default_instructor.organization,
#                    instructor_id: default_instructor.id,
#                    posted_at: Time.now.utc,
#                    started_at: Time.now.utc,
#                    due_at: 'Apr  6 23:59:59 -0500 2020',
#                    closed_at: 'Apr 10 23:59:59 -0500 2020',
#                    assignment_type: CLASS_DIAGRAM_TYPE)
#   end
end
