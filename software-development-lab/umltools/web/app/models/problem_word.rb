class ProblemWord < ApplicationRecord
  # fields :nouns, :verbs, :problem_id, :assignment_type
  belongs_to :problem

  def noun_list
    nouns.split('|').join("\n")
  end

  def verb_list
    verbs.split('|').join("\n")
  end
end
