require 'yaml'
class CheckItem < ApplicationRecord
  # fields :category, :explanation_id, :issue

  has_and_belongs_to_many :check_lists
  belongs_to :explanation

  # return list of symbols for detailed items
  def detailed_items
    return @detailed_symbol_list unless @detailed_symbol_list.nil?

    @detailed_symbol_list = if (explanation_item = Explanation.where(name: category).take).nil?
                              []
                            else
                              explanation_item.detailed_issue_list.map(&:to_sym)
                            end
  end

  # return CheckItem with given category, issue if it exists; create new owise
  #   issue should be a string
  def self.get(explanation, issue)
    ci = CheckItem.where(category: explanation.name,
                         issue: issue).take
    if ci.nil?
      ci = CheckItem.create(category: explanation.name,
                            issue: issue,
                            explanation_id: explanation.id)
    end
    ci
  end

  # do same as get but works on a list of issues
  # issues_to_include: list of string
  def self.get_list(explanation, issues_to_include)
    result = []
    explanation.detailed_issue_list.each do |issue_name|
      result << get(explanation, issue_name) if issues_to_include.include? issue_name
    end
    result
  end

  # return list of subissues that are NOT mentioned in issues_to_exclude
  def self.get_list_complement(explanation, issues_to_exclude)
    result = []
    explanation.detailed_issue_list.each do |issue_name|
      result << get(explanation, issue_name) unless issues_to_exclude.include? issue_name
    end
    result
  end
end
