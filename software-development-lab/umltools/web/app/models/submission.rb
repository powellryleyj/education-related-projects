require 'fileutils'
class Submission < ApplicationRecord
  # fields :check_id, :from_ip, :orig_name, :problem_id, :save_name, :ticket, :time, :type, :file_type_id, :generate_diagram

  has_and_belongs_to_many :suggestions
  belongs_to :problem
  belongs_to :student
  belongs_to :check_list


  # relative to cwd when run script/server
  # (2/2/19) powellr - TODO: Address this since there is no reason is should be exposed publicly.
  # structure should be 'web/subs/group_name/problem_name/student_email/save_name'
  def path
    base_dir = Rails.configuration.submission_dir # should be 'web/subs'
    group_name = student_group_name # see function definition
    problem_name = problem.name.to_s # name of the problem being submitted to
    student_email = student.email_address.to_s # email of the student submitting
    File.join(base_dir, group_name, problem_name, student_email, save_name)
  end

  def image_file
    Submission.imageFileFromModelFile path
  end

  def self.imageFileFromModelFile(filename)
    model_file = filename
    pathEnd = model_file.rindex '/'
    newPath = 'public/' + model_file.slice(pathEnd + 1, model_file.length)
    fnameEnd = newPath.rindex '.'
    newPath.slice(0, fnameEnd) + '.png'
  end

  def self.webImageFileFromModelFile(filename)
    sysPath = Submission.imageFileFromModelFile filename
    cutIndex = sysPath.index '/'
    sysPath.slice cutIndex, sysPath.length
  end

  def self.checks_directory
    Rails.configuration.check_dir
  end

  # create a submission: CGI file object -> Submission
  # def self.create(submission_file, check_list, file_type, remote_ip_addr)
  def self.create(submission_file, problem, account_id, remote_ip_addr)
    sub = Submission.new
    sub.time = Time.now
    sub.file_type_id = problem.assignment_type
    sub.problem = problem
    # sub.check_id = check_list ? check_list.id : 0
    sub.check_id = problem.check_list
    sub.from_ip = remote_ip_addr
    sub.orig_name, sub.save_name =
        submission_file_name(submission_file.original_filename,
                             sub.time)
    sub.generate_diagram = false
    sub.save
    sub = Submission.find sub.id
    account = Account.find_by_id(account_id)
    if account && account.student?
      account.child.submissions << sub
      account.save
      # write the file
      #logger.puts("Writing file to #{sub.path}")
      make_save_file(sub, submission_file)
    end

    return sub
  end

  def run_checks(generate_diagram)
    options = []
    if self.file_type_id == Problem::SEQUENCE_DIAGRAM_TYPE
      options << '-S'
    end

    cl = CheckList.find_by_id(check_id)
    if cl
      check_file = cl.written_umlint_yaml
      options << '-c'
      options << "#{check_file}"
    end
    if (generate_diagram)
       options << '-G'
       self.generate_diagram = true
    end
    options << '-i'
    options << self.path

    old_stdout = $stdout
    $stdout = (check_output = StringIO.new)
    UmlTools.check_main(options)
    messages = check_output.string.chomp
    $stdout = old_stdout

    self.suggestions = Suggestion.setup_suggestions(messages.split("\n"))
    self.save
    self
  end

  def ordered_suggestions
    ws = suggestions
    ws.sort
  end

  protected

  # (2/2/19) powellr - the use of tickets have been deprecated
  # def self.gen_ticket(time)
  #   t = time.strftime('%H%M').to_i
  #   r = rand(10_000)
  #   t * 10_000 + r
  # end

  # strips off any preceding directory names
  # replaces anything that is not alphanumeric, underscore, period, hyphen, or '@' with an underscore
  # converts leading character to downcase equivalent
  # will raise NameError if the extension is anything but '.xmi' or '.mdl' (case insensitive)
  # returns sanitized string
  def self.sanitize_filename(original)
    sanitized = File.basename(original) # strips off any preceding directory names
    # following line performs a secondary check for extensions that are not .xmi or .mdl
    raise NameError, "#{sanitized} does not have an accepted extension." unless File.extname(sanitized) =~ /\.xmi|\.mdl/i

    sanitized.gsub!(/[^\w.\-@]/, '_') # replace anything that matches to an underscore
    sanitized[0] = sanitized[0].downcase # replace leading character with downcase variant
    sanitized
  end

  # inserts the date of this submission after the basename and before the extension
  # (2/2/19) powellr - TODO: Does a submission really need a date? kept only for sake of convention
  def self.convert_to_save_name(original, time)
    base_name = File.basename(original, '.*') # strips off extension
    extension = File.extname(original)
    date = time.to_date.to_s # converts time to date in the format 'YYYY-MM-DD'
    base_name + date + extension # returns the concatenation
  end

  # given submission file name and time stamp, returns original name and save name
  # will raise a NameError if there is no extension to the file
  # (2/2/19) powellr - removed 'self' as it does not make sense for the function usage
  def self.submission_file_name(original, time)
    sanitized = sanitize_filename original
    save_name = convert_to_save_name(sanitized, time)
    [original, save_name]
  end

  # assumes a group containing a particular student is only assigned to a particular problem once
  def student_group_name
    # find the group where group.problem_id == problem.id and group.student_id == student.id
    Group.where(problem_id: problem.id, student_id: student.id).name
  end

  def self.make_save_file(sub, file)
    FileUtils.mkdir_p File.dirname(sub.path)
    File.open(sub.path, "wb") { |f| f.write(file.read) }
  end

  # (2/2/19) powellr - the following has been rewritten
  # given submission file name and time stamp, returns original name and save name
  # type: string X Time X Ticket -> string X string
  # def self.submission_file_name(original_filename, time = Date.now)
  # # note basename call required for IE; Firefox returns just the
  # #      basename already
  # orig_name = File.basename(original_filename)
  # extension = File.extname(orig_name).gsub(/[^\w\-]/, '_').squeeze('_')
  # # above line seems to rewrite the extension's period
  # extension[0] = '.' if extension != ''
  # first_part = orig_name[0, orig_name.length - extension.length]
  # first_part = first_part.gsub(/[^\w\-]/, '_').squeeze('_')
  #
  # time_str = time.strftime('.%Y%m%d')
  # save_name = first_part + time_str + '.' + ticket.to_s + extension
  #
  # [orig_name, save_name]

  # end
end
