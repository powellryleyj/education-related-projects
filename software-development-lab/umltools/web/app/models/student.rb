class Student < Account

  has_and_belongs_to_many :groups
  has_many :submissions

  def parent
    self.becomes(Account)
  end
end
