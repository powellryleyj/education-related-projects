class Instructor < Account
  has_many :problems
  belongs_to :checklist
  has_many :groups

  def parent
    self.becomes(Account)
  end
end
