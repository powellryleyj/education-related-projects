require 'digest/sha1'
require_relative 'application_record'
#
# Note: create instructors through umlgrader/create_instructor.
#       Must be logged in as an existing instructor to create an instructor. To
#       create the first instructor call 'rake instructor:create'

class Account < ApplicationRecord
  STUDENT = 0
  INSTRUCTOR = 1

  # fields :email_address, :given_name, :organization, :password_digest, :account_type, :surname, :id
  has_secure_password

  def best_name
    g = given_name || ''
    s = surname    || ''
    if g.empty? || s.empty?
      g.empty? ? s : g
    elsif g.empty? && s.empty?
      e = email_address || ''
      e.empty? ? 'unknown' : e
    else
      g + ' ' + s
    end
  end

  def change_password(new_pass)
    # bad: password = newpass
    update(password_digest: new_pass)
  end

  def password?
    password_digest.exists?
  end

  def authenticate(expected)
    password_digest == expected
  end

  def instructor?
    account_type == INSTRUCTOR
  end

  def student?
    account_type == STUDENT
  end

  # (1/19/19) powellr - disables as default instantiation has been moved to seeds.rb
  # # generally don't use
  # def self.install_some_instructors
  #   hasker = Account.where(:email_address => 'hasker@msoe.edu').
  #     first_or_create(:given_name => 'Rob',
  #                         :surname => 'Hasker',
  #                         :organization => 'MSOE',
  #                         :account_type => 1)
  #   hasker.password = 'grape'
  #   hasker.save
  #   rowemi = Account.where(:email_address => 'rowemi@uwplatt.edu').
  #     first_or_create(:given_name => 'Mike',
  #                         :surname => 'Rowe',
  #                         :organization => 'UW-Platteville',
  #                         :account_type => 1)
  #   rowemi.password = 'grape'
  #   rowemi.save
  #   shiy = Account.where(:email_address => 'shiy@uwplatt.edu').
  #     first_or_create(:given_name => 'Yan',
  #                         :surname => 'Shi',
  #                         :organization => 'UW-Platteville',
  #                         :account_type => 1)
  #   shiy.password = 'grape'
  #   shiy.save
  # end

  def child
    if account_type == INSTRUCTOR
      becomes(Instructor)
    elsif account_type == STUDENT
      becomes(Student)
    end
  end
end
