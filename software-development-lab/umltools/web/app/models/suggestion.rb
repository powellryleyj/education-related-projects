# (12/9/18) powellr - This class, and all of its usages have been renamed from
#  Warning -> Suggestion as it was conflicting with an newly introduced
# "Warning" module introduced in Ruby-2.4.x
#
#
# Class for recording suggestions posted to the user. Each suggestion is stored
#   in the database just once, so when a new suggestion is created we need
#   to fist check if it is already in the database. At one time this
#   was done no the basis of :message, but when the messages get very
#   large (such as lots of undocumented classes) then we overflow the
#   maximum size for a string. :message_hash solves this problem by
#   providing a quick way to look up a suggestion object but allowing for
#   very long suggestions.
#

require 'digest/sha1'

class Suggestion < ApplicationRecord
  # attr_accessible :category, :detailed_issue, :explanation_id, :message, :element_id

  has_and_belongs_to_many :submissions
  belongs_to :explanation

  # string (message) -> Suggestion
  def self.setup(issue)
    parts      = issue.split(';', 4)
    issue_name = parts[0]
    subissue   = parts[1]
    msg        = parts[2] || '' # rwh, 2016: don't crash on bad issue
    assoc_id   = parts[3]
    hash       = Suggestion.hash_for_message(msg)
    entry      = Suggestion.lookup_message_by_hash(msg, hash)
    if entry.nil?
      w = Suggestion.new
      # robustly form a link to the associated explanation (so at
      #  the very least there are precedence levels)
      explanation = Explanation.where(name: issue_name)
      if explanation.size == 1
            explanation = explanation[0]
      else
            explanation = Explanation.where(name: 'UNKNOWN')
      end
      w.explanation    = explanation
      w.message        = msg
      w.message_hash   = hash
      w.element_id     = assoc_id
      w.category       = issue_name
      w.detailed_issue = subissue
      w.save
      entry = Suggestion.find w.id
    end
    entry
  end

  def <=>(other)
    if explanation.precedence == other.explanation.precedence
      # otherwise, sort alphabetically
      message <=> other.message
    else
      # sort precedences so high items first
      -(explanation.precedence <=> other.explanation.precedence)
    end
  end

  def self.hash_for_message(msg)
    # rwh, sep 2016: provide fault tolerance if msg is nil
    #   (though in fact it should never be)
    Digest::SHA1.hexdigest(msg || '')
  end

  def self.lookup_message_by_hash(msg, hash)
    Suggestion.where(message_hash: hash).where(message: msg).take
  end

  def self.lookup_message(msg)
    h = Suggestion.hash_for_message(msg)
    return lookup_message_by_hash(msg, h)
  end

  # list(string) -> list(Suggestion)
  def self.setup_suggestions(msg_list)
    msg_list = msg_list.map { |x| x.strip }.select { |x| !x.nil? && !x.empty? }
    if msg_list.empty?
      return []
    else
      suggestions = msg_list.map { |m| setup(m) }
      return suggestions
    end
  end
end
