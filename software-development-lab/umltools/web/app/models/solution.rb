require 'fileutils'
class Solution < Submission
  # relative to cwd when run script/server

  def path
    File.join(Rails.configuration.solution_dir, problem.instructor.email_address, save_name)
  end

  # create a solution: CGI file object -> Solution (subclass of Submission)
  def self.create(submission_file, problem, remote_ip_addr)
    soln = Solution.new
    soln.time = Time.new
    soln.check_id = problem.check_list
    soln.file_type_id = problem.assignment_type # Assume the types will be the same
    soln.problem = problem
    soln.from_ip = remote_ip_addr
    # soln.ticket  = gen_ticket(soln.time) # (2/2/19) powellr - the use of tickets have been deprecated
    soln.orig_name, soln.save_name =
      submission_file_name(submission_file.original_filename,
                           soln.time)
    # write the file
    make_save_file(soln, submission_file)

    soln.generate_diagram = false
    soln.save
    soln = Submission.find soln.id

    return soln
  end

  def run_checks(generate_diagram)
    options = []
    if file_type_id == Problem::SEQUENCE_DIAGRAM_TYPE ||
       (problem.assignment_type.to_i == Problem::HIGH_LEVEL_DIAGRAM_TYPE &&
        problem.problem_word.assignment_type == Problem::SEQUENCE_DIAGRAM_TYPE)
      options << '-S'
    end
    cl = CheckList.find_by_id self.check_id
    if cl
      check_file = cl.written_umlint_yaml
      options << '-c'
      options << "#{check_file}"
    end
    if generate_diagram
      options << '-G'
      self.generate_diagram = true
    end
    options << '-p'
    options << self.problem_id
    options << '-i'
    if self.problem.assignment_type.to_i != Problem::HIGH_LEVEL_DIAGRAM_TYPE
      options << self.problem.path
    end
    options << self.path
    old_stdout = $stdout
    $stdout = (check_output = StringIO.new)
    UmlTools.pdiff_main(options)
    messages = check_output.string.chomp
    $stdout = old_stdout

    # messages = `#{Rails.configuration.umlgrader_path} -i "#{problem.path}" "#{self.path}"`
    self.suggestions = Suggestion.setup_suggestions(messages.split("\n"))
    self.save
    self
  end

  def ordered_results
    ordered_suggestions
  end
end
