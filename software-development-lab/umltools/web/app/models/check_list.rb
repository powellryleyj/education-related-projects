require 'yaml'
require 'set'
require 'json'
class CheckList < ApplicationRecord
  # fields :description, :instructor_id, :is_inclusion_list, :language, :retired

  has_and_belongs_to_many :check_items
  belongs_to :instructor
  has_many :submissions

  # Note: if add item here, must add to web/app/models/check_list.rb as well
  # Note: don't use CheckItem in following to avoid adding unnecessary
  #       items to the database
  @@inactive_checkitems = Set.new %w[MISSING_CONTAINER PUBLICATTR]

  #
  # note: need to write to a file before changing in database
  #

  # version identifier for purpose of file format
  @@file_version = '1.0'

  def self.create_from_file(checklist_file, uploading_instructor)
    FileUtils.mkdir_p Rails.root.join('public', 'uploads')
    File.open(Rails.root.join('public', 'uploads', checklist_file.original_filename), 'wb') do |file|
      file.write(checklist_file.read)
    end
    new_file = File.read(Rails.root.join('public', 'uploads', checklist_file.original_filename))
    data = JSON.parse(new_file)
    # instructor = Instructor.where(:email_address => data[:contact_email_address]).take
    new_checklist = CheckList.create(description: data['description'],
                                     instructor_id: uploading_instructor.id,
                                     language: data['language'],
                                     is_inclusion_list: data['is_inclusion_list'],
                                     retired: false)
    data['checks'].each do |k, v|
      category = k
      v.each do |issue|
        check_item = CheckItem.where(category: category, issue: issue).first_or_create
        new_checklist.check_items << check_item
      end
    end
    new_checklist.save
    File.rename(Rails.root.join('public', 'uploads', checklist_file.original_filename), Rails.root.join('public', 'uploads', new_checklist.id.to_s + '.json'))
    new_checklist.id
  end

  def self.check_types
    lists = CheckList.where(retired: false).sort_by(&:description)
    [['Apply all checks', 0]] + lists.map { |l| [l.description, l.id] }
  end

  def self.all_check_types
    lists = CheckList.all.map { |l| [l.description || 'new', l.id] }
    lists.sort! { |a, b| a[0] <=> b[0] }
    lists
  end

  def to_json_string
    data = {}
    data[:is_inclusion_list] = is_inclusion_list
    data[:checks] = {}
    checks = check_items.sort_by(&:category)
    last_category = nil
    checks.each do |ci|
      if ci.category != last_category
        data[:checks][ci.category] = []
        last_category = ci.category
      end
      data[:checks][ci.category] << ci.issue
    end
    # data[:contact_email_address] = instructor.email_address
    data[:description] = description
    data[:language] = language
    data[:organization] = instructor.organization
    data[:tool_version] = @@file_version
    data.to_json
  end

  def written_json_string
    text = to_json_string
    FileUtils.mkdir_p Rails.root.join('public', 'downloads')
    filename = File.join('public', 'downloads', "chk#{id}.json")
    File.open(filename, 'w+') { |io| io.puts(text) }
    # note: may need to switch following to call .absolute_path
    File.expand_path(filename)
  end

  # Return checklist in YAML format needed by UMLint
  # Sample file for
  #     x = CheckItem.new('X', :ALL)
  #     y = CheckItem.new('Y', 'yy')
  #     z = CheckItem.new(:Y,  :zz)
  #     cl = ChecksToExclude.new('testorg', 'testdescr', 'testemail', 'testver',
  #                              'testlang', [x, y, z])
  # vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
  #   --- !ruby/object:ChecksToExclude
  #   checks_to_exclude:
  #     :X: !ruby/object:Set
  #       hash: {}
  #     :Y: !ruby/object:Set
  #       hash:
  #         :yy: true
  #         :zz: true
  #
  #   contact_email_address: testemail
  #   description: testdescr
  #   language: testlang
  #   organization: testorg
  #   tool_version: testver
  def to_umlint_yaml
    result = '--- !ruby/object:UmlTools::ListOfChecks::' +
             (is_inclusion_list ? 'ChecksToInclude' : 'ChecksToExclude') + "\n"
    result += is_inclusion_list ? "checks_to_include:\n" : "checks_to_exclude:\n"
    checks = check_items.sort_by(&:category)
    last_category = nil
    checks.each do |ci|
      if ci.category != last_category
        result += "  :#{ci.category}: !ruby/object:Set\n"
        result += if ci.issue == 'ALL'
                    "    hash: {}\n"
                  else
                    "    hash:\n"
                  end
        last_category = ci.category
      end
      result += "      :#{ci.issue}: true\n" if ci.issue != 'ALL'
    end
    result += "\n"
    result += 'contact_email_address: ' + instructor.email_address + "\n"
    result += 'description: "' + description + "\"\n"
    result += 'language: ' + language + "\n"
    result += 'organization: ' + instructor.organization + "\n"
    result += 'tool_version: ' + @@file_version + "\n"
    result
  end

  # returns path to file containing umlint representation of checklist
  def written_umlint_yaml
    text = to_umlint_yaml
    # filename = File.join(Submission.checks_directory, "chk#{id}.txt"
    filename = File.join('public', "chk#{id}.cfg")
    # puts "Writing checklist to #{filename}"
    File.open(filename, 'w+') { |io| io.puts(text) }
    # note: may need to switch following to call .absolute_path
    File.expand_path(filename)
  end

  def organization
    if !instructor.nil?
      i = instructor.parent
    else
      i = nil
    end
    i && i.organization || "unknown"
  end

  def contact
    if !instructor.nil?
      i = instructor.parent
    else
      i = nil
    end

    if i.nil?
      return 'unknown'
    else
      return i.best_name
    end
  end

  def all_check_names
    Explanation.selectable_umlint_explanations.map(&:name)
  end

  # returns map with all checks "on" except those that default to off
  def all_checks_enabled
    result = {}
    checks = all_check_names
    checks.each do |ci_name|
      sym = ci_name.to_sym
      next if @@inactive_checkitems.include?(ci_name)

      detailed = Explanation.where(name: ci_name).take
                            .detailed_issue_list.map(&:to_sym)
      result[sym] = Set.new(detailed)
      result[sym] << :ALL
    end
    result
  end

  # return items to be included in the checklist
  def inclusion_map
    result = {}
    checks = check_items
    if is_inclusion_list
      # include only those items that are named
      checks.each do |ci|
        category = ci.category.to_sym
        issue = ci.issue.to_sym
        if issue == :ALL
          result[category] = Set.new([:ALL])
        elsif result[category]
          result[category] << issue
        else
          result[category] = Set.new([issue])
        end
      end
    else
      # include all items EXCEPT named ones
      result = all_checks_enabled
      checks.each do |ci|
        category = ci.category.to_sym
        issue = ci.issue.to_sym
        if issue == :ALL || result[category].nil?
          result[category] = Set.new
        else
          result[category].delete(:ALL)
          result[category].delete(issue)
        end
      end
    end
    result
  end

  # update checklist according to parameters and save it
  #   assumption: instructor field is set
  # Returns true if update successful, false otherwise
  def updated_to(params)
    #--------------------------------------------------
    # sample params:
    # {"checklist_id"=>"1", "commit"=>"Submit changes", "language"=>"C++", "authenticity_token"=>"bIW8XMkE/GonKQfrLwGLS/YDmh6ZzmharOpBFRDShoo=", "action"=>"do_update_checklist", "include"=>{"11", "22", "33", "6", "34", "23", "12", "7", "8", "13", "35", "25", "36", "14", "15", "26", "37", "16", "38", "27", "17", "29", "30", "18", "31", "3", "20", "32", "4", "21", "5"},
    # "checkitem"=>{"34"=>{"illegal_dependency_stereotypes", "no_plain_dependencies"},
    # "12"=>{"invisible_use_cases", "invisible_actors"},
    # "14"=>{"mixed_styles_for_method_names", "mixed_attribute_naming_styles", "no_mixed_styles_for_class_names"},
    # "19"=>{"missing_class_documentation"}, "21"=>{"nondescript_attribute", "nondescript_identifiers"},
    # "5"=>{"no_collections", "no_complex_attributes", "non_simple_attributes"}},
    # "description"=>"SE 3330", "controller"=>"umlint", "password"=>"size20", "retired"=>"0"}
    #--------------------------------------------------
    self.description = params[:description]
    self.description = 'unnamed' if params[:description] == ''
    self.language    = params[:language]
    self.retired     = params[:retired] == '1'
    # will set this, then update record with this information
    items_to_check = []
    safe_include = params.permit(include: %w[22 16 10 56 57 31 36 41 42 6 7 8 58 59 63 24 43 44 4 5 9 14 15
                                             18 29 30 19 23 37 38 3 12 13 17 20 25 26 28 40 33 34 35 39 27 21 60 62 61 53 54 52 55
                                             48 49 45 46 47 50 51])
    all_explanations = Explanation.all
    if !params[:include].nil?
      all_inclusive_checks = safe_include[:include].to_h.keys
      all_inclusive_checks = all_inclusive_checks.collect(&:to_i)
    else
      all_inclusive_checks = []
    end

    subissues_to_include = {}
    unless params[:checkitem].nil?
      safe_checkitem = params.require(:checkitem).permit("22": %w[missing_actor_documentation missing_class_documentation missing_member_documentation missing_use_case_documentation],
                                                         "16": %w[mixed_attribute_naming_styles mixed_styles_for_method_names no_mixed_styles_for_class_names],
                                                         "56": %w[seq_message_labels seq_argument_labels], "42": %w[lower_case_attributes lower_case_roles],
                                                         "6": %w[no_collections no_complex_attributes non_simple_attributes], "63": %w[noun verb],
                                                         "24": %w[nondescript_attribute nondescript_identifier nondescript_method],
                                                         "14": %w[invisible_actors invisible_use_cases],
                                                         "3": %w[reference_to_missing_items role_name_at_source],
                                                         "40": %w[unassociated_actor class_associated_to_use_case],
                                                         "33": %w[use_cases_associate_to_actors generalization_between_use_cases no_stereotypes_or_names duplicated_association],
                                                         "39": %w[illegal_dependency_stereotypes no_named_dependencies no_plain_dependencies],
                                                         "60": %w[missing extra wrong_type], "62": %w[missing extra invalid_type out_of_order],
                                                         "61": %w[missing extra mislabeled], "53": %w[extra_final missing_final],
                                                         "54": %w[extra_initial missing_initial], "52": %w[extra_states_present states_missing],
                                                         "55": %w[extra_transitions_present transition_missing transition_reversed transition_incorrect],
                                                         "45": %w[missing_attributes unexpected_attributes], "46": %w[missing_classes unexpected_classes],
                                                         "47": %w[missing_methods unexpected_methods],
                                                         "50": %w[incorrect_role_name missing_associations multiplicity_error role_associated_with_wrong_classes unexpected_associations],
                                                         "51": %w[just_one_super no_super wrong_supers])
      safe_checkitem.each do |key, items|
        key = key.to_i
        subissues_to_include[key] = items.to_h.collect { |k, _v| k.to_s } unless all_inclusive_checks.include?(key)
      end
    end
    if is_inclusion_list
      # ensure check_list has only those items in params[]
      all_explanations.each do |expl|
        if all_inclusive_checks.include?(expl.id)
          items_to_check << CheckItem.get(expl, 'ALL')
        elsif subissues_to_include[expl.id]
          items_to_check += CheckItem.get_list(expl, subissues_to_include[expl.id])
        end
      end
    else
      # ensure check_list includes all but those items listed
      all_explanations.each do |expl|
        if all_inclusive_checks.include?(expl.id)
          # do nothing - item will be checked
        elsif subissues_to_include[expl.id]
          # add those items NOT mentioned
          items_to_check +=
            CheckItem.get_list_complement(expl, subissues_to_include[expl.id])
        else
          # item is completely excluded
          items_to_check << CheckItem.get(expl, 'ALL')
        end
      end
    end
    self.check_items = items_to_check
    save
    true
  end

  # this is called from config/environment.rb
  # (1/19/19) powellr - disables as default instantiation has been moved to seeds.rb
  # def self.install_defaults
  #   nodoc1 = CheckItem.where(category: 'NODOC',
  #                            issue: 'missing_actor_documentation').first_or_create
  #   nodoc2 = CheckItem.where(category: 'NODOC',
  #                            issue: 'missing_use_case_documentation').first_or_create
  #   nodoc3 = CheckItem.where(category: 'NODOC',
  #                            issue: 'missing_member_documentation').first_or_create
  #   noindex = CheckItem.where(category: 'INDEX',
  #                             issue: 'ALL').first_or_create
  #
  #   cl = CheckList.where(description: 'SE 2030').take
  #   if cl.nil?
  #     cl = CheckList.create(description: 'SE 2030',
  #                           instructor_id: hasker.id,
  #                           language: 'Java',
  #                           is_inclusion_list: false,
  #                           retired: false)
  #     cl.check_items << nodoc1
  #     cl.check_items << nodoc2
  #     cl.check_items << nodoc3
  #     cl.check_items << noindex
  #   end
  #   cl
  # end
end
