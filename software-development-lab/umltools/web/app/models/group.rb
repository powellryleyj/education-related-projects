class Group < ApplicationRecord
  # fields :name, :description

  belongs_to :instructor
  has_and_belongs_to_many :problems
  has_and_belongs_to_many :students


end
