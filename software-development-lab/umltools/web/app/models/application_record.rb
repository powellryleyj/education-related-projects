# Needed for the upgrade from Rails 4.2 to Rails 5.0
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end
