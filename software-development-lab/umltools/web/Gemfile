source 'https://rubygems.org'

ruby '2.4.5'

# Code Coverage
gem 'simplecov'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.1.0'
gem 'responders'
gem 'railties'

# Graphics library used for drawing UML diagrams (only sequence right now 4/28/2015)
gem 'cairo', '~> 1.14.1'

# sqlite3 as dev db
gem 'sqlite3'
# Use mysql for deployment: (commented for TeamCity build purposes -> powellr on 10/20/18 )
# gem 'mysql'

# Use SCSS for stylesheets
gem 'sass-rails'

gem 'bootstrap-sass', '~> 3.1.1'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.2.2'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

gem 'jquery-ui-rails'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

gem 'jquery-turbolinks'

gem 'bootstrap-datepicker-rails'

# For lib:
gem 'rake', '~> 10.4.2'
gem 'ci_reporter', '~> 2.0'
# gem 'flexmock' - removed as of (10/30) by powellr
gem 'test-unit'
gem 'simplecov-rcov'
gem 'simplecov-teamcity-summary'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder' # (10/30/18) powellr - removed gem version so that it could update to most recent release
               # as it was causing a deprecation warning dealing with Mime::JSON

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

#upgrade gems
gem 'rails-observers'
gem 'activesupport'
gem 'activemodel'
gem 'actionpack'
gem 'actionpack-page_caching'
gem 'actionpack-action_caching'
# gem 'activerecord-deprecated_finders' # (10/30/18) powellr - removed since deprecated as of rails-5.0

# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'

# Use unicorn as the app server
gem 'unicorn'

# For use of attr_accessible
# gem 'protected_attributes_continued'

# Use Capistrano for deployment
gem 'capistrano', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]

gem 'utf8-cleaner'

# CI::Reporter::TestUnit
gem 'ci_reporter_test_unit'

# For use of assign in testing
gem 'rails-controller-testing'

gem 'hirb'

# For putting multiple submissions in zip files for download
gem 'rubyzip'
