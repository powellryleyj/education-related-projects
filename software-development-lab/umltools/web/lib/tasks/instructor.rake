require File.join(File.dirname(__FILE__), '..', '..', 'app', 'models', 'account')

namespace :instructor do

  desc 'Create a new user'
  task :create => :environment do
    puts 'Email:'
    email = $stdin.gets.delete "\n"
    if Account.where(:email_address => email).pluck(:email_address).to_s.match(email.to_s)
      raise 'Email already in use!'
    end
    puts 'Password:'
    pass = $stdin.gets.delete "\n"
    puts 'Confirm password:'
    confirm = $stdin.gets.delete "\n"
    if pass != confirm
      raise 'Passwords do not match!'
    end
    puts 'First name:'
    name = $stdin.gets.delete "\n"
    puts 'Last name:'
    surname = $stdin.gets.delete "\n"
    puts 'Organization:'
    org = $stdin.gets.delete "\n"
    # Create the instructor
    # instructor = Instructor.create(:email_address => email, :password => pass,
    #   :given_name => name, :surname => surname, :organization => org)
    i = Instructor.new
    i.email_address = email
    i.change_password pass
    i.given_name = name
    i.surname = surname
    i.organization = org
    i.account_type = Account::INSTRUCTOR
    i.save
  end
end
