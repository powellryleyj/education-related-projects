require File.join(File.dirname(__FILE__), '..', '..', 'app', 'models', 'account')

namespace :student do

  desc 'Create a new user'
  task :create => :environment do
    puts 'Email:'
    email = $stdin.gets.delete "\n"
    if Account.where(:email_address => email).pluck(:email_address).to_s.match(email.to_s)
      raise 'Email already in use!'
    end
    puts 'Password:'
    pass = $stdin.gets.delete "\n"
    puts 'Confirm password:'
    confirm = $stdin.gets.delete "\n"
    if pass != confirm
      raise 'Passwords do not match!'
    end
    puts 'First name:'
    name = $stdin.gets.delete "\n"
    puts 'Last name:'
    surname = $stdin.gets.delete "\n"
    puts 'Organization:'
    org = $stdin.gets.delete "\n"
    # Create the instructor
    # instructor = Instructor.create(:email_address => email, :password => pass,
    #   :given_name => name, :surname => surname, :organization => org)
    s = Student.new
    s.email_address = email
    s.change_password pass
    s.given_name = name
    s.surname = surname
    s.organization = org
    s.account_type = Account::STUDENT
    s.save
  end
end
