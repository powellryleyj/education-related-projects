# require 'flexmock'
# require 'flexmock/test_unit'
require 'ci/reporter/rake/test_unit'

namespace :lib do
  @coverage_directory = File.join(File.dirname(__FILE__), '/reports/coverage')
  @test_report_directory =  File.join(File.dirname(__FILE__), '/reports/tests')

  desc "Run tests"
  task :test => [:simplecov_setup, 'ci:setup:testunit'] do
    require(File.join(File.dirname(__FILE__), '..', '..', 'test', 'lib', 'test_all.rb'))
  end

  desc "Setup code coverage analysis (before running tests task)"
  task :simplecov_setup do
    require 'simplecov'
    require 'simplecov-rcov'
    require 'simplecov-teamcity-summary'
    #must be started before tests are loaded
    SimpleCov.formatter = SimpleCov::Formatter::RcovFormatter
    SimpleCov.start do
      add_filter do |src|
        !(src.filename =~ /^#{$lib_directory}/)
      end
      at_exit do
        SimpleCov.result.format!
        SimpleCov::Formatter::TeamcitySummaryFormatter.new.format(SimpleCov.result) if ENV['TEAMCITY_VERSION']
      end
      coverage_dir @coverage_directory
      ENV["CI_REPORTS"] = @test_report_directory
    end
  end

  #Does not work.  Heckle is an older gem with a new beta available that I can't get to work.
  desc "Not functional!  Mutation testing tool."
  task :heckle do
    require 'heckle/heckle_runner'
    path_pattern = "#{File.join(File.dirname(__FILE__), '..', '..', 'test', 'lib','test_all.rb')}".split('\\').join('/')
    options = {
        :force => false,
        :debug => false,
        :focus => false,
        :timeout => 5,
        :test_pattern => path_pattern,
        :nodes => [],
    }
    HeckleRunner.new('TestUtils', nil, options).run
  end

end
