module UmlTools
  class HighLevelDiff
    def initialize(model, nouns, verbs)
      @model = model
      @nouns = nouns.split '|'
      @verbs = verbs.split '|'
      @is_sequence = model.instance_of? UmlTools::Model::Sequence::FullModel
    end

    def errors
      check_nouns + check_verbs
    end

    def check_nouns
      # Class names/variables
      nouns = @nouns.dup.map { |n| n.downcase }
      if @is_sequence
        # Sequence diagram
        @model.lifelines.each { |lifeline|
          if nouns.include? lifeline.name.downcase
            nouns.delete lifeline.name.downcase
          end
        }
      else
        # Class diagram
        @model.logical_model.classes.each { |cls|
          if nouns.include? cls.name.downcase
            nouns.delete cls.name.downcase
          end
          cls.attributes.each { |attr|
            if nouns.include? attr.name.downcase
              nouns.delete attr.name.downcase
            end
          }
        }
      end
      ret = []
      nouns.each { |noun|
        ret << Issue.new(:MISSING_HIGH_LEVEL, :noun, "Missing noun #{noun}.")
      }
      ret
    end

    def check_verbs
      # Methods
      verbs = @verbs.dup.map { |v| v.downcase }
      if @is_sequence
        # Sequence diagram
        @model.messages.each { |message|
          if verbs.include? message.name.downcase
            verbs.delete message.name.downcase
          end
        }
      else
        # Class diagram
        @model.logical_model.classes.each { |cls|
          cls.methods.each { |method|
            if verbs.include? method.name.downcase
              verbs.delete method.name.downcase
            end
          }
        }
      end
      ret = []
      verbs.each { |verb|
        ret << Issue.new(:MISSING_HIGH_LEVEL, :verb, "Missing verb #{verb}.")
      }
      ret
    end
  end
end
