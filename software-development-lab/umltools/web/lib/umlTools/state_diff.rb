module UmlTools
  require_relative 'helper'

  class StateDiff

    attr_reader :submitted_model, :correct_model


    # submitted_full_model: FullModel
    # correct_full_model: FullModel
    def initialize(correct_full_model, submitted_full_model)
      @errors = []
      @submitted_model = submitted_full_model.state
      @correct_model = correct_full_model.state
    end

    def extra_states
      if @extra_states.nil?
        compare_states
      end
      @extra_states
    end

    def missing_states
      if @missing_states.nil?
        compare_states
      end
      @missing_states
    end

    def final_correct_minus_submitted
      if @final_correct_minus_submitted.nil?
        compare_final_initial_nodes
      end
      @final_correct_minus_submitted
    end

    def initial_correct_minus_submitted
      if @initial_correct_minus_submitted.nil?
        compare_final_initial_nodes
      end
      @initial_correct_minus_submitted
    end

    def extra_transitions
      if @extra_transitions.nil?
        compare_transitions
      end
      @extra_transitions
    end

    def missing_transitions
      if @missing_transitions.nil?
        compare_transitions
      end
      @missing_transitions
    end

    def reversed_transitions
      if @reversed_transitions.nil?
        compare_transitions
      end
      @reversed_transitions
    end

    def incorrect_transitions
      if @incorrect_transitions.nil?
        compare_transitions
      end
      @incorrect_transitions
    end

    # Compares the states of the submitted model and correct model by name
    # and returns a list of states that the submitted model is missing and
    # a list of extra states on the submitted model.
    def compare_states
      submitted_states = @submitted_model.states.clone
      correct_states = @correct_model.states.clone
      extra_states = []
      missing_states = []
      until correct_states.empty?
        correct_state = correct_states.shift
        matched_states = submitted_states.select { |submitted_state| match_state_by_name?(submitted_state, correct_state) }

        if matched_states.empty?
          missing_states << correct_state
        else
          matched_state = matched_states[0]
          index = submitted_states.index(matched_state)
          submitted_states.delete_at(index)
        end
      end # while
      missing_states.sort!
      extra_states = submitted_states.sort # remaining items in a
      @extra_states = extra_states
      @missing_states = missing_states
    end

    def compare_final_initial_nodes
      # compare counts and add errors
      @final_correct_minus_submitted = @correct_model.final_nodes.size - @submitted_model.final_nodes.size
      @initial_correct_minus_submitted = @correct_model.initial_nodes.size - @submitted_model.initial_nodes.size
    end

    # Compares the transitions of the submitted model and correct model by the names of the
    # source and correct states, as well as the existence of triggers/guards/effects
    def compare_transitions
      submitted_transitions = @submitted_model.transitions.clone
      correct_transitions = @correct_model.transitions.clone
      missing_transitions = []
      incorrect_trigger_transitions = []
      incorrect_guard_transitions = []
      incorrect_effect_transitions = []

      # Find the transitions in the submitted model that have the same names for the source
      # target states as a transition in the correct model. The match_state_by_name? method
      # is used to determine if the state names match
      until correct_transitions.empty?
        correct_transition = correct_transitions.shift
        correct_transition_source = correct_transition.source_node
        correct_transition_target = correct_transition.target_node

        possible_matches = submitted_transitions.select { |submitted|
          match_state_by_name?(submitted.source_node, correct_transition.source_node) &&
              match_state_by_name?(submitted.target_node, correct_transition.target_node)
        }

        if possible_matches.empty?
          missing_transitions << correct_transition
        else
          matching_transition = nil

          possible_matches.each { |possible_match|
            if do_triggers_match?(possible_match, correct_transition) &&
                do_guards_match?(possible_match, correct_transition) &&
                do_effects_match?(possible_match, correct_transition)
              matching_transition = possible_match
            end
          }

          if matching_transition
            index = submitted_transitions.index(possible_matches[0])
            submitted_transitions.delete_at(index)
          else
            missing_transitions << correct_transition
          end

        end
      end

      @extra_transitions = filter_error_transitions(submitted_transitions)
      @missing_transitions = filter_error_transitions(missing_transitions)

      # put reversed transitions in their own category rather than missing and extra
      find_reversed_and_incorrect_transitions
    end

    # For transitions we don't want to double count errors in the case that either the
    # source or target state was named incorrectly. If the source or target appears
    # in the extra or missing states, we won't count the transition as being extra/missing
    def filter_error_transitions(transitions)
      transitions.select { |transition|
        !extra_states.include?(transition.source_node) &&
            !extra_states.include?(transition.target_node) &&
            !missing_states.include?(transition.source_node) &&
            !missing_states.include?(transition.target_node)
      }
    end

    # This method looks through the missing and extra transitions to find those that might be
    # reversed. If that is the case (i.e. they switched the source and target) it is more helpful
    # to give a single error saying it is reversed instead of saying there is one that is missing
    # and another that is extra
    def find_reversed_and_incorrect_transitions
      @reversed_transitions = []
      @incorrect_transitions = []
      to_delete = []

      @extra_transitions.each { |extra_transition|
        @missing_transitions.each { |missing_transition|
          # if the source and targets are switched, we have a reversed transition
          if match_state_by_name?(extra_transition.source_node, missing_transition.target_node) &&
              match_state_by_name?(extra_transition.target_node, missing_transition.source_node) &&
              !to_delete.include?(missing_transition) then
            @reversed_transitions << extra_transition
            to_delete << extra_transition
            to_delete << missing_transition
            # if the source and target match, the transition is incorrect because of trigger, guard or effect
          elsif match_state_by_name?(extra_transition.source_node, missing_transition.source_node) &&
              match_state_by_name?(extra_transition.target_node, missing_transition.target_node) &&
              !to_delete.include?(missing_transition) then
            @incorrect_transitions << extra_transition
            to_delete << extra_transition
            to_delete << missing_transition
          end
        }
      }

      @extra_transitions = @extra_transitions - to_delete
      @missing_transitions = @missing_transitions - to_delete
    end

    # Compares the triggers of the given transitions. Assumes multiple triggers on the correct transition
    # are separate possible matches, as well as any '|' separated values within the triggers. Each trigger
    # on the submitted transition must match at least one of the possible values to be considered a match.
    # If the correct transition has '*' as a trigger, it will simply check for existence.
    def do_triggers_match?(submitted_transition, correct_transition)
      matched = false
      submitted_triggers = submitted_transition.triggers.map { |x| x.name }
      correct_triggers = correct_transition.triggers.map { |x| x.name }
      if submitted_triggers.size == 0
        # If both are empty => true, but if submitted is empty and correct is not => false
        matched = (correct_triggers.size == 0)
      elsif !correct_triggers.include? '*'
        possible_values = []
        matched_triggers = []
        correct_triggers.each { |trigger|
          possible_values += separate_and_escape(trigger)
        }
        submitted_triggers.each { |trigger|
          if match_by_value(trigger, possible_values)
            matched_triggers << trigger
          end
        }
        if matched_triggers.size == submitted_triggers.size
          matched = true
        end
      else
        matched = submitted_triggers.size > 0
      end
      matched
    end

    # Compares the guards of the given transitions. Checks if the text of the guard
    # matches at least one of the '|' separated values in the correct transition's
    # guard. If the correct transition has '*' as a guard, it will simply check for
    # existence.
    def do_guards_match?(submitted_transition, correct_transition)
      matched = false
      if submitted_transition.guard.nil? && correct_transition.guard.nil?
        matched = true
      elsif correct_transition.guard != '*'
        possible_values = separate_and_escape(correct_transition.guard)
        matched = match_by_value(submitted_transition.guard, possible_values)
      else
        matched = !submitted_transition.guard.nil?
      end
      matched
    end

    # Compares the effects of the given transitions. Checks if the text of the effect
    # matches at least one of the '|'' separated values in the correct transition's
    # effect. If the correct transition has '*' as a effect, it will simply check for
    # existence.
    def do_effects_match?(submitted_transition, correct_transition)
      matched = false
      if submitted_transition.effect.nil? && correct_transition.effect.nil?
        matched = true
      elsif correct_transition.effect != '*'
        possible_values = separate_and_escape(correct_transition.effect)
        matched = match_by_value(submitted_transition.effect, possible_values)
      else
        matched = !submitted_transition.effect.nil?
      end
      matched
    end

    # Returns a list of '|' separated string values with escape characters removed.
    # Ex. "abc | def \|\| ghi" --> ["abc", "def || ghi"]
    def separate_and_escape(value)
      (value.nil?) ? [] : split_values = value.split(/(?<!\\)[|]/).map { |x| x.strip.delete '\\' }
    end

    # Checks if the name of a submitted state matches the name of the correct state.
    # This includes the correct state's name and keywords including as a comma-separated
    # list in the documentation of the element. In order to match, the submitted state's
    # name must simply contain at least one keyword, it does not have to be a perfect
    # match.
    def match_state_by_name?(submitted_state, correct_state)
      possible_names = [correct_state.name]
      if (!correct_state.doc.nil?)
        possible_names += correct_state.doc.split(',').map { |x| x.strip }
      end
      matched = match_by_value(submitted_state.name, possible_names)
      # if they are not the same type (ex. class vs. initial node), no match
      if (!submitted_state.instance_of?(correct_state.class))
        matched = false
      end
      matched
    end

    # Compares a name to a list of possible names. To be a match, the name
    # must contain all of the individual words in at least one of the possible
    # names. For example "a b c" would match with ["a b", "b c"], but not
    # ["a b c d"]. It only needs to match one of the possible names so it
    # would also match ["a b", "d"].
    def match_by_value(submitted_name, possible_names)
      matched = false
      submitted_names = get_individual_identifiers(submitted_name)
      possible_names.each { |name|
        subnames = get_individual_identifiers(name)
        matches = submitted_names & subnames
        if matches.size == subnames.size
          matched = true
        end
      }
      matched
    end

    # Splits a string by spaces, case, and underscores
    def get_individual_identifiers(word)
      result = []
      word = '' unless !word.nil?
      UmlTools.words_in_identifier(word).each { |x| x.split(' ').each { |y| result << y.downcase } }
      result
    end

    def errors
      msgs = []
      if !extra_states.empty?
        message = $explicit_errors ? "Extra state(s): #{Helper.names(extra_states)}" : "Extra state(s) in diagram"
        msgs << Issue.new(:STATE_MISMATCH, :extra_states_present, message)
      end
      if !missing_states.empty?
        message = $explicit_errors ? "Missing state(s): #{Helper.names(missing_states)}" : "Missing state(s)"
        msgs << Issue.new(:STATE_MISMATCH, :states_missing, message)
      end
      if (final_correct_minus_submitted < 0)
        message = $explicit_errors ? "#{final_correct_minus_submitted.abs} extra final node(s) in diagram" : "Extra final node(s) in diagram"
        msgs << Issue.new(:FINAL_MISMATCH, :extra_final, message)
      end
      if (final_correct_minus_submitted > 0)
        message = $explicit_errors ? "#{final_correct_minus_submitted} missing final node(s)" : "Diagram missing final node(s)"
        msgs << Issue.new(:FINAL_MISMATCH, :missing_final, message)
      end
      if (initial_correct_minus_submitted < 0)
        message = $explicit_errors ? "#{initial_correct_minus_submitted.abs} extra initial node(s) in diagram" : "Extra initial node(s) in diagram"
        msgs << Issue.new(:INITIAL_MISMATCH, :extra_initial, message)
      end
      if (initial_correct_minus_submitted > 0)
        message = $explicit_errors ? "#{initial_correct_minus_submitted} missing initial node(s)" : "Diagram missing intial node(s)"
        msgs << Issue.new(:INITIAL_MISMATCH, :missing_initial, message)
      end
      unless extra_transitions.empty?
        message = "Extra transition(s): #{Helper.names(extra_transitions)}"
        msgs << Issue.new(:TRANSITION_MISMATCH, :extra_transitions_present, message)
      end
      unless missing_transitions.empty?
        message = "Missing transition(s): #{Helper.names(missing_transitions)}"
        msgs << Issue.new(:TRANSITION_MISMATCH, :transition_missing, message)
        msgs
      end
      unless reversed_transitions.empty?
        message = "Reversed transition(s): #{Helper.names(reversed_transitions)}"
        msgs << Issue.new(:TRANSITION_MISMATCH, :transition_reversed, message)
        msgs
      end
      unless incorrect_transitions.empty?
        message = "Transition(s) with incorrect trigger, guard, or effect: #{Helper.names(incorrect_transitions)}"
        msgs << Issue.new(:TRANSITION_MISMATCH, :transition_incorrect, message)
        msgs
      end

      msgs
    end
  end
end
