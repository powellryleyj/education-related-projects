module UmlTools
#!/usr/bin/env ruby
# logical_diff.rb: differences between model files; assumes 1st is target, 2nd is sub
#
# See 'usage' function for argument list
#

  require_relative 'check'
  require_relative 'petal_utils'
  require_relative 'petal'
  require_relative 'issues'
  require_relative 'model/associable_element'
  require_relative 'model/full_model'
  require_relative 'checklist/check_list'
  require_relative 'checklist/checks_to_exclude'
  require_relative 'helper'

  require 'set'

  $explicit_errors = false # give names in error messages?

# extend Petal classes to support matching
  module Model
    class Element
      # element which is paired with this one
      attr_accessor :doppleganger

      def match_name
        if @doppleganger
          @doppleganger.name
        else
          name
        end
      end

      def match_ident
        if @doppleganger
          @doppleganger.ident
        else
          ident
        end
      end

      # possible_names for the class; Set(String)
      def possible_names
        unless @possible_names
          words = doc_words
          words += [name] unless name.nil?
          words = words.map { |x| x.match_text }
          @possible_names = Set.new(words)
        end
        @possible_names
      end

      # string -> bool
      def matches_by_name?(candidate)
        possible_names.include?(candidate.match_text)
      end

      # just one candidate, ask if some term is in that candidate
      # string -> bool
      def possible_match?(candidate)
        return false if candidate.nil? || candidate.empty?
        return true if matches_by_name?(candidate)
        possible_names.each { |pn|
          return true if candidate.match_text.index(pn)
        }
        false
      end

    end # class Element

    # assumption: will process all classes in target diagram
    class MClass < AssociableElement
      attr_reader :extra_attributes, :missing_attributes
      attr_reader :extra_operations, :missing_operations
      attr_reader :extra_roles, :missing_roles, :reversed_roles, :misplaced_roles
      attr_accessor :loose_multiplicity_matching

      def doppleganger=(dop)
        @doppleganger = dop
        @missing_attributes, @extra_attributes =
            find_unmatched_elements(attributes, @doppleganger.attributes)
        @missing_operations, @extra_operations =
            find_unmatched_elements(methods, @doppleganger.methods)
      end

      # must be called after setting dopplegangers for all classes
      # assumption: self is an element from the target model,
      #    doppleganger is an element from the submitted model
      # must be called before set_weak_association_matches!
      # this version matches on names
      def set_strong_association_matches!
        visible_associates.each { |expected_role|
          # expected_role: role in current class that should have a match
          #   in the doppleganger class
          # candidates: list(Role) - items in doppleganger that could match
          #   on the basis of having already-matched suppliers
          # Note: supplier for the role is apparently the object that
          #   is the type for the role; consumer would always be self
          found_name_match = false
          candidates = @doppleganger.visible_associates.select { |cand_role|
            cand_role.doppleganger.nil? && # skip already matched items
                expected_role.supplier && cand_role.supplier &&
                expected_role.supplier.doppleganger &&
                expected_role.supplier.doppleganger == cand_role.supplier
          }
          # narrow on matching role names at this end
          if candidates.size > 1 && expected_role.name
            name_matches = candidates.select { |cand_role|
              expected_role.possible_match?(cand_role.name)
            }
            unless name_matches.empty?
              found_name_match = true
              candidates = name_matches
            end
          end
          # narrow on matching role names at other end;
          if candidates.size > 1 && expected_role.consumer_role
            name_matches = candidates.select { |cand_role|
              expected_role.consumer_role.possible_match?(
                  cand_role.consumer_role_name)
            }
            unless name_matches.empty?
              found_name_match = true
              candidates = name_matches
            end
          end

          # have filtered candidates; make best match available
          # first, sort to bias towards minimum name size; the sort does nothing
          #   if just one candidate, and when there are more this will
          #   effectively ensure there is a minimal number of unmatched
          #   characters (remembering it's rare there would be role names
          #   at both ends)
          # Of course, the real match may be in the documentation,
          #  but hopefully the most likely name is the displayed name
          # Note: don't force a match unless some name matched - cases where
          #  there is just one possible match are handled later
          if candidates.size > 0 && (found_name_match || candidates.size == 1)
            candidates.sort! { |x, y|
              # the .to_s's are to allow for nils in the following:
              xn = x.name.to_s + x.consumer_role_name.to_s
              yn = y.name.to_s + y.consumer_role_name.to_s
              xn.size == yn.size ? xn <=> yn : xn.size <=> yn.size
            }
            # set dopplegangers - this helps ensure each matched just once
            expected_role.doppleganger = candidates[0]
            candidates[0].doppleganger = expected_role
            expected_role.association.doppleganger = candidates[0].association
            candidates[0].association.doppleganger = expected_role.association
          end
        } # end of visible_associates.each, 1st pass (setting role dopplegangers)
      end

      # set_strong_association_matches!

      # must be called after set_strong_association_matches! has been called
      #   for all classes
      # Find matches which are reversed with the expected role names at the wrong
      #   ends.
      # assumption: self is an element from the target model,
      #    doppleganger is an element from the submitted model
      # sets :reversed_roles
      def set_reversed_association_matches!
        @reversed_roles = []
        # first, check matched associations to see if any are reversed
        visible_associates.each { |expected_role|
          if (actual = expected_role.doppleganger)
            if (expected_role.has_name? && actual.consumer_role_has_name? &&
                expected_role.possible_match?(actual.name)) ||
                (expected_role.consumer_role_has_name? &&
                    actual.consumer_role_has_name? &&
                    expected_role.consumer_role.possible_match?(
                        actual.consumer_role_name))
              # already have match at one end, so skip this item
            elsif expected_role.has_name? && actual.consumer_role_has_name? &&
                expected_role.possible_match?(actual.consumer_role_name)
              @reversed_roles << expected_role
            elsif expected_role.consumer_role_has_name? && actual.has_name? &&
                expected_role.consumer_role.possible_match?(actual.name)
              @reversed_roles << expected_role
            end
          end
        } # end of visible_associates.each, 1st pass
        #
        # next, search unmatched items for further reversed roles
        #
        visible_associates.each { |expected_role|
          if expected_role.doppleganger.nil?
            # expected_role: role in current class that should have a match
            #   in the doppleganger class
            # candidates: list(Role) - items in doppleganger that could match
            #   on the basis of having already-matched suppliers
            # Note: supplier for the role is apparently the object that
            #   is the type for the role; consumer would always be self
            candidates = @doppleganger.visible_associates.select { |cand_role|
              cand_role.doppleganger.nil? && # skip already matched items
                  expected_role.supplier && cand_role.supplier &&
                  expected_role.supplier.doppleganger &&
                  expected_role.supplier.doppleganger == cand_role.supplier
            }
            # narrow on matching role names at opposite ends
            if candidates.size > 1 && expected_role.has_name?
              name_matches = candidates.select { |cand_role|
                expected_role.possible_match?(cand_role.consumer_role_name)
              }
              unless name_matches.empty?
                found_name_match = true
                candidates = name_matches
              end
            end
            # narrow on matching role names using the other direction
            if candidates.size > 1 && expected_role.consumer_role_has_name?
              name_matches = candidates.select { |cand_role|
                expected_role.consumer_role.possible_match?(cand_role.name)
              }
              unless name_matches.empty?
                found_name_match = true
                candidates = name_matches
              end
            end
            if found_name_match && !candidates.empty?
              @reversed_roles << expected_role
              # pick first candidate at random
              # set doppleganger pointers - this ensures each matched just once
              expected_role.doppleganger = candidates[0]
              candidates[0].doppleganger = expected_role
              expected_role.association.doppleganger = candidates[0].association
              candidates[0].association.doppleganger = expected_role.association
            end
          end # if expected_role.doppleganger.nil?
        } # end of visible_associates.each, 2nd pass
      end

      # set_reversed_association_matches!

      # must be called after set_reversed_association_matches! has been called
      #   for all classes
      # Find matches in which the expected role name appears as the association
      #   name.
      # assumption: self is an element from the target model,
      #    doppleganger is an element from the submitted model
      # sets :misplaced_roles
      def set_misplaced_role_matches!
        @misplaced_roles = []
        visible_associates.each { |expected_role|
          if expected_role.doppleganger.nil?
            # expected_role: role in current class that should have a match
            #   in the doppleganger class
            # candidates: list(Role) - items in doppleganger that could match
            #   on the basis of having already-matched suppliers
            # Note: supplier for the role is apparently the object that
            #   is the type for the role; consumer would always be self
            candidates = @doppleganger.visible_associates.select { |cand_role|
              cand_role.doppleganger.nil? && # skip already matched items
                  expected_role.supplier && cand_role.supplier &&
                  expected_role.supplier.doppleganger &&
                  expected_role.supplier.doppleganger == cand_role.supplier
            }
            # narrow on matching role names at opposite ends
            # note: search even if just one item left since there must have
            #       been a reason why this item wasn't already matched
            #       and we want to make sure there's a name match before
            #       actually matching the two items
            if candidates.size > 0 && expected_role.has_name?
              name_matches = candidates.select { |cand_role|
                expected_role.possible_match?(cand_role.association.name)
              }
              unless name_matches.empty?
                found_name_match = true
                candidates = name_matches
              end
            end
            # narrow on matching role names using the other direction
            if candidates.size > 0 && expected_role.consumer_role_has_name?
              name_matches = candidates.select { |cand_role|
                expected_role.consumer_role.possible_match?(cand_role.association.name)
              }
              unless name_matches.empty?
                found_name_match = true
                candidates = name_matches
              end
            end
            if found_name_match && !candidates.empty?
              @misplaced_roles << expected_role
              # pick first candidate at random
              # set doppleganger pointers - this ensures each matched just once
              expected_role.doppleganger = candidates[0]
              candidates[0].doppleganger = expected_role
              expected_role.association.doppleganger = candidates[0].association
              candidates[0].association.doppleganger = expected_role.association
            end
          end # if expected_role.doppleganger.nil?
        } # end of visible_associates.each, 2nd pass
      end

      # set_misplaced_role_matches!

      # must be called after set_misplaced_role_matches! has been called
      #   for all classes
      # This version attempts to match on multiplicities.  If there are
      # multiple matches, then this will just pick the first, so this
      # match should have a low priority.
      # assumption: self is an element from the target model,
      #    doppleganger is an element from the submitted model
      def match_on_multiplicities!
        visible_associates.each { |expected_role|
          if expected_role.doppleganger.nil?
            # expected_role: role in current class that should have a match
            #   in the doppleganger class
            # candidates: list(Role) - items in doppleganger that could match
            #   on the basis of having already-matched suppliers
            # Note: supplier for the role is apparently the object that
            #   is the type for the role; consumer would always be self
            candidates = @doppleganger.visible_associates.select { |cand_role|
              cand_role.doppleganger.nil? && # skip already matched items
                  expected_role.supplier && cand_role.supplier &&
                  expected_role.supplier.doppleganger &&
                  expected_role.supplier.doppleganger == cand_role.supplier
            }
            # try to match on role multiplicities
            mult_matches = candidates.select { |c|
              c.effective_multiplicity && expected_role.effective_multiplicity &&
                  c.effective_multiplicity == expected_role.effective_multiplicity
            }
            unless mult_matches.empty?
              # pick first candidate at random
              # set doppleganger pointers - this ensures each matched just once
              expected_role.doppleganger = mult_matches[0]
              mult_matches[0].doppleganger = expected_role
              expected_role.association.doppleganger = mult_matches[0].association
              mult_matches[0].association.doppleganger = expected_role.association
            end
          end # if expected_role.doppleganger.nil?
        } # end of visible_associates.each, 2nd pass
      end

      # set_weak_association_mismatches!

      # must be called after set_misplaced_role_matches! has been called
      #   for all classes
      # This version forces matches wherever there is just one choice
      # assumption: self is an element from the target model,
      #    doppleganger is an element from the submitted model
      # also sets :extra_roles, :missing_roles
      def set_singleton_association_matches!
        visible_associates.each { |expected_role|
          if expected_role.doppleganger.nil?
            # expected_role: role in current class that should have a match
            #   in the doppleganger class
            # candidates: list(Role) - items in doppleganger that could match
            #   on the basis of having already-matched suppliers
            # Note: supplier for the role is apparently the object that
            #   is the type for the role; consumer would always be self
            candidates = @doppleganger.visible_associates.select { |cand_role|
              cand_role.doppleganger.nil? && # skip already matched items
                  expected_role.supplier && cand_role.supplier &&
                  expected_role.supplier.doppleganger &&
                  expected_role.supplier.doppleganger == cand_role.supplier
            }
            if candidates.size == 1
              expected_role.doppleganger = candidates[0]
              candidates[0].doppleganger = expected_role
              expected_role.association.doppleganger = candidates[0].association
              candidates[0].association.doppleganger = expected_role.association
            end
          end # if expected_role.doppleganger.nil?
        } # end of visible_associates.each, 3rd pass
      end

      # set_singleton_association_matches!

      # must be called after set_singleton_association_matches!
      def set_mismatches!
        # now record unmatched items
        @extra_roles = @doppleganger.visible_associates.select {
            |a| a.doppleganger.nil?
        }
        @missing_roles = visible_associates.select { |a| a.doppleganger.nil? }
      end

      # set_association_matches!

      # must be called after setting role matches for all classes
      def mismatches
        msgs = []
        msgs += member_errors
        msgs += association_errors
        msgs += super_errors
        msgs
      end

      def member_errors
        msgs = []
        if $explicit_errors
          if !@extra_attributes.empty?
            msgs << Issue.new(:ATTR_MISMATCH, :unexpected_attributes,
                              "Unexpected attributes in #{match_ident}: #{Helper.names(@extra_attributes)}")
          end
          if !@missing_attributes.empty?
            msgs << Issue.new(:ATTR_MISMATCH, :missing_attributes,
                              "Missing attributes in #{match_ident}: #{Helper.names(@missing_attributes)}")
          end
          if !@extra_operations.empty?
            msgs << Issue.new(:METHOD_MISMATCH, :unexpected_methods,
                              "Unexpected operations in #{match_ident}: #{Helper.names(@extra_operations)}")
          end
          if !@missing_operations.empty?
            msgs << Issue.new(:METHOD_MISMATCH, :missing_methods,
                              "Missing operations in #{match_ident}: #{Helper.names(@missing_operations)}")
          end
        else
          if !@extra_attributes.empty?
            msgs << Issue.new(:ATTR_MISMATCH, :unexpected_attributes,
                              "Unexpected attributes in #{match_ident}")
          end
          if !@missing_attributes.empty?
            msgs << Issue.new(:ATTR_MISMATCH, :missing_attributes,
                              "Missing attributes in #{match_ident}")
          end
          if !@extra_operations.empty?
            msgs << Issue.new(:METHOD_MISMATCH, :unexpected_methods,
                              "Unexpected operations in #{match_ident}")
          end
          if !@missing_operations.empty?
            msgs << Issue.new(:METHOD_MISMATCH, :missing_methods,
                              "Missing operations in #{match_ident}")
          end
        end # if $explicit_errors
        msgs
      end

      # returns errors related to extra_roles, missing_roles, and matched items
      def association_errors
        msgs = []

        @reversed_roles.each { |reversed|
          # if no name, skip - the error in the association will be noted when
          #   examining the other end
          unless reversed.full_name.nil?
            msgs << Issue.new(:REVERSED_ASSOCIATION, :reversed,
                              "#{reversed.association.ident.cap_first} with name #{reversed.full_name} appears to be reversed")
          end
        }
        @misplaced_roles.each { |misplaced|
          msgs << Issue.new(:MISPLACED_ROLE_NAME, :misplaced,
                            "#{misplaced.association.ident.cap_first} has a name, #{misplaced.association.displayed_name}, that should be a role (at one of the ends)")
        }
        if @extra_roles.size == 1 && @missing_roles.size == 1
          msgs << Issue.new(:ROLE_MISMATCH, :role_associated_with_wrong_classes,
                            "#{match_ident.cap_first} should be associated with #{@missing_roles[0].supplier_name} instead of #{@extra_roles[0].supplier_name}")
        elsif !@extra_roles.empty? && @extra_roles.size == @missing_roles.size
          extra_names = @extra_roles.map { |r| r.supplier_name }
          missing_names = @missing_roles.map { |r| r.supplier_name }
          msgs << Issue.new(:ROLE_MISMATCH, :role_associated_with_wrong_classes,
                            "#{match_ident.cap_first} has the right number of associations, but should be associated with #{Helper.unique_names(missing_names)} instead of #{Helper.unique_names(extra_names)}")
        else
          if @extra_roles.size == 1
            msgs << Issue.new(:ROLE_MISMATCH, :unexpected_associations,
                              "#{match_ident.cap_first} has an unexpected association with #{@extra_roles[0].supplier_name}")
          elsif @extra_roles.size > 1
            names = @extra_roles.map { |r| r.supplier_name }
            msgs << Issue.new(:ROLE_MISMATCH, :unexpected_associations,
                              "#{match_ident.cap_first} has unexpected associations with #{Helper.unique_names(names)}")
          end
          # rwh, todo: add support for $explicit_errors here
          if @missing_roles.size == 1
            msgs << Issue.new(:ROLE_MISMATCH, :missing_associations,
                              "#{match_ident.cap_first} is missing an association")
          elsif @missing_roles.size > 1
            msgs << Issue.new(:ROLE_MISMATCH, :missing_associations,
                              "#{match_ident.cap_first} is missing #{@missing_roles.size} associations")
          end
        end

        visible_associates.each { |expected_role|
          if expected_role.doppleganger &&
              @reversed_roles.count(expected_role) == 0
            actual_role = expected_role.doppleganger
            identify_multiplicity_differences(expected_role, actual_role, msgs)
            identify_role_differences(expected_role, actual_role, msgs)
          end
        }

        msgs
      end

      :private

      def identify_multiplicity_differences(expected_role, actual_role, msgs)
        expected_multiplicity = expected_role.effective_multiplicity
        actual_multiplicity = actual_role.effective_multiplicity
        if @loose_multiplicity_matching
          expected_multiplicity = '1' if expected_multiplicity == '0..1'
          actual_multiplicity = '1' if actual_multiplicity == '0..1'
        end
        if !expected_multiplicity.empty? &&
            expected_multiplicity != actual_multiplicity
          if $explicit_errors
            if actual_multiplicity.nil? || actual_multiplicity.empty?
              detail = " (none given; expected #{expected_multiplicity})"
            else
              detail = " (#{actual_multiplicity} instead of #{expected_multiplicity})"
            end
          else
            detail = ''
          end
          msgs << Issue.new(:ROLE_MISMATCH, :multiplicity_error,
                            "Error in multiplicity for #{actual_role.ident}" +
                                detail)
        end
      end

      def identify_role_differences(expected_role, actual_role, msgs)
        expected_name = expected_role.name
        actual_name = actual_role.name
        if expected_name && !expected_name.empty? &&
            !expected_role.possible_match?(actual_name) &&
            @misplaced_roles.count(expected_role) == 0
          if $explicit_errors
            if actual_name.nil? || actual_name.empty?
              detail = " (no name given; expected #{expected_name})"
            else
              detail = " (#{actual_name} instead of #{expected_name})"
            end
          else
            detail = ''
          end
          msgs << Issue.new(:ROLE_MISMATCH, :incorrect_role_name,
                            "Incorrect role name for #{actual_role.ident}" +
                                detail)
        end
      end

      :public

      def super_errors
        msgs = []
        super_names = visible_supers.map { |s| s.name }
        doppler_names = @doppleganger.visible_supers.map { |x| x.name }
        if super_names.sort != doppler_names.sort
          if visible_supers.size > 1 && visible_supers.size != doppler_names.size
            # would be rare - expecting multiple inheritance!
            msgs << Issue.new(:SUPER_MISMATCH, :wrong_supers,
                              "Number of super classes for #{match_name} does not match the expected number, #{visible_supers.size}")
          elsif visible_supers.empty?
            msgs << Issue.new(:SUPER_MISMATCH, :no_super,
                              "Class #{match_name} should not be inherited from another class")
          elsif @doppleganger.visible_supers.empty?
            msgs << Issue.new(:SUPER_MISMATCH, :wrong_supers,
                              "Class #{match_name} should be inherited from another class")
          elsif @doppleganger.visible_supers.size > 1 && visible_supers.size > 1
            msgs << Issue.new(:SUPER_MISMATCH, :wrong_supers,
                              "Class #{match_name} should be inherited from other classes")
          elsif @doppleganger.visible_supers.size > 1
            msgs << Issue.new(:SUPER_MISMATCH, :just_one_super,
                              "Class #{match_name} should be inherited from just one class")
          else
            msgs << Issue.new(:SUPER_MISMATCH, :wrong_supers,
                              "Class #{match_name} is inherited from the wrong class")
          end
        end
        msgs
      end

      protected

      # returns [in_a_only, in_b_only] where in_a_only items are those in b_list not
      #   in a_list and in_b_only are those in a_list not in b_list
      #   For every item in both lists, sets .doppler to that item
      # "a_list" is the items for the current object, dopper_list is the
      #   items in the corresponding doppler
      # does not change either list
      # b_list is assumed to be the items in the student's model
      def find_unmatched_elements(a_list, b_list)
        a_list = a_list.clone
        b_list = b_list.clone
        in_a_only = []
        in_b_only = []
        while !b_list.empty?
          b_item = b_list.shift
          matches_from_a = a_list.select { |a| a.possible_match?(b_item.name) }

          if matches_from_a.empty?
            in_b_only << b_item
          else
            a_item = matches_from_a[0]
            b_item.doppleganger = a_item.doppleganger
            a_item.doppleganger = b_item.doppleganger
            a_index = a_list.index(a_item)
            a_list.delete_at(a_index)
          end
        end # while
        # construct return values
        in_b_only.sort!
        in_a_only = a_list.sort # remaining items in a
        [in_a_only, in_b_only]
      end

    end # MClass extensions
  end # module Model

# captures differences between two model files
  class LogicalDiff
    # types: Model, Model, LogicalModel, LogicalModel
    attr_reader :target_model, :submission_model, :target_cd, :submisson_cd
    # types: [MClass], [MClass], [ClassMatch]
    attr_reader :missing_classes, :extra_classes, :matching_classes
    attr_reader :loose_multiplicity_matching

    # assumption: m1 is baseline
    def initialize(a_target, a_submission)
      @target_model = a_target
      @submission_model = a_submission
      @target_cd = a_target.logical_model
      @submission_cd = a_submission.logical_model

      @loose_multiplicity_matching =
          !@target_model.format_supports_detailed_multiplicities? ||
              !@submission_model.format_supports_detailed_multiplicities?

      @missing_classes = []
      @extra_classes = []
      @matching_classes = []

      set_class_matches
    end

    def errors
      msgs = []
      if @missing_classes.size == 1
        msgs << Issue.new(:CLASS_MISMATCH, :missing_classes,
                          "Missing class: #{Helper.names(@missing_classes)}")
      elsif @missing_classes.size > 1
        msgs << Issue.new(:CLASS_MISMATCH, :missing_classes,
                          "Missing classes: #{Helper.names(@missing_classes)}")
      end
      if @extra_classes.size == 1
        msgs << Issue.new(:CLASS_MISMATCH, :unexpected_classes,
                          "#{@extra_classes[0].ident.cap_first} should not be in the diagram")
      elsif @extra_classes.size > 1
        msgs << Issue.new(:CLASS_MISMATCH, :unexpected_classes,
                          "Classes that should not be in the diagram: #{Helper.names(@extra_classes)}")
      end

      @target_model.logical_model.visible_classes.each { |target_class|
        if target_class.doppleganger
          errors = target_class.mismatches
          msgs += errors
        end
      }
      msgs
    end

    private

    def set_class_matches
      all_classes = @target_model.logical_model.classes +
          @submission_model.logical_model.classes
      all_classes.each { |c|
        c.loose_multiplicity_matching = @loose_multiplicity_matching
      }
      target_classes = @target_model.logical_model.visible_classes
      @submission_model.logical_model.visible_classes.each { |submitted|
        matches = target_classes.select { |sc|
          sc.doppleganger.nil? && sc.matches_by_name?(submitted.name)
        }
        while !matches.empty?
          first = matches.shift
          if first.doppleganger.nil?
            first.doppleganger = submitted
            submitted.doppleganger = first
            matches = []
          end
        end
        if submitted.class? && !submitted.has_name?
          # rwh, sep 2016: added this case; diagram notes are unnamed classes
        elsif submitted.doppleganger.nil?
          @extra_classes << submitted
        end
      }
      @missing_classes = target_classes.select { |tc|
        tc.doppleganger.nil?
      }
      # now set role matches for each class - must be after setting
      #   class dopplegangers
      target_classes = @target_model.logical_model.visible_classes
      target_classes.each { |tc|
        tc.set_strong_association_matches! if tc.doppleganger
      }
      target_classes.each { |tc|
        tc.set_reversed_association_matches! if tc.doppleganger
      }
      target_classes.each { |tc|
        tc.set_misplaced_role_matches! if tc.doppleganger
      }
      target_classes.each { |tc|
        tc.match_on_multiplicities! if tc.doppleganger
      }
      target_classes.each { |tc|
        tc.set_singleton_association_matches! if tc.doppleganger
      }
      target_classes.each { |tc|
        tc.set_mismatches! if tc.doppleganger
      }
    end

  end
end
