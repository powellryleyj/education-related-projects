module UmlTools
#!/usr/bin/env ruby
# before optimizations (macbook): 20.486, 20.561, 20.610
# Read model files as generated by Rational Rose and convert them into a
# collection of models.
# When used as a standalone program, dumps the model file as a lisp expression.
# Usage: model.rb _file_
#

require_relative 'sexpr'
require_relative 'model/full_model'

require 'rexml/document'

# Module for processing Rational Rose model files
module Model

  $generated_ids = 0

end

######################################################################

$debug_petal = false

# list(string) -> int
def self.petal_main(args)
  if args[0] == '--help' || args[0] == '-?'
    # note -f is also allowed for check.rb, but not for umlint
    #   -f sets the printable file name for the purposes of error messages
    #   but is only useful when executed from umlint shell script
    puts 'Usage: umldump [-d] [-i] [-m] [files...]'
    puts '  -d: debug mode: more detailed info when not a model file'
    puts '  -i: print element id numbers'
    puts '  -m: print metrics'
    puts '  -p: force processing as model file'
    puts '  -x: force processing as xmi file'
    puts '  If file not specified or -, reads from standard input'
    return 1
  end
  print_metrics = false
  file_format = nil
  #puts "Arguments: #{args.join ' '}"
  while args[0] =~ /^-/
    if args[0] == '-i'
      Model::Element.print_ids = true
    elsif args[0] == '-I'
      Model::Element.print_just_ids = true
    elsif args[0] == '-m'
      print_metrics = true
    elsif args[0] == '-p'
      file_format = :model
    elsif args[0] == '-x'
      file_format = :xmi
    end
    args.shift
  end
  
  model_file = args[0]
  
  begin
    model = Model::FullModel.load(model_file, file_format)
    puts model.dump
  rescue
    puts "Error: #{model_file} does not appear to contain a UML model"
    return 2
  end
  
  
  0
end

if __FILE__ == $0
  
  exit(petal_main(ARGV))
  
end

end # end module
