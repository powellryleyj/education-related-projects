module UmlTools
# Store arbitrary s-expressions, where an s-expression is either an atom
# or a list of s-expressions.
#
# Author::      Rob Hasker (hasker@uwplatt.edu)
# Copyright:    Copyright (C) 2010 Robert W. Hasker

#require 'ruby-debug'

# Base type for all s-expressions; either atoms or lists of s-expressions
  class SExpr

    # Does SExpr represent a list?
    def list?
      false
    end

    # Does SExpr represent an atom?
    def atom?
      false
    end

    # Does SExpr match target_str (a string)?  Always false for lists.
    def is?(target_str)
      false
    end

    # Is SExpr a list with +target1+ and +target2+ as the first two atoms?
    # +target1, 2+: Strings
    # +target2+ can be nil, in which case only target1 is examined
    # SExpr.matches? handles atoms by checking that target1 is a match and
    # target2 is nil
    def matches?(target1, target2)
      target2.nil? && is?(target1)
    end

    # Given test, dives into SExpr until find SExpr for which test returns
    # true; returns an empty list if no item can be found
    # Type: block(SExpr -> bool) -> array(SExpr)
    def select(&test)
      if yield(self)
        [self]
      else
        []
      end
    end

    # Given test, dives into SExpr to find all elements satisfying test.
    # type: block(SExpr -> bool) -> list[SExpr]
    # like select, but searches children nodes as well when a match is found
    def select_all(&test)
      if yield(self)
        [self]
      else
        []
      end
    end

    # search down path of named nodes (length of path = #layers searched) and
    #   return next sibling after last entry in path:
    # Does not skip levels - each element of the path must match the
    #   corresponding level.
    # type: array(String) -> SExpr | nil
    def find_by_path(path)
      if path.empty?
        self
      else
        nil
      end
    end

    # Like find_by_path, but can skip levels to find matches and allows for
    # multiple items matching the path
    # type: array(String) -> list(SExpr)
    def find_all_by_path(path)
      if path.empty?
        [self]
      else
        []
      end
    end

    # Read SExpr from lib
    # type: String -> SExpr
    def self.read(src)
      parts = tokenize(src)
      #puts "PROCESSING #{parts.join('| ')}"
      if parts.empty?
        result = nil
      elsif parts[0] == '('
        result = SList.read(parts)
      else
        result = SAtom.read(parts)
      end
      if !parts.empty?
        result = [result]
        while !parts.empty?
          if parts[0] == '('
            result << SList.read(parts)
          else
            result << SAtom.read(parts)
          end
        end
        result = SList.new(result)
      end
      return result
    end

    private

    # is given character a whitespace character?
    def self.space?(char)
      char == ?\s || char == ?\t || char == ?\n || char == ?\r
    end

    # Tokenize string into list of strings
    # type: String -> Array(String)
    def self.tokenize(src)
      words = []
      i = 0
      while i < src.size
        #puts "Processing #{i}: #{lib[i..i]}"
        if src[i] == ?(
          words << '('
        elsif src[i] == ?)
          words << ')'
        elsif src[i] == ?"
          start = i
          i += 1
          while i < src.size && src[i] != ?"
            i += 1
          end
          words << src[start..i]
        elsif src[i] == ?| && (i == 0 || src[i - 1] == ?\n)
          start = i + 1 # skip first |
          i += 1
          while i < src.size - 1 &&
              (src[i] != ?\n || src[i] == ?\n && src[i + 1] == ?| ||
                  # some DOS files have \r\n\n terminators:
                  i < src.size - 2 && src[i] == ?\n && src[i + 1] == ?\n && src[i + 2] == ?|)
            i += 1
          end
          item = src[start..i].gsub(/\r\n/, "\n")
          item = item.gsub(/\n\|/, "\n").chomp('|').chomp
          words << item
        elsif !space?(src[i])
          start = i
          i += 1
          while i < src.size && !space?(src[i]) && src[i] != ?( && src[i] != ?)
            #puts "skipping past #{i}: #{lib[i..i]}"
            i += 1
          end
          i -= 1 # went one too far
          words << src[start..i]
        end
        i += 1
      end
      return words
    end
  end

# List of SExprs, treated as a single SExpr
  class SList < SExpr

    # Array(SExpr): array of s-expressions
    attr_reader :items

    # Initialize SList from list of SExprs; defaults to empty list
    def initialize(items = [])
      @items = items
    end

    def list?
      true
    end

    # Whether list is empty
    def empty?
      @items.empty?
    end

    # Does list have head with target1, second with target2
    # If target2 is nil, always returns nil
    def matches?(target1, target2)
      target2 && @items[1] && @items[0].is?(target1) && @items[1].is?(target2)
    end

    # Return first atom of list, or nil if list is empty
    def head
      return nil if @items.empty?
      @items[0]
    end

    # Return head or nil if list is empty
    def first
      head
    end

    # return second item in list or nil if list has less than 2 items
    def second
      return nil if @items.size < 2
      @items[1]
    end

    # return third item in list or nil if list has less than 3 items
    def third
      return nil if @items.size < 3
      @items[2]
    end

    # See SExpr.select
    # block(SExpr -> bool) -> list[SExpr]
    def select(&test)
      if yield(self)
        [self]
      else
        subs = []
        @items.each { |i|
          sub = i.select(&test)
          subs += sub
        }
        subs
      end
    end

    # See SExpr.select_all
    # block(SExpr -> bool) -> list[SExpr]
    def select_all(&test)
      result = []
      if yield(self)
        result = [self]
      end
      @items.each { |i|
        sub = i.select_all(&test)
        result += sub
      }
      result
    end

    # See SExpr.find_by_path
    def find_by_path(path)
      if path.empty?
        self
      else
        @items.each_index { |i|
          if @items[i].head == path[0] && i < @items.length - 1
            return @items[i + 1].find_by_path(path.last(path.size - 1))
          end
        }
        nil
      end
    end

    # See SExpr.find_all_by_path
    def find_all_by_path(path)
      if path.empty?
        [self]
      else
        result = []
        @items.each_index { |i|
          if @items[i].head == path[0] && i < @items.length - 1
            subs = @items[i + 1].find_all_by_path(path.last(path.size - 1))
          else
            subs = @items[i].find_all_by_path(path)
          end
          result += subs
        }
        result
      end
    end

    # Returns comma-separated string version of list
    def to_s
      '[' + @items.join(', ') + ']'
    end

    # Read SList from string lib
    # type: String -> SList
    def self.read(src)
      if src[0] == '('
        items = []
        #puts "read1 '#{lib[0]}'"
        src.shift
        while src[0] != ')'
          #puts "scanning '#{lib[0]}'"
          if src[0] == '('
            items << SList.read(src)
          else
            items << SAtom.read(src)
          end
        end
        #puts "read3 '#{lib[0]}'"
        src.shift
        return SList.new(items)
      end
    end
  end

# An atom in an SExpr
  class SAtom < SExpr

    # String: text of atom
    attr_reader :name

    # Initialize SAtom from string +at+
    # If +at+ starts with a quote, strips of quote at start and end
    def initialize(at)
      raise 'Unexpected nil atom' if at.nil?
      if at.size > 1 && at[0] == ?" || at[-1] == ?"
        @name = at[1..-2]
      else
        @name = at
      end
    end

    # SAtom.atom? - always true
    def atom?
      true
    end

    # Returns name of atom
    def head
      @name
    end

    # Returns whether target_str matches name of atom
    def is?(target_str)
      @name == target_str
    end

    # Returns name of atom as a string
    def to_s
      @name.to_s
    end

    # Reads SAtom from head of lib, dropping head from the list
    # type: list(string) -> SAtom
    def self.read(src)
      SAtom.new(src.shift)
    end
  end
end
