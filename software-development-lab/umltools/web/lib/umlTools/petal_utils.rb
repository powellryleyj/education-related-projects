module UmlTools
# petal_utils.rb: model file processing utilities

  require 'set'

# return how words are in an identifier, splitting on camel case and _'s
  def self.words_in_identifier(ident)
    caps = []
    ident.size.times { |i| caps << i if ident[i] >= ?A && ident[i] <= ?Z }
    caps = [0] + caps if caps.empty? || caps[0] != 0
    ranges = caps.zip(caps.empty? ? [] :
                          caps.last(caps.size - 1).map { |i| i - 1 })
    words = []
    ranges.each { |start, count| words << ident[start..(count || ident.size)] }
    words.map! { |word| word.split(/[ _]/) }
    words.flatten
  end


# standard library classes for various languages
  class LibraryClass
    # types: string, :cpp|:java, int, Set(string)
    attr_reader :name, :language, :expected_type_parameters, :methods

    # language: :cpp, :java
    def initialize(n, l, expected_params, ms)
      @name = n
      @language = l
      @expected_type_parameters = expected_params
      @methods = Set.new(ms)
    end

    # return library for given name, or nil if none found
    def self.find(name)
      return nil if name.nil?
      word = name.split('<')[0].strip
      @@libs[word]
    end

    def self.in_stl?(class_name)
      lib = self.find(class_name)
      lib && lib.language == :cpp
    end

    def self.in_java_class_library?(class_name)
      lib = self.find(class_name)
      lib && lib.language == :java
    end

    # returns elements in container<xyz, pdq> as a list of strings
    #   if there are no brackets, returns nil
    #   if there are brackets but there's nothing in between, returns []
    #   Note: if have nested templates, those will be returned in pieces
    def self.type_parameters(name)
      classname, bracket, elts = name.delete(' ').partition('<')
      if elts.empty?
        return nil # no <>'s
      else
        elts = elts[0..-2] if elts[-1] == ?>
        # now, parse out the fields, but handle nested <>s
        res = []
        last_start = 0
        depth = 0
        elts.size.times { |i|
          if elts[i] == ?, && depth <= 0
            res << (i == 0 ? '' : elts[last_start..(i-1)])
            last_start = i + 1
          elsif elts[i] == ?<
            depth += 1
          elsif elts[i] == ?>
            depth -= 1
          end
        }
        #puts "Last start for #{elts}: #{last_start} (#{last_start <= elts.size})"
        #puts "res before: #{res}"
        res << elts[last_start..-1] if last_start < elts.size
        res << '' if elts[-1] == ?,
        #puts "res after: #{res}"
        return res
      end
    end

    # register all libraries - must be called once
    def self.setup
      java_collection_methods =
          %w(add addAll clear contains containsAll equals hashCode isEmpty iterator remove removeAll retainAll size toArray)
      java_list_methods = %w(iterator listIterator)
      java_vector_methods =
          %w(add addAll addElement capacity clear clone contains containsAll copyInto elementAt elements ensureCapacity equals firstElement get hashCode indexOf insertElementAt isEmpty lastElement lastIndexOf remove removeAll removeAllElements removeElement removeElementAt removeRange retainAll set setElementAt setSize size subList toArray toString trimToSize)

      @@libs = {
          'deque' =>
              LibraryClass.new('deque', :cpp, 1,
                               %w(begin end rbegin rend empty size max_size resize operator[] operator= at front back assign push_back push_front pop_back pop_front insert erase swap clear)),
          'list' =>
              LibraryClass.new('list', :cpp, 1,
                               %w(begin end rbegin rend empty size max_size resize front back assign push_front pop_front push_back pop_back insert erase swap clear splice remove remove_if unique merge sort reverse)),
          'map' =>
              LibraryClass.new('map', :cpp, 2,
                               %w(begin end rbegin rend empty size max_size key_comp value_comp operator= swap insert erase clear find count lower_bound upper_bound equal_range operator[] operator== operator<)),
          'queue' =>
              LibraryClass.new('queue', :cpp, 1,
                               %w(empty size front back push pop)),
          'set' =>
              LibraryClass.new('set', :cpp, 1,
                               %w(begin end rbegin rend size max_size key_comp value_comp operator= swap insert erase clear find count lower_bound upper_bound equal_range operator== operator<)),
          'stack' =>
              LibraryClass.new('stack', :cpp, 1,
                               %w(empty size top push pop)),
          'vector' =>
              LibraryClass.new('vector', :cpp, 1,
                               %w(begin end rbegin rend empty size max_size capacity operator[] operator= reverse front back push_back pop_back swap insert erase clear resize operator== operator<)),
          'List' =>
              LibraryClass.new('List', :java, 1,
                               %w(add addAll clear contains containsAll equals get hashCode indexOf isEmpty iterator lastIndexOf listIterator remove removeAll retainAll set size subList toArray)),
          'Map' =>
              LibraryClass.new('Map', :java, 2,
                               %w(clear containsKey containsValue entrySet equals get hashCode isEmpty keySet put putAll remove size values)),
          'Queue' =>
              LibraryClass.new('Queue', :java, 1,
                               java_collection_methods +
                                   %w(element offer peek poll remove)),
          'Set' =>
              LibraryClass.new('Set', :java, 1,
                               %w(add addAll clear contains containsAll equals hashCode isEmpty iterator remove removeAll retainAll size toArray)),
          'Stack' =>
              LibraryClass.new('Stack', :java, 1,
                               java_vector_methods +
                                   %w(empty peek pop push search) +
                                   java_list_methods),
          'Vector' =>
              LibraryClass.new('Vector', :java, 1,
                               java_vector_methods + java_list_methods)
      }
    end

  end

  LibraryClass.setup

  class ProgrammingLanguage

  end # ProgrammingLanguage

end
