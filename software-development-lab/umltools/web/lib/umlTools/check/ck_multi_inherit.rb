#!/usr/bin/env ruby
# check for multiple inheritance

require_relative '../petal'
require_relative '../check'
require_relative 'model_checker'

module UmlTools
  module Check
    class CheckMultiInheritance < ModelChecker

      def check()

        # check for a class inheriting from two others - probably indicates
        #   a backwards generalization
        bad = @logmod.classes.select { |c| c.supers.length > 1 }
        bad.map { |c|
          super_names = c.supers.map { |s| s.nonempty_name }
          Issue.new(:MULTIGEN, :multiple_inheritance,
                    "Class #{c.nonempty_name} inherits from multiple classes (#{super_names.join(', ')}) - generalization arrows may be backwards")
        }
      end

    end
  end
end
