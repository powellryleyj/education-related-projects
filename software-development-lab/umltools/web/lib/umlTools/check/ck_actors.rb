#!/usr/bin/env ruby
# check for problems with actors

require_relative '../petal'
require_relative '../check'
require_relative 'model_checker'
require 'set'
#require 'ruby-debug'

module UmlTools
  module Check
    class CheckActors < ModelChecker

      def check
        messages = []

        unassociated = @ucmod.visible_actors.select { |a|
          a.visible_associates.select { |x| x.supplier && x.supplier.use_case? }.empty?
        }

        # pull out items with supers that are actors
        unassociated.delete_if { |a|
          actor_supers = a.supers.select { |x| x.actor? }
          !actor_supers.empty?
          # old way:
          #       supers = @model.visible_associable_elements.select {
          #         |other| other.supers.include? a
          #       }
          #       !supers.empty?
        }

        unassociated = unassociated.map { |x| x.nonempty_name }
        unassociated.sort!

        if unassociated.size == 1
          messages << Issue.new(:UC_BAD_ACTOR, :unassociated_actor,
                                "Actor #{unassociated[0]} is not associated with any use case and may be unnecessary")
        elsif unassociated.size > 1
          messages << Issue.new(:UC_BAD_ACTOR, :unassociated_actor,
                                "Actors #{unassociated.join(', ')} are not associated with any use cases and may be unnecessary")
        end

        messages
      end

    end
  end

end
