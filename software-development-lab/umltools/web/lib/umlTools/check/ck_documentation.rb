#!/usr/bin/env ruby
# check for documentation in classes, attributes, operations, and use cases

require_relative '../petal'
require_relative '../check'
require_relative 'model_checker'

module UmlTools
  module Check
    class CheckDocumentation < ModelChecker

      def check()
        messages = []

        # unless the user exports it, xmi formats as exported by Rhapsody
        #   have no documentation, but in the right format it does. This
        #   check is here to allow for future types
        unless @model.format_supports_documentation?
          return []
        end

        wo_doc = @ucmod.visible_actors.select { |x| !x.has_doc? }
        if wo_doc.size == 1
          messages << Issue.new(:NODOC, :missing_actor_documentation,
                                "Missing documentation: #{wo_doc[0].ident}")
        elsif wo_doc.size > 1
          wo_doc.map! { |c| c.nonempty_name }
          messages << Issue.new(:NODOC, :missing_actor_documentation,
                                "Missing documentation: actors #{wo_doc.join(', ')}")
        end

        uc_wo_doc = @ucmod.visible_use_cases.select { |x| !x.has_doc? }
        if uc_wo_doc.size == 1
          messages << Issue.new(:NODOC, :missing_use_case_documentation,
                                "Missing documentation: #{uc_wo_doc[0].ident}")
        elsif uc_wo_doc.size > 1
          uc_wo_doc.map! { |c| c.nonempty_name }
          messages << Issue.new(:NODOC, :missing_use_case_documentation,
                                "Missing documentation: use cases #{uc_wo_doc.join(', ')}")
        end

        # find all classes without documentation, skipping invisible ones and
        #   ones that are standard library classes
        classes_wo_doc = @logmod.visible_classes.select { |c|
          !c.has_doc? && !c.in_library?
        }
        if classes_wo_doc.size == 1
          messages << Issue.new(:NODOC, :missing_class_documentation,
                                "Missing documentation: #{classes_wo_doc[0].ident}")
        elsif classes_wo_doc.size > 1
          classes_wo_doc.map! { |c| c.nonempty_name }
          messages << Issue.new(:NODOC, :missing_class_documentation,
                                "Missing documentation: classes #{classes_wo_doc.join(', ')}")
        end

        # find undocumentated attributes, methods, but skip ones which are standard
        #   library methods
        @logmod.classes.map { |c|
          as = c.attributes.select { |e| !e.has_doc? && !e.in_library_class? }
          ms = c.methods.select { |e| !e.has_doc? && !e.in_library_class? }
          problems = (as + ms).map { |x| x.nonempty_name }
          if problems.size == 1
            messages << Issue.new(:NODOC, :missing_member_documentation,
                                  "Missing documentation for #{c.ident} member: " +
                                      problems.join(', '))
          elsif problems.size > 1
            messages << Issue.new(:NODOC, :missing_member_documentation,
                                  "Missing documentation for #{c.ident} members: " +
                                      problems.join(', '))
          end
        }

        messages
      end

    end
  end

end
