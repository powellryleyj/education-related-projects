#!/usr/bin/env ruby
# check for hidden elements (not visible in any view)

require_relative '../petal'
require_relative '../check'
require_relative 'model_checker'

module UmlTools
  module Check
    class CheckHiddenElements < ModelChecker

      def check
        check_use_case_model + check_logical_model
      end

      def check_use_case_model
        messages = []

        hidden_actors = @ucmod.actors.select { |x| x.hidden? }
        hidden_names = hidden_actors.map { |x| x.nonempty_name }.join(', ')
        if !hidden_actors.empty?
          # would like to use ? operator here, but rcov gives the wrong results
          if hidden_actors.size == 1
            messages << Issue.new(:INVISIBLE, :invisible_actors,
                                  "Actor #{hidden_names} is not visible in any view")
          else
            messages << Issue.new(:INVISIBLE, :invisible_actors,
                                  "Actors #{hidden_names} are not visible in any view")
          end
        end

        hidden_use_cases = @ucmod.use_cases.select { |x| x.hidden? }
        hidden_names = hidden_use_cases.map { |x| x.nonempty_name }.join(', ')
        if !hidden_use_cases.empty?
          # would like to use ? operator here, but rcov gives the wrong results
          if hidden_use_cases.size == 1
            messages << Issue.new(:INVISIBLE, :invisible_use_cases,
                                  "Use case #{hidden_names} is not visible in any view")
          else
            messages << Issue.new(:INVISIBLE, :invisible_use_cases,
                                  "Use cases #{hidden_names} are not visible in any view")
          end
        end

        messages
      end

      def check_logical_model
        messages = []

        hidden = @logmod.hidden_classes
        hidden_names = hidden.map { |x| x.nonempty_name }.join(', ')
        if !hidden.empty?
          messages << Issue.new(:INVISIBLE, :invisible_classes,
                                ((hidden.size == 1) ?
                                    'The following class is ' :
                                    'The following classes are ') +
                                    'not visible in any view: ' + hidden_names)
        end

        messages
      end

    end
  end

end
