#!/usr/bin/env ruby
# check for use cases that have problems

require_relative '../petal'
require_relative '../check'
require_relative 'model_checker'
require 'set'
#require 'ruby-debug'

module UmlTools
  module Check
    class CheckUseCases < ModelChecker

      def check
        messages = []

        #
        # Definition of backwards include:
        #  * uc_i is _reachable_ from actor a if a---uc_1 and there is a path
        #    of dependencies (in either direction) from uc_1 to uc_i
        #    following valid directions for arrow (away from actor for
        #    include, towards actor for extend)
        #  * uc_a <--<<include>>-- uc_b should be uc_a --<<include>>--> uc_b
        #    if uc_a is reachable from some actor but uc_b is not
        #
        #  That is, we're looking for
        #    actor ------- included <--<<include>>---- u
        #  (so u is on the dependency_list for included)
        #  and telling them to change it to
        #    actor ------- dependency ---<<include>>---> u
        @ucmod.visible_use_cases.each { |u|
          if !u.reachable_from_actor?
            u.uses.each { |dependency|
              if dependency.stereotype && dependency.stereotype == 'include' &&
                  dependency.supplier && dependency.supplier.use_case? &&
                  dependency.supplier.reachable_from_actor?
                messages << Issue.new(:UC_REV_INCL, :backwards_include,
                                      "<<include>> dependency from #{u.nonempty_name} to #{dependency.supplier.nonempty_name} may be backwards")
              end
            }
          end
        }

        #
        # Definition of backwards extend:
        #  * uc_a --<<extend>>--> uc_b should be uc_a <--<<extend>>-- uc_b
        #    if uc_a is reachable from some actor but uc_b is not
        # Note that @extensions gives those use cases which are
        #  extensions of the given case, so in the following code
        #  we're looking for
        #    actor ------- u ---<<extend>>---> dependency
        #  and telling them to change it to
        #    actor ------- u <--<<extend>>---- dependency
        #
        @ucmod.visible_use_cases.each { |u|
          if u.reachable_from_actor?
            u.uses.each { |dependency|
              if dependency.stereotype && dependency.stereotype == 'extend' &&
                  dependency.supplier && dependency.supplier.use_case? &&
                  !dependency.supplier.reachable_from_actor?
                messages << Issue.new(:UC_REV_EXT, :backwards_extends,
                                      "<<extend>> dependency from #{u.nonempty_name} to #{dependency.supplier.nonempty_name} may be backwards")
              end
            }
          end
        }

        # check for generalizations
        @ucmod.visible_use_cases.each { |u|
          u.supers.each { |sup|
            messages << Issue.new(:UC_BAD_ASSOCIATION,
                                  :generalization_between_use_cases,
                                  "Generalization relationships between use cases (#{u.nonempty_name} and #{sup.nonempty_name}) is not allowed")
          }
        }

        # check for use cases with a single word in the name
        single_word_cases =
            @ucmod.visible_use_cases.select {
                |u| u.name && !u.name.empty? && UmlTools.words_in_identifier(u.name).size <= 1
            }
        single_word_cases = single_word_cases.map { |u| u.name }
        if single_word_cases.size == 1
          messages << Issue.new(:UC_NAME, :single_word_name,
                                "A single word was used for the use case #{single_word_cases[0]} - include both a verb and object")
        elsif single_word_cases.size > 1
          messages << Issue.new(:UC_NAME, :single_word_name,
                                "A single word was used for the use cases #{single_word_cases.join(', ')} - include both a verb and object for each")
        end

        # check for use cases that are very removed from the actor - this
        #  generally indicates a misuse of class diagrams
        distant_use_cases = @ucmod.visible_use_cases.select { |u|
          u.actor_distance > 3
        }
        distant_use_cases = distant_use_cases.map { |u| u.name }
        distant_use_cases.sort!
        if !distant_use_cases.empty?
          messages << Issue.new(:REMOTE_USE_CASE, :more_than_3_removed,
                                "Use case(s) #{distant_use_cases.join(', ')} more than 3 links from an actor")
        end

        messages
      end

    end
  end
end
