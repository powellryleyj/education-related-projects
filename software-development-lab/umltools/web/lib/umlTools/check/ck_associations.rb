#!/usr/bin/env ruby
# check for improper associations in logical models

require_relative '../petal'
require_relative '../check'
require_relative 'model_checker'
require 'set'
#require 'ruby-debug'

module UmlTools
  module Check
    class CheckAssociations < ModelChecker

      def check
        msgs1 = check_uc_model
        msgs2 = check_logical_model
        msgs1 + msgs2
      end

      def check_uc_model

        messages = []

        # check for labeled or stereotyped actor-use case associations
        bad_assocs = @ucmod.visible_associations.select { |assoc|
          assoc.associates_actor_and_use_case? &&
              # note: a role and b role both exist if get to this point
              has_stereotype = assoc.stereotype && !assoc.stereotype.empty?
          # bad associations has a name somewhere
          has_stereotype || assoc.has_named_label?
        }
        bad_assocs.each { |a|
          messages << Issue.new(:UC_BAD_ASSOCIATION, :no_stereotypes_or_names,
                                "#{a.ident.cap_first} is either marked with a stereotype or has a name; such associations must be unmarked")
        }

        # check for associations with include, extend on them - these should be
        #  dependencies instead. Ones between actors and use cases are already
        #  handled above
        bad_assocs = @ucmod.visible_associations.select { |assoc|
          !assoc.associates_actor_and_use_case? &&
              (assoc.stereotype == 'include' || assoc.stereotype == 'extend')
        }
        bad_assocs.each { |a|
          messages << Issue.new(:UC_BAD_STEREOTYPE, :make_dependency,
                                "#{a.ident.cap_first} is marked as <<extend>> or <<include>>; change to a dependency")
        }

        # check for associations not connecting actors (or classes) to use cases
        bad_assocs = @ucmod.visible_associations.select { |assoc|
          assoc.has_both_suppliers? && !assoc.associates_actor_and_use_case?
          # ignore associations without both suppliers
        }
        bad_assocs.each { |a|
          messages << Issue.new(:UC_BAD_ASSOCIATION, :use_cases_associate_to_actors,
                                "Invalid #{a.ident}: associate use cases to actors only")
        }

        # check for associations between regular classes and use cases
        #   these indicate classes that do not belong on the use case diagram
        bad_assocs = @ucmod.visible_associations.select { |assoc|
          assoc.associates_regular_class_and_use_case?
        }
        bad_assocs.each { |a|
          # note: a_role and b_role both exist
          supplier_role = a.a_role.supplier.class? ? a.a_role : a.b_role
          messages << Issue.new(:UC_BAD_ACTOR, :class_associated_to_use_case,
                                "#{supplier_role.supplier.ident.cap_first} is not an actor and should not appear on a use case diagram")
        }

        # check for associations with bad role elements
        bad_assocs = @ucmod.visible_associations.select { |assoc|
          assoc.bad_roles.size > 0
        }
        bad_assocs.each { |a|
          messages << Issue.new(:BAD_ASSOC_ROLE, :reference_to_missing_items,
                                "#{a.ident.cap_first} contains references to missing items")
        }

        # check for navigable associations; skip include/extend because
        #  that's covered in the previous check
        navigable_assocs = @ucmod.visible_associations.select { |assoc|
          # both must be navigable or neither navigable
          (assoc.a_role.navigable? != assoc.b_role.navigable?) &&
              assoc.stereotype != 'include' && assoc.stereotype != 'extend'
        }
        navigable_assocs.each { |a|
          messages << Issue.new(:NAVIGABLE_ASSOC, :make_undirected,
                                "#{a.ident.cap_first} is navigable; change to undirected association")
        }

        # check for associations with multiplicities
        bad_assocs = @ucmod.visible_associations.select { |assoc|
          !assoc.a_role.missing_multiplicity? || !assoc.b_role.missing_multiplicity?
        }
        bad_assocs.each { |a|
          messages << Issue.new(:UC_HAS_MULTIPLICITY, :unexpected_multiplicity,
                                "#{a.ident.cap_first} has multiplicity and should not")
        }

        # check for dependencies
        elements_with_deps = @ucmod.visible_elements.select { |e|
          e.uses && !e.uses.empty?
        }
        elements_with_deps.each { |e|
          e.uses.each { |u|
            if u.name && !u.name.empty?
              messages << Issue.new(:UC_USES, :no_named_dependencies,
                                    "Dependencies such as between #{e.nonempty_name} and #{u.supplier_name} cannot be named; use <<extend>> and <<include>>")
            elsif u.stereotype.nil?
              messages << Issue.new(:UC_USES, :no_plain_dependencies,
                                    "Plain dependencies such as between #{e.nonempty_name} and #{u.supplier_name} should not appear in use case diagrams")
            elsif u.stereotype != 'extend' && u.stereotype != 'include'
              messages << Issue.new(:UC_USES, :illegal_dependency_stereotypes,
                                    "Illegal dependency between #{e.nonempty_name} and #{u.supplier_name}; must use either <<extend>> or <<include>>")
            end
          }
        }

        # check for use cases with no associations (and not involved in
        #   generalization) - shouldn't have unassociated elements in the model
        # Note: actors are not included here because they can have associations
        #   in the class model that hide errors in the use case model
        in_assoc = @ucmod.visible_associations.map { |assoc|
          assoc.a_role && assoc.a_role.supplier
        }
        in_assoc += @ucmod.visible_associations.map { |assoc|
          assoc.b_role && assoc.b_role.supplier
        }
        not_in_assoc = Set.new(@ucmod.visible_use_cases) - Set.new(in_assoc)
        # refined by deleting all which have actors associated with them
        not_in_assoc.delete_if { |c| c.use_case? && c.reachable_from_actor? }
        messages += not_in_assoc.map { |e|
          Issue.new(:WIDOWELEMENT, :unassociated_use_case,
                    "#{e.ident.cap_first} is not associated with anything and may be unnecessary")
        }

        # find duplicate associations
        all_associations = @ucmod.visible_associations.map { |a|
          a.ident
        }
        all_associations.sort!
        duplicated_associations = []
        all_associations.each_index { |i|
          if all_associations[i] == all_associations[i + 1]
            duplicated_associations << all_associations[i]
          end
        }
        duplicated_associations.uniq!
        duplicated_associations.map { |a|
          messages << Issue.new(:UC_BAD_ASSOCIATION, :duplicated_association,
                                "#{a.cap_first} is a duplicated association - remove the extra association")
        }

        messages
      end

      def check_logical_model

        # note: multiplicity check initializes messages, so must be first!

        # check for composites with multiplicities other than 1 -
        #   composite expresses ownership and there can be only one owner
        # Skip roles without suppliers - these may be from use case models
        bad_multiplicity = @logmod.visible_associations.select { |assoc|
          assoc.a_role.supplier && assoc.b_role.supplier &&
              (bad_composition?(assoc.a_role) || bad_composition?(assoc.b_role))
        }
        messages = bad_multiplicity.map { |assoc|
          Issue.new(:COMPMUL1, :expected_multiplicity_1,
                    "#{assoc.ident.cap_first}: composition should have multiplicity 1")
        }

        # check for associations with bad role elements
        bad_assocs = @logmod.visible_associations.select { |assoc|
          assoc.bad_roles.size > 0
        }
        bad_assocs.each { |a|
          messages << Issue.new(:BAD_ASSOC_ROLE, :reference_to_missing_items,
                                "#{a.ident.cap_first} contains references to missing items")
        }

        # check for associations with bad role elements
        bad_assocs = @logmod.visible_associations.select { |assoc|
          assoc.directed? && assoc.named_role_at_source?
        }
        bad_assocs.each { |a|
          messages << Issue.new(:INVALID_USE_OF_ROLES, :role_name_at_source,
                                "#{a.ident.cap_first} has a role name at the wrong end")
        }

        # check for dynamic relationships
        classes_with_uses = @logmod.visible_classes.select { |c|
          if c.uses.nil? || c.uses.empty?
            false # no uses relationship
          elsif c.uses.size == 1 && c.uses[0].supplier &&
              c.uses[0].supplier.stereotype == 'interface'
            false # has uses, but just a dependency on an interface
          else
            true # a dynamic relationship
          end
        }
        classes_with_uses.each { |c|
          if c.uses.size == 1
            used = "a dynamic relationship with #{c.uses[0].supplier_name}"
          else
            used = c.uses.map { |x| x.supplier_name }
            used = "dynamic relationships with #{used.join(', ')}"
          end
          messages << Issue.new(:DYNAMIC, :no_dynamic_associations,
                                "#{c.ident.cap_first} has #{used} - documenting these is rarely important")
        }

        # check for classes with no associations (and not involved in
        #   generalization) - shouldn't have unassociated classes in the model
        in_assoc = @logmod.visible_associations.map { |assoc|
          assoc.a_role && assoc.a_role.supplier
        }
        in_assoc += @logmod.visible_associations.map { |assoc|
          assoc.b_role && assoc.b_role.supplier
        }
        not_in_assoc = Set.new(@logmod.visible_classes) - Set.new(in_assoc)
        not_in_assoc.delete_if { |c|
          !c.supers.empty? || c.stereotype == 'interface'
        }
        not_in_assoc.delete_if { |c|
          supers = @model.visible_associable_elements.select { |other|
            other.supers.include? c
          }
          !supers.empty?
        }
        not_in_assoc.delete_if { |c|
          c.doc_words.first_index { |i|
            c.doc_words[i].casecmp('utility') == 0 &&
                c.doc_words[i + 1].casecmp('class') == 0
          }
        }
        messages += not_in_assoc.map { |c|
          Issue.new(:WIDOWCLASS, :unassociated_class,
                    "#{c.ident.cap_first} is not associated with any other class and may be unnecessary")
        }

        # check for a<>->b<-<>c (class owned by two others)
        # do this by building a table giving ownership
        owner_of = {}
        @logmod.visible_associations.map { |a|
          #puts "examing #{a.ident}"
          if a.a_role.has_supplier? && a.b_role.has_supplier?
            if a.a_role.composite?
              #puts "checking #{a.a_role.supplier_name}"
              #puts "ownerof = #{owner_of[a.b_role.supplier_name]}"
              if owner_of[a.b_role.supplier_name].nil?
                owner_of[a.b_role.supplier_name] = a.a_role.supplier_name
              else
                messages << Issue.new(:CLASS2OWN, :dual_owners,
                                      "Class #{a.b_role.supplier_name.cap_first} has two owners: #{owner_of[a.b_role.supplier_name]} and #{a.a_role.supplier_name}")
              end
            elsif a.b_role.composite?
              #puts "checking #{a.b_role.supplier_name}"
              #puts "ownerof = #{owner_of[a.a_role.supplier_name]}"
              if owner_of[a.a_role.supplier_name].nil?
                owner_of[a.a_role.supplier_name] = a.b_role.supplier_name
              else
                messages << Issue.new(:CLASS2OWN, :dual_owners,
                                      "Class #{a.a_role.supplier_name.cap_first} has two owners: #{owner_of[a.a_role.supplier_name]} and #{a.b_role.supplier_name}")
              end
            end
          end
        }

        # check for containers that seem to not contain anything
        #  - a container is defined as a template class
        #  - this only applies for non-simple classes - containers for
        #    characters and integers are ignored
        containers = @logmod.visible_classes.select { |c| c.template? }
        containers.each { |c|
          elt_type = c.contained_type
          if elt_type && !ModelChecker.simple_types.include?(elt_type)
            # for now, search for _some_ associated class with many elements
            # treat unknown multiplicity as being a *
            possible_elts = c.associates.select { |role|
              (role.multiplicity.nil? && !role.composite?) ||
                  role.multiple_element_multiplicity?
            }
            if possible_elts.empty?
              messages << Issue.new(:MISSINGELT, :container_without_element,
                                    "Container #{c.ident} appears to have no association to any elements")
            end
          end
        }

        messages
      end

      private

      # role is an composite but the role's multiplicity is not 1
      def bad_composition?(role)
        role.composite? && !role.ok_composite_multiplicity?
      end

    end
  end

end
