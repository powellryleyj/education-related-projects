require_relative 'ck_actors'
require_relative 'ck_associations'
require_relative 'ck_attr_names'
require_relative 'ck_class_names'
require_relative 'ck_complex_attrs'
require_relative 'ck_documentation'
require_relative 'ck_hidden'
require_relative 'ck_idents'
require_relative 'ck_methods'
require_relative 'ck_multi_inherit'
require_relative 'ck_multiplicities'
require_relative 'ck_roles'
require_relative 'ck_standard_classes'
require_relative 'ck_use_cases'

module UmlTools
  module Check
    class AllChecks
      attr_accessor :use_cases, :standard_classes, :roles, :multiplicities, :multi_inherit,
                    :methods, :idents, :hidden, :documentation, :complex_attrs,
                    :class_names, :attr_names, :associations, :actors

      def initialize(model)
        @use_cases = Check::CheckUseCases.new(model)
        @standard_classes = Check::CheckStandardClasses.new(model)
        @roles = Check::CheckRoles.new(model)
        @multiplicities = Check::CheckMultiplicities.new(model)
        @multi_inherit = Check::CheckMultiInheritance.new(model)
        @methods = Check::CheckMethods.new(model)
        @idents = Check::CheckIdentifiers.new(model)
        @hidden = Check::CheckHiddenElements.new(model)
        @documentation = Check::CheckDocumentation.new(model)
        @complex_attrs = Check::CheckComplexAttributes.new(model)
        @class_names = Check::CheckClassNames.new(model)
        @attr_names = Check::CheckAttributeNames.new(model)
        @associations = Check::CheckAssociations.new(model)
        @actors = Check::CheckActors.new(model)
      end

      # model X boolean X CheckList -> list(Issue)
      def self.run_all_checks(model, verbose, checklist = nil)
        messages = []
        all_checks = Check::AllChecks.new(model)
        all_checks.instance_variables.each do |check|
          if verbose
            $stderr.puts "Running #{check}"
            messages += all_checks.instance_variable_get(check).check.map { |m| checker.class.name + ': ' + m }
          else
            messages += all_checks.instance_variable_get(check).check
          end
        end
        model.all_load_errors.each { |err|
          messages << Issue.new(:SYNTAX, :syntax_error, "Syntax error in the model: #{err}", nil)
        }
        messages = checklist.filtered(messages) if checklist
        #puts "At end, returning #{messages}"
        return messages
      end
    end
  end
end
