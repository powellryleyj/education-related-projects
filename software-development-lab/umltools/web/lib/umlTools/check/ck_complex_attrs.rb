#!/usr/bin/env ruby
# check for complex attributes

require_relative '../petal'
require_relative '../check'
require_relative 'model_checker'
module UmlTools

  module Check
    # check complex attributes in logical models
    class CheckComplexAttributes < ModelChecker

      def check
        #
        # rwh, todo: revise to get better error messages out of complex types
        #
        # check for attribute names that contain [] or * (arrays, pointers)
        struct_messages = @logmod.classes.map { |c|
          bad_attributes = c.attributes.select { |a|
            a.name =~ /\[/ || a.name =~ /\]/ || a.name =~ /\*/ ||
                (a.type && (a.type =~ /\[/ || a.type =~ /\]/ || a.type =~ /\*/))

          }
          bad_attributes.map! { |a| a.nonempty_name }
          if bad_attributes.length == 1
            Issue.new(:COMPLEXATTR, :no_complex_attributes,
                      "Complex attribute #{bad_attributes[0]} in class #{c.nonempty_name} - replace by an association to a container")
          elsif bad_attributes.length > 1
            Issue.new(:COMPLEXATTR, :no_complex_attributes,
                      "Complex attributes #{bad_attributes.join(', ')} in class #{c.nonempty_name} - replace by associations with containers")
          end
        }
        struct_messages.delete(nil)

        # check for attribute names that end with 'list', 'map', 'table'
        collection_messages = @logmod.classes.map { |c|
          bad_attributes = c.attributes.select { |a|
            a.name =~ /[lL][iI][sS][tT]$/ || a.name =~ /[mM][aA][pP]$/ ||
                a.name =~ /[tT][aA][bB][lL][eE]$/
          }
          bad_attributes.map! { |a| a.nonempty_name }
          if bad_attributes.length == 1
            Issue.new(:COMPLEXATTR, :no_collections,
                      "Complex attribute #{bad_attributes[0]} in class #{c.nonempty_name} - replace by an association to a container")
          elsif bad_attributes.length > 1
            Issue.new(:COMPLEXATTR, :no_collections,
                      "Complex attributes #{bad_attributes.sort.join(', ')} in class #{c.nonempty_name} - replace by associations with containers")
          end
        }
        collection_messages.delete(nil)

        # next two checks need all class & actor names (in lower case for matching)
        all_class_and_actor_names = @logmod.short_class_names + @ucmod.actor_names
        all_class_and_actor_names = all_class_and_actor_names.map { |n| n.downcase }
        # clear out names related to date, time, color - classic names
        #    for classes that could be built-in but are not
        all_class_and_actor_names.delete_if { |x| x =~ /date/ || x =~ /time/ || x =~ /color/ }

        # Check for non-simple attributes by name and type
        #    (searching all attributes within each class)
        class_attribute_messages = @logmod.classes.map { |c|
          # check for attributes which have names of classes embedded in them
          #   towards the end of the name
          #   * but, allow if stereotype is 'enum' or have defined type
          #   * also, ignore any name < 4 chars; too easy to match
          #     This covers "on", in particular.
          bad_attributes = c.attributes.select { |attribute|

            if !attribute.type &&
               (!attribute.stereotype || attribute.stereotype.downcase != 'enum') &&
               attribute.name.size > 3
              target = attribute.name.downcase
              results = all_class_and_actor_names.find { |class_name|
                class_name.index(target, -[target.size + 1, class_name.size].min) ||
                    target.index(class_name, -[class_name.size + 1, target.size].min)
              }
              results
            end
          }
          # Add in attributes whose type is the name of a class, and class name
          #  not something built in or not a simple type
          bad_attributes += c.attributes.select { |attribute|
            if (attribute.type && 
                !ModelChecker.simple_types.include?(attribute.type))
              target = attribute.type.downcase
              all_class_and_actor_names.find { |class_name| class_name == target }
            end
          }
          bad_attributes.map! { |a| a.nonempty_name }
          bad_attributes.sort!
          bad_attributes.uniq!
          if bad_attributes.length == 1
            Issue.new(:COMPLEXATTR, :non_simple_attributes,
                      "Attribute #{bad_attributes[0]} in class #{c.nonempty_name} appears to not be simple - replace by an association with the class")
          elsif bad_attributes.length > 1
            Issue.new(:COMPLEXATTR, :non_simple_attributes,
                      "Attributes #{bad_attributes.join(', ')} in class #{c.nonempty_name} are apparently not simple - replace by associations")
          end
        }
        class_attribute_messages.delete(nil)

        struct_messages + collection_messages + class_attribute_messages
      end

    end
  end

end
