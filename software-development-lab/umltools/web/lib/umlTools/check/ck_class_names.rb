#!/usr/bin/env ruby
# check class names

require_relative '../petal'
require_relative '../check'
require_relative 'model_checker'
#require 'ruby-debug'
module UmlTools

  module Check
    class CheckClassNames < ModelChecker

      def check()
        messages = []

        # check for lower-case class names - should be upper case
        #   Skip library class names, and names starting w/ 'wx'
        #   ('wx' signals wxWidgits classes)
        lc_names = @logmod.classes.select { |c|
          c.name && (c.name[0] < ?A || c.name[0] > ?Z) &&
              LibraryClass.find(c.name).nil? &&
              c.name[0] != ?w && c.name[1] != ?x
        }
        messages += lc_names.map { |c|
          Issue.new(:REQCAPS, :capitalize_class_names,
                    "#{c.ident.cap_first} should begin with a capital letter")
        }

        # check for and, or in class names - should split into multiple classes
        has_and = []
        has_or = []
        @logmod.classes.map { |it|
          words =
              it.name ? UmlTools.words_in_identifier(it.name).map { |x| x.downcase } : []
          has_and << it if words.include? 'and'
          has_or << it if words.include? 'or'
        }
        messages += has_and.map { |c|
          Issue.new(:NOAND, :no_and_in_class_names,
                    "#{c.ident.cap_first} has 'and' in it - rename or split into two classes")
        }
        messages += has_or.map { |c|
          Issue.new(:NOOR, :no_or_in_class_names,
                    "#{c.ident.cap_first} has 'or' in it - rename or use generalization")
        }

        # check for <> with nothing in between - an empty container class?
        #   Note: checks that the number of type arguments matches the expected
        #   number for standard library classes are done in ck_standard_classes.
        #   However, this check is kept here to catch non-standard classes
        #   as well. This means the other check does not cover the empty typelist
        #   case.
        lc_names = @logmod.classes.select { |c|
          if c.name && c.name.delete(' ').include?('<>')
            messages << Issue.new(:BAD_CONTAINER, :no_contained_type,
                                  "Container #{c.name} has no contents - nothing between < and >")
          end
        }

        # check for inconsistent naming schemes - should have _'s or camel case
        #   for all class names
        has_underscore = @logmod.classes.select { |c| c.name && c.name.index('_') }
        has_camel_case = @logmod.classes.select { |c|
          # more than one capital, no spaces, and no underscores
          n = c.name
          n && n.count('A-Z') > 1 && !n.index(' ') && !n.index('_')
        }
        if has_camel_case.size > 0 && has_underscore.size > 0
          messages << Issue.new(:MIXEDSTYLES, :no_mixed_styles_for_class_names,
                                'Avoid mixing class naming styles (using underscores in some names and not others)')
        end
        messages
      end

    end
  end

end
