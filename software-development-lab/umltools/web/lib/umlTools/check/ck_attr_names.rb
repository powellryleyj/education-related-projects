#!/usr/bin/env ruby
# check class names

require_relative '../petal'
require_relative '../check'
require_relative 'model_checker'
require_relative '../petal_utils'

module UmlTools
  module Check
    # check attribute names in logical models
    class CheckAttributeNames < ModelChecker

      def check()
        attrs = @logmod.all_attributes
        messages = []

        # check for attributes with capitalized first names - prefer
        #   lower case letters
        # All-capitalized are treated as named constants and ignored for this check
        uc_names = attrs.select { |it|
          #debugger if it.name && it.base_name.nil?
          it.base_name && it.base_name =~ /^[A-Z].*[a-z]/
        }
        messages += uc_names.map { |it|
          Issue.new(:USELOWERCASE, :lower_case_attributes,
                    it.ident.cap_first + ' should begin with a lower case letter')
        }

        # check for names with nondescript words in them
        #   Note: word must appear _in_ the name - can't be the whole name
        #   The whole name case is processed elsewhere.
        #   (Would need to generalize this check if have words w/ len != 4)
        has_non_descript_word = attrs.select { |it|
          words = UmlTools.words_in_identifier(it.base_name).map { |x| x.downcase }
          common = words & %w(flag data)
          !common.empty? && it.base_name.size != 4
        }
        messages += has_non_descript_word.map { |it|
          Issue.new(:NONDESCRIPT, :nondescript_attribute,
                    it.ident.cap_first + ' contains a nondescript word')
        }

        # check for id, idx, index - should use direct association instead
        has_index_words = attrs.select { |it|
          words = UmlTools.words_in_identifier(it.base_name).map { |x| x.downcase }
          common = words & %w(id idx index)
          !common.empty?
        }
        messages += has_index_words.map { |it|
          Issue.new(:INDEX, :use_direct_association,
                    it.ident.cap_first + ' names an index - replace by a direct association')
        }

        # -- I'm not convinced this is a good check:
        #    has_binary_values = attrs.select { |it|
        #      words = UmlTools.words_in_identifier(it.name).map { |x| x.downcase }
        #      common = words & ['on', 'off', 'up', 'down']
        #      !common.empty?
        #    }

        #    messages += has_binary_values.map { |it|
        #     it.ident.cap_first + ' contains a binary value such as up/down or on/off'
        #    }

        # look for inconsistencies in naming conventions: mixing underscores
        #   with camel case - use one or the other
        #   Note: skip first char so leading underscores are ignored since
        #         leading underscores are sometimes used to distinguish attributes
        #         from local variables.
        has_underscore = attrs.select { |it| it.base_name[1..-1].index('_') }
        has_camel_case = attrs.select { |it|
          # a capital after the first letter, no spaces, and no underscores
          n = it.base_name
          n[1, n.size].count('A-Z') > 0 && !n.index(' ') && !n.index('_')
        }
        if has_camel_case.size > 0 && has_underscore.size > 0
          messages << Issue.new(:MIXEDSTYLES, :mixed_attribute_naming_styles,
                                'Avoid mixing naming styles between attributes (using underscores in some names and not others)')
        end

        # look for public attributes
        public_attributes = attrs.select { |x| x.protection == :public }
        public_attributes.each { |at|
          messages << Issue.new(:PUBLICATTR, :non_public_attributes,
                                "#{at.ident.cap_first} is public")
        }

        messages
      end

    end
  end

end
