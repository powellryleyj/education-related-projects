#!/usr/bin/env ruby
# check for problems with method names

require_relative '../petal'
require_relative '../check'
require_relative 'model_checker'
require 'set'

module UmlTools
  module Check
    # check method names in logical models
    class CheckMethods < ModelChecker

      def check()
        methods = @logmod.all_non_library_methods
        messages = []

        # check for methods with nondescript words in them
        #   Note: word must appear _in_ the name - can't be the whole name
        #   The whole name case is processed elsewhere.
        #   (Would need to generalize this check if add other nondescript words)
        has_non_descript_word = methods.select { |it|
          words = UmlTools.words_in_identifier(it.name).map { |x| x.downcase }
          common = words & %w(process)
          !common.empty? && it.name.downcase != 'process'
        }
        messages += has_non_descript_word.map { |it|
          Issue.new(:NONDESCRIPT, :nondescript_method,
                    it.ident.cap_first + ' contains a nondescript word')
        }

        # look for inconsistencies in naming conventions: mixing underscores
        #   with camel case - use one or the other
        has_underscore = methods.select { |it| it.name.index('_') }
        has_camel_case = methods.select { |it|
          # a capital after the first letter, no spaces, and no underscores
          n = it.name
          n[1, n.size].count('A-Z') > 0 && !n.index(' ') && !n.index('_')
        }
        if has_camel_case.size > 0 && has_underscore.size > 0
          messages << Issue.new(:MIXEDSTYLES, :mixed_styles_for_method_names,
                                'Avoid mixing naming styles between method names (using underscores in some and not others)')
        end

        messages
      end

    end
  end

end
