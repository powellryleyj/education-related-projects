require_relative 'ck_message_labels'
require_relative 'ck_lifeline_interaction'
require_relative 'ck_message_types'
require_relative 'ck_guard_conditions'

module UmlTools
  module Check
    module Sequence
      class AllChecks
        attr_accessor :message_labels, :message_types, :message_composition, :guard_conditions

        def initialize(model)
          @message_labels = UmlTools::Check::Sequence::CheckMessageLabels.new(model)
          @message_types = UmlTools::Check::Sequence::CheckMessageTypes.new(model)
          @message_composition = UmlTools::Check::Sequence::CheckLifelineInteraction.new(model)
          @guard_conditions = UmlTools::Check::Sequence::CheckGuardConditions.new(model)
        end

        # TODO: This should probably be refactored to adhere to DRY principle and share more code
        # TODO: with the non-sequence diagram portion.
        def self.run_all_checks(model, verbose, checklist = nil)
          messages = []
          all_checks = Check::Sequence::AllChecks.new(model)
          all_checks.instance_variables.each do |check|
            if verbose
              $stderr.puts "Running #{check}"
              messages += all_checks.instance_variable_get(check).check.map { |m| checker.class.name + ': ' + m }
            else
              messages += all_checks.instance_variable_get(check).check
            end
          end
          model.all_load_errors.each { |err|
            messages << Issue.new(:SYNTAX, :syntax_error, "Syntax error in the model: #{err}", nil)
          }
          messages = checklist.filtered(messages) if checklist
          #puts "At end, returning #{messages}"
          return messages
        end
      end
    end
  end
end
