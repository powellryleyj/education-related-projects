#!/usr/bin/env ruby

module UmlTools
  module Check
    module Sequence
      # base class for sequence checkers
      class ModelChecker
        attr_reader :model

        def initialize(model)
          @model = model
        end

        # checks made by each test; returns list(issue)
        def check
          []
        end
      end
    end
  end
end