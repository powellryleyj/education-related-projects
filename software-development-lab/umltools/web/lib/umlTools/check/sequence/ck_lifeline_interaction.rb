#!/usr/bin/env ruby
require_relative '../../check'
require_relative 'model_checker'

module UmlTools
  module Check
    module Sequence
      class CheckLifelineInteraction < ModelChecker
        def check
          messages = []
          @model.messages.each { |message|
            # Actor interactions
            if (message.sender.type == 'uml:Actor' && message.receiver.stereotype != 'boundary') or
                (message.receiver.type == 'uml:Actor' && message.sender.stereotype != 'boundary') or
                (message.sender.stereotype == 'boundary' && (message.receiver.stereotype == 'boundary' or message.receiver.type == 'entity')) or
                (message.receiver.stereotype == 'boundary' && (message.sender.stereotype == 'boundary' or message.sender.type == 'entity')) or
                (%w(control controller).include?(message.sender.stereotype) && message.receiver.type == 'uml:Actor') or
                (%w(control controller).include?(message.receiver.stereotype) && message.sender.type == 'uml:Actor') or
                (message.sender.stereotype == 'entity' && !%w(control controller).include?(message.receiver.stereotype)) or
                (message.receiver.stereotype == 'entity' && !%w(control controller).include?(message.sender.stereotype))
              messages << Issue.new(:LIFELINE_INTERACTION, :seq_lifeline_interaction,
                                    "Message going from #{message.sender.name} to #{message.receiver.name} involves an invalid interaction!", message)
            end
          }
          messages
        end
      end
    end
  end
end
