#!/usr/bin/env ruby
require_relative '../../check'
require_relative 'model_checker'

module UmlTools
  module Check
    module Sequence
      class CheckGuardConditions < ModelChecker
        # Attempt to validate a boolean expression as best as possible.
        def check
          messages = []
          @model.messages.each { |message|
            unless message.guard.nil?
              # Remove spaces
              guard = message.guard.tr ' ', ''
              # Check parenthesis count
              if guard.count('(') != guard.count(')')
                messages << Issue.new(:GUARD_CONDITIONS, :seq_guard_conditions,
                                      "Message going from #{message.sender.name} to #{message.receiver.name} has an invalid guard condition. " +
                                          "(Guard: #{message.guard} Error: Number of opening parenthesis does not match number of closing.", message)
              end
              # Remove parenthesis
              guard = guard.tr '(', ''
              guard = guard.tr ')', ''
              # Split on equality operators
              args = guard.split /(&&|\|\|)/
              args.reject! { |arg| arg == '&&' or arg == '||' }
              args.each { |arg|
                conditionals = /(!=|==|>=|<=|<|>)/
                params = arg.split conditionals
                params = params.reject { |param| conditionals.match param }
                # Check for assignment operator
                if params.select { |p| p.include? '=' and conditionals.match(p) == nil }.any?
                  messages << Issue.new(:GUARD_CONDITIONS, :seq_guard_conditions,
                                        "Message going from #{message.sender.name} to #{message.receiver.name} has an invalid guard condition. " +
                                            "(Guard: #{message.guard}, Invalid portion: #{arg}, Error: Accidental assignment operator, should be ==.", message)
                end
                # TODO: Possibly expand on this to support language syntax other than Java (eg. Ruby supporting ? in var name)
                # Check for invalid characters
                invalid = params.select { |p| not (p.gsub(/[a-zA-Z0-9\/\*\+\-%=_]/, '').gsub(conditionals, '').empty?) }
                if invalid.any?
                  messages << Issue.new(:GUARD_CONDITIONS, :seq_guard_conditions,
                                        "Message going from #{message.sender.name} to #{message.receiver.name} has an invalid guard condition. " +
                                            "(Guard: #{message.guard}, Invalid portion: #{arg}, Error: Invalid characters found, only a-z & A-Z & 0-9 & _ is allowed.", message)
                end
                # Cannot start with a number (unless the whole arg is number)
                if params.select { |p| self.remove_arithmetic(p).select { |var| self.is_invalid_var? var }.any? }.any?
                  messages << Issue.new(:GUARD_CONDITIONS, :seq_guard_conditions,
                                        "Message going from #{message.sender.name} to #{message.receiver.name} has an invalid guard condition. " +
                                            "(Guard: #{message.guard}, Invalid portion: #{arg}, Error: First character of a variable cannot be a number.", message)
                end
              }
            end
          }
          messages
        end
        def remove_arithmetic(str)
          reg = /[\/\-\+%=<>]/
          str.split(reg).reject { |s| reg.match s }
        end
        def is_invalid_var?(str)
          # If the first character is a number, returns true if the entire string isn't a number.
          (true if Float(str[0]) rescue false) and (false if Float(str) rescue true)
        end
      end
    end
  end
end
