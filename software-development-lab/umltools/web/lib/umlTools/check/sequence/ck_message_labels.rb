#!/usr/bin/env ruby
require_relative '../../check'
require_relative 'model_checker'

module UmlTools
  module Check
    module Sequence
      class CheckMessageLabels < ModelChecker
        def check
          messages = []
          @model.messages.each { |message|
            if message.name.nil? || message.name == ''
              messages << Issue.new(:MESSAGE_LABELS, :seq_message_labels,
                                    "Message going from #{message.sender.name} to #{message.receiver.name} does not have a method signature", message)
            end
            message.arguments.each { |arg|
              if arg.name.nil? || arg.name == ''
                messages << Issue.new(:MESSAGE_LABELS, :seq_argument_labels,
                                      "Message going from #{message.sender.name} to #{message.receiver.name} has an unnamed argument", message)
              end
            }
          }
          messages
        end

      end
    end
  end
end
