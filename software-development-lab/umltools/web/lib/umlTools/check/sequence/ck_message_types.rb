#!/usr/bin/env ruby
require_relative '../../check'
require_relative 'model_checker'

module UmlTools
  module Check
    module Sequence
      class CheckMessageTypes < ModelChecker
        def check
          messages = []
          @model.messages.each { |message|
            message.arguments.each { |arg|
              if arg.type.nil? || arg.type == ''
                messages << Issue.new(:MESSAGE_TYPES, :seq_message_arg_types,
                                      "Message going from #{message.sender.name} to #{message.receiver.name} has an argument with no type specified.", message)
              end
            }
          }
          messages
        end
      end
    end
  end
end
