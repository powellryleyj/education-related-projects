#!/usr/bin/env ruby

module UmlTools
  module Check
    # base class for all checkers, and runs all checkers
    class ModelChecker
      attr_reader :model, :logmod, :ucmod

      def initialize(model)
        @model = model
        @logmod = model.logical # logical model: class diagram
        @ucmod = model.uc # use case model
      end

      # checks made by each test; returns list(issue)
      def check
        []
      end

      # used for various checks:
      @@SIMPLE_TYPES = Set.new %w(char shortint short unsignedshort int unsigned longint long unsignedlong longlong bool wchar_t float double longdouble string byte short int long float double boolean char BigDecimal BigInteger Byte Short Integer Long Float Double Boolean Char Date String)

      def self.simple_types
        @@SIMPLE_TYPES
      end
    end
  end


end
