#!/usr/bin/env ruby
# checks for classes in standard libraries

require_relative '../petal'
require_relative '../check'
require_relative 'model_checker'
require 'set'

module UmlTools
  module Check
    class CheckStandardClasses < ModelChecker

      def check()
        messages = []
        # classes which students frequently abuse
        # check for extra methods, attributes on pre-defined classes
        to_check = @logmod.classes.each { |c|
          lib = LibraryClass.find(c.name)
          if lib
            # check right number of type parameters
            params = LibraryClass.type_parameters(c.name)
            if params.nil? && lib.expected_type_parameters > 0 # no params given
              # rwh, todo: decide what to do with these
              #messages << Issue.new(:BAD_CONTAINER, :expected_type_params,
              #                      "Container class #{c.name} needs < and > describing the contents")
            elsif params && params.size > 0 && params.size != lib.expected_type_parameters
              # note: case where params.size == 0 is covered by ck_class_names
              args_msg = params.size == 1 ? '1 argument' : "#{params.size} arguments"
              messages << Issue.new(:BAD_CONTAINER, :wrong_number_of_types,
                                    "Container #{c.name} has #{args_msg} and needs #{lib.expected_type_parameters}")
            end
            # check operation list
            in_model = Set.new(c.methods.map { |m| m.name })
            problem = in_model - lib.methods
            if !problem.empty?
              bad = problem.to_a.sort.join(', ')
              messages << Issue.new(:INVALIDOP, :invalid_op_in_standard_class,
                                    "Invalid operation(s) in standard class #{c.nonempty_name}: #{bad}")
            end
          end
        }

        messages
      end
    end
  end
end
