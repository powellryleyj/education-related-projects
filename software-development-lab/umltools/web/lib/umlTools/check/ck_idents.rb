#!/usr/bin/env ruby
# check for general problems with identifiers

require_relative '../petal'
require_relative '../check'
require_relative 'model_checker'

module UmlTools
  module Check
    class CheckIdentifiers < ModelChecker

      @@JAVA_RESERVED = Set.new %w(abstract assert boolean break byte case catch char class
                                 const continue default do double else extends false final
                                 finally float for goto if implements import instanceof
                                 int interface long native new null package private
                                 protected public return short static strictfp super
                                 switch synchronized this throw throws transient true try
                                 void volatile while)

      @@CPP_RESERVED = Set.new %w(asm auto break case catch char class const continue
                                default delete do double else enum extern float for friend
                                goto if inline int long new operator private protected
                                public register return short signed sizeof static struct
                                switch template this throw try typedef union unsigned
                                virtual void volatile while)

      @@COMMON_ABBREVIATIONS =
          Set.new %w(ack filename filenames hr hrs html login min msg num nums resize sms url
                 username waypoint webcam wx)

      @@WORDS_TO_IGNORE = @@JAVA_RESERVED + @@CPP_RESERVED + @@COMMON_ABBREVIATIONS

      @@NONDESCRIPT_WORDS = Set.new %w(control data flag process)

      def misspellings(all_words)
        messages = []

        # check that aspell is available
        # if aspell not available, will get error message to stdout
        begin
          out = `echo "cats meowr" | aspell list 2>&1`
          out.chomp!
          have_aspell = out == 'meowr'
        end

        if not have_aspell
          messages << Issue.new(:INSTALLATION, :no_spell_check, 'Spell check not available')
        else
          # spell check it
          #   note: as of version 3.1.20, no longer using --dont-use-other-dicts
          io = IO.popen('aspell --ignore-case list', 'r+')
          all_words.each { |w| io.puts(w) }
          io.close_write
          bad_words = io.readlines
          bad_words.map! { |w| w.chomp }
          io.close_read

          bad_words.delete_if { |w| @@WORDS_TO_IGNORE.include?(w) }

          if !bad_words.empty?
            messages << Issue.new(:UNRECOGNIZED, :unrecognized_words,
                                  'Unrecognized words in identifiers: ' +
                                      "#{bad_words.join(', ')}")
          end
        end
        messages
      end

      def check()
        messages = []

        all_candidates = @model.all_named_elements

        # filter out class names that instantiate templates (in C++ or Java)
        all_candidates.delete_if { |c| c.class? && c.template? }

        # filter out operation names that are overloaded operators in C++
        all_candidates.delete_if { |c| c.method? && c.operator? }

        # create list of all words
        all_words = all_candidates.map { |c| UmlTools.words_in_identifier(c.name) }
        all_words.flatten!
        all_words.map! { |w| w.downcase }
        all_words.sort!
        all_words.uniq!

        # check for identifiers with spaces in them (skipping use cases)
        with_ws = all_candidates.select { |e| e.name.index(' ') && !e.use_case? }
        messages += with_ws.map { |c|
          Issue.new(:SPACES, :no_spaces_in_names,
                    "#{c.ident.cap_first} contains spaces")
        }

        # check for illegal characters in identifiers; ignore [] pairs at end
        with_bad = all_candidates.select { |e|
          tmp = e.name
          tmp = tmp.sub(/(\[\])+$/, '')
          tmp = tmp.gsub('::', '')
          non_c_ident(tmp)
        }
        messages += with_bad.map { |c|
          Issue.new(:ILLEGALID, :no_illegal_identifiers,
                    "#{c.ident.cap_first} is not a legal identifier")
        }

        # check for identifiers which are nondescript
        nondescript = all_candidates.select { |e|
          @@NONDESCRIPT_WORDS.include?(e.name.downcase)
        }
        messages += nondescript.map { |it|
          Issue.new(:NONDESCRIPT, :nondescript_identifier,
                    it.ident.cap_first + ' is a nondescript identifier')
        }

        # checked for identifiers that are reserved words in C++ or Java
        reserved = all_candidates.select { |e|
          @@JAVA_RESERVED.include?(e.name) || @@CPP_RESERVED.include?(e.name)
        }
        messages += reserved.map { |c|
          if @@JAVA_RESERVED.include?(c.name)
            lang = 'Java' + (@@CPP_RESERVED.include?(c.name) ? ' and C++' : '')
          else
            lang = 'C++'
          end
          Issue.new(:RESERVED, :no_reserved_words,
                    "\`#{c.name}' is a reserved word in " + lang +
                        " but was used as a name in #{c.ident}")
        }

        # check for misspelled words - often indicates bad abbreviations
        #   If aspell is not available, does not check spelling
        messages += misspellings(all_words)

        messages
      end

      protected

      def non_c_ident(str)
        str && !str.empty? &&
            (str.index(/[^a-zA-Z0-9_ ]/) || str[0, 1] !~ /[a-zA-Z_]/)
      end

    end
  end

end
