#!/usr/bin/env ruby
# check for errors in roles (especially multiplicities)

require_relative '../petal'
require_relative '../check'
require_relative 'model_checker'
require 'set'
#require 'ruby-debug'

module UmlTools
  module Check
    class CheckRoles < ModelChecker

      def check()
        messages = []
        all_roles = @logmod.visible_roles

        # check for a multiplicity of 0 - does not make sense
        zero_multiplicity = all_roles.select { |role|
          role.multiplicity == '0'
        }
        messages += zero_multiplicity.map { |role|
          Issue.new(:NOZERO, :no_multiplicity_0,
                    "#{role.ident.cap_first}: do not use multiplicity 0")
        }

        # check for multiplicity 0..* - change to just *
        zero_star_multiplicity = all_roles.select { |role|
          role.multiplicity == '0..*'
        }
        messages += zero_star_multiplicity.map { |role|
          Issue.new(:NO0DOTSTAR, :no_multiplicity_0_star,
                    "#{role.ident.cap_first}: use multiplicity '*' rather than '0..*'")
        }

        # check for role names with capitalized first names - prefer
        #   lower case letters
        uc_names = all_roles.select { |it|
          it.name && !it.name.empty? && it.name[0] >= ?A && it.name[0] <= ?Z
        }
        messages += uc_names.map { |it|
          Issue.new(:USELOWERCASE, :lower_case_roles,
                    it.ident.cap_first + ' should begin with a lower case letter')
        }

        # check for collections without templates; that is, find roles that are
        #   navigable (can reach from the other end) but for which source is
        #   not a template
        # By default, this check is inactive.
        multiplicity_without_collections = all_roles.select { |role|
          starred = role.multiplicity &&
              (role.multiplicity.include?(?*) || role.multiplicity.include?(?n))
          starred && role.navigable? && role.consumer_role &&
              !role.consumer_role.navigable? && role.consumer_class &&
              !role.consumer_class.template?
        }
        messages += multiplicity_without_collections.map { |role|
          other_end_name = role.consumer_class ? role.consumer_class.ident :
              'class at other end of association'
          Issue.new(:MISSING_CONTAINER, :starred_navigable_wo_container,
                    "#{role.ident.cap_first}: #{other_end_name} should be a container")
        }

      end

    end
  end
end
