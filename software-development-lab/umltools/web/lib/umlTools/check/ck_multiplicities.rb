#!/usr/bin/env ruby
# check for missing multiplicities

require_relative '../petal'
require_relative '../check'
require_relative 'model_checker'
require 'set'

module UmlTools
  module Check
    class CheckMultiplicities < ModelChecker

      def check()

        messages = []

        associations_missing_cards = @logmod.visible_associations.select { |a|
          a.a_role.missing_multiplicity? && a.b_role.missing_multiplicity?
        }

        roles_handled = Set.new
        associations_missing_cards.each { |a|
          roles_handled.add(a.a_role)
          roles_handled.add(a.b_role)
        }

        roles_missing_cards = @logmod.visible_roles.select { |r|
          r.missing_multiplicity? && !roles_handled.include?(r)
        }

        missing_card = associations_missing_cards + roles_missing_cards

        if !missing_card.empty?
          items = (missing_card.map { |it| it.ident }).sort.join(', ')
          if missing_card.size == 1
            messages << Issue.new(:NOMULTI, :missing_multiplicities,
                                  "Missing multiplicity: #{items}")
          else
            messages << Issue.new(:NOMULTI, :missing_multiplicities,
                                  "Missing multiplicities: #{items}")
          end
        end

        messages
      end

    end
  end
end
