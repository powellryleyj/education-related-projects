#!/usr/bin/env ruby

require_relative 'check_list'

module UmlTools

  module ListOfChecks
# List of items to check; others are ignored
# rwh, Jan 2012: it's not clear this is really used
    class ChecksToInclude < CheckList
      # in addition to above:
      # @checks_to_include: Map :category -> set(CheckItem)
      #   [] means include/exclude all in that category

      def initialize(organization, description, contact_email_address,
                     tool_version, language, list_of_checks_to_include)
        super(organization, description, contact_email_address,
              tool_version, language)
        @checks_to_include = setup_map(list_of_checks_to_include)
      end

      # is check for given category, problem pair one of those that should be done?
      def do?(category, problem)
        # only do checks that are named
        items = @checks_to_include[category]
        items && (items.empty? || items.include?(problem))
      end

      # ensure category/problem not included in checks; :ALL excludes all problems
      def exclude(category, problem)
        if problem == :ALL
          @checks_to_include.delete category
        elsif lookup = @checks_to_include[category]
          lookup.delete problem
        end
      end
    end
  end
end
