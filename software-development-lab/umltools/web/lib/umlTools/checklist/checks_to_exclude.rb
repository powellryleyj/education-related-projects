#!/usr/bin/env ruby
require_relative 'check_list'
require_relative 'check_item'

module UmlTools
  module ListOfChecks

    #
    # Checks that should not be made; others are allowed
    #
    class ChecksToExclude < CheckList
      attr_reader :checks_to_exclude
      # in addition to above:
      # @checks_to_exclude: Map :category -> set(CheckItem)
      #   [] means exclude all in that category

      def initialize(organization, description, contact_email_address,
                     tool_version, language, list_of_checks_to_exclude)
        super(organization, description, contact_email_address,
              tool_version, language)
        @checks_to_exclude = setup_map(list_of_checks_to_exclude)
      end

      # is check for given category, problem pair one of those that should NOT be done?
      def do?(category, problem)
        # do check unless there are explicit directions to not do it
        items = @checks_to_exclude[category]
        items.nil? || (!items.empty? && !items.include?(problem))
      end

      # ensure category/problem not included in checks; :ALL excludes all problems
      def exclude(category, problem)
        if problem == :ALL
          @checks_to_exclude[category] = Set.new
        elsif lookup = @checks_to_exclude[category]
          lookup << problem
        else
          result = (@checks_to_exclude[category] = Set.new)
          result << problem
        end
      end

      # exclusions to use by default
      # Note: if add item here, must add to web/app/models/check_list.rb as well
      def self.default(is_sequence)
        exclude = [CheckItem.new(:MISSING_CONTAINER, :ALL)]
        # TODO (rwh, 2016): rip out dependency on Explanation!!! (this is a rails model dependency)
        if defined?(Explanation) && Explanation.class == Class
          if is_sequence
            Explanation.where(:diagram => ['use case', 'class']).each { |exp|
              exclude << CheckItem.new(exp.name, :ALL)
            }
          else
            Explanation.where(:diagram => 'sequence').each { |exp|
              exclude << CheckItem.new(exp.name, :ALL)
            }
          end
        end
        ChecksToExclude.new('default', 'generic', 'none', '0', '', exclude)
      end

      def ==(other)
        @organization == other.organization and
            @description == other.description and
            @contact_email_address == other.contact_email_address and
            @language == other.language and
            @tool_version == other.tool_version and
            @checks_to_exclude == other.checks_to_exclude
      end
    end
  end
  end
