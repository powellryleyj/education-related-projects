#!/usr/bin/env ruby

module UmlTools
  module ListOfChecks
    # Specific issue to check (or not check)
    class CheckItem
      # symbols; :issue is :ALL if covering all issues in category
      attr_reader :category, :issue

      def initialize(category, issue)
        @category = category.to_sym
        @issue = issue.to_sym
      end
    end
  end
end
