#!/usr/bin/env ruby

module UmlTools
  module ListOfChecks
    # List of items to check
    class CheckList
      # all strings
      attr_reader :organization, :description, :contact_email_address,
                  :tool_version, :language

      def initialize(organization, description, contact_email_address,
                     tool_version, language)
        @organization = organization
        @description = description
        @contact_email_address = contact_email_address
        @tool_version = tool_version
        @language = language
      end

      def filtered(issues)
        issues.select { |i| do?(i.problem, i.particular_problem) }
      end

      private
      def setup_map(list_of_checks)
        checks = {}
        list_of_checks.each { |ck| checks[ck.category] = [] }
        list_of_checks.each { |ck| checks[ck.category] << ck.issue }
        checks.each_key { |k|
          if checks[k] == [:ALL]
            checks[k] = Set.new
          else
            checks[k] = Set.new(checks[k])
          end
        }
        checks
      end
    end
  end
end
