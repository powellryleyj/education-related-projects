#!/usr/bin/env ruby

require_relative 'checks_to_include'
require_relative 'checks_to_exclude'

require 'yaml'

# used to save checks
module UmlTools
  module ListOfChecks
# unused, and assumes there's a to_yaml method that I
#   don't have:
#   def dump_file(filename)
#     File.open(filename, "w") { |io| io.puts(to_yaml) }
#   end
#
    def self.load(yaml_string)
      YAML.load yaml_string
    end

    def self.load_file(filename)
      YAML.load_file filename
    end
  end
end
