require_relative 'issues'
require_relative 'model/sequence/full_model'
require_relative 'helper'

module UmlTools
  
  # TODO, rwh sep 2016: not clear DiffIssue is a reasonable class - IDs should not be in command client
  class DiffIssue
    attr_accessor :message, :element_id

    def initialize(msg, elem_id)
      @message = msg
      @element_id = elem_id
    end

    def to_s
      message
    end
  end

  class SequenceDiff
    attr_reader :target_model, :submission_model

    def initialize(target, submission)
      @target_model = target
      @submission_model = submission
    end

    # Sorts the enumerable then returns a comma-delimited string of the elements
    def names(diffIssues)
      ns = diffIssues.collect{ |x| x.message }
      ns.sort!
      ns.join ', '
    end

    # Splits a string by spaces, case, and underscores
    def get_individual_identifiers(word)
      result = []
      word = '' if word.nil?
      UmlTools.words_in_identifier(word).each { |x| x.split(' ').each { |y| result << y.downcase } }
      result
    end

    # Compares a name to a list of possible names. To be a match, the name
    # must contain all of the individual words in at least one of the possible
    # names. For example "a b c" would match with ["a b", "b c"], but not
    # ["a b c d"]. It only needs to match one of the possible names so it
    # would also match ["a b", "d"].
    def match_by_value(submitted_name, possible_names)
      matched = false
      submitted_names = get_individual_identifiers(submitted_name)
      possible_names.each { |name|
        subnames = get_individual_identifiers(name)
        matches = submitted_names - subnames
        if matches.empty?
          matched = true
        end
      }
      matched
    end

    #Element Ids will have to be propaigated into the msgs array, but for now are just thrown away,
    #Not enough time remaining.
    def errors
      msgs = []
      unless missing_lifeline_names.empty?
        message = "Missing lifeline(s): #{names missing_lifeline_names}"
        msgs << Issue.new(:LIFELINE_MISMATCH, :missing, message, nil)
      end
      unless extra_lifeline_names.empty?
        message = "Extra lifeline(s): #{names missing_lifeline_names}"
        msgs << Issue.new(:LIFELINE_MISMATCH, :extra, message, nil)
      end
      unless wrong_lifeline_types.empty?
        message = "Wrong-Type lifeline(s): #{names wrong_lifeline_types}"
        msgs << Issue.new(:LIFELINE_MISMATCH, :wrong_type, message, nil)
      end
      unless missing_messages.empty?
        message = "Missing message(s): #{names missing_messages}"
        msgs << Issue.new(:MESSAGE_MISMATCH, :missing, message, nil)
      end
      unless extra_messages.empty?
        message = "Extra message(s): #{names missing_messages}"
        msgs << Issue.new(:MESSAGE_MISMATCH, :extra, message, nil)
      end
      unless mislabeled_messages.empty?
        message = "Mislabeled message(s): #{names missing_messages}"
        msgs << Issue.new(:MESSAGE_MISMATCH, :mislabeled, message, nil)
      end
      unless missing_arguments.empty?
        message = "Missing message argument(s): #{names missing_arguments}"
        msgs << Issue.new(:MESSAGE_ARG_MISMATCH, :missing, message, nil)
      end
      unless extra_arguments.empty?
        message = "Extra message argument(s): #{names extra_arguments}"
        msgs << Issue.new(:MESSAGE_ARG_MISMATCH, :extra, message, nil)
      end
      unless invalid_argument_types.empty?
        message = "Message argument(s) with invalid type(s): #{names invalid_argument_types}"
        msgs << Issue.new(:MESSAGE_ARG_MISMATCH, :invalid_type, message, nil)
      end
      unless out_of_position_arguments.empty?
        message = "Message argument(s) out of order: #{names out_of_position_arguments}"
        msgs << Issue.new(:MESSAGE_ARG_MISMATCH, :out_of_order, message, nil)
      end
      msgs
    end

    def missing_lifeline_names
      if @missing_lifelines.nil?
        compare_lifelines
      end
      @missing_lifelines
    end

    def extra_lifeline_names
      if @extra_lifelines.nil?
        compare_lifelines
      end
      @extra_lifelines
    end

    def wrong_lifeline_types
      if @wrong_type_lifelines.nil?
        compare_lifelines
      end
      @wrong_type_lifelines
    end

    def missing_messages
      if @missing_messages.nil?
        compare_messages
      end
      @missing_messages
    end

    def extra_messages
      if @extra_messages.nil?
        compare_messages
      end
      @extra_messages
    end

    def mislabeled_messages
      if @mislabeled_messages.nil?
        compare_messages
      end
      @mislabeled_messages
    end

    def missing_arguments
      if @missing_arguments.nil?
        compare_messages
      end
      @missing_arguments
    end

    def extra_arguments
      if @extra_message_arguments.nil?
        compare_messages
      end
      @extra_message_arguments
    end

    def invalid_argument_types
      if @invalid_argument_types.nil?
        compare_messages
      end
      @invalid_argument_types
    end

    def out_of_position_arguments
      if @argument_out_of_position.nil?
        compare_messages
      end
      @argument_out_of_position
    end

    # Checks lifelines on the target and submission to find missing and extra lifelines
    def compare_lifelines
      @missing_lifelines = []
      @wrong_type_lifelines = []
      sub_lifelines = @submission_model.lifelines
      @target_model.lifelines.each { |lifeline|
        matches = sub_lifelines.select { |sub_l| match_lifeline_name sub_l, lifeline }
        if matches.empty?
          @missing_lifelines << DiffIssue.new(lifeline.name, nil)
        else
          wrong_type = matches.select { |l| l.type.downcase != lifeline.type.downcase }
          if !wrong_type.empty?
            @wrong_type_lifelines << DiffIssue.new("#{lifeline.name} (should be #{lifeline.type})", lifeline)
          end
        end
        sub_lifelines -= matches
      }
      @extra_lifelines = sub_lifelines.collect { |l| DiffIssue.new(l.name, l) }
    end

    # Checks messages between lifelines on the target and submission to find missing and extra messages
    def compare_messages
      @missing_messages = []
      @mislabeled_messages = []
      @missing_arguments = []
      @invalid_argument_types = []
      @extra_messages = []
      @extra_message_arguments = []
      @argument_out_of_position = []
      sub_matches = @submission_model.messages
      @target_model.messages.each { |t_message|
        # Match on sender name
        possible_matches = @submission_model.messages.select { |m| match_lifeline_name m.sender, t_message.sender }
        if possible_matches.empty?
          @missing_messages << DiffIssue.new("#{t_message.sender.name} to #{t_message.receiver.name}", nil)
        else
          # Match on receiver name
          possible_matches = possible_matches.select { |m| match_lifeline_name m.receiver, t_message.receiver }
          if possible_matches.empty?
            @missing_messages << DiffIssue.new("#{t_message.sender.name} to #{t_message.receiver.name}", nil)
          else
            # Lifelines matched, check signature
            possible_matches = possible_matches.select { |m| match_message_name m, t_message }
            if possible_matches.empty?
              @mislabeled_messages << DiffIssue.new("#{t_message.sender.name} to #{t_message.receiver.name}", t_message)
            else
              # Signature matched
              # Consider this a complete match with regard to extra messages even if arguments haven't been checked yet
              sub_matches -= possible_matches

              # Match on arguments
              possible_matches.each { |s_message|
                t_args = t_message.arguments.dup
                s_message.arguments.each { |s_arg|
                  matched = false
                  t_message.arguments.each { |t_arg|
                    if match_message_arg s_arg, t_arg
                      # Argument matched by name
                      matched = true
                      t_args.delete t_arg

                      # Check argument type
                      if t_arg.type.upcase != s_arg.type.upcase
                        @invalid_argument_types << DiffIssue.new("#{s_arg.name}:#{s_arg.type} (should be #{t_arg.type}) from #{s_message.sender.name} to #{s_message.receiver.name}", s_message)
                      end
                    end
                  }
                  # Extra arguments
                  unless matched
                    @extra_message_arguments << DiffIssue.new("#{s_arg.name}:#{s_arg.type} from #{s_message.sender.name} to #{s_message.receiver.name}", s_message)
                  end
                }
                # Missing arguments
                t_args.each { |t_arg|
                  @missing_arguments << "#{t_arg.name}:#{t_arg.type} from #{s_message.sender.name} to #{s_message.receiver.name}"
                }
                # Argument positions
                if @extra_message_arguments.empty? && @missing_arguments.empty?
                  t_message.arguments.each_with_index { |t_arg, i|
                    unless match_message_arg t_arg, s_message.arguments[i]
                      @argument_out_of_position << DiffIssue.new("Argument in position #{i} from #{s_message.sender.name} to #{s_message.receiver.name} is out of place! Should be #{t_arg.name}", s_message)
                    end
                  }
                end
              }
            end
          end
        end
      }
      # Extra messages
      sub_matches.each { |e| @extra_messages << DiffIssue.new("#{e.sender.name} to #{e.receiver.name}", e) }
    end

    def match_message_name(m, t_m)
      match_by_value m.name, t_m.valid_names
    end

    def match_lifeline_name(l, t_l)
      match_by_value l.name, t_l.valid_names
    end

    def match_message_arg(a, t_a)
      match_by_value a.name, [t_a.name]
    end
  end
end
