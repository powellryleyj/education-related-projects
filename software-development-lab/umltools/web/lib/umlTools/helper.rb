module UmlTools
# This class contains helper methods used by multiple classes
  class Helper

    # return a string of all elements in elts separated by commas
    def self.names(elts)
      ns = elts.map { |x| x.nonempty_name }
      ns.sort!
      ns.join(', ')
    end

    # return names in list (as a string), suppressing non-unique ones
    def self.unique_names(namelist)
      namelist.sort.uniq.join(', ')
    end

    def self.words_in_identifier(ident)
      caps = []
      ident.size.times { |i| caps << i if ident[i] >= ?A && ident[i] <= ?Z }
      caps = [0] + caps if caps.empty? || caps[0] != 0
      ranges = caps.zip(caps.empty? ? [] :
                            caps.last(caps.size - 1).map { |i| i - 1 })
      words = []
      ranges.each { |start, count| words << ident[start..(count || ident.size)] }
      words.map! { |word| word.split(/[ _]/) }
      words.flatten
    end
  end
end