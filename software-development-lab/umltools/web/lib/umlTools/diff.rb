# diff: compute the difference between a given (submitted) model and a target
#   model. This is separate from the UMLTools website to enable running the
#   checks from the command prompt and to keep the code separate from the
#   web application.

module UmlTools

  require_relative 'logical_diff'
  require_relative 'state_diff'
  require_relative 'sequence_diff'
  require_relative 'high_level_diff'

  $debug_pdiff = false

  def self.usage
    puts 'Usage: pdiff [-S] [-c file] [-d] [-e] [-i] [-v] target submission'
    puts '  -S: specify that the models being checked are sequence diagrams'
    puts '  -c: specify checklist (either items to include or to exclude)'
    puts '  -p: specify problem id for retrieving target yaml'
    puts '  -d: debug mode: more detailed info when not a model file'
    puts '  -e: explicit errors: print more detail on errors'
    puts '  -i: print message id numbers with messages'
    puts '  -v: verbose output: names checks as being executed'
  end

  def self.show_differences(messages)
    messages.sort!
    if messages.empty?
      unless Issue.echo?
        puts 'No differences identified, but be sure to review the model carefully'
      end
    else
      puts "#{messages.join("\n")}"
    end
  end

  # main program for computing differences
  #   "pdiff": difference in petal files, where petal is the format of a Rational
  #            Rose file; this could be renamed to model_diff                   
  #   Structure: 
  #     1. parse command-line arguments
  #     2. load the checklist
  #     3. load the target (the solution to be matched)
  #     4. load the submitted model
  #     5. perform the comparison; this includes running many of the checks
  #        used by UMLint
  #     6. write differences and warnings to standard output
  #
  # $debug_diff allows running this code without the exception handler so
  #   exceptions can be traced back to their source more easily
  #
  def self.pdiff_main(args)
    checklist = nil # issues to include/exclude in checks
    print_fname = nil # printable version of input file name
    problem_id = nil
    if args[0] == '--help' || args[0] == '-?'
      UmlTools.usage
      return 1
    end
    is_sequence = false
    # rwh, todo (Feb 2019): rename 'diagram' to 'write_diagram' or similar
    diagram = false
    #puts "Arguments: #{args.join ' '}"
    while args[0] =~ /^-/
      if args[0] == '-c' && args[1] != ''
        checklist = args[1]
        args.shift
      elsif args[0] == '-d'
        $debug_pdiff = true
      elsif args[0] == '-e'
        $explicit_errors = true
      elsif args[0] == '-i'
        Issue.set_echo_ids(true)
      elsif args[0] == '-v'
        verb = true
      elsif args[0] == '-S'
        is_sequence = true
      elsif args[0] == '-G'
        diagram = true
      elsif args[0] == '-p' && args[1] != ''
        problem_id = args[1]
        args.shift
      end
      args.shift
    end

    if checklist
      error = nil
      checklist_file = checklist
      begin
        checklist = ListOfChecks::load_file checklist_file
        error = "Error: checklist file #{checklist_file} has invalid format" unless checklist.kind_of? ListOfChecks::CheckList
      rescue Errno::ENOENT => e
        error = "Error: No such checklist file: #{checklist_file}"
      end
      if error
        puts error
        return 2
      end
    end

    usage if args.length != 2 && problem_id == nil

    if args.length == 2
      target_file = args[0]
      submit_file = args[1]
    else
      submit_file = args[0]
    end

    # Attempt to load serialized target
    problem = nil
    if problem_id
      begin
        problem = Problem.find(problem_id)
        target = YAML::load problem.serialized_model
      rescue
        target = nil
      end
    end

    high_level = problem != nil && problem.problem_word != nil

    # Target couldn't be pulled from serialized data, reparse it
    unless target || high_level
      if $debug_pdiff
        target = UmlTools::load_diagram(target_file, is_sequence)
      else
        begin
          target = UmlTools::load_diagram(target_file, is_sequence)
        rescue
          if Issue.echo?
            # don't echo file name in this case since it's displayed on the
            #   web site and the file name refers to subs/ when called from
            #   the web site
            puts "BAD_MODEL.pdiff_solution_error.Error: official solution (#{target_file}) does not appear to contain a UML model; get help"
          else
            puts "Error: official solution (#{target_file}) does not appear to contain a UML model; get help"
          end
          return 3
        end
      end
      # Save serialized target if possible
      if target && problem_id
        begin
          Problem.find(problem_id).update(serialized_model: YAML::dump(target))
        rescue
          puts 'There was an issue trying to save the serialized target back to the database!'
        end
      end
    end

    if $debug_pdiff
      submission = UmlTools::load_diagram(submit_file, is_sequence)
    else
      begin
        submission = UmlTools::load_diagram(submit_file, is_sequence)
        #File.open('rCheck.log', 'w+') { |file| file.write(diagram.to_s) }
        if diagram
           submission.generateDiagram Submission.imageFileFromModelFile submit_file
        end
      rescue => error
        File.open('umlgraderSubmission.log', 'w+') { |file| file.write error.to_s + "\n" + error.backtrace.join("\n") }
        if Issue.echo?
          puts "BAD_MODEL.submission_petal_error.Error: submitted solution (#{submit_file}) does not appear to contain a UML model"
        else
          puts "Error: submitted solution (#{submit_file}) does not appear to contain a UML model"
        end
        return 1
      end
    end

    if high_level
      diff_errors = HighLevelDiff.new(submission, problem.problem_word.nouns, problem.problem_word.verbs).errors
    elsif is_sequence
      diff_errors = SequenceDiff.new(target, submission).errors
    else
      ld = LogicalDiff.new(target, submission)
      sd = StateDiff.new(target, submission)
      diff_errors = ld.errors + sd.errors
    end

    checklist = ListOfChecks::ChecksToExclude.default is_sequence unless checklist
    checklist.exclude(:NODOC, :ALL)
    checklist.exclude(:INVISIBLE, :ALL)
    checklist.exclude(:MULTIGEN, :ALL)
    checklist.exclude(:UC_REV_INCL, :ALL)
    checklist.exclude(:UC_REV_EXT, :ALL)
    # added with Rhapsody since all attributes default to public:
    checklist.exclude(:PUBLICATTR, :ALL)

    model_errors = check_results(submission, verb, checklist, is_sequence)

    diff_errors = checklist.filtered(diff_errors) if checklist

    UmlTools.show_differences(diff_errors + model_errors)

    0
  end

  if __FILE__ == $0
    exit(pdiff_main(ARGV))
  end
end
