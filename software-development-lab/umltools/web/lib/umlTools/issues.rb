module UmlTools
# Issues found found by umlint checks
  class Issue

    # symbols identifying the category of the problem
    attr_reader :problem, :particular_problem

    attr_reader :message                # string giving the text of the issue
    # TODO: (rwh, 2016): refactor this out - it's the .id of a db element
    attr_reader :associated_element_id  # nullable string

    # whether problem identifiers are to be included in the string version of
    # of an Issue
    @@echo_problem_ids = false

    # initialize an issue using a problem (a symbol) and a message (string)
    def initialize(problem, particular_problem, message, 
                   associated_element_id = nil)
      @problem = problem
      @particular_problem = particular_problem
      @message = message
      @associated_element_id = associated_element_id
    end

    # set whether or not to echo ids (sets @@echo_problem_ids)
    def self.set_echo_ids(setting)
      @@echo_problem_ids = setting
    end

    # should problem ids be echoed?
    def self.echo?
      @@echo_problem_ids
    end

    # convert Issue into a printable string
    def to_s
      if @@echo_problem_ids
        if @associated_element_id
          "#{@problem};#{@particular_problem};#{@message};#{@associated_element_id}"
        else
          "#{@problem};#{@particular_problem};#{@message}"
        end
      else
        @message
      end
    end

    # compare issues by comparing their messages (ignoring problem idents)
    def <=>(other)
      @message <=> other.message
    end
  end

  # convert list of issues into a list of strings
  def self.msgs(issues)
    issues.map { |i| i.to_s }
  end
end
