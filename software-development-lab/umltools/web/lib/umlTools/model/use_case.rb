require_relative 'associable_element'

module UmlTools
  module Model
# Use Case in a use case model
    class UseCase < AssociableElement
      # actor_distance: number of hops (associations to traverse) to reach
      #   an actor. -1 means actor cannot be reached. This measure ignores
      #   stereotypes.
      # type: int
      attr_reader :actor_distance
      # Flag for when cannot reach actor
      @@ACTOR_NOT_REACHABLE = -1
      # extensions: list of use cases pointing to this one by an <<extend>>
      # type: list(UseCase)
      attr_reader :extensions

      # initialize with atom name, .mdl identifier (atom), string
      #   stereotype, string documentation, list(ident) supers,
      #   list(ident) dependencies
      # Also handles case where elements are simple strings and lists
      def initialize(name, id, stereotype, doc, sids, uses)
        super(name, id, stereotype, doc, sids, uses)
        @extensions = []
        @actor_distance = @@ACTOR_NOT_REACHABLE
      end

      # must be called after finding links
      # note this notion of extension is <<extend>>, not inheritance
      def record_extension(extension)
        extensions << extension
      end

      # Set whether use case is reachable from some actor
      # note: must be called after setting @extensions
      def set_actor_distance(new_distance)
        if @actor_distance == @@ACTOR_NOT_REACHABLE ||
            @actor_distance > new_distance
          @actor_distance = new_distance
          @uses.each { |u|
            if u.stereotype && u.stereotype == 'include' && u.supplier.use_case?
              u.supplier.set_actor_distance(@actor_distance + 1)
            end
          }
          @extensions.each { |u| u.set_actor_distance(@actor_distance + 1) }
          @sub_elements.each { |uc| uc.set_actor_distance(@actor_distance + 1) }
        end
      end

      # element is a use case
      def use_case?
        true
      end

      def ident
        'use case ' + (name || nonempty_name)
      end

      # can we get to an actor from this use case, ignoring the types of
      #   dependencies that are in the way?
      def reachable_from_actor?
        @actor_distance != @@ACTOR_NOT_REACHABLE
      end

      # no elements since use cases have just a name
      def all_elements
        []
      end

      def to_s
        "<UseCase #{@name}#{supers_as_string}>"
      end

      def dump
        if Element.print_just_ids
          "(usecase #{@id})"
        else
          "(UseCase #{@name}#{dump_id}#{dumped_supers}#{self.pr 'stereo', @stereotype}#{self.pr 'doc', @doc}#{self.prn 'hidden', @visible})"
        end
      end

      # Read UseCase from SExpr
      def self.read(sexpr)
        name = sexpr.find_by_path %w(UseCase)
        id = sexpr.find_by_path %w(quid)
        stereotype = sexpr.find_by_path %w(stereotype)
        doc = sexpr.find_by_path %w(documentation)
        (sups, uses) = read_super_and_uses(sexpr, 'visible_modules',
                                           'Dependency_Relationship')
        return self.new(name, id, stereotype, doc, sups, uses)
      end

      # read UseCase from REXML::Document
      def self.read_from_xmi(elt, details_table)
        name = elt.attributes['name']
        id = elt.attributes['xmi:id']
        # note: might also check visibility attribute - public seems to be visible
        stereotype = nil
        doc = nil
        sups = nil
        uses = nil
        if (details_for_item = details_table[id])
          if doc_entry = details_for_item.elements['modelDocument']
            doc = doc_entry.text
          end
        end
        return self.new(name, id, stereotype, doc, sups, uses)
      end

      # read UseCase from REXML::Document
      def self.read_from_xmi_21(elt, associations_table, dependencies_table,
          details)
        name = elt.attributes['name']
        id = elt.attributes['xmi:id']
        stereotype = nil
        doc = nil # not available in 2.1 xmi
        sups = nil
        uses = nil
        (sups, uses) = read_super_and_uses_from_xmi_21(elt, associations_table,
                                                       dependencies_table,
                                                       details)
        return self.new(name, id, stereotype, doc, sups, uses).with_21(details)
      end
    end
  end
end
