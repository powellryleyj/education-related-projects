require_relative 'role'
require_relative 'element'

module UmlTools
  module Model
# Association between two classes or use cases
    class Association < ConcealableElement
      # legal roles at the two ends of the associations
      #   In .mdl files, a_role is the information that is typically
      #   displayed at the target end of the arrow (and so typically
      #   applies to the source end). In .xmi files, I've taken info
      #   for a_role from the source end, b_role from the target end.
      #   But note the user could draw the line "backwards", effectively
      #   reversing the sense of source and target, so there is no
      #   real source or target at the model level.
      attr_reader :a_role, :b_role
      # roles that point to bad data
      attr_reader :bad_roles

      # initialize assocation from atom name, atom id, string stereotype,
      #   string documentation, Role a_role, Role b_role
      # Also handles case where elements are simple strings and lists
      def initialize(name, id, stereotype, doc, a_role, b_role)
        super(name, id, stereotype, doc)
        raise "a_role for #{name}, id #{id} must be role, not #{a_role.class}" unless a_role.kind_of? Role
        raise "b_role for #{name}, id #{id} must be role, not #{b_role.class}" unless b_role.kind_of? Role
        @a_role = a_role
        @b_role = b_role
        @a_role.association = self
        @a_role.consumer_role = @b_role
        @b_role.association = self
        @b_role.consumer_role = @a_role
        @bad_roles = []
      end

      # add bad to list of bad_roles unless it's already in the list
      def register_bad_role(bad)
        @bad_roles << bad if !@bad_roles.include? bad
      end

      # printable identifier using either its name or the names of the role
      #   objects at both ends
      def ident
        if name
          "association #{name}"
        else
          "association between #{a_role.supplier_name} and #{b_role.supplier_name}"
        end
      end

      # name displayed on line for association, if any
      def displayed_name
        if a_role.name && b_role.name
          a_role.name + '/' + b_role.name
        else
          a_role.name || b_role.name
        end
      end

      # all elements: the Role objects at both end
      def all_elements
        [@a_role, @b_role]
      end

      def has_both_suppliers?
        a_role.supplier && b_role.supplier
      end

      # is one supplier an actor and the other a use case?
      #   Really checks for classes so can treat them as actors as well
      #   (and since all actors are classes).
      def associates_actor_and_use_case?
        return false unless has_both_suppliers?
        a_supplier = a_role.supplier
        b_supplier = b_role.supplier
        a_supplier.class? && b_supplier.use_case? ||
            a_supplier.use_case? && b_supplier.class?
      end

      # is one supplier a regular class and the other a use case?
      def associates_regular_class_and_use_case?
        return false unless has_both_suppliers?
        a_supplier = a_role.supplier
        b_supplier = b_role.supplier
        a_supplier.class? && !a_supplier.actor? && b_supplier.use_case? ||
            a_supplier.use_case? && b_supplier.class? && !b_supplier.actor?
      end

      # true if some role has a name or if the association itself has a name
      def has_named_label?
        name && !name.empty? ||
            a_role && a_role.name && !a_role.name.empty? ||
            b_role && b_role.name && !b_role.name.empty?
      end

      # true if association is directed (navigable on one end but not the other)
      def directed?
        a_role && b_role && a_role.navigable? != b_role.navigable?
      end

      # true if role is at source end of a directed association
      def named_role_at_source?
        a_role && b_role &&
            (a_role.navigable? && b_role.name && !b_role.name.empty? ||
                b_role.navigable? && a_role.name && !a_role.name.empty?)
      end

# Read Assocation from SExpr
      def self.read(sexpr)
        name = sexpr.find_by_path %w(Association)
        id = sexpr.find_by_path %w(quid)
        stereotype = sexpr.find_by_path %w(stereotype)
        doc = sexpr.find_by_path %w(documentation)
        roles = sexpr.find_by_path %w(roles)
        a = roles.items[2] && Role.read(roles.items[2])
        b = roles.items[3] && Role.read(roles.items[3])
        if a && b
          if a.assoc_type == :aggregate
            a.upgrade_assoc_type_from(roles.items[3])
          elsif b.assoc_type == :aggregate
            b.upgrade_assoc_type_from(roles.items[2])
          end
        end
        assoc = self.new(name, id, stereotype, doc, a, b)
        a.container = assoc
        b.container = assoc
        return assoc
      end

      # read Association from REXML::Document
      def self.read_from_xmi(element, details_table)
        name = element.attributes['name']
        id = element.attributes['xmi:id']
        # note: might also check visibility attribute - public seems to be visible
        stereotype = nil
        doc = nil
        details_for_item = details_table[id]
        unless details_for_item
          raise "Missing details for association #{id}"
        end
        prop_entry = details_for_item.elements['properties']
        if prop_entry
          stereotype = prop_entry.attributes['stereotype']
        end
        doc_entry = details_for_item.elements['documentation']
        if doc_entry
          doc = doc_entry.attributes['value']
        end
        # currently unused:
        #prot = details_for_item.attributes['role']

        # note: see documentation for :a_role, :b_role above.
        # The a_role is taken from <source> connectors, while
        # the b_role is taken from <target> connectors. However,
        # this relates more to how the user entered the connector
        # than any true difference, so the roles can easily be
        # reversed on a real diagram.

        src = nil
        dst = nil
        member_ends = REXML::XPath.match(element, 'memberEnd')
        member_ends.each { |memberEnd|
          if memberEnd.attributes['xmi:idref'].include? 'src'
            details = details_for_item.elements['source']
            src = Role.read_from_xmi(memberEnd, details)
          else
            details = details_for_item.elements['target']
            dst = Role.read_from_xmi(memberEnd, details)
          end
        }
        # rwh, sep 2016: apparently EA can include associations with
        #   no ends, most likely from diagram notes. Create dummy ones
        #   so we do not get errors on them:
        src ||= Role.new('dummy', 	# name
			 nil,	# id
			 nil, # stereotype
                         "", 	# doc
			 nil, # supplier_id
                         nil, # multiplicity
                         false, # navigable
                         :plain)	# assoc_type
        dst ||= Role.new('dummy', 	# name
			 nil,	# id
			 nil, # stereotype
                         "", 	# doc
			 nil, # supplier_id
                         nil, # multiplicity
                         false, # navigable
                         :plain)	# assoc_type

        # b_entry = element.elements['ownedEnd']
        # b_details = details_for_item.elements['target']
        # b = Role.read_from_xmi(b_entry, b_details)

        assoc = self.new(name, id, stereotype, doc, src, dst)
        # note: .mdl version needed roles to be upgraded from
        #       aggregates to composites, but .xmi version
        #       records this information directly

        src.container = assoc
        dst.container = assoc
        return assoc
      end

      # read Association from REXML::Document, version 2.1
      def self.read_from_xmi_21(elt, details)
        name = elt.attributes['name'] # when anonymous, it will be "itsXXX"
        # where XXX is the name of the class
        id = elt.attributes['xmi:id']
        stereotype = nil
        doc = nil # always nil for XMI


        member_end_pair = elt.attributes['memberEnd']
        if member_end_pair
          member_end_pair = member_end_pair.split # will use to determine a, b roles
        else
          raise "Missing memberEnd attribute for uml:Association #{id}"
        end

        unless member_end_pair.size == 2
          raise "Missing memberEnd item for uml:Association #{id}"
        end


        if navigable_ends = elt.attributes['navigableOwnedEnd']
          navigable_ends = navigable_ends.split
        else
          navigable_ends = []
        end

        # rwh, todo: remove following documentation
        # memberEnd in min2.xmi (undirected, named association, 1-1): two words
        #   first:  xmi:id of ownedAttribute, uml:Property, anonymous
        #   second: xmi:id of ownedAttribute, uml:Property, anonymous
        # in this case, the memberend refers to an ownedAttribute with the association object set

        # association in min-bad_21uml.xmi (directed association from baseclass to stuff, unnamed)
        #   memberEnd attribute:
        #   navigableOwnedEnd attribute:
        #   ownedEnd subitems (two): these specify multiplicities, aggregation
        #   type: refers to a class

        role_list = []
        elt.elements.each('ownedEnd') { |end_elt|
          role_list << Role.read_from_xmi_21(end_elt, navigable_ends, details)
        }
        if role_list.size == 0
          # create roles from member end pair directly - this seems to happen when
          #   there's a simple bidirectional association
          # initialize(name, id, stereotype, doc, supplier_id, multiplicity,
          #            navigable, assoc_type)
          a = Role.new(nil, member_end_pair[0], nil, nil, member_end_pair[0], '1',
                       true, nil).with_21(details)
          b = Role.new(nil, member_end_pair[1], nil, nil, member_end_pair[1], '1',
                       true, nil).with_21(details)
        elsif role_list.size == 1
          # work out which already exists, which needs to be created
          if role_list[0].id == member_end_pair[0]
            a = role_list[0]
            b = Role.new(nil, member_end_pair[1], nil, nil, member_end_pair[1], '1',
                         true, nil).with_21(details)
          else
            a = Role.new(nil, member_end_pair[0], nil, nil, member_end_pair[0], '1',
                         true, nil).with_21(details)
            b = role_list[0]
          end
        else
          if role_list[0].id != member_end_pair[0]
            role_list.reverse!
          end
          a = role_list[0]
          b = role_list[1]
        end

        # it appears that composite vs. plain, vs aggregate
        #   is switched between a and b roles for 2.1; switch back
        a.assoc_type, b.assoc_type = b.assoc_type, a.assoc_type if a && b

        # note: .mdl version needed roles to be upgraded from
        #       aggregates to composites, but .xmi version
        #       records this information directly

        assoc = self.new(name, id, stereotype, doc, a, b).with_21(details)

        # rwh, todo: need following two calls?
        a.container = assoc
        b.container = assoc
        return assoc
      end

      def find_links_in(table)
        @a_role.find_links_in table
        @b_role.find_links_in table
      end

      def to_s
        if name
          "<assoc #{name} between #{a_role} and #{b_role}>"
        else
          "<assoc between #{a_role} and #{b_role}>"
        end
      end

      # return abbreviated name in lisp syntax - used by dump
      def abbrev
        if a_role.name || b_role.name
          "(assoc#{prs name}#{prs a_role.name}#{prs b_role.name})"
        elsif name
          "(assoc#{prs name})"
        else
          ''
        end
      end

      def dump
        if Element.print_just_ids
          "(association #{@id} #{a_role.dump} #{b_role.dump})"
        else
          "(association#{prs name}#{dump_id} #{a_role.dump} #{b_role.dump}#{pr 'stereo', @stereotype}#{pr 'doc', @doc}#{self.prn 'hidden', @visible}#{self.prl 'bad_roles', @bad_roles.map { |x| x.dump }})"
        end
      end
    end
  end
end
