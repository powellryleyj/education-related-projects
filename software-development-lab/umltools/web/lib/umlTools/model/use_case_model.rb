require_relative 'model'
require_relative 'use_case'
require_relative 'actor'
require_relative 'association'

module UmlTools
  module Model
# Model for use case diagram
    class UseCaseModel < Model
      attr_reader :actors, :use_cases
      # also defines @all_elements: list(Element)

      # Init model given lists of actors, use cases, and associations
      # all_elements is set to the sum of these lists
      def initialize(actors, use_cases, associations)
        super(associations)
        @actors = actors
        @use_cases = use_cases
        @all_elements = @actors + @use_cases + @associations
        @all_elements_result = nil # cached
        @visible_use_cases = nil # cached
      end

      # all association roles in the model
      # type: -> list(Association)
      def roles
        rs = @associations.map { |as| [as.a_role, as.b_role] }
        rs.flatten
      end

      # names of all actors
      # type: -> list(String)
      def actor_names
        names = @actors.map { |a| a.name }
        names.delete(nil)
        names
      end

      # all elements in the model (actors, use cases, associations)
      # type: -> list(Element)
      def all_elements
        unless @all_elements_result
          all = @all_elements + @all_elements.map { |e| e.all_elements }
          @all_elements_result = all.flatten
        end
        @all_elements_result
      end

      # return top level elements, sorted by name
      def all_sorted_elements
        @actors.sort + @use_cases.sort + @associations.sort
      end

      # all elements with names
      # type: -> list(Element)
      def all_named_elements
        all_elements.select { |e| e.name }
      end

      # all hidden elements, including unnamed ones
      # type: -> list(Element)
      def hidden_elements
        @actors.select { |x| x.hidden? } + @use_cases.select { |x| x.hidden? }
      end

      # all visible elements
      # type: -> list(Element)
      def visible_elements
        visible_actors + visible_use_cases
      end

      # all visible associable elements
      # type: -> list(Element)
      def visible_associable_elements
        visible_actors + visible_use_cases
      end

      # all visible actors
      # type: -> list(Actor)
      def visible_actors
        @actors.select { |x| x.visible? }
      end

      # all visible use cases
      # type: -> list(UseCase)
      def visible_use_cases
        @visible_use_cases = @use_cases.select { |x| x.visible? } unless @visible_use_cases
        @visible_use_cases
      end

      # Read elements of logical model from sexpr
      # Sexpr is assumed to be the result of finding 'root_usecase_package'
      # type: SExpr -> LogicalModel
      # note: caller must invoke post_process_sexpr; assumption is that post_process_sexpr
      #       will not be called until _all_ elements have been read and linked
      def self.read_elements(use_case_sexpr)
        #$stderr.puts "Looking at: [#{use_case_sexpr}]"
        raise 'Use case model not found' if use_case_sexpr.nil?

        objects = use_case_sexpr.select { |s| s.matches?('object', 'Class') }
        actors = objects.map { |o| Actor.read(o) } if objects

        objects = use_case_sexpr.select { |s| s.matches?('object', 'UseCase') }
        use_cases = objects.map { |o| UseCase.read(o) } if objects

        objects = use_case_sexpr.select { |s| s.matches?('object', 'Association') }
        associations = objects.map { |o| Association.read(o) } if objects

        mdl = UseCaseModel.new(actors, use_cases, associations)
        return mdl
      end

      # post processing for use case models - must be done after linking items
      def post_process_sexpr(logical_presentation_items)
        super

        # set extensions list for each class - must be done after finding links
        visible_use_cases.each { |uc|
          uc.uses.each { |dependency|
            if dependency.stereotype == 'extend'
              dependency.supplier && dependency.supplier.record_extension(uc)
            end
          }
        }

        # set actor_distance for each use case - must be called after
        #   setting extensions
        visible_associations.each { |as|
          if as.a_role.supplier && as.b_role.supplier
            if as.a_role.supplier.actor? && as.b_role.supplier.use_case?
              as.b_role.supplier.set_actor_distance(1)
            elsif as.a_role.supplier.use_case? && as.b_role.supplier.actor?
              as.a_role.supplier.set_actor_distance(1)
            end
          end
        }
      end

      # read use case model from s-expression, handling post-processing
      # This is intended to be a stand-alone operation when only want to read
      #   the use case model and we know there are no elements that actually
      #   belong to another model.  Clients would generally call
      #   FullModel.read instead.
      # type: SExpr -> UseCaseModel
      def self.read(sexpr)
        use_case_sexpr = sexpr.second.find_by_path %w(root_usecase_package)
        mdl = read_elements(use_case_sexpr)

        elements = mdl.actors + mdl.use_cases + mdl.associations
        # fix up links between elements
        table = {}
        elements.each { |e| table[e.id] = e }
        elements.each { |e| e.find_links_in table }
        # and complete the processing
        mdl.post_process_sexpr(use_case_sexpr.
                                   find_all_by_path(%w(logical_presentations)))
        return mdl
      end

      # Read UseCaseMode from xmi document
      # type: xmi document, details table -> UseCaseMode
      # note: caller must invoke post_process; assumption is that post_process
      #       will not be called until _all_ elements have been read and linked
      def self.read_xmi_elements(uml_model_doc, details_table)
        raise 'Use case model not found' if uml_model_doc.nil?

        actors = []
        associations = []
        use_cases = []
        uml_model_doc.elements.each { |x|
          if x.attributes['name'] == 'Use Case Model'
            actor_objects = x.elements.select { |elt|
              elt.attributes['xmi:type'] == 'uml:Actor'
            }
            actors += actor_objects.map { |o| Actor.read_from_xmi(o, details_table) }
            assoc_objects = x.elements.select { |elt|
              elt.attributes['xmi:type'] == 'uml:Association'
            }
            associations += assoc_objects.map { |o|
              Association.read_from_xmi(o, details_table)
            }
            case_objects = x.elements.select { |elt|
              elt.attributes['xmi:type'] == 'uml:UseCase'
            }
            use_cases += case_objects.map { |o|
              UseCase.read_from_xmi(o, details_table)
            }
          end
        }

        mdl = UseCaseModel.new(actors, use_cases, associations)
        return mdl
      end

      # Read UseCaseModel from version 2.1 xmi document
      # type: xmi document -> UseCaseModel
      # note: caller must invoke post_process; assumption is that post_process
      #       will not be called until _all_ elements have been read and linked
      def self.read_xmi_21_elements(xmidoc, associations_table,
          dependencies_table, details)
        actors = []
        associations = []
        use_cases = []

        xmidoc.elements.each('xmi:XMI/uml:Model//packagedElement') { |elt|
          #puts "processing element w/ type #{elt.attributes['xmi:type']}, name #{elt.attributes['name']}"
          if elt.attributes['xmi:type'] == 'uml:Actor'
            actors << Actor.read_from_xmi_21(elt, associations_table,
                                             dependencies_table, details)
          elsif elt.attributes['xmi:type'] == 'uml:UseCase'
            use_cases << UseCase.read_from_xmi_21(elt, associations_table,
                                                  dependencies_table,
                                                  details)
          end
        }

        associations = select_associations(actors + use_cases, associations_table)

        mdl = UseCaseModel.new(actors, use_cases, associations)
        return mdl
      end

      # convert use case model to string for debugging
      def to_s
        "UseCaseModel[\n  " + @all_elements.join("\n  ") + "\n]"
      end

      # Dump lisp-style string of model
      def dump
        items = all_sorted_elements.map { |x| x.dump }
        "(UseCaseModel\n  " + items.join("\n  ") + "\n)"
      end
    end
  end
end
