require_relative 'element'

module UmlTools
  module Model
    class State < Element
      # Lists of xmi ID's for the incoming and outgoing transitions of the state
      attr_reader :incoming_transitions, :outgoing_transitions

      def initialize(name, id, stereotype, doc, incoming_transitions, outgoing_transitions)
        super(name, id, stereotype, doc)
        @incoming_transitions = incoming_transitions
        @outgoing_transitions = outgoing_transitions
      end

      def add_outgoing_transition(transition)
        @outgoing_transitions << transition
      end

      def add_incoming_transition(transition)
        @incoming_transitions << transition
      end

      def self.read_from_xmi(element, details_table, transitions)
        name = element.attributes['name']
        id = element.attributes['xmi:id']
        incoming = REXML::XPath.match(element, 'incoming')
        outgoing = REXML::XPath.match(element, 'outgoing')
        details_for_item = details_table[id]
        stereotype = nil
        doc = nil

        outgoing_transitions = []
        incoming_transitions = []

        # For each of the <outgoing> elements, we want to find the right transition element
        # that we parsed earlier
        outgoing.each { |outgoing_element|
          outgoing_id = outgoing_element.attributes['xmi:idref']

          transitions.each { |transition|
            if outgoing_id == transition.id
              outgoing_transitions << transition
            end
          }
        }

        incoming.each { |incoming_element|
          incoming_id = incoming_element.attributes['xmi:idref']

          transitions.each { |transition|
            if incoming_id == transition.id
              incoming_transitions << transition
            end
          }
        }

        if (details_for_item)
          doc = details_for_item.elements['properties'].attributes['documentation']
          stereotype = details_for_item.elements['properties'].attributes['stereotype']
        end

        state = State.new(name, id, stereotype, doc, incoming_transitions, outgoing_transitions)

        # set the new state as the source or target node of the transitions as appropriate
        state.incoming_transitions.each { |transition|
          transition.target_node = state
        }

        state.outgoing_transitions.each { |transition|
          transition.source_node = state
        }

        return state
      end
    end
  end
end
