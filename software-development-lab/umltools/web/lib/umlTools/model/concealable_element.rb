require_relative 'element'

module UmlTools
  module Model
# elements which can have the property of being visible or invisible
# "Visible" means in some diagram that appears on the screen
    class ConcealableElement < Element
      # Is element visible?
      attr_writer :visible

      # Init element from atom name, atom identifier, string stereotype,
      #   and string documentation
      # Initializes element to be visible; will reset when process appropriate
      #   part of .mdl file
      def initialize(n, id, stereotype, doc)
        super(n, id, stereotype, doc)
        @visible = false
      end

      # Is element visible?  Assumes have processed all model elements to
      #   set visibility attribute appropriately
      def visible?
        @visible
      end

      # Is element invisible?  Assumes have processed all model elements to
      #   set visibility attribute appropriately
      def hidden?
        !visible?
      end
    end

    class Connector < ConcealableElement
      # string .mdl identifier
      attr_reader :supplier_id
      # Element at source end of dependency
      attr_reader :supplier
      # element at destination end, currently set for just xmi 2.1 files
      attr_accessor :source_id
      attr_reader :source
      # alternative way to look up the dependency for xmi 2.1 files
      attr_accessor :client_id
      #name to be displayed when dumped
      attr_reader :dump_identifier

      def initialize(n, id, stereotype, doc, supplier_id=nil, dump_name='connector')
        super(n, id, stereotype, doc)
        @supplier_id = supplier_id
        @dump_identifier = dump_name
      end

      def to_s
        "<#{ident}#{pr 'stereo', @stereotype}>"
      end

      # either specified name of supplier or "unknown"
      def supplier_name
        @supplier && @supplier.name || '[unknown]'
      end

      # read data for dependency; supplier is initially just an id, but is
      #   filled in with the actual object later
      def self.read(sexpr)
        name = sexpr.find_by_path %w(label)
        id = sexpr.find_by_path %w(quid)
        stereo = sexpr.find_by_path(%w(stereotype))
        doc = sexpr.find_by_path %w(documentation)
        supplier_id = sexpr.find_by_path(%w(quidu))
        dep = self.new(name, id, stereo, doc, supplier_id && supplier_id.head)
        return dep
      end

      # resolve links in table unless @supplier is already set
      # type: Map(id, Element) -> void
      def find_links_in(table)
        if @supplier_id.nil? && @name && !@name.empty?
          candidates = table.values.select { |x| x.name == @name }
          if candidates.size == 1
            @supplier_id = candidates[0].id
            @name = nil # only use name to get at destination
          end
        end
        @supplier = @supplier || table[@supplier_id]
        @source = @source || table[@source_id]
      end

      def dump
        if Element.print_just_ids
          "(#{@dump_identifier} #{@id}#{prs @supplier && @supplier.id})"
        else
          "(#{@dump_identifier}#{prs @name}#{dump_id}#{pr 'stereo', @stereotype}#{prs @supplier && @supplier.name}#{pr 'doc', @doc})"
        end
      end

      # read data for generalization arrow; supplier is initially just an id,
      #   but is filled in with the actual object later
      # rwh, todo: merge with similar code in association.read_from_xmi
      # elt is from the extensions section
      # elt -> the non-extensions class element
      # details -> the extensions section for the class element
      def self.read_from_xmi(element, details_table)
        id = element.attributes['xmi:id']
        supplier_id = element.attributes['end'] # base class

        details_for_item = details_table[id]
        if details_for_item
          name = details_for_item.attributes['name']
          stereo = details_for_item.elements['properties'].attributes['stereotype']
          doc = details_for_item.elements['documentation'].attributes['value']
        else
          raise "Missing details for #{element.name} #{id}"
        end

        self.new(name, id, stereo, doc, supplier_id)
      end
    end
  end
end
