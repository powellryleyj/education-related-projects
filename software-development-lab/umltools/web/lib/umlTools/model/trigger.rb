require_relative 'element'

module UmlTools

  module Model
    class Trigger < Element
      attr_reader :type, :expression

      def initialize(name, id, stereotype, doc, type, expression)
        super(name, id, stereotype, doc)
        @type = type
        @expression = expression
      end

      def self.read_from_xmi(element, details_table)
        name = element.attributes['name']
        id = element.attributes['xmi:id']
        event = REXML::XPath.first(element, 'event')
        type = event.attributes['xmi:type']
        expression = nil

        if type == 'uml:ChangeEvent'
          expression = REXML::XPath.first(event, 'changeExpression').attributes['symbol']
        elsif type == 'uml:TimeEvent'
          expression = REXML::XPath.first(event, 'when').attributes['expr']
        end

        Trigger.new(name, id, nil, nil, type, expression)
      end
    end
  end
end
