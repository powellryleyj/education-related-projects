require_relative 'concealable_element'

module UmlTools
  module Model
# The generalization arrow between two classes
#   Needed because these can be hidden, so need to make the relationship
#   explicit to give a place to hang the 'hidden' flag.
#   since the two share a lot in common
    class GeneralizationRelationship < Connector


      # init from atom name, atom .mdl ident, string stereotype, string
      #   documentation, string .mdl ident for supplier
      def initialize(n, id, stereotype, doc, supplier_id)
        super(n, id, stereotype, doc, supplier_id, 'subclass_of')
        # will reset following below
        @supplier = nil
      end

      def ident
        # Note: generalizations can have names, though it's rare
        'generalization_relationship ' +
            (name || ('of ' + ((@supplier ? @supplier.name : nil) || @supplier_id)))
      end

      # is a dependency
      def generalization_relationship?
        true
      end

      # read data for generalization arrow; supplier is initially just an id,
      #   but is filled in with the actual object later
      # rwh, todo: merge with similar code in association.read_from_xmi
      # rwh, todo: find out if can have stereotype, documentation, name for
      #   generalization relationships in xmi files
      # elt is from the extensions section
      def self.read_from_xmi_21(elt, details)
        name = nil # rwh, todo: is there a way to read name for generalization?
        id = elt.attributes['xmi:id']
        supplier_id = elt.attributes['general'] # base class

        stereo = 'generalization'
        doc = nil # no doc

        gen = self.new(name, id, stereo, doc, supplier_id).with_21(details)
      end
    end
  end
end
