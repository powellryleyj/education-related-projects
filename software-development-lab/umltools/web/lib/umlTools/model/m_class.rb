require_relative 'associable_element'
require_relative 'attribute'
require_relative 'method'
require_relative '../petal_utils'

# Represents a class in the model
module UmlTools
  module Model
    class MClass < AssociableElement
      # list(Attribute), list(Method)
      attr_reader :attributes, :methods

      # for xmi formats only; list of associations hiding as attributes
      attr_accessor :covert_xmi_associations

      # initialize with atom name, .mdl identifier (atom), string
      #   stereotype, string documentation, list(ident) supers,
      #   list(Attributes), list(Methods), list(ident) uses
      # Also handles case where elements are simple strings and lists
      # uses: objects that have a dynamic relationship
      def initialize(name, id, stereotype, doc, sids, attributes, methods, uses)
        super(name, id, stereotype, doc, sids, uses)
        @attributes = attributes || []
        @methods = methods || []
        @all_elements = nil # cached
      end

      # Complete initialization using given table - identifies supers, uses
      # type: Map(id->Element) -> void
      # ids that don't map to proper elements are handled by recording an error
      def find_links_in(table)
        super(table)
        @attributes.each { |x| x.container = self }
        @methods.each { |x| x.container = self }
      end

      # returns printable identifier, marking hidden classes as hidden
      def ident
        if visible?
          'class ' + (name || nonempty_name)
        else
          'class ' + (name || nonempty_name) + ' (hidden)'
        end
      end

      # element is a named class
      def class?
        true
      end

      # element can take on the role of an actor
      def actor?
        @stereotype == 'actor'
      end

      # does class name represent a template?
      def template?
        # tollerant version:
        self.name && self.name =~ /<.*>/
        # version which checks for tight definition:
        #self.name.delete(' ') =~ /^\w*<[\w,<>*]*>$/
      end

      # is this a standard library class?
      def in_library?
        LibraryClass.find(name)
      end

      # name of type (sans pointers, references, etc.) in container
      # returns nil if class name does not represent a template
      def contained_type
        return nil if self.name.nil?
        sans_spaces = self.name.delete ' '
        lt_ix = sans_spaces.index '<'
        if lt_ix && sans_spaces[-1] == '>'[0]
          the_type = sans_spaces[(lt_ix+1)..-2]
          return the_type.delete('*&[]0-9')
        end
        nil
      end

      def sorted_attributes
        @attributes.sort
      end

      def sorted_methods
        @methods.sort
      end

      # all attributes, methods, and generalization elements
      def all_elements
        @all_elements = @attributes + @methods + @super_relationships unless @all_elements
        @all_elements
      end

      def to_s
        "<MClass #{@name}#{supers_as_string}/#{@attributes.join(', ')}/#{@methods.join(', ')}>"
      end

      def dump
        attr_descr = sorted_attributes.map { |x| x.dump }
        meth_descr = sorted_methods.map { |x| x.dump }
        if Element.print_just_ids
          "(class #{id} #{self.prl 'attributes', attr_descr}#{self.prl 'methods', meth_descr})"
        else
          "(MClass #{@name}#{dump_id}#{dumped_supers}#{self.pr 'stereo', @stereotype}#{self.prl 'attributes', attr_descr}#{self.prl 'methods', meth_descr}#{self.pr 'doc', @doc}#{self.prn 'hidden', @visible})"
        end
      end

      # read MClass from SExpr
      def self.read(sexpr)
        name = sexpr.find_by_path %w(Class)
        id = sexpr.find_by_path %w(quid)
        stereotype = sexpr.find_by_path %w(stereotype)
        doc = sexpr.find_by_path %w(documentation)
        attrs = sexpr.find_by_path %w(class_attributes)
        load_errors = []
        begin
          if attrs
            attrs = Attribute.read_list attrs
          end
        rescue Exception => e
          attrs = []
          load_errors << e.message
        end
        mets = sexpr.find_by_path %w(operations)
        begin
          if mets
            mets = Method.read_list mets
          end
        rescue Exception => e
          mets = []
          load_errors << e.message
        end
        (sups, uses) = read_super_and_uses(sexpr, 'used_nodes',
                                           'Uses_Relationship')
        newobj = self.new(name, id, stereotype, doc, sups, attrs, mets, uses)
        newobj.load_errors = load_errors
        return newobj
      end

      # read MClass from REXML::Document
      def self.read_from_xmi(elt, details_table)
        name = elt.attributes['name']
        id = elt.attributes['xmi:id']
        # note: might also check visibility attribute - public seems to be visible
        stereotype = nil
        doc = nil
        sups = nil
        uses = nil
        details_for_item = details_table[id]
        if !(details_for_item)
          raise "Missing details for class #{name}"
        end

        doc_entry = details_for_item.elements['properties'].attributes['documentation']
        if (doc_entry)
          doc = doc_entry
        end

        stereotype_entry = details_for_item.elements['properties'].attributes['stereotype']
        if (stereotype_entry)
          stereotype = stereotype_entry
        end

        attributes = []
        operations = []
        # attributes, operations can be retrieved by the following, but all
        #  of the real information is in the details section
        # elt.elements.each('ownedAttribute') { |e|
        #   attributes << Attribute.read_from_xmi(e, details_table) }
        # elt.elements.each('ownedOperation') { |e|
        #   attributes << Method.read_from_xmi(e, details_table) }
        if details_for_item.elements['attributes']
          details_for_item.elements['attributes'].each_element { |e|
            attributes << Attribute.read_from_xmi(e)
          }
        end

        if details_for_item.elements['operations']
          details_for_item.elements['operations'].each_element { |e|
            operations << Method.read_from_xmi(e)
          }
        end
        (sups, uses) = read_super_and_uses_from_xmi(details_for_item, details_table)
        newobj = self.new(name, id, stereotype, doc, sups,
                          attributes, operations, uses)
        return newobj
      end

      # read MClass from version 2.1 REXML::Document
      def self.read_from_xmi_21(elt, associations_table, dependencies_table,
          details)
        name = elt.attributes['name']
        id = elt.attributes['xmi:id']
        # note: might also check visibility attribute - public seems to be visible
        stereotype = nil
        doc = nil # always empty in xmi files
        sups = nil
        uses = nil

        attributes = []
        operations = []
        sups = []
        covert_associations = [] # from associations masquerading as attributes

        # Process attributes of class.
        # note: Associations are read first, so associations_table will contain
        #       links to associated associations; in some cases the supplier
        #       ids need to be set to the id of the containing class
        elt.elements.each('ownedAttribute') { |e|
          if aid = e.attributes['association']
            # xmi:id is the property id, and the corresponding association in the
            #   table may need its supplier id updated
            assoc = associations_table[aid]
            # debugging code:
            # [as, bs] = [assoc.a_role.supplier_id, assoc.b_role.supplier_id]
            # puts "Found association #{aid}, a.suid = #{as}, b.suid = #{bs}"
            attribute_id = e.attributes['xmi:id']
            if attribute_id && assoc.a_role && assoc.a_role.supplier_id == attribute_id
              assoc.a_role.supplier_id = e.attributes['type']
              assoc.a_role.multiplicity = Role.read_cardinality_xmi_21(e)
              # puts "Updating a_role supplier id to #{assoc.a_role.supplier_id}"
              # and grab name if it's provided
              assoc.a_role.name ||= e.attributes['name']
            end
            if attribute_id && assoc.b_role && assoc.b_role.supplier_id == attribute_id
              assoc.b_role.supplier_id = e.attributes['type']
              assoc.b_role.multiplicity = Role.read_cardinality_xmi_21(e)
              # puts "Updating b_role supplier id to #{assoc.b_role.supplier_id}"
              assoc.b_role.name ||= e.attributes['name']
            end
          elsif (type_attr = e.attributes['type']) &&
              (profile_elt = details[type_attr]) &&
              (target_id = profile_elt.attributes['base_Class'])
            # this is shown as an attribute, but it's really an association

            # have association from elt (packagedElement representing a class)
            #  to base_Class elt (that is, profile_elt.attributes['base_Class']
            #  is the id of the target object
            # So set a_role.supplier_id to be elt.attributes['xmi:id'] and
            #  b_role.supplier_id to be profile_elt.attributes['base_Class']

            # role.initialize(name, id, stereotype, doc, supplier_id, multiplicity,
            #                 navigable, assoc_type)
            # supplier
            a_role = Role.new(nil, '$role_a_' + ($generated_ids += 1).to_s,
                              nil, nil, id, '1', false, :plain)
            # consumer
            b_role_multiplicity = Role.read_cardinality_xmi_21(e)
            b_role = Role.new(e.attributes['name'],
                              '$role_b_' + ($generated_ids += 1).to_s,
                              nil,
                              profile_elt.attributes['description'],
                              target_id, b_role_multiplicity, true, :plain)
            # association.initialize(name, id, stereotype, doc, a_role, b_role)
            new_assoc = Association.new(nil,
                                        '$assoc_' + ($generated_ids += 1).to_s,
                                        nil, nil, a_role, b_role)
            a_role.container = new_assoc
            b_role.container = new_assoc
            new_assoc.visible = true
            covert_associations << new_assoc
          else
            # regular attribute
            attributes << Attribute.read_from_xmi_21(e, details)
          end
        }
        elt.elements.each('ownedOperation') { |e|
          operations << Method.read_from_xmi_21(e, details)
        }

        (sups, uses) = read_super_and_uses_from_xmi_21(elt, associations_table,
                                                       dependencies_table,
                                                       details)

        newobj = self.new(name, id, stereotype, doc, sups,
                          attributes, operations, uses).with_21(details)
        newobj.covert_xmi_associations = covert_associations

        return newobj
      end
    end # MClass
  end
end
