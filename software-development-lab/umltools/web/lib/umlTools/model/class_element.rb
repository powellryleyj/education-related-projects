module UmlTools
  module Model
# element that occurs within a class
    class ClassElement < Element
      # protection level: :public, :private, :protected, or nil
      # base_name is name without class qualifiers (i.e. 'c' in 'a::b::c')
      attr_reader :protection, :base_name

      # initialize with atom name, .mdl identifier (atom), string
      #   stereotype, string documentation, protection level as a string
      # Also handles case where elements are simple strings and lists
      def initialize(name, id, stereotype, doc, protection)
        super(name, id, stereotype, doc)
        @protection = Element.read_protection(protection)
        @base_name = @name && (@name.split('::')[-1] || @name)
      end

      # is this item in a standard library class?
      def in_library_class?
        lc = LibraryClass.find(@container.name)
        lc
      end

      def dump
        publicity = @protection ? " #{@protection}" : ''
        if Element.print_just_ids
          "(elt #{@id})"
        elsif @container
          "(elt (. #{@container.name} #{@name}#{dump_id})#{publicity}#{pr 'stereo', @stereotype}#{pr 'doc', @doc})"
        else
          "(elt #{@name}#{publicity}#{pr 'stereo', @stereotype}#{pr 'doc', @doc})"
        end
      end
    end
  end
end