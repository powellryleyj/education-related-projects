require_relative 'concealable_element'

module UmlTools
  module Model
    # Transition between two states
    class Transition < Connector
      # Represent the states that the machine starts and ends in, respectively
      attr_accessor :source_node, :target_node, :guard, :effect, :triggers

      def initialize(name, id, stereotype, doc, guard, effect, triggers)
        super('', id, stereotype, doc)
        @guard = guard
        @effect = effect
        @triggers = triggers
        @source_node = nil
        @target_node = nil
      end

      def self.read_from_xmi(element, details_table, triggers_hash)
        id = element.attributes['xmi:id']
        guard = element.elements['guard/specification']
        guard_condition = guard ? guard.attributes['body'] : nil

        effect = element.elements['effect'] ? element.elements['effect'].attributes['body'] : nil
        details_for_item = details_table[id]
        stereotype = nil
        doc = nil
        triggers = []

        # Transitions can have trigger elements as children that contain only the trigger id.
        # The actual trigger information is elsewhere in the xmi and is passed in similar to
        # how the details_table is
        REXML::XPath.match(element, 'trigger').each { |trigger_element|
          triggers << triggers_hash[trigger_element.attributes['xmi:idref']]
        }

        if details_for_item
          doc = details_for_item.elements['documentation'].attributes['value']
          stereotype = details_for_item.elements['properties'].attributes['stereotype']
        end

        Transition.new(name, id, stereotype, doc, guard_condition, effect, triggers)
      end

      def nonempty_name
        return source_node.name + '->' + target_node.name
      end
    end
  end
end
