require_relative 'model'
require_relative 'initial_node'
require_relative 'final_node'
require_relative 'transition'
require_relative 'state'
require_relative 'trigger'

module UmlTools
  module Model
# Model for state diagram
    class StateModel < Model
      attr_reader :states, :transitions, :initial_nodes, :final_nodes

      # Init model given lists of states, transitions, initial nodes, and final nodes
      # all_elements is set to the sum of these lists
      def initialize(states, transitions, initial_nodes, final_nodes)
        super([])
        @states = states
        @transitions = transitions
        @initial_nodes = initial_nodes
        @final_nodes = final_nodes
        @all_elements = @states + @transitions + @final_nodes + @initial_nodes
      end

      # all elements in the model (states, transitions and initial/final nodes)
      # type: -> list(Element)
      def all_elements
        @all_elements
      end

      # all elements with names
      # type: -> list(Element)
      def all_named_elements
        all_elements.select { |e| e.name }
      end

      # all hidden elements, including unnamed ones
      # type: -> list(Element)
      def hidden_elements
        @all_elements.select { |x| x.hidden? }
      end

      # all visible elements
      # type: -> list(Element)
      def visible_elements
        @all_elements.select { |x| x.visible? }
      end

      def self.read_xmi_elements(uml_model_doc, details_table)
        raise 'State model not found' if uml_model_doc.nil?

        states = []
        transitions = []
        initial_nodes = []
        final_nodes = []
        triggers = Hash.new()


        REXML::XPath.match(uml_model_doc, '//packagedElement[@xmi:type="uml:Trigger"]').each { |trigger_element|
          trigger = Trigger.read_from_xmi(trigger_element, details_table)
          triggers[trigger.id] = trigger
        }

        REXML::XPath.match(uml_model_doc, '//region').each { |region_element|
          transition_elements = REXML::XPath.match(region_element, 'transition')
          state_elements = []
          initial_node_elements = []
          final_node_elements = []
          transitions += transition_elements.map { |transition_element|
            Transition.read_from_xmi(transition_element, details_table, triggers)
          }

          # Parse the <subvertex> elements, which are the states and the final node
          REXML::XPath.match(region_element, 'subvertex').each { |subvertex|
            case subvertex.attributes['xmi:type']
              when 'uml:FinalState'
                final_node_elements << subvertex
              when 'uml:Pseudostate'
                initial_node_elements << subvertex
              when 'uml:State'
                state_elements << subvertex
            end
          }
          final_nodes += final_node_elements.map { |final_node|
            FinalNode.read_from_xmi(final_node, details_table, transitions)
          }
          initial_nodes += initial_node_elements.map { |initial_node|
            InitialNode.read_from_xmi(initial_node, details_table, transitions)
          }
          states += state_elements.map { |state_node|
            State.read_from_xmi(state_node, details_table, transitions)
          }
        }

        mdl = StateModel.new(states, transitions, initial_nodes, final_nodes)
        return mdl
      end
    end
  end
end
