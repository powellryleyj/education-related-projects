require_relative 'class_element'

module UmlTools
  module Model
    # Attribute in a class
    class Attribute < ClassElement
      # Name of type as a string
      attr_reader :type

      # Initialize attribute but with protection level as well
      # Also handles case where elements are simple strings and lists
      def initialize(name, id, type, stereotype, doc, protection)
        super(name, id, stereotype, doc, protection)
        @type = type && (type.is_a?(SAtom) ? type.name : type)
      end

      def ident
        @container ? "attribute #{name} in #{@container.name}" :
            "attribute #{name}"
      end

      def attribute?
        true
      end

# unused:
#     # is this method a standard attribute in a standard library class?
#     # is this attribute a standard attribute in a standard library class?
#     def library_class_member?
#       false     # currently no library classes have any recorded class members
#     end

      def to_s
        "(Attr #{@name})"
      end

      # Read Attribute from SExpr
      def self.read(sexpr)
        name = sexpr.find_by_path %w(ClassAttribute)
        id = sexpr.find_by_path %w(quid)
        type = sexpr.find_by_path %w(type)
        stereotype = sexpr.find_by_path %w(stereotype)
        doc = sexpr.find_by_path %w(documentation)
        prot = sexpr.find_by_path %w(exportControl)
        return self.new(name, id, type, stereotype, doc,
                        (prot && prot.head) || 'Private')
      end

      # read Attribute from REXML::Document
      #   This reads from the extensions section, so no details
      #   parameter (since elt is from the details of the caller)
      def self.read_from_xmi(elt)
        name = elt.attributes['name']
        id = elt.attributes['xmi:idref']
        type = nil
        stereotype = nil
        doc = nil
        prot = nil

        if properties_entry = elt.elements['properties']
          type = properties_entry.attributes['type']
        end
        if stereotype_entry = elt.elements['stereotype']
          stereotype = stereotype_entry.attributes['stereotype']
        end
        if doc_entry = elt.elements['documentation']
          doc = doc_entry.attributes['value']
        end
        if x = elt.attributes['scope']
          prot = x
        end

        return self.new(name, id, type, stereotype, doc, prot || 'Private')
      end

      # read Attribute from REXML::Document
      def self.read_from_xmi_21(elt, details)
        name = elt.attributes['name']
        id = elt.attributes['xmi:id']
        type_id = elt.attributes['type']
        type = nil
        stereotype = nil
        doc = nil
        prot = nil

        if properties_entry = elt.elements['properties']
          type = properties_entry.attributes['type']
        end
        if stereotype_entry = elt.elements['stereotype']
          stereotype = stereotype_entry.text
        end
        if doc_entry = elt.elements['document']
          doc = doc_entry.text
        end
        if x = elt.attributes['visibility']
          prot = x
        end

        return self.new(name, id, type, stereotype, doc, prot || 'Private').
            with_21(details)
      end

      # Read list(Attribute) from SExpr
      def self.read_list(sexpr)
        raise "unexpected atom for attributes: #{sexpr.head}" if sexpr.atom?
        attrs = []
        sexpr.items.each do |o|
          if o.matches?('object', 'ClassAttribute')
            attrs << Attribute.read(o)
          end
        end
        return attrs
      end

      def dump
        publicity = @protection ? " #{@protection}" : ''
        if Element.print_just_ids
          "(attr #{@id})"
        elsif @container
          "(attr (. #{@container.name} #{@name}#{dump_id})#{pr 'type', @type}#{publicity}#{pr 'stereo', @stereotype}#{pr 'doc', @doc})"
        else
          "(attr #{@name}#{dump_id}#{publicity}#{pr 'type', @type}#{pr 'stereo', @stereotype}#{pr 'doc', @doc})"
        end
      end
    end
  end
end
