require_relative 'class_element'

module UmlTools
  module Model
# Method in a class
    class Method < ClassElement

      # initialize with atom name, atom .mdl id, string stereotype, string
      #  documentation, string protection
      # Also handles case where elements are simple strings and lists
      def initialize(name, id, stereotype, doc, protection)
        super(name, id, stereotype, doc, protection)
      end

      def ident
        @container ? "operation #{name} in #{@container.name}" :
            "operation #{name}"
      end

      # element is a method
      def method?
        true
      end

      # element is a C++ operator identifier
      def operator?
        self.name && self.name.delete(' ') =~ /^operator\W+$/
      end

# unused:
#     # is this method a standard attribute in a standard library class?
#     def library_class_member?
#       lc = LibraryClass.find(@container.name)
#       return lc && lc.methods.include?(name)
#     end

      def to_s
        '(Method ' + @name + ')'
      end

      # read Method from SExpr
      def self.read(sexpr)
        name = sexpr.find_by_path %w(Operation)
        id = sexpr.find_by_path %w(quid)
        stereotype = sexpr.find_by_path %w(stereotype)
        doc = sexpr.find_by_path %w(documentation)
        prot = sexpr.find_by_path %w(opExportControl)
        return self.new(name, id, stereotype, doc,
                        (prot && prot.head) || 'Public')
      end

      # read Method from REXML::Document
      #   This reads from the extensions section, so no details
      #   parameter (since elt is from the details of the caller)
      def self.read_from_xmi(elt)
        name = elt.attributes['name']
        id = elt.attributes['xmi:idref']
        stereotype = nil
        doc = nil
        prot = nil

        if stereotype_entry = elt.elements['stereotype']
          stereotype = stereotype_entry.attributes['stereotype']
        end
        if doc_entry = elt.elements['documentation']
          doc = doc_entry.attributes['value']
        end
        if x = elt.attributes['scope']
          prot = x
        end

        return self.new(name, id, stereotype, doc, prot || 'Public')
      end

      # read Method from REXML::Document, version 2.1
      #   - reads an ownedOperation - the ownedParameter is ignored
      def self.read_from_xmi_21(elt, details)
        name = elt.attributes['name']
        id = elt.attributes['xmi:id']
        if x = elt.attributes['visibility']
          prot = x
        end

        stereotype = nil
        doc = nil # no documentation in 2.1 files
        prot = nil

        #
        # rwh, todo: following will never be true
        #
        if stereotype_entry = elt.elements['stereotype']
          stereotype = stereotype_entry.text
        end
        if x = elt.attributes['visibility']
          prot = x
        end

        return self.new(name, id, stereotype, doc, prot || 'Public').
            with_21(details)
      end

      # read list(Method) from SExpr
      def self.read_list(sexpr)
        raise "unexpected atom for methods: #{sexpr.head}" if sexpr.atom?
        attrs = []
        sexpr.items.each do |o|
          if o.matches?('object', 'Operation')
            attrs << Method.read(o)
          end
        end
        return attrs
      end
    end
  end
end
