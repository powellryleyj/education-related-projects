require_relative 'concealable_element'

module UmlTools
  module Model
    # A final node in a State Diagram
    class FinalNode < ConcealableElement

      attr_reader :incoming

      def initialize(name, id, stereotype, doc, incoming)
        super(name, id, stereotype, doc)
        @incoming = incoming
      end

      # element is a final node
      def final_node?
        true
      end

      # read Final Node from REXML::Document
      def self.read_from_xmi(elt, details_table, transitions)
        name = elt.attributes['name']
        id = elt.attributes['xmi:id']
        incoming = REXML::XPath.match(elt, 'incoming')
        stereotype = nil
        doc = nil

        incoming_transitions = []

        incoming.each { |incoming_element|
          incoming_id = incoming_element.attributes['xmi:idref']

          transitions.each { |transition|
            if incoming_id == transition.id
              incoming_transitions << transition
            end
          }
        }

        details_element = details_table[id]
        if details_element
          details_element_properties = details_element.elements['properties']
          stereotype = details_element_properties.attributes['stereotype']
          doc = details_element_properties.attributes['documentation']
        end

        final = FinalNode.new(name, id, stereotype, doc, incoming_transitions)

        final.incoming.each { |transition|
          transition.target_node = final
        }

        return final
      end
    end
  end
end
