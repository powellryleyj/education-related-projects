require_relative 'generalization_relationship'
require_relative 'dependency'

module UmlTools
  module Model
    # abstract class for element which can be involved in association and/or
    #    inheritance
    class AssociableElement < ConcealableElement
      # list(GeneralizationRelationship): all super class gens for this element
      attr_reader :super_relationships
      # all elements this has a dynamic dependency upon
      attr_reader :uses
      # all elements which make up this element including by inheritance
      # for classes these are attributes, methods, and subclasses
      attr_reader :sub_elements
      # list(Role): all other items associated with this element
      #    These are the items that would be declared as class members
      #    for the current class in an implementation
      attr_reader :associates
      # also: duplicated_associations - list(list(association)) of all
      #   associations that point to the same destination

      # type: atom X atom X label|nil X string X list(GeneralizationRelationship)
      #       X list(string) -> void
      # Also handles case where elements are simple strings and lists
      # initialize element where sids are super ids and uses is a list
      #   of ids of elements upon which this element depends
      # Note: caller must call find_links_in to complete the initialization
      def initialize(name, id, stereotype, doc, supers, uses)
        super(name, id, stereotype, doc)
        @super_relationships = supers || []
        @uses = uses || []
        @sub_elements = []
        @associates = []
        @visible_supers = nil # caches results
        @duplicated_associations = nil # caches results
        @member_roles = nil # cached results
      end

      def supers
        @super_relationships.map { |x| x.supplier }
      end

      def visible_supers
        if @visible_supers.nil?
          visible = @super_relationships.select {
              |x| x.visible? && x.supplier.visible?
          }
          @visible_supers = visible.map { |x| x.supplier }
        end
        @visible_supers
      end

      # Complete initialization using given table - identifies supers, uses
      # type: Map(id->Element) -> void
      # ids that don't map to proper elements are handled by recording an error
      def find_links_in(table)
        @super_relationships.each { |sr|
          sr.find_links_in(table)
          if sr.supplier.nil?
            @load_errors << "reference to missing super: #{sr.supplier_id}"
          else
            sr.supplier.add_sub_element(self)
          end
        }
        @super_relationships.delete_if { |s| s.supplier.nil? }
        @uses = @uses.each { |i| i.find_links_in(table) }
      end

      # add Element to list of subelements for this element
      def add_sub_element(elt)
        @sub_elements << elt
      end

      # add assoc role as an associate to this class
      # only adds each role once
      # type: Role -> void
      def add_associate(assoc_role)
        @associates << assoc_role
      end

      # visible associates - connection is visible
      # type: -> list(Role)
      def visible_associates
        unless @visible_associates
          @visible_associates = @associates.select { |r|
            r.visible? && r.association && r.association.visible? &&
                r.supplier && r.supplier.visible?
          }
        end
        @visible_associates
      end

      # What roles would result in members for the current class.
      # These are visible roles that can be reached from the
      #  current class, respecting navigability and looking
      #  only at visible associations. Uses the fact that all
      #  actors are also classes.
      # Caches results for speed.
      # type: -> list(Role)
      def member_roles
        unless @member_roles
          @member_roles = visible_associates.select { |r|
            r.supplier.class? &&
                (r.navigable? || !r.navigable? && r.consumer_role &&
                    !r.consumer_role.navigable?)
          }
        end
        @member_roles
      end

      # returns list of (visible) associations which refer to the same
      #  end point
      # type: -> list(list(Role))
      def duplicated_associations
        unless @duplicated_associations
          @duplicated_associations = []
          as = visible_associates
          while !as.empty?
            first = as.pop
            if first.supplier && first.supplier.role
              same = as.select { |a| false }
            end
          end
        end
        @duplicated_associations
      end

      # Returns the list of supers for this element as a string
      #   Subclasses use this to generate documentation
      def supers_as_string
        if @super_relationships.empty?
          ''
        else
          ' - from ' + (supers.map { |s|
            if !s.nil?
              (s.name || s.nonempty_name)
            else
              'Unnamed Element'
            end
          }).join(',')
        end
      end

      # Returns super list for dumping
      # intent is that subclasses would add these to dumped elements
      def dumped_supers
        if Element.print_just_ids
          ''
        else
          "#{self.prl 'extends', supers.map { |s| (s.name || s.nonempty_name) }}" +
              "#{self.prl 'uses', @uses.map { |x| x.dump }}"
        end
      end

      # read data for super ids and uses ids; uses_target is target string
      #   for items in uses relationships; dependency_target is the
      #   text associated with an object node
      # type: SExpr X string X string -> list(string) X list(string)
      def self.read_super_and_uses(sexpr, uses_target, dependency_target)
        sups = sexpr.find_by_path %w(superclasses)
        if sups
          all_supers = sups.select { |s|
            s.matches?('object', 'Inheritance_Relationship') }
          sups = all_supers.map { |s| GeneralizationRelationship.read(s) }
        end
        uses = sexpr.find_by_path [uses_target]
        if uses
          all_uses = uses.select { |s| s.matches?('object', dependency_target) }
          uses = all_uses.map { |s| Dependency.read(s) }
        end
        [sups, uses]
      end

      # read super and uses ids from XMI data
      #   elt: element that can have a super
      # type: SExpr X string X string -> list(string) X list(string)
      def self.read_super_and_uses_from_xmi(item_with_super_and_uses, details_table)
        generalizations = []
        dependencies = []
        links = item_with_super_and_uses.elements['links']

        if links
          generalizations = links.get_elements 'Generalization'
          if generalizations
            generalizations = generalizations.map { |x|
              GeneralizationRelationship.read_from_xmi(x, details_table)
            }
          end

          dependencies = links.get_elements 'Dependency'
          if dependencies
            dependencies = dependencies.map { |element|
              Dependency.read_from_xmi(element, details_table)
            }
          end
        end

        [generalizations, dependencies]
      end

      # read super and uses ids from XMI data, version 2.1
      #   elt: element that can have a super
      # type: SExpr X string X string -> list(string) X list(string)
      def self.read_super_and_uses_from_xmi_21(elt, associations_table,
          dependencies_table, details)
        supers = []
        uses = []

        # sets supplier_id - need to go back and set class
        elt.elements.each('generalization') { |e|
          if e.attributes['xmi:type'] == 'uml:Generalization'
            supers << GeneralizationRelationship.read_from_xmi_21(e, details)
          end
        }

        # rwh, todo: make this more efficient by computing it just once
        by_client_ids = {}
        dependencies_table.values.each { |d|
          by_client_ids[d.client_id] = d if d.client_id
        }

        elt.elements.each('include') { |e|
          if e.attributes['xmi:type'] == 'uml:Include'
            id = e.attributes['xmi:id']
            if (dep = dependencies_table[id])
              dep.stereotype = 'include'
            elsif (dep = by_client_ids[e.attributes['addition']])
              dep.stereotype = 'include'
            else
              # no uml:Include, so construct a new one; either source
              #   or destination could be nil
              source = e.attributes['includingCase']
              destination = e.attributes['addition']
              # name is an alternative way to get at the destination
              name = destination ? nil : e.attributes['name']
              dep = Dependency.new(name, id, 'include', nil, destination).
                  with_21(details)
              dep.source_id = source
            end
            uses << dep
          end
        }

        elt.elements.each('extend') { |e|
          if e.attributes['xmi:type'] == 'uml:Extend'
            id = e.attributes['xmi:id']
            if (dep = dependencies_table[id])
              dep.stereotype = 'extend'
            elsif (dep = by_client_ids[e.attributes['extendedCase']])
              dep.stereotype = 'extend'
            else
              # no uml:Extend, so construct a new one; either source
              #   or destination could be nil
              source = e.attributes['extension']
              # destination will be name of object, not an id number
              #   not clear what happens if two objects have the same name!
              destination = e.attributes['extendedCase']
              # name is an alternative way to get at the destination
              name = destination ? nil : e.attributes['name']
              dep = Dependency.new(name, id, 'extend', nil, destination).
                  with_21(details)
              dep.source_id = source
            end
            uses << dep
          end
        }

        client_dependencies = elt.attributes['clientDependency'] || ''
        client_dependencies.split.each { |d_id|
          dep = dependencies_table[d_id]
          uses << dep if dep
        }

        [supers, uses]
      end
    end
  end
end
