# require_relative 'extensions'

module UmlTools
  module Model
# Role at source of association
# It's not clear this is concealable, but the uniformity helps
# rwh, todo: rename consumer, supplier - these are not obvious from
#   looking at the diagram; I believe the supplier is the class that
#   would be the type of an instance of the role, and the consumer
#   is the class that would hold that instance.
    class Role < ConcealableElement
      # string identifier for supplier, supplier element, string multiplicity,
      #   boolean navigability
      attr_accessor :supplier_id, :supplier # writeable for version 2.1 files
      attr_accessor :multiplicity # writeable for version 2.1 files
      # Assocation, Role of object at other end of association
      # also: :is_navigable (through navigable? method)
      attr_accessor :association, :consumer_role
      # Type of assocation: :plain, :aggregate, :composite
      # nomanclature:
      #   * hollow diamond: aggregation => relatively weak relationship
      #   * filled diamond: composite => deep memory management (ownership)
      # Aggregate: marked as aggregate in model, containment of "by reference"
      # Composite: marked as aggregate, containment of "by value"
      attr_accessor :assoc_type

      # Init from atom name, atom .mdl identifier, string stereotype,
      #  string documentation, string .mdl identifier of supplier,
      #  string multiplicity, boolean navigability, symbol association type
      def initialize(name, id, stereotype, doc, supplier_id, multiplicity,
                     navigable, assoc_type)
        super(name, id, stereotype, doc)
        @supplier_id =
            supplier_id &&
                (supplier_id.is_a?(SExpr) && supplier_id.atom? ? supplier_id.head : supplier_id)
        @multiplicity = multiplicity
        @is_navigable = navigable
        @assoc_type = assoc_type
        @visible = true # roles are always visible
        # initialize remaining items to nil - will reset below
        @supplier = nil
      end

      def navigable?
        @is_navigable
      end

      # make it possible to update the name, but only if it's not set
      #   and only for a role
      def name=(new_name)
        @name ||= new_name
      end

      # return name of supplier (that is, source class) in a printable form
      #   generally can match these names - the exceptions cover broken models
      def supplier_name
        if @supplier && @supplier.name
          @supplier.name
        elsif @supplier_id && @supplier_id != 0
          '' + @supplier_id
        else
          'unknown element'
        end
      end

      # is there a supplier and does that supplier have a name?
      def has_supplier?
        @supplier && @supplier.name
      end

      # if appropriate, make this Role name anonymous - used for xmi 2.1 files
      #   Like Element, but looks at @supplier instead
      def delete_anonymous_xmi_name
        # $stderr.puts "}}} anonymous? #{@name}, #{@supplier && @supplier.name}" if @name
        if @name && @supplier && @supplier.has_name? &&
            @name =~ /^its#{@supplier.name.cap_first}(_[0-9]*)?$/
          @name = nil
          # $stderr.puts "--- deleted"
        end
      end

      # return name of consumer (that is, name of role at other end)
      #   or return nil if there is no associated consumer role object
      #   (which would probably indicate a broken model)
      def consumer_role_name
        @consumer_role && @consumer_role.name
      end

      # does the consumer have a role name
      def consumer_role_has_name?
        c = consumer_role_name
        c && !c.empty?
      end

      # object at other end of association, if defined
      def consumer
        @consumer_role && @consumer_role.supplier
      end

      # object at other end of association, if defined as an MClass
      def consumer_class
        c = @consumer_role && @consumer_role.supplier
        c && c.class? ? c : nil
      end

      # name using role names from either end of association (or both)
      # assumption: one side or the other has a name
      def full_name
        if has_name? && consumer_role_has_name?
          name + '/' + consumer_role_name
        else
          consumer_role_name
        end
      end

      # printable name, using association if necessary
      def ident
        if name
          'role ' + (name || nonempty_name) + ' from ' + supplier_name
        else
          'role ' + supplier_name + ' in association with ' +
              consumer_role.supplier_name
        end
      end

      # is the multiplicity undefined?
      def missing_multiplicity?
        (@multiplicity.nil? || @multiplicity == '') && !composite?
      end

      # is multiplicity specified to be 0, 1, or implied by containment
      def single_element_multiplicity?
        return composite? || (%w(0 1 0..0 0..1 1..1).include?(@multiplicity) && !@multiplicity.nil?)
      end

      # is multiplicity specified to be multiple element
      def multiple_element_multiplicity?
        return !@multiplicity.nil? && !single_element_multiplicity?
      end

      # specified multiplicity if given, 1 if multiplicity is 1..1 or
      #   in a composite; always returns a string ("" if no multiplicity)
      def effective_multiplicity
        if @multiplicity == '1..1'
          '1'
        elsif @multiplicity && !@multiplicity.empty?
          @multiplicity
        elsif composite?
          '1'
        else
          ''
        end
      end

      # read Role from SExpr
      def self.read(sexpr)
        name = sexpr.find_by_path %w(Role)
        quid = sexpr.find_by_path %w(quid)
        stereotype = sexpr.find_by_path %w(stereotype)
        doc = sexpr.find_by_path %w(documentation)
        suid = sexpr.find_by_path %w(quidu)
        card = sexpr.find_by_path %w(client_cardinality cardinality)
        nav = sexpr.find_by_path %w(is_navigable)
        is_aggr = sexpr.find_by_path %w(is_aggregate)
        return self.new(name, quid, stereotype, doc, suid,
                        card ? card.head : nil,
                        nav ? nav.head == 'TRUE' : false, # default: false
                        # start by assuming aggregate; will upgrade to
                        #   composite if other side is by value:
                        is_aggr && is_aggr.head == 'TRUE' ? :aggregate : :plain
        )
      end

      # read Role from REXML::Document
      # elt is from the base portion, details from the extension portion
      def self.read_from_xmi(elt, details)
        name = details.elements['string(style/@value)'][/(?<=alias=)(.*)(?=;)/]
        id = elt.attributes['xmi:idref']
        stereotype = nil
        doc = nil
        suid = details.attributes['xmi:idref']
        card = nil
        navigable = false
        assoc = nil

        stereotype = details.elements['role'].attributes['stereotype']

        doc_entry = details.elements['documentation']
        if (doc_entry)
          doc = doc_entry.attributes['value']
        end

        type_entry = details.elements['type']
        if (type_entry)
          card = type_entry.attributes['multiplicity']
          aggr = type_entry.attributes['aggregation']
          assoc = if aggr == 'composite'
                    :composite
                  elsif aggr == 'shared'
                    :aggregate
                  else
                    :plain
                  end

        end

        modifier_entry = details.elements['modifiers']
        if (modifier_entry)
          navigable = modifier_entry.attributes['isNavigable'] == 'true'
        end

        newobj = self.new(name, id, stereotype, doc, suid, card, navigable, assoc)
        return newobj
      end

      def self.read_cardinality_xmi_21(elt)
        # general principles, at least for Rhapsody:
        #   - "ownedAttribute" covers both simple and complex attributes and
        #     means there would be a declaration for that item in the source
        #     class.
        #   - If the upper or lower value is not listed at all, it is assumed
        #     the cardinality is 1.
        #   - Giving a lower value without a 'value' attribute means that
        #     lower value is _not_ specified; either the upper value gives
        #     the cardinality or there is no cardinality when there should
        #     have been one.
        upper_elt = elt.elements['upperValue']
        lower_elt = elt.elements['lowerValue']
        if upper_elt.nil? && lower_elt.nil?
          return '1'

        end
        upper = upper_elt && upper_elt.attributes['value']
        # If the lower limit does not exist but the upper does,
        #   assume a lower limit of 1
        if lower_elt
          lower = lower_elt.attributes['value']
          # but if lower_elt exists but upper does not, assume 1:
          lower = '1' if lower.nil? && upper_elt.nil?
        else
          lower = '1'
        end
        if upper && lower && lower != upper
          card = "#{lower}..#{upper}"
        else
          # note: can get here with upper and lower both being nil; this means
          #       upperValue and/or lowerValue were specified but no value
          #       entry is there for each, and is the only case where the
          #       cardinality is not being specified by the user.
          card = upper || lower
        end
        #puts "Reading upper #{upper_elt}"
        #puts "    and lower #{lower_elt}"
        #puts "Resulting card: #{card}"
        card
      end

      # read Role from REXML::Document, version 2.1
      def self.read_from_xmi_21(elt, navigable_ends, details)
        # at an "ownedEnd" object
        name = elt.attributes['name'] # if anonymous, will be itsXXX where XXX is the class
        quid = elt.attributes['xmi:id'] # id of element at end of conn
        doc = nil # always empty

        unless elt.attributes['xmi:type'] == 'uml:Property'
          raise "Unexpected ownedEnd object #{quid}"
        end

        stereotype = nil # rwh, todo: how to set?
        suid = nil
        card = nil
        navigable = navigable_ends.include? quid
        assoc = nil
        assoc_type = nil

        suid = elt.attributes['type']

        if x = elt.attributes['visibility'] # rwh, todo: what to do with this?
          prot = x
        end

        if elt.attributes['aggregation'] == 'composite'
          assoc_type = :composite
        elsif elt.attributes['aggregation'] == 'aggregate'
          assoc_type = :aggregate
        else
          assoc_type = :plain
        end

        # card will be nil if nothing is specified, but will be 1
        #    if it is appropriate to use the default, 1
        card = read_cardinality_xmi_21(elt)

        # initialize(name, id, stereotype, doc, supplier_id, multiplicity,
        #            navigable, assoc_type)
        return self.new(name, quid, stereotype, doc, suid, card,
                        navigable, assoc_type).with_21(details)
      end

      # set up supplier from table (unless supplier is already set)
      # missing supplier is logged as an error
      # type: Map(id, Element) -> void
      def find_links_in(table)
        if @supplier.nil?
          if @supplier_id.nil?
            if @association
              @association.register_bad_role(self)
            else
              @load_errors << "role #{@id} missing supplier"
            end
          else
            tmp_supplier = table[@supplier_id]
            if tmp_supplier
              @supplier = tmp_supplier
            else
              if @association
                @association.register_bad_role(self)
              else
                @load_errors << "reference to missing supplier: #{@supplier_id}"
              end
            end
          end
        end
      end

      # does role represent an aggregation?
      def aggregate?
        @assoc_type == :aggregate
      end

      # does role represent a composite?
      def composite?
        @assoc_type == :composite
      end

      # is multiplicity acceptable for a composite relationships?
      def ok_composite_multiplicity?
        @multiplicity.nil? || @multiplicity.empty? ||
            @multiplicity == '1' || @multiplicity == '1..1'
      end

      def to_s
        role_name = name ? " (name: #{name})" : ''
        navigability = navigable? ? 'navigable' : ''
        "(Role#{@supplier && @supplier.name && (' ' + @supplier.name)}" +
            "#{role_name}#{prs @multiplicity}#{prs navigability} #{@assoc_type})"
      end

      def dump
        role_name = name ? " (name #{name})" : ''
        navigability = navigable? ? 'navigable' : ''
        if Element.print_just_ids
          "(role #{@id}#{@supplier && @supplier.name && (' '+@supplier.id)})"
        else
          "(role#{dump_id}#{@supplier && @supplier.name && (' '+@supplier.name)}"+
              role_name +
              prs(@multiplicity) +
              prs(navigability) +
              " #{@assoc_type}" +
              prs(@container.abbrev) +
              pr('stereo', @stereotype) +
              pr('doc', @doc) +
              ')'
        end
      end

      # refine association type using information about the role at the
      #   other end of an association
      # Must be public to allow calling from Role.read, but not intended to
      #   be called externally
      def upgrade_assoc_type_from(other_role)
        containment = other_role.find_by_path %w(Containment)
        if containment && containment.head == 'By Value'
          @assoc_type = :composite
        end
      end
    end
  end
end
