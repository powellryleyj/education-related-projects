#<argument xmi:type="uml:LiteralString" xmi:id="EAID_AR000000_E17D_4a7e_92B7_4B9D90678E18" name="shoppingListID"/>
module UmlTools
  module Model
    module Sequence
      class MessageArgument
        attr_reader :id, :name, :type
        def initialize(id, name, type)
          @id = id
          @name = name
          @type = type
        end
      end
    end
  end
end