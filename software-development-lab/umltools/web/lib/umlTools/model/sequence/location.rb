module UmlTools
  module Model
    module Sequence
      class Location
        attr_reader :top, :bottom, :left, :right
        attr_writer :top, :bottom, :left, :right
        def initialize
          @top = top
          @bottom = bottom
          @left = left
          @right = right
        end
      end
    end
  end
end