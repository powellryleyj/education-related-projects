require_relative 'lifeline'
require_relative 'location'
require_relative 'message'
require_relative 'message_argument'
require 'cairo'

module UmlTools
  module Model
    module Sequence
        class FullModel
          # Actors, Controllers, Models, etc
          attr_reader :lifelines
          # Connections between lifelines
          attr_reader :messages
          # Errors encountered while parsing
          attr_reader :all_load_errors


          def initialize(lifelines, messages)
            @lifelines = lifelines
            @messages = messages
            @all_load_errors = []

            # TODO, rwh 2016: rip this crap out
            if false
              #loads images into surfaces for diagram
              basePath = 'app/assets/images/sequence diagram resources/'
              
              controlFname = basePath + 'Control.png'
              entityFname = basePath + 'Entity.png'
              actorFname = basePath + 'Actor.png'
              boundaryFname = basePath + 'Boundary.png'
              @control = Cairo::ImageSurface.from_png controlFname
              @entity = Cairo::ImageSurface.from_png entityFname
              @actor = Cairo::ImageSurface.from_png actorFname
              @boundary = Cairo::ImageSurface.from_png boundaryFname
            end
          end

          def self.read_from_xmi(doc)
            ## TODO: Fragments (loops, etc)
            @all_load_errors = []

            if doc.nil?
              @all_load_errors << 'XMI document cannot be nil!'
              raise
            end

            # Occurrence mapping
            occurrences = Hash.new
            doc.get_elements('//fragment').each { |frag|
              occurrences[frag.attributes['xmi:id']] = frag.attributes['covered']
            }


            if occurrences.empty?
              @all_load_errors << 'Occurrence mapping was not found!'
              return
            end


            # Lifelines
            lifelines = Hash.new
            doc.get_elements('//lifeline').each { |lifeline|
              lid = lifeline.attributes['xmi:id']
              name = lifeline.attributes['name']
              id = lifeline.attributes['represents']

              # Get lifeline type&stereotype from extensions
              element = doc.elements["//element[@xmi:idref='#{lid}']"]
              element_id = nil
              if element.nil?
                # Try to use ownedAttribute to find the extension
                oa = doc.elements["//ownedAttribute[@xmi:id='#{id}']"]
                begin
                  element_id = oa.elements['type'].attributes['idref']
                  element = doc.elements["//element[@xmi:idref='#{element_id}']"]
                rescue
                  @all_load_errors << 'Unable to find element in extensions!'
                end
                if element.nil?
                  @all_load_errors << 'Unable to find element in extensions!'
                end
              end
              type = element.attributes['xmi:type']
              stereotype = element.elements['properties'].attributes['stereotype']
              valid_names = []
              if element.elements['properties'].attributes.include? 'documentation'
                valid_names += element.elements['properties'].attributes['documentation'].split ', '
              end
              begin
                location_string = doc.elements["//element[@subject='#{lid}']"].attributes['geometry']
              rescue
                # For some unknown and probably stupid reason actors aren't directly referenced like every other type...
                # Try to use the owned attribute to get the type to then find the location string. If this fails
                # then we're pretty much SoL.
                if element_id.nil?
                  @all_load_errors << 'Element ID was not found for an actor, cannot find geometry information'
                else
                  location_string = doc.elements["//element[@subject='#{element_id}']"].attributes['geometry']
                end
              end
              location = Location.new
              location_string.split(';').each { |geo|
                parts = geo.downcase.split '='
                location.send parts[0] + '=', parts[1].to_i
              }
              lifelines[lid] = Lifeline.new id, type, name, stereotype, location, valid_names
            }
            # Messages
            messages = []
            doc.get_elements('//message').each { |message|
              id = message.attributes['xmi:id']
              kind = message.attributes['messageKind'] # What is this? ex. complete
              sort = message.attributes['messageSort'] # What is this? ex. synchCall
              name = message.attributes['name']
              sender = lifelines[occurrences[message.attributes['sendEvent']]]
              receiver = lifelines[occurrences[message.attributes['receiveEvent']]]
              arguments = []
              message.get_elements('argument').each { |arg|
                arguments.push MessageArgument.new(arg.attributes['id'], arg.attributes['name'],
                                                   arg.attributes['type'])
              }

              # Load connector for setting additional attributes of the message after creation
              connector = doc.elements["//connector[@xmi:idref='#{id}']"]
              valid_names = []
              documentation = connector.elements['documentation']
              if documentation.attributes.has_key? 'value'
                valid_names += documentation.attributes['value'].split ', '
              end
              message = Message.new id, name, kind, sort, sender, receiver, arguments, valid_names
              # Guard condition
              extended_prop = connector.elements['extendedProperties']
              if extended_prop.attributes.has_key? 'conditional'
                message.guard = extended_prop.attributes['conditional']
              end
              messages.push message
            }
            FullModel.new lifelines.values, messages
          end

          # Read model from text string
          def self.read(text, format = nil)
            if format == :xmi || (format.nil? && text[0] == '<')
              doc = REXML::Document.new(text)
              model = self.read_from_xmi(doc)
            else
              ## TODO: Add support for other format (Rhapsody?)
            end
          end

          # Load in the file
          def self.load(file, format = nil)
            if file.nil? || file.empty?
              text = $stdin.read
            elsif !File.exists?(file)
              raise "No such file: #{file}"
            else
              text = IO.read(file)
            end
            model = self.read(text, format)
            model
          end

          # TODO rwh 2016: rip out
          def getLifelineSurface(stereotype, type)
            raise NotImplementedError
            #lifeline image
            curImage = @control

            if stereotype == 'entity'
                curImage = @entity
            elsif stereotype == 'control'
              curImage = @control
            elsif type == 'uml:Actor'
              curImage = @actor
            elsif stereotype == 'boundary'
              curImage = @boundary
            end

            return curImage
          end

          # TODO rwh 2016: rip out
          #I am ashamed of this method, but I don't have the time to fix it.
          #Null checks are missing, and text alignment is terrible.
          #I write poor code at 8am in the morning...
          def generateDiagram(fname)
            messageXOffset = {}
            width = 0
            height = 480

            padding_width = 16

            messageY = 128
            messageYGap = 32
            messageTextYOffset = 8
            messageTextXOffset = 16

            arrowOffset = 4

            for lifeline in @lifelines
              rightX = lifeline.location.right
              if rightX > width
                width = rightX
              end
            end

            width += padding_width * 2

            surface = Cairo::ImageSurface.new(width, height)
            context = Cairo::Context.new surface

            context.set_font_size(11)

            #white backfill
            oldSurface = context.source
            context.set_source_rgb(255, 255, 255)
            context.paint
            context.set_source oldSurface

            for lifeline in @lifelines
              loc = lifeline.location
              loc.left += padding_width

              curImage = getLifelineSurface lifeline.stereotype, lifeline.type

              #draws lifeline
              messageXOffset[lifeline] = curImage.width/2
              context.move_to loc.left + messageXOffset[lifeline], loc.top + curImage.height/2
              context.line_to loc.left + messageXOffset[lifeline], loc.bottom

              #draws lifeline name
              context.move_to loc.left, loc.top - 4
              context.show_text lifeline.name
              context.stroke
            end

            for message in @messages
              senderLoc = message.sender.location
              receiveLoc = message.receiver.location

              startX = senderLoc.left + messageXOffset[message.sender]
              destX = receiveLoc.left + messageXOffset[message.receiver]
              modifier = 1;

              if (senderLoc.left > receiveLoc.left)
                modifier = -1;
              end

              #message arrow
              context.move_to startX, messageY
              context.line_to destX, messageY
              context.line_to destX - (modifier * arrowOffset), messageY - arrowOffset
              context.move_to destX, messageY
              context.line_to destX - (modifier * arrowOffset), messageY + arrowOffset

              #message text
              startTextX = startX + messageTextXOffset
              if (modifier == -1)
                startTextX = destX + messageTextXOffset
              end


              messageText = (message.name || '') + '('
              if (message.arguments.nil?)
                messageText += message.arguments.collect{|x| x.name}.join(', ')
              end
              messageText += ')'
              if message.guard != nil
                messageText = '[' + message.guard + ']' + messageText
              end

              #calculates message text offset
              messageLineLength = (startX - destX).abs
              messageTextLength = (messageText.length * 9);
              # puts messageTextLength.to_s + '/' + messageLineLength.to_s
              messageTextXOffset = (messageTextLength < messageLineLength) ?
                                    (messageLineLength - messageTextLength)/2.0 : 4

              #draws message text
              context.move_to startTextX + messageTextXOffset, messageY - messageTextYOffset
              context.show_text messageText

              context.stroke
              messageY += messageYGap
            end

            for lifeline in @lifelines
              curImage = getLifelineSurface lifeline.stereotype, lifeline.type
              loc = lifeline.location

              context.set_source curImage, loc.left, loc.top
              context.paint
            end

            surface.write_to_png fname
          end
        end
    end
  end
end
