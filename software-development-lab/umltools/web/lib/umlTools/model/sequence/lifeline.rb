#
# TODO (rwh sep2016): refactor this so don't have such a silly class
# TODO (rwh sep 2016): I'm not even convinced this is used anywhere!
#
module UmlTools
  module Model
    module Sequence
      class Lifeline
        attr_reader :id, :type, :name, :stereotype, :valid_names, :location
        def initialize(id, type, name, stereotype, location, valid_names = [])
          @id = id
          @type = type
          @name = name
          @stereotype = stereotype
          @location = location
          @valid_names = valid_names
          @valid_names << name
        end
      end
    end
  end
end
