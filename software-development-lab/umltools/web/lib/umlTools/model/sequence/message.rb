#<message xmi:type="uml:Message" xmi:id="EAID_C9F28A3B_E467_495d_9FB5_6277D04959C4" messageKind="complete" messageSort="synchCall" sendEvent="EAID_FR000000_3DD5_42d1_A556_C098681014ED" receiveEvent="EAID_FR000000_7A79_474f_A6DC_2921F932D788"/>
module UmlTools
  module Model
    module Sequence
      class Message
        attr_reader :id, :name, :message_kind, :message_sort, :sender, :receiver, :arguments, :valid_names
        attr_accessor :guard
        def initialize(id, name, message_kind, message_sort, sender, receiver, arguments, valid_names)
          @id = id
          @name = name
          @message_kind = message_kind
          @message_sort = message_sort
          @sender = sender
          @receiver = receiver
          @arguments = arguments
          @guard = nil
          @valid_names = valid_names
          @valid_names << name
        end
      end
    end
  end
end