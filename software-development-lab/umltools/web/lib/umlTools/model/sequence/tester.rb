require_relative 'full_model'
require('rexml/document')
module UmlTools
  # TODO: rwh 2016: move this to test environment!
  model = Model::Sequence::FullModel.load File.join(File.dirname(__FILE__), 
                                                    'StudentSequenceDiagram.xmi'
                                                   )
  model.generateDiagram 'diagram.png'
end
