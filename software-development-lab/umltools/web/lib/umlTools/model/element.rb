require_relative '../sexpr.rb'

module UmlTools
  module Model
# Base class for all model elements
    class Element
      include Comparable

      # Identification string (hex code from .mdl file)
      attr_reader :id
      # Containing element, typically a MClass or UseCase
      attr_accessor :container
      # String giving stereotype in lower case, if set; otherwise nil
      attr_accessor :stereotype
      # String giving original documentation, if set; otherwise nil
      #   doc_words gives this documention as an array of words;
      #   doc_words is only set if necessary.
      #   Doc is pre-processed to convert to \n line formats.
      attr_reader :doc
      # load_errors: array of strings containing error messages if any
      #              - load recovers gracefully from all such errors
      attr_accessor :load_errors

      @@print_ids = false

      def self.print_ids=(value)
        @@print_ids = value
      end

      def self.print_ids
        @@print_ids
      end

      @@print_just_ids = false

      def self.print_just_ids=(value)
        @@print_just_ids = value
      end

      def self.print_just_ids
        @@print_just_ids
      end

      # Initialize element given name, id, stereotype, and doc
      #   Handles the case where any one of these is from the .mdl
      #   file (in which case they will be sexprs). If they aren't
      #   from sexprs, the types are string X string X string X string.
      # <b>NOTE</b>: after initialization, creator will need to ensure
      #   find_links_in is called to finalize setup
      # Initializes load errors to the empty list.
      def initialize(n, id, stereotype, doc)
        @load_errors = []
        @name = n && n.is_a?(SExpr) && n.atom? ? n.head : n
        if id.nil?
          #raise "Missing id field for #{self.class}" if $debug_petal
          @load_errors << "Missing id field for #{self.class}"
          @id = '[unknown]'
        else
          @id = id.is_a?(SExpr) && id.atom? ? id.head : id
        end

        @stereotype = stereotype &&
            (stereotype.is_a?(SExpr) ? stereotype.head.downcase : stereotype.downcase)
        @stereotype = nil if @stereotype && @stereotype.empty?

        doc_text = doc && (doc.is_a?(SExpr) ? doc.name : doc)
        set_doc(doc_text)
      end

      :private

      def set_doc(text)
        @doc = nil
        if text && !text.empty?
          @doc = text.gsub("\n\r", "\n").gsub("\r\n", "\n").gsub("\r", "\n")
        end
        @doc_words = nil
        @doc
      end

      :public

      # comparison of real names; if either is unnamed, !=
      #  Allows other to be nil
      def ==(other)
        name && other && other.name && name == other.name
      end

      # compares names; if both are unnamed, comparison is ==
      # used to compare ids as well, but those are too volatile
      def <=>(other)
        nonempty_name <=> other.nonempty_name
      end

      # add revised information and return self
      def with_21(details)
        if e = details[id]
          set_doc(e.attributes['description'])
          if display_name = e.attributes['displayName']
            @name = display_name
          end
        end
        self
      end

      # is this element named?
      def has_name?
        n = name
        n && !n.empty?
      end

      # Name of element as given in the .mdl file or nil if element is unnamed
      # type: -> String
      def name
        if @name && !@name.empty? && @name !~ /UNNAMED/
          @name
        else
          nil
        end
      end

      # if appropriate, make this name anonymous - used for xmi 2.1 files in which
      def delete_anonymous_xmi_name
        # $stderr.puts ">>> anonymous? #{@name}, #{@container && @container.name}" if @name
        if @name && @container && @container.has_name? &&
            @name =~ /^its#{@container.name.cap_first}(_[0-9]*)?$/
          @name = nil
        end
      end

      def doc_words
        unless @doc_words
          @doc_words = @doc.nil? ? [] : @doc.split(' ')
        end
        @doc_words
      end

      # name of element, including UNNAMED if applicable
      # NEVER returns nil or an empty string
      # type: -> String
      def nonempty_name
        if @name && !@name.empty?
          @name
        else
          'Unnamed Element'
        end
      end

      # set up links between elements; for base class, there is nothing to do
      # type: Map(id->Element) -> void
      def find_links_in(table)
        # nothing to do
      end

      # return (reasonably unique) string giving element identifier
      # assumption: returned string always has a prefix such as "class" or
      #   "actor" and that prefix is in lower case
      # default: element + name
      def ident
        'element ' + (name || '[unnamed]')
      end

      def dump_id
        @@print_ids ? " #{@id} " : ''
      end

      # does element represent a class?
      def class?
        false
      end

      # does element represent an attribute?
      def attribute?
        false
      end

      # does element represent a method?
      def method?
        false
      end

      # does element represent a dependency?
      # note dependencies are often used to represent dynamic associations in
      #   class models and include/extend associations in use case models
      def dependency?
        false
      end

      # does element represent a generalization relationship link?
      def generalization_relationship?
        false
      end

      # does element represent a use case?
      def use_case?
        false
      end

      # does element represent an actor?
      def actor?
        false
      end

      # does element represent an initial node?
      def initial_node?
        false
      end

      # does element represent a final node?
      def final_node?
        false
      end

      # does element have documentation?
      # fault-tolerant: allows for nil or empty string
      def has_doc?
        @doc && !@doc.empty?
      end

      # dump element - default: return empty string
      def dump
        ''
      end

      protected

      # convert protection from a string to a symbol
      # results: :public, :private, :protected, or nil
      def self.read_protection(prot)
        case (prot && prot.downcase)
          when 'public'
            return :public
          when 'private'
            return :private
          when 'protected'
            return :protected
          else
            return nil
        end
      end

      # Return documentation string in form " (doc atom)" or "" if atom.nil?
      # string X item -> string
      # could be static, but seems to cause problems if do make it so
      def pr(doc, item)
        if item.nil?
          ''
        else
          " (#{doc} #{item})"
        end
      end

      # Return documentation for a list of items, where items are separated by
      #   a space
      # string X list(item) -> string
      # could be static, but seems to cause problems if do make it so
      def prl(doc, item_list)
        if item_list.nil? || item_list.empty?
          ''
        else
          " (#{doc} #{item_list.join(' ')})"
        end
      end

      # string -> string
      # adds leading space to item if item is non-nil
      def prs(item)
        if item.nil? || item == ''
          ''
        else
          ' ' + item
        end
      end

      # string -> string
      # like prs, but prints doc only if item IS false/nil
      def prn(doc, item)
        if item
          ''
        else
          ' ' + doc
        end
      end
    end
  end
end
