require_relative 'concealable_element'

module UmlTools
  module Model
    # An initial node in a State Diagram
    class InitialNode < ConcealableElement

      attr_reader :outgoing

      def initialize(name, id, stereotype, doc, outgoing)
        super(name, id, stereotype, doc)
        @outgoing = outgoing
      end

      # element is an initial node
      def initial_node?
        true
      end

      # read Initial Node from REXML::Document
      def self.read_from_xmi(elt, details_table, transitions)
        name = elt.attributes['name']
        id = elt.attributes['xmi:id']
        outgoing = REXML::XPath.match(elt, 'outgoing')
        stereotype = nil
        doc = nil

        outgoing_transitions = []

        outgoing.each { |outgoing_element|
          outgoing_id = outgoing_element.attributes['xmi:idref']

          transitions.each { |transition|
            if outgoing_id == transition.id
              outgoing_transitions << transition
            end
          }
        }

        details_element = details_table[id]
        if details_element
          details_element_properties = details_element.elements['properties']
          stereotype = details_element_properties.attributes['stereotype']
          doc = details_element_properties.attributes['documentation']
        end

        initial = InitialNode.new(name, id, stereotype, doc, outgoing_transitions)

        initial.outgoing.each { |transition|
          transition.source_node = initial
        }

        return initial
      end
    end
  end
end
