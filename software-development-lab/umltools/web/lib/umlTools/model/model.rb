require_relative 'concealable_element'

module UmlTools
  module Model
    # Abstract base class for all types of models
    # subclasses must define all_elements method
    class Model
      attr_reader :associations

      # Init generic model type given list associations
      def initialize(associations)
        @associations = associations
        @visible_associations = nil # cached
      end

      private
      # do common post-processing steps; returns table of all objects
      #    that can be linked in - this is also useful for setting
      #    visibility
      # Note this table will not have roles in it; roles are treated in a
      #    different way above, so it's not simple to mark roles as visible/
      #    hidden.
      def post_process_combined
        @associations.each { |assoc|
          a_supplier = assoc.a_role.supplier
          a_supplier.add_associate(assoc.b_role) if a_supplier
          b_supplier = assoc.b_role.supplier
          b_supplier.add_associate(assoc.a_role) if b_supplier
        }

        table = {}
        all_elements.each { |e| table[e.id] = e }
        all_elements.each { |e| e.find_links_in table }

        table
      end

      protected
      # given a collection of associatble elements, and associations,
      #   returns a list of associations matching some element
      def self.select_associations(elements, associations_table)
        target_ids = {}
        elements.each { |e| target_ids[e.id] = true }

        associations = associations_table.values.select { |a|
          target_ids[a.a_role.supplier_id] || target_ids[a.b_role.supplier_id]
        }
        return associations
      end

      public
      # process model after having read elements and doing any linking
      # Sets suppliers for associations and marks hidden elements
      # type: list(element) X list(sexpr) -> void
      def post_process_sexpr(logical_presentation_items)
        objects_table = post_process_combined

        logical_presentation_items.each { |logical_presentations|
          visible_objs = logical_presentations.select_all { |it|
            it.list? && it.head.name == 'object' && it.second.name =~ /View/
          }
          objs = visible_objs.map { |x| [x.second.name, x.third.name] }
          visible_ids = visible_objs.map { |cv| cv.find_by_path %w(quidu) }
          visible_ids.each { |vi|
            objects_table[vi.name].visible = true if vi && objects_table[vi.name]
          }
        }
      end

      # post process model in xmi case
      #  - sets all objects to be visible
      def post_process_xmi
        objects_table = post_process_combined

        objects_table.each_value { |x|
          if x.is_a? ConcealableElement
            x.visible = true
          end
        }
      end

      # All visible associations in the model
      # type: -> list(Association)
      def visible_associations
        @visible_associations = @visible_associations ||
            @associations.select { |x| x.visible? }
        @visible_associations
      end

      # All hidden associations in the model
      # type: -> list(Association)
      def hidden_associations
        @associations.select { |x| x.hidden? }
      end
    end
  end
end
