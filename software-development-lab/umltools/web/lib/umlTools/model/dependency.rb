module UmlTools
  module Model
# Dependency between two elements - possibly dynamic relationship between
#   classes or include/extend relationship between use cases
#   Supplier is the _destination_ of the arrow; current class is the source
# Note: If use case UC1 includes UC2, the arrow is UC1 -- <<include>> --> UC2
#       If UC2 extends UC1, the arrow is UC1 <-- <<extend>> -- UC2
# rwh, todo: rewrite to have both consumer and suppliers for all
#       types of model files (currently has it only for xmi 2.1)
    class Dependency < Connector

      # init from atom name, atom .mdl ident, string stereotype, string
      #   documentation, string .mdl ident for supplier
      # Also handles case where elements are simple strings and lists
      # Note: supplier can be nil, but then name should be specified
      def initialize(n, id, stereotype, doc, supplier_id)
        super(n, id, stereotype, doc, supplier_id, 'dependency')
        # will reset following below
        @supplier = nil
        @source = nil
      end

      def ident
        'dependency ' + (name || (' to ' + @supplier.name || @supplier_id))
      end

      # is a dependency
      def dependency?
        true
      end

      # read data for dependency arrow; supplier is initially just an id,
      #   but is filled in with the actual object later
      # rwh, todo: find out if can have stereotype, documentation, name for
      #   generalization relationships in xmi files
      # Note: If use case UC1 includes UC2, have UC1 -- <<include>> --> UC2
      #       If UC2 extends UC1, the arrow is UC1 <-- <<extend>> -- UC2
      def self.read_from_xmi_21(elt, details)
        name = nil # name on dependency is name of receiver
        id = elt.attributes['xmi:id']

        stereo = (elt.attributes['xmi:type'] == 'uml:Include' ?
            'include' : 'extend')
        doc = nil # always empty for xmi 2.1

        # element that's the source of the arrow
        source_id = elt.attributes['client']
        # element that's the destination of the arrow
        supplier_id = elt.attributes['supplier']
        # client_id must be used to look up the dependency if
        #   available
        client_id = elt.attributes['client']

        dependency = self.new(name, id, stereo, doc, supplier_id).with_21(details)
        dependency.source_id = source_id
        dependency.client_id = client_id
        return dependency
      end
    end
  end
end