require_relative 'm_class'
require_relative 'association'

# Logical model: captures the class diagrams
module UmlTools
  module Model
    class LogicalModel < Model
      # All elements, classes, and associations in the model
      # list(MClass)
      attr_reader :classes
      # also defines @all_elements: list(Element)

      # Initialize logical model from lists of classes and associations
      def initialize(classes, associations)
        super(associations)
        @classes = classes
        @all_elements = @classes + @associations
        @visible_classes = nil # cached
        @visible_roles = nil # cached
        @all_elements_result = nil # cached
      end

      # all visible elements
      # type: -> list(Element)
      def visible_elements
        visible_classes + visible_roles
      end

      # all visible associable elements
      # type: -> list(Element)
      def visible_associable_elements
        visible_classes
      end

      # All visible classes in the model
      # type: -> list(MClass)
      def visible_classes
        @visible_classes = @visible_classes || @classes.select { |x| x.visible? }
        @visible_classes
      end

      # All hidden classes in the model
      # type: -> list(MClass)
      def hidden_classes
        @classes.select { |x| x.hidden? }
      end

      # All attributes in the model (across all classes)
      # type: -> list(Attribute)
      def all_attributes
        attrs = classes.map { |c| c.attributes }
        attrs.flatten
      end

      # All methods in the model (across all classes)
      # type: -> list(Method)
      def all_methods
        mets = classes.map { |c| c.methods }
        mets.flatten
      end

      # return methods for classes that are not in a library
      def all_non_library_methods
        non_libs = classes.select { |c| !c.in_library? }
        methods = non_libs.map { |c| c.methods }
        methods.flatten
      end

      # All roles in the model (the ends of all associations)
      # type: -> list(Role)
      def roles
        rs = @associations.map { |as| [as.a_role, as.b_role] }
        rs.flatten
      end

      # All visible roles in the model
      # type: -> list(Role)
      def visible_roles
        unless @visible_roles
          va = visible_associations
          rs = va.map { |as| [as.a_role, as.b_role] }
          @visible_roles = rs.flatten
        end
        @visible_roles
      end

      # All elements in the model, including those elements which are
      # nested within others
      # type: -> list(Element)
      def all_elements
        unless @all_elements_result
          all = @all_elements + @all_elements.map { |e| e.all_elements }
          @all_elements_result = all.flatten
        end
        @all_elements_result
      end

      # return top level elements, sorted by name
      def all_sorted_elements
        @classes.sort + @associations.sort
      end

      # All named elements in the model
      # type: -> list(Element)
      def all_named_elements
        all_elements.select { |e| e.name }
      end

      # Names of all classes after stripping any template information
      # Used to list classes in certain contexts
      # type: -> list(string)
      def short_class_names
        names = classes.map { |c|
          c.contained_type || c.name
        }
        names.delete(nil)
        names.delete('')
        names
      end

      # Read elements of logical model from sexpr
      # Sexpr is assumed to be the result of finding 'root_category'
      # type: SExpr -> LogicalModel
      # note: caller must invoke post_process_sexpr; assumption is that post_process_sexpr
      #       will not be called until _all_ elements have been read and linked
      def self.read_elements(logical_sexpr)
        #$stderr.puts "Looking at: [#{logical_sexpr}]"
        raise 'Logical model not found' if logical_sexpr.nil?
        objects = logical_sexpr.select { |s| s.matches?('object', 'Class') }
        classes = objects.map { |o| MClass.read(o) }

        objects = logical_sexpr.select { |s| s.matches?('object', 'Association') }
        associations = objects.map { |o| Association.read(o) }

        mdl = LogicalModel.new(classes, associations)
        return mdl
      end

      # Reads logical model from sexpr
      # This is intended to be a stand-alone operation when only want to read
      #   the logical model and we know there are no elements that actually
      #   belong to another model.  Clients would generally call
      #   FullModel.read instead.
      # type: SExpr -> LogicalModel
      def self.read(sexpr)
        logical_sexpr = sexpr.second.find_by_path %w(root_category)
        mdl = read_elements(logical_sexpr)
        elements = mdl.classes + mdl.associations
        # fix up links between elements
        table = {}
        elements.each { |e| table[e.id] = e }
        elements.each { |e| e.find_links_in table }
        # and complete the processing
        mdl.post_process_sexpr(logical_sexpr.find_all_by_path(%w(logical_presentations)))
        return mdl
      end

      # Read elements of logical model from xmi document
      # uml_model_doc is a uml:Model element
      # type: REXML::Document -> LogicalModel
      # note: caller must invoke post_process; assumption is that post_process
      #       will not be called until _all_ elements have been read and linked
      def self.read_from_xmi(uml_model_doc, details_table)
        raise 'Logical model not found' if uml_model_doc.nil?

        classes = []
        associations = []

        model_root = uml_model_doc.elements['packagedElement']
        if (model_root.elements['packagedElement[@xmi:type="uml:Class"]'])
          # if there are classes at this level, reset to original root
          model_root = uml_model_doc
        end

        model_root.each_element('packagedElement') { |x|
          # class diagram will have a type of uml:Package and contain elements with the uml:Class type
          # necessary to differentiate from use case diagrams
          if (x.attributes['xmi:type'] == 'uml:Package' && 
              x.elements['packagedElement[@xmi:type="uml:Class"]'])
            class_objects = x.elements.select { |elt|
              elt.attributes['xmi:type'] == 'uml:Class'
            }
            classes += class_objects.map { |o|
              MClass.read_from_xmi(o, details_table)
            }
            assoc_objects = x.elements.select { |elt|
              elt.attributes['xmi:type'] == 'uml:Association'
            }
            associations += assoc_objects.map { |o|
              Association.read_from_xmi(o, details_table)
            }
          end
        }

        mdl = LogicalModel.new(classes, associations)

        return mdl
      end

      # Read elements of logical model from version 2.1 xmi document
      # type: REXML::Document X associations table -> LogicalModel
      # note: caller must invoke post_process; assumption is that post_process
      #       will not be called until _all_ elements have been read and linked
      def self.read_from_xmi_21(xmidoc, associations_table,
          dependencies_table, details)
        classes = []

        # read classes
        xmidoc.elements.each('xmi:XMI/uml:Model/packagedElement/packagedElement') { |elt|
          if elt.attributes['xmi:type'] == 'uml:Class'
            classes << MClass.read_from_xmi_21(elt, associations_table,
                                               dependencies_table, details)
          end
        }

        associations = select_associations(classes, associations_table)

        mdl = LogicalModel.new(classes, associations)
        return mdl
      end

      # adding associations for xmi-based models
      def add_xmi_associations(new_associations)
        @associations = (@associations || []) + new_associations
      end

      # special processing for :xmi_ea: filter out classes with
      #   no .name - these are diagram notes
      #   This must be done after all other processing because .name is not
      #   a valid check until late in the process of loading models in :ea_xmi
      def remove_ea_xmi_diagram_notes
        @classes = @classes.select { |c| c && c.name }
      end

      # Return string representation of LogicalModel for debugging
      def to_s
        "LogicalModel[\n  " + @all_elements.join("\n  ") + "\n]"
      end

      # Return lisp-style string of model
      def dump
        items = all_sorted_elements.map { |x| x.dump }
        "(LogicalModel\n  " + items.join("\n  ") + "\n)"
      end
    end
  end
end
