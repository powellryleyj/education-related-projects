require_relative 'm_class'

module UmlTools
  module Model
# An actor, typically in a use case model
#  Derived from MClass since actors can appear as classes in the
#  logical model and even have attributes and operations.
    class Actor < MClass

      # initialize with atom name, .mdl identifier (atom), string
      #   stereotype, string documentation, list(ident) supers,
      #   list(ident) dependencies
      # Also handles case where elements are simple strings and lists
      # The lists of attributes and methods are set to empty.
      def initialize(name, id, stereotype, doc, sids, uses)
        super(name, id, stereotype, doc, sids, [], [], uses)
      end

      # is an actor
      def actor?
        true
      end

      # identifier name for actor
      def ident
        'actor ' + (name || nonempty_name)
      end

      # no elements for actors
      def all_elements
        []
      end

      def to_s
        "<Actor #{@name}#{supers_as_string}>"
      end

      def dump
        if Element.print_just_ids
          "(actor #{@id})"
        else
          # rwh, todo: print dependencies (and same with classes)
          "(Actor #{@name}#{dump_id}#{dumped_supers}#{self.pr 'stereo', @stereotype}#{self.pr 'doc', @doc}#{self.prn 'hidden', @visible})"
        end
      end

      # read Actor from SExpr
      # caller must call find_links_in
      def self.read(sexpr)
        name = sexpr.find_by_path %w(Class)
        id = sexpr.find_by_path %w(quid)
        stereotype = sexpr.find_by_path %w(stereotype)
        doc = sexpr.find_by_path %w(documentation)
        (sups, uses) = read_super_and_uses(sexpr, 'used_nodes',
                                           'Uses_Relationship')
        return self.new(name, id, stereotype, doc, sups, uses)
      end

      # read Actor from REXML::Document
      # caller must call find_links_in
      def self.read_from_xmi(elt, details_table)
        name = elt.attributes['name']
        id = elt.attributes['xmi:id']
        # note: might also check visibility attribute - public seems to be visible
        # also need: stereotype, documentation, sups, uses
        stereotype = nil
        doc = nil
        sups = nil
        uses = nil
        if (details_for_item = details_table[id])
          if doc_entry = details_for_item.elements['modelDocument']
            doc = doc_entry.text
          end
        end
        return self.new(name, id, stereotype, doc, sups, uses)
      end

      # read Actor from REXML::Document
      # caller must call find_links_in
      def self.read_from_xmi_21(elt, associations_table, dependencies,
          details)
        name = elt.attributes['name']
        id = elt.attributes['xmi:id']
        stereotype = nil
        doc = nil # not available in 2.1 xmi
        sups = nil
        uses = nil
        (sups, uses) = read_super_and_uses_from_xmi_21(elt, associations_table,
                                                       dependencies,
                                                       details)
        return self.new(name, id, stereotype, doc, sups, uses).with_21(details)
      end
    end
  end
end
