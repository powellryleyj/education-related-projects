#!/usr/bin/env ruby
# check.rb: run all checks on named file for UMLint
#   This is separate from the UMLTools website to enable running the
#   checks from the command prompt and to keep the code separate from the
#   web application.

module UmlTools
#   usage: see end of this file
# NOTE: handling multiple files done by umlint script

  require_relative 'petal'
  require_relative 'issues'
  require_relative 'check/model_checker'
  require_relative 'check/sequence/model_checker'
  require_relative 'checklist/checks_to_exclude'
  require_relative 'checklist/checks_to_include'
  require_relative 'checklist/check_list'
  require_relative 'checklist/serializable'
  require_relative 'pstats'
  require_relative 'check/all_checks'
  require_relative 'check/sequence/all_checks'
  require_relative 'model/sequence/full_model'
  
  require 'set'

  # Model X bool X ListOfChecks::CheckList -> list(string)
  def self.check_results(model, verbose, checklist = nil, is_sequence = false, high_level=false)
    if is_sequence
      Check::Sequence::AllChecks.run_all_checks(model, verbose, checklist)
    else
      Check::AllChecks.run_all_checks(model, verbose, checklist)
    end
  end

  def self.show(messages, filename = nil, silent = false)
    messages.sort!
    if messages.empty?
      unless Issue.echo? || silent
        puts 'No issues identified, but be sure to review the model carefully.'
      end
    else
      # if not silent, have already printed file name:
      puts "#{filename}:\n" if silent && filename
      puts "#{messages.join("\n")}"
    end
  end

  def self.load_diagram(model_file, is_sequence)
    if is_sequence
      Model::Sequence::FullModel.load(model_file)
    else
      Model::FullModel.load(model_file)
    end
  end

  ######################################################################

  $debug_umlint = false

  def self.print_usage
    puts 'Usage: umlint [-c file] [-d] [-i] [-m] [-v] [-S] [files...]'
    puts '  -c: specify checklist (either items to include or to exclude)'
    puts '      [] as the checklist filename means include all checks'
    puts '  -d: debug mode: more detailed info when not a model file'
    puts '  -G: Generate a diagram image for the linted diagram'
    puts '  -i: print message id numbers with messages'
    puts '  -m: print metrics'
    puts '  -s: silent unless issues are identified'
    puts '  -v: verbose output: names checks as being executed'
    puts '  -S: file is a sequence diagram; uml will be used if not specified'
    puts '  If file not specified or -, reads from standard input'
  end

  # main program for identifying errors in models
  #   Structure: 
  #     1. parse command-line arguments
  #     2. load the checklist
  #     3. load the submitted model
  #     4. apply specified checks to the model, collecting messages
  #     5. write messages to standard output
  #
  # $debug_umlint allows running this code without the exception handler so
  #   exceptions can be traced back to their source more easily
  #
  def self.check_main(cmd_args)
    print_fname = nil # printable version of input file name
    print_metrics = false
    checklist = nil # controls what checks are made (default: all made)
    silent = false # only print file names if there are issues
    is_sequence = false # true if file is a sequence diagram; uml by default
    diagram = false #diagram generation options
    #puts "Arguments: #{cmd_args.join ' '}"

    if cmd_args[0] == '--help' || cmd_args[0] == '-?'
      # note -f is also allowed for check.rb, but not for umlint
      #   -f sets the printable file name for the purposes of error messages
      #   but is only useful when executed from umlint shell script
      UmlTools.print_usage
      return 1
    end

    while cmd_args[0] =~ /^-/
      if cmd_args[0] == '-c' && cmd_args[1] != ''
        checklist = cmd_args[1]
        cmd_args.shift
      elsif cmd_args[0] == '-d'
        $debug_umlint = true
      elsif cmd_args[0] == '-i'
        Issue.set_echo_ids(true)
      elsif cmd_args[0] == '-m'
        print_metrics = true
      elsif cmd_args[0] == '-s'
        silent = true
      elsif cmd_args[0] == '-v'
        verb = true
      elsif cmd_args[0] == '-S'
        is_sequence = true
      elsif cmd_args[0] == '-G'
        diagram = true
      elsif cmd_args[0] == '-f' && cmd_args[1] != ''
        # note: -f only allowed for check.rb, not umlint
        print_fname = cmd_args[1]
        cmd_args.shift
      end
      cmd_args.shift
    end

    if checklist.nil?
      checklist = ListOfChecks::ChecksToExclude.default is_sequence
    elsif checklist == '[]'
      checklist = ListOfChecks::ChecksToExclude.new('empty', 'generic', 'none', '0', '', [])
    else
      error = nil
      checklist_file = checklist
      begin
        checklist = ListOfChecks::load_file checklist
        error = "Error: checklist file #{checklist_file} has invalid format" unless checklist.kind_of? ListOfChecks::CheckList
      rescue IOError, Errno::ENOENT
        error = "Error: No such checklist file: #{checklist_file}"
      end
      if error
        puts error
        return 3
      end
    end

    model_file = cmd_args[0]

    print_fname = model_file if print_fname.nil?
    print_fname = 'input' if print_fname.nil? || print_fname == ''

    messages = []

    if $debug_umlint
      model = load_diagram(model_file, is_sequence)
      messages = check_results(model, verb, checklist, is_sequence)
    else
      begin
        model = load_diagram(model_file, is_sequence)
        if diagram
          model.generateDiagram Submission.imageFileFromModelFile model_file
        end
        messages = check_results(model, verb, checklist, is_sequence)
      rescue => error
        File.open('umlintSubmission.log', 'w'){|file| (file.write error.to_s + "\n" + error.backtrace.join("\n"))}
        if Issue.echo?
          # don't echo file name in this case since it's displayed on the
          #   web site and the file name refers to subs/ when called from
          #   the web site
          puts 'BAD_MODEL.petal_error.Error: file does not appear to contain a UML model'
        else
          puts "Error: #{print_fname} does not appear to contain a UML model"
        end
        return 2
      end
    end

    show(messages, print_fname, silent)
    model.print_metrics if print_metrics
    0
  end

  if __FILE__ == $0

    exit(check_main(ARGV))

  end
end
