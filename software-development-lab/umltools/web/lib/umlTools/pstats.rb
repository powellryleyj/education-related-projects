module UmlTools
#!/usr/bin/env ruby
# add statistics to model files
# When used as a standalone program, reads a model file and prints statistics
# Usage: pstats.rb _file_
#

  require_relative 'model/model'
  require_relative 'model/use_case_model'
  require_relative 'model/logical_model'
  require_relative 'model/full_model'

# statistics for models
  class ModelStatistics
    attr_reader :complexity

    def initialize(complexity)
      @complexity = complexity
    end
  end

  class UseCaseModelStatistics < ModelStatistics
    attr_reader :actor_count, :use_case_count, :association_count

    def initialize(actor_count, use_case_count, association_count, complexity)
      super(complexity)
      @actor_count = actor_count
      @use_case_count = use_case_count
      @association_count = association_count
    end
  end

  class ClassModelStatistics < ModelStatistics
    attr_reader :attribute_count, :method_count, :class_count, :association_count

    def initialize(attribute_count, method_count, class_count, association_count,
                   complexity)
      super(complexity)
      @attribute_count = attribute_count
      @method_count = method_count
      @class_count = class_count
      @association_count = association_count
    end
  end

# mix-ins for computing statistics
  module Model

    class FullModel

      def print_metrics
        ucstats = @use_case_model ? @use_case_model.statistics : nil
        lstats = @logical_model ? @logical_model.statistics : nil
        if ucstats.nil? || ucstats.complexity == 0
          puts 'Use case model: empty'
        else
          puts "Use case model size: #{ucstats.actor_count} actors, #{ucstats.use_case_count} cases, #{ucstats.association_count} associations"
          puts "Use case complexity: #{ucstats.complexity}"
        end
        if lstats.nil? || lstats.complexity == 0
          puts 'Class model: empty'
        else
          puts "Class model size: #{lstats.class_count} classes w/ #{lstats.attribute_count} attributes and #{lstats.method_count} methods, #{lstats.association_count} associations"
          puts "Class complexity: #{lstats.complexity}"
        end
      end
    end

    class UseCaseModel < Model
      # compute statistics over visible model elements
      def statistics
        complexity = visible_associations.size * 2 + visible_actors.size +
            visible_use_cases.size
        UseCaseModelStatistics.new(visible_actors.size, visible_use_cases.size,
                                   visible_associations.size, complexity)
      end
    end

    class LogicalModel < Model
      # compute statistics over visible model elements
      def statistics
        attrs = 0
        mets = 0
        ok_assoc = 0
        visible_classes.each { |c|
          attrs += c.attributes.size
          mets += c.methods.size
        }
        ok_assoc += 2 * visible_associations.size
        complexity = attrs + mets + ok_assoc
        ClassModelStatistics.new(attrs, mets, visible_classes.size, ok_assoc,
                                 complexity)
      end
    end
  end

  def self.pstats_main(file)
    model = Model::FullModel.load(file)
    model.print_metrics
    0
  end

end
