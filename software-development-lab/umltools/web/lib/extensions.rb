class String
  def cap_first
    return '' if empty?
    self[0].chr.upcase + self[1..-1]
  end

  def match_text
    @match_text = self.delete(" \n\t_").downcase unless @match_text
    @match_text
  end
end

class Array
  # return index of first item satisfying predicate, where predicate given as
  #   a block.
  def first_index
    i = 0
    while i < size
      return i if yield(i)
      i += 1
    end
    nil
  end
end
