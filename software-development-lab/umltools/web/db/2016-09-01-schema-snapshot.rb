# NOTE: this is with rails 4.2.7
#
# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 1) do

  create_table "check_items", force: :cascade do |t|
    t.string   "category",       limit: 255
    t.string   "issue",          limit: 255
    t.integer  "explanation_id", limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "check_items_lists", id: false, force: :cascade do |t|
    t.integer "check_item_id", limit: 4
    t.integer "check_list_id", limit: 4
  end

  create_table "check_lists", force: :cascade do |t|
    t.text     "description",       limit: 65535
    t.integer  "instructor_id",     limit: 4
    t.string   "language",          limit: 255
    t.boolean  "is_inclusion_list"
    t.boolean  "retired"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  create_table "explanations", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.integer  "precedence",      limit: 4
    t.text     "description",     limit: 65535
    t.string   "diagram",         limit: 255
    t.text     "explanation",     limit: 65535
    t.text     "detailed_issues", limit: 65535
    t.text     "good_example",    limit: 65535
    t.text     "bad_example",     limit: 65535
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "explanations", ["name"], name: "index_explanations_on_name", unique: true, using: :btree

  create_table "instructors", force: :cascade do |t|
    t.string   "given_name",      limit: 255
    t.string   "surname",         limit: 255
    t.string   "organization",    limit: 255
    t.string   "email_address",   limit: 255
    t.string   "password_digest", limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "problem_words", force: :cascade do |t|
    t.integer  "problem_id",      limit: 4
    t.text     "nouns",           limit: 65535
    t.text     "verbs",           limit: 65535
    t.integer  "assignment_type", limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "problems", force: :cascade do |t|
    t.string   "name",             limit: 255
    t.text     "description",      limit: 65535
    t.string   "solution_file",    limit: 255
    t.string   "author",           limit: 255
    t.string   "organization",     limit: 255
    t.integer  "instructor_id",    limit: 4
    t.integer  "check_list",       limit: 4
    t.string   "assignment_type",  limit: 255
    t.text     "serialized_model", limit: 65535
    t.datetime "posted_at"
    t.datetime "started_at"
    t.datetime "due_at"
    t.datetime "closed_at"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "problems", ["instructor_id"], name: "index_problems_on_instructor_id", using: :btree

  create_table "submissions", force: :cascade do |t|
    t.string   "type",             limit: 255
    t.string   "orig_name",        limit: 255
    t.string   "save_name",        limit: 255
    t.integer  "check_id",         limit: 4
    t.string   "problem_id",       limit: 255
    t.string   "from_ip",          limit: 255
    t.integer  "ticket",           limit: 4
    t.integer  "file_type_id",     limit: 4
    t.boolean  "generate_diagram"
    t.datetime "time"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "submissions", ["problem_id"], name: "index_submissions_on_problem_id", using: :btree

  create_table "submissions_warnings", id: false, force: :cascade do |t|
    t.integer "submission_id", limit: 4
    t.integer "warning_id",    limit: 4
  end

  create_table "warnings", force: :cascade do |t|
    t.text     "message",        limit: 65535
    t.integer  "message_hash",   limit: 8
    t.integer  "explanation_id", limit: 4
    t.string   "category",       limit: 255
    t.string   "detailed_issue", limit: 255
    t.string   "element_id",     limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "warnings", ["message_hash"], name: "index_warnings_on_message_hash", using: :btree

end
