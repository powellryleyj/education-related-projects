# (1/8/19) powellr - The following lines doesn't affect source code but prevents RuboCop
# from picking up warnings involving 5+ parameter lists for this file only if installed
# locally
# rubocop:disable Metrics/ParameterLists
# rubocop:disable Layout/IndentHeredoc
#
# *Default Rails File Description*
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ :name => 'Chicago' }, { :name => 'Copenhagen' }])
#   Mayor.create(:name => 'Emanuel', :city => cities.first)
#
#
# *Note*
# This file is rather lengthy and is in the following order:
# 1. Explanation
# 2. CheckItem
# 3. Account
# 4. CheckList
# 5. Problem
# Each section is marked with a begin/end comment

# Begin Explanation Defaults
explanations =
  [
    #
    # --- special issue when controller finds a problem
    #
    [:UNKNOWN,
     10,
     'Unanticipated internal error',
     'all',
     'This issue has not been adequately documented - please contact ' \
       'the site administrator.',
     []],
    #
    # --- issue when syntax error found
    #
    [:BAD_MODEL,
     10,
     'Invalid model file',
     'all',
     'File appears to not contain a valid model.  This may mean the file ' \
       'was corrupted in some way.  Try reopening the file in the tool that ' \
       'created it. If it opens successfully, then you have probably found ' \
       'a fault with UMLint or UMLGrader.  Contact the author at ' \
       'https://faculty-web.msoe.edu//hasker/',
     %i[petal_error pdiff_solution_error submission_petal_error]],
    #
    # --- umlint issues
    #
    [:BAD_ASSOC_ROLE,
     7,
     'Association which refers to missing elements',
     'all',
     'Deleting objects from models sometimes results in associations ' \
       'between objects that no longer appear anywhere else in the model. ' \
       'The simplest way to fix such broken associations is to delete the ' \
       'association altogether and just recreate it.',
     %i[reference_to_missing_items role_name_at_source]],

    [:BAD_CONTAINER,
     5,
     'Incorrect use of container classes',
     'class',
     'All container classes such as List<> and set<> need ' \
       'some type of element to be contained. In most cases ' \
       'this needs to be exactly one type, but a few such as ' \
       'map<> need two such as map<string, int>.',
     [:no_elt_between_brackets]],

    [:CLASS2OWN,
     5,
     'Class which is "owned" by more than one class',
     'class',
     'Composition (shown with a filled-in diamond) indicates ' \
       "ownership, so having two classes showing ownership isn't " \
       "consistent.  It's possible the associations are backwards " \
       'or that aggregation (an open diamond) should have been used ' \
       'instead.',
     [:dual_owners]],

    [:COMPLEXATTR,
     4,
     'An attribute with a class name, indicating a non-simple attribute',
     'class',
     'Attributes listed as part of the class (as opposed to being ' \
       'linked by an association) should be "simple" attributes. ' \
       'Simple attributes are typically ones that can be ' \
       'represented by built-in data types such as numbers, ' \
       'strings, and dates.  In general, these are values that have ' \
       'no "identity": just because two objects happen to have ' \
       'the same height does not mean the objects are the same. ' \
       'If an attribute is complex - that is, contains multiple ' \
       'pieces of information - then that attribute should be ' \
       'represented by an instance of a class, and instead of ' \
       'naming the object in the attributes section the designer ' \
       'should create a separate class and use an association.',
     %i[no_collections no_complex_attributes non_simple_attributes]],

    [:COMPMUL1,
     4,
     'Using a multiplicity other than 1 with composition (ownership)',
     'class',
     'Composition (indicated by a filled-in diamond) expresses ' \
       "ownership, so a multiplicity other than 1 isn't consistent.",
     [:expected_multiplicity_1]],

    [:DYNAMIC,
     4,
     'Showing dynamic dependencies between classes',
     'class',
     'Dotted lines are used to show dependency.  In a class ' \
       "diagram it's typically used for a \"dynamic\" relationship " \
       'where a member function has a local variable or a parameter ' \
       'of the given type.  Documenting this sort of relationship ' \
       "is rarely useful - it's more important to document more " \
       "permanent relationships between objects.  It's also possible " \
       'a dotted line was chosen by mistake, in which case it ' \
       'should be deleted and replaced by a regular ' \
       'association (a solid line).',
     [:no_dynamic_associations]],

    [:ILLEGALID,
     5,
     'Illegal characters in identifiers (by Java and C++ rules)',
     'all',
     'Having illegal characters in identifiers (names of classes, ' \
       'attributes, methods, etc.) causes problems when generating ' \
       'code.  A class model ultimately describes a program, so it ' \
       'needs to follow the same rules as programming languages.',
     [:no_illegal_identifiers]],

    [:INDEX,
     3,
     'Attributes representing indices rather than using ' \
       'direct associations between classes',
     'class',
     'Attributes with words like "id" or "index" in them ' \
       'are used to access an object in a table.  This is a ' \
       'procedural view of data and should be replaced by a more ' \
       'object-oriented view.  A better design is to have the ' \
       'object refer directly to the target: remove the index ' \
       'attribute and add an association for the actual object that ' \
       'would be indexed.  This is more object-oriented because it ' \
       'allows objects to be processed separately from their ' \
       'containers.  It also leads to more robust code: storing ' \
       'indices instead of references to objects has been shown to ' \
       'lead to more programming errors.',
     [:use_direct_association]],

    [:INSTALLATION,
     10,
     'Tool installation error',
     'all',
     'There is an error in the installation of the tool and some ' \
       'features may not be available. Contact the site maintainer.',
     [:no_spell_check]],

    [:INVALIDOP,
     7,
     'Adding methods to library classes such as Queue in Java ' \
       'and list<> in C++',
     'class',
     'Standard classes in C++ and Java libraries have well-defined ' \
       'lists of methods.  Using these classes in the model is a ' \
       'good thing - no need to reinvent the wheel.  However, these ' \
       'classes already have lists of established operations and the ' \
       'programmer is not likely to add new ones.  Adding methods ' \
       'to existing classes usually indicates there is a missing ' \
       'class.  For example, a registration system might include ' \
       'a Roster class that has an association to a List<Student> ' \
       'class; the actual objects are stored in the list, but ' \
       'the roster would have special operations to add and remove ' \
       'students while enforcing restrictions such as the number ' \
       'of students permitted in a course.',
     [:invalid_op_in_standard_class]],

    [:INVALID_USE_OF_ROLES,
     7,
     'Using role names incorrectly',
     'class',
     "Given classes X and Y, if X ---&gt; Y and X has a variable 'data' in " \
       "of type Y is a member of class X, the role name 'data' would be at " \
       'Y end of the arrow. In this case, a role name appears at the source ' \
       'end (the X side), which conflicts with the direction of the ' \
       'association. If you cannot see a role name in this association, ' \
       'open the properties window and delete the role name there.',
     [:role_name_at_source]],

    [:INVISIBLE,
     5,
     'Actors, use cases, and classes that were deleted from the diagram ' \
       'but have not been deleted from the model',
     'all',
     'The element was probably deleted from the diagram, but it is ' \
       'still a part of the model.  Such elements cause problems ' \
       'with generated code and need to be deleted.  To delete it, ' \
       'open the model, open the appropriate view in the explorer ' \
       'window (in the upper left), right click on the name of the ' \
       'element, and chose the Delete or Remove from Model operation.',
     %i[invisible_actors invisible_use_cases]],

    [:MISSINGELT,
     5,
     'Container classes with no association to a collection of elements',
     'class',
     'A container class such as a list or vector should be in a 1 to ' \
       'many relationship to a class representing the elements of the ' \
       'collection.',
     [:container_without_element]],

    [:MIXEDSTYLES,
     2,
     'Checking that a consistent naming style was used, either ' \
       'capitalizing first letters of words or separating words ' \
       'with underscores',
     'class',
     'There are a number of accepted capitalization and underscore ' \
       "conventions.  It's fine to use any one of them, but be sure " \
       'to be consistent about what is used.',
     %i[mixed_attribute_naming_styles mixed_styles_for_method_names
        no_mixed_styles_for_class_names]],

    [:MULTIGEN,
     7,
     'Classes with multiple superclasses, typically indicating reversed ' \
       'inheritance arrows',
     'class',
     'A hollow triangle marks inheritance with the triangle ' \
       "pointing to the superclass.  It's rare for a class to have " \
       'more than one superclass, so having multiple generalization ' \
       'arrows pointing away from a class probably indicates an ' \
       'error.  The generalization arrows may be pointing in ' \
       'the wrong direction or the intent was to use standard ' \
       'association.',
     [:multiple_inheritance]],

    [:NAVIGABLE_ASSOC,
     5,
     'Using a directed arrow instead of an undirected association between ' \
       'an actor and a use case',
     'use case',
     'The associations between use cases and actors should not be ' \
       'navigable.  To remove the association, right click on it and ' \
       "clear the 'Navigable' check mark.",
     [:make_undirected]],

    [:NO0DOTSTAR,
     6,
     'Using 0..* rather than * for a multiplicity',
     'class',
     "The multiplicity '*' means 0 or more items, so '0..*' is " \
       "redundant.  Change it to simply '*'.  Note that the " \
       "Rational Rose menu item for '*' is 'n', so use that instead " \
       "of the menu item 'Zero or More'.  The fact that Rational " \
       "Rose uses '0..*' at all is a known bug.",
     [:no_multiplicity_0_star]],

    [:NOAND,
     7,
     "Probable low cohesion because of 'and' in a class name",
     'class',
     "The word 'and' in a class name typically indicates a class " \
       'that has multiple roles.  This may be convenient, but ' \
       'reduces reusability.  Either find a more general name for ' \
       'class that naturally includes both roles, or replace the one ' \
       'class by multiple classes.',
     [:no_and_in_class_names]],

    [:MISSING_CONTAINER,
     6,
     'Failing to use a container class where one is needed',
     'class',
     'When a class is the target of a directed association and ' \
       'there are multiple instances of that class, the class at ' \
       'the other end of the association should be some type of ' \
       'container in many languages (such as List<X> in Java or ' \
       'set<X> in C++).',
     [:starred_navigable_wo_container]],

    [:NODOC,
     1,
     'Failing to document an important model element',
     'all',
     'Use case and class diagrams are ideal places to document the ' \
       'key elements of a system because they contain little unnecessary ' \
       'detail.  Also, names alone are rarely enough: a little ' \
       'documentation covering responsibilities, constraints, and ' \
       'outcomes goes a long way towards making the system clear to ' \
       'other developers. ' \
       'Add documentation by clicking on the item and entering ' \
       'text in the box in the lower left corner of the modeling ' \
       'tool.',
     %i[missing_actor_documentation missing_class_documentation
        missing_member_documentation missing_use_case_documentation]],

    [:NOMULTI,
     6,
     'Failing to specify the multiplicities in an association between ' \
       'two classes (except when that multiplicity can be inferred from ' \
       'the type of association used)',
     'class',
     "An association between two classes isn't complete if " \
       'there is no indication of how many objects are involved. ' \
       'A class model captures information about variable ' \
       "declarations in a program, and a program can't declare " \
       'it uses some type without saying how many are used (or ' \
       'that the data is in a collection with an arbitrary number ' \
       'of elements).',
     [:missing_multiplicities]],

    [:NONDESCRIPT,
     4,
     "Using nondescript words such as 'flag' and 'process' for names",
     'class',
     "Words like 'flag' just capture general type information and " \
       "don't help significantly in understanding either the design " \
       "or the implementation.  'Control' and 'process' are even more " \
       'nondescript. Look for a more meaningful name that ' \
       'answers questions such as "How is this used?" and ' \
       '"What information does this store?"',
     %i[nondescript_attribute nondescript_identifier nondescript_method]],

    [:NOOR,
     7,
     "Low cohesion in which 'or' is used in class names",
     'class',
     "The word 'or' in a class name typically indicates a class " \
       'that has multiple roles.  This may be convenient, but ' \
       'reduces reusability.  Either find a more general name for ' \
       'class that naturally includes both roles, or replace the one ' \
       'class by multiple classes.  Inheritance is often useful in ' \
       'such cases.',
     [:no_or_in_class_names]],

    [:NOZERO,
     7,
     'Using multiplicity 0 in an association between two classes',
     'class',
     "A multiplicity of '0' indicates there are no instances of " \
       "the item.  This doesn't make any sense in a program.  Use " \
       '0..1 to indicate an optional item or * to indicate an ' \
       'arbitrary number.',
     [:no_multiplicity_0]],

    [:PUBLICATTR,
     2,
     'Having public attributes',
     'class',
     'Public attributes cause problems because there are no constraints ' \
       'on how they are accessed or modified.  Change them to be private ' \
       'protected and add a method to access the data.',
     [:non_public_attributes]],

    [:REMOTE_USE_CASE,
     8,
     'Use cases that are very distant from any actor',
     'use case',
     'Use cases represent actions taken by an actor or actors, not ' \
       'steps in a program. <<extend>> and <<include>> can be used to ' \
       'avoid repetition in a use case diagram and to highlight ' \
       'specific activities, but stacked <<extend>> and <<include>> ' \
       'are overly complicated and generally reflect a misunderstanding ' \
       'of use case diagrams. Get help from your instructor.',
     [:more_than_3_removed]],

    [:REQCAPS,
     5,
     'Checking for class names that do not start with a capital letter',
     'class',
     'The convention is that class names start with upper case ' \
       'letters.',
     [:capitalize_class_names]],

    [:RESERVED,
     5,
     'Using reserved words from Java or C++ as identifiers',
     'all',
     "Using a reserved word such as 'float' or 'while' as an " \
       'identifier name causes problems when generating ' \
       'code.  A class model ultimately describes a program, so it ' \
       'needs to follow the same rules as programming languages. ' \
       'Modify the name slightly so it is not a reserved word, ' \
       "perhaps by adding an '_' (underscore) or putting 'the' in " \
       'front.',
     [:no_reserved_words]],

    [:SPACES,
     3,
     'Having spaces in identifier names',
     'all',
     'Having spaces in identifiers (names of classes, attributes, ' \
       'methods, etc.) causes problems when generating code.  A ' \
       'model ultimately describes a program, so it needs to ' \
       'follow the same rules as programming languages.',
     [:no_spaces_in_names]],

    [:SYNTAX,
     10,
     'Syntax error in model',
     'all',
     'The model contains some type of syntax error.  This usually means ' \
       'the file has been corrupted.  It may be necessary to recreate ' \
       'corrupted files, but at times they can be repaired by hand.',
     [:syntax_error]],

    [:UC_BAD_ASSOCIATION,
     8,
     'Illegal associations in a use case diagram',
     'use case',
     'Each association (solid line) on a use case diagrams should be ' \
       'between an actor and a use case.  There should be no links ' \
       'between actors other than (at times) generalization. Links ' \
       'between use cases should be done with <<include>> and ' \
       '<<extend>>.',
     %i[use_cases_associate_to_actors generalization_between_use_cases
        no_stereotypes_or_names duplicated_association]],

    [:UC_BAD_STEREOTYPE,
     8,
     'Incorrectly using <<include>> and <<extend>> with regular ' \
       'associations',
     'use case',
     'The <<include>> and <<extend>> stereotypes are to be used ' \
       'only with dependency arrows (dotted or dashed lines). ' \
       'Here they have been used with regular associations between ' \
       'parts of the use case diagram.  Revise the diagram to use ' \
       'dependency arrows for these instead.',
     [:make_dependency]],

    [:UC_HAS_MULTIPLICITY,
     8,
     'Multiplicities appearing in a use case diagram',
     'use case',
     'The associations between use cases and roles should not have ' \
       'multiplicities.',
     [:unexpected_multiplicity]],

    [:UC_NAME,
     3,
     'Improper use case name',
     'use case',
     'Use case names should be verb phrases: an action word followed ' \
       "by an object. For example, 'Enter Address' or 'Arm Alarm'.",
     [:single_word_name]],

    [:UC_REV_EXT,
     6,
     'A reversed dependency arrow for an <<extend>>',
     'use case',
     'For <<extend>> relationships between use cases, the arrow ' \
       'points at the use case being extended; that is, the arrow ' \
       'points AWAY from the extension.',
     [:backwards_extends]],

    [:UC_REV_INCL,
     6,
     'A reversed dependency arrow for an <<include>>',
     'use case',
     'For <<include>> relationships between use cases, the arrow ' \
       'points at the use case which provides the services used by ' \
       'another use case.',
     [:backwards_include]],

    [:UC_USES,
     8,
     'A dependency arrow in a use case diagram that is not marked ' \
       'with <<extend>> or <<include>>',
     'use case',
     'The uses relationship should not appear on a use case diagram. ' \
       "It's possible that an <<extend>> or <<include>> relationship " \
       'was intended.  This can be set by opening the specification. ' \
       'Note that undirected associations should be used between ' \
       'actors and use cases.',
     %i[illegal_dependency_stereotypes no_named_dependencies
        no_plain_dependencies]],

    [:UC_BAD_ACTOR,
     8,
     'An actor that should not be on the use case diagram',
     'use case',
     "Actors that aren't associated with anything should not " \
       'appear on the use case diagram. These are often classes ' \
       'and should only be on the class diagram. If the class ' \
       'is also an actor, set the stereotype to Actor.',
     %i[unassociated_actor class_associated_to_use_case]],

    [:UNRECOGNIZED,
     3,
     'A word that does not appear in a dictionary',
     'all',
     'These are words which are in identifiers but do not appear in a ' \
       'dictionary. This can be a simply misspelling, but can also ' \
       'indicate a non-standard abbreviation. In any case, such words ' \
       'make it harder for others to interpret your model and reduce ' \
       'maintainability. Unfortunately, only English words are recognized ' \
       'at this point; support for other languages will be added in the ' \
       "future. For this check, The tool considers 'words' to be " \
       'sequences of letters starting with a capital letter or separated ' \
       'by _. If the word is a technical word in the domain, then it ' \
       'probably DOES belong in the model and should not be changed.',
     [:unrecognized_words]],

    [:USELOWERCASE,
     3,
     'A name that starts with a capital letter',
     'class',
     'The convention is that attributes (that is, class instance ' \
       'variables) and role names should start with lower case letters.',
     %i[lower_case_attributes lower_case_roles]],

    [:WIDOWCLASS,
     4,
     'A class with no associations to any other class',
     'class',
     "It's rare for a class to not be associated with any " \
       'other class.  The exception is a utility class, one that ' \
       'is associated with nearly every other class or captures a ' \
       'data type that could be built into the language. If it is a ' \
       "utility class, put the phrase 'utility class' somewhere in " \
       'the comment.',
     [:unassociated_class]],

    [:WIDOWELEMENT,
     4,
     'A use case that is not connected to anything',
     'use case',
     'Each use case must be associated with at ' \
       'least one actor, either directly or indirectly. ' \
       'Add an appropriate association or delete the element ' \
       'from the model.  Note that to delete the element, ' \
       "right click on it and chose 'Edit' and then 'Delete " \
       "from Model'.",
     [:unassociated_use_case]],

    #
    # --- elements used just by pdiff
    #
    [:ATTR_MISMATCH,
     8,
     'Misnamed, extra, or missing attributes',
     'diff',
     'Mistakes in attribute lists; either the attribute is in the wrong ' \
       'class or you may need to re-read the problem statement to identify ' \
       'information about an object to be stored. Another common mistake is ' \
       'to record an object as an attribute. For example, if an engine is ' \
       'a separate object, a car object would not list engine as an ' \
       'attribute. Spaces, underscores, and capitalization are ignored, ' \
       'and any occurrence of the word in the attribute list will be matched.',
     %i[missing_attributes unexpected_attributes]],

    [:CLASS_MISMATCH,
     8,
     'Misnamed, extra, or missing classes',
     'diff',
     'Mistakes in the classes in your model. You may have used a ' \
       'non-standard name, or you may have missed a class that was ' \
       'necessary. Re-read the problem statement for more clues. ' \
       'A common mistake is to use plurals when the singular form ' \
       'is more appropriate, such as Cars instead of Car.  Spaces, ' \
       'underscores, and capitalization are ignored.',
     %i[missing_classes unexpected_classes]],

    [:METHOD_MISMATCH,
     8,
     'Misnamed, extra, or missing methods',
     'diff',
     'Mistakes in the methods in your model. You may have missed ' \
       'an operation that is described in the problem statement or ' \
       'have placed the operation with the wrong class. ' \
       'The tool ignores spaces, underscores, and capitalization ' \
       'and looks for any occurrence of the target in ' \
       'the method name.',
     %i[missing_methods unexpected_methods]],

    [:MISPLACED_ROLE_NAME,
     7,
     'Role name is misplaced as an association name',
     'diff',
     'The expected role name appears as the name of the association - that ' \
       'is, the name that should be at one end of the association or the ' \
       'other appears to be be at the middle of the line. The name in the ' \
       'middle is a general description of the relationship, while the names ' \
       'at the ends will be the names of member variables.',
     [:misplaced]],

    [:REVERSED_ASSOCIATION,
     7,
     'Reversed roles or multiplicities in an association',
     'diff',
     "Given classes X and Y, if X ---&gt; Y and X has a variable 'data' in " \
       "of type Y is a member of class X, the role name 'data' would be at " \
       'Y end of the arrow. Likewise for multiplicities. It appears that ' \
       'the role name or multiplicities are at the wrong in in this solution.',
     [:reversed]],

    [:ROLE_MISMATCH,
     8,
     'Mismatch in multiplicities or role names',
     'diff',
     'Either a multiplicity or a role name is incorrectly specified. ' \
       'The tool looks for an exact match in both, so review the problem ' \
       'statement carefully. Note that 0..* should be written as just *. ',
     %i[incorrect_role_name missing_associations multiplicity_error
        role_associated_with_wrong_classes unexpected_associations]],

    [:SUPER_MISMATCH,
     8,
     'Mismatch in inheritance',
     'diff',
     'An error in the usage of inheritance. A common mistake is to use ' \
       'the wrong arrow for inheritance: it should be an open arrow with ' \
       'the tip pointing at the base class. This error also occurs when ' \
       'inheritance has been used but with the base classes do not match ' \
       'or when inheritance should not be used at all.',
     %i[just_one_super no_super wrong_supers]],

    [:STATE_MISMATCH,
     5,
     'Mismatch in states',
     'diff',
     'Certain states are either missing or extra. Make sure that all states ' \
       'included are necessary and that states are named in a descriptive way. ' \
       'If there is a missing state and an extra state, it is likely that the state ' \
       'is simply misnamed.',
     %i[extra_states_present states_missing]],

    [:FINAL_MISMATCH,
     5,
     'Mismatch in final nodes',
     'diff',
     'Final nodes are either missing or extra. It is possible to have multiple ' \
       'final nodes representing more than one way to exit the state machine.',
     %i[extra_final missing_final]],

    [:INITIAL_MISMATCH,
     5,
     'Mismatch in initial nodes',
     'diff',
     'Initial nodes are either missing or extra. Make sure that there is a way to ' \
       'enter the state machine. There is usually only a single initial node.',
     %i[extra_initial missing_initial]],

    [:TRANSITION_MISMATCH,
     5,
     'Mismatch in transitions',
     'diff',
     'There are many problems that can occur with transitions. Make sure that none of ' \
       'the transitions are reversed and that triggers, guards, and effects are included ' \
       'where necessary.',
     %i[extra_transitions_present transition_missing transition_reversed transition_incorrect]],

    [:MESSAGE_LABELS,
     3,
     'Messages between lifelines are labeled with method signatures',
     'sequence',
     'Ensure that every message between lifelines contain the method signature and any arguments.',
     %i[seq_message_labels seq_argument_labels]],

    [:MESSAGE_TYPES,
     3,
     'Each method signature and argument within a method signature contains a type',
     'sequence',
     'If the method signature for a message between two lifelines contains any parameters, ' \
       'each parameter should have a valid type associated with it.',
     [:seq_message_arg_types]],

    [:GUARD_CONDITIONS,
     4,
     'Guard conditions are of the proper format',
     'sequence',
     'Guard conditions should be a boolean. They should not contain any invalid characters or assignment operations.',
     [:seq_guard_conditions]],

    [:LIFELINE_INTERACTION,
     4,
     'Lifeline types should only interact with other allowed types.',
     'sequence',
     'Actors should only interact with boundary objects. Boundary objects should only interact with actors or controllers.' \
       'Controllers should only interact with boundary objects or entities and entities should only interact with controllers.',
     [:seq_lifeline_interaction]],

    [:LIFELINE_MISMATCH,
     4,
     'Mismatching lifelines',
     'sdiff',
     'There are extra/missing lifelines in the diagram or some lifelines are of the wrong type. Ensure that you have the exact number of ' \
       'lifelines of the correct type that are also spelled correctly (including capitalization).',
     %i[missing extra wrong_type]],

    [:MESSAGE_MISMATCH,
     4,
     'Mismatching messages',
     'sdiff',
     'There are extra or missing messages in the diagram. Ensure that you do not have any extra messages or ' \
       'messages that are between two lifelines that are not spelled exactly like the instructor specified ' \
       '(including capitalization). Also check that the message labels are correct.',
     %i[missing extra mislabeled]],

    [:MESSAGE_ARG_MISMATCH,
     4,
     'Mismatching message arguments',
     'sdiff',
     'There are extra, missing or invalidly-typed message arguments in the diagram. Message arguments should match ' \
       'the way the instructor spelled it exactly (including capitalization). ',
     %i[missing extra invalid_type out_of_order]],

    [:MISSING_HIGH_LEVEL,
     4,
     'Missing noun/verb',
     'all',
     'There are missing nouns or verbs in your class/sequence diagram. Class and variable names are checked for matches ' \
       'for nouns and methods are checked for verbs.',
     %i[noun verb]]
  ]

explanations.each do |name, precedence, description, diagram_type, explanation, issues|
  warn "Bad explanation entry for #{name}" if issues.class != Array
  name = name.to_s
  next if Explanation.exists? name: name

  Explanation.create [name: name,
                      precedence: precedence,
                      description: description,
                      diagram: diagram_type,
                      explanation: explanation,
                      detailed_issues: issues.join(',')]
end
# End Explanation Defaults

# Begin CheckItem Defaults
Explanation.all.each do |expl|
  expl.detailed_issue_list.each do |issue|
    next if CheckItem.where(category: expl.name, issue: issue).exists?

    CheckItem.create [category: expl.name,
                      issue: issue,
                      explanation_id: expl.id]
  end
end

# End CheckItem Defaults

# Begin Account Default
hasker_email = 'hasker@msoe.edu'
unless Instructor.where(email_address: hasker_email).exists?
  Instructor.create [email_address: hasker_email,
                     given_name: 'Robert',
                     surname: 'Hasker',
                     organization: 'MSOE',
                     account_type: Account::INSTRUCTOR,
                     password_digest: 'grape']
end
# End Account Default

# The following record is used for the remaining defaults
hasker = Instructor.where(email_address: hasker_email).first

# Begin Checklist Defaults
unless CheckList.where(description: 'SE 2030 (Default)').exists?
  CheckList.create [description: 'SE 2030 (Default)',
                    instructor_id: hasker.id,
                    language: 'Java',
                    is_inclusion_list: false,
                    retired: false]

  check_list = CheckList.where(description: 'SE 2030 (Default)').first

  CheckItem.all.each do |check_item|
    check_list.check_items << check_item
  end
end

# End Checklist Defaults

# Begin Problem Defaults
# (1/10/19) powellr - These defaults are included for demonstration purposes as per Dr. Hasker
# Begin Snow Plow Example
snow_plow_description = <<HERE
Draw a class diagram modeling a system for snow plows:

* Snow removal drivers have routes and are assigned trucks with which they clear those routes.  Each route is the responsibility of one driver.
* Trucks have numbers. There are two types of trucks: dump trucks and snow plows. For dump trucks, there is a name of the type of equipment attached.
* A route is a sequence of street segments to traverse.
* A street has a name is made up of a sequence of segments, where each segment is defined by the crossing streets that start and end that segment. If the street is a dead-end, the crossing street is NULL.
* Show classes, associations, multiplicities, attributes, operations, and generalizations.
HERE

unless Problem.where(name: 'Snow Plow Routes').nil?
  Problem.create [name: 'Snow Plow Routes',
                  description: snow_plow_description,
                  solution_file: 'snow-plow-soln.mdl',
                  author: hasker.best_name,
                  organization: hasker.organization,
                  instructor_id: hasker.id,
                  posted_at: Time.now,
                  started_at: Time.now,
                  due_at: 'Dec 31 23:59:59 -0500 2100',
                  closed_at: 'Dec 31 23:59:59 -0500 2100',
                  assignment_type: Problem::CLASS_DIAGRAM_TYPE]
end
# End Snow Plow Example
# Start Book Reader Example
book_reader_description = <<HERE
Model an electronic book reader which displays pages of books and collections.
Books have chapters, collections have articles.

* An electronic book reader has a collection of documents, and each document (from the publisher’s point of view) is associated with multiple book readers. Book readers have names.
* Documents are either books or collections.
* Books have 1 or more chapters. A book has a title and an author, both strings. A chapter has a number, a title, and the text comprising that chapter. The title and text are both strings.
* Collections have 1 or more articles, where each article has a title, an author, and the text for that article. A collection also has an editor.
* The reader has a display object which shows the contents of one “page”. Since the user can control the size of the font on the display, a page can have varying amounts of information on it.
* The display has a document currently being viewed. Again from the publisher’s point of view, each document is being viewed on zero or more displays.
* Each display is associated with pages being viewed. Each page object contains formatted text. For efficiency, the display maintains three pages: the current page, the previous page, and the next page. In the case of the current page being the first page of the document, the formatted text entry for the previous page will be empty. Likewise for the last page, where the next formatted text will be empty.
HERE

unless Problem.where(name: 'Book Reader').nil?
  Problem.create [name: 'Book Reader',
                  description: book_reader_description,
                  solution_file: 'book-reader-soln.mdl',
                  author: hasker.best_name,
                  organization: hasker.organization,
                  instructor_id: hasker.id,
                  posted_at: Time.now,
                  started_at: Time.now,
                  due_at: 'Dec 31 23:59:59 -0500 2100',
                  closed_at: 'Dec 31 23:59:59 -0500 2100',
                  assignment_type: Problem::CLASS_DIAGRAM_TYPE]
end
# End Book Reader Example
# End Problem Defaults

# (1/8/19) powellr - the following enables the previously disabled RuboCop directive and
# again does not affect the source code regardless of being installed or not
# rubocop:enable Metrics/ParameterLists
# rubocop:enable Layout/IndentHeredoc
