# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 1) do

  create_table "check_items", force: :cascade do |t|
    t.string   "category"
    t.string   "issue"
    t.integer  "explanation_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "check_items_lists", id: false, force: :cascade do |t|
    t.integer "check_item_id"
    t.integer "check_list_id"
  end

  create_table "check_lists", force: :cascade do |t|
    t.text     "description"
    t.integer  "instructor_id"
    t.string   "language"
    t.boolean  "is_inclusion_list"
    t.boolean  "retired"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "explanations", force: :cascade do |t|
    t.string   "name"
    t.integer  "precedence"
    t.text     "description"
    t.string   "diagram"
    t.text     "explanation"
    t.text     "detailed_issues"
    t.text     "good_example"
    t.text     "bad_example"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "explanations", ["name"], name: "index_explanations_on_name", unique: true

  create_table "instructors", force: :cascade do |t|
    t.string   "given_name"
    t.string   "surname"
    t.string   "organization"
    t.string   "email_address"
    t.string   "password_digest"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "problem_words", force: :cascade do |t|
    t.integer  "problem_id"
    t.text     "nouns"
    t.text     "verbs"
    t.integer  "assignment_type"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "problems", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "solution_file"
    t.string   "author"
    t.string   "organization"
    t.integer  "instructor_id"
    t.integer  "check_list"
    t.string   "assignment_type"
    t.text     "serialized_model"
    t.datetime "posted_at"
    t.datetime "started_at"
    t.datetime "due_at"
    t.datetime "closed_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "problems", ["instructor_id"], name: "index_problems_on_instructor_id"

  create_table "submissions", force: :cascade do |t|
    t.string   "type"
    t.string   "orig_name"
    t.string   "save_name"
    t.integer  "check_id"
    t.string   "problem_id"
    t.string   "from_ip"
    t.integer  "ticket"
    t.integer  "file_type_id"
    t.boolean  "generate_diagram"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "submissions", ["problem_id"], name: "index_submissions_on_problem_id"

  create_table "submissions_warnings", id: false, force: :cascade do |t|
    t.integer "submission_id"
    t.integer "warning_id"
  end

  create_table "warnings", force: :cascade do |t|
    t.text     "message"
    t.integer  "message_hash",   limit: 8
    t.integer  "explanation_id"
    t.string   "category"
    t.string   "detailed_issue"
    t.string   "element_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "warnings", ["message_hash"], name: "index_warnings_on_message_hash"

end
