class RenameTableProblemsGroupsToGroupsProblems < ActiveRecord::Migration[5.1]
  def change
    rename_table :problems_groups, :groups_problems
  end
end
