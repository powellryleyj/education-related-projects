class AddTableGroupProblems < ActiveRecord::Migration[5.1]
  def change
    create_table :group_problems do |t|
      t.integer :group_id
      t.integer :problem_id
    end
  end
end
