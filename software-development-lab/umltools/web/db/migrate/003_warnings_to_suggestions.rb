# This migration is to rename the Warnings model as ruby-2.4.x
# introduces a module called Warnings that the model is conflicting with
class WarningsToSuggestions < ActiveRecord::Migration[5.1]
  def change
    # Step 1: Renaming tables/indices of warnings table
    remove_index :warnings, :message_hash
    rename_table :warnings, :suggestions
    add_index :suggestions, :message_hash

    # Step 2: Renaming any relational tables/foreign keys
    rename_table :submissions_warnings, :submissions_suggestions
    rename_column :submissions_suggestions, :warning_id, :suggestion_id
  end
end
