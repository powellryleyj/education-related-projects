class Baseline < ActiveRecord::Migration[5.1]
  def change
    create_table :check_items do |t|
      t.string :category
      t.string :issue
      t.integer :explanation_id

      t.timestamps null: false
    end

    create_table :check_lists do |t|
      t.text :description
      t.integer :instructor_id
      t.string :language
      t.boolean :is_inclusion_list
      t.boolean :retired

      t.timestamps null: false
    end

    create_table :check_items_lists, :id => false do |t|
      t.integer :check_item_id
      t.integer :check_list_id
    end

    create_table :explanations do |t|
      t.string :name
      t.integer :precedence
      t.text :description
      t.string :diagram
      t.text :explanation
      t.text :detailed_issues
      t.text :good_example
      t.text :bad_example

      t.timestamps null: false
    end

    create_table :instructors do |t|
      t.string :given_name
      t.string :surname
      t.string :organization
      t.string :email_address
      t.string :password_digest

      t.timestamps null: false
    end

    create_table :problems do |t|
      t.string :name
      t.text :description
      t.string :solution_file
      t.string :author
      t.string :organization
      t.integer :instructor_id
      t.integer :check_list
      t.string :assignment_type
      t.text :serialized_model
      # when problem posted by user
      t.datetime :posted_at
      # when problem becomes available
      t.datetime :started_at
      # when submissions are no longer accepted
      t.datetime :due_at
      # date closed - always before or on due_at
      t.datetime :closed_at

      t.timestamps null: false
    end

    add_index :problems, :instructor_id

    create_table :submissions do |t|
      t.string :type
      t.string :orig_name
      t.string :save_name
      t.integer :check_id
      t.string :problem_id
      t.string :from_ip
      t.integer :ticket
      t.integer :file_type_id
      t.boolean :generate_diagram
      t.datetime :time

      t.timestamps null: false
    end

    add_index :submissions, :problem_id

    create_table :warnings do |t|
      t.text :message
      t.integer :message_hash, :limit => 8
      t.integer :explanation_id
      t.string :category
      t.string :detailed_issue
      # rwh, March 2016 added this because it was in the schema file as a string
      #    TODO: (open question: should this be an integer instead?)
      t.string :element_id

      t.timestamps null: false
    end

    add_index(:explanations, :name, {:unique=>true})
    # note: can't add :unique=>true for message_hash since
    #       multiple messages can have the same hash
    add_index(:warnings, :message_hash)

    create_table :submissions_warnings, :id => false do |t|
      t.integer :submission_id
      t.integer :warning_id
    end

    create_table :problem_words do |t|
      t.references :problem
      t.text :nouns
      t.text :verbs
      t.integer :assignment_type

      t.timestamps null: false
    end

# rwh, Mar 2016: this was in an early migration; not sure it's still needed
#     TODO: delete?
#     default_instructor = Instructor.new do |u|
#       u.given_name = "Default"
#       u.surname = "Default"
#       u.organization = "Default"
#       u.email_address = "Default"
#       u.password_digest = "empty"
#       u.created_at = u.updated_at = DateTime.now
#     end
  end
end
