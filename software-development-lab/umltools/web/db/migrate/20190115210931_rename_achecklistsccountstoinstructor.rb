class RenameAchecklistsccountstoinstructor < ActiveRecord::Migration[5.1]
  def change
    rename_column :check_lists, :account_id, :instructor_id
  end
end
