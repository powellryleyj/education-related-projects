class AddColumnInstructorIdToGroups < ActiveRecord::Migration[5.1]
  def change
    add_column :groups, :instructor_id, :integer
  end
end
