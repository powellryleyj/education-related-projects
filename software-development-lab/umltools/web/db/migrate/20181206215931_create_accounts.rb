class CreateAccounts < ActiveRecord::Migration[5.1]
  def change
    create_table :accounts do |t|
      t.string :given_name
      t.string :surname
      t.string :organization
      t.string :email_address
      t.string :password_digest
      t.integer :account_type

      t.timestamps
    end
  end
end
