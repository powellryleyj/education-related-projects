class RenameTableGroupMiddleMans < ActiveRecord::Migration[5.1]
  def change
    rename_table :group_students, :student_groups
    rename_table :group_problems, :problem_groups
  end
end
