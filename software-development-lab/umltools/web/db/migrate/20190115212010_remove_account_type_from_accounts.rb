class RemoveAccountTypeFromAccounts < ActiveRecord::Migration[5.1]
  def change
    remove_column :accounts, :account_type
  end
end
