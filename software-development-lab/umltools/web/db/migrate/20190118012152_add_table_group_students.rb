class AddTableGroupStudents < ActiveRecord::Migration[5.1]
  def change
    create_table :group_students do |t|
      t.integer :group_id
      t.integer :student_id
    end
  end
end
