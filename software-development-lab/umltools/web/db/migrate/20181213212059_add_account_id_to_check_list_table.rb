class AddAccountIdToCheckListTable < ActiveRecord::Migration[5.1]
  def change
    rename_column :check_lists, :instructor_id, :account_id
  end
end
