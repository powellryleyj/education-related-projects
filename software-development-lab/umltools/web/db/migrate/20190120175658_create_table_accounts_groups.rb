class CreateTableAccountsGroups < ActiveRecord::Migration[5.1]
  def change
    create_table :accounts_groups do |t|
      t.integer :student_id
      t.integer :group_id
    end
  end
end
