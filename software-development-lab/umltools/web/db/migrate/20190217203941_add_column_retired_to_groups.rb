class AddColumnRetiredToGroups < ActiveRecord::Migration[5.1]
  def change
    add_column :groups, :retired, :boolean
  end
end
