# (1/9/19) powellr - removes unused columns from the explanations table
class RemoveGoodBadExamples < ActiveRecord::Migration[5.1]
  def up
    remove_column :explanations, :good_example
    remove_column :explanations, :bad_example
  end

  def down
    add_column :explanations, :good_example
    add_column :explanations, :bad_example
  end
end
