class RenameTableGroupAssociationTables < ActiveRecord::Migration[5.1]
  def change
    rename_table :student_groups, :students_groups
    rename_table :problem_groups, :problems_groups
  end
end
