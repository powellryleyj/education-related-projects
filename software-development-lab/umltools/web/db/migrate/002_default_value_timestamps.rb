class DefaultValueTimestamps < ActiveRecord::Migration[5.1]
  def change

    change_column_default :check_items, :created_at, from: nil, to: Time.now
    change_column_default :check_items, :updated_at, from: nil, to: Time.now

    change_column_default :check_lists, :created_at, from: nil, to: Time.now
    change_column_default :check_lists, :updated_at, from: nil, to: Time.now

    change_column_default :explanations, :created_at, from: nil, to: Time.now
    change_column_default :explanations, :updated_at, from: nil, to: Time.now

    change_column_default :instructors, :created_at, from: nil, to: Time.now
    change_column_default :instructors, :updated_at, from: nil, to: Time.now

    change_column_default :problems, :created_at, from: nil, to: Time.now
    change_column_default :problems, :updated_at, from: nil, to: Time.now

    change_column_default :submissions, :created_at, from: nil, to: Time.now
    change_column_default :submissions, :updated_at, from: nil, to: Time.now

    change_column_default :problem_words, :created_at, from: nil, to: Time.now
    change_column_default :problem_words, :updated_at, from: nil, to: Time.now

    remove_timestamps :warnings
  end
end
