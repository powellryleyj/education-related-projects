# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Initialize the Rails application.
# (1/27/19) powellr - not sure if this still needs to exist since the initializer has been commented out
# in umlint_umlgrader_setip.rb
Umlint::Application.initialize!
