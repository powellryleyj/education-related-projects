# This module is meant to make it so when Instructor.all is called
# it will filter out the students and return Instructor instances
# rather than Account instances
#
# The second functionality is to make sure that people don't call the
# child method within an Instructor instance. All though doing so doesnt
# affect anything, it may lead to confusion later on if not blocked

module InstructorQueries
  def self.included base
    base.instance_eval do
      def all
        accounts = Account.all.where(account_type: Account::INSTRUCTOR)
        accounts.each { |account| account.becomes(Instructor)}
        return accounts
      end
    end

    base.class_eval do
      def child
        raise "Method Blocked By InstructorQueries Module"
      end
    end
  end
end

Instructor.send(:include, InstructorQueries)
