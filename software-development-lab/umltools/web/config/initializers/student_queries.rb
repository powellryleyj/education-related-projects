# This module is meant to make it so when Student.all is called
# it will filter out the instructors and return Student instances
# rather than Account instances
#
# The second functionality is to make sure that people don't call the
# child method within an Student instance. All though doing so doesnt
# affect anything, it may lead to confusion later on if not blocked

module StudentQueries
  def self.included base
    base.instance_eval do
      def all
        accounts = Account.all.where(account_type: Account::STUDENT)
        accounts.each { |account| account.becomes(Student)}
        return accounts
      end
    end

    base.class_eval do
      def child
        raise "Method Blocked By StudentQueries Module"
      end
    end
  end
end

Student.send(:include, StudentQueries)
