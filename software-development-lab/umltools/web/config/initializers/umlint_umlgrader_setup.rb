# umlint_umlgrader_setup.rb: application-specific initialization

# ensure all database items set up:
# config.after_initialize do
# (1/8/19) powellr - commented out since db defaults will be installed via
#                   db/seeds.rb
#   begin
#     Explanation.install_explanations
#     CheckList.install_defaults
#     Problem.setup_lab_examples_used_in_papers
#   rescue
#     # Ignore; this is needed since the statement gets executed when migrating
#     # (since the table doesn't exist at this point).
#   end
# end
