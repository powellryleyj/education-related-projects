Umlint::Application.routes.draw do
  # The priority is based upon order of creation:
  # first created -> highest priority.
  match '/umlint/index', to: 'umlint#index', via: [:get, :post]
  get 'umlint/tutorial', to: 'umlint#tutorial'
  get '/umlint/about', to: 'umlint#about'
  get '/umlint/checks', to: 'umlint#checks'

  match '/umlint/view', to: 'umlint#view', via: [:get, :post]
  get '/umlint/view_checklist', to: 'umlint#view_checklist'

  get '/umlint/login', to: 'umlint#login'
  match '/umlint/do_login', to: 'umlint#do_login', via: [:get, :post]
  get '/umlint/logout', to: 'umlint#logout'

  get '/umlint/manage_password', to: 'umlint#manage_password'
  post '/umlint/manage_password', to: 'umlint#new_password'

  match '/umlint/edit', to: 'umlint#edit', via: [:get, :post]
  get '/umlint/show_checklist_info', to: 'umlint#show_checklist_info'
  post '/umlint/do_update_checklist', to: 'umlint#do_update_checklist'

  post '/umlint/results', to: 'umlint#results'
  get '/umlint/show_warning_details/:item', to: 'umlint#show_warning_details'
  # these two must be below above ones so subset doesn't pick up action
  get '/umlint/:subset', to: 'umlint#index'
  get '/umlint', to: 'umlint#index'

  # umlgrader versions
  match '/umlgrader/create_instructor', to: 'umlgrader#create_instructor', via: [:get, :post]
  match '/umlgrader/modify_instructor', to: 'umlgrader#modify_instructor', via: [:get, :post]
  match '/umlgrader/index',   to: 'umlgrader#index',   via: [:get, :post]
  match '/umlgrader/results', to: 'umlgrader#results', via: [:get, :post]
  get 'umlgrader/tutorial', to: 'umlgrader#tutorial'
  get '/umlgrader/checks', to: 'umlgrader#checks'
  match '/umlgrader/view', to: 'umlgrader#view', via: [:get, :post]
  match '/umlgrader/edit', to: 'umlgrader#edit', via: [:get, :post]
  get '/umlgrader/show_checklist_info', to: 'umlgrader#show_checklist_info'
  post '/umlgrader/do_update_checklist', to: 'umlgrader#do_update_checklist'
  get '/umlgrader/about', to: 'umlgrader#about'
  get '/umlgrader/show_grader_results/:item', to: 'umlgrader#show_grader_results'
  get '/umlgrader/create_assignment', to: 'umlgrader#create_assignment'
  get '/umlgrader/modify_assignment/:id', to: 'umlgrader#modify_assignment', as: 'problem'
  get '/umlgrader/view_submissions/:id', to: 'umlgrader#view_submissions', as: 'submissions' # New route for submissions
  get '/umlgrader/download_submission_file/:id', to: 'umlgrader#download_submission_file', as: 'download_submission'
  get '/umlgrader/download_group_submissions/:id/:group_id', to: 'umlgrader#download_group_submissions', as: 'download_group_submissions'
  delete '/umlgrader/delete_assignment/:id', to: 'umlgrader#destroy', as: 'umlgrader_delete'
  post '/umlgrader/upload_assignment', to: 'umlgrader#upload_assignment'
  post '/umlgrader/upload_assignment/:id', to: 'umlgrader#reupload_assignment'
  get 'umlgrader/manage_assignments', to: 'umlgrader#manage_assignments'
  get '/umlgrader/logout', to: 'umlgrader#logout'
  # these two must be below above ones so problem doesn't pick up action
  get '/umlgrader/', to: 'umlgrader#index'
  get '/umlgrader/tutorial/download', to: 'umlgrader#download_file'
  match '/umlgrader/download_checklist_file/:checklist_id', to: 'umlgrader#download_checklist_file', as: 'download_checklist_file', via: :get
  match '/umlgrader/upload_checklist_file', to: 'umlgrader#upload_checklist_file', as: 'upload_checklist_file', via: :post
  get '/umlgrader/manage_groups', to: 'umlgrader#manage_groups'
  get '/umlgrader/manage_group/:id', to: 'umlgrader#manage_group', as: 'manage_group'
  post '/umlgrader/group_update_descriptions/:id', to: 'umlgrader#group_update_descriptions', as: 'group_upate_descriptions'
  post '/umlgrader/create_group/', to: 'umlgrader#create_group', as: 'create_group'
  post '/umlgrader/group_add_students/:id', to: 'umlgrader#group_add_students', as: 'group_add_students'
  post '/umlgrader/group_add_problem/:id', to: 'umlgrader#group_add_problem', as: 'group_add_problem'
  delete '/umlgrader/delete_group_student/:group/:student', to: 'umlgrader#delete_group_student', as: 'delete_group_student'
  delete '/umlgrader/delete_group_problem/:group/:problem', to: 'umlgrader#delete_group_problem', as: 'delete_group_problem'
  delete '/umlgrader/delete_group/:id', to: 'umlgrader#delete_group', as: 'delete_group'


  # get '/umlgrader/submit_bug_report', to: 'umlgrader#submit_bug_report'

  post '/desktop/umlgrader', to: 'umlgrader#desktop_results'
  post '/desktop/umlint', to: 'umlint#desktop_results'
  get '/desktop/', to: 'umlgrader#desktop_options'



  # Sample of regular route:
  #   get 'products/:id', to: 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   get 'products/:id/purchase', to: 'catalog#purchase', :as, to: :purchase
  # This route can be invoked with purchase_url(:id, to: product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on, to: :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to, to: 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # get ':controller(/:action(/:id))(.:format)'
end
