# General Information

## Project

[UML Tools: UMLGrader & UMLint](http://umltools.msoe.edu)

## Authors and Contributors

[UMLint](http://umltools.msoe.edu/umlint/about)

[UMLGrader](http://umltools.msoe.edu/umlgrader/about)

## Software Development Lab 2018\-2019

### Team Members

+ Ryley Powell (powellr)
+ John Benson (bensonja)
+ Nick Gorecki (goreckinj)

### Environment

+ 64-bit Ubuntu 18.04.1 LTS
+ Ruby-2.4.5
+ Rails-5.1.6

## Guide Assumptions

+ Commands are run from the terminal
+ Using Ubuntu Linux or other similar Linux platform

--------------------------------------------------------------------------------

# Build Instructions

## Dependencies

####	1. RVM \- See [this](https://rvm.io/rvm/install) to install

```
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
\curl -sSL https://get.rvm.io | bash -s stable --ruby
```

####	2. Aspell

```
# (10/27) powellr - Not sure if Aspell is really needed. Kept it as reference anyways.
sudo apt-get install -y aspell
```

####  3. Ruby and Rails

```
# (10/27) powellr - May have to use shell '/bin/bash --login'.
rvm install 2.4.5
gem install rails 5.1.6
gem install bundle
```

####	4. **(Optional)** Using Other Versions

```
# (10/27) powellr - Included this since the UMLGrader19 team for SDL18-19 is attempting to update the used versions of Ruby/Rails.
rvm install x.x.x       # Where x.x.x is the Ruby version you'd like to use.
rvm use x.x.x           # powellr - May have to use shell '/bin/bash --login'.
```

--------------------------------------------------------------------------------

## Build Directions

### General Instructions

####	1. Copy Baseline Configuration Files

```
# copy over development.rb
cd config/environments
cp baseline_development.rb development.rb

# copy over database.yml
cd config
cp baseline_database.yml database.yml
```

####	2. Install Ruby Gems

```
cd web
bundle install
```

--------------------------------------------------------------------------------

### Testing

####	1. Navigate to Web Directory

```
cd web
```

####	2. Export Environment

```
export RAILS_ENV=test
```

####	3. Create/Migrate Database

**Notes:** 

+ **The two following commands** only need to be run once to build the initial database.
+ For useful tips for managing/changing the database, see the section under `MISC: Database Management` later on this page.

```
# (10/27) powellr - This should only need to be done for the first you create the DB for the environemnt context
#
# Creates the baseline schema.rb file if it doesn't already exist, and any relevant *.sqlite3 files for the environment
# set in step 2.
rake db:create

# (12/9/18) powellr - Should be used after updating the DB that has been reflected via
# a migration file under umltools/web/db/migration/*.rb
#
# Updates the baseline schema.rb and relevant *.sqlite3 files based on migrations found under umltools/web/db/migration/*.rb
rake db:migrate
```

####	4. Run Tests

```
# Run all tests
rake test:run_all

# Test rails side
rake test

# Test umlgrader and umlint core
rake lib:test
```

--------------------------------------------------------------------------------

### Production Mode

####	1. Export Environment

```
export RAILS_ENV=production
```

####	3. Create/Migrate Database

**Notes:** 

+ **The two following commands** only need to be run once to build the initial database.
+ For useful tips for managing/changing the database, see the section under `MISC: Database Management` later on this page.

```
# (10/27) powellr - This should only need to be done for the first you create the DB for the environemnt context
#
# Creates the baseline schema.rb file if it doesn't already exist, and any relevant *.sqlite3 files for the environment
# set in step 2.
rake db:create

# (12/9/18) powellr - Should be used after updating the DB that has been reflected via
# a migration file under umltools/web/db/migration/*.rb
#
# Updates the baseline schema.rb and relevant *.sqlite3 files based on migrations found under umltools/web/db/migration/*.rb
rake db:migrate
```

####	3. Create Instructor

```
rake instructor:create
```

Follow the terminal prompts.

####	4. Pre\-compile Assets

```
# (10/27) powellr - Unsure how often this is needed.
rake assests:precompile
```

####	5. Start Server

```
# (10/27) powellr - The following seems to produce some warnings. Doesn't seem to have an effect however.
rvmsudo rails server -p 80 -e production

# (10/27) powellr - The following doesn't seem to work using Ruby-2.3.0 and Rails-4.2.10. Kept it as reference.
sudo rails server -p 80 -e production
```

####	6. Open via Browser

`http://localhost:80`

####	7. **(Optional)** Run Manual Tests

Navigate to `http://localhost/umlgrader`

**a. No Errors**

*File:* `web/test/lib/resources/clean.mdl`

*Expected Output:*

`No error output should be reported`

**b. Errors with All Checks Enabled**

*File:* `web/test/lib/resources/min-bad.mdl`

*Expected Output:*

> Class SubClass inherits from multiple classes (BaseClass, AltBase) - generalization arrows may be backwards
> Association between Stuff and BaseClass: composition should have multiplicity 1
> Attribute stuff in class BaseClass appears to not be simple - replace by an association with the class
> Complex attribute nums[] in class Stuff - replace by an association to a container
> Missing documentation: classes BaseClass, SubClass, Stuff, AltBase
> Missing documentation for class Stuff member: nums[]
> Missing documentation: classes BaseClass, SubClass, Stuff, AltBase

**c. Errors with All Checks Enabled**

*File:* `web/test/lib/resources/xmi/min2.xmi`

*Expected Output:*

> Attribute color in Bottle is public
> Attribute height in Baby is public
> Missing documentation for class Baby member: height
> Missing documentation for class Bottle member: color
> Missing documentation: classes Baby, Bottle

####	8. Shutdown Server

`CTRL+C` at the terminal that is running the server

--------------------------------------------------------------------------------

# TeamCity

**To Do**

+ Write about `Gemfile` configuration
+ Write about Ruby/Rails versions
+ Write about TeamCity Build-Steps

--------------------------------------------------------------------------------

# Deployment

**To Do**

+ Write about deployment depending on Dr. Hasker's feedback on whether this should be included

--------------------------------------------------------------------------------

# MISC

## Database Management
**To Do**
+ Migration files
+ Migration version numbers
+ `rake db:migrate`
+ `rake db:rollback`
+ Link Rails Guide

## RuboCop \- Ruby/Rails Static Analysis Tool

### Installation

**DO NOT ADD THIS TO THE GEMFILE.**

This tool is for *your* convenience in debugging; not for the project.
RuboCop also integrates nicely with the **RubyMine** IDE from JetBrains&trade;

+ Run `gem install rubocop` from terminal

### Configuration

RuboCop can be explicitly controlled via:

+ `.rubocop.yml`
+ `.rubocop_todo.yml`

**Note:** These configuration files should **not** be checked into the repository and have been ignored by `.gitignore`

### Documentation

You can find the documentation [here](https://rubocop.readthedocs.io/en/latest)

--------------------------------------------------------------------------------

**To Do**  

+ Link wherever current, as of 10/27/18, [README.rdoc](http://bitbucket.org/hasker/umltools/src/master/web/README.rdoc) gets archived
