#Here are some things to note once you pull the branch with the base accounts setup:

##Have two migrations with the following

###MIGRATION 1:
'''
class CreateAccounts < ActiveRecord::Migration[5.1]
  def change
    create_table :accounts do |t|
      t.string :given_name
      t.string :surname
      t.string :organization
      t.string :email_address
      t.string :password_digest
      t.integer :account_type

      t.timestamps
    end
  end
end
'''

###MIGRATION 2:
+ In schema.rb, MAKE SURE that instructor_id in check_lists table to account_id
'''
class AddAccountIdToCheckListTable < ActiveRecord::Migration[5.1]
  def change
    rename_column :check_lists, :instructor_id, :account_id
  end
end
'''
