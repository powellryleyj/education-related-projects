UMLGrader Project Contributors:

2013-2014 MSOE Contributors:
Name                  Email                   phone #
Matthew Herman	    hermanm@msoe.edu	    (920) 627-4214
Brett Nockerts	    nockertsb@msoe.edu	    (920) 764-0593
Clifford Kimpel	    kimpelc@msoe.edu	    (608) 728-3278
Andrew Scott    	atscott01@gmail.com	    (617) 394-8788
Rebecca Chandler	chandlerr@msoe.edu	    (414) 745-4020

How to build: nothing to build

See web/README.rdoc for setup instructions. 
