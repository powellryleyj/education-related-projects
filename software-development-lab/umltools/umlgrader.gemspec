Gem::Specification.new do |s|
  s.name        = 'UMLGrader'
  s.version     = '1.0.0'
  s.platform    = Gem::Platform::RUBY
  s.author      = 'Dr. Robert Hasker'
  s.authors     = ['Dr. Robert Hasker', 'Matthew Herman', 'Brett Nockerts', 'Clifford Kimpel',
                   'Andrew Scott', 'Rebecca Chandler', 'Brennan Kastner', 'Kevin Spaeth',
                   'Mitchell Morrell', 'Evan Ronayne']
  s.email       = 'hasker@msoe.edu'
  s.homepage    = 'http://umltools.msoe.edu/'
  s.summary     = 'Tools for learning portions of UML.'
  s.description = 'See umltools.msoe.edu for more information.'

  # If you have other dependencies, add them here
  s.add_dependency 'actionpack-action_caching'
  s.add_dependency 'actionpack-page_caching'
  s.add_dependency 'activerecord-deprecated_finders'
  s.add_dependency 'bcrypt', '~> 3.1.7'
  s.add_dependency 'bootstrap-datepicker-rails'
  s.add_dependency 'bootstrap-sass', '~> 3.1.1'
  s.add_dependency 'ci_reporter', '~> 2.0'
  s.add_dependency 'ci_reporter_test_unit'
  s.add_dependency 'coffee-rails', '~> 4.0.0'
  # s.add_dependency 'flexmock'
  s.add_dependency 'jbuilder', '~> 1.2'
  s.add_dependency 'jquery-rails'
  s.add_dependency 'jquery-turbolinks'
  s.add_dependency 'jquery-ui-rails'
  s.add_dependency 'mysql'
  s.add_dependency 'protected_attributes'
  s.add_dependency 'rails', '~> 4.2'
  s.add_dependency 'rails-observers'
  s.add_dependency 'rake', '~> 10.3.2'
  s.add_dependency 'responders', '~> 2.0'
  s.add_dependency 'sass-rails', '~> 4.0.0'
  s.add_dependency 'sdoc'
  s.add_dependency 'simplecov'
  s.add_dependency 'simplecov-rcov'
  s.add_dependency 'simplecov-teamcity-summary'
  s.add_dependency 'test-unit'
  s.add_dependency 'therubyracer'
  s.add_dependency 'turbolinks'
  s.add_dependency 'uglifier', '>= 1.3.0'
  s.add_dependency 'unicorn'
  s.add_dependency 'utf8-cleaner'

  s.required_rubygems_version = '>= 1.3.6'

  # If you need to check in files that aren't .rb files, add them here
  s.files        = Dir['lib/**/*.rb'] + Dir['test/lib/**/*']
  s.require_path = 'lib'

  # If you need an executable, add it here
  s.executables = ['umlgrader', 'umlint', 'umldump', 't']
end
