import json
import webbrowser
from urllib.error import URLError
from urllib.request import urlopen

import requests
from PyQt5 import QtWidgets
from PyQt5.QtCore import QDir, QUrl
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtWidgets import QFileDialog, QErrorMessage, QMessageBox

from ui.mainwindow import Ui_MainWindow
# noinspection PyUnresolvedReferences
from ui.icons import *

UML_TOOLS_DEFAULT_URL = 'http://umltools.msoe.edu'


class UmlToolsWindow(object):
    _post_boundary = '------WebKitFormBoundaryOLbXQCgO8LsriGQ8'
    loaded = False

    def __init__(self, settings):
        self.settings = settings

        # Setup the main UI
        self.window = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self.window)

        # Prepare UI
        self.prepare_ui()

        # Add action listeners
        self.connect_actions()

        # Show window
        self.window.show()

        # Fetch options from UMLTools
        self.load_ui_options()

    @property
    def url(self):
        return self.ui.urlEdit.text()

    def prepare_ui(self):
        url = self.settings.value('url', type=str)
        if not url:
            self.settings.setValue('url', UML_TOOLS_DEFAULT_URL)
            url = UML_TOOLS_DEFAULT_URL

        self.ui.urlEdit.setText(url)
        self.window.setWindowIcon(QIcon(QPixmap(':/main/uml-logo-1024x1024.png')))

    def connect_actions(self):
        """Connect button and menu actions with their respective functions"""
        # Menu actions
        self.ui.actionOpen_Website.triggered.connect(lambda: webbrowser.open(self.url))
        self.ui.actionExit.triggered.connect(lambda: self.close())

        # File picker
        self.ui.chooseFileBtn.clicked.connect(self.pick_file)

        # Buttons
        self.ui.refreshBtn.clicked.connect(self.load_ui_options)
        self.ui.submitBtn.clicked.connect(self.submit)
        for btn in [self.ui.submitBtn, self.ui.refreshBtn]:
            btn.clicked.connect(self.save_url)

    def save_url(self):
        self.settings.setValue('url', self.url)

    def load_ui_options(self):
        """Fetch the assignment and checklist information from the UML Tools website"""
        self.window.problems = {}
        self.window.checklists = {}
        self.ui.assignmentBox.clear()
        self.ui.checklistBox.clear()
        try:
            response = urlopen('{}/desktop'.format(self.url)).read().decode('utf-8')
            data = json.loads(response)

            for problem in data['problems']:
                self.ui.assignmentBox.addItem(problem[0])
                self.window.problems[problem[0]] = problem[1]
            for checklist in data['checklists']:
                self.ui.checklistBox.addItem(checklist[0])
                self.window.checklists[checklist[0]] = checklist[1]
            self.loaded = True
        except URLError:
            self.loaded = False
            QErrorMessage(self.window).showMessage(
                'An error was encountered when attempting to reach the UML Tools website')

    def pick_file(self):
        """Launches the file picker dialog and updates the label text with the selected filename"""
        file_name, _ = QFileDialog().getOpenFileName(self.window, 'Select file', '', 'XMI (*.xmi)')
        self.window.selectedFile = file_name
        # Update label text
        self.ui.fileLabel.setText(file_name.split(QDir().separator())[-1])

    def submit(self):
        """Submit the file to UML tools and render the response in the webview"""
        if not getattr(self.window, 'selectedFile', None):
            QMessageBox(self.window).critical(self.window, 'Error!', 'You must select a file!')
            return

        if not self.loaded:
            QMessageBox(self.window).critical(self.window, 'Error!',
                                              'You are not connected to UML Tools! Try hitting the refresh button.')
            return

        # Create the request
        if self.ui.tabWidget.currentWidget() == self.ui.lintTab:
            # UMLint
            url = '{}/desktop/umlint'.format(self.url)
            files = {'model_file[]': open(self.window.selectedFile)}
            data = {
                'checklist_id': self.window.checklists[self.ui.checklistBox.currentText()],
                'file_type_id': self.ui.fileTypeBox.currentIndex(),
                'utf8': u'\u2713'
            }
        else:
            # UMLGrader
            url = '{}/desktop/umlgrader'.format(self.url)
            files = {'model_file[]': open(self.window.selectedFile)}
            data = {
                'utf8': u'\u2713',
                'commit': 'Grade',
                'assignment': self.window.problems[self.ui.assignmentBox.currentText()]
            }

        result = requests.post(url, data=data, files=files)
        self.ui.webView.setHtml(result.text, QUrl(self.url))
