import sys

from PyQt5 import QtWidgets
from PyQt5.QtCore import QSettings

from window import UmlToolsWindow


def run():
    # Create the Qt application
    app = QtWidgets.QApplication(sys.argv)

    settings = QSettings('MSOE', 'UMLTools')

    # Setup the main UI
    _ = UmlToolsWindow(settings)

    # Launch application
    sys.exit(app.exec_())

if __name__ == '__main__':
    run()