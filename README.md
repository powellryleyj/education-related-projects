# Academic Projects
This repository contains work that I've produced/contributed to over the course of my time at [Milwaukee School of Engineering](www.msoe.edu) (MSOE).

The following table contains links to the top most directory of work/labs/projects within specific subjects as well as links to the official course descriptions. Click [here](https://catalog.msoe.edu/preview_program.php?catoid=24&poid=1204&returnto=710#acalog-page-title) for program overview.

**Note: All work _within this repository is archived as is_ with no changes since having finished the respective assignment/project/course.**
