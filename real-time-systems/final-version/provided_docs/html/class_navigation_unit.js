var class_navigation_unit =
[
    [ "NavigationUnit", "class_navigation_unit.html#a58eb7d09c1648decb0a0b0f39cfca620", null ],
    [ "~NavigationUnit", "class_navigation_unit.html#add21663cbf953b05d1d7014dc9848c4b", null ],
    [ "calibrateBlack", "class_navigation_unit.html#a29bd866b16f7490ee689b6ffb3f7f63e", null ],
    [ "calibrateWhite", "class_navigation_unit.html#a49a9380f0ab9916f3ccd3192ecaa22ea", null ],
    [ "handlePIDCalibrationUpdate", "class_navigation_unit.html#a91c5d25fe78b1cfc8ca0f61fd598d17b", null ],
    [ "setThresholds", "class_navigation_unit.html#a813d5a68ee0041d93db7b5c4699f4ce5", null ],
    [ "startChildRunnables", "class_navigation_unit.html#a671227ed410c4848dd7eb12048771b0c", null ],
    [ "stop", "class_navigation_unit.html#a1c624e12bbe07f35c99d6662c1fad5fd", null ],
    [ "taskMethod", "class_navigation_unit.html#a2d64fb5933a69684910710a4de54a0db", null ],
    [ "waitForShutdown", "class_navigation_unit.html#aab01754c104ff4436083cabc92a7b459", null ],
    [ "blacks", "class_navigation_unit.html#a7b6b6e322da43f58a4dee69ab4f35bcb", null ],
    [ "dataReader", "class_navigation_unit.html#a480e95f8c4c50aacc68b28b6e029dea1", null ],
    [ "driveQueue", "class_navigation_unit.html#a8b51a8efa051a9b8447e157ab43f60db", null ],
    [ "instructionQueue", "class_navigation_unit.html#a0813cfbf0ad96bbcec8349e54380ff19", null ],
    [ "lineFollowingActive", "class_navigation_unit.html#a175838dbac93dda7fbbf90eeea85104a", null ],
    [ "myPIDCtrl", "class_navigation_unit.html#a81b4678d632d9ebeab96c5946d1afe3d", null ],
    [ "thresholds", "class_navigation_unit.html#a8888c2b43cd92a464ef24779ec8c7523", null ],
    [ "whites", "class_navigation_unit.html#a7b1367953dbd3cc7ee38ec9a35e2d77f", null ]
];