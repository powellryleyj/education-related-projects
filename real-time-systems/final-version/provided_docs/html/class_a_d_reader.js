var class_a_d_reader =
[
    [ "ADReader", "class_a_d_reader.html#a74857b1691ff14b535ede856db46eff3", null ],
    [ "~ADReader", "class_a_d_reader.html#ac1394114abef9aded4f859343441a872", null ],
    [ "readChannelMax", "class_a_d_reader.html#abb1ba544c4c4c6cbfc189e1eb575d388", null ],
    [ "readChannelMin", "class_a_d_reader.html#aba5ecd2386970ea09abe32a2eb881e39", null ],
    [ "readChannelValue", "class_a_d_reader.html#a0a54a60282e68b6a095b6540db607e5b", null ],
    [ "readSensors", "class_a_d_reader.html#a148716d5cf558ab518a81cd18f720b7c", null ],
    [ "taskMethod", "class_a_d_reader.html#a0439ce43995e69295f8e78db5dcd5ec6", null ],
    [ "addressIOPin", "class_a_d_reader.html#a78a44b10319445b68e24f4504062b53d", null ],
    [ "clockPin", "class_a_d_reader.html#ae2adaa820d0c1f072539eec73f85dddb", null ],
    [ "csIOPin", "class_a_d_reader.html#a9320e5da530d2b6af3f6525884f3f46d", null ],
    [ "dataMutex", "class_a_d_reader.html#a4268b917366ebbfb9e754afc75a9783d", null ],
    [ "dataValues", "class_a_d_reader.html#a7ea2ebe0333b7b98c50e78debd5c89b3", null ],
    [ "doutPin", "class_a_d_reader.html#aebb2a2071343d24495d92d1a2bd03a35", null ],
    [ "maxDataValue", "class_a_d_reader.html#ac3bae9aa6ecb0783edc8eeafb9bf6430", null ],
    [ "minDataValue", "class_a_d_reader.html#a5d3ba6d1ec420fcd5d384e14be5ad451", null ]
];