var class_camera =
[
    [ "Camera", "class_camera.html#adb00ec9da175d671ab7c2dff0b207195", null ],
    [ "~Camera", "class_camera.html#ad1897942d0ccf91052386388a497349f", null ],
    [ "run", "class_camera.html#aaa3745c0cf0f286ef80b7eeebc248cc9", null ],
    [ "shutdown", "class_camera.html#ac8b41cf13a2d7e6f8492c6b5054caa29", null ],
    [ "takePicture", "class_camera.html#a8343f178c37c4eadb5d86b18e449a0e7", null ],
    [ "capture", "class_camera.html#a34fc51a73f10885bc365331b1fee03a0", null ],
    [ "lastFrame", "class_camera.html#ae3b77117566bc37d65b844fd17ea39d3", null ],
    [ "mtx", "class_camera.html#afacecc8145f0ffc8ad42bb71809283e1", null ]
];