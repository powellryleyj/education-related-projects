/**
 * @file main.cpp
 * @author  Walter Schilling (schilling@msoe.edu)
 * @version 1.0
 *
 * @section LICENSE This is a piece of a template solution to the lab.
 *
 *
 * @section DESCRIPTION
 *      This is the main method for the basic robot.  It will control the simplest operation of the robot over the network.
 */
using namespace std;
#include "ImageTransmitter.h"
#include "Camera.h"
#include "ImageCapturer.h"
#include <chrono>
#include <pthread.h>
#include <iostream>
#include "RunnableClass.h"
#include <sys/syscall.h>
#include <unistd.h>
#include <cstdlib>
#include <thread>
#include "GPIO.h"
#include "RobotController.h"
#include "NetworkCommands.h"
#include "RobotCfg.h"
#include "PWMManager.h"
#include "TaskRates.h"
#include "DistanceSensor.h"
#include "TaskRates.h"
#include "DiagnosticManager.h"
#include "NetworkManager.h"
#include "CollisionSensingRobotController.h"
#include "NavigationUnit.h"

using namespace std;



/**
 * This is the main program.  It will instantiate a network manager and robot controller as well as a Command Queue.
 * It will then block until the user enters a message on the console.
 * This will then cause it to shutdown the other classes and wait for their threads to terminate.
 */
int main(int argc, char* argv[]) {
	// These are the image sizes for the camera (c) and the transmitted image (t), both height (h) and width (w).
	int cw, ch, tw, th, fps, lpudp;

	if (argc != 9)
	{
		printf("Usage: %s ip port cameraWidth cameraHeight TransmitWidth transmitHeight <frame per second to send> <Lines per UDP Message>", argv[0]);
		exit(0);
	}

	cout << "Main thread id is : " << syscall(SYS_gettid) << "\n";
	// Convert the parameters into integers.
	int port = atoi(argv[2]);
	cw = atoi(argv[3]);
	ch = atoi(argv[4]);
	tw = atoi(argv[5]);
	th = atoi(argv[6]);
	fps = atoi(argv[7]);
	lpudp = atoi(argv[8]);

	CommandQueue *myQueue[NUMBER_OF_QUEUES];
	CommandQueue *driveQueue = myQueue[0] = new CommandQueue();
	CommandQueue *diagQueue = myQueue[1] = new CommandQueue();
	CommandQueue *navigationCommandQueue = myQueue[2] = new CommandQueue();


	//Declare a pair of players
	NetworkReceptionManager nm(9090, myQueue, "NetworkManager");
	NetworkTransmissionManager nt(&nm, "Network Transmission");

	PWMManager pwm("PWM Manager", PWM_MANAGER_TASK_RATE);
	se3910RPiHCSR04::DistanceSensor *ds = new se3910RPiHCSR04::DistanceSensor(22, 27, "Distance Sensor", DISTANCE_SENSOR_TASK_RATE);

	DiagnosticManager dm(diagQueue, &nt, ds, "Diagnostic Manager", DIAGNOSTIC_TASK_MANAGER_TASK_RATE);
	CollisionSensingRobotController mc(driveQueue, pwm,  DL, DR, "Col. Sensing Ctrl");
	NavigationUnit navUnit(driveQueue, navigationCommandQueue, "Nav Unit", NAV_TASK_RATE);
	
		// Instantiate a camera.
	Camera* myCamera = new Camera(cw, ch, "Camera");

	// Figure out the port to use.
	ImageTransmitter* it = new ImageTransmitter(argv[1], port, lpudp);
	
	ImageCapturer *is = new ImageCapturer(myCamera, it, tw, th, "Image Stream", (IMAGE_STREAM_TASK_RATE));


	// Start each of the two threads up.
	nm.start(NETWORK_RECEPTION_TASK_PRIORITY);
	nt.start(DIAGNOSTIC_TASK_MANAGER_TASK_PRIORITY-1);

	mc.start(COLLISION_SENSING_ROBOT_CONTROLLER_PRIORITY);
	pwm.start(PWM_MANAGER_TASK_PRIORITY);
	ds->start(DISTANCE_SENSOR_TASK_PRIORITY);
	dm.start(DIAGNOSTIC_TASK_MANAGER_TASK_PRIORITY);
	navUnit.start(NAV_TASK_PRIORITY);
	
	myCamera->start(CAMERA_TASK_PRIORITY);
	is->start(IMAGE_STREAM_TASK_PRIORITY);
	
	string msg;
	cin >> msg;

	while (msg.compare("QUIT")!=0)
	{
		cout << "Looping thread id is : " << syscall(SYS_gettid) << "\n";
		if (msg.compare("P")==0)
		{
			RunnableClass::printThreads();
		}
		else if (msg.compare("R")==0)
		{
			RunnableClass::resetAllThreadInformation();
		}

		cin >> msg;
	}


	navUnit.stop();
	dm.stop();
	ds->stop();
	pwm.stop();
	mc.stop();
	nt.stop();
	nm.stop();
	myCamera->shutdown();



	// Wait for the threads to die.
	navUnit.waitForShutdown();
	dm.waitForShutdown();
	ds->waitForShutdown();
	pwm.waitForShutdown();
	mc.waitForShutdown();
	nt.waitForShutdown();
	nm.waitForShutdown();

	delete myQueue[0];
	delete myQueue[1];
	delete myQueue[2];
	delete ds;
	delete myCamera;
	delete it;
	delete is;
}



