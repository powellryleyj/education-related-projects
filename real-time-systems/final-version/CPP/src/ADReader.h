/**
 * @file CommandQueue.cpp.cpp
 * @author  Walter Schilling (schilling@msoe.edu)
 * @version 1.0
 *
 * @section LICENSE
 *
 * This code is developed as part of the MSOE SE3910 Real Time Systems course,
 * but can be freely used by others.
 *
 * SE3910 Real Time Systems is a required course for students studying the
 * discipline of software engineering.
 *
 * This Software is provided under the License on an "AS IS" basis and
 * without warranties of any kind concerning the Software, including
 * without limitation merchantability, fitness for a particular purpose,
 * absence of defects or errors, accuracy, and non-infringement of
 * intellectual property rights other than copyright. This disclaimer
 * of warranty is an essential part of the License and a condition for
 * the grant of any rights to this Software.
 *
 * @section DESCRIPTION
 * Interface for the  AD Reader.
 * This interface defines the interface to work with with an A/D reader.  The chip on the Alphabot 2 is the TLC1543: 10-bit AD acquisition chip.
 */

#ifndef ADREADER_H_
#define ADREADER_H_

#include "PeriodicTask.h"
#include "GPIO.h"
#include "PeriodicTask.h"
#include <string>
#include <mutex>

/**
 * This macro defines the channels that are to be read and tested.
 */
#define NUMBER_OF_AD_CHANNELS (11)

using namespace se3910RPi;
class ADReader: public PeriodicTask {
private:
	/**
	 * This array will hold the current data values for each of the 11 channels.
	 */
	uint16_t dataValues[NUMBER_OF_AD_CHANNELS] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0 };
	/**
	 * This array will hold the max readings for each of the 11 channels.
	 */
	uint16_t maxDataValue[NUMBER_OF_AD_CHANNELS] = { 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0 };
	/**
	 * This array will hold the minimum values for each of the 11 channels.
	 */
	uint16_t minDataValue[NUMBER_OF_AD_CHANNELS] = { 0xFFFF, 0xFFFF, 0xFFFF,
			0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF };

	/**
	 * This is a pointer to the chip select GPIO pin.
	 */
	GPIO *csIOPin;
	/**
	 * This is a pointer to the address pin.
	 */
	GPIO *addressIOPin;
	/**
	 * This is a pointer to the clock pin GPIO instance.
	 */
	GPIO *clockPin;
	/**
	 * This is a pointer to the GPIO Data pin.
	 */
	GPIO *doutPin;

	/**
	 * This mutex prevents simultaneous access from multiple threads.
	 */
	std::mutex dataMutex;

	/**
	 * This method will read the data from all 11 A/D channels.  The current values and max and min values will be updated.  AFter all 11 channels have been
	 * read, the code will exit.
	 */
	void readSensors();

public:
	/**
	 * This is the constructor for the A/D converter code.  It will instantiate a new object to read this material.
	 * @param csPinNumber This is the GPIO pinb number for the chip select line.
	 * @param clockPinNumber This is the GPIO pin number used for the clock.
	 * @param addressPinNumber This is the GPIO pin number for the address line.
	 * @param dinPinNumber This is the GPIO pin number used for reading in data.
	 * @param threadName This is the name of the thread that will be running.
	 * @param period  This is the period for the periodic task.
	 */
	ADReader(int csPinNumber, int clockPinNumber, int addressPinNumber,
			int dinPinNumber, std::string threadName, uint32_t period);
	/**
	 * This is the deconstructor.  It will simply remove all allocated objects.
	 */
	virtual ~ADReader();

	/**
	 * This method will read the a/d value from one of the channels.  The method is safe for multithreading.
	 * @param channel This is the channel number that is to be read, between 0 and NUMBER_OF_AD_CHANNELS (exclusive).
	 * @return The return will be the raw value between 0 and 1023 for the given channel.  It will return 0 if the value is out of range.
	 */
	uint16_t readChannelValue(unsigned int channel);

	/**
	 * This method will read the max a/d value from one of the channels.  The method is safe for multithreading.
	 * @param channel This is the channel number that is to be read, between 0 and NUMBER_OF_AD_CHANNELS (exclusive).
	 * @return The return will be the raw value between 0 and 1023 for the given channel.  It will return 0 if the value is out of range.
	 */
	uint16_t readChannelMax(unsigned int channel);

	/**
	 * This method will read the min a/d value from one of the channels.  The method is safe for multithreading.
	 * @param channel This is the channel number that is to be read, between 0 and NUMBER_OF_AD_CHANNELS (exclusive).
	 * @return The return will be the raw value between 0 and 1023 for the given channel.  It will return 0 if the value is out of range.
	 */
	uint16_t readChannelMin(unsigned int channel);

	/**
	 * This is the task method.  The task method will be invoked periodically every taskPeriod
	 * units of time.
	 */
	virtual void taskMethod();
};

#endif /* ADREADER_H_ */
