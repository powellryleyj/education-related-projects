/**
 * @file NNavigationUnit.h
 * @author  Walter Schilling (schilling@msoe.edu)
 * @version 1.0
 *
 * @section LICENSE
 *
 * This code is developed as part of the MSOE SE3910 Real Time Systems course,
 * but can be freely used by others.
 *
 * SE3910 Real Time Systems is a required course for students studying the
 * discipline of software engineering.
 *
 * This Software is provided under the License on an "AS IS" basis and
 * without warranties of any kind concerning the Software, including
 * without limitation merchantability, fitness for a particular purpose,
 * absence of defects or errors, accuracy, and non-infringement of
 * intellectual property rights other than copyright. This disclaimer
 * of warranty is an essential part of the License and a condition for
 * the grant of any rights to this Software.
 *
 * @section DESCRIPTION
 *      This class is a navigation controller for the robot.  It is responsible for setting the robot up to follow lines on the course.
 */

#ifndef NAVIGATIONUNIT_H_
#define NAVIGATIONUNIT_H_

#include "ADReader.h"
#include "CommandQueue.h"
#include "PIDController.h"

class NavigationUnit: public PeriodicTask {

private:
	/**
	 * This is the PID controller that is present to help assist the navigation controller.
	 */
	PIDController *myPIDCtrl;

	/**
	 * This is a pointer to the queue used to send drive commands.
	 */
	CommandQueue *driveQueue;
	/**
	 * This is the queue that will be sued to receive instructions from the network manager.
	 */
	CommandQueue *instructionQueue;

	/**
	 * This is a pointer to the AD Reader that will be sued to obtain information about the line state.
	 */
	ADReader* dataReader;

	/**
	 * These 5 values set the dividing thresholds for white and black values.  Numbers above this are considered to be black, while numbers below are considered to be white.
	 */
	uint16_t thresholds[5] = { 512, 512, 512, 512, 512 };
	/**
	 * These 5 values represent the calibrated readings when the sensors have been read 25 times for white segments of the track.
	 */
	uint16_t whites[5] = { 1023, 1023, 1023, 1023, 1023 };
	/**
	 * These 5 values represent the calibrated readings when the sensors have read 25 times over a black area of the track.
	 */
	uint16_t blacks[5] = { 0, 0, 0, 0, 0 };
	/**
	 * This variable will determine whether the robot is actively following a line or not.
	 */
	bool lineFollowingActive = false;

	/**
	 * This method will calibrate the sensors when over a white area of the track.  The method will:
	 * 	Read each channel 25 times and store the values into 5 temporary arrays, sleeping 100ms between reads.
	 * 	Sort the values into ascending order
	 * 	Select the median value for each of the 5 channels.
	 * 	Update the thresholds
	 */
	void calibrateWhite();
	/**
	 * This method will calibrate the sensors when over a black area of the track.  The method will:
	 * 	Read each channel 25 times and store the values into 5 temporary arrays, sleeping 100ms between reads.
	 * 	Sort the values into ascending order
	 * 	Select the median value for each of the 5 channels.
	 * 	Update the thresholds
	 */

	void calibrateBlack();
	/**
	 * This method will calculate the threshold between white and black segments on the board.  It will do this by:
	 * Calculating the difference between the median black and white value previously stored for each of the 5 channels.
	 * Divide that difference by 3.
	 * Adding that difference to the median black threshold.
	 * Print out the black, white, and threshold values for each sensor.
	 */
	void setThresholds();

/**
 * This method will be invoked when there is an update to the PID calibration for one of the 3 PID controller configurable parameters.
 * @param parameter This is the parameter.  This PID parameter is sent as well as a tag that determines which parameter it is.  The parameter
 * is sent as an integer whereby the configuration parameter is multiplied by 1000 and then has 1000 added to it.  This allows values between
 * +/-1000.000 to be sent simply as an integer.
 *
 */
	void handlePIDCalibrationUpdate(uint32_t parameter);

public:
	/**
	 * This method will instantiate a new instance of a navigation unit.
	 * @param driveCommandQueue This is a pointer to the command queue that is used to send driving commands.
	 * @param instructionsQueue This is the queue from which the Nav unit will receive commands from the network.
	 * @param threadName This is the name of the given thread.
	 * @param taskRate This is the rate at which the task is to execute, given in us.
	 */
	NavigationUnit(CommandQueue *driveCommandQueue,
			CommandQueue *instructionsQueue, std::string threadName, uint32_t taskRate);
	/**
	 * This is the destructor which cleans up.
	 */
	virtual ~NavigationUnit();


	/**
	 * This is the task method for the class.  It contains the code that is to run periodically on the given thread.  This method will:
	 * 1. Determine if there is anything on the queue that needs to be handled.  (Note: This is a check, not a block like other classes have.)
	 * 1a. If there is something, handle it in the following manner:
	 * 1aa. If it is a white calibration event, stop the robot motors and start a white calibration.
	 * 1ab. If it is a black calibration event, stop the motors and start a black calibration.
	 * 1ac. If it is a line following start command, enqueue for the motors to go forward and set line following to be active.
	 * 1ad. If it is a command to stop line following, stop the robot from any motion and disable line following.
	 * 2. If line following is active,
	 * 2a. Read the 5 sensors, determining whether each one is black or white.  If it is black, add a value of the sensor number - 2 to the sum.  Ideally, if the black line is in the center, the sum will be 0.
	 * 2b. Based on the findings, either spin the robot left or right, turn the robot left or right, or make the robot go straight.
	 */
	void taskMethod();

	/**
	 * This method will stop the execution of the given class.
	 */
	virtual void stop() ;

	/**
	 * This method will block waiting for the given thread to terminate before continuing.  This should be overridden if any child class
	 * has it's own runnable objects encapsulated within it.
	 *
	 */
	virtual void waitForShutdown();

	/**
	 * This method will start up any runnable objects which are contained within a class that implements the RUnnable interface.
	 * If there are no other objects that are runnable, there is no need to override this method.  However, if a child class
	 * contains an instance of a runnable object, then this method must be overridden in the derived class.
	 *
	 */
	virtual void startChildRunnables();

};

#endif /* NAVIGATIONUNIT_H_ */
