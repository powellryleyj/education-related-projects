/*
 * TaskRates.h
 *
 * @section LICENSE
 *
 * This code is developed as part of the MSOE SE3910 Real Time Systems course,
 * but can be freely used by others.
 *
 * SE3910 Real Time Systems is a required course for students studying the
 * discipline of software engineering.
 *
 * This Software is provided under the License on an "AS IS" basis and
 * without warranties of any kind concerning the Software, including
 * without limitation merchantability, fitness for a particular purpose,
 * absence of defects or errors, accuracy, and non-infringement of
 * intellectual property rights other than copyright. This disclaimer
 * of warranty is an essential part of the License and a condition for
 * the grant of any rights to this Software.
 *
 * @section DESCRIPTION
 * This file defines the task rates for all periodic tasks within the system.
 */

#ifndef TASKRATES_H_
#define TASKRATES_H_

/**
 * This macro defines the task rate for the PWM manager in microseconds.  The periodic task will execute every n microseconds, where n is defined here.
 */
#define PWM_MANAGER_TASK_RATE (100)
#define PWM_MANAGER_TASK_PRIORITY (2)

/**
 * This variable defines the task rate for the distance sensor.
 */
#define DISTANCE_SENSOR_TASK_RATE (125000)
#define DISTANCE_SENSOR_TASK_PRIORITY (2)


/**
 * This defines the task rate for the diagnostic task manager.  This controls how often status updates are sent out.
 */
#define DIAGNOSTIC_TASK_MANAGER_TASK_RATE (500000)
#define DIAGNOSTIC_TASK_MANAGER_TASK_PRIORITY (2)

#define IMAGE_STREAM_TASK_RATE ((1000000/fps))
#define IMAGE_STREAM_TASK_PRIORITY (2)

#define CAMERA_TASK_PRIORITY (2)

/**
 * This is the task rate for the IR collision sensors.
 */
#define COLLISION_SENSING_TASK_RATE (50000)
#define COLLISION_SENSING_TASK_PRIORITY (2)

#define COLLISION_SENSING_ROBOT_CONTROLLER_PRIORITY (2)
/**
 * This is the task rate for the horn controller.
 */
#define HORN_TASK_RATE (110000)
#define HORN_TASK_PRIORITY (2)
/**
 * This is the task rate for the A/D controller.
 */
#define AD_TASK_RATE (20000)
#define AD_TASK_PRIORITY (2)

/**
 * This is the task rate for the nav unit.
 */
#define NAV_TASK_RATE (40000)
#define NAV_TASK_PRIORITY (2)

#define NETWORK_RECEPTION_TASK_PRIORITY (2)


#endif /* TASKRATES_H_ */
