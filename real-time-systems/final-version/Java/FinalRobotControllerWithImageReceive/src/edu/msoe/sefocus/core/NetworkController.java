package edu.msoe.sefocus.core;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class NetworkController implements iNetworkController {

	private Socket socket;
	private String ip;
	private DataOutputStream out;
	private DataInputStream in;
	private boolean connected = false;
	
	

	public NetworkController() {
		super();
	}

	@Override
	public void setIPAddress(String ip) {
		this.ip = ip;
	}

	@Override
	public void connect() throws UnknownHostException, IOException {
		socket = new Socket(ip, 9090);
		out = new DataOutputStream(socket.getOutputStream());
		in = new DataInputStream(socket.getInputStream());
		connected = true;
	}

	@Override
	public void disconnect() {
		try {
			out.write(0xFFFFFFFF);
			out.write(0x00000000);
			out.write(0xFFFFFFFF);

			socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		connected = false;
		out = null;
	}

	@Override
	public boolean getConnectionStatus() {
		return connected;
	}

	@Override
	public void sendMessage(int destination, int message) throws IOException {
		if (out != null) {
			int checksum = message ^ destination;
			System.out.printf("Sent: %x %x %x\n", destination, message, checksum);

			out.writeInt(destination);
			out.writeInt(message);
			out.writeInt(checksum);

			out.flush();
		}
	}

	@Override
	public int[] receiveMessage()  throws IOException {
		int retVal[] = {0,0};
		if (in != null)
		{
			boolean valid = false;

			int valuesRead[]={0,0,0};

			while (!valid) {
				// Move the values up by 1 location in the array.
				valuesRead[0] = valuesRead[1];
				valuesRead[1] = valuesRead[2];

				// Read in an int.
				valuesRead[2] = in.readInt();
				// Verify the checksum and set return values if correct.
				if (valuesRead[2] == (valuesRead[1] ^ valuesRead[0])) {
					retVal[0] = valuesRead[0];
					retVal[1] = valuesRead[1];
					valid = true;
				}
			}
		}
		return retVal;
	}
}
