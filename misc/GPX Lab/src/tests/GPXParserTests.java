package tests;

import edu.msoe.gps.GPXParserEventHandler;
import edu.msoe.gps.parser.Parser;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class GPXParserTests {

    private Parser parser;
    private GPXParserEventHandler handler;

    @Before
    public void setup() throws Exception {
        handler = new GPXParserEventHandler();
        parser = new Parser(handler);
    }

    @Test
    public void testCorrectPointCountAndTrackName() throws Exception {
        parser.parse("gpx_files/kickapoo-2014.gpx");

        assertEquals(2415, handler.getPoints().size());
        assertEquals("2014 Kickapoo Dam Challenge", handler.getTrackName());
    }

    @Test
    public void testIncorrectGPXElement() {
        try {
            parser.parse("gpx_files/invalid/incorrect_gpx_element.gpx");

            fail();
        } catch (Exception e) {
            assertEquals("Expected the 'gpx' tag to be the root element", e.getMessage());
        }
    }

    @Test
    public void testIncorrectTrackElement() {
        try {
            parser.parse("gpx_files/invalid/incorrect_track_element.gpx");

            fail();
        } catch (Exception e) {
            assertEquals("Expected the 'trk' tag to be inside a gpx element", e.getMessage());
        }
    }

    @Test
    public void testIncorrectNameElement() {
        try {
            parser.parse("gpx_files/invalid/incorrect_name_element.gpx");

            fail();
        } catch (Exception e) {
            assertEquals("Expected the 'name' tag to be inside a track element", e.getMessage());
        }
    }

    @Test
    public void testIncorrectTrackSegmentElement() {
        try {
            parser.parse("gpx_files/invalid/incorrect_track_seg_element.gpx");

            fail();
        } catch (Exception e) {
            assertEquals("Expected the 'trkseg' tag to be inside a track element", e.getMessage());
        }
    }

    @Test
    public void testIncorrectPointElement() {
        try {
            parser.parse("gpx_files/invalid/incorrect_point_element.gpx");

            fail();
        } catch (Exception e) {
            assertEquals("Expected the 'trkpt' tag to be inside a track segment element", e.getMessage());
        }
    }

    @Test
    public void testIncorrectPointElevationElement() {
        try {
            parser.parse("gpx_files/invalid/incorrect_point_elevation_element.gpx");

            fail();
        } catch (Exception e) {
            assertEquals("Expected the 'ele' tag to be inside a point element", e.getMessage());
        }
    }

    @Test
    public void testIncorrectPointTimeElement() {
        try {
            parser.parse("gpx_files/invalid/incorrect_point_time_element.gpx");

            fail();
        } catch (Exception e) {
            assertEquals("Expected the 'time' tag to be inside a point element", e.getMessage());
        }
    }
}
