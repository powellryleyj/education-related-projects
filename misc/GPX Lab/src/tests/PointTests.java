package tests;

import edu.msoe.gps.Point;
import org.junit.Test;

import static org.junit.Assert.*;

public class PointTests {

    private static final String TIME = "2014-10-04T14:19:59.000Z";

    private static final double DELTA = 1.0E-10;

    @Test
    public void testAllValid() {
        double latitude = 1;
        double longitude = 2;
        double elevation = 3;

        Point point = new Point(latitude, longitude, elevation, TIME);

        assertEquals(latitude, point.getLatitude(), DELTA);
        assertEquals(longitude, point.getLongitude(), DELTA);
        assertEquals(elevation, point.getElevation(), DELTA);
        assertEquals(1412432399000L, point.getTime().getTime());
    }

    @Test
    public void testTooLowLatitude() {
        try {
            new Point(-100, 0, 0, TIME);

            fail();
        } catch (Exception e) {
            assertEquals("Latitude has to be between -90 and 90 degrees", e.getMessage());
        }
    }

    @Test
    public void testTooHighLatitude() {
        try {
            new Point(100, 0, 0, TIME);

            fail();
        } catch (Exception e) {
            assertEquals("Latitude has to be between -90 and 90 degrees", e.getMessage());
        }
    }

    @Test
    public void testTooLowLongitude() {
        try {
            new Point(0, -190, 0, TIME);

            fail();
        } catch (Exception e) {
            assertEquals("Longitude has to be between -180 and 180 degrees", e.getMessage());
        }
    }

    @Test
    public void testTooHighLongitude() {
        try {
            new Point(0, 190, 0, TIME);

            fail();
        } catch (Exception e) {
            assertEquals("Longitude has to be between -180 and 180 degrees", e.getMessage());
        }
    }

    @Test
    public void testInvalidDateTime() {
        try {
            new Point(0, 0, 0, "invalid-date");

            fail();
        } catch (Exception e) {
            assertEquals("Date has to be in the following format: YYYY-MM-DDTHH:MM:SS.XXXZ", e.getMessage());
        }
    }
}
