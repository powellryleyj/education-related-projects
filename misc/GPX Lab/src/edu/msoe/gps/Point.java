package edu.msoe.gps;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * This class is to set up the point's latitude, longitude, elevation
 * @author Jingli Wei
 */
public class Point {
    private double latitude;
    private double longitude;
    private double elevation;

    private Date time;

    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
    /**
    * throw exception for if any of the following are invalid: latitude, longitude, elevation and time
    */
    public Point(double latitude, double longitude, double elevation, String time) {
        if (!isLatitudeValid(latitude)) {
            throw new IllegalArgumentException("Latitude has to be between -90 and 90 degrees");
        }

        if (!isLongitudeValid(longitude)) {
            throw new IllegalArgumentException("Longitude has to be between -180 and 180 degrees");
        }

        try
        {
            this.time = DATE_FORMAT.parse(time);
        } catch (ParseException e)
        {
            throw new IllegalArgumentException("Date has to be in the following format: YYYY-MM-DDTHH:MM:SS.XXXZ");
        }

        this.latitude = latitude;
        this.longitude = longitude;
        this.elevation = elevation;
    }

    /**
    * get latitude
    */
    public double getLatitude() {
        return latitude;
    }

    /**
    * get longitude
    */
    public double getLongitude() {
        return longitude;
    }

    /**
    * get elevation
    */
    public double getElevation() {
        return elevation;
    }

    /**
    * get time
    */
    public Date getTime() {
        return time;
    }

   /**
    * check whether is the latitude valid or not
    */
    private boolean isLatitudeValid(double latitude) {
        return latitude >= -90 && latitude <= 90;
    }

    /**
     * check whether is the longitude valid or not
     */
    public boolean isLongitudeValid(double longitude) {
        return longitude >= -180 && longitude <= 180;
    }

    /**
     * compare elevation
     */
    public int compareElevation(Point other){
        return new Double(getElevation()).compareTo(other.getElevation());
    }

    /**
     * compare latitude
     */
    public int compareLatitude(Point other){
        return new Double(getLatitude()).compareTo(other.getLatitude());
    }

    /**
     * compare longitude
     */
    public int compareLongitude(Point other){
        return new Double(getLongitude()).compareTo(other.getLongitude());
    }
}
