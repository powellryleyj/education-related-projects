package edu.msoe.gps;

import edu.msoe.gps.parser.Parser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * utilizes the XML parser to read in a file which then creates a list of points stored as a Track.
 * 
 * @author powellr
 * @version 1.0
 */
public class Track {
    private File file;

    private Parser parser;
    private GPXParserEventHandler handler;

    private String trackName;
    private List<Point> points;

    private static final double EARTH_RADIUS_IN_MILES = 3963.1676;
    private static final double EARTH_RADIUS_IN_KILOMETERS = 6378.1;

    public Track(File file) throws Exception {
        this.file = file;

        handler = new GPXParserEventHandler();
        parser = new Parser(handler);

        points = new ArrayList<>();
    }

    /**
     * Loads the GPX file and parses it
     */
    public void load() throws Exception {
        parser.parse(file.getAbsolutePath());

        trackName = handler.getTrackName();
        points = handler.getPoints();
    }

    public String getTrackName() {
        return trackName;
    }

    /**
     * compares all elevations of points contained within the Track
     * @return the maximum elevation value
     */
    public double getMaxElevation() {
        return points.stream().max(Point::compareElevation).get().getElevation();
    }

    /**
     * compares all latitudes of points contained within the Track
     * @return the maximum latitude value
     */
    public double getMaxLatitude() {
        return points.stream().max(Point::compareLatitude).get().getLatitude();
    }

    /**
     * compares all longitudes of points contained within the Track
     * @return the maximum longitude value
     */
    public double getMaxLongitude() {
        return points.stream().max(Point::compareLongitude).get().getLongitude();
    }

    /**
     * compares all elevations of points contained within the Track
     * @return the minimum elevation value
     */
    public double getMinElevtion() {
        return points.stream().min(Point::compareElevation).get().getElevation();
    }

    /**
     * compares all latitudes of points contained within the Track
     * @return the minimum latitude value
     */
    public double getMinLatitude() {
        return points.stream().min(Point::compareLatitude).get().getLatitude();
    }

    /**
     * compares all longitude of points contained within the Track
     * @return the minimum longitude value
     */
    public double getMinLongitude() {
        return points.stream().min(Point::compareLongitude).get().getLongitude();
    }

    /**
     * finds the average speed between each GPX point by utilizing the Haversine Formula
     * @param useMiles true indicates miles per hour; false indicates kilometers per hour
     * @return the average speed between GPX points
     */
    public double getAverageSpeed(boolean useMiles) {
        double averageSpeed = 0;
        int counter = 0;

        for (int i = 0; i < points.size() - 1; i++) {
            double time = timeBetween(points.get(i), points.get(i + 1));

            if (time != 0) {
                averageSpeed += haversineFormula(points.get(i), points.get(i + 1), useMiles) / time;
            } else {
                counter++;
            }
        }

        return averageSpeed / (double) (points.size() - counter) * 3600;
    }

    /**
     * finds the maximum speed between two GPX points by utilizing the Haversine Formula
     * @param useMiles true indicates mile per hour; false indicates kilometers per hour
     * @return the maximum speed between two points
     */
    public double getMaxSpeed(boolean useMiles) {
        List<Double> speeds = new ArrayList<>();

        for (int i = 0; i < points.size() - 1; i++) {
            double time = timeBetween(points.get(i), points.get(i + 1));

            if (time != 0) {
                speeds.add(haversineFormula(points.get(i), points.get(i + 1), useMiles) / time);
            }
        }

        Optional<Double> maxSpeed = speeds.stream().max(Double::compareTo);

        if (!maxSpeed.isPresent()) {
            return 0;
        }

        return maxSpeed.get() * 3600;
    }

    /**
     * finds the total distance between all the GPX points by utilizing the Haversine Formula
     * @param useMiles true indicates miles per hour; false indicates kilometers per hour
     * @return the total distance
     */
    public double getTotalDistance(boolean useMiles) {
        double totalDistance = 0;

        for (int i = 0; i < points.size() - 1; i++) {
            totalDistance += haversineFormula(points.get(i), points.get(i + 1), useMiles);
        }

        return totalDistance;
    }

    public String toString() {
        return trackName;
    }
    
    public int sizeOfTrack(){
    	return points.size();
    }

    /**
     * calculates distance between two points consisting of longitude and latitude
     * @param pointOne the first point
     * @param pointTwo the second point
     * @param useMiles true indicates miles per hour; false indicates kilometers per hour
     * @return the distance between the two points examined
     */
    private double haversineFormula(Point pointOne, Point pointTwo, boolean useMiles) {
        double lat1 = Math.toRadians(pointOne.getLatitude());
        double long1 = Math.toRadians(pointOne.getLongitude());

        double lat2 = Math.toRadians(pointTwo.getLatitude());
        double long2 = Math.toRadians(pointTwo.getLongitude());

        double latDistance = lat2 - lat1;
        double longDistance = long2 - long1;

        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(lat1) * Math.cos(lat2) * Math.sin(longDistance / 2) * Math.sin(longDistance / 2);
        double c = 2 * Math.asin(Math.min(1, Math.sqrt(a)));

        return c * (useMiles ? EARTH_RADIUS_IN_MILES : EARTH_RADIUS_IN_KILOMETERS);
    }

    /**
     * finds the time between two GPX points by means of time-stamp
     * @param pointOne the first point
     * @param pointTwo the second point
     * @return the time between in seconds
     */
    private double timeBetween(Point pointOne, Point pointTwo) {
        long timeOne = pointOne.getTime().getTime();
        long timeTwo = pointTwo.getTime().getTime();

        return (timeTwo - timeOne) / 1000;
    }
}
