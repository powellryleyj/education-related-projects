package edu.msoe.gps.parser;

import org.xml.sax.ext.LexicalHandler;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.ErrorHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * DO NOT MODIFY THE CODE IN THIS ABSTRACT CLASS.
 *
 * This is just a "convenience" class that provides default implementations of
 * all of the various interface methods that the GPXParser's event handler needs to
 * implement. The implementations here merely print the events to the console so
 * that you can see them as they are generated.
 *
 * You must implement a concrete class that extends this abstract class, and
 * override the methods so that you can capture the event data of interest to
 * your application.
 *
 * You must pass the reference to your concrete class to the GPXParser constructor.
 *
 * @author hornick
 *
 */
public abstract class AbstractParserEventHandler implements ContentHandler, ErrorHandler, LexicalHandler
{
    // note that the following attributes are inherited by any class
    // that extends this abstract class.
    protected int line; // if errors are discovered, contains the line in the
    // file where it occurred
    protected int column; // if errors are discovered, contains the column
    // number of file where it occurred

    // This method simply logs the event information to the console. By
    // modifying the implementation,
    // it can be made to log to a file instead.
    private void log(String prefix, String data) {
        System.out.println(prefix + ": " + data);
    }

    // Note: for complete Javadoc on the following methods, see the official
    // documentation

    // This method is called the GPXParser is about to throw
    // a SAXException indicating a warning.
    public void warning(SAXParseException exception) throws SAXException
    {
        line = exception.getLineNumber();
        column = exception.getColumnNumber();
        log("*warning event!", "The parser is going to throw a SAXException!");
    }

    // This method is called whenever the GPXParser is about to throw
    // a SAXException indicating an error.
    public void error(SAXParseException exception) throws SAXException {
        line = exception.getLineNumber();
        column = exception.getColumnNumber();
        log("**error event!", "The parser is going to throw a SAXException!");
    }

    // This method is called whenever the GPXParser is about to throw
    // a SAXException indicating a fatal error.
    public void fatalError(SAXParseException exception) throws SAXException {
        line = exception.getLineNumber();
        column = exception.getColumnNumber();
        log("**fatalerror event!", "The parser is going to throw a SAXException!");
    }

    // ContentHandler method
    public void setDocumentLocator(Locator locator) {
        line = locator.getLineNumber();
        column = locator.getColumnNumber();
        // log( "setDocumentLocator", "Line:"+line+", Col:"+column );
    }

    @Override
    // ContentHandler method
    public void startDocument() throws SAXException {
        log("startDocument event!", "start of xml document");
    }

    @Override
    // ContentHandler method
    public void endDocument() throws SAXException {
        log("endDocument event!", "end of xml document");
    }

    @Override
    // This ContentHandler method is called whenever the GPXParser encounters a new
    // tag
    // which may or may not have Attributes. For example,
    // <trkpt lat="43.30627219" lon="-87.98892299">
    // contains two attributes (lat and lon).
    // The code below shows how to access the values of the attributes.
    public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
        String attributes = "none";
        if (atts.getLength() > 0) { // check for element attributes; print them
            // only if they exist.
            attributes = " [";
            for (int index = 0; index < atts.getLength(); index++) {
                attributes += " " + atts.getLocalName(index) + "=" + atts.getValue(atts.getQName(index));
            }
            attributes += "]";
        }
        log("<<startElement event", localName + ", attributes:" + attributes);
    }

    @Override
    // This ContentHandler method is called whenever the GPXParser encounters an
    // END tag (e.g. </someTag>)
    public void endElement(String uri, String localName, String qName) throws SAXException {
        log(">>endElement event", localName);
    }

    @Override
    // This ContentHandler method is called whenever the GPXParser encounters text
    // between tags.
    // For example, for the tag <ele>249.5</ele>, the characters "249.5" are
    // reported.
    public void characters(char[] ch, int start, int length) throws SAXException {
        String chars = "";

        for (int index = start; index < start + length; index++) {
            if (!Character.isWhitespace(ch[index])) // ignore whitespace
                chars += ch[index];
        }

        if (!chars.isEmpty()) // only whitespace was found, and ignored
            log("characters event: " + length + " chars found", chars);
    }

    @Override
    // ContentHandler method
    public void startPrefixMapping(String prefix, String uri) throws SAXException {
        // Null implementation provided; this event can be ignored
        // log("ContentHandler startPrefixMapping", "" );
    }

    @Override
    // ContentHandler method
    public void endPrefixMapping(String prefix) throws SAXException {
        // Null implementation provided; this event can be ignored
        // log("ContentHandler endPrefixMapping", "" );
    }

    @Override
    // ContentHandler method
    public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
        String c = "[";
        for (int index = start; index < start + length; index++)
            c += ch[index];
        c += "]";
        // Null implementation provided; this event can be ignored
        // log("ContentHandler ignorableWhitespace (c, start, length)", c + ","+
        // start + "," + length);
    }

    @Override
    // ContentHandler method
    public void processingInstruction(String target, String data) throws SAXException {
        // Null implementation provided; this event can be ignored
        // log("ContentHandler processingInstruction (target, data)", target +
        // "," + data);
    }

    @Override
    // ContentHandler method
    public void skippedEntity(String name) throws SAXException {
        // Null implementation provided; this event can be ignored
        // log("ContentHandler skippedEntity (name)", name);
    }

    @Override
    // LexicalHandler method
    public void startDTD(String name, String publicId, String systemId) throws SAXException {
        // Null implementation provided; this event can be ignored
        // log("LexicalHandler startDTD(name,publicId, systemId)", name + ","
        // +publicId + "," + systemId );
    }

    @Override
    // LexicalHandler method
    public void endDTD() throws SAXException {
        // Null implementation provided; this event can be ignored
        // log("LexicalHandler endDTD", "" );
    }

    @Override
    // LexicalHandler method
    public void startEntity(String name) throws SAXException {
        // Null implementation provided; this event can be ignored
        // log("LexicalHandler startEntity(name)", name );
    }

    @Override
    // LexicalHandler method
    public void endEntity(String name) throws SAXException {
        // Null implementation provided; this event can be ignored
        // log("LexicalHandler endEntity(name)", name );
    }

    @Override
    // LexicalHandler method
    public void startCDATA() throws SAXException {
        // Null implementation provided; this event can be ignored
        // log("LexicalHandler startCDATA", "" );
    }

    @Override
    // LexicalHandler method
    public void endCDATA() throws SAXException {
        // Null implementation provided; this event can be ignored
        // log("LexicalHandler endCDATA", "" );
    }

    @Override
    // This LexicalHandler method is called whenever the GPXParser encounters a
    // comment <!-- like this -->
    public void comment(char[] ch, int start, int length) throws SAXException {
        String c = "<!--";
        for (int index = start; index < start + length; index++)
            c += ch[index];
        c += ">";
        log("comment event: ", c);
    }

}
