package edu.msoe.gps;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.DecimalFormat;

/**
 * @author Brandon Jackson (jacksonbrant)
 * @version 1.0
 * @created 27-Oct-2016 2:03:26 PM
 */
public class GPSInterface extends JFrame {

    private static final long serialVersionUID = 1L;

    /**
     * OpenListener private class listens for the open file button
     */
    private class OpenListener implements ActionListener {

        /**
         * calls JFileChooser to select a file to open.
         * Then, it calls the updateInterface() method to update labels
         */
        public void actionPerformed(ActionEvent e) {
            try {
                JFileChooser chooser = new JFileChooser();
                File workingDirectory = new File(System.getProperty("user.dir"));

                chooser.setCurrentDirectory(workingDirectory);
                chooser.showOpenDialog(null);

                File openFile = chooser.getSelectedFile();

                if (openFile == null) {
                    return;
                }

                Track track = manager.loadTrack(openFile);

                trackBox.addItem(track);

                updateInterface();

                JOptionPane.showMessageDialog(null, "File was opened successfully", "Loaded GPX File", JOptionPane.INFORMATION_MESSAGE);
            } catch (Exception ex) {
                String message = ex.getMessage();

                if (ex.getCause() != null) {
                    message += "\n\n" + ex.getCause().getMessage();
                }

                JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * ExitListener listens for a JComboBox item to be selected
     */
    private class TrackListener implements ActionListener {

        /**
         * changes the currentTrack to the selected track
         */
        public void actionPerformed(ActionEvent e) {
            currentTrack = (Track) trackBox.getSelectedItem();
            updateInterface();
        }
    }

    public static double FEET_IN_A_METER = 3.28084;
    /**
     * a formatter for numbers with long decimal values
     */
    private DecimalFormat formatter = new DecimalFormat(".##");

    /**
     * a formatter for degree numbers with long decimal values
     */
    private DecimalFormat degreeFormatter = new DecimalFormat(".#######");

    /**
     * the current track selected in the jComboBox
     */
    private Track currentTrack;

    /**
     * the menu bar displayed at the top of the JFrame
     */
    private JMenuBar menuBar;

    /**
     * The menu for the menuBar
     */
    private JMenu menu;

    /**
     * menu item used to open GPX files
     */
    private JMenuItem menuOpen;

    /**
     * Menu item used to close the program
     */
    private JMenuItem menuCloseProgram;

    /**
     * Where the tracks are stored (up to 10 tracks)
     */
    private JComboBox<Track> trackBox;

    /**
     * the left JPanel in the interface
     */
    private JPanel leftPanel;

    /**
     * the right JPanel in the interface
     */
    private JPanel rightPanel;

    /**
     * label for the JComboBox
     */
    private JLabel comboBoxLabel;

    /**
     * label that indicates the track name label
     */
    private JLabel trackNameLabel;

    /**
     * label that indicates the track name
     */
    private JLabel trackName;

    /**
     * label that indicates the minimum latitude label
     */
    private JLabel minLatLabel;

    /**
     * label that indicates the minimum latitude
     */
    private JLabel minLat;

    /**
     * label that indicates the maximum latitude label
     */
    private JLabel maxLatLabel;

    /**
     * label that indicates the maximum latitude
     */
    private JLabel maxLat;

    /**
     * label that indicates the minimum elevation label
     */
    private JLabel minElevationLabel;

    /**
     * label that indicates the minimum elevation
     */
    private JLabel minElevation;

    /**
     * label that indicates the maximum elevation label
     */
    private JLabel maxElevationLabel;

    /**
     * label that indicates the maximum elevation
     */
    private JLabel maxElevation;

    /**
     * label that indicates the distance label
     */
    private JLabel distanceLabel;

    /**
     * label that indicates the distance
     */
    private JLabel distance;

    /**
     * label that indicates the minimum longitude label
     */
    private JLabel minLongLabel;

    /**
     * label that indicates the minimum longitude
     */
    private JLabel minLong;

    /**
     * label that indicates the maximum longitude label
     */
    private JLabel maxLongLabel;

    /**
     * label that indicates the maximum longitude
     */
    private JLabel maxLong;

    /**
     * label that indicates the average speed label
     */
    private JLabel avgSpdLabel;

    /**
     * label that indicates the average speed
     */
    private JLabel avgSpd;

    /**
     * label that indicates the maximum speed label
     */
    private JLabel maxSpdLabel;

    /**
     * label that indicates the maximum speed
     */
    private JLabel maxSpd;

    /**
     * GridBagConstraints for the left panel
     */
    private GridBagConstraints leftC;

    /**
     * GridBagConstraints for the right panel
     */
    private GridBagConstraints rightC;

    /**
     * GridBagConstraints for the JFrame
     */
    private GridBagConstraints frameC;

    /**
     * TrackManager object to get the tracks and their information
     */
    private TrackManager manager = new TrackManager();

    /**
     * the height of the window
     */
    private static final int HEIGHT = 340;

    /**
     * the width of the window
     */
    private static final int WIDTH = 750;

    /**
     * The constructor initializes the GPSInterface.
     * creates action listeners for all buttons
     */
    public GPSInterface() {

        setTitle("GPX Track Viewer");
        setSize(WIDTH, HEIGHT);
        setLayout(new GridBagLayout());
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        ActionListener openListener = new OpenListener();
        ActionListener trackListener = new TrackListener();

        leftPanel = new JPanel();
        rightPanel = new JPanel();
        leftPanel.setLayout(new GridBagLayout());
        rightPanel.setLayout(new GridBagLayout());

        leftC = new GridBagConstraints();
        leftC.gridx = 0;
        leftC.gridy = 0;
        rightC = new GridBagConstraints();
        rightC.gridx = 0;
        rightC.gridy = 0;
        frameC = new GridBagConstraints();
        frameC.anchor = GridBagConstraints.NORTH;
        frameC.ipadx = 20;
        frameC.gridy = 0;
        frameC.gridx = 0;

        frameC.fill = GridBagConstraints.HORIZONTAL;

        menu = new JMenu("File                                                                           " +
                "                                                                                        " +
                "                                                                                        " +
                "                                                                    ");//pad with spaces for menuBar
        menuBar = new JMenuBar();
        menuOpen = new JMenuItem("Open GPX File");
        menuCloseProgram = new JMenuItem("Exit Program");
        menuOpen.addActionListener(openListener);
        menuCloseProgram.addActionListener(e -> System.exit(0));
        menu.add(menuOpen);
        menu.add(menuCloseProgram);
        menuBar.add(menu);
        this.setJMenuBar(menuBar);
        frameC.ipady = 20;

        leftC.ipady = 8;

        leftC.anchor = GridBagConstraints.NORTHEAST;
        rightC.anchor = GridBagConstraints.NORTHEAST;

        //Left Panel
        comboBoxLabel = new JLabel("Select Track: ");
        leftPanel.add(comboBoxLabel, leftC);

        leftC.gridx = 1;

        trackBox = new JComboBox();
        trackBox.addActionListener(trackListener);

        leftPanel.add(trackBox, leftC);
        leftC.ipady = 20;

        leftC.gridy = 1;
        trackName = new JLabel("-");
        leftPanel.add(trackName, leftC);
        leftC.gridx = 0;
        trackNameLabel = new JLabel("Track name: ");
        leftPanel.add(trackNameLabel, leftC);

        leftC.gridy = 2;
        minLatLabel = new JLabel("Minimum Latitude: ");
        leftPanel.add(minLatLabel, leftC);
        leftC.gridx = 1;
        minLat = new JLabel("-");
        leftPanel.add(minLat, leftC);

        leftC.gridy = 3;
        maxLat = new JLabel("-");
        leftPanel.add(maxLat, leftC);
        leftC.gridx = 0;
        maxLatLabel = new JLabel("Maximum Latitude: ");
        leftPanel.add(maxLatLabel, leftC);

        leftC.gridy = 4;
        minElevationLabel = new JLabel("Minimum Elevation: ");
        leftPanel.add(minElevationLabel, leftC);
        leftC.gridx = 1;
        minElevation = new JLabel("-");
        leftPanel.add(minElevation, leftC);

        leftC.gridy = 5;
        maxElevation = new JLabel("-");
        leftPanel.add(maxElevation, leftC);
        leftC.gridx = 0;
        maxElevationLabel = new JLabel("Maximum Elevation: ");
        leftPanel.add(maxElevationLabel, leftC);

        //right panel

        JLabel empty = new JLabel();
        rightC.ipady = 33;
        rightPanel.add(empty, rightC);
        rightC.ipady = 20;

        rightC.gridy = 1;
        distanceLabel = new JLabel("Total Distance Traveled: ");
        rightPanel.add(distanceLabel, rightC);
        rightC.gridx = 1;
        distance = new JLabel("         -");
        rightPanel.add(distance, rightC);

        rightC.gridy = 2;
        minLong = new JLabel("-");
        rightPanel.add(minLong, rightC);
        rightC.gridx = 0;
        minLongLabel = new JLabel("Minimum Longitude: ");
        rightPanel.add(minLongLabel, rightC);

        rightC.gridy = 3;
        maxLongLabel = new JLabel("Maximum Longitude: ");
        rightPanel.add(maxLongLabel, rightC);
        rightC.gridx = 1;
        maxLong = new JLabel("-");
        rightPanel.add(maxLong, rightC);

        rightC.gridy = 4;
        avgSpd = new JLabel("-");
        rightPanel.add(avgSpd, rightC);
        rightC.gridx = 0;
        avgSpdLabel = new JLabel("Average Speed: ");
        rightPanel.add(avgSpdLabel, rightC);

        rightC.gridy = 5;
        maxSpdLabel = new JLabel("Maximum Speed: ");
        rightPanel.add(maxSpdLabel, rightC);
        rightC.gridx = 1;
        maxSpd = new JLabel("-");
        rightPanel.add(maxSpd, rightC);

        add(leftPanel, frameC);
        frameC.gridx = 1;
        add(rightPanel, frameC);

        setLocationRelativeTo(null);
        setVisible(true);
    }

    /**
     * The updateInterface method updates all of the JLabels to display the
     * information about the Track
     */
    private void updateInterface() {

        trackName.setText(currentTrack.getTrackName());

        minLat.setText(degreeFormatter.format(currentTrack.getMinLatitude()) + "\u00b0");
        maxLat.setText(degreeFormatter.format(currentTrack.getMaxLatitude()) + "\u00b0");

        minLong.setText(degreeFormatter.format(currentTrack.getMinLongitude()) + "\u00b0");
        maxLong.setText(degreeFormatter.format(currentTrack.getMaxLongitude()) + "\u00b0");

        minElevation.setText(formatter.format(currentTrack.getMinElevtion()) + " m, "
                + formatter.format(FEET_IN_A_METER * currentTrack.getMinElevtion()) + " ft");
        maxElevation.setText(formatter.format(currentTrack.getMaxElevation()) + " m, "
                + formatter.format(FEET_IN_A_METER * currentTrack.getMaxElevation()) + " ft");

        if(currentTrack.sizeOfTrack() >= 2) {
            avgSpd.setText(formatter.format(currentTrack.getAverageSpeed(false)) + " km/h, "
                    + formatter.format(currentTrack.getAverageSpeed(true)) + " mph");
            maxSpd.setText(formatter.format(currentTrack.getMaxSpeed(false)) + " km/h, "
                    + formatter.format(currentTrack.getMaxSpeed(true)) + " mph");
            distance.setText(formatter.format(currentTrack.getTotalDistance(false)) + " km, "
                    + formatter.format(currentTrack.getTotalDistance(true)) + " miles");
        } else {
            avgSpd.setText("not enough data");
            maxSpd.setText("not enough data");
            distance.setText("not enough data");
        }
    }

    /**
     * Test main for the GPSInterface class
     *
     * @param args
     */
    public static void main(String[] args) {
        new GPSInterface();
    }
}