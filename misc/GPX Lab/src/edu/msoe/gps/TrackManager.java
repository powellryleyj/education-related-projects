package edu.msoe.gps;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Loads new files into the GPX while not exceeding 10 tracks
 *
 * @author powellr
 * @version 1.0
 */
public class TrackManager {

    private List<Track> gpxFiles;

    public TrackManager() {
        gpxFiles = new ArrayList<>(10);
    }

    /**
     * stores and returns the current Track generated from a GPXFile
     * @param gpxFile a file formatted in XML
     * @return the current Track generated from the passed in gpxFile
     * @throws Exception any issue that may arise from improper XML format, or that the gpxFiles list
     * has exceeded 10 entries
     */
    public Track loadTrack(File gpxFile) throws Exception {
        if (gpxFiles.size() < 10) {
            try {
                Track gpx = new Track(gpxFile);
                gpx.load();

                gpxFiles.add(gpx);

                return gpx;
            } catch (Exception e) {
                throw new Exception("Unable to load the specified GPX file", e);
            }
        } else {
            throw new Exception("There are already 10 GPX tracks loaded");
        }
    }
}
