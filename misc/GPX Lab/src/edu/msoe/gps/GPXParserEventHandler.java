package edu.msoe.gps;

import edu.msoe.gps.parser.AbstractParserEventHandler;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;

import java.util.*;
import java.util.stream.Stream;

/**
 * The parser event handler that handles parsing the GPX file
 *
 * @author Connor Christie (christieck)
 */
public class GPXParserEventHandler extends AbstractParserEventHandler {

    private static final String POINT_LATITUDE_ATTR = "lat";
    private static final String POINT_LONGITUDE_ATTR = "lon";

    private Map<Element, Boolean> elements = new HashMap<>();

    private String trackName;

    private String latitude;
    private String longitude;
    private String elevation;
    private String time;

    private List<Point> points;

    /**
     * Gets the parsed track name
     *
     * @return The parsed track name
     */
    public String getTrackName() {
        return trackName;
    }

    /**
     * Gets the parsed points
     *
     * @return The parsed points
     */
    public List<Point> getPoints() {
        return points;
    }

    @Override
    public void startDocument() throws SAXException {
        points = new ArrayList<>();
        elements = new HashMap<>();

        Arrays.stream(Element.values()).forEach(x -> elements.put(x, false));
    }

    @Override
    public void endDocument() throws SAXException {

    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (isElement(localName, Element.GPX))
        {
            // GPX element
            Element element = Element.GPX;

            if (!element.isNestedCorrectly(elements)) {
                throw new SAXNotRecognizedException("Expected the '" + localName + "' tag to be the root element");
            }

            elements.put(element, true);
        } else if (isElement(localName, Element.TRACK))
        {
            // Track element
            Element element = Element.TRACK;

            if (!element.isNestedCorrectly(elements)) {
                throw new SAXNotRecognizedException("Expected the '" + localName + "' tag to be inside a gpx element");
            }

            elements.put(element, true);
        } else if (isElement(localName, Element.TRACK_NAME))
        {
            // Track Name element
            Element element = Element.TRACK_NAME;

            if (!element.isNestedCorrectly(elements)) {
                throw new SAXNotRecognizedException("Expected the '" + localName + "' tag to be inside a track element");
            }

            elements.put(element, true);
        } else if (isElement(localName, Element.TRACK_SEGMENT))
        {
            // Track Segment element
            Element element = Element.TRACK_SEGMENT;

            if (!element.isNestedCorrectly(elements)) {
                throw new SAXNotRecognizedException("Expected the '" + localName + "' tag to be inside a track element");
            }

            elements.put(element, true);
        } else if (isElement(localName, Element.POINT))
        {
            // Point element
            Element element = Element.POINT;

            if (!element.isNestedCorrectly(elements)) {
                throw new SAXNotRecognizedException("Expected the '" + localName + "' tag to be inside a track segment element");
            }

            elements.put(element, true);

            parsePointAttributes(attributes);
        } else if (isElement(localName, Element.POINT_ELEVATION))
        {
            // Point Elevation element
            Element element = Element.POINT_ELEVATION;

            if (!element.isNestedCorrectly(elements)) {
                throw new SAXNotRecognizedException("Expected the '" + localName + "' tag to be inside a point element");
            }

            elements.put(element, true);
        } else if (isElement(localName, Element.POINT_TIME))
        {
            // Point Time element
            Element element = Element.POINT_TIME;

            if (!element.isNestedCorrectly(elements)) {
                throw new SAXNotRecognizedException("Expected the '" + localName + "' tag to be inside a point element");
            }

            elements.put(element, true);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String chars = "";

        for (int index = start; index < start + length; index++) {
            chars += ch[index];
        }

        if (inElement(Element.TRACK_NAME)) {
            trackName = chars;
        } else if (inElement(Element.POINT_ELEVATION)) {
            elevation = chars;
        } else if (inElement(Element.POINT_TIME)) {
            time = chars;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (isElement(localName, Element.GPX))
        {
            elements.put(Element.GPX, false);
        } else if (isElement(localName, Element.TRACK))
        {
            elements.put(Element.TRACK, false);
        } else if (isElement(localName, Element.TRACK_NAME))
        {
            elements.put(Element.TRACK_NAME, false);
        } else if (isElement(localName, Element.TRACK_SEGMENT))
        {
            elements.put(Element.TRACK_SEGMENT, false);
        } else if (isElement(localName, Element.POINT))
        {
            elements.put(Element.POINT, false);

            points.add(constructPoint());
        } else if (isElement(localName, Element.POINT_ELEVATION))
        {
            elements.put(Element.POINT_ELEVATION, false);
        } else if (isElement(localName, Element.POINT_TIME))
        {
            elements.put(Element.POINT_TIME, false);
        }
    }

    /**
     * Constructs a point from the latitude, longitude, elevation and time
     *
     * @return The point
     * @throws SAXException If a number was expected but badly formatted
     */
    private Point constructPoint() throws SAXException {
        try {
            double latitudeValue = Double.parseDouble(latitude);
            double longitudeValue = Double.parseDouble(longitude);
            double elevationValue = Double.parseDouble(elevation);

            return new Point(latitudeValue, longitudeValue, elevationValue, time);
        } catch (NumberFormatException e) {
            throw new SAXException("Latitude, longitude and elevation values all have to be decimal values");
        }
    }

    /**
     * Parses the point's attribute to get the latitude and longitude from it
     *
     * @param attributes The attribute object to parse
     */
    private void parsePointAttributes(Attributes attributes) {
        for (int i = 0; i < attributes.getLength(); i++) {
            String localName = attributes.getLocalName(i);

            if (localName.equalsIgnoreCase(POINT_LATITUDE_ATTR)) {
                latitude = attributes.getValue(attributes.getQName(i));
            } else if (localName.equalsIgnoreCase(POINT_LONGITUDE_ATTR)) {
                longitude = attributes.getValue(attributes.getQName(i));
            }
        }
    }

    private boolean inElement(Element element) {
        // Make sure we are not in any of this element's children elements
        // Check all elements are false that have a parent of this element

        Stream<Element> elementStream = Arrays.stream(Element.values());

        boolean inElement = elements.get(element);
        boolean notInChildren = elementStream.filter(x -> x.getParent() == element).allMatch(x -> !elements.get(x));

        return inElement && notInChildren;
    }

    private boolean isElement(String localName, Element element) {
        return localName.equalsIgnoreCase(element.getName());
    }

    private enum Element {
        GPX("gpx",              null),
        TRACK("trk",            Element.GPX),
        TRACK_NAME("name",      Element.TRACK),
        TRACK_SEGMENT("trkseg", Element.TRACK),
        POINT("trkpt",          Element.TRACK_SEGMENT),
        POINT_ELEVATION("ele",  Element.POINT),
        POINT_TIME("time",      Element.POINT);

        private String name;
        private Element parent;

        Element(String name, Element parent) {
            this.name = name;
            this.parent = parent;
        }

        public String getName() {
            return name;
        }

        public Element getParent() {
            return parent;
        }

        /**
         * Checks if this element is nested correctly in the specified list of elements
         *
         * @param elements The current list of elements
         * @return If the element is nested correctly
         */
        public boolean isNestedCorrectly(Map<Element, Boolean> elements) {
            Map<Element, Boolean> workingElements = new HashMap<>(elements);

            Element currentElement = parent;

            while (currentElement != null) {
                if (!elements.get(currentElement)) {
                    return false;
                }

                workingElements.remove(currentElement);
                currentElement = currentElement.parent;
            }

            return workingElements.entrySet().stream()
                    .allMatch(x -> !x.getValue());
        }
    }
}
