package lab6;

/**
 * Ryley Powell
 * Lab 6 - Commanding Calculators
 */
public class AppendDigitCommand extends CalculatorCommand {

    private char digit;
    private boolean newNum;
    private String display;

    public AppendDigitCommand(Calculator c, char d) {
        super(c);
        this.digit = d;
        this.newNum = c.getNewNumber();
        this.display = c.getDisplay();
    }

    /**
     * calls Calculator.appendDigit()
     */
    public void execute() {
        calculator.appendDigit(digit);
    }

    /**
     * See CalculatorCommand
     */
    public void unexecute() {
        calculator.setState(display, null, null, newNum);
    }
}
