package lab6;

/**
 * Ryley Powell
 * Lab 6 - Commanding Calculators
 */
public class SaveCommand extends CalculatorCommand {

    private String display, accumulator, memory;
    private boolean newNum;

    public SaveCommand(Calculator c) {
        super(c);
        this.display = c.getDisplay();
        this.accumulator = c.getAccumulator();
        this.memory = c.getMemory();
        this.newNum = c.getNewNumber();
    }

    /**
     * calls Calculator.saveToMemory()
     */
    public void execute() {
        calculator.saveToMemory();
    }

    /**
     * See CalculatorCommand
     */
    public void unexecute() {
        calculator.setState(display, memory, accumulator, newNum);
    }
}
