package lab6;

/**
 * Ryley Powell
 * Lab 6 - Commanding Calculators
 */
public class RecallCommand extends CalculatorCommand {

    private String display, memory;
    private boolean newNum;

    public RecallCommand(Calculator c) {
        super(c);
        this.display = c.getDisplay();
        this.memory = c.getMemory();
        this.newNum = c.getNewNumber();
    }

    /**
     * calls Calculator.recallFromMemory()
     */
    public void execute() {
        calculator.recallFromMemory();
    }

    /**
     * See CalculatorCommand
     */
    public void unexecute() {
        calculator.setState(display, memory, null, newNum);
    }
}
