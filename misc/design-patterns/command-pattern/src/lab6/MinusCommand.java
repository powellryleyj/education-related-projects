package lab6;

/**
 * Ryley Powell
 * Lab 6 - Commanding Calculators
 */
public class MinusCommand extends CalculatorCommand {

    private String display, accumulator;
    private boolean newNum;

    public MinusCommand(Calculator c) {
        super(c);
        this.display = c.getDisplay();
        this.accumulator = c.getAccumulator();
        this.newNum = c.getNewNumber();
    }

    /**
     * calls Calculator.minus()
     */
    public void execute() {
        calculator.minus();
    }

    /**
     * See CalculatorCommand
     */
    public void unexecute() {
        calculator.setState(display, null, accumulator, newNum);
    }
}
