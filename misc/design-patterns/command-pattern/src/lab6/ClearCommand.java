package lab6;

/**
 * Ryley Powell
 * Lab 6 - Commanding Calculators
 */
public class ClearCommand extends CalculatorCommand {

    private String display, accumulator, memory;
    private boolean newNum;

    public ClearCommand(Calculator c) {
        super(c);
        this.display = c.getDisplay();
        this.accumulator = c.getAccumulator();
        this.memory = c.getMemory();
        this.newNum = c.getNewNumber();
    }

    /**
     * calls Calculator.clear()
     */
    public void execute() {
        calculator.clear();
    }

    /**
     * See CalculatorCommand
     */
    public void unexecute() {
        calculator.setState(display, memory, accumulator, newNum);
    }
}
