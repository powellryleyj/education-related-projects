package lab6;

/**
 * Ryley Powell
 * Lab 6 - Commanding Calculators
 */
public class EnterCommand extends CalculatorCommand {

    private String display, accumulator;
    private boolean newNum;

    public EnterCommand(Calculator c) {
        super(c);
        this.display = c.getDisplay();
        this.accumulator = c.getAccumulator();
        this.newNum = c.getNewNumber();
    }

    /**
     * calls Calculator.enter()
     */
    public void execute() {
        calculator.enter();
    }

    /**
     * See CalculatorCommand
     */
    public void unexecute() {
        calculator.setState(display, null, accumulator, newNum);
    }
}
