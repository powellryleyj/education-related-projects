package lab6;

import java.math.BigInteger;

/**
 * Mirrors standard three-function calculator (add, subtract, multiply, with memory)
 * All calculations use arbitrary-sized integers. Does not allow entering negative
 * numbers, but negatives are supported.
 * @author R. Hasker
 * @date Feb. 2018
 */
public class Calculator {
    private BigInteger display = BigInteger.ZERO;       // value shown on display
    private BigInteger accumulator = BigInteger.ZERO;   // current computed result
    private BigInteger memory = BigInteger.ZERO;        // stored value
    private boolean newNumber = true; // if true, next digit clears the display for new entry

    public Calculator() { }

    /**
     * Currently computed result
     * @return Result as a string
     */
    public String result() {
        return display.toString();
    }

    // get operations: return strings so clients cannot change state by mistake

    public String getDisplay() { return result(); }

    public String getAccumulator() { return accumulator.toString(); }

    public String getMemory() { return memory.toString(); }

    /**
     * Return state of newNumber; this is intended just for restoring state, so does not
     *    follow the is convention
     * @return whether or not next digit entered will clear the display
     */
    public boolean getNewNumber() { return newNumber; }

    /**
     * Set calculator state using given text; if a field is null or non-numeric then it's not changed.
     * This is intended to be used to set the state in testing and the Command pattern.
     * @param displayText Text to be displayed as current calculator value
     * @param memoryText Text for integer to store in memory
     * @param accumulatorText Text for integer capturing accumulated value
     * @param readyForNewNumber Does next digit added clear the display?
     */
    public void setState(String displayText, String memoryText, String accumulatorText,
                         boolean readyForNewNumber) {
        if (displayText != null && displayText.matches("-?\\d+"))
            display = new BigInteger(displayText);
        if (memoryText != null && memoryText.matches("-?\\d+"))
            memory = new BigInteger(memoryText);
        if (accumulatorText != null && accumulatorText.matches("-?\\d+"))
            accumulator = new BigInteger(accumulatorText);
        newNumber = readyForNewNumber;
    }

    /**
     * Mirrors standard clear-all button on calculator: set display, accumulator,
     * and memory to 0.
     */
    public void clear() {
        display = BigInteger.ZERO;
        accumulator = BigInteger.ZERO;
        memory = BigInteger.ZERO;
        newNumber = true;
    }

    /**
     * If character represents an (ASCII) digit, add that digit to the end of the display.
     * @param c Character to append
     */
    public void appendDigit(char c) {
        if (Character.isDigit(c)) {
            if (newNumber)
                display = BigInteger.ZERO;
            newNumber = false;
            BigInteger biDigit = new BigInteger("" + c);
            display = display.multiply(BigInteger.TEN).add(biDigit);
        }
    }

    /**
     * Enters a number to allow another number to be entered. Only one number
     * can be entered at a time, so a second enter command will clear the first.
     */
    public void enter() {
        accumulator = display;
        newNumber = true;
    }

    /**
     * Perform the add operation and set to accept more numbers (as
     * opposed to adding them at the end of previous numbers).
     */
    public void plus() {
        display = accumulator = accumulator.add(display);
        newNumber = true;
    }

    /**
     * Perform the subtraction operation, where the displayed number is
     * subtracted from the previous result. 3,4- will compute -1.
     * Also sets calculator to accept new numbers.
     */
    public void minus() {
        display = accumulator = accumulator.subtract(display);
        newNumber = true;
    }

    /**
     * Perform the multiplication operation and set up for entering more numbers.
     */
    public void times() {
        display = accumulator = accumulator.multiply(display);
        newNumber = true;
    }

    /**
     * Like enter, but also stores current display in memory.
     */
    public void saveToMemory() {
        memory = display;
        accumulator = display;
        newNumber = true;
    }

    /**
     * Pull saved value from memory, but if user starts typing digits then start using them instead
     */
    public void recallFromMemory() {
        display = memory;
        newNumber = true;
    }

    /**
     * Clear memory by resetting it to 0.
     */
    public void clearMemory() {
        memory = BigInteger.ZERO;
    }
}
