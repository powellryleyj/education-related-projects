package lab6;

/**
 * Ryley Powell
 * Lab 6 - Commanding Calculators
 */
public class PlusCommand extends CalculatorCommand {

    private String display, accumulator;
    private boolean newNum;

    public PlusCommand(Calculator c) {
        super(c);
        this.display = c.getDisplay();
        this.accumulator = c.getAccumulator();
        this.newNum = c.getNewNumber();
    }

    /**
     * calls Calculator.plus()
     */
    public void execute() {
        calculator.plus();
    }

    /**
     * See CalculatorCommand
     */
    public void unexecute() {
        calculator.setState(display, null, accumulator, newNum);

    }
}
