package lab6;

/**
 * Ryley Powell
 * Lab 6 - Commanding Calculators
 */
public class TimesCommand extends CalculatorCommand {

    private String display, accumulator;
    private boolean newNum;

    public TimesCommand(Calculator c) {
        super(c);
        this.display = c.getDisplay();
        this.accumulator = c.getAccumulator();
        this.newNum = c.getNewNumber();
    }

    /**
     * calls Calculator.times()
     */
    public void execute() {
        calculator.times();
    }

    /**
     * See CalculatorCommand
     */
    public void unexecute() {
        calculator.setState(display, null, accumulator, newNum);

    }
}
