import java.io.*;
import java.nio.ByteBuffer;

public class CipherMessage {

    private byte[] message;
    private long size, givenCheckSum, outputCheckSum;

    private Encrypter e;

    public CipherMessage(Encrypter e){
        this.e = e;
       }

    /**
     * reads data from the filepath specified, checking for proper format and validating the checksum, and stores
     * the data; treats the data read as encrypted data
     * @param filePath - the path of the file
     * @throws Exception - throws an exception if an I/O problem occurs, the file is of improper format, or the checksum
     * is not valid
     */
    public void readCipherText(String filePath) throws Exception {
        validateFileFormat(filePath);
        validateChecksum(true);
    }

    /**
     * reads data from the filepath specified, checking for proper format and validating the checksum, and stores
     * the data; treats the data read as non-encrypted data
     * @param filePath - the path of the file
     * @throws Exception - throws an exception if an I/O problem occurs, the file is of improper format, or the checksum
     * is not valid
     */
    public void readClearText(String filePath) throws Exception {
        validateFileFormat(filePath);
        validateChecksum(false);
    }

    /**
     * writes encrypted data to the filepath specified, in the format of size, checksum, and message
     * @param filePath - the path of the file
     * @throws IOException - throws an exception if an I/O problem occurs
     */
    public void writeCipherText(String filePath) throws IOException {
        DataOutputStream dos = new DataOutputStream(new FileOutputStream(filePath));
        dos.write(longToByteArray(size));
        message = e.encrypt(message);
        outputCheckSum = byteArrayToLong(message);
        dos.write(longToByteArray(outputCheckSum));
        dos.write(message);
        dos.flush();
        dos.close();
    }

    /**
     * writes non-encrypted data to the filepath specified, in the format of size, checksum, and message
     * @param filePath - the path of the file
     * @throws IOException - throws an exception if an I/O problem occurs
     */
    public void writeClearText(String filePath) throws  IOException {
        DataOutputStream dos = new DataOutputStream(new FileOutputStream(filePath));
        dos.write(longToByteArray(size));
        dos.write(longToByteArray(outputCheckSum));
        dos.write(message);
        dos.flush();
        dos.close();
    }

    /**
     * private worker method that reads in data for readClearText() and readCipherText(), stores the data, as well checks
     * to ensure proper formatting of the file
     * @param filePath - the path to the file
     * @throws Exception - throws an I/O exception if the file cannot be found, otherwise will throw a general exception
     * indicating the file is of improper format
     */
    private void validateFileFormat(String filePath) throws Exception {
        // format: size checksum message
        int messageCheck;
        DataInputStream dis = new DataInputStream(new FileInputStream(filePath));
        // try/catch block is utilized to customize the message of any I/O exceptions that could possibly be thrown
        // since if the read methods fail, then the file is of improper format
        try {
            size = dis.readLong();
            givenCheckSum = dis.readLong();
            message = new byte[(int)size];
            messageCheck = dis.read(message, 0, (int) size);
        } catch(Exception e) {
            throw new Exception("Illegal input file format");
        }
        // the actual check ensuring the file is in proper format
        if(longToByteArray(size).length != 8
                || longToByteArray(givenCheckSum).length != 8
                || messageCheck != size) {
            throw new Exception("Illegal input file format");
        } else {
            dis.close();
        }
    }

    /**
     * private worker method that compares the given checksum to the checksum computed from the message; this method
     * will also decrypt the message if it is encrypted already such that the outputCheckSum can be computed for output
     * @param encrypted - flag telling this method whether the data should be treated as encrypted or non-encrypted
     * @throws Exception - throws an exception if the given checksum does not match what has been computed from the
     * message body
     */
    private void validateChecksum(boolean encrypted) throws Exception {
        //checksum is the total value of all the bytes within the message body
        //if the provided checksum doesn't match the computed checksum, throw
        // an error and don't write to the output
        long inputCheckSum;
        if(encrypted) {
            inputCheckSum = byteArrayToLong(message);
            message = e.decrypt(message);
            outputCheckSum = byteArrayToLong(message);
        } else {
            inputCheckSum = byteArrayToLong(message);
        }
        if(givenCheckSum != inputCheckSum) {
            throw new Exception("Invalid checksum; got " + inputCheckSum + ", expected " + givenCheckSum);
        }
    }

    /**
     * private worker method that converts a byte array to a long
     * @param b - the byte array to convert
     * @return - the byte array as a long value
     */
    private long byteArrayToLong(byte[] b){
        long ret = 0;
        for(int i = 0; i < b.length; i++) {
            ret += (int) b[i];
        }
        return ret;
    }

    /**
     * private worker method that converts a long to a byte array
     * @param l - the long to convert
     * @return - the long value as a byte array
     */
    private byte[] longToByteArray(long l){
        ByteBuffer bb = ByteBuffer.allocate(Long.BYTES);
        bb.putLong(l);
        return bb.array();
    }
}
