public interface Encrypter {

    public byte[] decrypt(byte[] bytes);

    public byte[] encrypt(byte[] bytes);
}
