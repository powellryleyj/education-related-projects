public class XOREncrypter implements Encrypter {

    private byte[] key;
    private int keyIndex;

    public XOREncrypter(byte[] key){
        this.key = key;
        keyIndex = 0;
    }

    /**
     * decrypts the given byte array by applying the stored key
     * @param bytes - the byte array to decrypt
     * @return - the decrypted byte array
     */
    @Override
    public byte[] decrypt(byte[] bytes) {
        return encrypt(bytes);
    }

    /**
     * encrypts the given byte array by applying the stored key
     * @param bytes - the byte array to encrypt
     * @return - the encrypted byte array
     */
    @Override
    public byte[] encrypt(byte[] bytes) {
        keyIndex = 0;
        byte[] ret = new byte[bytes.length];
        for(int i = 0; i < bytes.length; i++) {
            ret[i] = (byte)(bytes[i] ^ key[keyIndex]);
            keyIndex++;
            if(keyIndex == (key.length)) {
                keyIndex = 0;
            }
        }
        return ret;
    }
}
