import java.util.Scanner;

public class Main {

    /**
     * controls the logic of the program
     * @param args
     */
    public static void main(String args[]){
        Scanner s = new Scanner(System.in);
        System.out.print("Input: ");
        String inputFile = s.next();
        System.out.print("Output: ");
        String outputFile = s.next();
        System.out.print("Encrypt (y/n): ");
        String encrypt = s.next();
        System.out.print("Method: (rev, shift, xor): ");
        String method = s.next();
        CipherMessage cm;
        String key;
        int shiftIndex;
        try {
            switch (method.trim()) {
                case "shift":
                    System.out.print("Shift amount: ");
                    shiftIndex = s.nextInt();
                    cm = new CipherMessage(new ShiftEncrypter(shiftIndex));
                    break;
                case "xor":
                    System.out.print("Key: ");
                    key = s.next();
                    cm = new CipherMessage(new XOREncrypter(key.getBytes()));
                    break;
                case "rev":
                    cm = new CipherMessage(new ReverseEncrypter());
                    break;
                default:
                    throw new Exception("Something bad happened...");
            }

            switch (encrypt.trim()) {
                case "y":
                    cm.readClearText(inputFile.trim());
                    cm.writeCipherText(outputFile.trim());
                    System.out.println("Encrypted "
                            + inputFile + " to " + outputFile + " using " + method);
                    break;
                case "n":
                    cm.readCipherText(inputFile);
                    cm.writeClearText(outputFile);
                    System.out.println("Decrypted "
                            + inputFile + " to " + outputFile + " using " + method);
                    break;
                default:
                    throw new Exception("Something bad happened...");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
