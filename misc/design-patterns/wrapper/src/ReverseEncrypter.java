public class ReverseEncrypter implements Encrypter {

    public ReverseEncrypter(){}

    /**
     * decrypts the given byte array by reversing the byte array
     * @param bytes - the given byte array
     * @return - the decrypted byte array
     */
    @Override
    public byte[] decrypt(byte[] bytes) {
        return encrypt(bytes);
    }

    /**
     * encrypts the given byte array by reversing the byte array
     * @param bytes - the given byte array
     * @return - the encrypted byte array
     */
    @Override
    public byte[] encrypt(byte[] bytes) {
        int index = 0;
        byte[] ret = new byte[bytes.length];
        for(int i = bytes.length - 1; i >=0; i--) {
            ret[index] = bytes[i];
            index++;
        }
        return ret;
    }
}
