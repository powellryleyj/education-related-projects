public class ShiftEncrypter implements Encrypter {

    private int shiftIndex;

    public ShiftEncrypter(int shiftIndex){
        this.shiftIndex = shiftIndex;
    }

    /**
     * decrypts the given byte array by applying the shiftIndex in reverse
     * @param bytes - the byte array to decrypt
     * @return - the decrypted byte array
     */
    @Override
    public byte[] decrypt(byte[] bytes) {
        byte[] ret = new byte[bytes.length];
        int tempByte;
        for(int i = 0; i < bytes.length; i++){
            tempByte = bytes[i];
            ret[i] = (byte)(tempByte - shiftIndex);
        }
        return ret;
    }

    /**
     * encrypts the given byte array by applying the shiftIndex
     * @param bytes - the byte array to encrypted
     * @return - the encrypted byte array
     */
    @Override
    public byte[] encrypt(byte[] bytes) {
        byte[] ret = new byte[bytes.length];
        int tempByte;
        for(int i = 0; i < bytes.length; i++) {
            tempByte = bytes[i];
            ret[i] = (byte)(tempByte + shiftIndex);
        }
        return ret;
    }
}
