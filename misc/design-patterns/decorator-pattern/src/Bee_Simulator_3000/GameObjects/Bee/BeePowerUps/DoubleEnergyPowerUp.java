package Bee_Simulator_3000.GameObjects.Bee.BeePowerUps;

import Bee_Simulator_3000.GameObjects.Bee.Bee;

public class DoubleEnergyPowerUp extends BeePowerUpDecorator {

    //TODO set turns active
    //TODO handle case of attacked by Bee; Attacking Nectar/Venus Flower
    //TODO handle multiple wrappings

    private static final int ENERGY_MULTIPLIER = 2;
    private static final int TURNS_ACTIVE = 5; //number of turns active

    //TODO find/create image
    //private final Image doubledEnergyBee = new Image("Bee_Simulator_3000/GameImages/doubledEnergyBee.png");
    public DoubleEnergyPowerUp(Bee bee) {
        super(bee);
        this.turnsActive = TURNS_ACTIVE;
    }

    private void doubleEnergy() {
        bee.setEnergyModifier(bee.getEnergyModifier() * ENERGY_MULTIPLIER);
    }

    private void halveEnergy() {
        bee.setEnergyModifier(bee.getEnergyModifier() / 2);
    }

    @Override
    public void tearDown() {
        halveEnergy();
    }

    @Override
    public void setup() {
        doubleEnergy();
    }
}
