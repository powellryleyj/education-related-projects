package Bee_Simulator_3000.GameObjects.Bee.BeePowerUps;

import Bee_Simulator_3000.GameObjects.Bee.Bee;
import javafx.scene.image.Image;

public class DoubleSpeedPowerUp extends BeePowerUpDecorator {

    //TODO set turns active
    //TODO handle multiple speed powerups
    //TODO handle column/row skipping in MovementBehavior
    private final static int TURNS_ACTIVE = 5; // active for 5 turns
    private final static int SPEED_MULTIPLIER = 2;

    //TODO find image
    private final Image spedUpBee = new Image("Bee_Simulator_3000/GameImages/beeSpeedUp.png");

    public DoubleSpeedPowerUp(Bee bee) {
        super(bee);
        turnsActive = TURNS_ACTIVE;
    }

    private void doubleSpeed() {
        bee.setSpeedModifier(bee.getSpeedModifier() * SPEED_MULTIPLIER);
    }

    private void halveSpeed() {
        bee.setSpeedModifier(bee.getSpeedModifier() / SPEED_MULTIPLIER);
    }

    @Override
    public void setup() {
        doubleSpeed();
        bee.setImage(spedUpBee);
    }

    @Override
    public void tearDown() {
        halveSpeed();
        bee.setImage(Bee.DEFAULT_BEE_IMAGE);
    }
}
