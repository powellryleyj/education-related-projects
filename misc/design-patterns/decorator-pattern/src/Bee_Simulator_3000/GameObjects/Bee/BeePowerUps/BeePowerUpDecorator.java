package Bee_Simulator_3000.GameObjects.Bee.BeePowerUps;

import Bee_Simulator_3000.GameObjects.Bee.Bee;
import javafx.geometry.Point2D;
import javafx.scene.image.Image;

public abstract class BeePowerUpDecorator extends Bee {

    protected Bee bee;
    protected int turnsActive;

    //TODO get pictures of Bees/wrapped Bees/multiple wrapped Bees

    public BeePowerUpDecorator(Bee bee) {
        this.bee = bee;
        setup();
    }

    /**
     * sets up the "Power Up"
     */
    public abstract void setup();

    /**
     * tears down the "Power Up"
     *
     */
    public abstract void tearDown();

    /**
     * overrides takeTurn from Bee to call on the wrapped Bee object, this will also decrement the turns a powerup is active for
     *
     * @return - the bees new position
     */
    @Override
    public Point2D takeTurn(int beeIndex) {
        Point2D temp = super.takeTurn(beeIndex);
        if (turnsActive == 0) {
            tearDown();
        } else if (turnsActive > 0) {
            turnsActive--;
        } else {
            // do nothing
        }
        return temp;
    }

    public Bee getBee() {
        return this.bee;
    }

    /**
     * overrides getPosition from GameObject to call on the wrapped Bee object
     *
     * @return the position of the wrapped Bee object
     */
    @Override
    public Point2D getPosition() {
        return bee.getPosition();
    }

    /**
     * overrides getRadius from GameObject to call on the wrapped Bee object
     *
     * @return - the radius of the wrapped Bee object
     */
    @Override
    public double getRadius() {
        return bee.getRadius();
    }

    /**
     * overrides setPosition from GameObject to call on the wrapped Bee object
     *
     * @param point2D - the new position
     * @return - true if the position was successfully updated; false otherwise
     */
    @Override
    public boolean setPosition(Point2D point2D) {
        return bee.setPosition(point2D);
    }

    /**
     * overrides getEnergy from GameObject to call on the wrapped Bee object
     *
     * @return - the energy level of the wrapped Bee object
     */
    @Override
    public int getEnergy() {
        return bee.getEnergy();
    }

    /**
     * overrides the getEnergy from GameObject to call on the wrapped Bee object
     *
     * @param energy - the energy level to set
     */
    public void setEnergy(int energy) {
        bee.setEnergy(energy);
    }

    /**
     * overrides the getSideLength from GameObject to call on the wrapped Bee object
     *
     * @return - the side length of the wrapped Bee object
     */
    public int getSideLength() {
        return bee.getSideLength();
    }

    /**
     * overrides the getImage from Bee to call on the wrapped Bee object
     *
     * @return - the image representing the wrapped Bee object
     */
    @Override
    public Image getImage() {
        return bee.getImage();
    }

    /**
     * overrides the setImage from Bee to call on the wrapped Bee object
     *
     * @param image - the new image
     */
    @Override
    public void setImage(Image image) {
        this.bee.setImage(image);
    }

    /**
     * overrides the isAlive method from Bee to call on the wrapped Bee object
     *
     * @return - true if the wrapped Bee object is alive; false otherwise
     */
    @Override
    public boolean isAlive() {
        return bee.isAlive();
    }

    /**
     * overrides the checkAlive method from Bee to call on the wrapped Bee object
     *
     * @return - true if the wrapped Bee object is alive; false otherwise
     */
    @Override
    public boolean checkAlive() {
        return bee.checkAlive();
    }
}
