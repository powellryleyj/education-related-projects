package Bee_Simulator_3000.GameObjects.Bee.BeePowerUps;

import Bee_Simulator_3000.GameObjects.Bee.Bee;
import javafx.scene.image.Image;

public class ShieldPowerUp extends BeePowerUpDecorator {

    //TODO set turns active
    //TODO handle attacked by bee or venus bee trap
    //TODO handle multiple wrappings
    private static final int TURNS_ACTIVE = 5; //number of turns active
    private final Image shieldedBee = new Image("Bee_Simulator_3000/GameImages/beeShielded.png");


    public ShieldPowerUp(Bee bee) {
        super(bee);
        this.turnsActive = TURNS_ACTIVE;
    }

    private void shieldBee() {
        bee.setShielded(true);
    }

    private void removeShield() {
        bee.setShielded(false);
    }

    @Override
    public void setup() {
        shieldBee();
        bee.setImage(shieldedBee);
    }

    @Override
    public void tearDown() {
        removeShield();
        bee.setImage(Bee.DEFAULT_BEE_IMAGE);
    }
}
