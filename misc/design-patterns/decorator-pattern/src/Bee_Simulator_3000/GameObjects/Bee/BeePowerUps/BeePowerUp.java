package Bee_Simulator_3000.GameObjects.Bee.BeePowerUps;

import Bee_Simulator_3000.GameBoard;
import Bee_Simulator_3000.GameObjects.GameObject;
import javafx.scene.image.Image;


public class BeePowerUp extends GameObject {


    private static final Image doubleSpeed = new Image("Bee_Simulator_3000/GameImages/double_speed.png");
    private static final Image doubleEnergy = new Image("Bee_Simulator_3000/GameImages/double_energy.png");
    private static final Image shield = new Image("Bee_Simulator_3000/GameImages/shield.png");
    private final static int SIDE_LENGTH = 25;
    private final static double RADIUS = Math.pow(Math.pow(SIDE_LENGTH, 2) + Math.pow
            (SIDE_LENGTH, 2), 0.5) / 2;

    public BeePowerUp() {
        super(SIDE_LENGTH, RADIUS);
        int i = GameBoard.generateRandomInteger(0, 3);
        if (GameBoard.DEBUGGING) {
            System.out.println("Power up type: " + i);
        }
        if (i == 0) {
            this.powerUpType = PowerUpType.DOUBLE_SPEED;
        } else if (i == 1) {
            this.powerUpType = PowerUpType.DOUBLE_ENERGY;
        } else {
            this.powerUpType = PowerUpType.SHIELD;
        }
    }

    public Image getIcon() {
        Image ret;
        if (powerUpType.equals(PowerUpType.DOUBLE_SPEED)) {
            ret = doubleSpeed;
        } else if (powerUpType.equals(PowerUpType.DOUBLE_ENERGY)) {
            ret = doubleEnergy;
        } else if (powerUpType.equals(PowerUpType.SHIELD)) {
            ret = shield;
        } else {
            ret = null;
            if (GameBoard.DEBUGGING) {
                System.out.println("Something is here that shouldn't be. BeePowerUp.getIcon()");
            }
        }
        return ret;
    }
}
