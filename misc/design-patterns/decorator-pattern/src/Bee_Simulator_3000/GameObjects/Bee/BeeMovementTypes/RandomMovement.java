package Bee_Simulator_3000.GameObjects.Bee.BeeMovementTypes;


import Bee_Simulator_3000.GameBoard;
import Bee_Simulator_3000.GameObjects.Bee.Bee;
import javafx.geometry.Point2D;

/**
 * @author Ryley Powell (powellr)
 * @version 2.0
 * @created 14-Dec-2017 3:46:48 PM
 * Edited by: Ryley Powell (powellr)
 */
public class RandomMovement extends MovementBehavior {

	public RandomMovement(Bee b){
       super(b);
	}

    /**
     * determines the next position of the Bee
     * @return - returns the point the bee will move to
     */
	public Point2D move(){
        if (GameBoard.DEBUGGING) {
            System.out.println(" is moving with random movement."); // for debugging purposes
        }
	    double x = b.getPosition().getX(), y = b.getPosition().getY();
	    randomizeDirection();
        if (!checkXBoundary((x + (b.getMovementSpeed() * xDirection)))) {
	        xDirection = flipDirection(xDirection);
            x += (b.getMovementSpeed() * xDirection);
        } else {
            x += (b.getMovementSpeed() * xDirection);
        }

        if (!checkYBoundary((y + (b.getMovementSpeed() * yDirection)))) {
	        yDirection = flipDirection(yDirection);
            y += (b.getMovementSpeed() * yDirection);
        } else {
            y += (b.getMovementSpeed() * yDirection);
        }

        return new Point2D(x, y);
	}

    /**
     * generates a random direction for both x and y when called.
     */
	private void randomizeDirection() {
        double xRand = GameBoard.generateRandomDouble(0, 1);
        double yRand = GameBoard.generateRandomDouble(0, 1);
        if(xRand >= 0.5) {
            xDirection = flipDirection(xDirection);
        }
        if(yRand >= 0.5) {
            yDirection = flipDirection(yDirection);
        }
    }
}