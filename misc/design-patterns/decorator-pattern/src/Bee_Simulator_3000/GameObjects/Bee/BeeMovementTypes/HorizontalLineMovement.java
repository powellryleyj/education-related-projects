package Bee_Simulator_3000.GameObjects.Bee.BeeMovementTypes;

import Bee_Simulator_3000.GameBoard;
import Bee_Simulator_3000.GameObjects.Bee.Bee;
import javafx.geometry.Point2D;

/**
 * @author Ryley Powell (powellr)
 * @version 2.0
 * @created 14-Dec-2017 3:46:48 PM
 * Edited by: Ryley Powell (powellr)
 */
public class HorizontalLineMovement extends MovementBehavior {


    public HorizontalLineMovement(Bee b){
        super(b);
    }

    /**
     * determines the next position of the Bee
     * @return - returns true if the Bee has moved; false otherwise
     */
    public Point2D move() {
        if (GameBoard.DEBUGGING) {
            System.out.println(" is moving with horizontal movement."); // for debugging purposes
        }
        double x = b.getPosition().getX(), y = b.getPosition().getY();
        if (!checkXBoundary((x + (b.getMovementSpeed() * xDirection)))) {
            xDirection = flipDirection(xDirection);
            if(!checkYBoundary(adjustRow())){
                yDirection = flipDirection(yDirection);
                y = adjustRow();
            } else {
                y = adjustRow();
            }
        } else {
            x += (b.getMovementSpeed() * xDirection);
        }
        return new Point2D(x, y);
    }

    /**
     * sends the Bee to the next row with respect to its yDirection
     */
    private double adjustRow() {
        double y = b.getPosition().getY();
        y += (b.getMovementSpeed() * yDirection);
        return y;
    }
}
