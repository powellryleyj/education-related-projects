package Bee_Simulator_3000.GameObjects.Bee.BeeMovementTypes;


import Bee_Simulator_3000.GameBoard;
import Bee_Simulator_3000.GameObjects.Bee.Bee;
import javafx.geometry.Point2D;

/**
 * @author Ryley Powell (powellr)
 * @version 2.0
 * @created 14-Dec-2017 3:46:48 PM
 * Edited by: Ryley Powell (powellr)
 */
public abstract class MovementBehavior {

    protected Bee b;

    protected int xDirection;
    protected int yDirection;

    protected MovementBehavior(Bee b) {
        this.b = b;
        xDirection = randomizeInitialDirection();
        yDirection = randomizeInitialDirection();
    }

    /**
     * determines the next position of the Bee
     * @return - returns the point the bee will move to
     */
	public abstract Point2D move();

    private int randomizeInitialDirection() {
        double decision = GameBoard.generateRandomDouble(0, 1);
        if (decision >= 0.5) {
            return 1;
        }
        return -1;
    }

    /**
     * inverts the direction specified by the parameter axis
     * @param axis - either the x or y axis.
     */
    protected int flipDirection(int axis) {
        axis *= -1;
        return axis;
    }

    /**
     * checks the canvas boundaries set in GameBoard against the xValue passed to this function
     * @param xValue - the xValue to be checked
     * @return - true if and only if the xValue is valid; false otherwise
     */
    protected boolean checkXBoundary(double xValue) {
        boolean ret = false;

        if (xValue >= 0 && xValue <= GameBoard.CANVAS_WIDTH) {
            ret = true;
        }
        return ret;
    }

    /**
     * checks the canvas boundaries set in GameBoard against the yValue passed to this function
     * @param yValue - the yValue to be checked
     * @return - true if and only if the yValue is valid; false otherwise
     */
    protected boolean checkYBoundary(double yValue) {
        boolean ret = false;

        if (yValue >= 0 && yValue <= GameBoard.CANVAS_HEIGHT) {
            ret = true;
        }
        return ret;
    }
}