package Bee_Simulator_3000.GameObjects.Bee.BeeMovementTypes;


import Bee_Simulator_3000.GameBoard;
import Bee_Simulator_3000.GameObjects.Bee.Bee;
import javafx.geometry.Point2D;

/**
 * @author Ryley Powell (powellr)
 * @version 2.0
 * @created 14-Dec-2017 3:46:48 PM
 * Edited by: Ryley Powell (powellr)
 */
public class VerticalLineMovement extends MovementBehavior {

	public VerticalLineMovement(Bee b){
		super(b);
	}

    /**
     * determines the next position of the Bee
     * @return - return the point the bee will move to
     */
	public Point2D move() {
        if (GameBoard.DEBUGGING) {
            System.out.println(" is moving with vertical movement."); // for debugging purposes
        }
        double x = b.getPosition().getX(), y = b.getPosition().getY();
        if (!checkYBoundary((y + (b.getMovementSpeed() * yDirection)))) {
            yDirection = flipDirection(yDirection);
            if(!checkXBoundary(adjustColumn())) {
                xDirection = flipDirection(xDirection);
                x = adjustColumn();
            } else {
                x = adjustColumn();
            }
        } else {
            y += (b.getMovementSpeed() * yDirection);
        }
        return new Point2D(x, y);
    }

    /**
     * sends the Bee to the next column with respect to its xDirection
     */
    private double adjustColumn(){
        double x = b.getPosition().getX();
        x += (b.getMovementSpeed() * xDirection);
        return x;
    }

}