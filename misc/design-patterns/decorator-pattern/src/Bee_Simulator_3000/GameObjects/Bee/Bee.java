package Bee_Simulator_3000.GameObjects.Bee;

import Bee_Simulator_3000.GameBoard;
import Bee_Simulator_3000.GameObjects.Bee.BeeMovementTypes.HorizontalLineMovement;
import Bee_Simulator_3000.GameObjects.Bee.BeeMovementTypes.MovementBehavior;
import Bee_Simulator_3000.GameObjects.Bee.BeeMovementTypes.RandomMovement;
import Bee_Simulator_3000.GameObjects.Bee.BeeMovementTypes.VerticalLineMovement;
import Bee_Simulator_3000.GameObjects.Bee.BeePowerUps.BeePowerUp;
import Bee_Simulator_3000.GameObjects.Bee.BeePowerUps.DoubleEnergyPowerUp;
import Bee_Simulator_3000.GameObjects.Bee.BeePowerUps.DoubleSpeedPowerUp;
import Bee_Simulator_3000.GameObjects.Bee.BeePowerUps.ShieldPowerUp;
import Bee_Simulator_3000.GameObjects.Flower.Flower;
import Bee_Simulator_3000.GameObjects.GameObject;
import javafx.geometry.Point2D;
import javafx.scene.image.Image;

/**
 * @author Ryley Powell (powellr)
 * @version 2.0
 * @created 14-Dec-2017 3:46:48 PM
 * Edited by: Ryley Powell (powellr)
 */
public class Bee extends GameObject {

    public final static Image DEFAULT_BEE_IMAGE = new Image("Bee_Simulator_3000/GameImages/bee.png"); // default bee image
    private boolean alive;
    private boolean onFlower;
    private final static int SIDE_LENGTH = 50;
	private final static double RADIUS = Math.pow(Math.pow(SIDE_LENGTH, 2) + Math.pow
			(SIDE_LENGTH, 2), 0.5)/2;

	private MovementBehavior movementBehavior;
    private GameBoard gameBoard;
    private int movementSpeed = 50; //base movement speed
    private int speedModifier;
    private int energyModifier;
    private boolean shielded;
    private Image beeIcon;
    private boolean poweredUp;

    // won't be used; only in place for BeePowerUpDecorator
    protected Bee() {
    }

    public Bee(GameBoard gameBoard) {
		super(SIDE_LENGTH, RADIUS);
		this.gameBoard = gameBoard;
        setImage(DEFAULT_BEE_IMAGE);
		generateMovementType();
        energy = GameBoard.generateRandomInteger(20, 100);
		alive = true;
        poweredUp = false;
        shielded = false;
        speedModifier = 1;
        energyModifier = 1;
	}

    public int getMovementSpeed() {
        return this.movementSpeed * this.speedModifier;
    }

    public void setMovementSpeed(int movementSpeed) {
        this.movementSpeed = movementSpeed;
    }

    public boolean isShielded() {
        return shielded;
    }

    public void setShielded(boolean shielded) {
        this.shielded = shielded;
    }

    public int getEnergyModifier() {
        return energyModifier;
    }

    public void setEnergyModifier(int energyModifier) {
        this.energyModifier = energyModifier;
    }

    public int getSpeedModifier() {
        return speedModifier;
    }

    public void setSpeedModifier(int speedModifier) {
        this.speedModifier = speedModifier;
    }

    public boolean setPosition(Point2D point2D) {
		return super.setPosition(point2D);
	}

    /**
     * generates a random movement type between StraightLineMovement, Horizontal and Vertical, and RandomMovement.
     */
	private void generateMovementType() {
        double rand = GameBoard.generateRandomDouble(0, 1);
		if(rand >= 0.5) {
            rand = GameBoard.generateRandomDouble(0, 1);
		    if(rand >= 0.5) {
                movementBehavior = new VerticalLineMovement(this);
            } else {
		        movementBehavior = new HorizontalLineMovement(this);
            }
        } else {
		    movementBehavior = new RandomMovement(this);
        }
	}

    /**
     * Executes the actions outlined under section "Rules for Bees" in the "se2811-lab3-7.pdf"
     * @return - returns whether the Bee is alive to the GameBoard "controller"; value will be true if the Bee
     * is alive and false otherwise
     */
    public Point2D takeTurn(int beeIndex) {
	    // 1. move
		Point2D newPosition = move();
        // 2. check for neighbors
        GameObject closestNeighbor = gameBoard.collisions(this, newPosition, this
                .getSideLength()/2);
        if(onFlower){
            Point2D oldPosition = this.getPosition();
            setPosition(newPosition);
        	while(closestNeighbor!=null){
        		setPosition(move());
                closestNeighbor = gameBoard.collisions(this, this.getPosition(), this
                        .getSideLength()/2);
        		energy++;
			}
			onFlower = false;
        	newPosition = getPosition();
        	setPosition(oldPosition);
		}
        // 3. interact with neighbors
        if(closestNeighbor!=null) {
            newPosition = attackNeighbor(closestNeighbor, newPosition, beeIndex);
        }
        // 4. check if dead
        this.alive = checkAlive();
        // 5. update garden on new position
	    return newPosition;
    }

    /**
     * moves this Bee with respect to its MovementBehavior
     * @return - the new position of this Bee wrapped as a Point2D object
     */
    public Point2D move() {
	    energy--; // uses 1 energy point per move
		return movementBehavior.move();
	}

    /**
     * checks to see if this Bee is alive indicated by having an energy value of greater than 0
     * @return true if this Bee is alive; false otherwise
     */
    public boolean checkAlive() {
	    boolean ret = true;
	    if(energy <= 0) {
	        ret = false;
        }
        return ret;
    }

    /**
     * attacks (takes energy) the GameObject provided based on the Class signature.
     * @param neighbor - a subclass of GameObject
     */
    public Point2D attackNeighbor(GameObject neighbor, Point2D position, int beeIndex) {
        int neighborEnergy = neighbor.getEnergy();
	    if(neighbor instanceof Flower) {
            if(neighborEnergy < 0){
                this.energy = 0;
            }else{
                this.energy += (neighborEnergy * energyModifier);
            }
	        onFlower = true;
	        return new Point2D(neighbor.getPosition().getX() + neighbor.getSideLength()/4,
                    neighbor.getPosition().getY() + neighbor.getSideLength()/4);
        } else if(neighbor instanceof Bee) {
            this.setEnergy(++this.energy);
            neighbor.setEnergy(--neighborEnergy);
            generateMovementType();
            return position;
        } else if (neighbor instanceof BeePowerUp) {
            if (!poweredUp) {
                pickupPowerUp(neighbor, beeIndex);
            }
            return position;
        } else {
            System.out.println("bee.attackNeighbor(): Something is here that shouldn't be.");
            return null;
        }
    }


    public void pickupPowerUp(GameObject neighbor, int beeIndex) {
        PowerUpType p = neighbor.getPowerUpType();
        if (p.equals(PowerUpType.DOUBLE_SPEED)) {
            gameBoard.powerUpBee(beeIndex, new DoubleSpeedPowerUp(this));
        } else if (p.equals(PowerUpType.DOUBLE_ENERGY)) {
            gameBoard.powerUpBee(beeIndex, new DoubleEnergyPowerUp(this));
        } else if (p.equals(PowerUpType.SHIELD)) {
            gameBoard.powerUpBee(beeIndex, new ShieldPowerUp(this));
        } else {
            System.out.println("Something shouldn't be here; Bee.pickupPowerUp()");
        }
    }

    /**
     * gets the Image representing this Bee
     * @return - the Image representation
     */
	public Image getImage(){
		return beeIcon;
	}

    /**
     * sets the image of the bee
     *
     * @param image - the new image
     */
    public void setImage(Image image) {
        this.beeIcon = image;
    }

    /**
     * gets the status of this Bee
     *
     * @return - true if this Bee is alive; false otherwise
     */
    public boolean isAlive(){
        return alive;
    }
}