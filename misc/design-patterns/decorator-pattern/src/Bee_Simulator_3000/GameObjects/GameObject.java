package Bee_Simulator_3000.GameObjects;

import Bee_Simulator_3000.GameBoard;
import javafx.geometry.Point2D;

/**
 * @author Gillian Horton (hortong)
 * @version 2.0
 * @created 14-Dec-2017 3:46:48 PM
 * Edited by: Ryley Powell (powellr)
 */
public abstract class GameObject {
    private Point2D position;
    private final double radius;
    private final int sideLength;
    protected int energy;
    protected PowerUpType powerUpType;

    public GameObject(int sideLength, double radius) {
        this.radius = radius;
        this.sideLength = sideLength;
        powerUpType = null;
    }

    // won't be used; only in place for BeePowerUpDecorator
    protected GameObject() {
        this.radius = 0;
        this.sideLength = 0;
    }

    public PowerUpType getPowerUpType() {
        return powerUpType;
    }

    public enum PowerUpType {DOUBLE_SPEED, DOUBLE_ENERGY, SHIELD}

    public Point2D getPosition() {
        return position;
    }

    public double getRadius() {
        return radius;
    }


    public boolean setPosition(Point2D newPosition){
        boolean ret = false;
        double x = newPosition.getX();
        double y = newPosition.getY();

        if (x >= 0 && x <= GameBoard.CANVAS_WIDTH &&
                y >= 0 && y <= GameBoard.CANVAS_HEIGHT) {
            this.position = newPosition;
            ret = true;
        }
        return ret;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }


    public int getSideLength() {
        return sideLength;
    }
}
