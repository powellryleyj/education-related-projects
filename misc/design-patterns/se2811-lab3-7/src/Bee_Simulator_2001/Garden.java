package Bee_Simulator_2001;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.Pane;
import javafx.scene.transform.Rotate;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * @author horton
 * @version 1.0
 * @created 14-Dec-2017 3:46:48 PM
 */
public class Garden implements Initializable {

	private ArrayList<GardenResident> gardenResidents;
	private ArrayList<Bee> bees;
	private ArrayList<Flower> flowers;
	@FXML
	Pane flowerBox;
	@FXML
	private Canvas flowerCanvas;
	@FXML
    private Canvas beeCanvas;
	@FXML
    private TextArea instructions;
	private GraphicsContext flowerGC;
    private GraphicsContext beeGC;
	public static int CANVAS_WIDTH = 1200;
	public static int CANVAS_HEIGHT = 1000;

	public static final boolean DEBUGGING = false;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
	    instructions.setText("Flower and Bee Simulation:\n\nRules:\n\tIn this simulation bees " +
                "have 3 movement" +
                " behaviors and flowers come in two different types.\n\tBees can move either " +
                "vertically, horizontally, or randomly.If a bee runs into another bee, then it " +
                "attacks takes some of that bees energy. The attacking bee then gets a new " +
                "movement behavior.\n\tFlowers can either be nectar flowers, " +
                "or" +
                " venus bee traps. If a bee runs into a nectar flower, it receives some energy" +
                ". " +
                "however, if the nectar flower is out of nectar it receives none.If a bee " +
                "runs " +
                "into a venus bee trap, it is instantly eaten.\n\nControls:\n\tIn order to advance time, press an arrow " +
                "key.\n\tIf you want to exit the program then press the x key.\n\n Watch bees die" +
                " " +
                "and enjoy!");
		gardenResidents = new ArrayList<>();
		bees = new ArrayList<>();
		flowers = new ArrayList<>();
		flowerCanvas.setFocusTraversable(true);
		beeCanvas.setFocusTraversable(true);
		flowerGC = flowerCanvas.getGraphicsContext2D();
		beeGC = beeCanvas.getGraphicsContext2D();
		flowerBox.setBackground(new Background(new BackgroundImage(new Image("Bee_Simulator_2001/grassBackground.jpg"), null, null,
				null, null)));
		populateWithPlants();
		populateWithBees();
	}

    /**
     * populates the canvas with 20 plants
     */
	private void populateWithPlants(){
		for(int i = 0; i < 20; i++){
			Flower newFlower = new Flower();
			newFlower.setPosition(generateRandomPosition(newFlower.getRadius()));
			gardenResidents.add(newFlower);
			flowers.add(newFlower);
			flowerGC.drawImage(newFlower.getImage(), newFlower.getPosition().getX(),
					newFlower.getPosition().getY(), newFlower.getSideLength(), newFlower
                            .getSideLength());
		}
	}

    /**
     * populates the canvas with 20 bees
     */
	private void populateWithBees(){
		for(int i = 0; i < 20; i++){
			Bee newBee = new Bee(this);
			newBee.setPosition(generateRandomPosition(newBee.getRadius()));
			gardenResidents.add(newBee);
			bees.add(newBee);
			beeGC.drawImage(newBee.getImage(), newBee.getPosition().getX(),
					newBee.getPosition().getY(), newBee.getSideLength(), newBee.getSideLength());
		}
	}

    /**
     * generates a random position on the canvas that is not colliding with another item already on
     * the canvas
     * @param radius
     * @return
     */
	private Point2D generateRandomPosition(double radius){
		int x = (int)((Math.random()*(CANVAS_WIDTH-(radius*2))));
		int y = (int)((Math.random()*(CANVAS_HEIGHT-(radius*2))));
		Point2D newPoint = new Point2D(x,y);
		return collisions(null, newPoint, radius) == null ? newPoint : generateRandomPosition
                (radius);
	}

	/**
	 * determines if the specified point collides with any object currently in the garden by
	 * iterating through the gardenResidents collection
     * @param thisResident the resident attempting to move to the point. null if the resident is
     *                     not in the residents list yet.
	 * @param point to check for collisions with
	 * @param sideLength of the object at point. used for calculating the minimum distance between
	 *                  two objects
	 * @return the GardenResident in collision with or null if there is none.
	 */
	public GardenResident collisions(GardenResident thisResident, Point2D point, double
            sideLength){
		for (GardenResident gardenResident: gardenResidents) {
			double centerX = point.getX() + sideLength/2;
			double centerY = point.getY() + sideLength/2;
			double centerXR = gardenResident.getPosition().getX() + gardenResident.getSideLength
                    ()/2;
			double centerYR = gardenResident.getPosition().getY() + gardenResident.getSideLength
                    ()/2;
			double distance = Math.pow(
							Math.pow(centerX - centerXR, 2) + Math.pow(centerY - centerYR, 2), 0.5);
			if(distance<(sideLength/2 + gardenResident.getSideLength()/2) && gardenResident !=
                    thisResident){
				return gardenResident;
			}
		}
		return null;
	}

	/**
	 * clears the canvas at the position the bee currently is at and draws the bee at its new
	 * position facing the direction it is moving.
	 * @param bee to erase and redraw
	 * @param newPosition new position to draw the bee at
	 * @return the new position the bee was drawn at.
	 */
	public Point2D moveBee(Bee bee, Point2D newPosition){
		beeGC.clearRect(bee.getPosition().getX(), bee.getPosition().getY(), bee
                .getSideLength(), bee.getSideLength());
		//potentially rotates the bee to be facing the direction of its movement.
        /*
		double angle = getDirection(bee.getPosition(), newPosition);
		angle = Math.toDegrees(angle);
		//takes into account the original orientation of the bee image;
		angle += 45;
		if(angle<0){
		    angle += 360;
        }
		drawRotatedImage(bee.getImage(), angle,
				newPosition.getX(), newPosition.getY(), bee);
        */
        beeGC.drawImage(bee.getImage(), newPosition.getX(), newPosition.getY(),  bee
                .getSideLength(), bee.getSideLength());

		return newPosition;
	}

    /**
     * determines the angle at which the bee should oriented from is oldPoint and newPoint
     * @param oldPoint at which the bee was
     * @param newPoint at which the bee will be
     * @return the angle the bee is facing.
     */
	private double getDirection(Point2D oldPoint, Point2D newPoint){
	    double x = newPoint.getX() - oldPoint.getX();
	    double y = newPoint.getY() - oldPoint.getY();
	    if(x<0){
	    	return Math.PI + Math.atan(y/x);
		}
		return Math.atan(y/x);
	}


	/**
	 * for each bee in the bee collection, calls its move method.
	 * @param event arrow key pressed on the keyboard
	 */
	public void advanceTime(KeyEvent event){
        if(event.getCode().isArrowKey()){
            int i = 0;
			for (int j = 0; j < bees.size(); j++){
			    Bee bee = bees.get(j);
			    if(DEBUGGING) {
					System.out.println("Bee " + i);
				}
				boolean beeAlive = bee.isAlive();
				if(beeAlive) {
				    Point2D newPosition = bee.takeTurn();
				    bee.setPosition(moveBee(bee, newPosition));
                } else {
				    killBee(bee);
				    j-=1;
                }
                i++;
				// bee.setPosition(moveBee(bee, bee.getPosition())); //first gets the new position by
				// calling bee.move(), then moves the bee on the canvas by calling moveBee(), and
				// finally sets the bee position to be the new position.
			}
		}else if(event.getText().equals("x")){
            System.exit(0);
        }
	}

    /**
     * removes a bee from the canvas and from the arrayList
     * @param bee to remove
     */
	private void killBee(Bee bee) {
		beeGC.clearRect(bee.getPosition().getX(), bee.getPosition().getY(), bee
				.getSideLength(), bee.getSideLength());
		bees.remove(bee);

	}

	/**
     * rotates the graphics context in preparation from drawing the image.
     * @param angle to rotate at
     * @param px x coordinate of point to rotate around.
     * @param py y coordinate of point to rotate around.
     */
	private void rotate(double angle, double px, double py) {
		Rotate r = new Rotate(angle, px, py);
		beeGC.setTransform(r.getMxx(), r.getMyx(), r.getMxy(), r.getMyy(), r.getTx(), r.getTy());
	}

    /**
     * draws the image on the canvas at the angle and point specified.
     * @param image to draw on the canvas
     * @param angle to rotate the image
     * @param tlpx x coordinate of the image
     * @param tlpy y coordinate of the image
     */
	private void drawRotatedImage(Image image, double angle, double tlpx, double tlpy, Bee bee) {
		beeGC.save(); // saves the current state on stack, including the current transform
		rotate(angle, tlpx + bee.getRadius(), tlpy + bee.getRadius());
		beeGC.drawImage(image, tlpx, tlpy,  bee.getSideLength(), bee.getSideLength());
		beeGC.restore(); // back to original state (before rotation)
	}

    /**
     * reusable worker function that generates a positive integer value between the range specified; this method utilizes the
     * Math class and therefore is not truly random and should not be used for encryption purposes
     * @param min - the minimum value within the range
     * @param max - the maximum value within the range
     * @return a positive integer within the specified range
     */
	public static int generateRandomInteger(int min, int max) {
		return (int) (min + (Math.random() * max));
	}

    /**
     * reusable worker function that generates a positive double value between the range specified; this method utilizes the
     * Math class and therefore is not truly random and should not be used for encryption purposes
     * @param min - the minimum value within the range
     * @param max - the maximum value within the range
     * @return a positive double within the specified range
     */
	public static double generateRandomDouble(double min, double max) {
	    return (min + (Math.random() * max));
    }
}