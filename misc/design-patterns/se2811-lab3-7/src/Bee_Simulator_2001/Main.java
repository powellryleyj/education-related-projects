package Bee_Simulator_2001;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("Garden.fxml"));
        primaryStage.setTitle("Welcome to our garden");
        primaryStage.setScene(new Scene(root, 1750, 1000));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
