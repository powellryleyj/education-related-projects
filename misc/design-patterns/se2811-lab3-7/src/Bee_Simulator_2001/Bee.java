package Bee_Simulator_2001;

import javafx.geometry.Point2D;
import javafx.scene.image.Image;

/**
 * @author Ryley
 * @version 1.0
 * @created 14-Dec-2017 3:46:48 PM
 */
public class Bee extends GardenResident{

    private Garden gameBoard;
    private boolean alive;
    private boolean onFlower;

    public final static int SIDE_LENGTH = 50;
	private final static double RADIUS = Math.pow(Math.pow(SIDE_LENGTH, 2) + Math.pow
			(SIDE_LENGTH, 2), 0.5)/2;

	private MovementBehavior movementBehavior;
	private final Image beeIcon = new Image("Bee_Simulator_2001/bee.png");


	public Bee(Garden gameBoard){
		super(SIDE_LENGTH, RADIUS);
		this.gameBoard = gameBoard;
		generateMovementType();
		energy = Garden.generateRandomInteger(20, 100);
		alive = true;
	}

	public boolean setPosition(Point2D point2D){
		return super.setPosition(point2D);
	}

    /**
     * generates a random movement type between StraightLineMovement, Horizontal and Vertical, and RandomMovement.
     */
	private void generateMovementType() {
		double rand = Garden.generateRandomDouble(0, 1);
		if(rand >= 0.5) {
		    rand = Garden.generateRandomDouble(0, 1);
		    if(rand >= 0.5) {
                movementBehavior = new VerticalLineMovement(this);
            } else {
		        movementBehavior = new HorizontalLineMovement(this);
            }
        } else {
		    movementBehavior = new RandomMovement(this);
        }
	}

    /**
     * Executes the actions outlined under section "Rules for Bees" in the "se2811-lab3-7.pdf"
     * @return - returns whether the Bee is alive to the Garden "controller"; value will be true if the Bee
     * is alive and false otherwise
     */
	public Point2D takeTurn() {
	    // 1. move
		Point2D newPosition = move();
        // 2. check for neighbors
        GardenResident closestNeighbor = gameBoard.collisions(this, newPosition, this
                .getSideLength()/2);
        if(onFlower){
            Point2D oldPosition = this.getPosition();
            setPosition(newPosition);
        	while(closestNeighbor!=null){
        		setPosition(move());
                closestNeighbor = gameBoard.collisions(this, this.getPosition(), this
                        .getSideLength()/2);
        		energy++;
			}
			onFlower = false;
        	newPosition = getPosition();
        	setPosition(oldPosition);
		}
        // 3. interact with neighbors
        if(closestNeighbor!=null) {
            newPosition = attackNeighbor(closestNeighbor, newPosition);
        }
        // 4. check if dead
        this.alive = checkAlive();
        // 5. update garden on new position
	    return newPosition;
    }

    /**
     * moves this Bee with respect to its MovementBehavior
     * @return - the new position of this Bee wrapped as a Point2D object
     */
	private Point2D move(){
	    energy--; // uses 1 energy point per move
		return movementBehavior.move();
	}

    /**
     * checks to see if this Bee is alive indicated by having an energy value of greater than 0
     * @return true if this Bee is alive; false otherwise
     */
	private boolean checkAlive() {
	    boolean ret = true;
	    if(energy <= 0) {
	        ret = false;
        }
        return ret;
    }

    /**
     * attacks (takes energy) the GardenResident provided based on the Class signature.
     * @param neighbor - a subclass of GardenResident
     */
    private Point2D attackNeighbor(GardenResident neighbor, Point2D position) {
        int neighborEnergy = neighbor.getEnergy();
	    if(neighbor instanceof Flower) {
            if(neighborEnergy < 0){
                this.energy = 0;
            }else{
                this.energy += neighborEnergy;
            }
	        onFlower = true;
	        return new Point2D(neighbor.getPosition().getX() + neighbor.getSideLength()/4,
                    neighbor.getPosition().getY() + neighbor.getSideLength()/4);
        } else if(neighbor instanceof Bee) {
	        this.setEnergy(++this.energy);
	        neighbor.setEnergy(--neighborEnergy);
	        generateMovementType();
	        return position;
        } else {
	        System.out.println("bee.attackNeighbor(): Something is here that shouldn't be.");
	        return null;
        }
    }

    /**
     * gets the Image representing this Bee
     * @return - the Image representation
     */
	public Image getImage(){
		return beeIcon;
	}

	public boolean isAlive(){
		return alive;
	}
}