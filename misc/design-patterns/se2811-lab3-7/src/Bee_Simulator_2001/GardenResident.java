package Bee_Simulator_2001;

import javafx.geometry.Point2D;

public abstract class GardenResident {
    private Point2D position;
    private final double radius;
    private final int sideLength;
    protected int energy;

    public GardenResident(int sideLength, double radius){
        this.radius = radius;
        this.sideLength = sideLength;
       }

    public Point2D getPosition() {
        return position;
    }

    public double getRadius() {
        return radius;
    }


    public boolean setPosition(Point2D newPosition){
        boolean ret = false;
        double x = newPosition.getX();
        double y = newPosition.getY();

        if(x >= 0 && x <= Garden.CANVAS_WIDTH &&
                y >= 0 && y <= Garden.CANVAS_HEIGHT) {
            this.position = newPosition;
            ret = true;
        }
        return ret;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }


    public int getSideLength() {
        return sideLength;
    }
}
