package Bee_Simulator_2001;


import javafx.geometry.Point2D;

/**
 * @author Ryley
 * @version 1.0
 * @created 14-Dec-2017 3:46:49 PM
 */
public class RandomMovement extends MovementBehavior {

	public RandomMovement(Bee b){
       super(b);
	}

    /**
     * determines the next position of the Bee
     * @return - returns the point the bee will move to
     */
	public Point2D move(){
	    if(Garden.DEBUGGING) {
            System.out.println(" is moving with random movement."); // for debugging purposes
        }
	    double x = b.getPosition().getX(), y = b.getPosition().getY();
	    randomizeDirection();
	    if(!checkXBoundary((x + (MOVEMENT_SPEED * xDirection)))) {
	        xDirection = flipDirection(xDirection);
	        x += (MOVEMENT_SPEED * xDirection);
        } else {
	        x += (MOVEMENT_SPEED * xDirection);
        }

        if(!checkYBoundary((y + (MOVEMENT_SPEED * yDirection)))) {
	        yDirection = flipDirection(yDirection);
	        y += (MOVEMENT_SPEED * yDirection);
        } else {
	        y += (MOVEMENT_SPEED * yDirection);
        }

        return new Point2D(x, y);
	}

    /**
     * generates a random direction for both x and y when called.
     */
	private void randomizeDirection() {
        double xRand = Garden.generateRandomDouble(0, 1);
        double yRand = Garden.generateRandomDouble(0, 1);
        if(xRand >= 0.5) {
            xDirection = flipDirection(xDirection);
        }
        if(yRand >= 0.5) {
            yDirection = flipDirection(yDirection);
        }
    }
}