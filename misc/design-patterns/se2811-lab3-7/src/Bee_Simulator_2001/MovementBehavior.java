package Bee_Simulator_2001;


import javafx.geometry.Point2D;

/**
 * @author Ryley
 * @version 1.0
 * @created 14-Dec-2017 3:46:48 PM
 */
public abstract class MovementBehavior {

    protected Bee b;

    protected final int MOVEMENT_SPEED = 50;
    protected int xDirection;
    protected int yDirection;

    protected MovementBehavior(Bee b) {
        this.b = b;
        xDirection = randomizeInitialDirection();
        yDirection = randomizeInitialDirection();
    }

    /**
     * determines the next position of the Bee
     * @return - returns the point the bee will move to
     */
	public abstract Point2D move();

    private int randomizeInitialDirection() {
        double decision = Garden.generateRandomDouble(0, 1);
        if (decision >= 0.5) {
            return 1;
        }
        return -1;
    }

    /**
     * inverts the direction specified by the parameter axis
     * @param axis - either the x or y axis.
     */
    protected int flipDirection(int axis) {
        axis *= -1;
        return axis;
    }

    /**
     * checks the canvas boundaries set in Garden against the xValue passed to this function
     * @param xValue - the xValue to be checked
     * @return - true if and only if the xValue is valid; false otherwise
     */
    protected boolean checkXBoundary(double xValue) {
        boolean ret = false;

        if(xValue >= 0 && xValue <= Garden.CANVAS_WIDTH) {
            ret = true;
        }
        return ret;
    }

    /**
     * checks the canvas boundaries set in Garden against the yValue passed to this function
     * @param yValue - the yValue to be checked
     * @return - true if and only if the yValue is valid; false otherwise
     */
    protected boolean checkYBoundary(double yValue) {
        boolean ret = false;

        if(yValue >= 0 && yValue <= Garden.CANVAS_HEIGHT) {
            ret = true;
        }
        return ret;
    }
}