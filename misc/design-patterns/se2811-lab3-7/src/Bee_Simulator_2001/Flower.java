package Bee_Simulator_2001;

import javafx.geometry.Point2D;
import javafx.scene.image.Image;

/**
 * @author Horton
 * @version 1.0
 * @created 14-Dec-2017 3:46:48 PM
 */
public class Flower extends GardenResident{

	private int energy;
	private final static Image nectarFlower = new Image("Bee_Simulator_2001/flower4.png");
	private final static Image carnivoreFlower = new Image("Bee_Simulator_2001/beeTrap.png");
	private final static int SIDE_LENGTH = 100;
	private final static double RADIUS = Math.pow(Math.pow(50, 2) + Math.pow(50, 2), 0.5);
	enum FlowerType { NECTAR, CARNIVORE}
	private FlowerType flowerType;

	/**
	 * constructor for a flower object, object has a 33% chance of being a carnivore flower.
	 * if it is a nectar flower it initially has 15 nectar.
	 */
	public Flower(){
		super(SIDE_LENGTH, RADIUS);
		if(Math.random() < 0.33){
			flowerType = FlowerType.CARNIVORE;
		}else {
			flowerType = FlowerType.NECTAR;
			energy = 15;
		}
	}

	public boolean setPosition(Point2D point2D){
		return super.setPosition(point2D);
	}


	@Override
    /**
     * delegation call such that a bee can call the giveEnergy method through a GardenResident object
     */
	public int getEnergy() {
		return giveEnergy();
	}

	/**
	 * method to be called by a bee object when a bee land on the flower.
	 * @return returns 5 energy if the flower is a nectar flower, and is not out of nectar,
	 * returns 0 energy if the flower is out of energy, and returns -1 if the flower is a
	 * carnivorous flower.
	 */
	public int giveEnergy(){
		if (flowerType.equals(FlowerType.CARNIVORE)) {
			return -1;
		}else if(energy < 0){
			energy -= 5;
			return 5;
		}
		return 0;
	}

	/**
	 * returns the correct image depending on the flower type
	 * @return
	 */
	public Image getImage() {
		if(flowerType.equals(FlowerType.CARNIVORE)){
			return carnivoreFlower;
		}
		return nectarFlower;
	}

}