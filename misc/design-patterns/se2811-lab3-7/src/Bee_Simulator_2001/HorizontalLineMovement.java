package Bee_Simulator_2001;

import javafx.geometry.Point2D;

/**
 * @author Ryley
 * @version 1.0
 * @created 21-Dec-2017 4:10 PM
 */
public class HorizontalLineMovement extends MovementBehavior {


    public HorizontalLineMovement(Bee b){
        super(b);
    }

    /**
     * determines the next position of the Bee
     * @return - returns true if the Bee has moved; false otherwise
     */
    public Point2D move() {
        if(Garden.DEBUGGING) {
            System.out.println(" is moving with horizontal movement."); // for debugging purposes
        }
        double x = b.getPosition().getX(), y = b.getPosition().getY();
        if(!checkXBoundary((x + (MOVEMENT_SPEED * xDirection)))){
            xDirection = flipDirection(xDirection);
            if(!checkYBoundary(adjustRow())){
                yDirection = flipDirection(yDirection);
                y = adjustRow();
            } else {
                y = adjustRow();
            }
        } else {
            x += (MOVEMENT_SPEED * xDirection);
        }
        return new Point2D(x, y);
    }

    /**
     * sends the Bee to the next row with respect to its yDirection
     */
    private double adjustRow() {
        double y = b.getPosition().getY();
        y += (MOVEMENT_SPEED * yDirection);
        return y;
    }
}
