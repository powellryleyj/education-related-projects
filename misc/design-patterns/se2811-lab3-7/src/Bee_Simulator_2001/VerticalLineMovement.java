package Bee_Simulator_2001;


import javafx.geometry.Point2D;

/**
 * @author Ryley
 * @version 1.0
 * @created 14-Dec-2017 3:46:49 PM
 */
public class VerticalLineMovement extends MovementBehavior {

	public VerticalLineMovement(Bee b){
		super(b);
	}

    /**
     * determines the next position of the Bee
     * @return - return the point the bee will move to
     */
	public Point2D move() {
	    if(Garden.DEBUGGING) {
            System.out.println(" is moving with vertical movement."); // for debugging purposes
        }
        double x = b.getPosition().getX(), y = b.getPosition().getY();
        if(!checkYBoundary((y + (MOVEMENT_SPEED * yDirection)))){
            yDirection = flipDirection(yDirection);
            if(!checkXBoundary(adjustColumn())) {
                xDirection = flipDirection(xDirection);
                x = adjustColumn();
            } else {
                x = adjustColumn();
            }
        } else {
            y += (MOVEMENT_SPEED * yDirection);
        }
        return new Point2D(x, y);
    }

    /**
     * sends the Bee to the next column with respect to its xDirection
     */
    private double adjustColumn(){
        double x = b.getPosition().getX();
        x += (MOVEMENT_SPEED * xDirection);
        return x;
    }

}