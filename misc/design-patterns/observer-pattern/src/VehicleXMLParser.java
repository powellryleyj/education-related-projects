/*
 * @author Jay Urbain, Josiah Yoder
 * Winter 2014-2015
 *
 * XML SAX Parser for bus locations.
 * 
 */

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class VehicleXMLParser extends DefaultHandler {

    private String chars;

    // The following variables hold values read from the file until a
    //     VehicleText object can be created.
    /**
     * Vehicle ID -- a number
     */
    private String vid = "";
    /**
     * Vehicle latitude (degrees)
     */
    private String lat = "";
    /**
     * Vehicle longitude (degrees)
     */
    private String lon = "";

    /**
     * Vehicle route
     */
    private String rt = "";

    /**
     * Timestamp (see VehicleText for more info)
     */
    private String tmstmp = "";

    public List<VehicleText> vehicleList;

    /*
      Querying:
      http://realtime.ridemcts.com/bustime/api/v1/getvehicles?key=_______&rt=GRE
    */

    public VehicleXMLParser() throws Exception {
        super();
    }

    //===========================================================
    // SAX DocumentHandler methods
    //===========================================================

    public void startDocument() throws SAXException {
        //System.out.println("START DOCUMENT File");
        vehicleList = new ArrayList<VehicleText>();
    }

    public void endDocument() throws SAXException {
    }

    public void startElement(String namespaceURI, String lName, // local name
                             String qName, // qualified name
                             Attributes attrs) throws SAXException {
        String elementName;
        Attributes attributes;

        String eName = lName; // element name
        if ("".equals(eName))
            eName = qName; // namespaceAware = false
        elementName = eName;

        if (attrs != null) {
            attributes = attrs;
        }

        chars = "";
    }

    public void endElement(String namespaceURI, String sName, // simple name
                           String qName // qualified name
                           ) throws SAXException {

        String eName = sName; // element name
        if ("".equals(eName))
            eName = qName; // namespaceAware = false

        if(eName.equals("vid")) {
            vid = chars;
        }

        if(eName.equals("lat")) {
            lat = chars;
        }

        if(eName.equals("lon")) {
            lon = chars;
        }

        if(eName.equals("tmstmp")) {
            tmstmp = chars;
        }

        if(eName.equals("rt")) {
            rt = chars;
        }

        if(eName.equals("vehicle")) {
            VehicleText vehicleText = new VehicleText(vid, tmstmp, lat, lon, rt);
            vehicleList.add(vehicleText);
        }
    }

    /**
     * Receive notification of character data inside an element, and save that
     * character data for later use.
     *
     * @param buf The characters.
     * @param offset The start position in the character array.
     * @param len The number of characters to use from the
     *               character array.
     * @exception org.xml.sax.SAXException Any SAX exception, possibly
     *            wrapping another exception, if a SAXException occurs
     * @see org.xml.sax.ContentHandler#characters
     */
    public void characters(char buf[], int offset, int len) throws SAXException {
        for(int i = 0; i < len; i++) {
            chars += buf[i + offset];
        }
        if(offset < 0 || offset + len > buf.length) {
            throw new SAXException(
                                   "Bad range given to characters method: buf has length "+buf.length+", " +
                                   "but asked for range ["+offset+" - "+(offset+len)+").");
        }
    }

    /**
     * Parse the weather data.
     * @param xml the XML string containing the weather data
     * @throws RealtimeWrapperException if, for example, the XML data is malformed.
     */
    public synchronized void parseXML(String xml) throws RealtimeWrapperException {
        // Use the default (non-validating) parser
        SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setValidating(false);
        try {
            // Parse the input
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse(new InputSource(new StringReader(xml)), 
                            (DefaultHandler) this);
        } catch (Throwable t) {
            t.printStackTrace();
            throw new RealtimeWrapperException("Error while parsing the XML file.");
        }
    }

    public List<VehicleText> getForecastList() {
        return vehicleList;
    }
}
