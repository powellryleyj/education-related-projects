import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Lab5 extends Application {

    public static final boolean DEBUGGING = true;

    /**
     * Runs the JavaFX application
     *
     * @param primaryStage the stage being displayed to
     * @throws Exception an exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("Lab5.fxml"));
        primaryStage.setTitle("Bus Tracker");
        primaryStage.setScene(new Scene(root, 840, 688));
        primaryStage.show();
    }

    public Lab5() {
    }

    /**
     * Entry point to the application
     * @param args unused
     */
    public static void main(String[] args) {
        launch(args);
    }
}
