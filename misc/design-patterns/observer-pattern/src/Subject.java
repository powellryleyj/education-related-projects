import java.util.HashMap;

public interface Subject {
    void attach(Observer observer);
    void detach(Observer observer);
    void notifyObservers();
    Object getContextSpecificInfo(HashMap<String, String> context);
}
