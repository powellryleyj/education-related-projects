public class Bus {
    private VehicleText info;
    private double milesEastOfCityHall, milesNorthOfCityHall;

    public Bus(VehicleText info){
        this.info = info;
        translateLatLon();
    }

    public void setInfo(VehicleText info) {
        this.info = info;
        translateLatLon();
    }

    public String getVid() {
        return info.getVid();
    }

    public String getTmstmp() {
        return info.getTmstmp();
    }

    public double getLat() {
        return Double.parseDouble(info.getLat());
    }

    public double getLon() {
        return Double.parseDouble(info.getLon());
    }

    public String getRoute() { return info.getRt(); }

    public double getMilesEastOfCityHall() {
        return milesEastOfCityHall;
    }

    public double getMilesNorthOfCityHall() {
        return milesNorthOfCityHall;
    }

    private void translateLatLon(){
        double deltaLat = getLat() - BusDataWrapper.CITY_HALL_LAT;
        double deltaLon = getLon() - BusDataWrapper.CITY_HALL_LON;
        milesNorthOfCityHall = deltaLat * BusDataWrapper.LAT_TO_MI_CONVERSION_FACTOR;
        milesEastOfCityHall = deltaLon * BusDataWrapper.LON_TO_MI_CONVERSION_FACTOR;
    }
}
