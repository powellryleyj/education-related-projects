import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BusDataWrapper implements Subject {
    public static final double CITY_HALL_LAT               = 43.04173,   CITY_HALL_LON               = -87.90976,
                               ART_MUSEUM_LAT              = 43.039290,  ART_MUSEUM_LON              = -87.897069,
                               AIRPORT_LAT                 = 42.948085,  AIRPORT_LON                 = -87.904405,
                               LAT_TO_MI_CONVERSION_FACTOR = 69.03,      LON_TO_MI_CONVERSION_FACTOR = 50.63;
    public static final String DATA_CONTEXT = "BUS_DATA", ROUTE_CONTEXT = "route";

    private HashMap<String, Bus> buses;
    private ArrayList<Observer> observers;
    private RealtimeWrapper dataSource;
    private Timeline refreshTimer;

    public BusDataWrapper(){
        buses = new HashMap<>();
        observers = new ArrayList<>();
        try {
            dataSource = new RealtimeWrapper("key.txt");
        } catch(RealtimeWrapperException e){
            new Alert(Alert.AlertType.ERROR, e.getMessage()).showAndWait();
            Platform.exit();
        }
        try {
            List<VehicleText> rawData = dataSource.fetchVehicles();
            for(VehicleText vt : rawData){
                buses.put(vt.getVid(), new Bus(vt));
            }
        } catch(RealtimeWrapperException e){
            new Alert(Alert.AlertType.ERROR, e.getMessage()).show();
        }
        refreshTimer = new Timeline(new KeyFrame(Duration.millis(10000), event -> update()));
        refreshTimer.setCycleCount(Animation.INDEFINITE);
        refreshTimer.play();
    }

    @Override
    public void attach(Observer observer){
        observers.add(observer);
    }

    @Override
    public void detach(Observer observer){
        observers.remove(observer);
    }

    @Override
    public void notifyObservers(){
        for(Observer o : observers){
            o.update();
        }
    }


    public HashMap<String, Bus> getBuses() {
        return buses;
    }


    private void update() {
        try {
            List<VehicleText> rawData = dataSource.fetchVehicles();
            for (VehicleText vt : rawData) {
                synchronized (buses) {
                    buses.get(vt.getVid()).setInfo(vt);

                    if (Lab5.DEBUGGING) {
                        System.out.println(vt.toString());
                    }
                }
            }
        } catch (RealtimeWrapperException e) {
            new Alert(Alert.AlertType.ERROR, e.getMessage()).show();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        notifyObservers();
    }


    public Object getContextSpecificInfo(HashMap<String, String> context){
        if(context.containsKey(DATA_CONTEXT)) {
            String vid = context.get(DATA_CONTEXT);
            if(Lab5.DEBUGGING) {
                System.out.println(context.toString());
            }
            return buses.get(vid);
        }
        return null;
    }
}
