import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Scanner;

/**
 * Wrapper to the MCTS realtime Web API
 */
public class RealtimeWrapper {
    private final static String VEHICLE_SEARCH_URL =
        "http://realtime.ridemcts.com/bustime/api/v1/getvehicles?key=%s&rt=14,15,21,23,27,43,52,53,54,55";

    private final String key;

    /**
     * Creates a Vehicle API capable of fetching
     * XML from the Milwaukee County Transit System
     * realtime page.
     *
     * You must request a developer key to access this information.
     * First, create an account at http://realtime.ridemcts.com/.
     * Then, request API access.  API access requires
     * your address information, including a phone number. Use MSOE's address for
     * the address, your msoe email as your email for the account, and your phone
     * number for the phone number.
     *
     * MSOE's address is:
     * 1025 North Broadway
     * Milwaukee WI 53202
     *
     * Do not share your key with anyone else or post it online.
     *
     * @param keyfile File containing only the developer key required to connect to the page, as ASCII text.
     */
    public RealtimeWrapper(String keyfile) throws RealtimeWrapperException {
        Scanner fileScanner;
        try {
            fileScanner = new Scanner(new FileReader(keyfile));
        } catch (FileNotFoundException e) {
            throw new RealtimeWrapperException("Could not open the key file: "
                                               + keyfile, e);
        }
        if(!fileScanner.hasNext()) {
            throw new RealtimeWrapperException(
                                               "Could not find key. (Should be the first white-space-delimited " +
                                               "word in the file)");
        }
        this.key = fileScanner.next();
    }

    /**
     * Fetch the current positions of vehicles along the specified route
     * (hard-coded to the GREEN line (GRE) in this implementation)
     * @return list of raw VehicleText objects.
     */
    public List<VehicleText> fetchVehicles() throws RealtimeWrapperException {
        String vehicleXMLString;
        vehicleXMLString = getVehicleXML();
        VehicleXMLParser vehicleXMLParser;
        try {
            vehicleXMLParser = new VehicleXMLParser();
        } catch(Exception e) {
            throw new RealtimeWrapperException("Received response, but could not parse "
                                               + "the response from the webpage", e);
        }
        vehicleXMLParser.parseXML(vehicleXMLString);
        
        return vehicleXMLParser.getForecastList();
    }

    /**
     * Submit URL query
     *
     * @param query well formed URL query, i.e., Convert spaces to +, etc. to make a 
     *   valid URL.
     *   Use the proper encoding: URLEncoder.encode(xxx, "UTF-8");
     * @return URL response String
     * @throws RealtimeWrapperException if anything goes wrong.
     */
    private static String urlQuery(String query) throws RealtimeWrapperException {

        System.out.println(" Querying " + query);
        String response = "";

        try {
            // Assume: query = URLEncoder.encode(query, "UTF-8");
            URL url = new URL(query);
            URLConnection connection = url.openConnection();
            //connection.addRequestProperty("Referer", HTTP_REFERER);

            // Get the XML response
            String line;
            StringBuilder builder = new StringBuilder();
            BufferedReader reader = 
                new BufferedReader(new InputStreamReader(connection.getInputStream()));
            while((line = reader.readLine()) != null) {
                builder.append(line);
            }

            response = builder.toString();
            // System.out.println(response);
        }
        catch (Exception e) {
            System.err.println("Something went wrong in urlQuery ...");
            e.printStackTrace();
            throw new RealtimeWrapperException("Something went wrong while querying the url: "+query);
        }
        return response;
    }

    /**
     * Format the required URL and send out the fetch.
     * @return String representation of the vehicles
     * @throws RealtimeWrapperException if something goes wrong...
     */
    private String getVehicleXML() throws RealtimeWrapperException {
        System.out.println("New version");
        String url = String.format(VEHICLE_SEARCH_URL, key);
        return urlQuery(url);
    }
}
