import com.sun.istack.internal.NotNull;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;

import java.text.DecimalFormat;
import java.time.Instant;
import java.util.ArrayList;


/**
 * this object will follow the observer pattern and "observe" the subject object
 * passed to it's constructor.
 *
 * displays relevant data and calculated statistics
 *
 * @author Ryley Powell (powellr)
 */
public class DataTab extends Tab implements Observer {


    /**
     * conversion factor from seconds to hours
     */
    private static final double HOURS_PER_SECOND = 0.00027778;

    /**
     * subject being tracked by this observer
     */
    private BusDataWrapper subject;

    //private HashMap<String, String> context; // NOT USED

    /**
     * the VID of the bus being tracked  by this observer
     */
    private String busID;

    /**
     * the pane wrapped by this observer
     */
    private Pane dataPane;

    /**
     * holds a list of points since a particular bus is being tracked
     *
     * Point serves as a wrapper to the longitude and latitude in which
     * the longitude corresponds to the x-coordinate and latitude corresponds to
     * the y-coordinate
     *
     * this list is used when calculating the total distance traveled by the bus
     * therefore indirectly when calculating the average speed of the bus
     */
    private ArrayList<Point2D> positions;

    /**
     * flag variable that determines whether the position list and startTime should be reset
     * dependent upon a new bus being selected by the user
     */
    private boolean firstUpdate;

    /**
     * serves as the "time zero" since the bus had been initially tracked
     */
    private Instant startTime; // easier usage than LocalDateTime since no parsing is needed

    /**
     * constant values to make selecting nodes easier
     */
    private enum NODE_IDS{
        BUS_INPUT,
        MILES_TO_NORTH,
        MILES_TO_EAST,
        AVERAGE_BUS_SPEED,
        DISTANCE_TRAVELED,
        ERROR_LABEL,
    }

    public DataTab(String title,@NotNull BusDataWrapper subject, Node source){
        super(title, source);
        this.subject = subject; // this is busDataWrapper
        subject.attach(this);
        busID = ""; // instantiated to do nothing
        firstUpdate = true;
        //context = new HashMap<>(); // not used
        initTab();
    }

    /**
     * dynamically creates the content of this tab such that multiple tabs can exist
     */
    private void initTab() {
        //instantiate pane
        dataPane = new Pane();
        dataPane.setPrefSize(929.5, 660.0);

        //start text input
        //input label
        Label textPrompt = new Label("Type a Bus ID:");
        textPrompt.setLayoutX(54.0);
        textPrompt.setLayoutY(14.0);
        textPrompt.setPrefWidth(260.0);

        //textfield input
        TextField busInput = new TextField();
        busInput.setId(NODE_IDS.BUS_INPUT.name());
        busInput.setLayoutX(54.0);
        busInput.setLayoutY(64.0);
        busInput.setPrefWidth(312.0);
        busInput.textProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue.equals("")) {
                updateLabel(NODE_IDS.ERROR_LABEL.name(), "Input was left blank.");
            } else {
                busID = newValue;
                firstUpdate = true;
                positions = new ArrayList<>();
                update();
            }
        });

        //textfield error label
        Label errorLabel = new Label();
        errorLabel.setLayoutX(505.0);
        errorLabel.setLayoutY(67.0);
        errorLabel.setId(NODE_IDS.ERROR_LABEL.name());
        //end text input

        //start data fields
        //distance label
        Label distanceFromLabel = new Label("Distance From City Hall:");
        distanceFromLabel.setLayoutX(54.0);
        distanceFromLabel.setLayoutY(154.0);

        //miles to north label
        Label milesToNorthLabel = new Label("Miles to the North:");
        milesToNorthLabel.setLayoutX(54.0);
        milesToNorthLabel.setLayoutY(194.0);

        //miles to north label output
        Label milesToNorthOutput = new Label();
        milesToNorthOutput.setLayoutX(54.0);
        milesToNorthOutput.setLayoutY(234.0);
        milesToNorthOutput.setId(NODE_IDS.MILES_TO_NORTH.name());

        //miles to east label
        Label milesToEastLabel = new Label("Miles to the East:");
        milesToEastLabel.setLayoutX(54.0);
        milesToEastLabel.setLayoutY(274.0);

        //miles to east label output
        Label milesToEastOutput = new Label();
        milesToEastOutput.setLayoutX(54.0);
        milesToEastOutput.setLayoutY(314.0);
        milesToEastOutput.setId(NODE_IDS.MILES_TO_EAST.name());
        //end data fields

        //start stat fields
        //distance traveled label
        Label distanceTraveled = new Label("Distance Traveled:");
        distanceTraveled.setLayoutX(505.0);
        distanceTraveled.setLayoutY(154.0);

        //distance traveled output label
        Label distanceTraveledOutput = new Label();
        distanceTraveledOutput.setLayoutX(505.0);
        distanceTraveledOutput.setLayoutY(194.0);
        distanceTraveledOutput.setId(NODE_IDS.DISTANCE_TRAVELED.name());

        //average speed label
        Label averageSpeed = new Label("Average Speed:");
        averageSpeed.setLayoutX(505.0);
        averageSpeed.setLayoutY(234.0);

        //average speed output label
        Label averageSpeedOutput = new Label();
        averageSpeedOutput.setLayoutX(505.0);
        averageSpeedOutput.setLayoutY(274.0);
        averageSpeedOutput.setId(NODE_IDS.AVERAGE_BUS_SPEED.name());
        //end stats

        //start separators (to make things "pretty" looking)
        Separator s0 = new Separator();
        s0.setLayoutX(0.0);
        s0.setLayoutY(146.0);
        s0.setOpacity(1.0);
        s0.setPrefWidth(927.0);
        //end separators

        //add content to pane
        dataPane.getChildren().addAll(textPrompt, busInput, errorLabel, distanceFromLabel,
                milesToNorthLabel, milesToNorthOutput, milesToEastLabel, milesToEastOutput,
                distanceTraveled, distanceTraveledOutput, averageSpeed, averageSpeedOutput,
                s0);

        //set tab content to pane
        this.setContent(dataPane);
        this.setOnClosed(event -> subject.detach(this));
    }

    /**
     * updates this observer based on state change from the subject; in the context of this
     * project, the subject is the BusDataWrapper
     */
    @Override
    public void update(){
        if(subject.getBuses().containsKey(busID)) {
            if(firstUpdate) {
                startTime = Instant.now();
                firstUpdate = false;
            }
            Bus temp = subject.getBuses().get(busID);
            this.setText("Bus: " + temp.getVid());
            convertToPoint(temp.getLon(), temp.getLat());
            updateTabLabels(temp);
        } else if(busID.equals("")) {
            //special case -> DO NOTHING
        } else {
            updateLabel(NODE_IDS.ERROR_LABEL.name(), "Bus ID couldn't be found.");
        }
    }

    /**
     * worker method that updates all labels that are subject to change
     * @param bus - the reference to the bus being tracked by this observer
     */
    private void updateTabLabels(@NotNull Bus bus) {
        DecimalFormat decimalFormat = new DecimalFormat("0.0");

        updateLabel(NODE_IDS.MILES_TO_NORTH.name(),
                decimalFormat.format(bus.getMilesNorthOfCityHall()));

        updateLabel(NODE_IDS.MILES_TO_EAST.name(),
                decimalFormat.format(bus.getMilesEastOfCityHall()));

        double dist = calculateDistanceTraveled();

        updateLabel(NODE_IDS.DISTANCE_TRAVELED.name(),
                decimalFormat.format(dist) + " miles");

        updateLabel(NODE_IDS.AVERAGE_BUS_SPEED.name(),
                Integer.toString((int) Math.round(calculateAverageSpeed(dist))) + " miles per hour");

        updateLabel(NODE_IDS.ERROR_LABEL.name(), "");
    }

    /**
     * worker method that converts latitude and longitude to miles and stores the result as
     * a Point2D object within the positions list
     * @param lon - the longitude of the bus object (x-coordinate)
     * @param lat - the latitude of the bus object (y-coordinate)
     */
    private void convertToPoint(Double lon, Double lat) {
        positions.add(new Point2D(
                lon * BusDataWrapper.LON_TO_MI_CONVERSION_FACTOR,
                lat * BusDataWrapper.LAT_TO_MI_CONVERSION_FACTOR
        ));
    }

    /**
     * worker method that calculates the averages speed by using the following formula:
     *
     * averageSpeed = total distance / time elapsed
     *
     * ***NOTE*** the return value is heavily dependent on the time elapsed and
     *  will be skewed towards a larger average speed if the time elapsed is a relatively small number.
     *  This will balance out as more time has passed and the total distance increases
     *
     * @param distanceTraveled - the total distance traveled by the bus object
     * @return - will return 0.0 if the distance provided is less than or equal to 0; otherwise
     * will return the calculated value.
     */
    private double calculateAverageSpeed(double distanceTraveled) {
        double ret = 0.0;
        if(distanceTraveled > 0.0) {
            Instant currentTime = Instant.now();
            long start = startTime.getEpochSecond();
            long current = currentTime.getEpochSecond();
            long timeElapsedSeconds = current - start;
            double timeElapsedHours = timeElapsedSeconds * HOURS_PER_SECOND;
            if(Lab5.DEBUGGING) {

                System.out.println(timeElapsedSeconds + "\n" + timeElapsedHours);
            }
            ret = distanceTraveled / timeElapsedHours;
        }
        return ret;
    }

    /**
     * worker method that calculates sum of the distance between each recorded point in
     * the positions list
     * @return - will return 0.0 if only the size of the positions list is 1 or less due to
     * needing at least two recorded positions to calculate distance; otherwise will return
     * the calculated value
     */
    private double calculateDistanceTraveled(){
        double dist = 0.0; //default value
        if(positions.size() > 1) {
            Point2D currPoint, oldPoint = positions.get(0);
            for (Point2D p: positions) {
                currPoint = p;
                dist += currPoint.distance(oldPoint);
                oldPoint = currPoint;
            }
        }
        if(Lab5.DEBUGGING) {
            System.out.println(dist);
        }
        return dist;
    }

    /**
     * worker method that reduces duplicate code by capturing a sequence of methods within a single
     * method call
     * @param labelID - the ID assigned to the label to be updated
     * @param updateText - the new text of the label
     */
    private void updateLabel(String labelID, String updateText) {
        Label temp = (Label) dataPane.lookup("#" + labelID);
        temp.setText(updateText);
    }

    /**
     * UNUSED METHOD
     * @throws UnsupportedOperationException - this method will throw an Exception if invoked
     */
    @Override
    public void addContext(String info) throws UnsupportedOperationException {

        //context.put(BusDataWrapper.DATA_CONTEXT, info);
    }


}
