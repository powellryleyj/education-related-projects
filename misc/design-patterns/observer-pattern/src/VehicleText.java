/*
 * @author Jay Urbain, Josiah Yoder
 * Date: 1/5/2014
 */

/**
 * This class represents the raw text describing a position of a single bus.
 * The web API wrapper will return some sort of collection of these representing all the vehicles on the route.
 *
 * You probably don't want to use this as your state object...
 */
public class VehicleText {
    /**
     * Vehicle ID -- a number
     */
    private final String vid;
    /**
     * Timestamp when information was captured (e.g. 20150115 10:23)
     */
    private final String tmstmp;
    /**
     * Vehicle latitude (degrees)
     */
    private final String lat;
    /**
     * Vehicle longitude (degrees)
     */
    private final String lon;

    /**
     * Vehicle route
     */
    private final String rt;

    /**
     * @param vid Vehicle ID - a number
     * @param lat Vehicle latitude (degrees)
     * @param lon Vehicle longitude (degrees)
     * @param tmstmp Timestamp (e.g. 20150115 10:23)
     */
    public VehicleText(String vid, String tmstmp, String lat, String lon, String rt) {
        this.vid = vid;
        this.tmstmp = tmstmp;
        this.lat = lat;
        this.lon = lon;
        this.rt = rt;
    }

    /**
     * @return String representation of the VehicleText object.
     *   e.g. [vid:3483, lat:42.39548201923, lon: -87.904839823498234]
     */
    public String toString() {
        return "[vid:" + vid + ", tmstmp:" + tmstmp + ", lat:"
            + lat + ", lon:" + lon + ", rt:" + rt + "]";
    }

    public String getVid() {
        return vid;
    }

    public String getTmstmp() {
        return tmstmp;
    }

    public String getLat() {
        return lat;
    }

    public String getLon() {
        return lon;
    }

    public String getRt() { return rt; }
}
