import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.Arrays;

public class Controller {
    @FXML
    private Pane mapPane;
    @FXML
    private TabPane tabPane;
    @FXML
    private CheckBox rt14, rt15, rt21, rt23, rt27, rt43, rt52, rt53, rt54, rt55;
    private ImageView circle;
    private ImageView airportImage;
    private ImageView artImage;
    private BusDataWrapper subject;
    private ArrayList<CheckBox> allRoutes;
    //middle square pixels = 491.52,363.78
    // degree of latitude in Milwaukee is 69.03 miles (north/south) and a degree of longitude is 50.63 miles (east/west).
    //40.96 x pixels per mile
    //40.42 y pixels per mile
    private static final double MIDDLE_X = 491.52;
    private static final double MIDDLE_Y = 363.78;
    private static final double LATITUDE_CONVERSION = 69.03;
    private static final double LONGITUDE_CONVERSION = 50.63;
    private static final double PIXELS_PER_MILE_X = 40.96;
    private static final double PIXELS_PER_MILE_Y = 40.42;

    /**
     * initializes the system
     */
    public void initialize(){
        setMapObjects();
        subject = new BusDataWrapper();
        CheckBox[] temp = {rt15, rt14, rt21, rt23, rt27, rt43, rt52, rt53, rt54, rt55};
        allRoutes = new ArrayList<>(Arrays.asList(temp));
    }

    /**
     * adds a new data tab to the application window
     */
    public void addDataTab(){
        DataTab tab = new DataTab("Data Tab", subject, tabPane);
        tabPane.getTabs().add(tab);
    }

    /**
     *
     */
    @FXML
    public void trackRoutes(){
        String contextInfo = "";
        // mapTab.clear();
        for(CheckBox cb : allRoutes){
            if(cb.isSelected()){
                String routeNum = cb.getText().split(" ")[1];
                contextInfo += routeNum +",";
            }
        }
        if(!contextInfo.isEmpty()) {
            contextInfo = contextInfo.substring(0, contextInfo.length() - 1);
        }
        // mapTab.addContext(contextInfo);
    }
    private void setMapObjects() {
        ArrayList<Double> tempList;
        circle = new ImageView(new Image("File:circle.png"));
        circle.setFitHeight(50);
        circle.setFitWidth(50);
        mapPane.getChildren().add(new ImageView(new Image("File:Gridlines.png")));
        mapPane.getChildren().add(circle);
        circle.relocate(MIDDLE_X,MIDDLE_Y);
        Label cityLabel = new Label("City Hall");
        cityLabel.relocate(MIDDLE_X,MIDDLE_Y);
        mapPane.getChildren().add(cityLabel);

        //calculate airport
        tempList = calculatePlacement( 42.948085,87.904405);
        airportImage = new ImageView(new Image("File:triangle.png"));
        Label airportLabel = new Label("Airport");
        airportImage.setFitHeight(50);
        airportImage.setFitWidth(50);

        artImage = new ImageView(new Image("File:triangle.png"));
        Label artLabel = new Label("Art is here");
        artImage.setFitHeight(50);
        artImage.setFitWidth(50);

        mapPane.getChildren().add(airportImage);
        mapPane.getChildren().add(artImage);
        airportImage.relocate(tempList.get(0),tempList.get(1));
        airportLabel.relocate(tempList.get(0),tempList.get(1));
        mapPane.getChildren().add(airportLabel);
        tempList.clear();
        //Milwaukee Art Museum is 43.039290 degrees north, 87.897069 degrees west.
        tempList = calculatePlacement(43.039290,87.897069);
        artImage.relocate(tempList.get(0),tempList.get(1));
        artLabel.relocate(tempList.get(0),tempList.get(1));
        mapPane.getChildren().add(artLabel);

    }
    private ArrayList<Double> calculatePlacement(double lat, double lon){
        double cityHallLat = 43.04173;
        double cityHallLong = 87.90976;
        double adjustedLat = lat - cityHallLat;
        double adjustedLong = lon - cityHallLong;
        double mileAdjustedLat = adjustedLat * LATITUDE_CONVERSION;
        double mileAdjustedLong = adjustedLong * LONGITUDE_CONVERSION;
        double pixelAdjustedLat = mileAdjustedLat * -PIXELS_PER_MILE_Y + MIDDLE_Y;
        double pixelAdjustedLong = mileAdjustedLong * PIXELS_PER_MILE_X + MIDDLE_X;
        ArrayList cords = new ArrayList<Double>();
        cords.add(pixelAdjustedLong);
        cords.add(pixelAdjustedLat);
        return cords;

    }
    private void printBus(){

    }
    public void close(){
        Platform.exit();
    }
}
