/*
 * Josiah Yoder
 * Winter 2014-2015
 */

/**
 * Represents anything that goes wrong while trying to fetch data
 * from the website.
 */
public class RealtimeWrapperException extends Exception {
    /**
     * Creates a new exception
     * @param message My interpretation of what went wrong.
     * @param cause The original thrown exception. Can be accessed with getCause().
     */
    public RealtimeWrapperException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param message My interpretation of what went wrong.
     */
    public RealtimeWrapperException(String message) {
        super(message);
    }
}
