public interface Observer {
    void update();
    void addContext(String info);
}
