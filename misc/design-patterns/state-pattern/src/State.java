import javafx.scene.paint.Color;

public interface State {

    Color GREEN_ON = Color.web("#7CFC00");
    Color GREEN_OFF = Color.DARKGREEN;

    Color YELLOW_ON = Color.YELLOW;
    Color YELLOW_OFF = Color.web("#CCCC00"); // DARK YELLOW

    Color RED_ON = Color.RED;
    Color RED_OFF = Color.DARKRED;

    void doStateTransition();
}
