import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.shape.Circle;
import javafx.util.Duration;

public class StopLight {

    @FXML
    private Circle greenLight;

    @FXML
    private Circle yellowLight;

    @FXML
    private Circle redLight;

    private GreenLightState greenLightState;
    private YellowLightState yellowLightState;
    private RedLightState redLightState;

    private Timeline greenRedTimer;
    private Timeline yellowTimer;

    private int currentState;

    public final int GREEN_STATE = 0;
    public final int YELLOW_STATE = 1;
    public final int RED_STATE = 2;


    public StopLight(){

    }

    public void setCurrentState(int stateNumRepresentation) {
        currentState = stateNumRepresentation;
    }

    public Circle getGreenLight() {
        return greenLight;
    }

    public Circle getYellowLight() {
        return yellowLight;
    }

    public Circle getRedLight() {
        return redLight;
    }

    @FXML
    private void initialize() {
        greenLightState = new GreenLightState(this);
        yellowLightState = new YellowLightState(this);
        redLightState = new RedLightState(this);
        currentState = RED_STATE;
        greenRedTimer = new Timeline(new KeyFrame(Duration.millis(10000), event -> transition()));
        greenRedTimer.setCycleCount(Animation.INDEFINITE);
        yellowTimer = new Timeline(new KeyFrame(Duration.millis(3000), event -> transition()));
        yellowTimer.setCycleCount(Animation.INDEFINITE);
        transition();
        greenRedTimer.play();
    }

    @FXML
    public void transition() {
        switch (currentState) {
            case RED_STATE:
                greenLightState.doStateTransition();
                break;
            case GREEN_STATE:
                yellowLightState.doStateTransition();
                greenRedTimer.stop();
                yellowTimer.play();
                break;
            case YELLOW_STATE:
                redLightState.doStateTransition();
                yellowTimer.stop();
                greenRedTimer.play();
                break;
        }


    }
}
