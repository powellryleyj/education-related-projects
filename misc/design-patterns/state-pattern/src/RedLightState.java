public class RedLightState implements State {

    private StopLight stopLight;

    public RedLightState(StopLight stopLight) {
        this.stopLight = stopLight;
    }

    @Override
    public void doStateTransition() {
        stopLight.setCurrentState(stopLight.RED_STATE);
        stopLight.getGreenLight().setFill(GREEN_OFF); // is this needed?
        stopLight.getYellowLight().setFill(YELLOW_OFF);
        stopLight.getRedLight().setFill(RED_ON);
    }
}
