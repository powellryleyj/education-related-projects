public class GreenLightState implements State {

    private StopLight stopLight;

    public GreenLightState(StopLight stopLight) {
        this.stopLight = stopLight;
    }

    @Override
    public void doStateTransition() {
        stopLight.setCurrentState(stopLight.GREEN_STATE);
        stopLight.getGreenLight().setFill(GREEN_ON);
        stopLight.getYellowLight().setFill(YELLOW_OFF); // is this needed?
        stopLight.getRedLight().setFill(RED_OFF);
    }
}
