public class YellowLightState implements State {

    private StopLight stopLight;

    public YellowLightState(StopLight stopLight) {
        this.stopLight = stopLight;
    }

    @Override
    public void doStateTransition() {
        stopLight.setCurrentState(stopLight.YELLOW_STATE);
        stopLight.getGreenLight().setFill(GREEN_OFF);
        stopLight.getYellowLight().setFill(YELLOW_ON);
        stopLight.getRedLight().setFill(RED_OFF); // is this needed?
    }
}
