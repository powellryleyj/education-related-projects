package VehicleApp;


/**
 * @author Ryley
 * @version 1.0
 * @created 30-Nov-2017 2:16:49 PM
 */
public enum VehicleEnum {
	CAR_WEIGHT(2000), // in pounds
	PICKUP_WEIGHT(3000), // in pounds
	DUMPTRUCK_WEIGHT(4000), // in pounds
    PICKUP_LOAD_CAPACITY(1000), // in pounds
    DUMPTRUCK_LOAD_CAPACITY(4000), // in pounds
	CAR_WHEELS(4),
	PICKUP_WHEELS(4),
    DUMPTRUCK_SIX_WHEELS(6);

	public final int value;

	VehicleEnum(final int value) {
		this.value = value;
	}
}