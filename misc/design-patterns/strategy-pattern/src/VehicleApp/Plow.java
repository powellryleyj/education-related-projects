package VehicleApp;

import com.sun.istack.internal.NotNull;

/**
 * defines the basic model of a plow. This class uses the decorator pattern to be able to create a pickup or dumptruck
 * with a plow attachment. This class also shows the advantage and disadvantage of the decorator pattern.
 * @author Ryley Powell (powellr)
 * @version 1.0
 * @created 30-Nov-2017 2:16:49 PM
 */
public class Plow extends Truck {

    private boolean plowRaised;
    private Dumptruck truck;

    /**
     * defines the basic characteristics of a plowable truck
     * @param t - an object of class Truck or its child classes
     */
    public Plow(@NotNull Truck t) {
        super(t.getNumWheels(),
                t.getWeight(),
                t.getLoadCapacity());
        this.plowRaised = false;
    }

    /**
     * specialized constructor that stores a reference Dumptruck in this Plow; shows the disadvantage of the decorator pattern
     * due to Dumptruck containing specialized methods that can only be accessed if either A, all children classes of
     * Truck contain the methods, or B, the Plow stores a reference to the specialized class, Dumptruck, and "overrides"
     * the methods that are special to that class which ends in delegation calls to the specialized methods
     * @param t - an object of class Dumptruck
     */
    public Plow(@NotNull Dumptruck t) {
        super(t.getNumWheels(),
                t.getWeight(),
                t.getLoadCapacity());
        this.truck = t;
        this.plowRaised = false;
    }

    /**
     * returns the state of the plow
     * @return - true if raised; false otherwise
     */
    public boolean isPlowRaised(){
        return plowRaised;
    }

    /**
     * raises the plow on this Plow
     * @return - true if raised; false otherwise
     */
    public boolean raisePlow(){
        if(!isPlowRaised()) {
            plowRaised = true;
            System.out.println("The plow has been raised!");
            return true;
        } else {
            return false;
        }
    }

    /**
     * lowers the plow on this Plow
     * @return - true if raised; false otherwise
     */
    public boolean lowerPlow() {
        if(isPlowRaised()) {
            plowRaised = false;
            System.out.println("The plow has been lowered!");
            return true;
        } else {
            return false;
        }
    }

    /**
     * an adaption of getLoadRaised from Dumptruck that allows for Plow to call the method.
     * @return - the state of the load on the internal Dumptruck stored within this Plow; true if raised; false otherwise
     * @throws Exception - if the internal Dumptruck is null, an exception will be thrown to prevent any functionality
     * of this method from being leaked
     */
    public boolean getLoadRaised() throws Exception {
        if(truck != null) {
            return truck.getLoadRaised();
        } else {
            throw new Exception("The truck is not a dumptruck, therefore you cannot " +
                    "raise or lower the load on this vehicle.");
        }
    }

    /**
     * an adaption of lowerLoad from Dumptruck that allows for Plow to call the method.
     * @return - the state of the load on the internal Dumptruck stored within this Plow; true if lowered; false otherwise
     * @throws Exception - if the internal Dumptruck is null, an exception will be thrown to prevent any functionality
     * of this method from being leaked
     */
    public boolean lowerLoad() throws Exception {
        if(getLoadRaised()) {
            return truck.lowerLoad();
        } else {
            return false;
        }
    }

    /**
     * an adaption of raiseLoad from Dumptruck that allows for Plow to call the method.
     * @return - the state of the load on the internal Dumptruck stored within this Plow; true if raised; false otherwise
     * @throws Exception - if the internal Dumptruck is null, an exception will be thrown to prevent any functionality
     * of this method from being leaked
     */
    public boolean raiseLoad() throws Exception {
        if(!getLoadRaised()) {
            return truck.raiseLoad();
        } else {
            return false;
        }
    }

}
