package VehicleApp;


/**
 * defines the basic model of a dumptruck
 * @author Ryley Powell (powellr)
 * @version 1.0
 * @created 30-Nov-2017 2:16:49 PM
 */
public class Dumptruck extends Truck {

	private boolean loadRaised;

    /**
     * defines the basic characteristics of a dumptruck
     */
	public Dumptruck(){
	    super(VehicleEnum.DUMPTRUCK_SIX_WHEELS.value,
                VehicleEnum.DUMPTRUCK_WEIGHT.value,
                VehicleEnum.DUMPTRUCK_LOAD_CAPACITY.value);
	    loadRaised = false;
	}

    /**
     * returns the state of the load
     * @return - true if raised; false otherwise
     */
    public boolean getLoadRaised(){ return loadRaised; }

    /**
     * lowers the load on this dumptruck
     * @return - true if lowered; false otherwise
     */
    public boolean lowerLoad() {
        if (getLoadRaised()) {
            loadRaised = false;
            System.out.println("Load has been lowered!");
            return true;
        } else {
            return false;
        }
    }

    /**
     * raises the load on this dumptruck
     * @return - true if raised; false otherwise
     */
	public boolean raiseLoad(){
	    if(!getLoadRaised()){
	        loadRaised = true;
	        System.out.println("Load has been raised!");
	        return true;
        } else {
            return false;
        }
	}

}