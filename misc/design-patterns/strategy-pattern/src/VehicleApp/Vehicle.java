package VehicleApp;

/**
 * @author Ryley Powell (powellr)
 * @version 1.0
 * @created 30-Nov-2017 2:16:49 PM
 */
public abstract class Vehicle {

	private int numWheels;
	private int weight;
	private boolean engineOn;

    /**
     * the standard constructor of the system that defines all vehicles that inherit from this
     * @param numWheels - number of wheels on this vehicle
     * @param weight - the weight of this vehicle in pounds
     */
	public Vehicle(int numWheels, int weight){
		this.numWheels = numWheels;
		this.weight = weight;
		engineOn = false;
	}

    /**
     * returns the number of wheels on this vehicles
     * @return - number of wheels
     */
    public int getNumWheels() {
        return numWheels;
    }

    /**
     * returns the weight of this vehicle in pounds
     * @return - weight
     */
    public int getWeight() {
        return weight;
    }

	/**
	 * combines the functionality of moving forwards and backwards into one method based on the speed of the vehicle
	 * @param speed - will move forward if the value is positive, and backwards if the value is negative
	 * @param accel - shows the change in speed
	 */
	public void move(double speed, double accel){
        if(speed > 0) {
            System.out.println("You're traveling forwards with a speed of " + speed + " mph and acceleration of " +
            accel + " mph.");
        } else if(speed == 0 && accel == 0) {
            System.out.println("You're at a standstill.");
        } else if(speed == 0) {
            System.out.println("You're at a standstill, revving your engine.");
        } else if(speed < 0){
            System.out.println("You're traveling backwards with a speed of " + speed + " mph and acceleration of " +
            accel + " mph.");
        }
	}

    /**
     * starts the engine of this vehicle
     * @return - true if the engine has been started; false otherwise
     */
	public boolean start(){
        if(!engineOn) {
            engineOn = true;
            System.out.println("Engine has been started!");
            return true;
        } else {
            return false;
        }

	}

    /**
     * stops the engine of this vehicle
     * @return - false otherwise
     */
	public boolean stop(){
        if(engineOn) {
            engineOn = false;
            System.out.println("Engine has been stopped!");
            return true;
        } else {
            return false;
        }
	}
}