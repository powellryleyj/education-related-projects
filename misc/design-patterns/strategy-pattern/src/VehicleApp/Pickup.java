package VehicleApp;


/**
 * This class represents the basic model of a truck
 * @author Ryley Powell (powellr)
 * @version 1.0
 * @created 30-Nov-2017 2:16:49 PM
 */
public class Pickup extends Truck {

    /**
     * defines the basic characteristics of a pickup
     */
	public Pickup(){
		super(VehicleEnum.PICKUP_WHEELS.value,
				VehicleEnum.PICKUP_WEIGHT.value,
				VehicleEnum.PICKUP_LOAD_CAPACITY.value);
	}

}