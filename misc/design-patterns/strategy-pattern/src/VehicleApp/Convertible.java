package VehicleApp;


/**
 * defines the basic model of a convertible car
 * @author Ryley Powell (powellr)
 * @version 1.0
 * @created 30-Nov-2017 2:16:49 PM
 */
public class Convertible extends Car{

    private boolean roofRaised;

    /**
     * defines the basic characteristics of a convertible
     */
	public Convertible(){
		super();
		roofRaised = false;
	}

    /**
     * lowers the roof on this convertible
     * @return - true if the roof has been lowered; false otherwise
     */
	public boolean lowerRoof(){
	    if(roofRaised){
	        roofRaised = false;
	        System.out.println("The roof has been lowered.");
	        return true;
        } else {
            return false;
        }
	}

    /**
     * raises the roof on this convertible
     * @return - true if the roof has been raised; false otherwise
     */
	public boolean raiseRoof(){
	    if(!roofRaised) {
	        roofRaised = true;
	        System.out.println("The roof has been raised.");
	        return true;
        } else {
            return false;
        }
	}

}