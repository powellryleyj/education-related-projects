package VehicleApp;


/**
 * @author Ryley
 * @version 1.0
 * @created 30-Nov-2017 2:16:49 PM
 */
public class VehicleApp {

	public static void main(String args[]){
		Vehicle car = new Car();
		car.start();
		car.move(20, 5);
		car.move(0, 0);
		car.move(-5, 0);
		car.stop();
		System.out.println("The car has " + car.getNumWheels() + " wheels and weighs "
                + car.getWeight() + " pounds." );
		System.out.println();

		Convertible convertible = new Convertible();
		convertible.start();
		convertible.move(20,0);
		convertible.move(0,0);
		convertible.lowerRoof();
		convertible.raiseRoof();
		convertible.stop();
        System.out.println("The car has " + convertible.getNumWheels() + " wheels and weighs "
                + convertible.getWeight() + " pounds." );
        System.out.println();


        Truck pickup = new Pickup();
        pickup.start();
        pickup.move(20,0);
        pickup.move(0,0);
        pickup.stop();
        System.out.println("The truck has " + pickup.getNumWheels() + " wheels, a weight of "
            + pickup.getWeight() + " pounds, and a load capacity of "
            + pickup.getLoadCapacity() + " pounds.");
        System.out.println();


        Dumptruck dumpTruck = new Dumptruck();
        dumpTruck.start();
        dumpTruck.move(20, 5);
        dumpTruck.move(0,0);
        dumpTruck.raiseLoad();
        dumpTruck.lowerLoad();
        dumpTruck.stop();
        System.out.println("The truck has " + dumpTruck.getNumWheels() + " wheels, a weight of "
                + dumpTruck.getWeight() + " pounds, and a load capacity of "
                + dumpTruck.getLoadCapacity() + " pounds.");
        System.out.println();


        Plow pickupPlow = new Plow(pickup);
        pickupPlow.start();
        pickupPlow.move(0,0);
        try {
            pickupPlow.raiseLoad();
            pickupPlow.lowerLoad();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        pickupPlow.raisePlow();
        pickupPlow.lowerPlow();
        pickupPlow.stop();
        System.out.println("The plow has " + pickupPlow.getNumWheels() + " wheels, a weight of "
                + pickupPlow.getWeight() + " pounds, and a load capacity of "
                + pickupPlow.getLoadCapacity() + " pounds.");
        System.out.println();


        Plow dumpTruckPlow = new Plow(dumpTruck);
        dumpTruckPlow.start();
        dumpTruckPlow.move(0,0);
        try {
            dumpTruckPlow.raiseLoad();
            dumpTruckPlow.lowerLoad();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        dumpTruckPlow.raisePlow();
        dumpTruckPlow.lowerPlow();
        dumpTruckPlow.stop();
        System.out.println("The plow has " + dumpTruckPlow.getNumWheels() + " wheels, a weight of "
                + dumpTruckPlow.getWeight() + " pounds, and a load capacity of "
                + dumpTruckPlow.getLoadCapacity() + " pounds.");
        System.out.println();

	}

}