package VehicleApp;


/**
 * defines the basic model of a car
 * @author Ryley Powell (powellr)
 * @version 1.0
 * @created 30-Nov-2017 2:16:49 PM
 */
public class Car extends Vehicle {

	/**
	 * defines the basic characteristics of a car
	 */
	public Car(){
		super(VehicleEnum.CAR_WHEELS.value, VehicleEnum.CAR_WEIGHT.value);
	}

}