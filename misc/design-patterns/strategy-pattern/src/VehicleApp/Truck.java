package VehicleApp;

/**
 * This class defines the framework of a truck
 * @author Ryley Powell (powellr)
 * @version 1.0
 * @created 30-Nov-2017 2:16:49 PM
 */
public abstract class Truck extends Vehicle {

    private int loadCapacity;

    /**
     * defines the basic characteristics of a truck
     * @param wheels - the number of wheels on this truck
     * @param weight - the weight in pounds of this truck
     * @param loadCapacity - the amount of weight this truck can hold
     */
    public Truck(int wheels, int weight, int loadCapacity){
        super(wheels, weight);
        this.loadCapacity = loadCapacity;
    }

    /**
     * returns the load capacity on this truck
     * @return - loadCapacity
     */
    public int getLoadCapacity(){
        return loadCapacity;
    }

}
