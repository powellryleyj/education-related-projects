package edu.msoe.sefocus.core;

import java.io.IOException;

public class WallSELineFollower implements iNavigationController, iDiagnosticsController {
		private iNetworkController network;

		private static int START_WHITE_CALIBRATION = 0x40000000;
		private static int START_BLACK_CALIBRATION = 0x20000000;
		private static int START_LINE_FOLLOWING = 0x10000000;
		private static int STOP_LINE_FOLLOWING =  0x08000000;

	private static int UPDATE_KP =  0x04000000;
	private static int UPDATE_KD =  0x02000000;
	private static int UPDATE_KI =  0x01000000;







	private static int DISPLAY_THREAD_DIAGNOSTICS = 0x20000000;
		private static int RESET_THREAD_DIAGNOSTICS =   0x10000000;
		
		

		public WallSELineFollower(iNetworkController nw) {
			network = nw;
		}
	
	@Override
	public void startCalibrationWhite() {
		try {
			network.sendMessage(iNavigationController.NAVIGATION_CONTROL, START_WHITE_CALIBRATION);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void startCalibrationBlack() {
		try {
			network.sendMessage(iNavigationController.NAVIGATION_CONTROL, START_BLACK_CALIBRATION);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void startLineFollowing() {
		try {
			network.sendMessage(iNavigationController.NAVIGATION_CONTROL, START_LINE_FOLLOWING);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void stopLineFollowing() {
		try {
			network.sendMessage(iNavigationController.NAVIGATION_CONTROL, STOP_LINE_FOLLOWING);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void setKp(double value) {
		// Start by adding 1000 to the value.  This will force it to be positive.
		double temp = value + 1000;

		// Now multiply by 1000 to get ride of the decimal.
		temp = temp * 1000;

		// Now convert to an int.
		int transmissionValue = (int)Math.round(temp);

		transmissionValue = transmissionValue | UPDATE_KP;

		try {
			network.sendMessage(iNavigationController.NAVIGATION_CONTROL, transmissionValue);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void setKi(double value) {
		// Start by adding 1000 to the value.  This will force it to be positive.
		double temp = value + 1000;

		// Now multiply by 1000 to get ride of the decimal.
		temp = temp * 1000;

		// Now convert to an int.
		int transmissionValue = (int)Math.round(temp);

		transmissionValue = transmissionValue | UPDATE_KI;

		try {
			network.sendMessage(iNavigationController.NAVIGATION_CONTROL, transmissionValue);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void setKd(double value) {
		// Start by adding 1000 to the value.  This will force it to be positive.
		double temp = value + 1000;

		// Now multiply by 1000 to get ride of the decimal.
		temp = temp * 1000;

		// Now convert to an int.
		int transmissionValue = (int)Math.round(temp);

		transmissionValue = transmissionValue | UPDATE_KD;

		try {
			network.sendMessage(iNavigationController.NAVIGATION_CONTROL, transmissionValue);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void displayDiagnostics() {
		try {
			network.sendMessage(iDiagnosticsController.DIAGNOSTICS_CONTROL, DISPLAY_THREAD_DIAGNOSTICS);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void resetDiagnostics() {
		try {
			network.sendMessage(iDiagnosticsController.DIAGNOSTICS_CONTROL, RESET_THREAD_DIAGNOSTICS);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
