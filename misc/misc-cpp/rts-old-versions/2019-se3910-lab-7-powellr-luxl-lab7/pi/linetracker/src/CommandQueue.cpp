/**
 * @file CommandQueue.cpp.cpp
 * @author  Walter Schilling (schilling@msoe.edu)
 * @version 1.0
 *
 * @section LICENSE
 *
 *
 * This code is developed as part of the MSOE SE3910 Real Time Systems course,
 * but can be freely used by others.
 *
 * SE3910 Real Time Systems is a required course for students studying the
 * discipline of software engineering.
 *
 * This Software is provided under the License on an "AS IS" basis and
 * without warranties of any kind concerning the Software, including
 * without limitation merchantability, fitness for a particular purpose,
 * absence of defects or errors, accuracy, and non-infringement of
 * intellectual property rights other than copyright. This disclaimer
 * of warranty is an essential part of the License and a condition for
 * the grant of any rights to this Software.
 *
 * @section DESCRIPTION
 * Implementation for the Command Queue
 * This class, the CommandQueue, allows a user to enque a set of commands for a device. The commands must be simple integers,
 * but they can have bitmapped representations.  The queue can hold as many items as is necessary.
 */

#include "CommandQueue.h"
#include <semaphore.h>

CommandQueue::CommandQueue() {
	/**
	 * Initialize the semaphore to be a counting sem with nothing on it.
	 **/
	sem_init(&queueCountSemaphore, 0, 0);

}

CommandQueue::~CommandQueue() {
	/**
	 * Nothing is needed here for now, as there is nothing dynamically created that we need to clean up afterwards.
	 */
}

/**
 * This method will dequeue the next command from the queue.  This method will block if there are no items on the queue.
 * @return The return will be the next command that is to be processed.
 */
int CommandQueue::dequeue() {
	int retValue = 0;

	/**
	 * Block if there is nothing on the queue until something is enqueued.
	 */
	sem_wait(&queueCountSemaphore);
	{
		/**
		 * Lock the queue and dequeue an item from it.
		 */
		std::lock_guard<std::mutex> guard(queueMutex);

		/**
		 * Obtain the first item from the queue and then dispose of it.
		 */
		retValue = commandQueueContents.front();
		commandQueueContents.pop();
	}
	return retValue;
}

/**
 * This method will indicate whether or not the queue has an item that is ready to be dequeued.
 * @return true if there is an item on the queue.  False otherwise.
 */
bool CommandQueue::hasItem() {
	return commandQueueContents.size() > 0;
}

/**
 * This method will enqueue a command on the queue.  Any number can be enqueued as a command.
 * Enqueueing a command will cause a thread blocked waiting for a command to be unblocked.
 * @param value This is the value that is to be enqueued.
 */
void CommandQueue::enqueue(int value) {
	{
		// Lock the queue.
		std::lock_guard<std::mutex> guard(queueMutex);
		// Place the given item on the end of the queue.
		commandQueueContents.push(value);
		// Indicate that something has been enqueued through the semaphore.
		sem_post(&queueCountSemaphore);
	}
}
