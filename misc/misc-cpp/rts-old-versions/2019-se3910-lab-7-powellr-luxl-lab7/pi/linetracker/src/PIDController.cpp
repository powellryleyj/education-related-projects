/**
 * @file PIDCOntroller.h
 * @author  Walter Schilling (schilling@msoe.edu)
 * @version 1.0
 *
 * @section LICENSE
 *
 *
 * This code is developed as part of the MSOE SE3910 Real Time Systems course,
 * but can be freely used by others.
 *
 * SE3910 Real Time Systems is a required course for students studying the
 * discipline of software engineering.
 *
 * This Software is provided under the License on an "AS IS" basis and
 * without warranties of any kind concerning the Software, including
 * without limitation merchantability, fitness for a particular purpose,
 * absence of defects or errors, accuracy, and non-infringement of
 * intellectual property rights other than copyright. This disclaimer
 * of warranty is an essential part of the License and a condition for
 * the grant of any rights to this Software.
 *
 * @section DESCRIPTION
 * This defines the implementation of the PID controller.
 */
#include "PIDController.h"

/**
 * This is the default constructor.
 */
PIDController::PIDController() {
	/**
	 * It currently does nothing.
	 */
}

/**
 * This is the default destructor.
 */
PIDController::~PIDController() {
	/**
	 * This is the destructor.  It currently does nothing.
	 */

}

/**
 * This method will set the value that the PID controller is attempting to maintain.
 * @param setValue This si the value that the system is to be set to.
 */
void PIDController::setSetValue(double setValue) {
	this->setValue = setValue;
}

/**
 * This method will set the proportional gain for the system.
 * @param kp The proportional gain.
 */
void PIDController::setKp(double kp) {
	this->kp = kp;
}

/**
 * This method will set the integral gain for the system.
 * @param ki This si the integral gain.
 */
void PIDController::setKi(double ki) {
	this->ki = ki;
}

/**
 *
 * This method will set the derivative gain.
 * @param kd This is the derivative gain.
 */
void PIDController::setKd(double kd) {
	this->kd = kd;
}

/**
 * This method will update the output value.
 * @param currentValue This is the current, measured value from the system.
 * @return This is the control value to feed into the system.
 */
double PIDController::updateOutput(double currentValue) {
	/**
	 * 1. Calculate the error, which is the difference between the setpoint and the desired value.
	 */
	double error = currentValue - this->setValue;
	double pTerm = kp * error;

	/**
	 * 2. Now integrate the error.
	 */
	this->totalError += error;

	/**
	 * 3. Calculate the integral term.
	 */
	double iTerm = ki * this->totalError;

	/**
	 * 4. Now calculate the change in error.
	 */
	double delta = error - this->previousError;
	double dTerm = delta * kd;

	/**
	 * 5. Now calculate the output.
	 */
	this->output = pTerm + dTerm + iTerm;

	/**
	 * 6. Now keep track of the previous error for future reference.
	 */
	this->previousError = error;

	return this->output;
}

/**
 * This method will obtain the output for the given system.
 * @return The return is the output value.
 */
double PIDController::getOutput() {
	return this->output;
}

/**
 * This method will reset the controller to the default values.
 */
void PIDController::reset() {
	previousError = 0.0;
	totalError = 0.0;
	output = 0.0;
}
