/**
 * @file PWMManager.h
 * @author  Walter Schilling (schilling@msoe.edu)
 * @version 1.0
 *
 * @section LICENSE
 *
 *
 * This code is developed as part of the MSOE SE3910 Real Time Systems course,
 * but can be freely used by others.
 *
 * SE3910 Real Time Systems is a required course for students studying the
 * discipline of software engineering.
 *
 * This Software is provided under the License on an "AS IS" basis and
 * without warranties of any kind concerning the Software, including
 * without limitation merchantability, fitness for a particular purpose,
 * absence of defects or errors, accuracy, and non-infringement of
 * intellectual property rights other than copyright. This disclaimer
 * of warranty is an essential part of the License and a condition for
 * the grant of any rights to this Software.
 *
 * @section DESCRIPTION
 * This file defines the interface for the PWM manager.  The PWM manager allows us to control the speed of motors using PWM approaches.
 */

#ifndef PWMMANAGER_H_
#define PWMMANAGER_H_

#include "PeriodicTask.h"
#include "GPIO.h"
#include <map>

class PWMManager: public PeriodicTask {
private:
	/**
	 * This structure maps a GPIO pin to a given duty cycle.  The given pin will be turned on and off as is necessary.
	 */
	struct GPIOPWMControlMapStruct {
		/**
		 * This is the GPIO pin that is to be PWM controlled.
		 */
		se3910RPi::GPIO *gpioPin;
		/**
		 * This is the duty cycle for the given pin.
		 */
		uint32_t dutyCycle;
		/**
		 * This value indicates if the PWM value has been changed or not.
		 */
		bool changed;
		/**
		 * This is the duty cycle that is to be applied the next time the period for the PWM rolls over.
		 */
		uint32_t newDutyCycle;

	};

	/**
	 * This is a map which will map a given control map to a given GPIO number, allowing the given pin to be PWM controlled.
	 */
	std::map<int, GPIOPWMControlMapStruct> GPIOControlMap;
	/**
	 * This is the current count for all PWM channels.  It ranges between 0 and 100.
	 */
	uint32_t currentCount;

public:
	/**
	 * This method will instantiate a new PWM controller.
	 * @param threadName This si the name of the thread.
	 * @param period  This is the period task rate for the task, given in microseconds.
	 */
	PWMManager(std::string threadName, uint32_t period);
	/**
	 * This is the destructor that will destroy the given instance of the PWM manager.
	 */
	virtual ~PWMManager();
	/**
	 * This method will add a given GPIO pin to the PWM controller, making the pin turn on and off when the times expire.
	 * @param gpioNumber This si the GPIO pin number.
	 * @param instance This is the GPIO instance associated with the pin.
	 * @param dutyCycle This si the duty cycle for the given pin.
	 */
	void addPWMPin(int gpioNumber, se3910RPi::GPIO* instance,
			uint32_t dutyCycle);
	/**
	 * This method will remove a given GPIO pin from being controlled via PWM.
	 * @param gpioNumber This is the GPIO pin that is no longer to be PWM controlled.
	 */
	void removePWMPin(int gpioNumber);
	/**
	 * This method will adjust the duty cycle for the given GPIO pin.
	 * @param gpioNumber This is the pin number.
	 * @param dutyCycle This is the new duty cycle.
	 */
	void setDutyCycle(int gpioNumber, uint32_t dutyCycle);
	/**
	 * This is the task method that will periodically be invoked.
	 */
	virtual void taskMethod();
};

#endif /* PWMMANAGER_H_ */
