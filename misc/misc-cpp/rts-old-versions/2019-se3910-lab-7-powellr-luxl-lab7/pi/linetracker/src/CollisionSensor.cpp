
#include "CollisionSensor.h"
#include "NetworkCommands.h"

CollisionSensor::CollisionSensor(int sensorPin, CommandQueue* queue, std::string threadname, uint32_t period) : PeriodicTask(threadname, period) {
	this->sensorPin = new GPIO(sensorPin, GPIO::DIRECTION::GPIO_IN);
	this->queue = queue;
}

CollisionSensor::~CollisionSensor() {
	delete this->sensorPin;
}

void CollisionSensor::taskMethod() {
	if(this->sensorPin->getValue() == se3910RPi::GPIO::VALUE::GPIO_LOW) {
		this->obstructed = true;
		this->queue->enqueue(COLLISIONBITMAP | COLLISION_SENSED);
	} else if (this->obstructed == true && this->sensorPin->getValue() == se3910RPi::GPIO::VALUE::GPIO_HIGH) {
		this->obstructed = false;
		this->queue->enqueue(COLLISIONBITMAP | COLLISION_CLEARED);
	}
}

bool CollisionSensor::isObstructed() {
	return this->obstructed;
}

