/**
 * @file NNavigationUnit.h
 * @author  Walter Schilling (schilling@msoe.edu)
 * @version 1.0
 *
 * @section LICENSE
 *
 * This code is developed as part of the MSOE SE3910 Real Time Systems course,
 * but can be freely used by others.
 *
 * SE3910 Real Time Systems is a required course for students studying the
 * discipline of software engineering.
 *
 * This Software is provided under the License on an "AS IS" basis and
 * without warranties of any kind concerning the Software, including
 * without limitation merchantability, fitness for a particular purpose,
 * absence of defects or errors, accuracy, and non-infringement of
 * intellectual property rights other than copyright. This disclaimer
 * of warranty is an essential part of the License and a condition for
 * the grant of any rights to this Software.
 *
 * @section DESCRIPTION
 *      This is the implementation file for the nav unit.
 */

#include "NavigationUnit.h"
#include "RobotCfg.h"
#include "TaskRates.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include "NetworkCommands.h"
#include "ADReader.h"

/**
 * This method will instantiate a new instance of a navigation unit.
 * @param driveCommandQueue This is a pointer to the command queue that is used to send driving commands.
 * @param instructionsQueue This is the queue from which the Nav unit will receive commands from the network.
 * @param threadName This is the name of the given thread.
 * @param taskRate This is the rate at which the task is to execute, given in us.
 */
NavigationUnit::NavigationUnit(CommandQueue *driveCommandQueue,
		CommandQueue *instructionsQueue, std::string threadName,
		uint32_t taskRate) :
		PeriodicTask(threadName, taskRate) {

	/**
	 * Instantiate a data reader.
	 */
	dataReader = new ADReader(CHIPSEL, IOCLK, ADDR, DOUT, "AD Line Sensor",
	AD_TASK_RATE);

	/**
	 * Instantiate a PID controller.
	 */
	myPIDCtrl = new PIDController();

	/**
	 * Store pointers to the drive and instruction queues.
	 */
	this->driveQueue = driveCommandQueue;
	this->instructionQueue = instructionsQueue;

	/**
	 * Set up the initial pid values, (50 40, 2 for Kp, Kd, and Ki).
	 */
	myPIDCtrl->setKp(50.0);
	myPIDCtrl->setKd(40.0);
	myPIDCtrl->setKi(2.0);

}
/**
 * This is the destructor which cleans up.
 */
NavigationUnit::~NavigationUnit() {
	/**
	 * Delete the data Reader and pid controller.
	 */
	delete dataReader;
	delete myPIDCtrl;
}

/**
 * This is the task method for the class.  It contains the code that is to run periodically on the given thread.  This method will:
 * 1. Determine if there is anything on the queue that needs to be handled.  (Note: This is a check, not a block like other classes have.)
 * 1a. If there is something, handle it in the following manner:
 * 1aa. If it is a white calibration event, stop the robot motors and start a white calibration.
 * 1ab. If it is a black calibration event, stop the motors and start a black calibration.
 * 1ac. If it is a line following start command, enqueue for the motors to go forward and set line following to be active.
 * 1ad. If it is a command to stop line following, stop the robot from any motion and disable line following.
 * 2. If line following is active,
 * 2a. Read the 5 sensors, determining whether each one is black or white.  If it is black, add a value of the sensor number - 2 to the sum.  Ideally, if the black line is in the center, the sum will be 0.
 * 2b. Based on the findings, either spin the robot left or right, turn the robot left or right, or make the robot go straight.
 */
void NavigationUnit::taskMethod() {
	/**
	 * This is specific details for the method.
	 * 1. If there is anything in the instruction queue.
	 */

	if (instructionQueue->hasItem()) {
		/**
		 * 1.1 Dequeue the first item.
		 */
		int command = instructionQueue->dequeue();

		/**
		 * 1.2 If the command is a white cal command, stop the robot and calibrate the white.
		 */
		if (command == START_WHITE_CALIBRATION) {
			/**
			 * 1.2.1 Stop the robot from any motion.
			 */
			driveQueue->enqueue(MOTORDIRECTIONBITMAP | STOP);

			/**
			 * 1.2.2 Perform the white calibration.
			 */
			calibrateWhite();
		}
		/**
		 * 1.3 Else If the command is a black cal command, stop the robot and calibrate the black.
		 */
		else if (command == START_BLACK_CALIBRATION) {
			/**
			 * 1.3.1 Stop the robot from any motion.
			 */

			driveQueue->enqueue(MOTORDIRECTIONBITMAP | STOP);
			/**
			 * 1.3.2 Perform the black calibration.
			 */

			calibrateBlack();
		}
		/**
		 * 1.4 If the command is to start line following, reset the pid, start the robot driving forward, and set line following to be active.
		 */

		else if (command == START_LINE_FOLLOWING) {
			/**
			 * 1.4.1 Reset the pid controller.
			 */
			myPIDCtrl->reset();
			/**
			 * 1.4.2 Enqueue a forward direction commend..
			 */
			driveQueue->enqueue(MOTORDIRECTIONBITMAP | FORWARD);
			/**
			 * 1.4.3 Set the line following to be true.
			 */
			lineFollowingActive = true;
		}
		/**
		 * 1.5 If the command is to stop line following, reset the pid, stop the robot, and set line following to be false.
		 */

		else if (command == STOP_LINE_FOLLOWING) {
			/**
			 * 1.5.1 Stop the robot from any motion.
			 */

			driveQueue->enqueue(MOTORDIRECTIONBITMAP | STOP);

			/**
			 * 1.5.2 Set the line following to be true.
			 */
			lineFollowingActive = false;
		}

		/**
		 * 1.6 If the command is an update to a PID parameter, call the handlePIDCalibrationUpdate method.
		 */

		else if (((command & UPDATE_KP) == UPDATE_KP)
				|| ((command & UPDATE_KD) == UPDATE_KD)
				|| ((command & UPDATE_KI) == UPDATE_KI)) {
			handlePIDCalibrationUpdate(command);
		}
	}

	/**
	 * 2.0 If line following is active,
	 */

	if (lineFollowingActive) {
		int result = 0;
		int count = 0;

		/**
		 * 2.1 Obtain the weighted average for the line location.
		 */

		for (unsigned int index = 0; index < 5; index++) {
			/**
			 * 2.1.1 Read the 5 sensors and calculate a sum.  The sum is the (index-2), where the indices are 0, 1, 2, 3, and 4 for the 5 sensors.
			 * While doing this, also determine how many sensors are black.
			 */
			if (dataReader->readChannelValue(index) < thresholds[index]) {
				/**
				 *
				 *Add to the value because the segment is "black."  Also increment the count of black segments by 1.
				 */
				result += (index - 2);
				count++;
			} else {
				// Do nothing, because the area under the sensor is white.
			}
		}

		/**
		 * 2.2 Calculate the c value.  The cvalue is the sum divided by the number of darkened segments or 0 if all segments are white.
		 * The value can range between -2 and 2.
		 */
		double cValue = count != 0 ? (result / ((double) count)) : 0.0;

		/**
		 * 2.3 Make sure the pid cpontroller setpoint is 0, for straight ahead following the line.
		 */
		myPIDCtrl->setSetValue(0);

		/**
		 * 2.4 Now run the cValue through the pid controller.
		 */
		double aValue = myPIDCtrl->updateOutput(cValue);

		/**
		 * 2.5 Bounds check the value coming back from the PID controller.  If it is greater than 100, set it to 100.  If it is less than -100 set it to -100.
		 *
		 */
		if (aValue > 100) {
			aValue = 100;
		}
		if (aValue < -100) {
			aValue = -100;
		}


		/**
		 * 2.6 Determine what to do based on the following relationships.
		 * aValue < -75 : spin left
		 * -75 < aValue < -25 : go forward left.
		 * 25 < aValue < 75 : go forward right
		 * avalue > 75 : spin right
		 * Otherwise: Go straight forward
		 *
		 */

		if (-75 < aValue && aValue < -25) {
			driveQueue->enqueue(MOTORDIRECTIONBITMAP | FORWARD | LEFT);
		} else if (75 > aValue && aValue > 25) {
			driveQueue->enqueue(MOTORDIRECTIONBITMAP | FORWARD | RIGHT);
		} else if (aValue < -75) {
			driveQueue->enqueue(MOTORDIRECTIONBITMAP | LEFT);
		} else if (aValue > 75) {
			driveQueue->enqueue(MOTORDIRECTIONBITMAP | RIGHT);
		} else {
			// Go straight.
			driveQueue->enqueue(MOTORDIRECTIONBITMAP | FORWARD);
		}
	}
}

/**
 * This method will be invoked when there is an update to the PID calibration for one of the 3 PID controller configurable parameters.
 * @param parameter This is the parameter.  This PID parameter is sent as well as a tag that determines which parameter it is.  The parameter
 * is sent as an integer whereby the configuration parameter is multiplied by 1000 and then has 1000 added to it.  This allows values between
 * +/-1000.000 to be sent simply as an integer.
 *
 */
void NavigationUnit::handlePIDCalibrationUpdate(uint32_t parameter) {

	/**
	 * 1. Remove the portion of the command which determined whether it is KP, KI, or KD that is being updated via masking.  This will result in just the raw value.
	 */
	int fvalue = parameter & ~(UPDATE_KP | UPDATE_KD | UPDATE_KI);

	/**
	 * 2. Scale the raw value accordingly by dividing the integer value by 1000 and subtracting 1000 from the value.
	 */
	double calValue = (fvalue / 1000.0) - 1000.0;

	/**
	 * 3. Based upon whether it is KP, KI, or KD, update the appropriate value in the PID controller.
	 */
	if ((parameter & UPDATE_KP) == UPDATE_KP) {
		myPIDCtrl->setKp(calValue);
	} else if ((parameter & UPDATE_KD) == UPDATE_KD) {
		myPIDCtrl->setKd(calValue);
	} else if ((parameter & UPDATE_KI) == UPDATE_KI) {
		myPIDCtrl->setKi(calValue);
	}
}

/**
 * This method will calibrate the sensors when over a white area of the track.  The method will:
 * 	Read each channel 25 times and store the values into 5 temporary arrays, sleeping 100ms between reads.
 * 	Sort the values into ascending order
 * 	Select the median value for each of the 5 channels.
 * 	Update the thresholds
 */
void NavigationUnit::calibrateWhite() {
	// Read each channel 25 times and store.
	unsigned int counter;
	unsigned int channel;

	std::vector<uint16_t> channelValues[5];

	for (counter = 0; counter < 25; counter++) {
		for (channel = 0; channel < 5; channel++) {
			// Read the channel into the array.
			channelValues[channel].push_back(dataReader->readChannelValue(channel));
		}
		// Now go to sleep for 100 ms until we read the next set.
		std::this_thread::sleep_for(std::chrono::milliseconds((100)));
	}

	// Now that we have all of the data, lets process it.
	for (channel = 0; channel < 5; channel++) {
		// Start by sorting the arrays.
		std::sort(channelValues[channel].begin(), channelValues[channel].end());

		// Now set the whites to be the middle value.
		whites[channel] = channelValues[channel][12];
	}

	// Now set the new thresholds.
	setThresholds();
}

/**
 * This method will calibrate the sensors when over a black area of the track.  The method will:
 * 	Read each channel 25 times and store the values into 5 temporary arrays, sleeping 100ms between reads.
 * 	Sort the values into ascending order
 * 	Select the median value for each of the 5 channels.
 * 	Update the thresholds
 */
void NavigationUnit::calibrateBlack() {
	// Read each channel 25 times and store.
	unsigned int counter;
	unsigned int channel;

	std::vector<uint16_t> channelValues[5];

	for (counter = 0; counter < 25; counter++) {
		for (channel = 0; channel < 5; channel++) {
			// Read the channel into the array.
			channelValues[channel].push_back(dataReader->readChannelValue(channel));
		}
		// Now go to sleep for 100 ms until we read the next set.
		std::this_thread::sleep_for(std::chrono::milliseconds((100)));
	}

	// Now that we have all of the data, lets process it.
	for (channel = 0; channel < 5; channel++) {
		// Start by sorting the arrays.
		std::sort(channelValues[channel].begin(), channelValues[channel].end());

		// Now set the whites to be the middle value.
		blacks[channel] = channelValues[channel][12];
	}

	// Now set the new thresholds.
	setThresholds();

}
/**
 * This method will calculate the threshold between white and black segments on the board.  It will do this by:
 * Calculating the difference between the median black and white value previously stored for each of the 5 channels.
 * Divide that difference by 3.
 * Adding that difference to the median black threshold.
 * Print out the black, white, and threshold values for each sensor.
 */
void NavigationUnit::setThresholds() {
	for (unsigned int index = 0; index < 5; index++) {
		thresholds[index] = whites[index] - blacks[index];
		thresholds[index] = blacks[index] + (thresholds[index]) / 3;
		std::cout << index << " " << blacks[index] << " " << thresholds[index] 	<< " " << whites[index] << "\n";
	}
}

/**
 * This method will stop the execution of the given class.
 */
void NavigationUnit::stop() {
	dataReader->stop();
	PeriodicTask::stop();
}

/**
 * This method will block waiting for the given thread to terminate before continuing.  This should be overridden if any child class
 * has it's own runnable objects encapsulated within it.
 *
 */
void NavigationUnit::waitForShutdown() {
	dataReader->waitForShutdown();
	PeriodicTask::waitForShutdown();
}

/**
 * This method will start up any runnable objects which are contained within a class that implements the RUnnable interface.
 * If there are no other objects that are runnable, there is no need to override this method.  However, if a child class
 * contains an instance of a runnable object, then this method must be overridden in the derived class.
 *
 */
void NavigationUnit::startChildRunnables() {
	dataReader->start();
}

