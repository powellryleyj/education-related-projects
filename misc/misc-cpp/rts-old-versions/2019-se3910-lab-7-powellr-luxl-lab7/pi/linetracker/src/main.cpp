/**
 * @file main.cpp
 * @author  Walter Schilling (schilling@msoe.edu)
 * @version 1.0
 *
 * @section LICENSE This is a piece of a template solution to the lab.
 *
 *
 * @section DESCRIPTION
 *      This is the main method for the basic robot.  It will control the simplest operation of the robot over the network.
 */
#include <pthread.h>
#include <iostream>
#include <unistd.h>
#include <cstdlib>
#include <thread>
#include "GPIO.h"
#include "RobotController.h"
#include "NetworkCommands.h"
#include "RobotCfg.h"
#include "PWMManager.h"
#include "TaskRates.h"
#include "DistanceSensor.h"
#include "TaskRates.h"
#include "DiagnosticManager.h"
#include "NetworkManager.h"
#include "CollisionSensingRobotController.h"
#include "NavigationUnit.h"

using namespace std;

/**
 * This is the main program.  It will instantiate a network manager and robot controller as well as a Command Queue.
 * It will then block until the user enters a message on the console.
 * This will then cause it to shutdown the other classes and wait for their threads to terminate.
 */
int main(int argc, char* argv[]) {
	CommandQueue *myQueue[NUMBER_OF_QUEUES];
	CommandQueue *driveQueue = myQueue[0] = new CommandQueue();
	CommandQueue *diagQueue = myQueue[1] = new CommandQueue();
	CommandQueue *navigationCommandQueue = myQueue[2] = new CommandQueue();


	//Declare a pair of players
	NetworkReceptionManager nm(9090, myQueue, "NetworkManager");
	NetworkTransmissionManager nt(&nm, "Network Transmission");

	PWMManager pwm("PWM Manager", PWM_MANAGER_TASK_RATE);
	se3910RPiHCSR04::DistanceSensor *ds = new se3910RPiHCSR04::DistanceSensor(22, 27, "Distance Sensor", DISTANCE_SENSOR_TASK_RATE);

	DiagnosticManager dm(diagQueue, &nt, ds, "Diagnostic Manager", DIAGNOSTIC_TASK_MANAGER_TASK_RATE);
	CollisionSensingRobotController mc(driveQueue, pwm,  DL, DR, "Col. Sensing Ctrl");
	NavigationUnit navUnit(driveQueue, navigationCommandQueue, "Nav Unit", NAV_TASK_RATE);

	// Start each of the two threads up.
	nm.start();
	nt.start();

	mc.start();
	pwm.start();
	ds->start(54);
	dm.start();
	navUnit.start();

	char msg[1024];
	do {
		cin >> msg;
		cout << msg;

		if (msg[0]=='P')
		{
			ds->printThreads();
		}
	}while (msg[0]!='Q');

	navUnit.stop();
	dm.stop();
	ds->stop();
	pwm.stop();
	mc.stop();
	nt.stop();
	nm.stop();



	// Wait for the threads to die.
	navUnit.waitForShutdown();
	dm.waitForShutdown();
	ds->waitForShutdown();
	pwm.waitForShutdown();
	mc.waitForShutdown();
	nt.waitForShutdown();
	nm.waitForShutdown();

	delete myQueue[0];
	delete myQueue[1];
	delete myQueue[2];
	delete ds;
}



