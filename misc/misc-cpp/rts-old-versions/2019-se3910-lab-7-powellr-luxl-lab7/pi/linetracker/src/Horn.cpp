/*
 * Horn.cpp
 *
 *  Created on: Oct 23, 2019
 *      Author: se3910
 */

#include <unistd.h>
#include "Horn.h"
#include "TaskRates.h"

Horn::Horn(int gpioPin, std::string threadName, uint32_t taskRate) : PeriodicTask(threadName, taskRate) {
	this->hornPin = new se3910RPi::GPIO(gpioPin, se3910RPi::GPIO::GPIO_OUT);
}

Horn::~Horn() {
	delete hornPin;
}

void Horn::soundHorn() {
	this->hornCount = 0;
	this->length = HORN_TASK_RATE;
	this->repetitionTime = 0;

}

void Horn::pulseHorn(int length, int period) {
	this->hornCount = 0;
	this->length = length * 1000;
	this->repetitionTime = (period - length) * 1000;
}

void Horn::silenceHorn() {
	this->hornCount = -1;
}

void Horn::taskMethod() {
//	needs to be rewritten based on re-reading of Deoxygen docs
	if(this->hornCount >= 0) { // need to make sure that horn will always sound for set values
		this->hornPin->setValue(se3910RPi::GPIO::VALUE::GPIO_HIGH);
		usleep(this->length);
		if(this->repetitionTime > 0) {
			this->hornPin->setValue(se3910RPi::GPIO::VALUE::GPIO_LOW);
			usleep(this->repetitionTime);
			this->hornCount += this->repetitionTime;
		}
		this->hornCount += this->length;
//		this->hornCount += this->taskPeriod; // may need to put sleep for pulse off time.
	} else {
		this->hornPin->setValue(se3910RPi::GPIO::VALUE::GPIO_LOW);
	}

//  if (this->hornCount == -1) {
//	  this->hornPin->setValue(se3910RPi::GPIO::VALUE::GPIO_LOW);
//  } else if (this->hornCount == 0) {
//	  int i = 0;
//	  while(i < (this->taskPeriod / this->repetitionTime)) {
//	    this->hornPin->setValue(se3910RPi::GPIO::VALUE::GPIO_HIGH);
//	    usleep(length);
//	    this->hornPin->setValue(se3910RPi::GPIO::VALUE::GPIO_LOW);
//	    usleep(this->repetitionTime);
//	    i = i + 1;
//	  }
//  }

}
