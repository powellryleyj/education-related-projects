
#ifndef COLLISIONSENSOR_H_
#define COLLISIONSENSOR_H_

#include "PeriodicTask.h"
#include "GPIO.h"
#include "CommandQueue.h"

using namespace se3910RPi;

class CollisionSensor : public PeriodicTask {
private:
	GPIO* sensorPin;
	bool obstructed = false;
	CommandQueue* queue;

public:
	CollisionSensor(int, CommandQueue*, std::string, uint32_t);
	~CollisionSensor();
	void taskMethod();
	bool isObstructed();

};

#endif
