
#include "CollisionSensingRobotController.h"
#include "TaskRates.h"
#include "RobotCfg.h"
#include "NetworkCommands.h"

CollisionSensingRobotController::CollisionSensingRobotController(CommandQueue* queue, PWMManager& pwmInstance, int leftSensorPin, int rightSensorPin, std::string threadname) : RobotController(queue, pwmInstance, threadname) {
	this->lcs = new CollisionSensor(leftSensorPin, queue, "Left Collision Sensor", COLLISION_SENSING_TASK_RATE);
	this->rcs = new CollisionSensor(rightSensorPin, queue, "Right Collision Sensor", COLLISION_SENSING_TASK_RATE);
	this->robotHorn = new Horn(BUZZER, "Robot Horn", HORN_TASK_RATE);
	this->previousOperation = 0;
}

CollisionSensingRobotController::~CollisionSensingRobotController() {
	delete this->lcs;
	delete this->rcs;
	delete this->robotHorn;
}

void CollisionSensingRobotController::run() {
	int command = this->referencequeue->dequeue();

	// process motion commands
	if((command & MOTORDIRECTIONBITMAP) == MOTORDIRECTIONBITMAP) {
		int motionCommand = command & MOTORDIRECTIONS;
		this->processMotionControlCommand(motionCommand);
		if((motionCommand & BACKWARD) == BACKWARD) {
			this->robotHorn->pulseHorn(300, 600); // pulse horn; values given in milliseconds
		} else {
			this->robotHorn->silenceHorn();
		}
	} else if((command & SPEEDDIRECTIONBITMAP) == SPEEDDIRECTIONBITMAP) {
		this->processSpeedControlCommand(command - SPEEDDIRECTIONBITMAP);
	} else if((command & COLLISIONBITMAP) == COLLISIONBITMAP) {
		int collisionCommand = command & COLLISIONBITMAP;
		bool rightSensorObstructed = this->rcs->isObstructed();
		bool leftSensorObstructed = this->lcs->isObstructed();

		if(((this->currentOperation & FORWARD) == FORWARD) && (collisionCommand == COLLISION_SENSED)) {
			this->previousOperation = this->currentOperation;
			if(rightSensorObstructed && leftSensorObstructed) {
				this->processMotionControlCommand(STOP);
				this->robotHorn->soundHorn();
			} else if(rightSensorObstructed && !leftSensorObstructed) {
				this->processMotionControlCommand(BACKWARD | RIGHT);
				this->robotHorn->pulseHorn(300, 600); // pulse horn; values given in milliseconds
			} else if(!rightSensorObstructed && leftSensorObstructed) {
				this->processMotionControlCommand(BACKWARD | LEFT);
				this->robotHorn->pulseHorn(300, 600); // pulse horn; values given in milliseconds
			}
		} else if((collisionCommand == COLLISION_CLEARED) && !rightSensorObstructed && !leftSensorObstructed) {
			this->processMotionControlCommand(this->previousOperation); // might cause a bug if robot gets COLLISION_SENSED command while not moving forward
		}

	}
}

void CollisionSensingRobotController::stop() {
	RobotController::stop();
	this->lcs->stop();
	this->rcs->stop();
	this->robotHorn->stop();

}

void CollisionSensingRobotController::waitForShutdown() {
	this->lcs->waitForShutdown();
	this->rcs->waitForShutdown();
	this->robotHorn->waitForShutdown();

}

void CollisionSensingRobotController::startChildRunnables() {
	this->lcs->start();
	this->rcs->start();
	this->robotHorn->start();
}
