
#ifndef COLLISIONSENSINGROBOTCONTROLLER_H_
#define COLLISIONSENSINGROBOTCONTROLLER_H_

#include "RobotController.h"
#include "CollisionSensor.h"
#include "Horn.h"
#include "CommandQueue.h"

class CollisionSensingRobotController : public RobotController {
protected:
	CollisionSensor* lcs;
	CollisionSensor* rcs;
	Horn* robotHorn;
	int previousOperation;

#if 0

    int leftMotorDifferential = 100;
    int rightMotorDifferential = 100;

    void processDifferentialMessage(int differentialValue);

    int processSpeedControlCommand(int value);
#endif

public:
	CollisionSensingRobotController(CommandQueue*, PWMManager&, int, int, std::string);
	~CollisionSensingRobotController();
	void run();
	void stop();
	void waitForShutdown();
	void startChildRunnables();
};

#endif
