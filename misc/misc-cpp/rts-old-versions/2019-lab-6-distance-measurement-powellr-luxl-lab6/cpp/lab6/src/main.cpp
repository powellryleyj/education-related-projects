/**
 * @file main.cpp
 * @author  Walter Schilling (schilling@msoe.edu)
 * @version 1.0
 *
 * @section LICENSE This is a piece of a template solution to the lab.
 *
 *
 * @section DESCRIPTION
 *      This is the main method for the basic robot.  It will control the simplest operation of the robot over the network.
 */
#include <pthread.h>
#include <iostream>
#include <unistd.h>
#include <cstdlib>
#include <thread>
#include "GPIO.h"
#include "RobotController.h"
#include "NetworkCommands.h"
#include "RobotCfg.h"
#include "PWMManager.h"
#include "TaskRates.h"
#include "DistanceSensor.h"
#include "TaskRates.h"
#include "DiagnosticManager.h"
#include "NetworkManager.h"

using namespace std;

class ConsolePrinter: public PeriodicTask
{
private:
	se3910RPiHCSR04::DistanceSensor *ds;

public:
	/**
	 * This method will instantiate a new instance of the distance sensor.
	 * @param ds This is a pointer to the distance sensor.
	 * @param threadName This is the name of the thread that is to periodically be invoked.
	 * @param period This is the period for the task.  This period is given in microseconds.
	 */
	ConsolePrinter(se3910RPiHCSR04::DistanceSensor* ds, std::string threadName,	uint32_t period);

	/**
	 * This is the task method.  The task method will be invoked periodically every taskPeriod
	 * units of time.
	 */
	virtual void taskMethod();


};

ConsolePrinter::ConsolePrinter(se3910RPiHCSR04::DistanceSensor* ds, std::string threadName,	uint32_t period)
: PeriodicTask(threadName, period) {
	this->ds = ds;
}

void ConsolePrinter::taskMethod()
{
	cout << "Current: " << ds->getCurrentDistance() << "\tMax: " << ds->getMaxDistance() << "\tMin: " << ds->getMinDistance() << "\n";
}

/**
 * This is the main program.  It will instantiate a network manager and robot controller as well as a Command Queue.
 * It will then block until the user enters a message on the console.
 * This will then cause it to shutdown the other classes and wait for their threads to terminate.
 */
int main(int argc, char* argv[]) {
	CommandQueue *myQueue[NUMBER_OF_QUEUES];
	myQueue[0] = new CommandQueue();
	myQueue[1] = new CommandQueue();

	//Declare a pair of players
	NetworkReceptionManager nm(9090, myQueue, "NetworkManager");
	NetworkTransmissionManager nt(&nm, "Network Transmission");

	PWMManager pwm("PWM Manager", 200);
	RobotController mc(myQueue[0], pwm, "RobotController");
	se3910RPiHCSR04::DistanceSensor *ds = new se3910RPiHCSR04::DistanceSensor(22, 27, "Distance Sensor", DISTANCE_SENSOR_TASK_RATE);

	DiagnosticManager dm(myQueue[1], &nt, ds, "Diagnostic Manager", DIAGNOSTIC_TASK_MANAGER_TASK_RATE);

	// Start each of the threads up.
	nm.start();
	nt.start();

	mc.start();
	pwm.start();
	ds->start(54);
	dm.start();

	ConsolePrinter * cp = new ConsolePrinter(ds, "Console Thread", 250000);
	cp->start();

	char msg[1024];
	do {
		cin >> msg;
		cout << msg;

		if (msg[0]=='P')
		{
			ds->printThreads();
		}
	}while (msg[0]!='Q');
	nm.stop();
	mc.stop();
	pwm.stop();

	ds->stop();
	cp->stop();
	delete ds;
	delete cp;

	// Wait for the threads to die.
	nm.waitForShutdown();
	mc.waitForShutdown();
	pwm.waitForShutdown();

	delete myQueue[0];
	delete myQueue[1];
}






