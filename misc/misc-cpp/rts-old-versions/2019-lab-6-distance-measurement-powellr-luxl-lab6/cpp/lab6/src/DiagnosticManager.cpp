/**
 * @file DiagnosticManager.h
 * @author  Walter Schilling (schilling@msoe.edu)
 * @version 1.0
 *
 * @section LICENSE
 *
 *
 * This code is developed as part of the MSOE SE3910 Real Time Systems course,
 * but can be freely used by others.
 *
 * SE3910 Real Time Systems is a required course for students studying the
 * discipline of software engineering.
 *
 * This Software is provided under the License on an "AS IS" basis and
 * without warranties of any kind concerning the Software, including
 * without limitation merchantability, fitness for a particular purpose,
 * absence of defects or errors, accuracy, and non-infringement of
 * intellectual property rights other than copyright. This disclaimer
 * of warranty is an essential part of the License and a condition for
 * the grant of any rights to this Software.
 *
 * @section DESCRIPTION
 * This file holds the implementation of the diagnostic manager.  The diagnostic manager is responsible for reporting back robot diagnostics status to the controller.
 */

#include "DiagnosticManager.h"
#include "NetworkCommands.h"

/**
 * This is the constructor for the class.
 * @param queue This is a pointer to the queue that is to be used to read commands destined for this module.
 * @param transmitInstance This si the instance of the network transmission class, which is where all messages will be sent from.
 * @param ds This is a pointer tio the instance of the distance sensor that is to be used by this class.
 * @param threadName This is the name of the periodic task thread.
 * @param period This is the period for the task, given in microseconds.
 */
DiagnosticManager::DiagnosticManager(CommandQueue *queue,
		NetworkTransmissionManager* transmitInstance,
		se3910RPiHCSR04::DistanceSensor *ds, std::string threadName,
		uint32_t period) :
		PeriodicTask(threadName, period) {
	this->referencequeue = queue;
	this->transmitInstance = transmitInstance;
	this->dsInstance = ds;
}

/**
 * This is the destructor which will clean up after the class.
 */
DiagnosticManager::~DiagnosticManager() {
	/**
	 * There is nothing to do in the destructor.
	 */
}

/**
 * This is the task method.  The task method will be invoked periodically every taskPeriod
 * units of time.  Right now, it only sends the distance back.
 */
void DiagnosticManager::taskMethod() {
	/**
	 * Start the task by determining if there is an item in the queue to be processed.  If there is, process the next item from the queue.
	 */
	if (this->referencequeue->hasItem()) {
		/**
		 * Dequeue the next item.
		 */
		int value = this->referencequeue->dequeue();

		/**
		 * The only thing we process from this queue is the reset command for max distances.  If that is received in the queue, reset the max distances accordingly.
		 */
		if (value == DIAG_RESET_MIN_MAX_DISTANCES) {
			this->dsInstance->resetDistanceRanges();
		}

	}

	/**
	 * Read the distance sensor average distance and transmit it.
	 */
	int valueToTransmit = this->dsInstance->getAverageDistance();

	/**
	 * Construct a message for the current reading.
	 */
	networkMessageStruct msgToSend;
	msgToSend.messageDestination = 1;
	msgToSend.message = valueToTransmit | DISTANCEREADINGBITMAP	| AVERAGEREADINGBITMAP;
	msgToSend.xorChecksum = msgToSend.message ^ msgToSend.messageDestination;

	/**
	 * Transmit the message by enqueuing it to send.
	 */
	this->transmitInstance->enqueueMessage(msgToSend);

	/**
	 * Read the distance sensor current distance and transmit it.
	 */
	valueToTransmit = this->dsInstance->getCurrentDistance();

	/**
	 * Construct a message for the current reading.
	 */
	msgToSend.messageDestination = 1;
	msgToSend.message = valueToTransmit | DISTANCEREADINGBITMAP
			| CURRENTREADINGBITMAP;
	msgToSend.xorChecksum = msgToSend.message ^ msgToSend.messageDestination;

	/**
	 * Transmit the message.
	 */
	this->transmitInstance->enqueueMessage(msgToSend);

	/**
	 * Read the distance sensor max distance and transmit it.
	 */
	valueToTransmit = this->dsInstance->getMaxDistance();

	/**
	 * Construct a message for the current reading.
	 */
	msgToSend.messageDestination = 1;
	msgToSend.message = valueToTransmit | DISTANCEREADINGBITMAP
			| MAXREADINGBITMAP;
	msgToSend.xorChecksum = msgToSend.message ^ msgToSend.messageDestination;

	/**
	 * Transmit the message.
	 */
	this->transmitInstance->enqueueMessage(msgToSend);

	/**
	 * Read the distance sensor min distance and transmit it.
	 */
	valueToTransmit = this->dsInstance->getMinDistance();

	/**
	 * Construct a message for the current reading.
	 */
	msgToSend.messageDestination = 1;
	msgToSend.message = valueToTransmit | DISTANCEREADINGBITMAP
			| MINREADINGBITMAP;
	msgToSend.xorChecksum = msgToSend.message ^ msgToSend.messageDestination;

	/**
	 * Transmit the message.
	 */
	this->transmitInstance->enqueueMessage(msgToSend);
}
