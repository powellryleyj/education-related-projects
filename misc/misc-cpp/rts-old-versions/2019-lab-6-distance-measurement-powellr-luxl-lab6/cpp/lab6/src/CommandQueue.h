/*
 * CommandQueue.h
 *
 *  Created on: Oct 2, 2019
 *      Author: se3910
 */

#include <mutex>
#include <queue>
#include <semaphore.h>

#ifndef COMMANDQUEUE_H_
#define COMMANDQUEUE_H_

class CommandQueue {
private:
    std::queue<int> commandQueueContents;
    sem_t queueCountSemaphore;
    std::mutex queueMutex;
public:
	CommandQueue();
	virtual ~CommandQueue();
	bool hasItem();
	int dequeue();
	void enqueue(int value);
};

#endif /* COMMANDQUEUE_H_ */
