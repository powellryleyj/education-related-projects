/*
 * DistanceSensor.h
 *
 *  Created on: Oct 20, 2019
 *      Author: se3910
 */

#ifndef DISTANCESENSOR_H_
#define DISTANCESENSOR_H_

#include "GPIO.h"
#include "PeriodicTask.h"

/**
 * This macro defines the speed of sound in millimeters per millisecond
 */
#define SPEED_OF_SOUND (0.000343)

namespace se3910RPiHCSR04 {

class DistanceSensor : public PeriodicTask {
private:

	se3910RPi::GPIO* tPin;
	se3910RPi::GPIO* ePin;
	int maxDistance = 0;
	int minDistance = 2500;
	int distanceRecordingCount = 0;
	int totalOfAllDistances = 0;
	int currentDistance = 0;

public:
	DistanceSensor(int trigPin, int echoPin, std::string threadName, uint32_t period);
	virtual ~DistanceSensor();
	void taskMethod();
	int getMaxDistance();
	int getMinDistance();
	int getCurrentDistance();
	int getAverageDistance();
	void resetDistanceRanges();
};

} /* namespace se3910RPiHCSR04 */

#endif /* DISTANCESENSOR_H_ */
