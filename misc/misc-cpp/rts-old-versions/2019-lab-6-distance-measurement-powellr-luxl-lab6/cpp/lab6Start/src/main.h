/*
 * main.h
 *
 *  Created on: Jul 2, 2019
 *      Author: se3910
 */

#ifndef MAIN_H_
#define MAIN_H_

/**
 * This class will provide for basic printing to the console of distance measurements obtained from the distance sensor.
 */
class ConsolePrinter: public PeriodicTask
{
private:
	/**
	 * This is an instance of the distance sensor that is to be used for development purposes.
	 */
	se3910RPiHCSR04::DistanceSensor *ds;

public:
	/**
	 * This method will instantiate a new instance of the console printer.
	 * @param ds This is a pointer to the distance sensor.
	 * @param threadName This is the name of the thread that is to periodically be invoked.
	 * @param period This is the period for the task.  This period is given in microseconds.
	 */
	ConsolePrinter(se3910RPiHCSR04::DistanceSensor* ds, std::string threadName,	uint32_t period);

	/**
	 * This is the task method.  The task method will be invoked periodically every taskPeriod
	 * units of time.
	 */
	virtual void taskMethod();
};

int main(int argc, char* argv[]);



#endif /* MAIN_H_ */
