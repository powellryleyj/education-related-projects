package edu.msoe.se3910.lightcontroller.core;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * This class will provide implementation for the network controller.
 *
 * @author wws
 */
public class NetworkController implements iNetworkController {
    /**
     * This is the socket that is to be used for communications.
     */
    private Socket socket;
    /**
     * This is the ip address of the given machine.
     */
    private String ip;
    /**
     * This is the data stream that is to be written to when communicating with the device.
     */
    private DataOutputStream out;

    /**
     * This boolean will indicate whether or not there is a current valid connection.
     */
    private boolean connected = false;

    /**
     * Default constructor.
     */
    public NetworkController() {
        super();
    }

    @Override
    public void setIPAddress(String ip) {
        this.ip = ip;
    }

    @Override
    public void connect() throws UnknownHostException, IOException {
        socket = new Socket(ip, 9090);
        out = new DataOutputStream(socket.getOutputStream());
        connected = true;
    }

    @Override
    public void disconnect() {
        try {
            out.write(0xFFFFFFFF);
            socket.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        connected = false;
    }

    @Override
    public boolean getConnectionStatus() {
        return connected;
    }

    @Override
    public void sendMessage(int destination, int message) throws IOException {
        int checksum = message ^ destination;
        System.out.printf("%x %x %x\n", destination, message, checksum);
        if (connected) {
            out.writeInt(destination);
            out.writeInt(message);
            out.writeInt(checksum);
            out.flush();
        }
    }

}
