/**
 *
 */

#include "PWMManager.h"

/**
 *
 */
PWMManager::PWMManager(std::string threadname, uint32_t period) : PeriodicTask(threadname, period) {
	this->currentCount = 0;
}

/**
 *
 */
PWMManager::~PWMManager() {

}

/**
 *
 */
void PWMManager::addPWMPin(int gpioNumber, se3910RPi::GPIO *instance, uint32_t dutyCycles) {
	GPIOPWMControlMapStruct m;
	m.gpioPin = instance;
	m.dutyCycle = dutyCycles;
	m.changed = false;
	this->GPIOControlMap.emplace(gpioNumber, m);
}

/**
 *
 */
void PWMManager::removePWM(int gpioNumber) {
	auto search = this->GPIOControlMap.find(gpioNumber);
	if (search != this->GPIOControlMap.end()) {
		this->GPIOControlMap.erase(gpioNumber);
	}
}

/**
 *
 */
void PWMManager::setDutyCycle(int gpioNumber, uint32_t dutyCycle) {
	auto search = this->GPIOControlMap.find(gpioNumber);
	if (search != this->GPIOControlMap.end()) {
		this->GPIOControlMap.at(gpioNumber).dutyCycle = dutyCycle;
	}

}

/**
 *
 */
void PWMManager::taskMethod() {
	if(currentCount == 0) {
		std::map<int, GPIOPWMControlMapStruct>::iterator it = GPIOControlMap.begin();
		while( it != GPIOControlMap.end()) {
			it->second.changed = false;
			if(it->second.dutyCycle > 0) {
				it->second.gpioPin->setValue(se3910RPi::GPIO::VALUE::GPIO_HIGH);
			}
			it++;
		}
	} else {
		std::map<int, GPIOPWMControlMapStruct>::iterator it = GPIOControlMap.begin();
		while (it != GPIOControlMap.end()) {
			if(it->second.changed == false && it->second.dutyCycle == currentCount) {
				it->second.gpioPin->setValue(se3910RPi::GPIO::VALUE::GPIO_LOW);
			}
			it++;
		}
	}
	currentCount = (currentCount + 1) % 100;
}
