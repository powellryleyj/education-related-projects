/*
 * MotorController.cpp
 *
 *  Created on: Oct 9, 2019
 *      Author: Lily Lux
 */

#include "MotorController.h"


/**
 * Constructor
 */
MotorController::MotorController(int forwardPinNumber, int backwardPinNumber, int enablePinNumber, PWMManager& manager): managerInstance(manager) {
    this->speed = 0;
    this->forwardIOPin = new se3910RPi::GPIO(forwardPinNumber, se3910RPi::GPIO::GPIO_OUT);
    this->backwardIOPin = new se3910RPi::GPIO(backwardPinNumber, se3910RPi::GPIO::GPIO_OUT);
    this->enablePin = new se3910RPi::GPIO(enablePinNumber, se3910RPi::GPIO::GPIO_OUT);
    this->managerInstance.addPWMPin(enablePinNumber, enablePin, 50);
}

/**
 * Destructor
 */
MotorController::~MotorController() {
    delete this->forwardIOPin;
    delete this->backwardIOPin;
    delete this->enablePin;

}

/**
 * Sets the speed and the duty cycle appropriately
 * @param int speed: int between 1 and 100.
 */
void MotorController::setSpeed(int speed) {
    if (0 < speed && speed < 100) {
        this->speed = speed;
        managerInstance.setDutyCycle(this->enablePin->getPinNumber(), speed);
    }
}

/**
 * Sets the direction of the motor
 */
void MotorController::setDirection(int direction) {
    printf("Direction: %d\n", direction);
	if (direction > 0) {
        //go forward
        this->forwardIOPin->setValue(se3910RPi::GPIO::GPIO_LOW);
        this->backwardIOPin->setValue(se3910RPi::GPIO::GPIO_HIGH);
    } else if (direction < 0) {
        //go backwards
        this->forwardIOPin->setValue(se3910RPi::GPIO::GPIO_HIGH);
        this->backwardIOPin->setValue(se3910RPi::GPIO::GPIO_LOW);
    } else {
        //stop
        this->forwardIOPin->setValue(se3910RPi::GPIO::GPIO_LOW);
        this->backwardIOPin->setValue(se3910RPi::GPIO::GPIO_LOW);
    }
}


/**
 * Stops the motor
 */
void MotorController::stop() {
    this->setDirection(0);
}
