/**
 * @file NetworkCommands.h
 * @author  Walter Schilling (schilling@msoe.edu)
 * @version 1.0
 *
 * @section LICENSE
 *
 * This code is developed as part of the MSOE SE3910 Real Time Systems course,
 * but can be freely used by others.
 *
 * SE3910 Real Time Systems is a required course for students studying the
 * discipline of software engineering.
 *
 * This Software is provided under the License on an "AS IS" basis and
 * without warranties of any kind concerning the Software, including
 * without limitation merchantability, fitness for a particular purpose,
 * absence of defects or errors, accuracy, and non-infringement of
 * intellectual property rights other than copyright. This disclaimer
 * of warranty is an essential part of the License and a condition for
 * the grant of any rights to this Software.
 *
 * @section DESCRIPTION
 * This file defines the commands that are to be sent over the network to the device.
 */

#ifndef NETWORKCOMMANDS_H_
#define NETWORKCOMMANDS_H_

/**
 * This segment defines the destination queues and devices for the given messages.
 */
#define MOTOR_CONTROL_DESTINATION (0x00000001)

/**
 * This command indicates that the rest of the word being sent over the network is consumed by the speed setting.
 */
#define SPEEDDIRECTIONBITMAP  0x40000000

/**
 * These are definitions related to the control of the robot via the network.
 */
#define MOTORDIRECTIONBITMAP  0x20000000

/**
 * This indicates the value that is or'ed with the MOTORDIRECTIONBITMAP to indicate that the direction of the robot is to be forward.
 */
#define  FORWARD  0x00000001
/**
 * This indicates the value that is or'ed with the MOTORDIRECTIONBITMAP to indicate that the direction of the robot is to be backward.
 */
#define  BACKWARD  0x00000002
/**
 * This indicates the value that is or'ed with the MOTORDIRECTIONBITMAP to indicate that the direction of the robot is to be left.
 */
#define  LEFT  0x00000004
/**
 * This indicates the value that is or'ed with the MOTORDIRECTIONBITMAP to indicate that the direction of the robot is to be right.
 */
#define  RIGHT  0x00000008
/**
 * This indicates the value that is or'ed with the MOTORDIRECTIONBITMAP to indicate that the robot is to stop.
 */
#define  STOP  0x00000010

/**
 * This definition is a combination of all motor directions, and can be used as a generic mask when commands are sent.
 */
#define MOTORDIRECTIONS (FORWARD | BACKWARD | LEFT | RIGHT| STOP)





#endif /* NETWORKCOMMANDS_H_ */
