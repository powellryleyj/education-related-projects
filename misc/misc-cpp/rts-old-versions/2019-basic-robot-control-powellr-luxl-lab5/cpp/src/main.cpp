/**
 * @file main.cpp
 * @author  Walter Schilling (schilling@msoe.edu)
 * @version 1.0
 *
 * @section LICENSE This is a piece of a template solution to the lab.
 *
 *
 * @section DESCRIPTION
 *      This is the main method for the basic robot.  It will control the simplest operation of the robot over the network.
 */
#include <pthread.h>
#include <iostream>
#include <unistd.h>
#include <cstdlib>
#include <thread>
#include "GPIO.h"
#include "NetworkManager.h"
#include "RobotController.h"
#include "NetworkCommands.h"
#include "RobotCfg.h"
#include "PWMManager.h"
#include "TaskRates.h"

using namespace std;

/**
 * This is the main program.  It will instantiate a network manager and robot controller as well as a Command Queue.
 * It will then block until the user enters a message on the console.
 * This will then cause it to shutdown the other classes and wait for their threads to terminate.
 */
int main(int argc, char* argv[]) {
	CommandQueue *myQueue[1];
	myQueue[0] = new CommandQueue();

	//Declare a pair of players
	NetworkManager nm(9090, myQueue, "NetworkManager");
	PWMManager pwm("PWM Manager", 200);
	RobotController mc(myQueue[0], pwm, "RobotController");


	// Start each of the threads up.
	nm.start();
	mc.start();
	pwm.start();

	char msg[1024];
	cin >> msg;
	cout << msg;

	nm.stop();
	mc.stop();
	pwm.stop();

	// Wait for the threads to die.
	nm.waitForShutdown();
	mc.waitForShutdown();
	pwm.waitForShutdown();

	delete myQueue[0];
}






