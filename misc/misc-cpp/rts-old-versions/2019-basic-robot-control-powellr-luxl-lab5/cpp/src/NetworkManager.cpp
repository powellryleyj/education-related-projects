/**
 * @file NetworkManager.h
 * @author  Ryley Powell (powellr@msoe.edu)
 * @version 1.0
 *
 * @section LICENSE
 *
 *
 * This code is developed as part of the MSOE SE3910 Real Time Systems course,
 * but can be freely used by others.
 *
 * SE3910 Real Time Systems is a required course for students studying the
 * discipline of software engineering.
 *
 * This Software is provided under the License on an "AS IS" basis and
 * without warranties of any kind concerning the Software, including
 * without limitation merchantability, fitness for a particular purpose,
 * absence of defects or errors, accuracy, and non-infringement of
 * intellectual property rights other than copyright. This disclaimer
 * of warranty is an essential part of the License and a condition for
 * the grant of any rights to this Software.
 *
 * @section DESCRIPTION
 *
 * This class is used to setup socket connections between a client application
 * and this server that will be used to dispatch the issued commands to the proper GPIO
 * pin.
 */

#include "NetworkManager.h"
#include "NetworkCommands.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <iostream>

/**
 * Constructor that assigns instance variables and constructs the base class
 * @param port the port that this server will listen on
 * @param commands a pointer to an array that will be used by the underlying LightController's
 */
NetworkManager::NetworkManager(int port, CommandQueue* commands[], std::string threadname) : RunnableClass(threadname) {
	this->port = port;
	this->referenceQueue = commands;
}

/**
 * Default destructor
 */
NetworkManager::~NetworkManager() {
	// Default Destructor -> Do nothing.
}

/**
 * This function will initialize the server socket, listen for potential connections, and then dispatch
 * the received commands to the appropriate queue.
 */
void NetworkManager::run() {
	struct sockaddr_in address;
	int opt = 1, accepted_sock, addrlen = sizeof(address), read_val;
	int32_t buffer[3];

	if ((this->server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
		std::cerr << "Socket creation failed." << std::endl;
		exit(EXIT_FAILURE);
	} else {
		std::cout << "Socket Created." << std::endl;
	}

	if(setsockopt(this->server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
		std::cerr << "Socket options failed." << std::endl;
		exit(EXIT_FAILURE);
	} else {
		std::cout << "Socket options set." << std::endl;
	}

	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons(this->port);

	if(bind(this->server_fd, (struct sockaddr *) &address, sizeof(address)) < 0) {
		std::cerr << "Socket binding failure." << std::endl;
		exit(EXIT_FAILURE);
	} else {
		std::cout << "Socket bound." << std::endl;
	}

	if(listen(this->server_fd, 3) < 0) {
		std::cerr << "Socket listening failure." << std::endl;
		exit(EXIT_FAILURE);
	} else {
		std::cout << "Socket listening." << std::endl;
	}

	if((accepted_sock = accept(this->server_fd, (struct sockaddr *) &address, (socklen_t*) &addrlen)) < 0) {
		std::cerr << "Socket accept failure." << std::endl;
		exit(EXIT_FAILURE);
	} else {
		std::cout << "Socket accepted connection." << std::endl;
	}

	while(this->keepGoing == true) {
		if ((read_val = recv(accepted_sock, (void *)buffer, (unsigned int) (3 * sizeof(int32_t)), MSG_WAITALL)) < 0) {
			std::cerr << "read_val : " << read_val << std::endl;
			std::cerr << strerror(errno) << std::endl;
			exit(EXIT_FAILURE);
		} else if(read_val == 0) {
			// do nothing until proper shutdown.
		} else {
			if(ntohl(buffer[0]) == 1 && (ntohl(buffer[0])^ntohl(buffer[1])) == ntohl(buffer[2])) {
				this->referenceQueue[0]->enqueue(ntohl(buffer[1]));
			} else if(ntohl(buffer[0]) == 2 && (ntohl(buffer[0])^ntohl(buffer[1])) == ntohl(buffer[2])) {
				this->referenceQueue[1]->enqueue(ntohl(buffer[1]));
			} else {
				std::cerr << "Checksum does not match; Dropping Message." << std::endl;
			}
		}

	}

	shutdown(accepted_sock, 2);
	shutdown(this->server_fd, 2);

}

//void NetworkManager::stop() {
//	this->keepGoing = false;
//}


