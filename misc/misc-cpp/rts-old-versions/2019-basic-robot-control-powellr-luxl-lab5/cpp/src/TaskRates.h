/**
 * @file TaskRates.h
 * @author  Walter Schilling (schilling@msoe.edu)
 * @version 1.0
 *
 * @section LICENSE
 *
 * This code is developed as part of the MSOE SE3910 Real Time Systems course,
 * but can be freely used by others.
 *
 * SE3910 Real Time Systems is a required course for students studying the
 * discipline of software engineering.
 *
 * This Software is provided under the License on an "AS IS" basis and
 * without warranties of any kind concerning the Software, including
 * without limitation merchantability, fitness for a particular purpose,
 * absence of defects or errors, accuracy, and non-infringement of
 * intellectual property rights other than copyright. This disclaimer
 * of warranty is an essential part of the License and a condition for
 * the grant of any rights to this Software.
 *
 * @section DESCRIPTION
 * This file defines the task rates for all periodic tasks within the system.
 */

#ifndef TASKRATES_H_
#define TASKRATES_H_

/**
 * This macro defines the task rate for the PWM manager in microseconds.  The periodic task will execute every n microseconds, where n is defined here.
 */
#define PWM_MANAGER_TASK_RATE (200)

#endif /* TASKRATES_H_ */
