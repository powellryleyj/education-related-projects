#ifndef ROBOTCONTROLLER_H
#define ROBOTCONTROLLER_H

#include "MotorController.h"
#include "PWMManager.h"
#include "CommandQueue.h"

class RobotController: public RunnableClass {
protected:
	CommandQueue* referencequeue;
	MotorController* leftMotor;
	MotorController* rightMotor;
	int currentSpeed;
	int currentOperation;

	int processMotionControlCommand(int command);
	int processSpeedControlCommand(int command);

public:
	RobotController(CommandQueue* queue, PWMManager& manager, std::string threadName);
	virtual ~RobotController();
	void run();
	void stop();
};

#endif
