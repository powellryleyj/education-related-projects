/*
 * ImageTransmitter.cpp
 *
 *  Created on: Nov 4, 2019
 *      Author: se3910
 */

#include "ImageTransmitter.h"

using namespace cv;

ImageTransmitter::ImageTransmitter(char *machineName, int port, int linesPerUDPDatagram) {
	this->destinationMachineName = machineName;
	this->myPort = port;
	this->linesPerUDPDatagram = linesPerUDPDatagram;
}

ImageTransmitter::~ImageTransmitter() {
	delete this->destinationMachineName;
}

int ImageTransmitter::streamImage(Mat *image) {
	if (image != nullptr && this->destinationMachineName != nullptr) {

		// send image

		delete image;
		return 0;
	} else {
		printf("Failed to stream image");
		return -1;
	}
	return 0;
}



