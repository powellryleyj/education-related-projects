/*
 * Camera.cpp
 *
 *  Created on: Nov 4, 2019
 *      Author: se3910
 */

#include "Camera.h"

using namespace cv;

Camera::Camera(int width, int height, std::string threadName): RunnableClass(threadName) {
    this->capture = new VideoCapture(0);
    this->capture->set(CV_CAP_PROP_FRAME_WIDTH, width);
    this->capture->set(CV_CAP_PROP_FRAME_HEIGHT, height);
    this->capture->set(CV_CAP_PROP_FPS, 30);
    this->lastFrame = new Mat(width, height, 0);
}

Camera::~Camera() {
    delete this->lastFrame;
    delete this->capture;
}

void Camera::run() {
	this->mtx.lock();
	if(!this->capture->isOpened()) {
		this->mtx.unlock();
	    printf("Failed to connect to the camera.");
	    exit(-1);
	}

	bool go = true;

	while(go) {
		this->mtx.lock();
		this->capture->grab();
		this->capture->retrieve(*this->lastFrame);
		go = this->keepGoing;
		this->mtx.unlock();
	}
}

void Camera::shutdown() {
	this->mtx.lock();
	this->keepGoing = false;
	this->mtx.unlock();
}

Mat* Camera::takePicture() {
	this->mtx.lock();
	Mat * retVal = NULL;
	if (!this->lastFrame->empty()) {
		retVal = new Mat(*this->lastFrame);
		this->lastFrame->copyTo(*retVal);
	}
	this->mtx.unlock();
	return retVal;


}




