/*
 * ImageCapturer.cpp
 *
 *  Created on: Jul 25, 2019
 *      Author: se3910
 */

#include "ImageCapturer.h"
#include <chrono>

using namespace std::chrono;
using namespace std;

/**
 * Construct a new instance of the image capturer. It will instantiate an instance of the Size class.
 * @param referencedCamera This is the camera that is referenced.
 * @param trans This is the image transmitter that is to send the given image across the network.
 * @param width This is the width of the image that is to be sent in pixels.
 * @param height This is the height of the image that is to be sent in pixels.
 * @param threadName This is the name of the thread that is to execute.
 * @param period This is tyhe period for the task, given in microseconds.
 */
ImageCapturer::ImageCapturer(Camera *referencedCamera, ImageTransmitter *trans,
		int width, int height, std::string threadName, uint32_t period) :
		PeriodicTask(threadName, period) {
	myCamera = referencedCamera;
	myTrans = trans;
	imageWidth = width;
	imageHeight = height;
	size = new Size(width, height);

}

ImageCapturer::~ImageCapturer() {
	delete size;
}

void ImageCapturer::taskMethod() {

	/*This is the virtual task  method. It will execute the given code that is to be executed by this class. It will execute once each task period. The algorithm is as follows:*/
	/*
	 * 1. Obtain the time since the epoch from the system clock in ms.*/
	milliseconds start = duration_cast<milliseconds>(
			system_clock::now().time_since_epoch());

	/*2. Take the picture from the camera.*/
	Mat* image = myCamera->takePicture();

	/*
	 * 3. If the image is not null,
	 */
	if (image != NULL) {
		if (image != NULL) {
			/**
			 * 3.1 Obtain the time since the epoch from the system clock in ms.
			 */
			milliseconds start2 = duration_cast<milliseconds>(
					system_clock::now().time_since_epoch());

			/**
			 * 3.2 Resize the image according to the desired size, if a resize needs to occur.
			 */
			Mat dst;
			if (image->cols != imageWidth || image->rows != imageHeight) {

				resize(*image, dst, *size);
			} else {
				image->copyTo(dst);
			}

			/**
			 * 3.3 Obtain the time since the epoch from the system clock in ms.
			 */
			milliseconds start3 = duration_cast<milliseconds>(
					system_clock::now().time_since_epoch());

			/**
			 * 3.4 Stream the image to the remote device.
			 */
			myTrans->streamImage(&dst);

			/**
			 * Delete the image that was taken.
			 */
			delete image;

			/**
			 * Obtain the time since the epoch from the system clock in ms.
			 */
			milliseconds end = duration_cast<milliseconds>(
					system_clock::now().time_since_epoch());
			milliseconds delta = start2 - start;
			cout << "Grab picture:\t" << (delta.count()) << "\t";
			delta = start3 - start2;
			cout << "Resize:\t" << (delta.count()) << "\t";
			delta = end - start3;
			cout << "Transmit:\t" << (delta.count()) << "\t";

			milliseconds delayTime =  std::chrono::milliseconds(getTaskPeriod()/1000) - (end - start);

			cout << "Delaying:\t" << delayTime.count() << "\n";
			/**
			 * Print out to the console in ms the amount of time it took to grab the picture, resize the picture, and transmit the picture in ms.
			 */
		}
	}
}
