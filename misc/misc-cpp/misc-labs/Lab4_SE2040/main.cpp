#include "util.h"
#include "Map.h"
#include "Robot.h"

using namespace std;

/**
 * worker method that reads data for robots and stores them in a vector
 * to be used later
 * @param map - a pointer to the map being used to be passed to the robot object
 * @param in  - input stream being used
 * @param robot - storage of pointers of robot objects
 */
void getRobots(Map* map, istream& in, vector<Robot>* robot) {
    string dataLine;
    int row, col, id;
    vector<int> uniqueIDs;
    in >> id >> row >> col >> dataLine;
    while(!cin.eof()) {
        Robot temp(map, id, row, col, dataLine);
        robot->push_back(temp);
        in >> id >> row >> col >> dataLine;
    }
}

/**
 * worker method that executes a single step on each robot contained within
 * robots per iterations specified
 * @param robots - the vector containing pointers to the robot objects
 * @param iterations - the number of iterations to complete; if the number is negative,
 * the value will grow to 0 and print the map each iteration
 * @param map - pointer to the map object being used
 */
void processCommands(vector<Robot>* robots, int iterations, Map* map) {
    if(iterations < 0) {
        for (int i = iterations; i < 0; i++) {
            map->write();
            for (unsigned int j = 0; j < robots->size(); j++) {
                robots->at(j).step();
            }
        }
    }
    else {
        for (int i = 0; i < iterations; i++) {
            for (unsigned int j = 0; j < robots->size(); j++) {
                robots->at(j).step();
            }
        }
    }
}

/**
 * main logic flow
 * @param ac - integer number of args
 * @param argv - arguments to be used
 * @return 0 if all is well
 */
int main(int ac, char** argv) {
    string fileName, dataLine;
    int iterations;
    std::vector<Robot> robots;
    se2040_util::set_input(ac, argv);
    Map map;
    map.load();
    cin >> iterations;
    getRobots(&map, cin, &robots);
    processCommands(&robots, iterations, &map);
    cout << "At end:" << endl;
    for(unsigned int i = 0; i < robots.size(); i++) {
        robots.at(i).displayStatus();
        cout << endl;
    }
    map.write();
    return 0;
}

