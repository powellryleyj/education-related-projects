///////////////////////////////////////////////////////////
//  Map.h
//  Implementation of the Class Map
//  Created on:      05-Apr-2017 8:23:58 PM
///////////////////////////////////////////////////////////

#ifndef map_h
#define map_h

#include <iostream>

class Location;
/**
 * A map of all locations. Store the locations as a two-dimensional array,
 * probably Locations *locations[HEIGHT][WIDTH].
 */
class Map
{

public:

	Map();
	bool canMoveTo(Location* destination);
	Location* eastOf(Location* source);
	//void load(std::istream& in);
	void load();
	Location* locationAt(int row, int col);
	Location* northOf(Location* source);
	Location* southOf(Location* source);
	Location* westOf(Location* source);
	void write();

private:

    static const int HEIGHT = 10;
    static const int WIDTH = 20;

	Location *m_Location[HEIGHT][WIDTH];

    void processLine(std::string line, int row);
//  void printLine(string line);

};
#endif // map_h
