///////////////////////////////////////////////////////////
//  Location.cpp
//  Implementation of the Class Location
//  Created on:      05-Apr-2017 8:22:11 PM
///////////////////////////////////////////////////////////

#include "Location.h"
#include <iostream>

using namespace std;

/**
 * Constructor.
 * @param row - the row of this location
 * @param column - the column of this location
 * @param token - the character representing the terrain/object that
 * is occupying this location
 */
Location::Location(int row, int column, char token){
	this->r = row;
	this->c = column;
    this->token = token;
    this->robotPresent = false;
}

/**
 * Returns the column of this location
 * @return the column of this location
 */
int Location::col(){
    return this->c;
}


/**
 * Return a character representing the display value for this location. If it
 * contains a robot, return R. If it contains gold (that has not been picked up),
 * return *. Otherwise, return the character from the input file (space, other
 * punctuation, etc).
 * @return the token representing the terrain/object occupying this location; returns 'R'
 * if and only if the location is currently occupied by the robot overriding the token
 * of the location
 */
char Location::display() {
    char t = this->token;
    if(this->robotPresent){
        t = (char) (this->robotID + 48);
    }
	return t;
}


/**
 * Track that a robot enters the location.
 */
void Location::enter(int robotID) {
    this->robotPresent = true;
    this->robotID = robotID;
}


/**
 * Returns true if the location contains gold.
 * @return - true if and only if the location contains gold
 */
bool Location::hasGold() {
    bool res = false;
    if(token == '*'){
        res = true;
    }
	return res;
}


/**
 * Returns true if the location contains a mountain.
 * @return true if and only if the location contains a montain
 */
bool Location::hasMountain() {
    bool res = false;
    if(token == '#'){
        res = true;
    }
	return res;
}


/**
 * Returns true if the current cell is occupied either by a mountain or a robot.
 * @return true if the location is not occupied by a mountain, robot, or edge of
 * the map.
 */
bool Location::occupied() {
    bool res = false;
    if(this->hasMountain() || this->robotPresent) {
        res = true;
    }
	return res;
}


/**
 * The map location for this location.
 * @return the row for this location
 */
int Location::row() {
	return this->r;
}


/**
 * Remove the gold from the current location, making the location "empty".
 */
void Location::takeGold() {
    this->token = ' ';
}


/**
 * Track that a robot has left the current location.
 */
void Location::vacate() {
    this->robotPresent = false;
}