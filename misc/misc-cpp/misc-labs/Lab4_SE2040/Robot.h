///////////////////////////////////////////////////////////
//  Robot.h
//  Implementation of the Class Robot
//  Created on:      05-Apr-2017 8:24:15 PM
///////////////////////////////////////////////////////////

#ifndef robot_h
#define robot_h

#include <iostream>
#include <vector>
class Map;
class Location;

/**
 * Tracks the state of a robot including the map the robot is in as well as its
 * current location and the amount of gold the robot has picked up.
 */
class Robot
{

public:
	Map *m_Map;
    Robot();
	Robot(Map* map, int id, int row, int col, std::string data );
	void displayStatus();
	Map* map();
	bool move(char direction);
    void step();
private:
	Location *current;
    int gold, id, steps;
    std::vector<char> commands;
    bool crashed;
    bool checkDirections(char direction);
    Location* moveTo(char direction);
};
#endif // robot_h
