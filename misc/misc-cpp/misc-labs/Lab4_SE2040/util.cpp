//
// implementation of se2040_util namespace
//

#include <fstream>
#include <iostream>
#include "util.h"

using namespace std;

// see util.h for documentation
void se2040_util::set_input(int argc, char *argv[]) {
    static bool called = false;
    if ( called ) {
        cerr << "programming error: set_input cannot be called twice." << endl;
        exit(1);
    }
    called = true;
    static ifstream alternative_input; // used if have filename on command line
    if (argc > 0 && argv[1] != nullptr && argv[1][0] != 0) {
        alternative_input.open(argv[1]);
        if (!alternative_input) {
            cerr << "Could not open " << argv[1] << " for input." << endl;
            exit(1);
        }
        // this magic sets the read buffer for cin to the read buffer for
        //   the input stream (redirecting input from the named file)
        cin.rdbuf(alternative_input.rdbuf());
    }
}
