///////////////////////////////////////////////////////////
//  Map.cpp
//  Implementation of the Class Map
//  Created on:      05-Apr-2017 8:23:58 PM
///////////////////////////////////////////////////////////

#include "Map.h"
#include <sstream>
#include "Location.h"
using namespace std;
/**
 * Constructor.
 */
Map::Map(){

}


/**
 * Return true if a robot can move into a destination location (there's no robot
 * there and no mountain).
 * @param destination - the new location to be moved to
 * @return true if the robat can move to the destination; false if otherwise
 */
bool Map::canMoveTo(Location* destination){
	bool res;
    if(destination != nullptr) {
        res = !destination->occupied();
        } else {
        res = false;
    }

    return res;
}


/**
 * Return the location to the east of the source location, or nullptr if the east
 * is at a wall.
 * @param source - the current location
 * @return the location east of the current
 */
Location* Map::eastOf(Location* source){
    int currCol = source->col();
    Location* newLoc;
    if(currCol + 1 < WIDTH){
        newLoc = m_Location[source->row()][currCol + 1];
    } else {
        newLoc = nullptr;
    }
	return  newLoc;
}

/**
 * Read the map from standard input, setting up the location data as required.
 */
void Map::load(){
    string line;
    int row = 0;
    for(int i = 0; i < HEIGHT+2; i++){
        getline(cin, line);
        if(i != 0 && i != 11) {
            processLine(line, row);
            row++;
        }
    }
}

/**
 * returns the location object specified by the row and column arguments
 * @param row - the row of the location
 * @param col - the column of the location
 * @return the location specified by the row and col arguments
 */
Location* Map::locationAt(int row, int col){
    Location* loc;
    if(row >= 0 && row <= (HEIGHT) && col >= 0 && col <= (WIDTH)) {
        loc = m_Location[row][col];
    } else {
        loc = nullptr;
    }
	return  loc;
}


/**
 * Return the location to the north of the source location, or nullptr if that
 * location is a wall.
 * @param source - the current location
 * @return the location north of the current location
 */
Location* Map::northOf(Location* source){
    int currRow = source->row();
    Location* newLoc;
    if(currRow - 1 >= 0){
        newLoc = m_Location[currRow - 1][source->col()];
    } else {
        newLoc = nullptr;
    }
    return  newLoc;
}


/**
 * Return the location to the south of the source location, or nullptr if that
 * location is a wall.
 * @param source - the current location
 * @return the location south of the current location
 */
Location* Map::southOf(Location* source){
    int currRow = source->row();
    Location* newLoc;
    if(currRow + 1 < HEIGHT){
        newLoc = m_Location[currRow + 1][source->col()];
    } else {
        newLoc = nullptr;
    }
    return  newLoc;
}


/**
 * Return the location to the west of the source location, or nullptr if that
 * location is a wall.
 * @param source - the current location
 * @return the location west of the current location
 */
Location* Map::westOf(Location* source){
    int currCol = source->col();
    Location* newLoc;
    if(currCol - 1 >= 0){
        newLoc = m_Location[source->row()][currCol - 1];
    } else {
        newLoc = nullptr;
    }
    return  newLoc;
}


/**
 * Write the map to standard output using the same format as load (with the
 * exception that the robot's position is marked by R).
 */
void Map::write(){
    string temp;
    char arr[WIDTH+2];
    for(int i = 0; i < HEIGHT+2; i++) {
        if (i == 0 || i == 11) {
            temp = "+--------------------+";
        } else {
            for (int j = 0; j < WIDTH + 2; j++) {
                if (j == 0 || j == 21) {
                    arr[j] = '|';
                } else {
                    arr[j] = m_Location[i-1][j-1]->display();
                }
            }
            temp = arr;
        }

        cout << temp << endl;
    }
}

/**
 * worker method that splits each line into its respective characters including whitespace
 * @param line - the line to be evaluated
 * @param row - the current row that is being evaluated
 */
void Map::processLine(string line, int row) {
    istringstream iss;
    char token;
    iss.str(line);
    for(int i = 0; i < WIDTH+2; i++){
        iss >> noskipws >> token;
        if(i != 0 && i != 21) {
            m_Location[row][i-1] = new Location(row, i-1, token);
        }
    }
}
