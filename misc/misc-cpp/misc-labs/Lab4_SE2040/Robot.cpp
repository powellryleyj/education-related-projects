///////////////////////////////////////////////////////////
//  Robot.cpp
//  Implementation of the Class Robot
//  Created on:      05-Apr-2017 8:24:15 PM
///////////////////////////////////////////////////////////

#include "Robot.h"
#include "Map.h"
#include "Location.h"
#include <sstream>

using namespace std;

/**
 * default constructor
 */
Robot::Robot(){}

/**
 * Constructor. if the starting position has gold, it will automatically
 * be picked up
 * @param map - the address of the map object being used
 * @param data - includes the id, starting row, starting col,
 * and commands (not seperated by whitespace)
 */
Robot::Robot(Map* map, int id, int row, int col, string data){
    this->gold = 0, this->steps = 0, this->id = id;
    this->crashed = false;
    this->m_Map = map;
    copy(data.begin(), data.end(), back_inserter(this->commands));
    current = map->locationAt(row, col);
    current->enter(id);
    if(current->hasGold()) {
        current->takeGold();
        ++gold;
    }
}


/**
 * Write the Robot's status to standard output using the format
 *   Robot at row, column (amount gold)
 */
void Robot::displayStatus(){
	cout << "Robot at " << (current->row()) <<
         ", " << (current->col()) << " (" << gold << " gold)";
}


/**
 * Returns the map of the world in which the robot is moving around.
 * @return - the map object tied to the robot
 */
Map* Robot::map(){
	return  m_Map;
}


/**
 * Move the robot one location in the specified direction (east, west, north,
 * south). Returns true if the robot moves, false if moving the robot would make
 * it exit the map or climb a mountain. This method should also ensure the robot
 * picks up any gold that it encounters.
 * @param direction - characters n s e w which signify the four cardianl directions
 * @return - true if the robot moves to the location; false if the location can not be
 * moved to
 */
bool Robot::move(char direction){
    bool canMove = checkDirections(direction);
    Location* curr = current;
    Location* newLoc;
    if(canMove) {
        newLoc = moveTo(direction);
        curr->vacate();
        newLoc->enter(id);
        if(newLoc->hasGold()) {
            newLoc->takeGold();
            ++gold;
        }
        current = newLoc;
    } else {
        canMove = false;
    }
	return canMove;
}

void Robot::step() {
    if(!crashed) {
        int index = steps % (int) commands.size();
        char command = commands[index];
        if (command == 'n'
            || command == 's'
            || command == 'e'
            || command == 'w') {
            bool res = this->move(command);
            if (!res) {
                cout << "Error in step " << steps << ": robot " << id << " cannot move " << command << endl;
                crashed = true;
            }
        } else if (command == '?') {
            this->displayStatus();
            cout << " at step " << steps << endl;
        } else if (command == 'p') {
            m_Map->write();
        } else {
            cerr << "Invalid command:" << command <<
                 ", cannot process." << endl;
        }
    }
    this->steps++;
}

/**
 * worker method that checks if the robot is able to move to the north, south, east, or
 * west of the current location
 * @param direction - characters n s e w which signifies the cardinal directions
 * @return true if the robot is able to move to the new location; false otherwise
 */
bool Robot::checkDirections(char direction) {
    bool res = false;
    switch(direction) {
        case 'n':
            res = m_Map->canMoveTo(m_Map->northOf(current));
            break;
        case 's':
            res = m_Map->canMoveTo(m_Map->southOf(current));
            break;
        case 'e':
            res = m_Map->canMoveTo(m_Map->eastOf(current));
            break;
        case 'w':
            res = m_Map->canMoveTo(m_Map->westOf(current));
            break;
        default:
            cerr << "Not a valid direction" << endl;
            break;
    }
    return res;
}

/**
 * worker method that returns the location east, north, south, or west of the
 * current location
 * @param direction - characters n s e w signifies the cardinal directions
 * @return the location north, east, south, or west of the current location
 */
Location* Robot::moveTo(char direction) {
    Location* loc;
    switch(direction) {
        case 'n':
            loc = m_Map->northOf(current);
            break;
        case 's':
            loc = m_Map->southOf(current);
            break;
        case 'e':
            loc = m_Map->eastOf(current);
            break;
        case 'w':
            loc = m_Map->westOf(current);
            break;
        default:
            cerr << "Not a valid direction" << endl;
            loc = nullptr;
            break;
    }
    return loc;
}