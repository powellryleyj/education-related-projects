///////////////////////////////////////////////////////////
//  Location.h
//  Implementation of the Class Location
//  Created on:      05-Apr-2017 8:22:11 PM
///////////////////////////////////////////////////////////

#ifndef location_h
#define location_h

/**
 * Tracks the state of a single location, including its row and column on the map
 * as well as whether there is a robot or gold at that location. Ensure gold is
 * removed from the map when it is picked up.
 */
class Location
{
private:
    int r;
    int c;
    char token;
    bool robotPresent;
	int robotID;
public:
	Location(int row, int column, char token);
	int col();
	char display();
	void enter(int robotID);
	bool hasGold();
	bool hasMountain();
	bool occupied();
	int row();
	void takeGold();
	void vacate();

};
#endif // location_h
