//
// Created by Ryley on 4/8/2017.
//

#ifndef LAB4_SE2040_UTIL_H
#define LAB4_SE2040_UTIL_H
//
// util.h: se2040 utilities
//

namespace se2040_util {

    // Given an integer number of arguments and an array of argument
    //   strings (as passed to main()), checks if there is at least
    //   one string in argv[]. If there is, this code treats it as
    //   the name of a file and attempts to open as standard input.
    //   If no name is specified, does nothing (leaving cin unchanged).
    //   Prints an error if a name is specified but the name cannot
    //   be opened as a file.
    //   This function must be called at most once.
    void set_input(int argc, char *argv[]);

}
#endif //LAB4_SE2040_UTIL_H
