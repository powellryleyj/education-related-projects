//
// auto.h - don't change this file!
//

#ifndef _auto_h
#define _auto_h

enum { NAMELEN = 41 }; // max length of names with the null terminator
enum { IDLEN    = 6 }; // length of plate_id, no terminator

// Auto: stores an auto's owner and plate_id
typedef struct
{
    char owner[NAMELEN];         // null-terminated 
    char plate_id[IDLEN];        // *not* null-terminated
} Auto;

// construct new item
Auto* make_auto(const char owner[], const char plate_id[]);

// return space for auto to the heap
void delete_auto(Auto *p);

// return true if two items have same the plate_id
int same_plates(const Auto *a, const Auto *b);

// return hash value for auto as an integer between 0 and 999 (inclusive)
int hash_of(const Auto *p);

// write up to max_size characters into dest; dest must have space for
//    sizeof(Auto) + 10 characters; returns dest
char* dump_auto(char dest[], const Auto* src);

// test code
void test_auto_operations();

#endif
