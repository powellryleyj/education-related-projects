// main.c: main code for lab 9
//
// Do not change this file.
//

#include "lookup.h"             // don't change the order of these - they're
#include "auto.h"             //   in this order as a test that each
#include <stdio.h>              //   of your .h files are independent
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "leak_detector_c.h"    // this MUST be the last include

enum { MAX_OUTPUT_SIZE = 10000 }; // max size of dump results
enum { EXTRA_SPACE = 10 };        // extra space for output text

void execute_tests();
void process_commands();

int main(int argc, char *argv[])
{
    if ( argc > 0 && argv[1] != NULL ) {
        if ( freopen(argv[1], "r", stdin) == NULL ) {
            printf("Cannot open %s for input.\n", argv[1]);
            return 1;
        }
    }
    atexit(report_mem_leak);
    execute_tests();
    process_commands();
    printf("[Normal termination.]\n");
    return 0;
}

void execute_tests()
{
    test_auto_operations();
    test_table_operations();
    test_removing_entries();
    printf("[All tests completed.]\n");
}

void add_entry(LookupTable *table);
void search_for_entry(LookupTable *table);
void remove_entry(LookupTable *table);

// read commands and process each
void process_commands()
{
    LookupTable *table = make_table();
    char cmd;
    printf("Enter command [a)dd, s)earch, r)emove, d)ump, q)uit]:\n");
    while ( scanf(" %c", &cmd) == 1 && !feof(stdin) && cmd != 'q' )
    {
        switch ( cmd )
        {
        case 'a':
            add_entry(table);
            break;
        case 's':
            search_for_entry(table);
            break;
        case 'r':
            remove_entry(table);
            break;
        case 'd':
        {
            char contents[MAX_OUTPUT_SIZE];
            printf("%s", dump_table(contents, MAX_OUTPUT_SIZE, table));
        }
        break;
        case 'q':
            break;
        default:
            printf("Illegal command: `%c'\n", cmd);
        }
        printf("Enter command [a)dd, s)earch, r)emove, d)ump, q)uit]:\n");
    }
    delete_table(table);
}

// reads exactly ADDRLEN chars into dest with NO null terminator
//    and no leading spaces; if fewer than IDLEN characters are
//    entered, fills the rest of dest with spaces
void read_plate_id(char dest[])
{
    char c;
    while ( isspace(c = getchar()) ) // skip leading spaces
        ;
    dest[0] = c;
    int i = 1;
    while ( i < IDLEN && !isspace(c = getchar()) ) {
        dest[i] = c;
        ++i;
    }
    for(; i < IDLEN; ++i) {
        dest[i] = ' ';
    }
}

// Prompt for new data and add it to the addr table, first checking it's
// not there.
void add_entry(LookupTable *table)
{
    char owner[NAMELEN], plate_id[IDLEN];
    printf("Enter owner name and plate id (with no spaces in the name):\n");
    scanf("%s", owner);
    read_plate_id(plate_id);
    Auto *target = make_auto(owner, plate_id);
    if ( lookup_auto(table, target) != NULL )
    {
        printf("An entry for %.*s already exists.\n", IDLEN, plate_id);
        delete_auto(target);
    }
    else
    {
        add_auto(table, target);
        char tmp[NAMELEN + IDLEN + EXTRA_SPACE];
        dump_auto(tmp, target);
        printf("%s added\n", tmp);
    }
}

// Prompt for a addr number and either print it's not there or 
// print the auto associated with that number
void search_for_entry(LookupTable *table)
{
    char plate_id[IDLEN];
    printf("Enter plate number to look up: ");
    read_plate_id(plate_id);
   
    Auto *target = make_auto("", plate_id);
    Auto *found = lookup_auto(table, target);
    if ( found == NULL )
        printf("No entry for number %.*s\n", IDLEN, plate_id);
    else
        printf("Entry with number %.*s: %s\n", IDLEN, plate_id, found->owner);
    delete_auto(target);
}

// Prompt for a plate_id number and remove the item referred to by that
// number.  Prints an error if the number is not present.
void remove_entry(LookupTable *table)
{
    char plate_id[IDLEN];
    printf("Enter the plate number: ");
    read_plate_id(plate_id);
    Auto *target = make_auto("", plate_id);
    Auto *found = lookup_auto(table, target);
    if ( found == NULL )
        printf("No entry for plate %.*s\n", IDLEN, plate_id);
    else
    {
        remove_auto(table, target);
        char tmp[NAMELEN + IDLEN + EXTRA_SPACE];
        dump_auto(tmp, target);
        printf("%sremoved\n", tmp);
    }
    delete_auto(target);
}
