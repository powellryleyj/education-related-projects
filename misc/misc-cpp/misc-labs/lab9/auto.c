//
// auto.c
// Author: Ryley Powell (powellr)
// SE 2040, Spring 2017
//

#include "auto.h"
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include "leak_detector_c.h"    // this MUST be the last include

Auto* make_auto(const char owner[], const char plate_id[])
{
    Auto *temp = malloc(sizeof(Auto));
    memset(temp->owner, '\0', sizeof(temp->owner));
    memset(temp->plate_id, '\0', sizeof(temp->plate_id));
    strncpy(temp->owner, owner, NAMELEN);
    strncpy(temp->plate_id, plate_id, IDLEN);
    return temp;
}

void delete_auto(Auto *p)
{
    free(p);
}

int same_plates(const Auto *a, const Auto *b)
{
    char str1[IDLEN], str2[IDLEN];
    strncpy(str1, a->plate_id, IDLEN);
    strncpy(str2, b->plate_id, IDLEN);
    if(!strncmp(str1, str2, IDLEN)) {
        return 1;
    } else {
        return 0;
    }
}

int hash_of(const Auto *p)
{
    int last = p->plate_id[IDLEN - 1] - '0';
    int next = p->plate_id[IDLEN - 2] - '0';
    int result = next * 10 + last;
    if ( result < 0 )           // allow for odd cases
        result = 0;
    return result;
}

char* dump_auto(char dest[], const Auto* src)
{
    char spacer[] = ": ";
    memset(dest, '\0', sizeof(Auto));
    strncat(dest, src->plate_id, IDLEN);
    strcat(dest, spacer);
    strncat(dest, src->owner, NAMELEN);
    //strcat(dest, temp);
    //strcat(dest, terminator);
    //int len = strlen(dest);
    //printf("%d, %s, %s\n", len, dest, dest);
    return dest;
}

void test_auto_operations()
{
    // do not change this code
    Auto *a = make_auto("george", "123456");
    const char longowner[NAMELEN] =
        "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
    Auto *b = make_auto(longowner, "123456");
    Auto *c = make_auto("george", "1234YZ");
    assert(same_plates(a, b));
    assert(!same_plates(a, c));
    assert(!same_plates(b, c));
    assert(hash_of(a) == 56);
    assert(hash_of(c) == 452);
    assert(strcmp(a->owner, "george") == 0);
    assert(strcmp(b->owner, longowner) == 0);
    char tmp[200];
    dump_auto(tmp, a);
    assert(strcmp(tmp, "123456: george") == 0);
    delete_auto(a);
    delete_auto(b);
    delete_auto(c);
}
