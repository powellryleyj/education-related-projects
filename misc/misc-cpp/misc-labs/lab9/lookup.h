// lookup.h: lookup table for autos

#ifndef _lookup_h
#define _lookup_h

enum { MAX_ENTRIES = 20 };

#include "auto.h"

typedef struct {
    int count;                           // number of filled slots
    Auto *entries[MAX_ENTRIES];          // hash table of entries
    Auto *deleted_marker;                // if used, marks deleted cells
} LookupTable;

// return an empty table
LookupTable* make_table();

// return table (and all entries) to heap
void delete_table(LookupTable* table);

// add auto to non-null table
void add_auto(LookupTable *table, Auto *new_auto);

// return auto matching target, or NULL if target not available
Auto* lookup_auto(LookupTable *table, const Auto* target);

// remove auto from non-null table
// not guaranteed to free space in table
// does nothing if target not found
void remove_auto(LookupTable *table, Auto *target);

// write up to max_size characters into dest, returning dest
char* dump_table(char dest[], int max_size, LookupTable *table);

// test LookupTable operations
void test_table_operations();

// test that removing works (fails softly if remove not implemented)
void test_removing_entries();

#endif
