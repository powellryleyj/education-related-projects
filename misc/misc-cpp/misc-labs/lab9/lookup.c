// revaddr.c
// Author: Ryley Powell (powellr)
// SE 2040, Spring 2017
//

#include "lookup.h"
#include "auto.h"
#include <string.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "leak_detector_c.h"

LookupTable* make_table() 
{
    LookupTable *table = malloc(sizeof(LookupTable));
    table->count = 0;
    memset(table->entries, '\0', sizeof(LookupTable));
    return table;
}

void delete_table(LookupTable* table)
{
    free(table);
}

void add_auto(LookupTable *table, Auto *new_auto)
{
    int hash_attempts = 0, hash_index, loop_condition = 1;
    if(table != NULL && table->count < MAX_ENTRIES) {
        while(loop_condition != 0) {
            hash_index = (hash_of(new_auto) + hash_attempts) % MAX_ENTRIES;
            if(table->entries[hash_index] == NULL) {
                table->entries[hash_index] = new_auto;
                table->count++;
                loop_condition = 0;
            } else {
                ++hash_attempts;
            }
        }
    }
}

Auto* lookup_auto(LookupTable *table, const Auto* target)
{
    int hash_attempts = 0, hash_index, loop_condition = 1;
    Auto *temp = NULL;
    if(table!= NULL) {
        hash_index = (hash_of(target) + hash_attempts) % MAX_ENTRIES;
        temp = table->entries[hash_index];
        if(temp != NULL) {
            while (loop_condition != 0) {
                if (same_plates(temp, target)) {
                    loop_condition = 0;
                } else {
                    ++hash_attempts;
                }
                hash_index = (hash_of(target) + hash_attempts) % MAX_ENTRIES;
                temp = table->entries[hash_index];
            }
        }
    }
    return temp;
}

void remove_auto(LookupTable *table, Auto *target)
{
    // TODO: write this after writing the rest of the lab
    // (Note solution will build and run even though this is not implemented)
}

char* dump_table(char dest[], int max_size, LookupTable *table)
{
    // TODO: revise this as necessary
    dest[0] = '\0';
    for(int i = 0; i < MAX_ENTRIES; ++i) {
        if ( table->entries[i] != NULL ) {
            char tmp[NAMELEN + 10];
            dump_auto(tmp, table->entries[i]);
            if ( strlen(dest) + strlen(tmp) + 1 < max_size ) {
                strcat(dest, tmp);
                strcat(dest, "\n");
            }
        }
    }
    return dest;
}

void test_table_operations()
{
    // do not change this code
    const int RESULT_SIZE = 10000;

    LookupTable *table = make_table();
    char result[RESULT_SIZE];
    dump_table(result, RESULT_SIZE, table);
    assert(strcmp(result, "") == 0);
    Auto *a = make_auto("a", "123456");
    Auto *b = make_auto("b", "123ABC");
    Auto *c = make_auto("c", "000000");
    dump_table(result, RESULT_SIZE, table);
    assert(strcmp(result, "") == 0);
    assert(lookup_auto(table, a) == NULL);
    add_auto(table, a);
    assert(lookup_auto(table, a) == a);
    dump_table(result, RESULT_SIZE, table);
    assert(strcmp(result, "123456: a\n") == 0);
    add_auto(table, b);
    add_auto(table, c);
    assert(lookup_auto(table, a) == a);
    assert(lookup_auto(table, c) == c);
    dump_table(result, RESULT_SIZE, table);
    assert(strcmp(result, "000000: c\n123456: a\n123ABC: b\n") == 0);

    delete_table(table);
}

void test_removing_entries()
{
    // do not change this code
    const int RESULT_SIZE = 10000;

    LookupTable *table = make_table();
    char result[RESULT_SIZE];
    Auto *a = make_auto("a", "123456");
    add_auto(table, a);
    dump_table(result, RESULT_SIZE, table);
    assert(strcmp(result, "123456: a\n") == 0);
    remove_auto(table, a);
    Auto *target = make_auto("a", "123456");
    if ( lookup_auto(table, target) != NULL ) {
        printf(">>> remove not implemented\n");
        delete_auto(target);
    } else {
        delete_auto(target);
        dump_table(result, RESULT_SIZE, table);
        assert(strcmp(result, "") == 0);
        Auto *b = make_auto("b", "123ABC");
        Auto *c = make_auto("c", "000000");
        add_auto(table, b);
        add_auto(table, c);
        assert(lookup_auto(table, b) == b);
        assert(lookup_auto(table, c) == c);
        dump_table(result, RESULT_SIZE, table);
        assert(strcmp(result, "000000: c\n123ABC: b\n") == 0);
        // delete b so have just c left
        target = make_auto(b->owner, b->plate_id);
        remove_auto(table, b);
        dump_table(result, RESULT_SIZE, table);
        assert(strcmp(result, "000000: c\n") == 0);
        assert(lookup_auto(table, target) == NULL);
        delete_auto(target);
        // delete last auto
        target = make_auto(c->owner, c->plate_id);
        remove_auto(table, c);
        dump_table(result, RESULT_SIZE, table);
        assert(strcmp(result, "") == 0);
        assert(lookup_auto(table, target) == NULL);
        delete_auto(target);
    }
    delete_table(table);
}
