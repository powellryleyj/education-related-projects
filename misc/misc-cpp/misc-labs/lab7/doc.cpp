//
// doc.cpp
// Author: Ryley Powell (powellr)
// SE 2040, Spring 2017
//

#include <iostream>
#include <fstream>
#include "doc.h"
#include "line.h"

constexpr bool DEBUG = false;

using namespace std;

/**
 * default constructor
 */
Document::Document() {
    //TODO
}

/**
 * destructor that cleans up any remaining lines within the document object
 */
Document::~Document() {
    //TODO destroy the document
    std::list<Line*>::iterator it = text.end();
    while(it != text.begin()) {
        delete *it;
        --it;
    }
    delete *it;
}

/**
 * reads the file specified by the path
 * @param source_file - path to the file
 */
void Document::read(string source_file) {
    ifstream infile(source_file);
    string text_of_line;
    Line* line;
    getline(infile, text_of_line);
    while(infile.good()) {
        if(DEBUG) {
            cout << text_of_line << endl;
        }
        line = new Line(text_of_line);
        text.push_back(line);
        getline(infile, text_of_line);
    }

}

/**
 * appends a line to the end of the document
 * @param new_line - the line to append
 */
void Document::append(string new_line) {
    Line* line = new Line(new_line);
    text.push_back(line);
}

/**
 * inserts a line before the specified line number
 * @param before - the index to insert before
 * @param new_line - the line to insert
 */
void Document::insert(int before, string new_line) {
    Line* line = new Line(new_line);
    std::list<Line*>::iterator it = text.begin();
    advance(it, before);
    text.insert(it, line);
}

/**
 * removes the line at ths specified index
 * @param num - the index to remove
 */
void Document::remove(int num) {
    std::list<Line*>::iterator it = text.begin();
    advance(it, num);
    delete *it;
    text.erase(it);

}

/**
 * removes the lines between the specified indices
 * @param start - the start index
 * @param end - the end index
 */
void Document::remove(int start, int end) {
    int it = end;
    while(it != start-1) {
        remove(it);
        --it;
    }
}

/**
 * prints out all the lines within the document with the respective line
 * number
 */
void Document::list() {
    int lineNum = 0;
    std::list<Line*>::iterator it = text.begin();
    while(it != text.end()){
        printLineNum(lineNum);
        cout << (*it)->to_string() << endl;
        ++it;
        ++lineNum;
    }
}

/**
 * returns a map of line index paired to the line of text in which is being
 * searched for by the specified text
 * @param target_text - the specified text
 * @param start - the start line to start searching at
 * @return map of all occurences
 */
std::map<int, Line*> Document::find(string target_text, int start) {
    map<int, Line*> map;
    std::list<Line*>::iterator it = text.begin();
    advance(it, start);
    bool containsWord;
    while(it != text.end()) {
        containsWord = (*it)->contains(target_text);
        if(containsWord) {
            map.insert(pair<int, Line*>(start, *it));
        }
        ++it;
        ++start;
    }
    return map;
}

/**
 * worker method that prints line numbers for the list() function
 * @param num - the line number to print
 */
void Document::printLineNum(int num) {
    if(num < 10 && num >= 0) {
        cout << "  " << num << ": ";
    }
    else if(num >= 10 && num < 100) {
        cout << " " << num << ": ";
    }
    else if(num >= 100) {
        cout << num << ": ";
    }
    else if(num >= 1000) {
        cout << "ERROR LINE NUMBER IS TOO BIG" << endl;
    }
    else {
        cout << "ERROR LINE NUMBER CANNOT BE NEGATIVE" << endl;
    }
}