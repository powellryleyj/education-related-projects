// line.cpp
// Author: Ryley Powell (powellr)
// SE 2040, Spring 2017
//


#include <iostream>
#include <sstream>
#include "line.h"
#include "words.h"

using namespace std;

constexpr bool DEBUG = false;

/**
 * creates a line consistent of works within the source string
 * @param source
 */
Line::Line(string source) {
   stringstream iss(source);
   string next;
   iss >> next;
   while ( iss ) {
      Word* word_from_pool = WordPool::instance()->get(next);

      contents.push_back(word_from_pool);

      iss >> next;
   }
}

/**
 * checks if the line contains the target text
 * @param target - the text to search for
 * @return true if the line contains the text; false otherwise
 */
bool Line::contains(string target) const {
    bool res = false;
    std::vector<Word*>::const_iterator it = contents.begin();
    while(it != contents.end()) {
        if(DEBUG) {
            cout << "Target Word: " << target << "; Compared against: " << (*it)->value() << endl;
        }
        if(((*it)->value().compare(target)) == 0) {
            res = true;
            if(DEBUG) {
                cout << "Word found!" << endl;
            }
        }
        ++it;
    }
    return res;
}

/**
 * converts the line to a printable string
 * @return the line as a string
 */
string Line::to_string() const {
   string result = "";
   for(auto wp : contents) {
      if ( !result.empty() )
         result += ' ';
      result += wp->value();
   }
   return result;
}

