//
// Created by Ryley on 4/25/2017.
//

#ifndef LAB6_WUMPUS_H
#define LAB6_WUMPUS_H

#include "Hazard.h"
#include "Movable.h"

class Wumpus : public Hazard, public Movable {
private:
    bool is_trapped;
    int turns_spent_trapped;

    bool generateChance();
public:
    Wumpus(int row, int col, Map *map);
    void displayStatus(), setTrapped(bool flag);
    bool breakFreeOfTrap(), getIsTrapped();
};
#endif //LAB6_WUMPUS_H
