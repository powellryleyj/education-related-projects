//
// Created by Ryley on 4/26/2017.
//

#ifndef LAB6_RNG_H
#define LAB6_RNG_H

#include <stdlib.h>
#include <time.h>

class RNG {
public:
    static int generateNumber(int lower_bound, int upper_bound) {
        srand(time(NULL));
        int random_number = 0;
        for (int i = 0; i < 100; i++) {
            random_number += (rand() % 13);
        }
        return ((random_number % (upper_bound + 1)) + lower_bound);
    }
};


#endif //LAB6_RNG_H
