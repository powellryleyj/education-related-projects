//
// Created by Ryley on 4/25/2017.
//

#include "Location.h"
#include "GamePiece.h"
#include "Hazard.h"

/**
 * constructor
 * @param row - the row in which this location is present
 * @param column - the column in which this location is present
 * @param game_piece - the GamePiece present as this location; if left as null,
 * the location will be treated as a simple floor.
 */
Location::Location(int row, int column, GamePiece* game_piece) {
    this->r = row;
    this->c = column;
    this->game_piece_present = game_piece;
    this->game_piece_override = nullptr;
    this->hasWumpusOrPlayer = false;
}

/**
 * simple getter that returns the column of this location
 * @return this locations column
 */
int Location::col() {
    return this->c;
}

/**
 * simple getter that returns the row of this location
 * @return this locations row
 */
int Location::row() {
    return this->r;
}

/**
 * returns the character associated with the GamePiece present at
 * this location. If no GamePiece is present, a single space will
 * be returned
 * @return the character representing the GamePiece at this location
 */
char Location::display() {
    char token = this->game_piece_present->displayToken();
    if(this->hasWumpusOrPlayer) {
        token = this->game_piece_override->displayToken();
    }
    return token;
}

/**
 * Used to track if the Wumpus or Player has entered the location
 * NOTE: this method currently assumes that only a Wumpus or Player
 * object will be passed to this location
 * @param game_piece - the Wumpus or Player
 */
void Location::enter(GamePiece* game_piece) {
    this->hasWumpusOrPlayer = true;
    this->game_piece_override = game_piece;
}

/**
 * used to track if the Wumpus or Player has left the location
 */
void Location::vacate() {
    this->hasWumpusOrPlayer = false;
    delete this->game_piece_override;
    this->game_piece_override = nullptr;
}

/**
 * checks if this location contains an Arrow
 * @return true if an Arrow is present; false otherwise
 */
bool Location::hasArrow() {
    bool res = false;
    if(this->game_piece_present->displayToken() == '^') {
        res = true;
    }
    return res;
}

/**
 * removes the Arrow from this location; if no arrow is present
 * this method will simply do nothing
 */
void Location::takeArrow() {
    if(this->game_piece_present->displayToken() == '^') {
        this->game_piece_present = new FloorTile();
    }
}

/**
 * checks if this location is occupied by a GamePiece
 * @return true if there is a GamePiece present; false otherwise
 */
bool Location::isOccupied() {
    bool res = false;
    if(this->game_piece_present != nullptr
       || this->hasWumpusOrPlayer) {
        res = true;
    }
    return res;
}

/**
 * returns a pointer to the GamePiece at this location; if the
 * Wumpus or Player is currently present in this location, it will
 * override the current GamePiece present
 * @return the GamePiece at this location
 */
GamePiece* Location::getGamePiece() {
    GamePiece* temp;
    if(this->hasWumpusOrPlayer) {
        temp = game_piece_override;
    }
    else {
        temp = game_piece_present;
    }
    return temp;
}

void Location::setTrap() {
    this->game_piece_present = new Trap();
}
