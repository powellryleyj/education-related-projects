//
// Created by Ryley on 4/25/2017.
//

#ifndef LAB6_PLAYER_H
#define LAB6_PLAYER_H

#include "GamePiece.h"
#include "Movable.h"

class Player : public GamePiece, public Movable {
private:
    int number_of_arrows, health_points, number_of_traps;
public:
    Player(int row, int col, Map* map);
    int healthRemaining();
    int arrowsRemaining();
    void displayStatus();
    void takeDamage(int damage), takeArrow();
    bool fireArrow(), setTrap();
};
#endif //LAB6_PLAYER_H
