//
// Created by Ryley on 4/25/2017.
//

#ifndef LAB6_LOCATION_H
#define LAB6_LOCATION_H

class GamePiece;

class Location {
private:
    int r, c;
    GamePiece * game_piece_present;
    GamePiece * game_piece_override;
    bool hasWumpusOrPlayer;
public:
    Location(int row, int column, GamePiece* game_piece);
    int col(), row();
    char display();
    bool hasArrow();
    void enter(GamePiece* game_piece);
    void vacate(), takeArrow(), setTrap();
    bool isOccupied();
    GamePiece* getGamePiece();
};
#endif //LAB6_LOCATION_H
