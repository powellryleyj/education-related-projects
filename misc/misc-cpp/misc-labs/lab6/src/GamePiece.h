//
// Created by Ryley on 4/25/2017.
//

#ifndef LAB6_OBJECT_H
#define LAB6_OBJECT_H

class GamePiece {
protected:
    GamePiece(){}
    char token;
public:
    virtual void displayStatus(){};
    char displayToken();
};

class Arrow: public GamePiece {
public:
    Arrow();
};

class CaveWall: public GamePiece {
public:
    CaveWall();
    bool breakArrow(int distanceFired);
    void displayStatus();
};

class FloorTile: public GamePiece {
public:
    FloorTile();
};
#endif //LAB6_OBJECT_H
