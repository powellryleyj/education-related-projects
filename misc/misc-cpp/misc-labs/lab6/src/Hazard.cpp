#include "Hazard.h"
#include <iostream>

/*
 * Hazard.cpp
 *
 *  Created on: Apr 26, 2017
 *      Author: bensonja
 */

// START HAZARD

/**
 * simple getter that gets the damage this Hazard deals
 * @return an integer value
 */
int Hazard::getDamage(){
	return this->damage_dealt;
}

// END HAZARD

// START PITFALL

/**
 * constructor
 */
Pitfall::Pitfall(){
	this->token = 'O';
	this->damage_dealt = 3;
}

/**
 * prints out a message indicating this Pitfall is nearby
 */
void Pitfall::displayStatus() {
	std::cout << "You feel a breeze..." << std::endl;
}

// END PITFALL

// START BAT

Bat::Bat() {
    this->token = 'b';
    this->damage_dealt = 1;
}


/**
 * prints out the message that indicates that a Bat is nearby
 */
void Bat::displayStatus() {
	std::cout << "You hear flapping..." << std::endl;
}

// END BAT

// START TRAP

Trap::Trap() {
	this->token = 'T';
	this->damage_dealt = 3;
}

// END TRAP

