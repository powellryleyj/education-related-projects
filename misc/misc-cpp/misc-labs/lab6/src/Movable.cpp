//
// Created by Ryley on 4/27/2017.
//

#include "Movable.h"

/**
 * constructor for all derived Moveable classes
 * @param row - starting row of this Moveable
 * @param col - starting column of this Moveable
 * @param map - Map this Moveable is located on
 */
Movable::Movable(int row, int col, Map *map) {
    if ((row >= 0)
        && (row < map->getHeight())
        && (col >= 0)
        && (col < map->getWidth())) {
        this->current_location = map->locationAt(row, col);
        this->game_map = map;
    }
}

/**
 * checks the direction if it is valid, and then relocates this Player
 * one location in the direction specified
 * @param direction - ascii characters n s e w indicating the four cardinal
 * directions
 * @return true if this Player has moved; false otherwise
 */
bool Movable::move(char direction) {
    bool res;
    Location *newLoc = nullptr;
    switch (direction) {
        case 'n' :
            res = canMoveTo(direction);
            if (res) {
                newLoc = this->game_map->northOf(current_location);
            }
            break;
        case 's' :
            res = canMoveTo(direction);
            if (res) {
                newLoc = this->game_map->southOf(current_location);
            }
            break;
        case 'e' :
            res = canMoveTo(direction);
            if (res) {
                newLoc = this->game_map->eastOf(current_location);
            }
            break;
        case 'w' :
            res = canMoveTo(direction);
            if (res) {
                newLoc = this->game_map->westOf(current_location);
            }
            break;
        default:
            newLoc = nullptr;
            res = false;
    }
    if (res && newLoc != nullptr) {
        this->current_location = newLoc;
    }
    return res;
}

/**
 * checks if the Location one space in the specified direction is valid
 * @param direction - characters n s w e representing the cardinal directions
 * @return true if valid; false otherwise
 */
bool Movable::canMoveTo(char direction) {
    bool res = false;
    Location *loc;
    switch (direction) {
        case 'n' :
            loc = this->game_map->northOf(current_location);
            break;
        case 's' :
            loc = this->game_map->southOf(current_location);
            break;
        case 'e' :
            loc = this->game_map->eastOf(current_location);
            break;
        case 'w' :
            loc = this->game_map->westOf(current_location);
            break;
        default:
            loc = nullptr;
    }
    if (loc != nullptr && loc->display() != '+') {
        res = true;
    }
    return res;
}
