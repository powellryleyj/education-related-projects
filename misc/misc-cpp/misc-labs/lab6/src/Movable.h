//
// Created by Ryley on 4/25/2017.
//

#ifndef LAB6_MOVABLE_H
#define LAB6_MOVABLE_H

#include "Map.h"
#include "Location.h"

class Movable {
protected:
    Movable(int row, int col, Map *map);
    Map *game_map;
    Location *current_location;
public:
    bool move(char direction);
    bool canMoveTo(char direction);
};

#endif //LAB6_MOVABLE_H
