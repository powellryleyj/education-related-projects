//
// Created by Ryley on 4/25/2017.
//

#include <iostream>
#include <sstream>
#include "Map.h"
#include "Location.h"

/**
 * constructor that creates Map of the specified Height and Width
 * @param rows - the number of rows representing the Height
 * @param cols - the number of columns representing the Width
 */
Map::Map(int rows, int cols) {
    this->HEIGHT = rows;
    this->WIDTH = cols;
    this->map_locations = new std::vector< std::vector<Location*>* >(this->HEIGHT, new std::vector<Location*>(this->WIDTH, nullptr));
    this->cavewall = new CaveWall;
    this->floorTile = new FloorTile;

}

/**
 * simple getter that returns the height of this Map
 * @return the height of this Map
 */
int Map::getHeight() {
    return this->HEIGHT;
}

/**
 * simple getter that returns the width of this Map
 * @return the width of this Map
 */
int Map::getWidth() {
    return this->WIDTH;
}

/**
 * reads the map from standard input, setting up the location data as required
 */
void Map::load() {
    std::string line;
    int row = 0;
    for(int i = 0; i < HEIGHT+2; i++) {
        std::getline(std::cin, line);
        if(i != 0 && i != HEIGHT + 1) {
            processLine(line, row);
            row++;
        }
    }
}

/**
 * worker function that splits each line into its respective characters
 * including whitespace
 * @param line - the line to be evaluated
 * @param row - the current row that is being evaluated
 */
void Map::processLine(std::string line, int row) {
    std::istringstream iss;
    char token;
    int col;
    iss.str(line);
    for(int i = 0; i < WIDTH + 2; i++) {
        iss >> std::noskipws >> token;
        col = 0;
        if(i != 0 && i != WIDTH +1 ) {
            if(token == '+') {
            	map_locations->at(col)->at(row) = new Location(row, i - 1, this->cavewall);
            } else {
            	map_locations->at(col)->at(row) = new Location(row, i - 1, this->floorTile);
            }
            ++col;
        }
    }

}

/**
 * reads the predetermined starting position of the Player; then randomly generates a number of other
 * GamePieces based on the HEIGHT and WIDTH of this Map
 */
void Map::generateGamePieces() {
    //TODO
}

/**
 * write the map to standard output using the same format as load
 */
void Map::write() {
    std::string temp;
    char arr[WIDTH + 2];
    int col;
    int row;
    for(int i = 0; i < HEIGHT + 2; i++) {
        if(i == 0 || i == HEIGHT + 1) {
            temp = "+--------------------+";
        } else {
            for(int j = 0; j < WIDTH + 2; j++) {
                if(j == 0 || j == WIDTH + 1) {
                    arr[j] = '|';
                } else {
                    arr[j] = map_locations->at(row)->at(col)->display();
                    ++col;
                    ++row;
                }
            }
            temp = arr;
        }
        std::cout << temp << std::endl;
    }
}

/**
 * return the location to the east of the source location, or nullptr
 * if that location is out of bounds
 * @param source - the current location
 * @return the location east of the current location
 */
Location* Map::eastOf(Location* source) {
    int currCol = source->col();
    Location* newLoc;
    if(currCol + 1 < WIDTH) {
        newLoc = map_locations->at(source->row())->at(currCol + 1);
    } else {
        newLoc = nullptr;
    }
    return newLoc;
}

/**
 * return the location to the west of the source location, or nullptr
 * if that location is out of bounds
 * @param source - the current location
 * @return the location west of the current location
 */
Location* Map::westOf(Location* source) {
    int currCol = source->col();
    Location* newLoc;
    if(currCol - 1 >= 0) {
        newLoc = map_locations->at(source->row())->at(currCol - 1);
    } else {
        newLoc = nullptr;
    }
    return newLoc;
}

/**
 * return the location to the north of the source location, or nullptr
 * if that location is out of bounds
 * @param source - the current location
 * @return the location north of the current location
 */
Location* Map::northOf(Location* source) {
    int currRow = source->row();
    Location* newLoc;
    if(currRow - 1 >= 0) {
        newLoc = map_locations->at(currRow - 1)->at(source->col());
    } else {
        newLoc = nullptr;
    }
    return newLoc;
}

/**
 * return the location to the south of the source location, or nullptr
 * if that location is out of bounds
 * @param source - the current location
 * @return the location south of the current location
 */
Location* Map::southOf(Location* source) {
    int currRow = source->row();
    Location* newLoc;
    if(currRow + 1 < HEIGHT) {
        newLoc = map_locations->at(currRow + 1)->at(source->col());
    } else {
        newLoc = nullptr;
    }
    return newLoc;
}

/**
 * returns the location object specified by the row and column arguments
 * @param row - the row of the location
 * @param col - the column of the location
 * @return the location specified by the row and col arguments
 */
Location* Map::locationAt(int row, int col) {
    Location* loc;
    if((row >= 0)
       && (row < HEIGHT)
       && (col >= 0)
       && (col < WIDTH)) {
        loc = map_locations->at(row)->at(col);
    } else {
        loc = nullptr;
    }
    return loc;
}

