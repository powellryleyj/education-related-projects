//
// Created by Ryley on 4/25/2017.
//

#ifndef LAB6_HAZARD_H
#define LAB6_HAZARD_H

#include "GamePiece.h"

class Hazard: public GamePiece {
protected:
    Hazard(){}
    int damage_dealt;
public:
    int getDamage();
};

class Pitfall: public Hazard {
public:
    Pitfall();
    void displayStatus();
};

class Bat: public Hazard {
public:
    Bat();
    void displayStatus();
};

class Trap: public Hazard {
public:
    Trap();
};

#endif //LAB6_HAZARD_H
