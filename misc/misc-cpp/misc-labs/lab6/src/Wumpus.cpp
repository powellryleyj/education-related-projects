//
// Created by Ryley on 4/25/2017.
//

#include "Wumpus.h"
#include "RNG.h"
#include <iostream>

/**
 * constructor
 * @param row - starting row of this Wumpus
 * @param col - starting column of this Wumpus
 * @param map - Map this Wumpus is located on
 */
Wumpus::Wumpus(int row, int col, Map *map) : Movable(row, col, map) {
    this->token = 'W';
    this->is_trapped = false;
    this->damage_dealt = 3;
    this->turns_spent_trapped = 0;
}

/**
 * prints out a message indicating this Wumpus is nearby
 */
void Wumpus::displayStatus() {
    std::cout << "You smell something terrible nearby..." << std::endl;
}

/**
 * determines via random chance if this Wumpus breaks free of the Trap; the more turns
 * spent in the Trap, the higher the chance of this Wumpus breaking free
 * @return true if this Wumpus breaks free; false otherwise
 */
bool Wumpus::breakFreeOfTrap() {
    bool res = generateChance();
    this->turns_spent_trapped += 1;
    if (!res) {
        std::cout << "You hear something struggling in your trap..." << std::endl;
    } else {
        std::cout << "You hear your trap break..." << std::endl;
    }
    return res;
}

/**
 * worker method that does the calculation of random chance
 * @return true if broken free; false otherwise;
 */
bool Wumpus::generateChance() {
    int chanceNeeded = 95 - (turns_spent_trapped * 5);
    int randomChance = RNG::generateNumber(0, 100);
    return chanceNeeded < randomChance;
}

/**
 * simple getter of whether this Wumpus is trapped
 * @return true if trapped; false otherwise
 */
bool Wumpus::getIsTrapped() {
    return this->is_trapped;
}

/**
 * simple setter that sets whether this Wumpus is trapped
 * @param flag - true if trapped; false otherwise
 */
void Wumpus::setTrapped(bool flag) {
    this->is_trapped = flag;
}