//
// Created by Ryley on 4/26/2017.
//

#include <iostream>
#include "GamePiece.h"
#include "RNG.h"

// START GAMEPIECE

char GamePiece::displayToken() {
    return this->token;
}

// END GAMEPIECE

// START ARROW

Arrow::Arrow() {
    this->token = '^';
}

// END ARROW

// START CAVEWALL

/**
 * default constructor
 */
CaveWall::CaveWall() {
    this->token = '+';
}

/**
 * method is currently unsupported
 */
void CaveWall::displayStatus() {
    std::cout << "You run into a wall..." << std::endl;
}

/**
 * calculates the probability of an Arrow fired at a wall breaking with
 * respect to how far the Arrow has traveled
 * @param distanceFired - the distance between the the location
 * of this CaveWall and the starting location of the Arrow
 * @return true if the Arrow has broke; false otherwise
 */
bool CaveWall::breakArrow(int distanceFired) {
    bool res = false;
    int rng = RNG::generateNumber(0, 100);
    switch(distanceFired) {
        case 5 :
            if (rng < 10) {
                res = true;
            }
            break;
        case 4 :
            if (rng < 30) {
                res = true;
            }
            break;
        case 3 :
            if (rng < 50) {
                res = true;
            }
            break;
        case 2 :
            if (rng < 70) {
                res = true;
            }
            break;
        case 1 :
            if (rng < 90) {
                res = true;
            }
            break;
        default :
            break;
    }
    if(res) {
        std::cout << "You hear the arrow you fired break..." << std::endl;
    }
    return res;
}

// END CAVEWALL

// START FLOORTILE

/**
 * default constructor
 */
FloorTile::FloorTile() {
    this->token = ' ';
}

// END FLOORTILE
