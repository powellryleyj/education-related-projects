//
// Created by Ryley on 4/25/2017.
//

#include "Player.h"
#include <iostream>

/**
 * constructor
 * @param row - starting row of this Player
 * @param col - starting column of this Player
 * @param map - Map this Player is located on
 */
Player::Player(int row, int col, Map *map) : Movable(row, col, map) {
    this->number_of_arrows = 5;
    this->health_points = 3;
    this->number_of_traps = 1;
    this->token = '@';
}

/**
 * simple getter that returns this Player remaining health
 * @return integer value; any value less than or equal to zero
 * indicates death of this Player
 */
int Player::healthRemaining() {
    return this->health_points;
}

/**
 * simple getter that returns the Player remaining arrows
 * @return integer value; the number of arrows left
 */
int Player::arrowsRemaining() {
    return this->number_of_arrows;
}

/**
 * prints out a message indicating where on the map this Player
 * is located
 */
void Player::displayStatus() {
    std::cout << "You are located at "
              << current_location->col()
              << ", "
              << current_location->row()
              << "."
              << std::endl;
}

/**
 * applies damage to this Player remaining health
 * @param damage - integer value greater than or equal to zero
 */
void Player::takeDamage(int damage) {
    this->health_points -= damage;
}

/**
 * removes an arrow from this Player remaining arrows
 * @return true if an Arrow has been fired; false otherwise
 */
bool Player::fireArrow() {
    bool res = false;
    if(this->number_of_arrows > 0 ) {
        this->number_of_arrows -= 1;
        res = true;
    }
    return res;
}

/**
 * sets a trap at the current location of this Player
 * @return true if a Trap has been placed; false otherwise
 */
bool Player::setTrap() {
    bool res = false;
    if(this->number_of_traps > 0) {
        this->number_of_traps -= 1;
        res = true;
        this->current_location->setTrap();
    }
    return res;
}

/**
 * if an arrow is present at the current location of this Player,
 * will take the arrow; does nothing otherwise
 */
void Player::takeArrow() {
    if(this->current_location->hasArrow()) {
        this->current_location->takeArrow();
        this->number_of_arrows += 1;
    }
}