//
// Created by Ryley on 4/26/2017.
//

#include <fstream>
#include <iostream>
#include "RNG.h"
#include "Map.h"
#include "Player.h"
#include "Wumpus.h"

using namespace std;

const string map_path = "gameMaps/map";

/**
 * randomly selects one of the 10 pre-generated Map files and
 * assigns std::cin's read buffer to it
 */
void getFileSetRdBuf() {
    //string path = map_path + to_string(RNG::generateNumber(0, 9));
	string path = map_path + '0';
    static ifstream in;
    in.open(path.c_str());
    if (!in) {
         cout << "Could not open " << path << " for input." << endl;
         exit(1);
    }
    cin.rdbuf(in.rdbuf());
}

/**
 * creates the game Map from the file selected; generates all
 * gamepieces onto the Map except for the Wumpus and Player
 * @return the address of the fully generated Map
 */
Map *getMap() {
    int row, col;
    cin >> row >> col;
    Map *map = new Map(row, col);
    map->load();
    map->generateGamePieces();
    return map;
}

/**
 * reads the initial Location of the Player and instantiates the
 * Player
 * @param map - the game Map
 * @return the address of the Player object
 */
Player *getPlayer(Map *map) {
    int row, col;
//    string henlo; TODO used these for testing. Delete for final push.
//    cin >> henlo;
//    cout << henlo << endl;
    cin >> row >> col;
    Player *p = new Player(row, col, map);
    return p;
}

/**
 * reads the initial Location of the Wumpus and instantiates the
 * Wumpus
 * @param map - the game Map
 * @return the address of the Wumpus object
 */
Wumpus *getWumpus(Map *map) {
    int row, col;
    cin >> row >> col;
    Wumpus *w = new Wumpus(row, col, map);
    return w;
}

/**
 * main logic flow
 * @return
 */
int main() {
    streambuf *backup = cin.rdbuf();
    getFileSetRdBuf();
    // get map, player, and wumpus
    Map *game_map = getMap();
    Player *user = getPlayer(game_map);
    Wumpus *wumpus = getWumpus(game_map);
    // reset input buffer
    cin.rdbuf(backup);
    game_map->write();

}

