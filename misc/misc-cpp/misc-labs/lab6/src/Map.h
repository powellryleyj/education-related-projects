//
// Created by Ryley on 4/25/2017.
//

#ifndef LAB6_MAP_H
#define LAB6_MAP_H

#include <vector>
#include <string>
#include "GamePiece.h"

class Location;

class Map {
private:
    int HEIGHT, WIDTH;
    std::vector<std::vector<Location*>*>* map_locations;
    void processLine(std::string line, int row);

    CaveWall *cavewall;
    FloorTile *floorTile;
public:
    Map(int rows, int cols);
    int getHeight(), getWidth();
    Location* westOf(Location* source);
    Location* eastOf(Location* source);
    Location* southOf(Location* source);
    Location* northOf(Location* source);
    Location* locationAt(int row, int col);
    void load(), write(), generateGamePieces();
};
#endif //LAB6_MAP_H
