cmake_minimum_required(VERSION 3.7)
project(lab6)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES
        src/GamePiece.h src/Movable.h src/Player.h src/Player.cpp src/Hazard.h src/Hazard.cpp src/Wumpus.h src/Wumpus.cpp src/Location.h
        src/Location.cpp src/Map.h src/Map.cpp src/HuntTheWumpusExtreme.cpp src/GamePiece.cpp src/RNG.h src/Movable.cpp)

add_executable(lab6 ${SOURCE_FILES})