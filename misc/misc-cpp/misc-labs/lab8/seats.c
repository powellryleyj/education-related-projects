// seats.c
// Author: [Ryley Powell (powellr)]
// SE 2040, Spring 2017
//

#include "seats.h"
#include <stdio.h>
#include <stdlib.h>

#define BAD_STATUS -1

// convert an input character to an appropriate status
static int to_status(char input) {
    switch ( input ) {
    case '-': 
        return NOT_AVAILABLE;
    case '.':
        return OPEN;
    case 'O':
        return TAKEN;
    default:
        return BAD_STATUS;
    }
}

// convert a status number to an appropriate character
static char from_status(int status) {
    switch ( status ) {
        case NOT_AVAILABLE:
            return '-';
        case OPEN:
            return '.';
        case TAKEN:
            return 'O';
        case BAD_STATUS:
        default:
            return '!';
    }
}

// read seat data from stdin
void read_seat_data(Seats *dest)
{
    for(int r = 0; r < dest->num_rows; ++r) {
        for(int s = 0; s < dest->seats_per_row; ++s) {
            char status;
            // read next character representing a seat; scanf returns
            //    number of fields read
            if ( scanf(" %c", &status) != 1 ) {
                printf("Missing seat status at row %d, seat %d\n", r, s);
                exit(2);            // halt if cannot read seat status
            } else if ( (dest->status[r][s] = to_status(status)) < 0 ) {
                printf("Illegal seat status at row %d, seat %d: %c\n",
                       r, s, status);
                exit(3);            // halt if read bad seat status
            }
        }
    }
}

// write seat data with - for unavailable seats, . for open seats, and
//    O for reserved seats
void display_seats(const Seats *hall)
{
    //
    // TODO: write seat data, one row per line, with leading spaces
    //
    for(int r = 0; r < hall->num_rows; ++r) {
        for(int s = 0; s < hall->seats_per_row; ++s) {
            printf(" %c", from_status(hall->status[r][s]));
        }
        printf("\n");
    }
}
