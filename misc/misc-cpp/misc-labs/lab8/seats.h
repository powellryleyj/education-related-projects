// seats.h: track seat usage in hall
//
// do not change this file
//

#ifndef _seats_h
#define _seats_h

// possible status for each seat: NOT_AVAILABLE, OPEN, or TAKEN
//   for example, status[0][0] = OPEN would indicate that the first
//   seat in the first row is open
enum { NOT_AVAILABLE, OPEN, TAKEN };

// max number of rows and max number of seats across each row
// this is similar to using #define ROWS 20 but more robust
enum { ROWS = 20, POSITIONS = 30 };

typedef struct {
    int num_rows, seats_per_row;
    int status[ROWS][POSITIONS];
} Seats;

// read seat information from standard input
void read_seat_data(Seats *dest);

// write seat information to standard out
void display_seats(const Seats *hall);

#endif
