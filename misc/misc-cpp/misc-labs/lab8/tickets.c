// tickets.c
// Author: [Ryley Powell (powellr)]
// SE 2040, Spring 2017
//

#include "seats.h"
#include <stdio.h>
#include <stdlib.h>
// TODO: add any required includes

// keeps track of different states each command can end up
enum {OKAY, ILLEGAL_COMMAND,
    OUT_OF_BOUNDS, BAD_RESERVATION,
    BAD_RELEASE, UNHANDLED_ERROR};

// process all commands read from stdin
void process_all_commands(Seats *hall);

void read_hall_size(Seats *hall);

void change_seat_status(Seats *hall, const char command,
                        const int row, const int seat_num);

void print_command_response(const char command, const int row,
                        const int seat_num, const int status);

int main(int argc, char *argv[])
{
    if ( argc > 0 && argv[1] != NULL ) {
        if ( freopen(argv[1], "r", stdin) == NULL ) {
            printf("Cannot open %s for input.\n", argv[1]);
            return 1;
        }
    }
    //
    // TODO: read number of rows and number of seats from standard input,
    //       declare a varible hall of type Seats*, and initialize the
    //       hall using the dgiven number of rows and seats per row.
    //
    Seats* hall = malloc(sizeof(Seats));
    read_hall_size(hall);

    read_seat_data(hall);
    printf("Original seating table:\n");
    display_seats(hall);
    process_all_commands(hall);
    printf("\nFinal seating table:\n");
    display_seats(hall);
    return 0;
}

//
// TODO: you write this and any associated functions
//
void process_all_commands(Seats *hall) {
    char command;
    int row, seat_num;
    while(!feof(stdin)) {
        if (scanf(" %c %d , %d \n", &command, &row, &seat_num) != 4) {
            //row = 20 positions 30
            if(command != 'r' && command != 'o') {
                print_command_response(command, row, seat_num, ILLEGAL_COMMAND);
            } else if(row < 0
               || row >= hall->num_rows
               || seat_num < 0
               || seat_num >= hall->seats_per_row) {
                print_command_response(command, row, seat_num, OUT_OF_BOUNDS);
            } else {
                change_seat_status(hall, command, row, seat_num);
            }
        } else {
            printf("scarf failed to read four things.\n");
            print_command_response(command, row, seat_num, UNHANDLED_ERROR);
        }
    }
}

void read_hall_size(Seats *hall) {
    if (scanf("%d %d", &hall->num_rows, &hall->seats_per_row) != 2) {
        printf("Error: could not read input.\n");
    } else {
        scanf("%d %d", &hall->num_rows + 1, &hall->seats_per_row + 1);
    }
}

void change_seat_status(Seats *hall, const char command,
                        const int row, const int seat_num) {
    if(command == 'r') {
        if(hall->status[row][seat_num] == OPEN) {
            hall->status[row][seat_num] = TAKEN;
            print_command_response(command, row, seat_num, OKAY);
        } else {
            print_command_response(command, row, seat_num, BAD_RESERVATION);
        }
    } else if(command == 'o') {
        if(hall->status[row][seat_num] == TAKEN) {
            hall->status[row][seat_num] = OPEN;
            print_command_response(command, row, seat_num, OKAY);
        } else {
            print_command_response(command, row, seat_num, BAD_RELEASE);
        }
    } else {
        print_command_response(command, row, seat_num, ILLEGAL_COMMAND);
    }
}

void print_command_response(const char command, const int row,
                            const int seat_num, const int status) {
    if(status == OKAY) {
        if(command == 'r') {
            printf("Reserving row %d, seat %d\n", row, seat_num);
        } else if(command == 'o') {
            printf("Releasing row %d, seat %d\n", row, seat_num);
        } else {
            printf("Something extraordinary happened...\nCommand: %c\nRow: %d\nSeat: %d\nStatus: %d\n",
                   command, row, seat_num, status);
        }
    } else if(status == ILLEGAL_COMMAND) {
        printf("Illegal command '%c' (row %d, seat %d).\n", command, row, seat_num);
    } else if(status == OUT_OF_BOUNDS) {
        if(command == 'r') {
            printf("Failed reservation request: row %d, seat %d is not legal.\n", row, seat_num);
        } else if(command == 'o') {
            printf("Failed request to release row %d, seat %d: illegal position.\n", row, seat_num);
        } else {
            printf("Something extraordinary happened...\nCommand: %c\nRow: %d\nSeat: %d\nStatus: %d\n",
                   command, row, seat_num, status);
        }
    } else if(status == BAD_RESERVATION) {
        printf("Failed reservation request: row %d, seat %d not open.\n", row, seat_num);
    } else if(status == BAD_RELEASE) {
        printf("Failed request to release row %d, seat %d: seat not taken.\n", row, seat_num);
    } else {
        printf("Something extraordinary happened...\nCommand: %c\nRow: %d\nSeat: %d\nStatus: %d\n",
                  command, row, seat_num, status);
    }
}

