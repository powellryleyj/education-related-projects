The Google map UI can be launched from the bottom button on the main swing UI.

4 Test files were added to the googleMapTestFiles folder. 3 of the files contain square tracks in
 close proximity to test that the multi colored tracks work properly. And the GoogleMapTest1Point
 .txt will to be used to test that the google map will handle a single point "path".

 test 1:
    Insure control buttons perform correctly.
    The toggle map type button should toggle between the satellite and roadmap views only.
    The zoom in button should zoom in on the google map.
    The zoom out button should zoom out on the google map.
    The direction arrows should pan the google map specified by the arrow.

 test 2:
     Insure multiple tracks can be loaded are displayed correctly, with varying colors.

 test 3:
     Insure the GoogleMapeUI handles a single point track without crashing.