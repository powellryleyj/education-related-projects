/*
                   GNU LESSER GENERAL PUBLIC LICENSE
                       Version 3, 29 June 2007

 Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.


  This version of the GNU Lesser General Public License incorporates
the terms and conditions of version 3 of the GNU General Public
License, supplemented by the additional permissions listed below.

  0. Additional Definitions.

  As used herein, "this License" refers to version 3 of the GNU Lesser
General Public License, and the "GNU GPL" refers to version 3 of the GNU
General Public License.

  "The Library" refers to a covered work governed by this License,
other than an Application or a Combined Work as defined below.

  An "Application" is any work that makes use of an interface provided
by the Library, but which is not otherwise based on the Library.
Defining a subclass of a class defined by the Library is deemed a mode
of using an interface provided by the Library.

  A "Combined Work" is a work produced by combining or linking an
Application with the Library.  The particular version of the Library
with which the Combined Work was made is also called the "Linked
Version".

  The "Minimal Corresponding Source" for a Combined Work means the
Corresponding Source for the Combined Work, excluding any source code
for portions of the Combined Work that, considered in isolation, are
based on the Application, and not on the Linked Version.

  The "Corresponding Application Code" for a Combined Work means the
object code and/or source code for the Application, including any data
and utility programs needed for reproducing the Combined Work from the
Application, but excluding the System Libraries of the Combined Work.

  1. Exception to Section 3 of the GNU GPL.

  You may convey a covered work under sections 3 and 4 of this License
without being bound by section 3 of the GNU GPL.

  2. Conveying Modified Versions.

  If you modify a copy of the Library, and, in your modifications, a
facility refers to a function or data to be supplied by an Application
that uses the facility (other than as an argument passed when the
facility is invoked), then you may convey a copy of the modified
version:

   a) under this License, provided that you make a good faith effort to
   ensure that, in the event an Application does not supply the
   function or data, the facility still operates, and performs
   whatever part of its purpose remains meaningful, or

   b) under the GNU GPL, with none of the additional permissions of
   this License applicable to that copy.

  3. Object Code Incorporating Material from Library Header Files.

  The object code form of an Application may incorporate material from
a header file that is part of the Library.  You may convey such object
code under terms of your choice, provided that, if the incorporated
material is not limited to numerical parameters, data structure
layouts and accessors, or small macros, inline functions and templates
(ten or fewer lines in length), you do both of the following:

   a) Give prominent notice with each copy of the object code that the
   Library is used in it and that the Library and its use are
   covered by this License.

   b) Accompany the object code with a copy of the GNU GPL and this license
   document.

  4. Combined Works.

  You may convey a Combined Work under terms of your choice that,
taken together, effectively do not restrict modification of the
portions of the Library contained in the Combined Work and reverse
engineering for debugging such modifications, if you also do each of
the following:

   a) Give prominent notice with each copy of the Combined Work that
   the Library is used in it and that the Library and its use are
   covered by this License.

   b) Accompany the Combined Work with a copy of the GNU GPL and this license
   document.

   c) For a Combined Work that displays copyright notices during
   execution, include the copyright notice for the Library among
   these notices, as well as a reference directing the user to the
   copies of the GNU GPL and this license document.

   d) Do one of the following:

       0) Convey the Minimal Corresponding Source under the terms of this
       License, and the Corresponding Application Code in a form
       suitable for, and under terms that permit, the user to
       recombine or relink the Application with a modified version of
       the Linked Version to produce a modified Combined Work, in the
       manner specified by section 6 of the GNU GPL for conveying
       Corresponding Source.

       1) Use a suitable shared library mechanism for linking with the
       Library.  A suitable mechanism is one that (a) uses at run time
       a copy of the Library already present on the user's computer
       system, and (b) will operate properly with a modified version
       of the Library that is interface-compatible with the Linked
       Version.

   e) Provide Installation Information, but only if you would otherwise
   be required to provide such information under section 6 of the
   GNU GPL, and only to the extent that such information is
   necessary to install and execute a modified version of the
   Combined Work produced by recombining or relinking the
   Application with a modified version of the Linked Version. (If
   you use option 4d0, the Installation Information must accompany
   the Minimal Corresponding Source and Corresponding Application
   Code. If you use option 4d1, you must provide the Installation
   Information in the manner specified by section 6 of the GNU GPL
   for conveying Corresponding Source.)

  5. Combined Libraries.

  You may place library facilities that are a work based on the
Library side by side in a single library together with other library
facilities that are not Applications and are not covered by this
License, and convey such a combined library under terms of your
choice, if you do both of the following:

   a) Accompany the combined library with a copy of the same work based
   on the Library, uncombined with any other library facilities,
   conveyed under the terms of this License.

   b) Give prominent notice with the combined library that part of it
   is a work based on the Library, and explaining where to find the
   accompanying uncombined form of the same work.

  6. Revised Versions of the GNU Lesser General Public License.

  The Free Software Foundation may publish revised and/or new versions
of the GNU Lesser General Public License from time to time. Such new
versions will be similar in spirit to the present version, but may
differ in detail to address new problems or concerns.

  Each version is given a distinguishing version number. If the
Library as you received it specifies that a certain numbered version
of the GNU Lesser General Public License "or any later version"
applies to it, you have the option of following the terms and
conditions either of that published version or of any later version
published by the Free Software Foundation. If the Library as you
received it does not specify a version number of the GNU Lesser
General Public License, you may choose any version of the GNU Lesser
General Public License ever published by the Free Software Foundation.

  If the Library as you received it specifies that a proxy can decide
whether future versions of the GNU Lesser General Public License shall
apply, that proxy's public statement of acceptance of any version is
permanent authorization for you to choose that version for the
Library.
 */

package tests;

import gpxAnalyzer.GPSMetrics;
import org.xml.sax.SAXException;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import org.junit.*;
import static org.junit.Assert.*;


/**
 * the GPSParserEventHandlerTests class tests relevant parsing methods within the
 * GPSParserEventHandler. Uses a number of test files with expected errors
 *
 * @author Joshua McComack
 * @version 1.0
 * @created 29-Oct-2016
 */
public class GPSParserEventHandlerTests {

    /**
     * Range of acceptable error when testing double values.
     */
    private static final double DELTA = 1.0E-10;


    /**
     * Tests the lat and lon points from gpxAnalyzer.GPSParserEventHandler with GoogleMapTest.txt
     * file
     */
    @Test
    public void testPointsGPSTest5Center() {
        try {
            GPSMetrics metrics = new GPSMetrics("parserTestFiles/GoogleMapTest.txt");
            ArrayList<Point2D.Double> expectedPoints = new ArrayList<>();
            expectedPoints.add(new Point2D.Double(43.30000000000000, -87.90000000000000));
            expectedPoints.add(new Point2D.Double(43.30000000000000, -88.00000000000000));
            expectedPoints.add(new Point2D.Double(43.40000000000000, -88.00000000000000));
            expectedPoints.add(new Point2D.Double(43.40000000000000, -87.90000000000000));
            expectedPoints.add(new Point2D.Double(43.30000000000000, -87.90000000000000));
            ArrayList<Point2D.Double> actualPoints = metrics.getPointList();
            if (actualPoints.size() != expectedPoints.size()){
                System.out.println("Actual point size: " + actualPoints.size() + " expected size: "
                + expectedPoints.size());
                fail();
            } else {
                for (int i =0; i < actualPoints.size(); i++){
                    assertEquals(actualPoints.get(i).getX(), expectedPoints.get(i).getX(), DELTA);
                    assertEquals(actualPoints.get(i).getY(), expectedPoints.get(i).getY(), DELTA);
                }
            }
        } catch (SAXException e){
            System.out.println("Parser through SAXException: " + e.getMessage());
            fail();
        } catch (Exception e) {
            System.out.println("Failed to construct parser");
            fail();
        }
    }

    /**
     * Tests the number of track points in the Waubeka.txt file.
     */
    @Test
    public void testWaubekaTrackPoints() {
        try {
            GPSMetrics metrics = new GPSMetrics("parserTestFiles/waubeka.txt");
            int expected = 1391;
            int actual = metrics.getPointList().size();
            assertEquals(expected, actual);
        } catch (SAXException e){
            System.out.println("Parser through SAXException: " + e.getMessage());
            fail();
        } catch (Exception e) {
            System.out.println("Failed to construct parser");
            fail();
        }
    }

    /**
     * Tests the track Name from gpxAnalyzer.GPSParserEventHandler with GoogleMapTest.txt file
     */
    @Test
    public void testNameGPSTest5Center() {
        try {
            GPSMetrics metrics = new GPSMetrics("parserTestFiles/GoogleMapTest.txt");
            String expected = "GPS Test: 5 points outlining a rectangle. 38464 meters/23.9 miles";
            String actual = metrics.getTrackName();
            assertEquals(expected, actual);
        } catch (SAXException e){
            System.out.println("Parser through SAXException: " + e.getMessage());
            fail();
        } catch (Exception e) {
            fail();
            System.out.println("Failed to construct parser");
        }
    }

    /**
     * Tests the track with one point from gpxAnalyzer.GPSParserEventHandler with
     * IgnoreEleAndTime.txt file. Two out of place ele and time elements should be ignored. with
     * only one track point recorded
     */
    @Test
    public void testIgnoreEleAndTime() {
        try {
            GPSMetrics metrics = new GPSMetrics("parserTestFiles/IgnoreEleAndTime.txt");
            assertEquals(1, metrics.getPointList().size());
        } catch (SAXException e){
            System.out.println("Parser through SAXException: " + e.getMessage());
            fail();
        } catch (Exception e) {
            fail();
            System.out.println("Failed to construct parser");
        }
    }

    /**
     * Tests the elevation for each point from gpxAnalyzer.GPSParserEventHandler with
     * GoogleMapTest.txt file
     */
    @Test
    public void testElevationGPSTest5Center() {
        try {
            GPSMetrics metrics = new GPSMetrics("parserTestFiles/GoogleMapTest.txt");
            ArrayList<Double> elevations = metrics.getElevationList();
            for(Double elevation : elevations){
                assertEquals(500.0000000000000, elevation, DELTA);
            }
        } catch (SAXException e){
            System.out.println("Parser through SAXException: " + e.getMessage());
            fail();
        } catch (Exception e) {
            System.out.println("Failed to construct parser");
            fail();
        }
    }

    /**
     * Tests BlankFile.text for expected SAXException with message:
     * "Premature end of file."
     */
    @Test
    public void testBlankFile() {
        genericSAXExceptionTest("parserTestFiles/BlankFile.txt", "Premature end of file.");
    }

    /**
     * Tests the Lat_Out_Of_Bounds.txt for expected SAXException with message:
     * "Latitude is out of bounds"
     */
    @Test
    public void testLatOutOfBoundsText() {
        genericSAXExceptionTest("parserTestFiles/Lat_Out_Of_Bounds.txt",
                "Latitude is out of bounds at line:16 col:15");
    }

    /**
     * Tests the Lon_Out_Of_Bounds.txt for expected SAXException with message:
     * "Longitude is out of bounds"
     */
    @Test
    public void testLonOutOfBoundsText() {
        genericSAXExceptionTest("parserTestFiles/Lon_Out_Of_Bounds.txt",
                "Longitude is out of bounds at line:16 col:15");
    }

    /**
     * Tests the MissingElevation.txt for expected SAXException with message:
     * "Missing elevation element"
     */
    @Test
    public void testMissingElevationText() {
        genericSAXExceptionTest("parserTestFiles/MissingElevation.txt",
                "Missing elevation element at line:22 col:16");
    }

    /**
     * Tests the MissingEndTag.txt for expected SAXException with message:
     * "The element type\"trkpt\" must be terminated by the matching end-tag \"</trkpt>\"."
     */
    @Test
    public void testMissingEndTagText() {
        genericSAXExceptionTest("parserTestFiles/MissingEndTag.txt", "The element type \"trkpt\" " +
                "must be terminated by the matching end-tag \"</trkpt>\".");
    }

    /**
     * Tests the missingName.txt for expected SAXException with message:
     * "Track missing name element"
     */
    @Test
    public void testMissingNameText() {
        genericSAXExceptionTest("parserTestFiles/missingName.txt",
                "Track missing name element at line:23 col:7");
    }

    /**
     * Tests the MissingTime.txt for expected SAXException with message:
     * "Point missing time element"
     */
    @Test
    public void testMissingTimeText() {
        genericSAXExceptionTest("parserTestFiles/MissingTime.txt",
                "Point missing time element at line:22 col:15");
    }

    /**
     * Tests the EmptyName.txt for expected SAXException with message:
     * "Track has no name"
     */
    @Test
    public void testEmptyNameText() {
        genericSAXExceptionTest("parserTestFiles/EmptyName.txt",
                "Track has no name at line:5 col:18");
    }

    /**
     * Tests the MissingStartTag.txt for expected SAXException with message:
     * "The element type \"trkpt\" must be terminated by the matching end-tag \"</trkpt>\"."
     */
    @Test
    public void testMissingStartTagText() {
        genericSAXExceptionTest("parserTestFiles/MissingStartTag.txt", "The element type \"trkpt\"" +
                " must be terminated by the matching end-tag \"</trkpt>\".");
    }

    /**
     * Tests the InvalidLat.txt for expected SAXException with message:
     * Invalid track point value in lat and lon attributes
     */
    @Test
    public void testInvalidLatText() {
        genericSAXExceptionTest("parserTestFiles/InvalidLat.txt",
                "Invalid track point value in lat and lon attributes at line:6 col:47");
    }

    /**
     * Tests the MissingTrackAtts.txt for expected SAXException with message:
     * Invalid track point value in lat and lon attributes
     */
    @Test
    public void testMissingTrackAttsText() {
        genericSAXExceptionTest("parserTestFiles/MissingTrackAtts.txt",
                "Invalid number of attributes for track point at line:6 col:14");
    }

    /**
     * Tests the NonGPXFile.txt for expected SAXException with message:
     * "Not a GPX file"
     */
    @Test
    public void testNonGPXFileText() {
        genericSAXExceptionTest("parserTestFiles/NonGPXFile.txt", "Not a GPX file");
    }

    /**
     * Tests the InvalidTime.txt for expected SAXException with message:
     * "Invalid time format"
     */
    @Test
    public void testInvalidTimeText() {
        genericSAXExceptionTest("parserTestFiles/InvalidTime.txt",
                "Invalid time format at line:16 col:29");
    }

    /**
     * Tests the InvalidDateFormat.txt for expected SAXException with message:
     * "Invalid date format"
     */
    @Test
    public void testInvalidDateFormatText() {
        genericSAXExceptionTest("parserTestFiles/InvalidDateFormat.txt",
                "Invalid time format at line:16 col:41");
    }

    /**
     * Tests an invalid file name for expected SAXException with message:
     * "Invalid file name"
     */
    @Test
    public void testInvalidFileName() {
        try {
            new GPSMetrics("invalid.txt");
            fail();
        } catch (SAXException e){
            fail();
        } catch (Exception e) {
            if (!e.getMessage().substring(e.getMessage().length() - 43,e.getMessage().length())
                    .equals("(The system cannot find the file specified)")){
                System.out.println("(The system cannot find the file specified)\nActual Message: " +
                        e.getMessage());
                fail();
            }
        }
    }

    /**
     * Tests a null file for expected SAXException with message:
     * "filename cannot be null!"
     */
    @Test
    public void testNullFile() {
        try {
            new GPSMetrics(null);
            fail();
        } catch (SAXException e){
            fail();
        } catch (Exception e) {
            if (!e.getMessage().substring(e.getMessage().length()-24,e.getMessage().length()).equals
                    ("filename cannot be null!")){
                System.out.println("Expected Message: filename cannot be null!\nActual Message: " +
                        e.getMessage());
                fail();
            }
        }
    }

    /**
     * Tests for an expected SAXException and message
     *
     * @param filePath the GPX file to be loaded
     * @param expectedMessage the expected exception message
     */
    private void genericSAXExceptionTest(String filePath, String expectedMessage){
        try {
            new GPSMetrics(filePath);
            fail();
        } catch (SAXException e){
            if (!e.getMessage().equals(expectedMessage)){
                System.out.println("Expected Message: " + expectedMessage + "\nActual Message: " +
                e.getMessage());
                fail();
            }
        } catch (Exception e) {
            System.out.println("Failed to construct parser: " + e.getMessage());
            fail();
        }
    }
}
