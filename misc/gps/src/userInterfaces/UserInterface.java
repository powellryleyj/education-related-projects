/*
                   GNU LESSER GENERAL PUBLIC LICENSE
                       Version 3, 29 June 2007

 Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.


  This version of the GNU Lesser General Public License incorporates
the terms and conditions of version 3 of the GNU General Public
License, supplemented by the additional permissions listed below.

  0. Additional Definitions.

  As used herein, "this License" refers to version 3 of the GNU Lesser
General Public License, and the "GNU GPL" refers to version 3 of the GNU
General Public License.

  "The Library" refers to a covered work governed by this License,
other than an Application or a Combined Work as defined below.

  An "Application" is any work that makes use of an interface provided
by the Library, but which is not otherwise based on the Library.
Defining a subclass of a class defined by the Library is deemed a mode
of using an interface provided by the Library.

  A "Combined Work" is a work produced by combining or linking an
Application with the Library.  The particular version of the Library
with which the Combined Work was made is also called the "Linked
Version".

  The "Minimal Corresponding Source" for a Combined Work means the
Corresponding Source for the Combined Work, excluding any source code
for portions of the Combined Work that, considered in isolation, are
based on the Application, and not on the Linked Version.

  The "Corresponding Application Code" for a Combined Work means the
object code and/or source code for the Application, including any data
and utility programs needed for reproducing the Combined Work from the
Application, but excluding the System Libraries of the Combined Work.

  1. Exception to Section 3 of the GNU GPL.

  You may convey a covered work under sections 3 and 4 of this License
without being bound by section 3 of the GNU GPL.

  2. Conveying Modified Versions.

  If you modify a copy of the Library, and, in your modifications, a
facility refers to a function or data to be supplied by an Application
that uses the facility (other than as an argument passed when the
facility is invoked), then you may convey a copy of the modified
version:

   a) under this License, provided that you make a good faith effort to
   ensure that, in the event an Application does not supply the
   function or data, the facility still operates, and performs
   whatever part of its purpose remains meaningful, or

   b) under the GNU GPL, with none of the additional permissions of
   this License applicable to that copy.

  3. Object Code Incorporating Material from Library Header Files.

  The object code form of an Application may incorporate material from
a header file that is part of the Library.  You may convey such object
code under terms of your choice, provided that, if the incorporated
material is not limited to numerical parameters, data structure
layouts and accessors, or small macros, inline functions and templates
(ten or fewer lines in length), you do both of the following:

   a) Give prominent notice with each copy of the object code that the
   Library is used in it and that the Library and its use are
   covered by this License.

   b) Accompany the object code with a copy of the GNU GPL and this license
   document.

  4. Combined Works.

  You may convey a Combined Work under terms of your choice that,
taken together, effectively do not restrict modification of the
portions of the Library contained in the Combined Work and reverse
engineering for debugging such modifications, if you also do each of
the following:

   a) Give prominent notice with each copy of the Combined Work that
   the Library is used in it and that the Library and its use are
   covered by this License.

   b) Accompany the Combined Work with a copy of the GNU GPL and this license
   document.

   c) For a Combined Work that displays copyright notices during
   execution, include the copyright notice for the Library among
   these notices, as well as a reference directing the user to the
   copies of the GNU GPL and this license document.

   d) Do one of the following:

       0) Convey the Minimal Corresponding Source under the terms of this
       License, and the Corresponding Application Code in a form
       suitable for, and under terms that permit, the user to
       recombine or relink the Application with a modified version of
       the Linked Version to produce a modified Combined Work, in the
       manner specified by section 6 of the GNU GPL for conveying
       Corresponding Source.

       1) Use a suitable shared library mechanism for linking with the
       Library.  A suitable mechanism is one that (a) uses at run time
       a copy of the Library already present on the user's computer
       system, and (b) will operate properly with a modified version
       of the Library that is interface-compatible with the Linked
       Version.

   e) Provide Installation Information, but only if you would otherwise
   be required to provide such information under section 6 of the
   GNU GPL, and only to the extent that such information is
   necessary to install and execute a modified version of the
   Combined Work produced by recombining or relinking the
   Application with a modified version of the Linked Version. (If
   you use option 4d0, the Installation Information must accompany
   the Minimal Corresponding Source and Corresponding Application
   Code. If you use option 4d1, you must provide the Installation
   Information in the manner specified by section 6 of the GNU GPL
   for conveying Corresponding Source.)

  5. Combined Libraries.

  You may place library facilities that are a work based on the
Library side by side in a single library together with other library
facilities that are not Applications and are not covered by this
License, and convey such a combined library under terms of your
choice, if you do both of the following:

   a) Accompany the combined library with a copy of the same work based
   on the Library, uncombined with any other library facilities,
   conveyed under the terms of this License.

   b) Give prominent notice with the combined library that part of it
   is a work based on the Library, and explaining where to find the
   accompanying uncombined form of the same work.

  6. Revised Versions of the GNU Lesser General Public License.

  The Free Software Foundation may publish revised and/or new versions
of the GNU Lesser General Public License from time to time. Such new
versions will be similar in spirit to the present version, but may
differ in detail to address new problems or concerns.

  Each version is given a distinguishing version number. If the
Library as you received it specifies that a certain numbered version
of the GNU Lesser General Public License "or any later version"
applies to it, you have the option of following the terms and
conditions either of that published version or of any later version
published by the Free Software Foundation. If the Library as you
received it does not specify a version number of the GNU Lesser
General Public License, you may choose any version of the GNU Lesser
General Public License ever published by the Free Software Foundation.

  If the Library as you received it specifies that a proxy can decide
whether future versions of the GNU Lesser General Public License shall
apply, that proxy's public statement of acceptance of any version is
permanent authorization for you to choose that version for the
Library.
 */

package userInterfaces;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import gpxAnalyzer.Controller;
import gpxAnalyzer.GPSMetrics;

import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Formatter;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * User Interface - contains all interactions with the user
 * Responsible for loading files selected by the user, passes the list to the controller,
 * and then is capable of displaying the output for each track, as selected by the user
 * @author Connor Hibbs
 */
public class UserInterface extends JFrame{

    private Controller controller;
    private JFileChooser fc;
    private JPanel togglePanel;
    private CardLayout cards;
    private JPanel metrics;
    private JPanel speeds;
    private JLabel track, minLat, minLon, maxLat, maxLon, minEle, maxEle, dist, avgSpd, maxSpd;
    private JTextField trackName, minLatText, maxLatText, minLonText, maxLonText, minEleText, maxEleText, distance, avgSpeed, maxSpeed;
    private JLabel trackTwo, trackTwoOut, speed, distanceOut, three, seven, ten, fifteen, twenty, above;
    private JTextField threeOut, sevenOut, tenOut, fifteenOut, twentyOut, aboveOut;
    private JComboBox<GPSMetrics> loadedTracks;
    private boolean filesLoaded = false;
    private boolean showMetrics = true;

    private void intitalizeMetricPanel(){
        metrics = new JPanel(new GridLayout(10, 2));
        metrics.setBorder(new EmptyBorder(20, 20, 20, 20));

        track = new JLabel("Track: ");
        minLat = new JLabel("Min Latitude: ");
        maxLat = new JLabel("Max Latitude: ");
        minLon = new JLabel("Min Longitude: ");
        maxLon = new JLabel("Max Longitude: ");
        minEle = new JLabel("Min Elevation: ");
        maxEle = new JLabel("Max Elevation: ");
        dist = new JLabel("Distance: ");
        avgSpd = new JLabel("Average Speed: ");
        maxSpd = new JLabel("Max Speed: ");

        trackName = new JTextField();
        trackName.setEditable(false);
        minLatText = new JTextField();
        minLatText.setEditable(false);
        maxLatText = new JTextField();
        maxLatText.setEditable(false);
        minLonText = new JTextField();
        minLonText.setEditable(false);
        maxLonText = new JTextField();
        maxLonText.setEditable(false);
        minEleText = new JTextField();
        minEleText.setEditable(false);
        maxEleText = new JTextField();
        maxEleText.setEditable(false);
        distance = new JTextField();
        distance.setEditable(false);
        avgSpeed = new JTextField();
        avgSpeed.setEditable(false);
        maxSpeed = new JTextField();
        maxSpeed.setEditable(false);

        metrics.add(track);
        metrics.add(trackName);
        metrics.add(minLat);
        metrics.add(minLatText);
        metrics.add(maxLat);
        metrics.add(maxLatText);
        metrics.add(minLon);
        metrics.add(minLonText);
        metrics.add(maxLon);
        metrics.add(maxLonText);
        metrics.add(minEle);
        metrics.add(minEleText);
        metrics.add(maxEle);
        metrics.add(maxEleText);
        metrics.add(dist);
        metrics.add(distance);
        metrics.add(avgSpd);
        metrics.add(avgSpeed);
        metrics.add(maxSpd);
        metrics.add(maxSpeed);
    }

    private void initalizeSpeedPanel(){

        speeds = new JPanel(new GridLayout(9,2));

        trackTwo = new JLabel("Track: ");
        trackTwo.setBorder(new EmptyBorder(10,10,10,10));
        speed = new JLabel("Speeds in mph");
        speed.setBorder(new LineBorder(new Color(0, 0, 0)));
        distanceOut = new JLabel("Distance Covered");
        distanceOut.setBorder(new LineBorder(new Color(0, 0, 0)));
        three = new JLabel("Less than 3");
        three.setBorder(new LineBorder(new Color(0, 0, 0)));
        seven = new JLabel("Between 3 and 7");
        seven.setBorder(new LineBorder(new Color(0, 0, 0)));
        ten = new JLabel("Between 7 and 10");
        ten.setBorder(new LineBorder(new Color(0, 0, 0)));
        fifteen = new JLabel("Between 10 and 15");
        fifteen.setBorder(new LineBorder(new Color(0, 0, 0)));
        twenty = new JLabel("Between 15 and 20");
        twenty.setBorder(new LineBorder(new Color(0, 0, 0)));
        above = new JLabel("More than 20");
        above.setBorder(new LineBorder(new Color(0, 0, 0)));
        trackTwoOut = new JLabel();
        trackTwoOut.setBorder(new EmptyBorder(10,10,10,10));

        threeOut = new JTextField();
        threeOut.setBorder(new LineBorder(new Color(0, 0, 0)));
        sevenOut = new JTextField();
        sevenOut.setBorder(new LineBorder(new Color(0, 0, 0)));
        tenOut = new JTextField();
        tenOut.setBorder(new LineBorder(new Color(0, 0, 0)));
        fifteenOut = new JTextField();
        fifteenOut.setBorder(new LineBorder(new Color(0, 0, 0)));
        twentyOut = new JTextField();
        twentyOut.setBorder(new LineBorder(new Color(0, 0, 0)));
        aboveOut = new JTextField();
        aboveOut.setBorder(new LineBorder(new Color(0, 0, 0)));

        speeds.add(trackTwo);
        speeds.add(trackTwoOut);
        speeds.add(speed);
        speeds.add(distanceOut);
        speeds.add(three);
        speeds.add(threeOut);
        speeds.add(seven);
        speeds.add(sevenOut);
        speeds.add(ten);
        speeds.add(tenOut);
        speeds.add(fifteen);
        speeds.add(fifteenOut);
        speeds.add(twenty);
        speeds.add(twentyOut);
        speeds.add(above);
        speeds.add(aboveOut);
        speeds.setBorder(new EmptyBorder(40,20,20,20));

    }

    private void switchPanel(){
        CardLayout cards = (CardLayout)(togglePanel.getLayout());
        //Show Table
        if(showMetrics){
            showMetrics = false;
            cards.show(togglePanel, "SPEED");
        //Show Metrics
        }else {
            showMetrics = true;
            cards.show(togglePanel, "METRIC");
        }
        repaint();
    }

    public UserInterface(Controller controller){
        super("GPS Plotter");
        this.controller = controller;
        setLayout(new BorderLayout());

        //create the loaded courses JComboBox
        loadedTracks = new JComboBox<>();
        loadedTracks.addActionListener(e -> {
            GPSMetrics gpsMetrics = (GPSMetrics)( (JComboBox)e.getSource() ).getSelectedItem();
            //String name = (String)( (JComboBox)e.getSource() ).getSelectedItem();
            if(gpsMetrics != null) {
                displayMetrics(gpsMetrics);
                displaySpeeds(gpsMetrics);
                controller.updateSelectedMetrics(gpsMetrics);
            }
        });

        //initialize the toggling panel
        cards = new CardLayout();
        togglePanel = new JPanel(cards);

        //create metrics and speed panels
        intitalizeMetricPanel();
        initalizeSpeedPanel();

        //add the two panels to the toggling panel
        togglePanel.add(metrics, "METRIC");
        togglePanel.add(speeds, "SPEED");

        //create a JPanel to hold the loaded courses
        JPanel tracksPanel = new JPanel(new BorderLayout());
        tracksPanel.add(new JLabel("Loaded Tracks"), BorderLayout.NORTH);
        tracksPanel.add(loadedTracks, BorderLayout.CENTER);

        //add toggle button
        JPanel mapPanel = new JPanel(new FlowLayout());
        JButton toggleButton = new JButton("Metrics/Table");
        toggleButton.addActionListener(e -> switchPanel());
        toggleButton.setSize(20,60);
        mapPanel.add(toggleButton);

        //add google map button
        JButton googleMapButton = new JButton("Google Map");
        googleMapButton.addActionListener(e-> showMap());
        googleMapButton.setSize(new Dimension(20,50));
        mapPanel.add(googleMapButton);

        //add 2d plot of tracks button
        JButton plotButton = new JButton("Plot Tracks");
        plotButton.addActionListener(e-> controller.display2dPlotUI());
        plotButton.setSize(new Dimension(20,50));
        mapPanel.add(plotButton);
        
        //add graph button
        JButton graphButton = new JButton("Elevation/Time Graph");
        graphButton.addActionListener(e -> showGraph());
        graphButton.setSize(20, 70);
        mapPanel.add(graphButton);

        //add both panels to the UI
        add(tracksPanel, BorderLayout.NORTH);
        add(togglePanel, BorderLayout.CENTER);
        add(mapPanel, BorderLayout.SOUTH);

        //create the JFileChooser
        fc = new JFileChooser();
        fc.setCurrentDirectory(new File("./parserTestFiles"));
        FileFilter ff = new FileNameExtensionFilter("GPX Files", "gpx", "txt");
        fc.setFileFilter(ff);
        fc.setMultiSelectionEnabled(true);

        setJMenuBar(new UIMenuBar());
        setMinimumSize(new Dimension(400, 200));
        setSize(550, 500);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
    }

    private void displaySpeeds(GPSMetrics gpsMetrics){

        threeOut.setText(String.valueOf(gpsMetrics.getSpeed3()));
        sevenOut.setText(String.valueOf(gpsMetrics.getSpeed3to7()));
        tenOut.setText(String.valueOf(gpsMetrics.getSpeed7to10()));
        fifteenOut.setText(String.valueOf(gpsMetrics.getSpeed10to15()));
        twentyOut.setText(String.valueOf(gpsMetrics.getSpeed15to20()));
        aboveOut.setText(String.valueOf(gpsMetrics.getSpeed20()));
        trackTwoOut.setText(gpsMetrics.getTrackName());

        repaint();

    }

    private void displayMetrics(GPSMetrics gpsMetrics) {

        NumberFormat doubleFormat = new DecimalFormat("#0.00");
        double minE = 0.0;
        double maxE = 0.0;

        minE = gpsMetrics.getMinElevation();
        maxE = gpsMetrics.getMaxElevation();

        trackName.setText(gpsMetrics.getTrackName());
        minLatText.setText(String.valueOf(doubleFormat.format(gpsMetrics.getMinLatitude())) + "°");
        maxLatText.setText(String.valueOf(doubleFormat.format(gpsMetrics.getMaxLatitude())) + "°");
        minLonText.setText(String.valueOf(doubleFormat.format(gpsMetrics.getMinLongitude())) + "°");
        maxLonText.setText(String.valueOf(doubleFormat.format(gpsMetrics.getMaxLongitude())) + "°");
        minEleText.setText(String.valueOf(doubleFormat.format(minE)) + " m; " + doubleFormat.format(gpsMetrics.metersToFeet(minE)) + " ft");
        maxEleText.setText(String.valueOf(doubleFormat.format(maxE)) + " m; " + doubleFormat.format(gpsMetrics.metersToFeet(maxE)) + " ft");

        if(gpsMetrics.getTotalDistance() != -1) {
            double d = gpsMetrics.getTotalDistance();
            double a = gpsMetrics.getAverageSpeed();
            double m = gpsMetrics.getAverageSpeed();
            distance.setText(String.valueOf(doubleFormat.format(d)) + " km; " +
                    String.valueOf(doubleFormat.format(gpsMetrics.kilometersToMiles(d))) + " mi");
            avgSpeed.setText(String.valueOf(doubleFormat.format(gpsMetrics.mphToKph(a))) + " mph; " +
                    String.valueOf(doubleFormat.format(a)) + " Kph");
            maxSpeed.setText(String.valueOf(doubleFormat.format(gpsMetrics.mphToKph(m))) + " mph; " +
                    String.valueOf(doubleFormat.format(m)) + " Kph");
        }else{
            distance.setText("Distance can't be computed.");
            avgSpeed.setText("Average speed can't be computed.");
            maxSpeed.setText("Max Speed can't be computed.");
        }
        repaint();
    }

    /**
     * Sequence for loading a file. If the user had previously loaded files these must be cleared before
     * new files can be loaded. To protect against accidental deletion, the user is prompted for confirmation
     * before loading new files and clearing the old ones.
     */
    public void load(){
        if(filesLoaded){
            String[] options = {"Go Back", "Continue"};
            int response =  JOptionPane.showOptionDialog(this, "You have tracks loaded already. \n " +
                            "Continuing will clear all currently loaded tracks", "Tracks Currently Loaded",
                    JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE,
                    null, options, options[0]);

            if(response != 1) return; //the user did not select "Continue"
        }

        if(fc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION){
            loadedTracks.removeAllItems(); //clear any previously opened tracks
            List<File> files = Arrays.asList(fc.getSelectedFiles());
            for(GPSMetrics gpsMetrics : controller.analyzeFiles(files)){
                loadedTracks.addItem(gpsMetrics);
            }
        }
    }

    /**
     * Alerts the user of an error
     * @param message message to be displayed to the user
     */
    public void errorMessage(String message){
        JOptionPane.showMessageDialog(this, message, "Error", JOptionPane.ERROR_MESSAGE);
    }
    
    private void showGraph(){
    	controller.displayElevationGraph();
    }

    /**
     * Notifies Controller to display google map
     */
    private void showMap(){
        controller.displayGoogleMapUI();
    }

    /**
     * JMenuBar to hold actions the user may take
     * @author Connor Hibbs
     */
    private class UIMenuBar extends JMenuBar{
        public UIMenuBar(){
            JMenu file = new JMenu("File");
            addItem(file, "Load", e -> load());
            addItem(file, "Exit", e -> System.exit(0));
            add(file);
        }

        /**
         * Creates a JMenuItem and adds it to a JMenu
         * @param parent JMenu that the JMenuItem should be added to
         * @param text Text to appear on the JMenuItem
         * @param listener ActionListener responsible for handling clicks on the JMenuItem
         * @return the JMenuItem creates (often ignored)
         */
        public JMenuItem addItem(JMenu parent, String text, ActionListener listener){
            JMenuItem item = new JMenuItem(text);
            item.addActionListener(listener);
            parent.add(item);
            return item;
        }
    }
}