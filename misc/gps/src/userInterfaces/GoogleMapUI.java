/*
                   GNU LESSER GENERAL PUBLIC LICENSE
                       Version 3, 29 June 2007

 Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.


  This version of the GNU Lesser General Public License incorporates
the terms and conditions of version 3 of the GNU General Public
License, supplemented by the additional permissions listed below.

  0. Additional Definitions.

  As used herein, "this License" refers to version 3 of the GNU Lesser
General Public License, and the "GNU GPL" refers to version 3 of the GNU
General Public License.

  "The Library" refers to a covered work governed by this License,
other than an Application or a Combined Work as defined below.

  An "Application" is any work that makes use of an interface provided
by the Library, but which is not otherwise based on the Library.
Defining a subclass of a class defined by the Library is deemed a mode
of using an interface provided by the Library.

  A "Combined Work" is a work produced by combining or linking an
Application with the Library.  The particular version of the Library
with which the Combined Work was made is also called the "Linked
Version".

  The "Minimal Corresponding Source" for a Combined Work means the
Corresponding Source for the Combined Work, excluding any source code
for portions of the Combined Work that, considered in isolation, are
based on the Application, and not on the Linked Version.

  The "Corresponding Application Code" for a Combined Work means the
object code and/or source code for the Application, including any data
and utility programs needed for reproducing the Combined Work from the
Application, but excluding the System Libraries of the Combined Work.

  1. Exception to Section 3 of the GNU GPL.

  You may convey a covered work under sections 3 and 4 of this License
without being bound by section 3 of the GNU GPL.

  2. Conveying Modified Versions.

  If you modify a copy of the Library, and, in your modifications, a
facility refers to a function or data to be supplied by an Application
that uses the facility (other than as an argument passed when the
facility is invoked), then you may convey a copy of the modified
version:

   a) under this License, provided that you make a good faith effort to
   ensure that, in the event an Application does not supply the
   function or data, the facility still operates, and performs
   whatever part of its purpose remains meaningful, or

   b) under the GNU GPL, with none of the additional permissions of
   this License applicable to that copy.

  3. Object Code Incorporating Material from Library Header Files.

  The object code form of an Application may incorporate material from
a header file that is part of the Library.  You may convey such object
code under terms of your choice, provided that, if the incorporated
material is not limited to numerical parameters, data structure
layouts and accessors, or small macros, inline functions and templates
(ten or fewer lines in length), you do both of the following:

   a) Give prominent notice with each copy of the object code that the
   Library is used in it and that the Library and its use are
   covered by this License.

   b) Accompany the object code with a copy of the GNU GPL and this license
   document.

  4. Combined Works.

  You may convey a Combined Work under terms of your choice that,
taken together, effectively do not restrict modification of the
portions of the Library contained in the Combined Work and reverse
engineering for debugging such modifications, if you also do each of
the following:

   a) Give prominent notice with each copy of the Combined Work that
   the Library is used in it and that the Library and its use are
   covered by this License.

   b) Accompany the Combined Work with a copy of the GNU GPL and this license
   document.

   c) For a Combined Work that displays copyright notices during
   execution, include the copyright notice for the Library among
   these notices, as well as a reference directing the user to the
   copies of the GNU GPL and this license document.

   d) Do one of the following:

       0) Convey the Minimal Corresponding Source under the terms of this
       License, and the Corresponding Application Code in a form
       suitable for, and under terms that permit, the user to
       recombine or relink the Application with a modified version of
       the Linked Version to produce a modified Combined Work, in the
       manner specified by section 6 of the GNU GPL for conveying
       Corresponding Source.

       1) Use a suitable shared library mechanism for linking with the
       Library.  A suitable mechanism is one that (a) uses at run time
       a copy of the Library already present on the user's computer
       system, and (b) will operate properly with a modified version
       of the Library that is interface-compatible with the Linked
       Version.

   e) Provide Installation Information, but only if you would otherwise
   be required to provide such information under section 6 of the
   GNU GPL, and only to the extent that such information is
   necessary to install and execute a modified version of the
   Combined Work produced by recombining or relinking the
   Application with a modified version of the Linked Version. (If
   you use option 4d0, the Installation Information must accompany
   the Minimal Corresponding Source and Corresponding Application
   Code. If you use option 4d1, you must provide the Installation
   Information in the manner specified by section 6 of the GNU GPL
   for conveying Corresponding Source.)

  5. Combined Libraries.

  You may place library facilities that are a work based on the
Library side by side in a single library together with other library
facilities that are not Applications and are not covered by this
License, and convey such a combined library under terms of your
choice, if you do both of the following:

   a) Accompany the combined library with a copy of the same work based
   on the Library, uncombined with any other library facilities,
   conveyed under the terms of this License.

   b) Give prominent notice with the combined library that part of it
   is a work based on the Library, and explaining where to find the
   accompanying uncombined form of the same work.

  6. Revised Versions of the GNU Lesser General Public License.

  The Free Software Foundation may publish revised and/or new versions
of the GNU Lesser General Public License from time to time. Such new
versions will be similar in spirit to the present version, but may
differ in detail to address new problems or concerns.

  Each version is given a distinguishing version number. If the
Library as you received it specifies that a certain numbered version
of the GNU Lesser General Public License "or any later version"
applies to it, you have the option of following the terms and
conditions either of that published version or of any later version
published by the Free Software Foundation. If the Library as you
received it does not specify a version number of the GNU Lesser
General Public License, you may choose any version of the GNU Lesser
General Public License ever published by the Free Software Foundation.

  If the Library as you received it specifies that a proxy can decide
whether future versions of the GNU Lesser General Public License shall
apply, that proxy's public statement of acceptance of any version is
permanent authorization for you to choose that version for the
Library.
 */

package userInterfaces;

import com.google.maps.model.LatLng;
import gpxAnalyzer.Controller;
import gpxAnalyzer.GPSMetrics;
import gpxAnalyzer.GoogleAPIKey;

import java.awt.*;
import java.io.IOException;
import java.net.URL;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import com.google.maps.model.EncodedPolyline;

import java.util.HashMap;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.*;

/**
 * The GoogleMapUI class will display a static google map image of of all paths currently loaded.
 */
public class GoogleMapUI {

    /**
     * Maximum URL length allowed by google servers.
     */
    public static final int MAX_URL_LENGTH = 8196;

    /**
     * Controls debug output to console for testing.
     */
    public static final boolean DEBUG_MODE = true;

    /**
     * JLabel for displaying the google map image.
     */
    private JLabel mapLabel;

    /**
     * URL to retrieve google map.
     */
    private URL mapURL;

    /**
     * Initial String version of google map URL;
     */
    private String MAP_URL;

    /**
     * Current used version of google map URL
     */
    private String currentURL;

    /**
     * Array of available hex code colors to color different tracks.
     */
    private String[] colors;

    /**
     * Reference to controller object.
     */
    private Controller controller;

    /**
     * List of all GPSMetrics
     */
    private List<GPSMetrics> gpsMetricsList;

    /**
     * Hash map of reduced gpsMetrics points.
     */
    private HashMap<GPSMetrics, List<Point2D.Double>> criticalPointList;

    /**
     * Current google map type. (roadmap or satellite).
     */
    private String mapType;

    /**
     * Tracks the current index of available track colors.
     */
    private int colorIndex;

    /**
     * Tracks the current zoom level of the google map.
     */
    private int zoomLevel;

    /**
     * Tracks the center point of the google map.
     */
    private Point2D center;

    /**
     * Tracks the number of iterations reducing the track list points.
     */
    private int iterations;

    /**
     * Tracks the maximum critical value used to reduce track points.
     */
    private static double maxCriticalValue;

    /**
     * Pan amount for moving within the google map.
     */
    private static final double PAN_AMOUNT = .01;

    /**
     * Google api key used to retrieve static map.
     */
    private static final String GOOGLE_API_KEY = GoogleAPIKey.KEY;

    /**
     * Height of the map.
     */
    private int height;

    /**
     * Width of the map.
     */
    private int width;


    /** constructor
     * @param height map height
     * @param width map width
     */
    public GoogleMapUI(int height, int width, List<GPSMetrics> metricsList, Controller controller) {
        this.controller = controller;
        gpsMetricsList = metricsList;
        zoomLevel = 10;
        iterations = 0;
        this.height = height;
        this.width = width;
        maxCriticalValue = 1;
        mapType = "roadmap";
        criticalPointList = new HashMap<>();
        center = new Point2D.Double(43.0389, -87.9065);
        colors = new String[]{"0xC0392B", "0x9B59B6", "0x2980B9", "0x1ABC9C", "0xF1C40F",
                "0xF39C12", "0xCB4335", "0x154360", "0x1D8348", "0x6E2C00"};


        resetURL();
        initUI(width, height);
        loadCriticalPointMap();
        loadPaths();
    }

    /**
     * Resets the original Map_URL variable.
     */
    private void resetURL(){
        MAP_URL = "https://maps.googleapis.com/maps/api/staticmap?" +
                "size="+ width + "x" + height +
                "&maptype=" + mapType + "&key=" + GOOGLE_API_KEY;
    }

    /**
     * Loads the criticalPointList with all the point tracks from the gpsMetricsList.
     */
    private void loadCriticalPointMap() {
        for (GPSMetrics m : gpsMetricsList){
            criticalPointList.put(m, m.getPointList());
        }
    }

    /**
     * Calculates the center of a given GPSMetric.
     * @param metrics to calculate center from.
     * @return Point2D of track center.
     */
    private Point2D calculateCenter(GPSMetrics metrics) {
        double xMin = 0;
        double xMax = 0;
        double yMin = 0;
        double yMax = 0;
        ArrayList<Point2D.Double> points = metrics.getPointList();
        for( int i = 0; i < points.size(); i++ ) {
            if (i == 0){
                xMin = points.get(i).getX();
                xMax = points.get(i).getX();
                yMin = points.get(i).getY();
                yMax = points.get(i).getY();
            } else {
                xMax = Math.max(points.get(i).getX(), xMax);
                xMin = Math.min(points.get(i).getX(), xMin);
                yMin = Math.min(points.get(i).getY(), yMin);
                yMax = Math.max(points.get(i).getY(), yMax);
            }
        }
        return new Point2D.Double((xMax + xMin) / 2, (yMax + yMin) / 2);
    }

    /**
     * Initializes all necessary UI elements.
     * @param width of JFrame.
     * @param height of JFrame.
     */
    private void initUI(int width, int height){
        JFrame frame = new JFrame();
        frame.setLayout(new FlowLayout());
        frame.setTitle("GPS Tracks");
        frame.setSize(width, height);
        mapLabel = new JLabel(new ImageIcon());

        Panel controlPanel = new Panel(new FlowLayout());
        JButton zoomInButton = new JButton("Zoom in");
        JButton zoomOutButton = new JButton("Zoom out");
        JButton toggleMapButton = new JButton("Toggle map type");
        toggleMapButton.addActionListener(e-> toggleMapType());
        zoomInButton.addActionListener(e-> zoomIn());
        zoomOutButton.addActionListener(e-> zoomOut());
        controlPanel.add(toggleMapButton);
        controlPanel.add(zoomInButton);
        controlPanel.add(zoomOutButton);

        JButton upButton = new JButton("up");
        JButton downButton = new JButton("down");
        JButton leftButton = new JButton("left");
        JButton rightButton = new JButton("right");

        Panel directions = new Panel(new BorderLayout());
        directions.add(upButton, BorderLayout.NORTH);
        directions.add(downButton, BorderLayout.SOUTH);
        directions.add(leftButton, BorderLayout.WEST);
        directions.add(rightButton, BorderLayout.EAST);

        upButton.addActionListener(e-> panUp());
        downButton.addActionListener(e-> panDown());
        leftButton.addActionListener(e-> panLeft());
        rightButton.addActionListener(e-> panRight());

        controlPanel.add(directions);
        frame.add(controlPanel);
        frame.add(mapLabel);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    /**
     * Pans the map image upward.
     */
    private void panUp(){
        center.setLocation(center.getX() + PAN_AMOUNT, center.getY());
        updateCenter();
    }

    /**
     * pans the map image downward.
     */
    private void panDown(){
        center.setLocation(center.getX() - PAN_AMOUNT, center.getY());
        updateCenter();
    }

    /**
     * Pans the map image left.
     */
    private void panLeft(){
        center.setLocation(center.getX(), center.getY() - PAN_AMOUNT);
        updateCenter();
    }

    /**
     * Pans the map image right.
     */
    private void panRight(){
        center.setLocation(center.getX(), center.getY() + PAN_AMOUNT);
        updateCenter();
    }

    /**
     * Updates the google map with the current center point.
     */
    private void updateCenter(){
        updateURL(insertUMLParameter(currentURL, "&center=", center.getX() + "," + center.getY()));
        SwingUtilities.invokeLater(this::drawMap);
    }

    /**
     * Toggles the current map type between roadmap and satellite.
     */
    private void toggleMapType(){
        mapType = mapType.equals("satellite") ? "roadmap" : "satellite";
        updateURL(insertUMLParameter(currentURL, "&maptype=", mapType));
        SwingUtilities.invokeLater(this::drawMap);
    }

    /**
     * Inserts a url parameter within the given url.
     * @param url to insert parameter in.
     * @param parameterID id of the url parameter.
     * @param parameter to add to the url.
     * @return String representation of the url with inserted parameter.
     */
    private String insertUMLParameter(String url, String parameterID, String parameter){
        String result;
        if (url.contains(parameterID)) {
            String firstHalf = url.substring(0, url.indexOf(parameterID));
            String secondHalf = url.substring(url.indexOf(parameterID) + parameterID.length(),
                    url.length());
            secondHalf = secondHalf.substring(secondHalf.indexOf('&') >= 0 ? secondHalf.indexOf
                    ('&') : secondHalf.length());
            result = firstHalf + parameterID + parameter + secondHalf;
        } else {
            result = url + parameterID + parameter;
        }
        return result;
    }

    /**
     * zooms the map image in.
     */
    private void zoomIn(){
        updateZoomAmount(++zoomLevel);
    }

    /**
     * zooms the map image out.
     */
    private void zoomOut(){
        if (zoomLevel >= 0){
            updateZoomAmount(--zoomLevel);
        }
    }

    /**
     * Updates the map with the current zoom amount.
     * @param amount of zoom.
     */
    private void updateZoomAmount(int amount){
        updateURL(insertUMLParameter(currentURL, "&zoom=", "" + amount));
        SwingUtilities.invokeLater(this::drawMap);
    }

    /**
     * Loads all paths from the GPSMetrics.
     */
    private void loadPaths(){
        for(GPSMetrics m : gpsMetricsList){
            addPathToURL(m, getNextColor());
        }
        updateZoomAmount(zoomLevel);
        SwingUtilities.invokeLater(this::drawMap);
    }

    /**
     * Retrieves the next color for the map paths.
     * @return hex code of a url color parameter.
     */
    private String getNextColor(){
        if (colorIndex == colors.length -1){
            colorIndex = 0;
        }
        return colors[colorIndex++];
    }

    /**
     * adds a path to display on the static google map.
     * @param metrics to retrieve path from.
     * @param color to display path in.
     * @throws IllegalArgumentException if the metrics object is null.
     */
    private void addPathToURL(GPSMetrics metrics, String color) throws IllegalArgumentException {
        if(metrics == null) {
            throw new IllegalArgumentException("GoogleMapDemo.addPathToURL(): path cannot be null");
        }

        // see https://developers.google.com/maps/documentation/static-maps/intro#Paths
        String pathString = "path=color:" + color +"|weight:5";

        pathString += "|enc:";
        ArrayList<LatLng> points = new ArrayList<>();
        criticalPointList.put(metrics, trimPointList(criticalPointList.get(metrics)));
        for(Point2D p: criticalPointList.get(metrics)) {
            points.add(new LatLng(p.getX(), p.getY()));
        }
        EncodedPolyline polyline = new EncodedPolyline(points);
        pathString += polyline.getEncodedPath(); // get polyline encodedPath for URL

        String url = MAP_URL += "&" + pathString;
        if (metrics.getPointList().size() > 1) {
            center = calculateCenter(metrics);
        }
        url += "&center=" + center.getX() + "," + center.getY();
        updateURL(url);
    }

    /**
     * Trims a list of non-critical points values.
     * @param points list of points to be reduced.
     * @return list with reduced number of points.
     */
    private List<Point2D.Double> trimPointList(List<Point2D.Double> points){
        for (int i = 2; i < points.size() - 3; i++) {
            if (Math.abs(getCriticalValue(points.get(i), points.get(i + 1), points.get(i + 2)))
                    < maxCriticalValue) {
                points.remove(i + 1);
            }
        }
        return points;
    }

    /**
     * Calculates the critical value of 3 given points.
     * @param first point in a path.
     * @param second middle point in a path.
     * @param third point in a path.
     * @return the critical value of the second point.
     */
    private static double getCriticalValue(Point2D first, Point2D second, Point2D third){
        double distance1to2 = getDistance(Math.abs(first.getX()-second.getX()),
                Math.abs(first.getY()-second.getY()));
        double distance2to3 = getDistance(Math.abs(second.getX()-third.getX()),
                Math.abs(second.getY()-third.getY()));
        double distance1to3 = getDistance(Math.abs(first.getX()-third.getX()),
                Math.abs(first.getY()-third.getY()));
        return distance1to2 + distance2to3 - distance1to3;
    }

    /**
     * Uses the pythagorean theorem to calculate the distance of a hypotenuse.
     * @param a length.
     * @param b length.
     * @return distance of hypotenuse.
     */
    private static double getDistance(double a, double b){
        return Math.sqrt(Math.pow(a,2) + Math.pow(b,2));
    }


    /**
     * Updates the URL object with provided string url.
     * @param url to update mapURL with.
     */
    private void updateURL(String url){
        try {
            currentURL = url;
            mapURL =  new URL(url);
        } catch (IOException e) {
            controller.errorMessage(e.getMessage());
        }
    }

    /**
     * Updates the map with the latest path information
     * Note: should be called on event-dispatch thread
     */
    private void drawMap() {
        if (DEBUG_MODE) {
            System.out.println("DEBUG: map url=" + mapURL.toString());
        }
        try {
            if (mapURL.toString().length() <= MAX_URL_LENGTH || iterations >= 10){
                mapLabel.setIcon(new ImageIcon(ImageIO.read(mapURL)));
            } else if (iterations++ < 10){
                maxCriticalValue += .1;
                resetURL();
                loadPaths();
            }

        } catch (IOException e) {
            if (DEBUG_MODE) {
                e.printStackTrace();
            }
            if (mapURL.toString().length() > MAX_URL_LENGTH){
                controller.errorMessage("URL too large, please load smaller file(s)");
                if (DEBUG_MODE){
                    System.out.println("URL Length: " + mapURL.toString().length());
                }
            } else {
                controller.errorMessage(e.getMessage());
            }
        }
    }

}
