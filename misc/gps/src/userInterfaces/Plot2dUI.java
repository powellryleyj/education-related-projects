/*
                   GNU LESSER GENERAL PUBLIC LICENSE
                       Version 3, 29 June 2007

 Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.


  This version of the GNU Lesser General Public License incorporates
the terms and conditions of version 3 of the GNU General Public
License, supplemented by the additional permissions listed below.

  0. Additional Definitions.

  As used herein, "this License" refers to version 3 of the GNU Lesser
General Public License, and the "GNU GPL" refers to version 3 of the GNU
General Public License.

  "The Library" refers to a covered work governed by this License,
other than an Application or a Combined Work as defined below.

  An "Application" is any work that makes use of an interface provided
by the Library, but which is not otherwise based on the Library.
Defining a subclass of a class defined by the Library is deemed a mode
of using an interface provided by the Library.

  A "Combined Work" is a work produced by combining or linking an
Application with the Library.  The particular version of the Library
with which the Combined Work was made is also called the "Linked
Version".

  The "Minimal Corresponding Source" for a Combined Work means the
Corresponding Source for the Combined Work, excluding any source code
for portions of the Combined Work that, considered in isolation, are
based on the Application, and not on the Linked Version.

  The "Corresponding Application Code" for a Combined Work means the
object code and/or source code for the Application, including any data
and utility programs needed for reproducing the Combined Work from the
Application, but excluding the System Libraries of the Combined Work.

  1. Exception to Section 3 of the GNU GPL.

  You may convey a covered work under sections 3 and 4 of this License
without being bound by section 3 of the GNU GPL.

  2. Conveying Modified Versions.

  If you modify a copy of the Library, and, in your modifications, a
facility refers to a function or data to be supplied by an Application
that uses the facility (other than as an argument passed when the
facility is invoked), then you may convey a copy of the modified
version:

   a) under this License, provided that you make a good faith effort to
   ensure that, in the event an Application does not supply the
   function or data, the facility still operates, and performs
   whatever part of its purpose remains meaningful, or

   b) under the GNU GPL, with none of the additional permissions of
   this License applicable to that copy.

  3. Object Code Incorporating Material from Library Header Files.

  The object code form of an Application may incorporate material from
a header file that is part of the Library.  You may convey such object
code under terms of your choice, provided that, if the incorporated
material is not limited to numerical parameters, data structure
layouts and accessors, or small macros, inline functions and templates
(ten or fewer lines in length), you do both of the following:

   a) Give prominent notice with each copy of the object code that the
   Library is used in it and that the Library and its use are
   covered by this License.

   b) Accompany the object code with a copy of the GNU GPL and this license
   document.

  4. Combined Works.

  You may convey a Combined Work under terms of your choice that,
taken together, effectively do not restrict modification of the
portions of the Library contained in the Combined Work and reverse
engineering for debugging such modifications, if you also do each of
the following:

   a) Give prominent notice with each copy of the Combined Work that
   the Library is used in it and that the Library and its use are
   covered by this License.

   b) Accompany the Combined Work with a copy of the GNU GPL and this license
   document.

   c) For a Combined Work that displays copyright notices during
   execution, include the copyright notice for the Library among
   these notices, as well as a reference directing the user to the
   copies of the GNU GPL and this license document.

   d) Do one of the following:

       0) Convey the Minimal Corresponding Source under the terms of this
       License, and the Corresponding Application Code in a form
       suitable for, and under terms that permit, the user to
       recombine or relink the Application with a modified version of
       the Linked Version to produce a modified Combined Work, in the
       manner specified by section 6 of the GNU GPL for conveying
       Corresponding Source.

       1) Use a suitable shared library mechanism for linking with the
       Library.  A suitable mechanism is one that (a) uses at run time
       a copy of the Library already present on the user's computer
       system, and (b) will operate properly with a modified version
       of the Library that is interface-compatible with the Linked
       Version.

   e) Provide Installation Information, but only if you would otherwise
   be required to provide such information under section 6 of the
   GNU GPL, and only to the extent that such information is
   necessary to install and execute a modified version of the
   Combined Work produced by recombining or relinking the
   Application with a modified version of the Linked Version. (If
   you use option 4d0, the Installation Information must accompany
   the Minimal Corresponding Source and Corresponding Application
   Code. If you use option 4d1, you must provide the Installation
   Information in the manner specified by section 6 of the GNU GPL
   for conveying Corresponding Source.)

  5. Combined Libraries.

  You may place library facilities that are a work based on the
Library side by side in a single library together with other library
facilities that are not Applications and are not covered by this
License, and convey such a combined library under terms of your
choice, if you do both of the following:

   a) Accompany the combined library with a copy of the same work based
   on the Library, uncombined with any other library facilities,
   conveyed under the terms of this License.

   b) Give prominent notice with the combined library that part of it
   is a work based on the Library, and explaining where to find the
   accompanying uncombined form of the same work.

  6. Revised Versions of the GNU Lesser General Public License.

  The Free Software Foundation may publish revised and/or new versions
of the GNU Lesser General Public License from time to time. Such new
versions will be similar in spirit to the present version, but may
differ in detail to address new problems or concerns.

  Each version is given a distinguishing version number. If the
Library as you received it specifies that a certain numbered version
of the GNU Lesser General Public License "or any later version"
applies to it, you have the option of following the terms and
conditions either of that published version or of any later version
published by the Free Software Foundation. If the Library as you
received it does not specify a version number of the GNU Lesser
General Public License, you may choose any version of the GNU Lesser
General Public License ever published by the Free Software Foundation.

  If the Library as you received it specifies that a proxy can decide
whether future versions of the GNU Lesser General Public License shall
apply, that proxy's public statement of acceptance of any version is
permanent authorization for you to choose that version for the
Library.
 */

package userInterfaces;

import gpxAnalyzer.Controller;
import gpxAnalyzer.GPSMetrics;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;
import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.awt.geom.Point2D;


/**
 * The Plot2dUI class displays a list of currently loaded tracks to allow the user to select any
 * number of listed tracks to be plotted on a 2d graph.
 */
public class Plot2dUI {

    /**
     * JFreeChart object used to draw tracks.
     */
    private JFreeChart plotter;

    /**
     * JList for the user to select a track.
     */
    private JList<String> tracksList;

    /**
     * Reference to controller object.
     */
    private Controller controller;

    /**
     * List of all given track metrics.
     */
    private Map<String, GPSMetrics> metricsMap;

    /**
     * List of all track names.
     */
    private List<String> data;

    /**
     * Max x coordinate value.
     */
    private double maxX;

    /**
     * Max y coordinate value.
     */
    private double maxY;

    /**
     * Min x coordinate value.
     */
    private double minX;

    /**
     * Min y coordinate value.
     */
    private double minY;

    /**
     * Array of available colors to color different tracks.
     */
    private Color[] colors;

    /**
     * Value to offset initial track x coordinate point to 0.
     */
    private double xOffset;

    /**
     * Value to offset initial track y coordinate point to 0.
     */
    private double yOffset;

    /**
     * Radius of the earth used to converting lat and lon to cartesian coordinates.
     */
    private static final int EARTH_RADIUS = 3959;

    /**
     * Constructs a Plot2dUI object.
     * @param metricsList list of track metrics to be plotted.
     * @param controller reference to controller object.
     */
    public Plot2dUI(List<GPSMetrics> metricsList, Controller controller){
        this.controller = controller;
        metricsMap = new HashMap<>();
        colors = new Color[]{Color.blue, Color.cyan, Color.green, Color.magenta,
                Color.orange, Color.pink, Color.red, Color.yellow, Color.darkGray, Color.gray};
        resetMinMax();
        loadMetricsMap(metricsList);
        initGUI();
    }

    /**
     * Resets the min and max values for re-evaluation.
     */
    private void resetMinMax() {
        maxX = Double.NEGATIVE_INFINITY;
        maxY = Double.NEGATIVE_INFINITY;
        minX = Double.POSITIVE_INFINITY;
        minY = Double.POSITIVE_INFINITY;
    }

    /**
     * Loads the metrics list into the hashmap.
     * @param metricsList to be loaded into the hashmap.
     */
    private void loadMetricsMap(List<GPSMetrics> metricsList) {
        metricsList.forEach(m -> {
            if (m.getPointList().size() > 0) {
                metricsMap.put(m.getTrackName() + " " + m.getDistanceAsString(), m);
            }
        });
    }

    /**
     * Initializes all necessary gui components.
     */
    private void initGUI() {
        JFrame frame = new JFrame("2D track plot");
        frame.setLayout(new BorderLayout());
        frame.setSize(400, 800);
        data = new ArrayList<>(metricsMap.keySet());
        tracksList = new JList<>(data.toArray(new String[metricsMap.size()]));
        tracksList.setCellRenderer((JList<? extends String> list, String value, int index,
                                    boolean isSelected, boolean cellHasFocus) -> {
            JLabel label = new JLabel(value);
            if (isSelected) {
                if (index < colors.length) {
                    label.setForeground(colors[index]);
                    System.out.println("Color set");
                }
                System.out.println(value + " Selected");
            }
            return label;
        });
        frame.add(tracksList, BorderLayout.CENTER);

        JButton plotButton = new JButton("Plot tracks");
        plotButton.addActionListener(e -> plotTracks());

        JPanel buttonPanel = new JPanel(new FlowLayout());
        buttonPanel.add(plotButton);
        frame.add(buttonPanel, BorderLayout.SOUTH);

        frame.setVisible(true);
    }

    /**
     * Button listener to plot tracks.
     */
    private void plotTracks() {
        if (!tracksList.isSelectionEmpty()) {
            initPlotter();
        } else {
            controller.errorMessage("Please select at least 1 track to plot.");
        }
    }

    /**
     * Plots all selected tracks.
     */
    private XYDataset plotSelectedTracks(XYLineAndShapeRenderer renderer) {
        XYSeriesCollection dataSet = new XYSeriesCollection();
        tracksList.getSelectedValuesList().forEach(track -> plotTrack(track, renderer, dataSet));
        return dataSet;
    }

    /**
     * Plots a given track.
     * @param track name of the track to be plotted.
     */
    private void plotTrack(String track, XYLineAndShapeRenderer renderer,
                           XYSeriesCollection dataSet) {
        if (metricsMap.get(track).getPointList().size() > 0) {
            int index = tracksList.getSelectedValuesList().indexOf(track);
            renderer.setSeriesPaint(index, colors[data.indexOf(track)]);
            XYSeries series = new XYSeries(track, false);
            metricsMap.get(track).getPointList().forEach(point ->
                    series.add(getCartesianX(point) + xOffset, getCartesianY(point) + yOffset));
            dataSet.addSeries(series);
        }
    }

    /**
     * Find the min and max coordinates to be used by the winPlotter.
     * @param tracks the list of selected tracks.
     */
    private void findMinMaxCoordinates(List<String> tracks) {
        for (String track : tracks){
            for (Point2D.Double point : metricsMap.get(track).getPointList()){
                maxX = Math.max(maxX, getCartesianX(point));
                maxY = Math.max(maxY, getCartesianY(point));
                minX = Math.min(minX, getCartesianX(point));
                minY = Math.min(minY, getCartesianY(point));
            }
        }
        normalizeMinMaxCoordinates();
        setOffsetValues(tracks);
    }

    /**
     * Sets the offset values of the x and y coordinates to plot first selected track at
     * coordinate 0,0.
     * @param tracks list of selected tracks.
     */
    private void setOffsetValues(List<String> tracks) {
        if (metricsMap.get(tracks.get(0)).getPointList().size() > 0) {
            xOffset = reverseSign(getCartesianX(metricsMap.get(tracks.get(0)).getPointList().get(0)));
            yOffset = reverseSign(getCartesianY(metricsMap.get(tracks.get(0)).getPointList().get(0)));
        } else {
            xOffset = 0;
            yOffset = 0;
        }
    }

    /**
     * Normalizes the min and max coordinates to equal each other.
     */
    private void normalizeMinMaxCoordinates() {
        double range = Math.abs(Math.max(maxX - minX, maxY - minY));
        range = range > 0 ? range : 1;
        maxX = range;
        maxY = range;
        minX = reverseSign(range);
        minY = reverseSign(range);
    }

    /**
     * Converts the given lat and lon to a Cartesian Y coordinate.
     * Formula from: http://sciencing.com/convert-xy-coordinates-longitude-latitude-8449009.html
     * @param point Point2D.Double of lat and lon coordinates.
     * @return Cartesian Y Coordinate.
     */
    public static double getCartesianY(Point2D.Double point){
        return EARTH_RADIUS * Math.cos(Math.toRadians(point.getX()))
                * Math.sin(Math.toRadians(point.getY()));
    }

    /**
     * Converts the given lat and lon to a Cartesian X coordinate.
     * Formula from: http://sciencing.com/convert-xy-coordinates-longitude-latitude-8449009.html
     * @param point Point2D.Double of lat and lon coordinates.
     * @return Cartesian X Coordinate.
     */
    public static double getCartesianX(Point2D.Double point){
        return EARTH_RADIUS * Math.cos(Math.toRadians(point.getX()))
                * Math.cos(Math.toRadians(point.getY()));
    }

    /**
     * Reverses the sign of the given double.
     * @param num the double to be calculated.
     * @return the double with the sign reversed.
     */
    private double reverseSign(double num) {
        return num > 0 ? 0 - num : Math.abs(num);
    }

    /**
     * Initializes the WinPlotter.
     */
    private void initPlotter() {
        resetMinMax();
        findMinMaxCoordinates(tracksList.getSelectedValuesList());
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        plotter = ChartFactory.createScatterPlot(
                "2d plot of tracks" ,
                "X" ,
                "Y" ,
                plotSelectedTracks(renderer));
        JFrame frame = new JFrame("Plotted Tracks");
        ChartPanel chartPanel = new ChartPanel(plotter);
        chartPanel.setPreferredSize(new Dimension(600, 600));
        XYPlot plot = plotter.getXYPlot();
        plot.getRangeAxis().setRange(minY, maxY);
        plot.getDomainAxis().setRange(minX, maxX);
        plot.setRenderer(renderer);
        frame.setContentPane(chartPanel);
        frame.pack();
        RefineryUtilities.centerFrameOnScreen(frame);
        frame.setVisible(true);
    }


}
