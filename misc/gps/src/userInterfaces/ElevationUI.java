/*
                   GNU LESSER GENERAL PUBLIC LICENSE
                       Version 3, 29 June 2007

 Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.


  This version of the GNU Lesser General Public License incorporates
the terms and conditions of version 3 of the GNU General Public
License, supplemented by the additional permissions listed below.

  0. Additional Definitions.

  As used herein, "this License" refers to version 3 of the GNU Lesser
General Public License, and the "GNU GPL" refers to version 3 of the GNU
General Public License.

  "The Library" refers to a covered work governed by this License,
other than an Application or a Combined Work as defined below.

  An "Application" is any work that makes use of an interface provided
by the Library, but which is not otherwise based on the Library.
Defining a subclass of a class defined by the Library is deemed a mode
of using an interface provided by the Library.

  A "Combined Work" is a work produced by combining or linking an
Application with the Library.  The particular version of the Library
with which the Combined Work was made is also called the "Linked
Version".

  The "Minimal Corresponding Source" for a Combined Work means the
Corresponding Source for the Combined Work, excluding any source code
for portions of the Combined Work that, considered in isolation, are
based on the Application, and not on the Linked Version.

  The "Corresponding Application Code" for a Combined Work means the
object code and/or source code for the Application, including any data
and utility programs needed for reproducing the Combined Work from the
Application, but excluding the System Libraries of the Combined Work.

  1. Exception to Section 3 of the GNU GPL.

  You may convey a covered work under sections 3 and 4 of this License
without being bound by section 3 of the GNU GPL.

  2. Conveying Modified Versions.

  If you modify a copy of the Library, and, in your modifications, a
facility refers to a function or data to be supplied by an Application
that uses the facility (other than as an argument passed when the
facility is invoked), then you may convey a copy of the modified
version:

   a) under this License, provided that you make a good faith effort to
   ensure that, in the event an Application does not supply the
   function or data, the facility still operates, and performs
   whatever part of its purpose remains meaningful, or

   b) under the GNU GPL, with none of the additional permissions of
   this License applicable to that copy.

  3. Object Code Incorporating Material from Library Header Files.

  The object code form of an Application may incorporate material from
a header file that is part of the Library.  You may convey such object
code under terms of your choice, provided that, if the incorporated
material is not limited to numerical parameters, data structure
layouts and accessors, or small macros, inline functions and templates
(ten or fewer lines in length), you do both of the following:

   a) Give prominent notice with each copy of the object code that the
   Library is used in it and that the Library and its use are
   covered by this License.

   b) Accompany the object code with a copy of the GNU GPL and this license
   document.

  4. Combined Works.

  You may convey a Combined Work under terms of your choice that,
taken together, effectively do not restrict modification of the
portions of the Library contained in the Combined Work and reverse
engineering for debugging such modifications, if you also do each of
the following:

   a) Give prominent notice with each copy of the Combined Work that
   the Library is used in it and that the Library and its use are
   covered by this License.

   b) Accompany the Combined Work with a copy of the GNU GPL and this license
   document.

   c) For a Combined Work that displays copyright notices during
   execution, include the copyright notice for the Library among
   these notices, as well as a reference directing the user to the
   copies of the GNU GPL and this license document.

   d) Do one of the following:

       0) Convey the Minimal Corresponding Source under the terms of this
       License, and the Corresponding Application Code in a form
       suitable for, and under terms that permit, the user to
       recombine or relink the Application with a modified version of
       the Linked Version to produce a modified Combined Work, in the
       manner specified by section 6 of the GNU GPL for conveying
       Corresponding Source.

       1) Use a suitable shared library mechanism for linking with the
       Library.  A suitable mechanism is one that (a) uses at run time
       a copy of the Library already present on the user's computer
       system, and (b) will operate properly with a modified version
       of the Library that is interface-compatible with the Linked
       Version.

   e) Provide Installation Information, but only if you would otherwise
   be required to provide such information under section 6 of the
   GNU GPL, and only to the extent that such information is
   necessary to install and execute a modified version of the
   Combined Work produced by recombining or relinking the
   Application with a modified version of the Linked Version. (If
   you use option 4d0, the Installation Information must accompany
   the Minimal Corresponding Source and Corresponding Application
   Code. If you use option 4d1, you must provide the Installation
   Information in the manner specified by section 6 of the GNU GPL
   for conveying Corresponding Source.)

  5. Combined Libraries.

  You may place library facilities that are a work based on the
Library side by side in a single library together with other library
facilities that are not Applications and are not covered by this
License, and convey such a combined library under terms of your
choice, if you do both of the following:

   a) Accompany the combined library with a copy of the same work based
   on the Library, uncombined with any other library facilities,
   conveyed under the terms of this License.

   b) Give prominent notice with the combined library that part of it
   is a work based on the Library, and explaining where to find the
   accompanying uncombined form of the same work.

  6. Revised Versions of the GNU Lesser General Public License.

  The Free Software Foundation may publish revised and/or new versions
of the GNU Lesser General Public License from time to time. Such new
versions will be similar in spirit to the present version, but may
differ in detail to address new problems or concerns.

  Each version is given a distinguishing version number. If the
Library as you received it specifies that a certain numbered version
of the GNU Lesser General Public License "or any later version"
applies to it, you have the option of following the terms and
conditions either of that published version or of any later version
published by the Free Software Foundation. If the Library as you
received it does not specify a version number of the GNU Lesser
General Public License, you may choose any version of the GNU Lesser
General Public License ever published by the Free Software Foundation.

  If the Library as you received it specifies that a proxy can decide
whether future versions of the GNU Lesser General Public License shall
apply, that proxy's public statement of acceptance of any version is
permanent authorization for you to choose that version for the
Library.
 */

package userInterfaces;


import gpxAnalyzer.Controller;
import gpxAnalyzer.GPSMetrics;
import gpxAnalyzer.GPSPoint;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RefineryUtilities;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;

/**
 * user interface to display a graph of the gps elevations as a function of time
 * @author Ryley Powell (powellr)
 * @version 1.0
 */
public class ElevationUI {

    //Frame Variables
    private JFrame frame;
    private JList<String> tracksList;
    private ChartPanel chartPanel;

    //Frame Constants
    private static final int FRAME_HEIGHT = 300, FRAME_WIDTH = 400;
    private static final String FRAME_TITLE = "Elevation Line Chart";

    //Chart Constants
    private static final String X_AXIS_TITLE_TIME = "Time (minutes)",
                                Y_AXIS_TITLE = "Elevation (meters)",
                                CHART_TITLE_TIME = "Elevation vs Time",
                                X_AXIS_TITLE_DISTANCE_KM = "Distance (kilometers)",
                                X_AXIS_TITLE_DISTANCE_MILES = "Distance (miles)",
                                CHART_TITLE_DISTANCE = "Elevation vs Distance";
    private static final int CHART_HEIGHT = 300, CHART_WIDTH = 400;

    //Chart Variables
    private long maxTime, minTime;
    private double maxElevation, minElevation, minDistance, maxDistance;
    private boolean functionOfDistanceOrTimeToggle, distanceUnitToggle;

    //Series Variables
    private List<GPSMetrics> gpsMetricsList;
    private Map<String, GPSMetrics> metricsMap;
    private Map<String, List<Double>> elevationMap;
    private Map<String, List<Long>> timeMap;
    private Map<String, List<Double>> distanceMapKM;
    private Map<String, List<Double>> distanceMapMiles;
    private List<String> metricsNamesList;

    //Series Constants
    private static final List<Color> LINE_COLORS = Collections.unmodifiableList(
        new ArrayList<Color>(10) {{
            add(new Color(200,0,0));
            add(new Color(255, 150, 0));
            add(new Color(255, 225, 0));
            add(new Color(0,160,0));
            add(new Color(0,0,150));
            add(new Color(150, 0, 150));
            add(new Color(0,200,255));
            add(new Color(0, 240, 0));
            add(new Color(255, 0, 255));
            add(new Color(125,0,0));
        }}
    );

    //Distance Constants
    private static final int EARTH_RADIUS = 6371;

    //Callback Variables
    private Controller controller;

    //Debugging Constant
    private final boolean DEBUGGING = true;

    /**
     * constructor; initializes all lists and maps used within the class.
     * @param metrics - the list of metric's files to be loaded into the UI
     * @param c - a reference to the main controller to be used for callback functions
     */
    public ElevationUI(List<GPSMetrics> metrics, Controller c){
        try {
            this.controller = c;
            this.gpsMetricsList = getGPSMetricsList(metrics);
            this.metricsMap = getMetricsMap(gpsMetricsList);
            this.metricsNamesList = getMetricsNamesList();
            this.elevationMap = getElevationMap(gpsMetricsList);
            this.timeMap = getTimeMap(gpsMetricsList);
            this.distanceMapKM = getDistanceMapKM(gpsMetricsList);
            this.distanceMapMiles = getDistanceMapMiles();
            this.functionOfDistanceOrTimeToggle = true;
            this.distanceUnitToggle = true;
        } catch (Exception e) {
            if(DEBUGGING) {
                e.printStackTrace();
            }
            controller.errorMessage(e.getMessage());
        }
        initGUI();
    }

    /**
     * worker method to sort out all metrics files that do not contain 2 or more points of data
     * @param metrics - a list of metrics files
     * @return a list of metrics files that contain enough data to graph
     */
    private List<GPSMetrics> getGPSMetricsList(List<GPSMetrics> metrics) {
        List<GPSMetrics> temp = new ArrayList<>();
        metrics.forEach(m -> {
            if(m.getPointList().size() > 1) {
                temp.add(m);
            } else {
                controller.errorMessage("Plot cannot be generated from files that do not contain enough data.");
            }
        });
        return temp;
    }

    /**
     * worker method that creates a map associating the track name of each metrics file with its respective metrics file
     * @param metrics - the list of metrics loaded into the UI
     * @return a map containing pairings of track names to metric files
     */
    private HashMap<String, GPSMetrics> getMetricsMap(List<GPSMetrics> metrics) {
        HashMap<String, GPSMetrics> temp = new HashMap<>();
        metrics.forEach(m -> {
            if (m.getPointList().size() > 1) {
                temp.put(m.getTrackName(), m);
            } else {
                controller.errorMessage("Plot cannot be generated from files that do not contain enough data.");
            }
        });
        return temp;
    }

    /**
     * worker method that puts all the metric track names into a list; used for syncing the colors of lines and
     * JList options together
     * @return a list containing all the metrics track names
     */
    private List<String> getMetricsNamesList() {
        List<String> temp = new ArrayList<>();
        metricsMap.keySet().forEach(m -> {
            if (metricsMap.get(m).getPointList().size() > 0) {
                temp.add(m);
            } else {
                controller.errorMessage("Plot cannot be generated from files that contain no data.");
            }
        });
        return temp;
    }

    /**
     * worker method that creates a map associating the track name of each metrics file with its respective list of
     * elevations
     * @param metrics - the list of metrics loaded into the UI
     * @return a map containing pairs of track names to elevations
     */
    private HashMap<String, List<Double>> getElevationMap(List<GPSMetrics> metrics) {
        HashMap<String, List<Double>> temp = new HashMap<>();
        metrics.forEach(m -> {
            if (m.getPointList().size() > 0) {
                temp.put(m.getTrackName(), m.getElevationList());
            } else {
                controller.errorMessage("There are not enough points within "
                        + m.getTrackName()
                        + " to display any data.");
            }
        });
        return temp;
    }

    /**
     * worker method that finds the total elevation gain over the list of elevations passed in; namely this method with
     * all to the total gain if the current index is of greater value than the preceding index and will do nothing
     * if the value is lower
     * @param elevations - the list of elevations of a particular metrics file
     * @return the total elevation gain in meters
     */
    private double getTotalElevationGain(List<Double> elevations) {
        double totalGain = 0, previousEle = elevations.get(0), currEle;
        for(int i = 1; i < elevations.size(); i++) {
            currEle = elevations.get(i);
            if(currEle > previousEle) {
                totalGain += currEle;
            }
            previousEle = currEle;
        }
        return totalGain;
    }

    /**
     * worker method that creates a map associating the track name of each metrics file with its respective list of
     * times
     * @param metrics - the list of metrics loaded into the UI
     * @return a map containing pairs of track names to times
     */
    private HashMap<String, List<Long>> getTimeMap(List<GPSMetrics> metrics) {
        HashMap<String, List<Long>> temp = new HashMap<>();
        metrics.forEach(m -> {
            if(m.getPointList().size() > 0) {
                try {
                    temp.put(m.getTrackName(), normalizeTimes(parseTimes(m.getUnparsedTimes())));
                } catch (ParseException | IllegalArgumentException e) {
                    if(DEBUGGING) {
                        e.printStackTrace();
                    }
                    controller.errorMessage(e.getMessage());
                    temp.remove(m.getTrackName());
                    metricsMap.remove(m.getTrackName());
                }
            } else {
                controller.errorMessage("There are not enough points within "
                        + m.getTrackName()
                        + " to calculate time between points.");
            }
        });
        return temp;
    }

    /**
     * worker method that parses the times gathered in getTimeMap() to a usable format
     * @param times - a list of times in their respective string format
     * @return a list of times converted into their respective long values in the unit of seconds
     * @throws ParseException - thrown if a string cannot be parsed into a long value
     * @throws IllegalArgumentException - thrown if the time calculated results in a negative value; or if the time of
     * the current point versus the previous point results in going "back in time"
     */
    private List<Long> parseTimes(List<String> times) throws ParseException, IllegalArgumentException {
        // Starting Time Format:
        // <time>2010-10-19T13:00:00Z</time>
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        // Convert to milliseconds since
        // January 1st, 1970

        // Temp Variables
        String chunk1, chunk2, formatted;
        Date formattedDate;

        // List
        ArrayList<Long> t = new ArrayList<>();

        for(int i = 0; i < times.size(); i++) {
            chunk1 = times.get(i).substring(0, 10);
            chunk2 = times.get(i).substring(11, times.get(i).length() - 1);
            formatted = chunk1 + " " + chunk2;
            formattedDate = format.parse(formatted);
            if(DEBUGGING) {
                //System.out.println(formatted);
                //System.out.println(format.parse(formatted));
                System.err.println(formattedDate.getTime());
                //System.out.println(formattedDate.toString());
            }
            t.add(formattedDate.getTime() / 1000);
            if(formattedDate.getTime() < 0) {
                throw new IllegalArgumentException("Time cannot be negative.");
            } else if ( (i > 0) && (t.get(i) - t.get(i - 1)) < 0) {
                throw new IllegalArgumentException("Traveling backwards in time is impossible, therefore time cannot be computed.");
            }
        }

        return t;
    }

    /**
     * worker method that takes the a list of times as type long and normalizes the list. namely, this method
     * takes the first index of the list, and subtracts the value from all indices following such that the first
     * time is considered "time zero" for graphing purposes; this method will also convert from seconds to minutes
     * @param times - a list of times parsed into type long
     * @return a list of type long that have been adjusted via the offset and in the unit of minutes
     */
    private List<Long> normalizeTimes(List<Long> times) {
        long offset = times.get(0);
        final long MINUTES = 60;
        for(int i = 0; i < times.size(); i++) {
            if(DEBUGGING) {
                //System.out.println(times.get(i) - offset);
            }
            times.set(i, (times.get(i) - offset)/MINUTES);
        }
        return times;
    }

    /**
     * worker method that creates a map associating the track name of each metrics file with its respective distance between each point
     * @param metrics - the list of metrics loaded into the UI
     * @return a map containing pairings of track names to distances (in KM)
     */
    private HashMap<String, List<Double>> getDistanceMapKM(List<GPSMetrics> metrics) {
        HashMap<String, List<Double>> temp = new HashMap<>();
        metrics.forEach(m -> {
            try {
                if (m.getPointList().size() > 1) {
                    temp.put(m.getTrackName(), getDistances(m.getGPSPointList()));
                } else {
                    controller.errorMessage("Distance cannot be computed for tracks less than two points.");
                }
            } catch (IllegalArgumentException e) {
                controller.errorMessage(e.getMessage());
                temp.remove(m.getTrackName());
                metricsMap.remove(m.getTrackName());
            }
        });
        return temp;
    }

    /**
     * worker method that creates a list of distances by getting the distance between two points using the Haversine formula
     * @param points - list of GPS points
     * @return the points
     */
    private List<Double> getDistances(List<GPSPoint> points) {
        List<Double> temp = new ArrayList<>();
        double distance = 0;
        for(int i = 0; i < points.size(); i++) {
            if(i != 0) {
                distance += getDistanceBetweenPoints(points.get(i - 1), points.get(i));
            }
            if(DEBUGGING) {
                System.out.println(distance);
            }
            temp.add(distance);
        }
        return temp;
    }

    /**
     * worker method to get the distance between two GPS points via the Haversine formula
     * @param point1 - the first point
     * @param point2 - the second point
     * @return the distance between the two points
     */
    private double getDistanceBetweenPoints(GPSPoint point1, GPSPoint point2) throws IllegalArgumentException {
        double prevLong, prevLat, currLong, currLat, deltaLong, deltaLat, a, c;

        prevLong = Math.toRadians(point1.getLon());
        prevLat = Math.toRadians(point1.getLat());
        currLong = Math.toRadians(point2.getLon());
        currLat = Math.toRadians(point2.getLat());

        deltaLong = currLong - prevLong;
        deltaLat = currLat - prevLat;

        a = Math.pow(Math.sin(deltaLat / 2), 2)
                + Math.cos(prevLat)
                * Math.cos(currLat)
                * Math.pow(Math.sin(deltaLong / 2), 2);

        c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        if(DEBUGGING) {
            System.out.println(c * EARTH_RADIUS);
        }
        if(c * EARTH_RADIUS < 0) {
            throw new IllegalArgumentException("Track cannot be added due to malformed data.\nDistance cannot be negative.");
        }
        return (c * EARTH_RADIUS);
    }

    /**
     * worker method that creates a map associating the metrics file name with the distances of each points in miles
     * @return a map of metrics file names paired with list of distances in the units of miles
     */
    private HashMap<String, List<Double>> getDistanceMapMiles() {
        HashMap<String, List<Double>> temp = new HashMap<>();
        distanceMapKM.keySet().forEach(m -> {
            if (distanceMapKM.get(m).size() > 1) {
                temp.put(m, convertKMtoMiles(distanceMapKM.get(m)));
            } else {
                controller.errorMessage("Distance cannot be computed for tracks less than two points.");
            }
        });
        return temp;
    }


    /**
     * worker method that converts the given list from KM to Miles; assumes the list is already in the unit of kilometers
     * @param distances - the list of distances to convert
     * @return a list of distances of unit miles
     */
    private List<Double> convertKMtoMiles(List<Double> distances) {
        List<Double> temp = new ArrayList<>();
        final double KM_TO_MPH = 0.621371;
        for(int i = 0; i < distances.size(); i++) {
            temp.add(distances.get(i) * KM_TO_MPH);
        }
        return temp;
    }

    /**
     * worker method that initializes the frame to display data. Creates a JList and single JButton to interact with.
     */
    private void initGUI() {
        frame = new JFrame(FRAME_TITLE);
        frame.setLayout(new GridLayout(1, 2));
        frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        frame.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        RefineryUtilities.centerFrameOnScreen(frame);
        createCloseOperation();

        tracksList = new JList<>(metricsMap.keySet().toArray(new String[metricsMap.size()]));
        tracksList.setCellRenderer((JList<? extends String> list, String value, int index,
                                    boolean isSelected, boolean cellHasFocus) -> {
            JLabel label = new JLabel(value +
                    " --- Total Elevation Gain: " +
                    getTotalElevationGain(elevationMap.get(value)) +
                    " meters");
            if(isSelected) {
                if(index < LINE_COLORS.size()) {
                    label.setForeground(LINE_COLORS.get(index));
                }
            }
            return label;
        });

        getMenu();

        JPanel p1 = new JPanel(new GridLayout(2, 1));
        p1.setSize(200, FRAME_HEIGHT);

        JButton plotButton = new JButton("Plot tracks");
        plotButton.addActionListener(e -> plotTracks());
        plotButton.setMargin(new Insets(10,10,10,10));

        JPanel p2 = new JPanel(new FlowLayout());
        p2.setBorder(new EmptyBorder(50,0,0,0));
        p2.setBackground(new Color(255,255,255));
        p2.add(plotButton);

        p1.add(tracksList);
        p1.add(p2);

        frame.add(p1);

        frame.setVisible(true);
    }

    /**
     * Changes default close operation of the ui window
     */
    private void createCloseOperation(){
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent windowEvent) {
                frame.dispose();
            }
        });
    }

    /**
     * worker method; creates the menu to select between distance or time data
     */
    private void getMenu() {
        JMenuBar jMenuBar = new JMenuBar();

        JMenu view = new JMenu("View");
        view.setMnemonic(KeyEvent.VK_V);
        view.getAccessibleContext().setAccessibleDescription("Additional viewing options.");

        jMenuBar.add(view);

        JMenuItem menuItem = new JMenuItem("Elevation vs. Time");
        menuItem.setMnemonic(KeyEvent.VK_T);
        menuItem.getAccessibleContext().setAccessibleDescription("Shows elevation as a function of time.");
        menuItem.addActionListener(e -> {
            functionOfDistanceOrTimeToggle = true;
            if(DEBUGGING) {
                System.out.println(functionOfDistanceOrTimeToggle + " " + distanceUnitToggle);
            }
        });

        view.add(menuItem);

        view.addSeparator();

        JMenu distance = new JMenu("Elevation vs. Distance");
        distance.setMnemonic(KeyEvent.VK_D);
        distance.getAccessibleContext().setAccessibleDescription("Shows elevation as a function of distance.");

        view.add(distance);

        menuItem = new JMenuItem("Distance (KM)");
        menuItem.setMnemonic(KeyEvent.VK_K);
        menuItem.getAccessibleContext().setAccessibleDescription("Shows the distance data in kilometers.");
        menuItem.addActionListener(e -> {
            functionOfDistanceOrTimeToggle = false;
            distanceUnitToggle = true;
            if(DEBUGGING) {
                System.out.println(functionOfDistanceOrTimeToggle + " " + distanceUnitToggle);
            }
        });

        distance.add(menuItem);

        menuItem = new JMenuItem("Distance (Miles)");
        menuItem.setMnemonic(KeyEvent.VK_M);
        menuItem.getAccessibleContext().setAccessibleDescription("Shows the distance data in miles.");
        menuItem.addActionListener(e -> {
            functionOfDistanceOrTimeToggle = false;
            distanceUnitToggle = false;
            if(DEBUGGING) {
                System.out.println(functionOfDistanceOrTimeToggle + " " + distanceUnitToggle);
            }
        });

        distance.add(menuItem);

        frame.setJMenuBar(jMenuBar);
    }

    /**
     * listener function that will initialize the plotter to given that at least one track has been selected
     */
    private void plotTracks() {
        if(!tracksList.isSelectionEmpty()) {
            initPlotter();
        } else {
            controller.errorMessage("Please select at least 1 track to plot.");
        }
        RefineryUtilities.centerFrameOnScreen(frame);
    }

    /**
     * worker method that initializes the graph
     */
    private void initPlotter() {
        if(chartPanel != null) {
            frame.remove(chartPanel);
        }
        getAxisBounds(tracksList.getSelectedValuesList());

        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();

        JFreeChart plotter = getCorrectPlotter(renderer);

        chartPanel = new ChartPanel(plotter);
        chartPanel.setPreferredSize(new Dimension(CHART_WIDTH, CHART_HEIGHT));

        XYPlot plot = plotter.getXYPlot();
        plot.getRangeAxis().setRange(minElevation, maxElevation);

        if(!functionOfDistanceOrTimeToggle) {
            plot.getDomainAxis().setRange(minDistance, maxDistance);
        } else {
            plot.getDomainAxis().setRange(minTime, maxTime);
        }
        plot.setRenderer(renderer);

        frame.add(chartPanel);
        frame.pack();
    }

    /**
     * worker method that finds the axis bounds to be used in the chart
     * @param tracks -  the names of the metrics files
     */
    private void getAxisBounds(List<String> tracks) {
        try {
            resetMinMax();
            findElevationAxisRange(tracks);
            if(!functionOfDistanceOrTimeToggle) {
                findDistanceAxisRange(tracks);
            } else {
                findTimeAxisRange(tracks);
            }
            fixAxisRanges();
            if(DEBUGGING) {
                System.out.println("Elevation max/min\n" +
                        maxElevation + "/" + minElevation +
                        "\nDistance max/min\n" + maxDistance + "/" +
                        minDistance + "\nTime max/min\n" + maxTime +
                        "/" + minTime);
            }
        } catch (Exception e) {
            if(DEBUGGING) {
                e.printStackTrace();
            }
            controller.errorMessage(e.getMessage());
        }
    }

    /**
     * Resets the min and max values for re-evaluation.
     */
    private void resetMinMax() {
        maxTime = Long.MIN_VALUE;
        maxElevation = Double.NEGATIVE_INFINITY;
        minTime = 0;
        minElevation = Double.POSITIVE_INFINITY;
        maxDistance = Double.NEGATIVE_INFINITY;
        minDistance = 0;
    }

    /**
     * finds the max and min elevation of all the metrics files
     * @param tracks - the names of the metrics files
     */
    private void findElevationAxisRange(List<String> tracks) {
        for(String track : tracks) {
            for(Double ele : elevationMap.get(track)) {
                maxElevation = Math.max(maxElevation, ele);
                minElevation = Math.min(minElevation, ele);
            }
        }
    }

    /**
     * finds the max times over all the metrics files
     * @param tracks - the names of the metrics files
     */
    private void findTimeAxisRange(List<String> tracks)  {
        for(String track : tracks) {
            for(Long time : timeMap.get(track)) {
                maxTime = Math.max(maxTime, time);
            }
        }
    }

    /**
     * finds the max times over all the metrics files
     * @param tracks - the names of the metrics files
     */
    private void findDistanceAxisRange(List<String> tracks) {
        Map<String, List<Double>> temp;
        if(distanceUnitToggle) {
            temp = distanceMapKM;
        } else {
            temp = distanceMapMiles;
        }
        for(String track : tracks) {
            for(Double distance : temp.get(track) ) {
                maxDistance = Math.max(maxDistance, distance);
            }
        }
    }

    /**
     * worker method that fixes any range that is held constant in that a range must not start and end with
     * the same value; the method simply adds 1 to any max value, and subtracts 1 from any min value if and only if
     * the range between said min and max is 0;
     */
    private void fixAxisRanges() {
        if(maxElevation == minElevation) {
            maxElevation += 1;
            minElevation -= 1;
        }
        if(maxTime == minTime) {
            maxTime += 1;
        }
        if(maxDistance == minDistance) {
            maxDistance += 1;
        }
    }

    /**
     * worker method that gets the correct data and labels based on toggles
     * @param renderer - a reference to the renderer to be used with the chart
     * @return the chart containing the data
     */
    private JFreeChart getCorrectPlotter(XYLineAndShapeRenderer renderer) {
        JFreeChart plotter;
        // false && true == Kilometers
        if(!functionOfDistanceOrTimeToggle && distanceUnitToggle) {
            plotter = ChartFactory.createXYLineChart(
                    CHART_TITLE_DISTANCE,
                    X_AXIS_TITLE_DISTANCE_KM,
                    Y_AXIS_TITLE,
                    plotSelectedTracks(renderer));
        }
        // false && false == Miles
        else if(!functionOfDistanceOrTimeToggle) {
            plotter = ChartFactory.createXYLineChart(
                    CHART_TITLE_DISTANCE,
                    X_AXIS_TITLE_DISTANCE_MILES,
                    Y_AXIS_TITLE,
                    plotSelectedTracks(renderer));
        }
        // defaults to Time
        else {
            plotter = ChartFactory.createXYLineChart(
                    CHART_TITLE_TIME,
                    X_AXIS_TITLE_TIME,
                    Y_AXIS_TITLE,
                    plotSelectedTracks(renderer));
        }

        return plotter;
    }

    /**
     * worker method that creates the data set to be plotted
     * @param r -  a reference to the renderer used with the chart
     * @return the data et to be plotted
     */
    private XYDataset plotSelectedTracks(XYLineAndShapeRenderer r) {
        XYSeriesCollection dataSet = new XYSeriesCollection();
        tracksList.getSelectedValuesList().forEach(track -> plotTrack(track, r, dataSet));
        return dataSet;
    }

    /**
     * worker method that constructs each series for each metrics file selected to be plotted
     * @param track - the track name to be plotted
     * @param r - a reference to the renderer used with the chart
     * @param d - a reference to the data set to be plotted
     */
    private void plotTrack(String track, XYLineAndShapeRenderer r, XYSeriesCollection d) {
        try {
            if (elevationMap.get(track).size() > 0
                    && timeMap.get(track).size() > 0
                    && distanceMapKM.get(track).size() > 0
                    && distanceMapMiles.get(track).size() > 0) {

                Map<String, List<Double>> distanceMap;
                int index = tracksList.getSelectedValuesList().indexOf(track);
                int colorIndex = metricsNamesList.indexOf(track);

                if(DEBUGGING) {
                    System.out.println("Index of track JList " + track + ": " + index);
                    System.out.println("Index of track metricsNamesList " + track + ": " + colorIndex);
                }

                r.setSeriesPaint(index, LINE_COLORS.get(colorIndex));
                XYSeries series = new XYSeries(track, false);

                if(!functionOfDistanceOrTimeToggle) {
                    if(distanceUnitToggle) {
                        distanceMap = distanceMapKM;
                    } else {
                        distanceMap = distanceMapMiles;
                    }
                    for(int i = 0; i < metricsMap.get(track).getGPSPointList().size(); i++) {
                        series.add(distanceMap.get(track).get(i),
                                elevationMap.get(track).get(i));
                        if(DEBUGGING) {
                        System.err.println("Distance: "
                        + (distanceMap.get(track).get(i))
                        + "; Elevation: "
                        + elevationMap.get(track).get(i));
                        }
                    }
                } else {
                    for (int i = 0; i < metricsMap.get(track).getGPSPointList().size(); i++) {
                        series.add(timeMap.get(track).get(i),
                                elevationMap.get(track).get(i));
                        if (DEBUGGING) {
                        System.err.println("Time: "
                        + (timeMap.get(track).get(i))
                        + "; Elevation: "
                        + elevationMap.get(track).get(i));
                        }
                    }
                }
                d.addSeries(series);
            }
        } catch(Exception e) {
            if(DEBUGGING) {
                e.printStackTrace();
            }
            controller.errorMessage(e.getMessage());
        }
    }


}
