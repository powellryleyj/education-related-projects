/*
                   GNU LESSER GENERAL PUBLIC LICENSE
                       Version 3, 29 June 2007

 Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.


  This version of the GNU Lesser General Public License incorporates
the terms and conditions of version 3 of the GNU General Public
License, supplemented by the additional permissions listed below.

  0. Additional Definitions.

  As used herein, "this License" refers to version 3 of the GNU Lesser
General Public License, and the "GNU GPL" refers to version 3 of the GNU
General Public License.

  "The Library" refers to a covered work governed by this License,
other than an Application or a Combined Work as defined below.

  An "Application" is any work that makes use of an interface provided
by the Library, but which is not otherwise based on the Library.
Defining a subclass of a class defined by the Library is deemed a mode
of using an interface provided by the Library.

  A "Combined Work" is a work produced by combining or linking an
Application with the Library.  The particular version of the Library
with which the Combined Work was made is also called the "Linked
Version".

  The "Minimal Corresponding Source" for a Combined Work means the
Corresponding Source for the Combined Work, excluding any source code
for portions of the Combined Work that, considered in isolation, are
based on the Application, and not on the Linked Version.

  The "Corresponding Application Code" for a Combined Work means the
object code and/or source code for the Application, including any data
and utility programs needed for reproducing the Combined Work from the
Application, but excluding the System Libraries of the Combined Work.

  1. Exception to Section 3 of the GNU GPL.

  You may convey a covered work under sections 3 and 4 of this License
without being bound by section 3 of the GNU GPL.

  2. Conveying Modified Versions.

  If you modify a copy of the Library, and, in your modifications, a
facility refers to a function or data to be supplied by an Application
that uses the facility (other than as an argument passed when the
facility is invoked), then you may convey a copy of the modified
version:

   a) under this License, provided that you make a good faith effort to
   ensure that, in the event an Application does not supply the
   function or data, the facility still operates, and performs
   whatever part of its purpose remains meaningful, or

   b) under the GNU GPL, with none of the additional permissions of
   this License applicable to that copy.

  3. Object Code Incorporating Material from Library Header Files.

  The object code form of an Application may incorporate material from
a header file that is part of the Library.  You may convey such object
code under terms of your choice, provided that, if the incorporated
material is not limited to numerical parameters, data structure
layouts and accessors, or small macros, inline functions and templates
(ten or fewer lines in length), you do both of the following:

   a) Give prominent notice with each copy of the object code that the
   Library is used in it and that the Library and its use are
   covered by this License.

   b) Accompany the object code with a copy of the GNU GPL and this license
   document.

  4. Combined Works.

  You may convey a Combined Work under terms of your choice that,
taken together, effectively do not restrict modification of the
portions of the Library contained in the Combined Work and reverse
engineering for debugging such modifications, if you also do each of
the following:

   a) Give prominent notice with each copy of the Combined Work that
   the Library is used in it and that the Library and its use are
   covered by this License.

   b) Accompany the Combined Work with a copy of the GNU GPL and this license
   document.

   c) For a Combined Work that displays copyright notices during
   execution, include the copyright notice for the Library among
   these notices, as well as a reference directing the user to the
   copies of the GNU GPL and this license document.

   d) Do one of the following:

       0) Convey the Minimal Corresponding Source under the terms of this
       License, and the Corresponding Application Code in a form
       suitable for, and under terms that permit, the user to
       recombine or relink the Application with a modified version of
       the Linked Version to produce a modified Combined Work, in the
       manner specified by section 6 of the GNU GPL for conveying
       Corresponding Source.

       1) Use a suitable shared library mechanism for linking with the
       Library.  A suitable mechanism is one that (a) uses at run time
       a copy of the Library already present on the user's computer
       system, and (b) will operate properly with a modified version
       of the Library that is interface-compatible with the Linked
       Version.

   e) Provide Installation Information, but only if you would otherwise
   be required to provide such information under section 6 of the
   GNU GPL, and only to the extent that such information is
   necessary to install and execute a modified version of the
   Combined Work produced by recombining or relinking the
   Application with a modified version of the Linked Version. (If
   you use option 4d0, the Installation Information must accompany
   the Minimal Corresponding Source and Corresponding Application
   Code. If you use option 4d1, you must provide the Installation
   Information in the manner specified by section 6 of the GNU GPL
   for conveying Corresponding Source.)

  5. Combined Libraries.

  You may place library facilities that are a work based on the
Library side by side in a single library together with other library
facilities that are not Applications and are not covered by this
License, and convey such a combined library under terms of your
choice, if you do both of the following:

   a) Accompany the combined library with a copy of the same work based
   on the Library, uncombined with any other library facilities,
   conveyed under the terms of this License.

   b) Give prominent notice with the combined library that part of it
   is a work based on the Library, and explaining where to find the
   accompanying uncombined form of the same work.

  6. Revised Versions of the GNU Lesser General Public License.

  The Free Software Foundation may publish revised and/or new versions
of the GNU Lesser General Public License from time to time. Such new
versions will be similar in spirit to the present version, but may
differ in detail to address new problems or concerns.

  Each version is given a distinguishing version number. If the
Library as you received it specifies that a certain numbered version
of the GNU Lesser General Public License "or any later version"
applies to it, you have the option of following the terms and
conditions either of that published version or of any later version
published by the Free Software Foundation. If the Library as you
received it does not specify a version number of the GNU Lesser
General Public License, you may choose any version of the GNU Lesser
General Public License ever published by the Free Software Foundation.

  If the Library as you received it specifies that a proxy can decide
whether future versions of the GNU Lesser General Public License shall
apply, that proxy's public statement of acceptance of any version is
permanent authorization for you to choose that version for the
Library.
 */

package logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

/**
 * singleton logger to record events to the file specified.
 * assumed to be working with .txt files
 * @author Ryley Powell (powellr)
 * @version 1.0
 */
public class EventLogger {
	
	public static final int SUCCESS = 0;
	
	public static final int FAILURE = -1;
	
	public static final int SUCCESS_WITH_ERRORS = 1;
	
	private static final String SPACER = "--------------------" +
			"--------------------";

	private static EventLogger instance = null;
	
	private File logFile;
	
	private BufferedWriter writer;
	
	private boolean timeStamp;
	
	private boolean spacers;
	
	private EventLogger(){
		timeStamp = true;
		spacers = true;
	}
	
	/**
	 * instantiates the EventLogger with injection of spacers/time-stamps
	 * defaulted to true if the EventLogger does not already exist 
	 * @return the instance of the logger
	 */
	public static EventLogger getInstance(){
		if(instance == null){
			instance = new EventLogger();
		}
		return instance;
	}
	
	/**
	 * finds the file in which to write log events to specified by
	 * the path argument. if the file does not exist, this method
	 * will create one specified by the end path name. this method
	 * will create the respective directories within the path if 
	 * not already created dependent on the createDirs argument
	 * @param path - the path to the file name including the file
	 * @param createDirs - true will create any missing directories
	 * on the path; false will assume all proper directories have been
	 * created
	 * @return will return a integer constant specified by the class. 0 indicates
	 * the file(and directories) have been created/found successfully. 1 indicates
	 * that the file was created/found successfully but the directories containing
	 * the file were not created if and only if createDirs is true. -1 indicates
	 * a NullPointerException or IOException has occurred and therefore the file
	 * could not be created/found
	 */
	public int getLogFile(Path path, boolean createDirs){
		int status = SUCCESS;
		try{
			if(path == null){
				throw new NullPointerException("Path cannot be null.");
			}
			else if(createDirs && path != null){
				Path parent = path.getParent();
				if(parent != null){
					new File(parent.toString()).mkdirs();
				}
				else{
					status = SUCCESS_WITH_ERRORS;
				}
			}
		
			logFile = new File(path.toString());
			logFile.createNewFile();
			wrapFile();
		}
		catch(NullPointerException | IOException e){
			status = FAILURE;
			e.printStackTrace();
		}
		
		return status;
	}
	
	/**
	 * simplified version of getLogFile(Path, boolean) which assumes
	 * that all directories and sub-directories on the Path argument has
	 * already been created.
	 * @param path - the path to the file name including the file
	 * @return will return a integer constant specified by the class. 0 indicates
	 * the file(and directories) have been created/found successfully. 1 indicates
	 * that the file was created/found successfully but the directories containing
	 * the file were not created if and only if createDirs is true. -1 indicates
	 * a NullPointerException or IOException has occurred and therefore the file
	 * could not be created/found
	 */
	public int getLogFile(Path path){
		return getLogFile(path, false);
	}
	
	/**
	 * writes the event provided to the log file. will inject spaces and 
	 * the time-stamp in which the event was logged dependent on whether
	 * the spacers/time-stamps are toggled on or off
	 * @param eventName - the name of the event being logged
	 * @param message - the message associated with the event being logged
	 * @throws IOException - if an IOException occurs
	 */
	public void logEvent(String eventName, String message) throws IOException{
		if(spacers){
			writeSpacer();
		}
		String eventHeader;
		if(timeStamp){
			Date d = new Date();
			eventHeader = eventName + " logged at " + d.toString();
		}
		else{
			eventHeader = eventName;
		}
		writer.write("HEADER:");
		writer.newLine();
		writer.write(eventHeader);
		writer.newLine();
		if(spacers){
			writeSpacer();
		}
		writer.write("MESSAGE:");
		writer.newLine();
		writer.write(message);
		writer.newLine();
		if(spacers){
			writeSpacer();
		}
		writer.newLine();
		writer.flush();
	}
	
	/**
	 * simplified version of logEvent(String,String) in which
	 * the eventName is defaulted to "Event". will inject spaces and 
	 * the time-stamp in which the event was logged dependent on whether
	 * the spacers/time-stamps are toggled on or off
	 * @param message - the message associated with the event being logged
	 * @throws IOException - if an IOException occurs
	 */
	public void logEvent(String message) throws IOException{
		logEvent("Event", message);
	}
	
	/**
	 * closes the writer associated with the log file
	 * @throws IOException - if an IOException occurs
	 */
	public void close() throws IOException{
		writer.close();
	}
	
	/**
	 * turns the injection of time-stamps on or off. default is on
	 */
	public void toggleTimeStamp(){
		timeStamp = !timeStamp;
	}
	
	/**
	 * turns the injection of spacers on or off. default is on
	 */
	public void toggleSpacers(){
		spacers = !spacers;
	}
	
	/*
	 * helper method to separate the wrapping of the file object
	 * from the main logic flow for easy maintenance
	 */
	private void wrapFile() throws IOException{
		writer = new BufferedWriter(new FileWriter(logFile, true));
	}
	
	/*
	 * helper method to condense repeated lines of code
	 */
	private void writeSpacer() throws IOException{
		writer.write(SPACER);
		writer.newLine();
	}
	
	public static void main(String[] args){
		try {
			EventLogger e = EventLogger.getInstance();
			int i = e.getLogFile(Paths.get("test.txt"), false);
			System.out.println(i);
			//e.toggleSpacers();
			//e.toggleTimeStamp();
			e.logEvent("test test test test");
			e.logEvent("TEST", "test test test");
			i = e.getLogFile(Paths.get("why/test.txt"), true);
			System.out.println(i);
			e.logEvent("test test test test");
			e.logEvent("TEST", "test test test");
		} 
		catch (IOException e1) {
			e1.printStackTrace();
		}
		
	}
}
