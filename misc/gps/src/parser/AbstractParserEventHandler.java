/*
                   GNU LESSER GENERAL PUBLIC LICENSE
                       Version 3, 29 June 2007

 Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.


  This version of the GNU Lesser General Public License incorporates
the terms and conditions of version 3 of the GNU General Public
License, supplemented by the additional permissions listed below.

  0. Additional Definitions.

  As used herein, "this License" refers to version 3 of the GNU Lesser
General Public License, and the "GNU GPL" refers to version 3 of the GNU
General Public License.

  "The Library" refers to a covered work governed by this License,
other than an Application or a Combined Work as defined below.

  An "Application" is any work that makes use of an interface provided
by the Library, but which is not otherwise based on the Library.
Defining a subclass of a class defined by the Library is deemed a mode
of using an interface provided by the Library.

  A "Combined Work" is a work produced by combining or linking an
Application with the Library.  The particular version of the Library
with which the Combined Work was made is also called the "Linked
Version".

  The "Minimal Corresponding Source" for a Combined Work means the
Corresponding Source for the Combined Work, excluding any source code
for portions of the Combined Work that, considered in isolation, are
based on the Application, and not on the Linked Version.

  The "Corresponding Application Code" for a Combined Work means the
object code and/or source code for the Application, including any data
and utility programs needed for reproducing the Combined Work from the
Application, but excluding the System Libraries of the Combined Work.

  1. Exception to Section 3 of the GNU GPL.

  You may convey a covered work under sections 3 and 4 of this License
without being bound by section 3 of the GNU GPL.

  2. Conveying Modified Versions.

  If you modify a copy of the Library, and, in your modifications, a
facility refers to a function or data to be supplied by an Application
that uses the facility (other than as an argument passed when the
facility is invoked), then you may convey a copy of the modified
version:

   a) under this License, provided that you make a good faith effort to
   ensure that, in the event an Application does not supply the
   function or data, the facility still operates, and performs
   whatever part of its purpose remains meaningful, or

   b) under the GNU GPL, with none of the additional permissions of
   this License applicable to that copy.

  3. Object Code Incorporating Material from Library Header Files.

  The object code form of an Application may incorporate material from
a header file that is part of the Library.  You may convey such object
code under terms of your choice, provided that, if the incorporated
material is not limited to numerical parameters, data structure
layouts and accessors, or small macros, inline functions and templates
(ten or fewer lines in length), you do both of the following:

   a) Give prominent notice with each copy of the object code that the
   Library is used in it and that the Library and its use are
   covered by this License.

   b) Accompany the object code with a copy of the GNU GPL and this license
   document.

  4. Combined Works.

  You may convey a Combined Work under terms of your choice that,
taken together, effectively do not restrict modification of the
portions of the Library contained in the Combined Work and reverse
engineering for debugging such modifications, if you also do each of
the following:

   a) Give prominent notice with each copy of the Combined Work that
   the Library is used in it and that the Library and its use are
   covered by this License.

   b) Accompany the Combined Work with a copy of the GNU GPL and this license
   document.

   c) For a Combined Work that displays copyright notices during
   execution, include the copyright notice for the Library among
   these notices, as well as a reference directing the user to the
   copies of the GNU GPL and this license document.

   d) Do one of the following:

       0) Convey the Minimal Corresponding Source under the terms of this
       License, and the Corresponding Application Code in a form
       suitable for, and under terms that permit, the user to
       recombine or relink the Application with a modified version of
       the Linked Version to produce a modified Combined Work, in the
       manner specified by section 6 of the GNU GPL for conveying
       Corresponding Source.

       1) Use a suitable shared library mechanism for linking with the
       Library.  A suitable mechanism is one that (a) uses at run time
       a copy of the Library already present on the user's computer
       system, and (b) will operate properly with a modified version
       of the Library that is interface-compatible with the Linked
       Version.

   e) Provide Installation Information, but only if you would otherwise
   be required to provide such information under section 6 of the
   GNU GPL, and only to the extent that such information is
   necessary to install and execute a modified version of the
   Combined Work produced by recombining or relinking the
   Application with a modified version of the Linked Version. (If
   you use option 4d0, the Installation Information must accompany
   the Minimal Corresponding Source and Corresponding Application
   Code. If you use option 4d1, you must provide the Installation
   Information in the manner specified by section 6 of the GNU GPL
   for conveying Corresponding Source.)

  5. Combined Libraries.

  You may place library facilities that are a work based on the
Library side by side in a single library together with other library
facilities that are not Applications and are not covered by this
License, and convey such a combined library under terms of your
choice, if you do both of the following:

   a) Accompany the combined library with a copy of the same work based
   on the Library, uncombined with any other library facilities,
   conveyed under the terms of this License.

   b) Give prominent notice with the combined library that part of it
   is a work based on the Library, and explaining where to find the
   accompanying uncombined form of the same work.

  6. Revised Versions of the GNU Lesser General Public License.

  The Free Software Foundation may publish revised and/or new versions
of the GNU Lesser General Public License from time to time. Such new
versions will be similar in spirit to the present version, but may
differ in detail to address new problems or concerns.

  Each version is given a distinguishing version number. If the
Library as you received it specifies that a certain numbered version
of the GNU Lesser General Public License "or any later version"
applies to it, you have the option of following the terms and
conditions either of that published version or of any later version
published by the Free Software Foundation. If the Library as you
received it does not specify a version number of the GNU Lesser
General Public License, you may choose any version of the GNU Lesser
General Public License ever published by the Free Software Foundation.

  If the Library as you received it specifies that a proxy can decide
whether future versions of the GNU Lesser General Public License shall
apply, that proxy's public statement of acceptance of any version is
permanent authorization for you to choose that version for the
Library.
 */

package parser;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.ErrorHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.ext.LexicalHandler;

/**
 * 	DO NOT MODIFY THE CODE IN THIS ABSTRACT CLASS.
 * 
 *  This is just a "convenience" class that provides default implementations
 *  of all of the various interface methods that the parser.Parser's event handler needs
 *  to implement. The implementations here merely print the events to the console
 *  so that you can see them as they are generated.
 *  
 *  You must implement a concrete class that extends this abstract class, and
 *  override the methods so that you can capture the event data of interest
 *  to your application.
 *  
 *  You must pass the reference to your concrete class to the parser.Parser constructor.
 *  
 * @author hornick
 *
 */
public abstract class AbstractParserEventHandler implements ContentHandler, ErrorHandler,
		                                                    LexicalHandler {
	// the Locator object is created/used by the parser to provide location (row, column) information 
	// for errors in the xml file being read.
	protected Locator locator; // note that this object is inherited by concrete classes

	// This method simply logs the event information to the console. By modifying the implementation,
	// it can be made to log to a file instead.
	private void log(String prefix, String data) {
		System.out.println( prefix + ": " + data);
	}

	/*
	 * This method retrieves the line number where an error was encountered
	 * in cases where the parser.Parser throws an exception.
	 */
	public int getLine() {
		if( locator != null )
			return locator.getLineNumber();
		else 
			return 0;
	}

	/*
	 * This method retrieves the column number where an error was encountered
	 * in cases where the parser.Parser throws an exception.
	 */
	public int getColumn() {
		if( locator != null )
			return locator.getColumnNumber();
		else 
			return 0;
	}

	// Note: for complete Javadoc on the following methods, see the official
	// documentation 

	// This method is called  the parser.Parser is about to throw
	// a SAXException indicating a warning.
	public void warning(SAXParseException exception) throws SAXException {
		log("*warning event!", "The parser is going to throw a SAXException!");
	}

	// This method is called whenever the parser.Parser is about to throw
	// a SAXException indicating an error.
	public void error(SAXParseException exception) throws SAXException {
		log("**error event!", "The parser is going to throw a SAXException!");
	}

	// This method is called whenever the parser.Parser is about to throw
	// a SAXException indicating a fatal error.
	public void fatalError(SAXParseException exception) throws SAXException {
		log("**fatalerror event!", "The parser is going to throw a SAXException!");
	}

	//ContentHandler method
	public void setDocumentLocator(Locator locator) {
		this.locator = locator;
		log( "setDocumentLocator ", "Line:"+locator.getLineNumber()+", Col:"+locator.getColumnNumber() );
	}

	@Override
	//ContentHandler method
	public void startDocument() throws SAXException {
		log("startDocument event!", "start of xml document");
	}

	@Override
	//ContentHandler method
	public void endDocument() throws SAXException {
		log("endDocument event!", "end of xml document");
	}

	@Override
	// This ContentHandler method is called whenever the parser.Parser encounters a new tag
	// which may or may not have Attributes. For example,
	// <trkpt lat="43.30627219" lon="-87.98892299">
	// contains two attributes (lat and lon).
	// The code below shows how to access the values of the attributes.
	public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
		String attributes = "none";		
		if( atts.getLength()> 0 ) { // check for element attributes; print them only if they exist.
			attributes = " [";
			for( int index=0; index<atts.getLength(); index++) {
				attributes += " "+ atts.getLocalName(index) + "=" + atts.getValue(index);
			}
			attributes += "]";
		}
		log("startElement event! (<tag> found)", localName + ", attributes:" + attributes );
	}

	@Override
	// This ContentHandler method is called whenever the parser.Parser encounters an END tag (e.g. </someTag>)
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		log("endElement event! (</tag> found)", localName);		
	}

	@Override
	// This ContentHandler method is called whenever the parser.Parser encounters text between tags.
	// For example, for the tag <ele>249.5</ele>, the characters "249.5" are reported.
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		String chars = ""; 

		for( int index=start; index<start+length; index++) {
			if(!Character.isWhitespace(ch[index])) // ignore whitespace
				chars += ch[index];
		}

		if( !chars.isEmpty() ) // only whitespace was found, and ignored
			log("characters found between <tag> and </tag>: ", chars );
	}


	@Override
	//ContentHandler method
	public void startPrefixMapping(String prefix, String uri)
			throws SAXException {
		//Null implementation provided; this event can be ignored
		//log("ContentHandler startPrefixMapping", "" );
	}

	@Override
	//ContentHandler method
	public void endPrefixMapping(String prefix) throws SAXException {
		//Null implementation provided; this event can be ignored
		//log("ContentHandler endPrefixMapping", "" );
	}

	@Override
	//ContentHandler method
	public void ignorableWhitespace(char[] ch, int start, int length)
			throws SAXException {
		//Null implementation provided; this event can be ignored
		//log("ContentHandler ignorableWhitespace (c, start, length)", c + ","+ start + "," + length);
	}

	@Override
	//ContentHandler method
	public void processingInstruction(String target, String data)
			throws SAXException {
		//Null implementation provided; this event can be ignored
		//log("ContentHandler processingInstruction (target, data)", target + "," + data);
	}

	@Override
	//ContentHandler method
	public void skippedEntity(String name) throws SAXException {
		//Null implementation provided; this event can be ignored
		//log("ContentHandler skippedEntity (name)", name);
	}
	
	@Override
	// LexicalHandler method
	public void startDTD(String name, String publicId, String systemId)
			throws SAXException {
		//Null implementation provided; this event can be ignored
		//log("LexicalHandler startDTD(name,publicId, systemId)", name + "," +publicId + "," + systemId );
	}

	@Override
	// LexicalHandler method
	public void endDTD() throws SAXException {
		//Null implementation provided; this event can be ignored
		//log("LexicalHandler endDTD", "" );
	}

	@Override
	// LexicalHandler method
	public void startEntity(String name) throws SAXException {
		//Null implementation provided; this event can be ignored
		//log("LexicalHandler startEntity(name)", name );
	}

	@Override
	// LexicalHandler method
	public void endEntity(String name) throws SAXException {
		//Null implementation provided; this event can be ignored
		//log("LexicalHandler endEntity(name)", name );
	}

	@Override
	// LexicalHandler method
	public void startCDATA() throws SAXException {
		//Null implementation provided; this event can be ignored
		//log("LexicalHandler startCDATA", "" );
	}

	@Override
	// LexicalHandler method
	public void endCDATA() throws SAXException {
		//Null implementation provided; this event can be ignored
		//log("LexicalHandler endCDATA", "" );
	}

	@Override
	// This LexicalHandler method is called whenever the parser.Parser encounters a comment <!-- like this -->
	public void comment(char[] ch, int start, int length) throws SAXException {
		String c = "<!--";
		for( int index=start; index<start+length; index++)
			c += ch[index];
		c += ">";
		log("comment event!", c );
	}


}
