//GNU LESSER GENERAL PUBLIC LICENSE
//        Version 3, 29 June 2007
//
//        Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
//        Everyone is permitted to copy and distribute verbatim copies
//        of this license document, but changing it is not allowed.
//
//
//        This version of the GNU Lesser General Public License incorporates
//        the terms and conditions of version 3 of the GNU General Public
//        License, supplemented by the additional permissions listed below.
//
//        0. Additional Definitions.
//
//        As used herein, "this License" refers to version 3 of the GNU Lesser
//        General Public License, and the "GNU GPL" refers to version 3 of the GNU
//        General Public License.
//
//        "The Library" refers to a covered work governed by this License,
//        other than an Application or a Combined Work as defined below.
//
//        An "Application" is any work that makes use of an interface provided
//by the Library, but which is not otherwise based on the Library.
//        Defining a subclass of a class defined by the Library is deemed a mode
//        of using an interface provided by the Library.
//
//        A "Combined Work" is a work produced by combining or linking an
//        Application with the Library.  The particular version of the Library
//        with which the Combined Work was made is also called the "Linked
//        Version".
//
//        The "Minimal Corresponding Source" for a Combined Work means the
//        Corresponding Source for the Combined Work, excluding any source code
//        for portions of the Combined Work that, considered in isolation, are
//        based on the Application, and not on the Linked Version.
//
//        The "Corresponding Application Code" for a Combined Work means the
//        object code and/or source code for the Application, including any data
//        and utility programs needed for reproducing the Combined Work from the
//        Application, but excluding the System Libraries of the Combined Work.
//
//        1. Exception to Section 3 of the GNU GPL.
//
//        You may convey a covered work under sections 3 and 4 of this License
//        without being bound by section 3 of the GNU GPL.
//
//        2. Conveying Modified Versions.
//
//        If you modify a copy of the Library, and, in your modifications, a
//        facility refers to a function or data to be supplied by an Application
//        that uses the facility (other than as an argument passed when the
//        facility is invoked), then you may convey a copy of the modified
//        version:
//
//        a) under this License, provided that you make a good faith effort to
//        ensure that, in the event an Application does not supply the
//        function or data, the facility still operates, and performs
//        whatever part of its purpose remains meaningful, or
//
//        b) under the GNU GPL, with none of the additional permissions of
//        this License applicable to that copy.
//
//        3. Object Code Incorporating Material from Library Header Files.
//
//        The object code form of an Application may incorporate material from
//        a header file that is part of the Library.  You may convey such object
//        code under terms of your choice, provided that, if the incorporated
//        material is not limited to numerical parameters, data structure
//        layouts and accessors, or small macros, inline functions and templates
//        (ten or fewer lines in length), you do both of the following:
//
//        a) Give prominent notice with each copy of the object code that the
//        Library is used in it and that the Library and its use are
//        covered by this License.
//
//        b) Accompany the object code with a copy of the GNU GPL and this license
//        document.
//
//        4. Combined Works.
//
//        You may convey a Combined Work under terms of your choice that,
//        taken together, effectively do not restrict modification of the
//        portions of the Library contained in the Combined Work and reverse
//        engineering for debugging such modifications, if you also do each of
//        the following:
//
//        a) Give prominent notice with each copy of the Combined Work that
//        the Library is used in it and that the Library and its use are
//        covered by this License.
//
//        b) Accompany the Combined Work with a copy of the GNU GPL and this license
//        document.
//
//        c) For a Combined Work that displays copyright notices during
//        execution, include the copyright notice for the Library among
//        these notices, as well as a reference directing the user to the
//        copies of the GNU GPL and this license document.
//
//        d) Do one of the following:
//
//        0) Convey the Minimal Corresponding Source under the terms of this
//        License, and the Corresponding Application Code in a form
//        suitable for, and under terms that permit, the user to
//        recombine or relink the Application with a modified version of
//        the Linked Version to produce a modified Combined Work, in the
//        manner specified by section 6 of the GNU GPL for conveying
//        Corresponding Source.
//
//        1) Use a suitable shared library mechanism for linking with the
//        Library.  A suitable mechanism is one that (a) uses at run time
//        a copy of the Library already present on the user's computer
//        system, and (b) will operate properly with a modified version
//        of the Library that is interface-compatible with the Linked
//        Version.
//
//        e) Provide Installation Information, but only if you would otherwise
//        be required to provide such information under section 6 of the
//        GNU GPL, and only to the extent that such information is
//        necessary to install and execute a modified version of the
//        Combined Work produced by recombining or relinking the
//        Application with a modified version of the Linked Version. (If
//        you use option 4d0, the Installation Information must accompany
//        the Minimal Corresponding Source and Corresponding Application
//        Code. If you use option 4d1, you must provide the Installation
//        Information in the manner specified by section 6 of the GNU GPL
//        for conveying Corresponding Source.)
//
//        5. Combined Libraries.
//
//        You may place library facilities that are a work based on the
//        Library side by side in a single library together with other library
//        facilities that are not Applications and are not covered by this
//        License, and convey such a combined library under terms of your
//        choice, if you do both of the following:
//
//        a) Accompany the combined library with a copy of the same work based
//        on the Library, uncombined with any other library facilities,
//        conveyed under the terms of this License.
//
//        b) Give prominent notice with the combined library that part of it
//        is a work based on the Library, and explaining where to find the
//        accompanying uncombined form of the same work.
//
//        6. Revised Versions of the GNU Lesser General Public License.
//
//        The Free Software Foundation may publish revised and/or new versions
//        of the GNU Lesser General Public License from time to time. Such new
//        versions will be similar in spirit to the present version, but may
//        differ in detail to address new problems or concerns.
//
//        Each version is given a distinguishing version number. If the
//        Library as you received it specifies that a certain numbered version
//        of the GNU Lesser General Public License "or any later version"
//        applies to it, you have the option of following the terms and
//        conditions either of that published version or of any later version
//        published by the Free Software Foundation. If the Library as you
//        received it does not specify a version number of the GNU Lesser
//        General Public License, you may choose any version of the GNU Lesser
//        General Public License ever published by the Free Software Foundation.
//
//        If the Library as you received it specifies that a proxy can decide
//        whether future versions of the GNU Lesser General Public License shall
//        apply, that proxy's public statement of acceptance of any version is
//        permanent authorization for you to choose that version for the
//        Library.

package gpxAnalyzer;

import org.xml.sax.SAXException;
import org.xml.sax.Attributes;
import parser.AbstractParserEventHandler;


/**
 * The GPSParserEventHandler class parses relevant GPS data from a GPX xml file.
 *
 * @author Joshua McComack
 * @version 1.0
 * @created 28-Oct-2016
 */
public class GPSParserEventHandler extends AbstractParserEventHandler {

    //metrics object used for storing data collected from parser
    private GPSMetrics gpsMetrics;

    //flags for tracking the state of the GPSParserEventHandler
    private boolean gpxFile;
    private boolean inGPXState;
    private boolean inTrackPointState;
    private boolean inEleState;
    private boolean inTimeState;
    private boolean inNameState;

    //values stored in a track point
    private Double lat;
    private Double lon;
    private Double elevation;
    private String time;

    //characters gathered between tags
    private String chars;

    //name of the track from the GPX file
    private String trackName;


    /**
     * Constructs a GPSParserEventHandler
     * @param gpsMetrics a GPSMetrics object to keep track of GPX file information
     */
    public GPSParserEventHandler(GPSMetrics gpsMetrics){
        this.gpsMetrics = gpsMetrics;
    }

    /**
     * Start document method is called at the beginning of the file. initializes gpxFile tag.
     */
    @Override
    public void startDocument() throws SAXException {
        gpxFile = false;
    }

    /**
     * Called at the end of a document.
     *
     * @throws SAXException thrown if not in a GPX file
     */
    @Override
    public void endDocument() throws SAXException {
        if (!gpxFile){
            throw new SAXException("Not a GPX file");
        }
    }

    /**
     * Starts a gpx, name, trkpt, ele, or time element.
     *
     * @param uri name space uri
     * @param localName local name of the element
     * @param qName qualified or prefixed name of the element
     * @param atts attributes of the element
     * @throws SAXException when encountering an error starting any of these elements
     */
    @Override
    public void startElement(String uri, String localName, String qName, Attributes atts)
            throws SAXException {
        switch (localName){
            case "gpx" : startGPX();
                break;
            case "name" : startName();
                break;
            case "trkpt" : startTrackPt(atts);
                break;
            case "ele" : startEle();
                break;
            case "time" : startTime();
        }
    }


    /**
     * Ends a gpx, name, trkpt, ele, or time element.
     *
     * @param uri name space uri
     * @param localName local name of the element
     * @param qName qualified or prefixed name of the element
     * @throws SAXException when encountering an error ending any of these elements
     */
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        switch (localName){
            case "gpx" : endGPX();
                break;
            case "name" : endName();
                break;
            case "trkpt" : endTrackPt();
                break;
            case "ele" : endEle();
                break;
            case "time" : endTime();
        }
    }

    /**
     * Collects characters found within the document. Only collects relevant characters found
     * within name, ele, and time tags.
     *
     * @param ch array of characters
     * @param start int start index
     * @param length int value of length
     */
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (inGPXState && (inNameState || inEleState || inTimeState)) {
            chars = "";
            for (int index = start; index < start + length; index++) {
                chars += ch[index];
            }
        }
    }

    /**
     * Starts a track point element.
     *
     * @param atts lat and lon attributes of the track point
     * @throws SAXException thrown for invalid value or number of attributes
     */
    private void startTrackPt(Attributes atts) throws SAXException {
        if (inGPXState) {
            inTrackPointState = true;
            if (atts.getLength() == 2) {
                try {
                    lat = Double.valueOf(atts.getValue(atts.getIndex("lat")));
                    lon = Double.valueOf(atts.getValue(atts.getIndex("lon")));
                } catch (IllegalArgumentException e) {
                    throw new SAXException("Invalid track point value in lat and lon attributes"
                            + getLocation());
                }
            } else {
                throw new SAXException("Invalid number of attributes for track point"
                        + getLocation());
            }
        }
    }

    /**
     * Initializes tags used to track states
     */
    private void initTags() {
        inTrackPointState = false;
        inEleState = false;
        inTimeState = false;
        inNameState = false;
    }

    /**
     * Starts a GPX element, initializes tags, sets GPX state and GPX File to true
     */
    private void startGPX() {
        initTags();
        inGPXState = true;
        gpxFile = true;
    }

    /**
     * Starts a time element, setting time state to true
     */
    private void startTime() {
        if (inGPXState && inTrackPointState) {
            inTimeState = true;
        }
    }

    /**
     * Starts an elevation element, setting the ele state to true
     */
    private void startEle() {
        if (inGPXState && inTrackPointState) {
            inEleState = true;
        }
    }

    /**
     * Starts a name element, setting the name state to true
     */
    private void startName() {
        if (inGPXState){
            inNameState = true;
        }
    }

    /**
     * Ends a name element, setting name state to false
     * @throws SAXException when the track has no name
     */
    private void endName() throws SAXException {
        if (inNameState) {
            inNameState = false;
            if (chars == null || chars.length() < 1) {
                throw new SAXException("Track has no name" + getLocation());
            } else {
                trackName = chars;
                gpsMetrics.setTrackName(trackName);
            }
        }
    }

    /**
     * Ends a GPX element, setting gpx state to false
     * @throws SAXException if track is missing the name element
     */
    private void endGPX() throws SAXException {
        if (trackName == null){
            throw new SAXException("Track missing name element" + getLocation());
        } else if (inGPXState) {
            inGPXState = false;
        }
    }

    /**
     * Ends a track point. setting the track state to false
     * @throws SAXException if any of the point values are invalid
     */
    private void endTrackPt() throws SAXException{
        if (inTrackPointState) {
            try {
                checkPointValid();
                gpsMetrics.addPoint(lat, lon, elevation, time);
            } catch (SAXException e) {
                throw e;
            } finally {
                inTrackPointState = false;
                resetPointAttributes();
            }
        }
    }

    /**
     * Resets all point values to null
     */
    private void resetPointAttributes() {
        lat = null;
        lon = null;
        elevation = null;
        time = null;
    }

    /**
     * Checks each point value for valid information
     * @throws SAXException if any point value is invalid
     */
    private void checkPointValid() throws SAXException {
        checkLatValid();
        checkLonValid();
        checkEleValid();
        checkTimeValid();
    }

    /**
     * Checks if the time element is valid
     * @throws SAXException if time is null
     */
    private void checkTimeValid() throws SAXException {
        if (time == null){
            throw new SAXException("Point missing time element" + getLocation());
        } else if (time.length()<20){
                throw new SAXException("Invalid time format" + getLocation());
        } else {
            checkDateFormat();
        }
    }

    /**
     * Checks if the time date format is valid
     * @throws SAXException if date format is invalid
     */
    private void checkDateFormat() throws SAXException {
        try {
            String date = time.substring(0, time.indexOf('T'));
            if (date.length() < 10) {
                throw new SAXException("Invalid time format" + getLocation());
            }
        } catch (IndexOutOfBoundsException e){
            throw new SAXException("Invalid time format" + getLocation());
        }
    }

    /**
     * Checks if the elevations element is valid
     * @throws SAXException if elevation is null
     */
    private void checkEleValid() throws SAXException {
        if (elevation == null){
            throw new SAXException("Missing elevation element" + getLocation());
        }
    }

    /**
     * Checks if latitude is valid
     * @throws SAXException if lat is null or out of bounds
     */
    private void checkLatValid() throws SAXException {
        if (lat == null || lat < -90.00 || lat > 90.00){
            throw new SAXException("Latitude is out of bounds" + getLocation());
        }
    }

    /**
     * Checks if longitude is valid
     * @throws SAXException if lon is null or out of bounds
     */
    private void checkLonValid() throws SAXException {
        if (lon == null || lon < -180.00 || lon > 180.00){
            throw new SAXException("Longitude is out of bounds" + getLocation());
        }
    }

    /**
     * Ends the elevations element, sets ele State to false
     * @throws SAXException if elevation has an invalid value
     */
    private void endEle() throws SAXException {
        if (inEleState) {
            try {
                elevation = Double.valueOf(chars);
            } catch (NumberFormatException e) {
                throw new SAXException("Elevation has invalid value" + getLocation());
            }
            inEleState = false;
        }
    }

    /**
     * Ends the time element, sets the time state to false
     * @throws SAXException if time has an invalid value
     */
    private void endTime() throws SAXException {
        if (inTimeState) {
            if (chars.length() < 1) {
                throw new SAXException("Time has invalid value" + getLocation());
            } else {
                time = chars;
                checkTimeValid();
            }
            inTimeState = false;
        }
    }

    /**
     * gets the line and column number of the locator
     * @return a String containing the current line and column number
     */
    private String getLocation(){
        return " at line:" + getLine() + " col:" + getColumn();
    }
}
