//GNU LESSER GENERAL PUBLIC LICENSE
//        Version 3, 29 June 2007
//
//        Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
//        Everyone is permitted to copy and distribute verbatim copies
//        of this license document, but changing it is not allowed.
//
//
//        This version of the GNU Lesser General Public License incorporates
//        the terms and conditions of version 3 of the GNU General Public
//        License, supplemented by the additional permissions listed below.
//
//        0. Additional Definitions.
//
//        As used herein, "this License" refers to version 3 of the GNU Lesser
//        General Public License, and the "GNU GPL" refers to version 3 of the GNU
//        General Public License.
//
//        "The Library" refers to a covered work governed by this License,
//        other than an Application or a Combined Work as defined below.
//
//        An "Application" is any work that makes use of an interface provided
//        by the Library, but which is not otherwise based on the Library.
//        Defining a subclass of a class defined by the Library is deemed a mode
//        of using an interface provided by the Library.
//
//        A "Combined Work" is a work produced by combining or linking an
//        Application with the Library.  The particular version of the Library
//        with which the Combined Work was made is also called the "Linked
//        Version".
//
//        The "Minimal Corresponding Source" for a Combined Work means the
//        Corresponding Source for the Combined Work, excluding any source code
//        for portions of the Combined Work that, considered in isolation, are
//        based on the Application, and not on the Linked Version.
//
//        The "Corresponding Application Code" for a Combined Work means the
//        object code and/or source code for the Application, including any data
//        and utility programs needed for reproducing the Combined Work from the
//        Application, but excluding the System Libraries of the Combined Work.
//
//        1. Exception to Section 3 of the GNU GPL.
//
//        You may convey a covered work under sections 3 and 4 of this License
//        without being bound by section 3 of the GNU GPL.
//
//        2. Conveying Modified Versions.
//
//        If you modify a copy of the Library, and, in your modifications, a
//        facility refers to a function or data to be supplied by an Application
//        that uses the facility (other than as an argument passed when the
//        facility is invoked), then you may convey a copy of the modified
//        version:
//
//        a) under this License, provided that you make a good faith effort to
//        ensure that, in the event an Application does not supply the
//        function or data, the facility still operates, and performs
//        whatever part of its purpose remains meaningful, or
//
//        b) under the GNU GPL, with none of the additional permissions of
//        this License applicable to that copy.
//
//        3. Object Code Incorporating Material from Library Header Files.
//
//        The object code form of an Application may incorporate material from
//        a header file that is part of the Library.  You may convey such object
//        code under terms of your choice, provided that, if the incorporated
//        material is not limited to numerical parameters, data structure
//        layouts and accessors, or small macros, inline functions and templates
//        (ten or fewer lines in length), you do both of the following:
//
//        a) Give prominent notice with each copy of the object code that the
//        Library is used in it and that the Library and its use are
//        covered by this License.
//
//        b) Accompany the object code with a copy of the GNU GPL and this license
//        document.
//
//        4. Combined Works.
//
//        You may convey a Combined Work under terms of your choice that,
//        taken together, effectively do not restrict modification of the
//        portions of the Library contained in the Combined Work and reverse
//        engineering for debugging such modifications, if you also do each of
//        the following:
//
//        a) Give prominent notice with each copy of the Combined Work that
//        the Library is used in it and that the Library and its use are
//        covered by this License.
//
//        b) Accompany the Combined Work with a copy of the GNU GPL and this license
//        document.
//
//        c) For a Combined Work that displays copyright notices during
//        execution, include the copyright notice for the Library among
//        these notices, as well as a reference directing the user to the
//        copies of the GNU GPL and this license document.
//
//        d) Do one of the following:
//
//        0) Convey the Minimal Corresponding Source under the terms of this
//        License, and the Corresponding Application Code in a form
//        suitable for, and under terms that permit, the user to
//        recombine or relink the Application with a modified version of
//        the Linked Version to produce a modified Combined Work, in the
//        manner specified by section 6 of the GNU GPL for conveying
//        Corresponding Source.
//
//        1) Use a suitable shared library mechanism for linking with the
//        Library.  A suitable mechanism is one that (a) uses at run time
//        a copy of the Library already present on the user's computer
//        system, and (b) will operate properly with a modified version
//        of the Library that is interface-compatible with the Linked
//        Version.
//
//        e) Provide Installation Information, but only if you would otherwise
//        be required to provide such information under section 6 of the
//        GNU GPL, and only to the extent that such information is
//        necessary to install and execute a modified version of the
//        Combined Work produced by recombining or relinking the
//        Application with a modified version of the Linked Version. (If
//        you use option 4d0, the Installation Information must accompany
//        the Minimal Corresponding Source and Corresponding Application
//        Code. If you use option 4d1, you must provide the Installation
//        Information in the manner specified by section 6 of the GNU GPL
//        for conveying Corresponding Source.)
//
//        5. Combined Libraries.
//
//        You may place library facilities that are a work based on the
//        Library side by side in a single library together with other library
//        facilities that are not Applications and are not covered by this
//        License, and convey such a combined library under terms of your
//        choice, if you do both of the following:
//
//        a) Accompany the combined library with a copy of the same work based
//        on the Library, uncombined with any other library facilities,
//        conveyed under the terms of this License.
//
//        b) Give prominent notice with the combined library that part of it
//        is a work based on the Library, and explaining where to find the
//        accompanying uncombined form of the same work.
//
//        6. Revised Versions of the GNU Lesser General Public License.
//
//        The Free Software Foundation may publish revised and/or new versions
//        of the GNU Lesser General Public License from time to time. Such new
//        versions will be similar in spirit to the present version, but may
//        differ in detail to address new problems or concerns.
//
//        Each version is given a distinguishing version number. If the
//        Library as you received it specifies that a certain numbered version
//        of the GNU Lesser General Public License "or any later version"
//        applies to it, you have the option of following the terms and
//        conditions either of that published version or of any later version
//        published by the Free Software Foundation. If the Library as you
//        received it does not specify a version number of the GNU Lesser
//        General Public License, you may choose any version of the GNU Lesser
//        General Public License ever published by the Free Software Foundation.
//
//        If the Library as you received it specifies that a proxy can decide
//        whether future versions of the GNU Lesser General Public License shall
//        apply, that proxy's public statement of acceptance of any version is
//        permanent authorization for you to choose that version for the
//        Library.

package gpxAnalyzer;

import org.xml.sax.SAXException;
import parser.Parser;

import java.awt.geom.Point2D;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * The gpxAnalyzer.GPSMetrics class represents instance of the processed data that has been passed through
 * the GPX files. Thus capable of returning list of point found in the file.
 * It stores a list of the points.
 * (each point holding the latitude, longitude values and elevation of points tracked at a certain instance)
 * It also calculates the speed between the latitude and longitude values as a part of processing the points.
 *
 * @author Kayeng Thao
 * @version 1.0
 * @created 25-Oct-2016
 */
public class GPSMetrics {

    // Variable:
    private ArrayList<GPSPoint> gpsPoints;
    private String pathName;

    private String fileName;
    private String outPut;
    private String trackName;

    private double[] distanceAtSpeeds;

    private Parser parser;
    private GPSParserEventHandler gpsHandler;

    public ArrayList<Double> pubTimeList;
    public ArrayList<Point2D.Double> pointList;
    public ArrayList<Double> elevationList;
    public ArrayList<Double> speedList;

    //Constant used in the distance formula
    private static final double earth_radius = 6371;

    /**
     * Empty Constructor
     */
    public GPSMetrics(){
        this.gpsPoints = new ArrayList<>();
        gpsHandler = new GPSParserEventHandler(this);
    }

    /**
     *  Constructor, creates the list of tracks for individual tracks.
     * (basically makes sure that we'll have new array of points for each list)
     */
    public GPSMetrics(String pathName) throws Exception {
        this.gpsPoints = new ArrayList<>();
        gpsHandler = new GPSParserEventHandler(this);
        parseFile(pathName);
        this.pathName = pathName;

        Path myPath = Paths.get(pathName);
        fileName = myPath.getFileName().toString();

        pointList = getPointList();
        elevationList = getElevationList();
        speedList = getSpeedList();

        distanceAtSpeeds = new double[6];

        calculateSpeeds();
    }

    private void calculateSpeeds() throws Exception {

        ArrayList<Double> listOfTimes = getTimes();
        ArrayList<Double> listOfDistances = getDistances();

        // loops until the end of the the loops (of equal size) and finds times
        // adds times to listOfSpeeds
        if(listOfDistances.size() > 1) {
            for (int i = 0; i < listOfTimes.size(); i++) {
                Double dist = listOfDistances.get(i);

                double currTime;
                currTime = listOfTimes.get(i);

                double speed = dist / currTime;
                if (speed < 3) {
                    distanceAtSpeeds[0] += Math.abs(dist);
                } else if (speed > 3 && speed < 7) {
                    distanceAtSpeeds[1] += Math.abs(dist);
                } else if (speed > 7 && speed < 10) {
                    distanceAtSpeeds[2] += Math.abs(dist);
                } else if (speed > 10 && speed < 15) {
                    distanceAtSpeeds[3] += Math.abs(dist);
                } else if (speed > 15 && speed < 20) {
                    distanceAtSpeeds[4] += Math.abs(dist);
                } else {
                    distanceAtSpeeds[5] += Math.abs(dist);
                }
            }
        }else{
            throw new NoSuchElementException("Track contains less than 2 points.");
        }

    }

    private String formatSpeed(double distance){

        String out = "";
        NumberFormat doubleFormat = new DecimalFormat("#0.00");

        out += doubleFormat.format(distance);
        out += " km; ";
        out += doubleFormat.format(kilometersToMiles(distance));
        out += " mi";

        return out;
    }

    public String getSpeed3(){
        return formatSpeed(distanceAtSpeeds[0]);
    }

    public String getSpeed3to7(){
        return formatSpeed(distanceAtSpeeds[1]);
    }

    public String getSpeed7to10(){
        return formatSpeed(distanceAtSpeeds[2]);
    }

    public String getSpeed10to15(){
        return formatSpeed(distanceAtSpeeds[3]);
    }

    public String getSpeed15to20(){
        return formatSpeed(distanceAtSpeeds[4]);
    }

    public String getSpeed20(){
        return formatSpeed(distanceAtSpeeds[5]);
    }

    /**
     * Constructs a new parser to parse an individual file
     * exceptions are propagate to class that calls it
     */
    private void parseFile(String fileName) throws Exception {
            parser = new Parser(gpsHandler);
            parser.parse(fileName);
    }

    /**
     * Allows the creation of a singular point by passing in respective point values.
     * Once point is created, adds the point to the list of GPSpoints.
     * @param lat - given latitude
     * @param lon - given longitude
     * @param ele - given elevation
     * @param time - given time
     */
    public void addPoint(double lat, double lon, double ele, String time) throws SAXException {
        GPSPoint currentPoint = new GPSPoint(lat,lon,ele,time);
        gpsPoints.add(currentPoint);
    }

    /**
     * Method that returns a list of the points in a respective loaded GPX file
     * @return listOfPoints
     */
    public ArrayList<Point2D.Double>  getPointList(){
        ArrayList<Point2D.Double> listOfPoints = new ArrayList<>();

        for(int i = 0; i < gpsPoints.size(); i++){
            listOfPoints.add(gpsPoints.get(i).getPoint());
        }

        return listOfPoints;
    }

    /**
     * Method that returns a list of the elevations in a respective loaded GPX file
     * @return listOfEle
     */
    public ArrayList<Double> getElevationList(){
        ArrayList<Double> listOfEle = new ArrayList<>();

        for(int i = 0; i < gpsPoints.size(); i++){
            listOfEle.add(gpsPoints.get(i).getEle());
        }

        return listOfEle;
    }

    /**
     * Method that returns a list of the speeds in a respective loaded GPX file
     * @return listOfSpeeds
     */
    public ArrayList<Double> getSpeedList() throws Exception {
        ArrayList<Double> listOfSpeeds = new ArrayList<>();

        // the sizes of times and speeds should be the same, due to the fact that
        // both come from the same set of data and thus should have equal numbers of times and distances

        ArrayList<Double> listOfTimes = getTimes();
        ArrayList<Double> listOfDistances = getDistances();

        if(listOfDistances.size() != listOfTimes.size()){
            throw new ParseException("Error in times to distance ratio", 0);
        } else {
            // loops until the end of the the loops (of equal size) and finds times
            // adds times to listOfSpeeds
            for(int i = 0; i < listOfTimes.size(); i++ ){
                Double dist = listOfDistances.get(i);
                Double time = listOfTimes.get(i);

                if(time == 0 || dist == 0){
                    listOfSpeeds.add(0.0);
                } else {
                    // distance in KM and time in hrs
                    Double speed = dist/time;
                    listOfSpeeds.add(speed);
                }
            }
        }

        return listOfSpeeds;
    }

    /**
     * worker method that obtains a list of each time recorded
     * @return un-parsed list of times
     */
    public ArrayList<String> getUnparsedTimes() {
        ArrayList<String> timeList = new ArrayList<>();
        for(GPSPoint point : gpsPoints) {
            timeList.add(point.getTime());
        }
        return timeList;
    }

    /**
     * Helper Method to return a list of times tracked between points
     * @return timeList
     */
    public ArrayList<Double> getTimes() throws Exception {
        pubTimeList= new ArrayList<>();

        // Time Format:
        // <time>2010-10-19T13:00:00Z</time>

        if (gpsPoints.size() != 0) {
            for (int i = 0; i < (gpsPoints.size() - 1); i++) {
                // gets the times in string format of: 2010-10-19T13:00:00Z
                String dateTimeString1 = gpsPoints.get(i).getTime();
                String dateTimeString2 = gpsPoints.get(i + 1).getTime();
                Double time = timeParseCalc(dateTimeString1, dateTimeString2);
                pubTimeList.add(time);
            }
        }else{
            throw new Exception("File contianed no points.");
        }

        return pubTimeList;
    }

    /**
     * Helper Method used to to parse the time values from time format to difference in hours
     * @param time1
     * @param time2
     * @return time
     * @throws ParseException "message will be propagated"
     */
    private Double timeParseCalc (String time1, String time2) throws Exception{
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss"); // maybe :ss

        int SECOND = 1000;
        int MINUTE = 60;
        int HOUR = 60;

        // assuming that all correct times follow a standardized format,
        // goes cuts out the 2010-10-19T13:00:00Z into 13:00:00

        String timeChunk1 = time1.substring(11,time1.length()-1);
        Date timeP1 = format.parse(timeChunk1);

        String timeChunk2 = time2.substring(11,time1.length()-1);
        Date timeP2 = format.parse(timeChunk2);

        //diff is in milliseconds
        double diff = timeP2.getTime()- timeP1.getTime();
        // milli/1000 = seconds
        // -> seconds/60 = minutes
        // -> minutes/60 = hours
        double time = diff/SECOND/MINUTE/HOUR;

        // time returned in hours
        return time;
    }

    /**
     * Helper Method to return a list of distances between points
     * @return distanceList
     */
    public ArrayList<Double> getDistances(){
        double earth_radius = 6371; // radius of the earth in KM
        ArrayList<Double> distanceList = new ArrayList<>();
        // begins at the second point (position 1)
        // does point2 (position 0) - point1 (position 1)  = distance

        for(int i = 0; i < (gpsPoints.size()-1); i++){
            // point i-1 is the previous point; it is split into latitude and longitude
            double lat1 = Math.toRadians(gpsPoints.get(i).getLat());
            double lon1 = Math.toRadians(gpsPoints.get(i).getLon());
            // point i is the "next" point; it is also spilt into latitude and longitude
            double lat2 = Math.toRadians(gpsPoints.get(i+1).getLat());
            double lon2 = Math.toRadians(gpsPoints.get(i+1).getLon());

            // gets the difference between the points
            double dLon = lon2 - lon1;
            double dLat = lat2 - lat1;

            // calculates an "a" and a "c" value based off the latitudes and longitudes
            double a = Math.pow(Math.sin(dLat / 2), 2) + Math.cos(lat1) * Math.cos(lat2)
                    * Math.pow(Math.sin(dLon / 2), 2);
            double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

            // this is the actual distance
            double distance = earth_radius * c;

            distanceList.add((distance));
        }

        return distanceList;
    }

    /**
     * returns the pathName given to the XML document
     * @return fileName
     */
    public String getPathName() {
        return pathName;
    }

    /**
     * returns the trackName given to the actual track
     * @return trackName
     */
    public String getTrackName() {
        return trackName;
    }

    /**
     * allows caller to set the trackName of the actual track
     * @param trackName "name taken in"
     */
    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    /**
     * Allows getting of fileName
     * @return fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * returns a nicely formatted fileName
     * @return fileName
     */
    public String toString(){
        return "File: " + getFileName();
    }

    /**
     * Gets the average speed from the analyzer's speedList
     *
     * @return "0.0" if speedList is empty - otherwise the average speed
     */
    public double getAverageSpeed() {
        ArrayList<Double> times = pubTimeList;
        if (pointList.size() <= 1){
            return -1;
        } else {
            double sum = 0.0;
            for (Double time : times){
                sum += time;
            }
            return (getTotalDistance()/sum);
        }
    }

    /**
     * Gets the max speed from the analyzer's speedList
     *
     * @return "0.0" if speedList is empty - otherwise the maximum speed
     */
    public double getMaxSpeed() {
        if (speedList.isEmpty()) {
            return -1;
        } else {
            return Collections.max(speedList);
        }
    }

    /**
     * Gets the minimum longitude from the analyzer's pointList
     *
     * @return the minimum longitude
     */
    public double getMinLongitude() {
        double minLongitude = Double.MAX_VALUE;
        for (Point2D.Double point : pointList) {
            //Expecting our pairings to be (latitude, longitude)
            final double currLongitude = point.getY();
            if (currLongitude < minLongitude) {
                minLongitude = currLongitude;
            }
        }
        return minLongitude;
    }

    /**
     * Gets the maximum longitude from the analyzer's pointList
     *
     * @return the maximum longitude
     */
    public double getMaxLongitude() {
        double maxLongitude = -180.0;
        for (Point2D.Double point : pointList) {
            //Expecting our pairings to be (latitude, longitude)
            final double currLongitude = point.getY();
            if (currLongitude > maxLongitude) {
                maxLongitude = currLongitude;
            }

        }
        return maxLongitude;
    }

    /**
     * Gets the minimum latitude from the analyzer's pointList
     *
     * @return the minimum latitude
     */
    public double getMinLatitude() {
        double minLatitude = Double.MAX_VALUE;
        for (Point2D.Double point : pointList) {
            //Expecting our pairings to be (latitude, longitude)
            final double currLatitude = point.getX();
            if (currLatitude < minLatitude) {
                minLatitude = currLatitude;
            }

        }
        return minLatitude;
    }

    /**
     * Gets the maximum latitude from the analyzer's pointList
     *
     * @return the maximum latitude
     */
    public double getMaxLatitude() {
        double maxLatitude = -90.0;
        for (Point2D.Double point : pointList) {
            //Expecting our pairings to be (latitude, longitude)
            final double currLatitude = point.getX();
            if (currLatitude > maxLatitude) {
                maxLatitude = currLatitude;
            }
        }
        return maxLatitude;

    }

    /**
     * Gets the minimum elevation from the analyzer's elevation list
     *
     * @return the minimum elevation
     */
    public double getMinElevation() throws NoSuchElementException {
        return Collections.min(elevationList);
    }

    /**
     * Gets the maximum elevation from the analyzer's elevation list
     *
     * @return the maximum elevation
     */
    public double getMaxElevation() throws NoSuchElementException {
        return Collections.max(elevationList);
    }

    /**
     * Gets the total distance travelled in km using the Haversine formula and the analyzer's pointList
     *
     * @return the total distance in km
     */
    public double getTotalDistance() {
        //Initializing temporary variables for distance calculations
        double dist = 0.0;
        double firstLong, firstLat;
        double nextLong, nextLat;
        double longitude_delta, latitude_delta;
        double a, c;
        if (pointList.size() > 1) {
            //For each pairing of points in the the point list compute distance between them in kilometers
            for (int i = 0; i < (pointList.size() - 1); i++) {
                firstLong = Math.toRadians(pointList.get(i).getY());
                firstLat = Math.toRadians(pointList.get(i).getX());
                nextLong = Math.toRadians(pointList.get(i + 1).getY());
                nextLat = Math.toRadians(pointList.get(i + 1).getX());

                longitude_delta = nextLong - firstLong;
                latitude_delta = nextLat - firstLat;

                a = Math.pow(Math.sin(latitude_delta / 2), 2) + Math.cos(firstLat) * Math.cos(nextLat) * Math.pow(Math.sin(longitude_delta / 2), 2);
                c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                double curr_dist = earth_radius * c;
                dist += curr_dist;
            }
        }else{
            dist = -1;
        }
        return dist;
    }

    /**
     * Converts the given kilometers into miles
     *
     * @param km the kilometers to be converted
     * @return the miles equivalent to the kilometers passed in
     */
    public double kilometersToMiles(double km) {
        return (km * 0.621371);
    }

    /**
     * Converts the given miles into Kilometers
     *
     * @param m the miles to be converted
     * @return the kilometers equivalent to the miles passed in
     */
    public double mphToKph(double m){
        return (m * 1.609);
    }


    public List<GPSPoint> getGPSPointList() {
        return gpsPoints;
    }

    /**
     * Converts the given meters to feet
     *
     * @param meters the meters to be converted
     * @return the feet equivalent to the meters passed in
     */
    public double metersToFeet(double meters) {
        return (meters * 3.28084);
    }

    /**
     * Returns the Distance traveled by the track as a string in km and miles.
     * @return String of distance in km and miles.
     */
    public String getDistanceAsString(){
        pointList = getPointList();
        return "distance: " +  String.format("%.3f", getTotalDistance()) + "km ("
                + String.format("%.3f", kilometersToMiles(getTotalDistance())) + "mi)";
    }
    
}
