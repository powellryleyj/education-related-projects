<?xml version="1.0" encoding="UTF-8"?>
<gpx xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:gpxdata="http://www.cluetrust.com/XML/GPXDATA/1/0" xmlns="http://www.topografix.com/GPX/1/0" xsi:schemaLocation="http://www.topografix.com/GPX/1/0 http://www.topografix.com/GPX/1/0/gpx.xsd http://www.cluetrust.com/XML/GPXDATA/1/0 http://www.cluetrust.com/Schemas/gpxdata10.xsd" version="1.0" creator="http://ridewithgps.com/">
  
  <author>RideWithGPS LLC</author>
  
  <url>http://ridewithgps.com/trips/4440325</url>
  
  <trk>
    <name>One Coordinate</name>
    
    <trkseg>
    
      <!-- This file contains a single trkpt coordinate -->
      <trkpt lat="45.0" lon="-90.0">
        <ele>249.5</ele>
        <time>2015-06-10T24:00:00Z</time>
      </trkpt>
    
      
    </trkseg>
    
  </trk>
  
</gpx>
