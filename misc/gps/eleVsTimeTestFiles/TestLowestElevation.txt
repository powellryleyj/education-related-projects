<?xml version="1.0" encoding="UTF-8"?>
<gpx>
  <trk>
    <name>GPS Test: 3 points, one is the Dead Sea the lowest elevation on earth (-430 m) </name>
    <trkseg>
      <trkpt lat="31.7683" lon="35.2137">
        <ele>754.00</ele>
        <time>2010-10-19T13:00:00Z</time>
      </trkpt>
      <trkpt lat="31.5590" lon="35.4732">
        <ele>-430.01184</ele>
        <time>2010-10-19T15:10:00Z</time>
      </trkpt>
      <trkpt lat="31.9454" lon="35.9284">
        <ele>776.0208</ele>
        <time>2010-10-19T17:20:00Z</time>
      </trkpt>
    </trkseg>
  </trk>
</gpx>