import edu.msoe.se1010.winPlotter.SketchPad;
import edu.msoe.se1010.winPlotter.WinPlotter;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.*;
import java.util.List;

/**
 * The class that will display all of the bus locations are to the user
 * @author mcneelypj
 * @version 1.0
 * @created 13-Jan-2017 3:43:54 PM
 */
public class RouteDisplay extends WindowAdapter implements BusObserver {

	private SketchPad plotter;
	private VehicleInfo vehicleInfo;
	private List<VehicleText> viList;
	private final static double chLat = 43.044240;
    private final static double chLon = -87.906446;
    private final double busConst = .004;

    /**
     * Constructor for the RouteDisplay class. Instantiates the plotter, build the gui and
     * attaches this class to the subject.
	 *
     * @param vi instance of VehicleInfo.
     */
    public RouteDisplay(VehicleInfo vi){
		vehicleInfo = vi;
        this.vehicleInfo.attach(this);
        buildUI();
		viList = new ArrayList<>();
    }

    /**
     * Builds the window where buses will be drawn.
     */
	private void buildUI(){
        plotter = new SketchPad(800,600);
        plotter.setWindowTitle("Bus Route");
		plotter.setWindowSize(800,600);
		plotter.setPlotBoundaries(-88.14,42.90,-87.85 ,43.17);
		plotter.setBackgroundColor(Color.BLACK);
		plotter.setGrid(true, .05, .05, Color.GRAY);
		plotter.addWindowListener(new WindowAdapter() {
            public void windowClosed(WindowEvent windowEvent){
                vehicleInfo.detach(RouteDisplay.this);
            }
        });
		drawCityHall();
        vehicleInfo.notifyObservers();
	}



    /**
     * Gets a notification of the most up-to date vehicle information.
     * @param viList The vehicleText for all buses on the selected bus route
     */
	public void notifyObserver(List<VehicleText> viList){
	    this.viList = viList;
	    update();
	}

    /**
     * Updates the UI with new information when ever the observer receives new information.
     */
	private void update(){
	    try{
	        Thread uiThread = new Thread(new Runnable() {
                /**
                 * When an object implementing interface <code>Runnable</code> is used
                 * to create a thread, starting the thread causes the object's
                 * <code>run</code> method to be called in that separately executing
                 * thread.
                 * <p>
                 * The general contract of the method <code>run</code> is that it may
                 * take any action whatsoever.
                 *
                 * @see Thread#run()
                 */
                @Override
                public void run() {
                    for (VehicleText vi : viList){
                        drawBus(vi.getLon(), vi.getLat());
                    }
                }
            });
	        uiThread.start();
	        uiThread.join();
        } catch(InterruptedException ex){
	        ex.getMessage();
        }
	}

	/**
	 * Draws a bus on the display window.
	 * @param longitude the longitude coordinate of the bus
	 * @param latitude the latitude coordinate of the bus
	 */

	private void drawBus(String longitude, String latitude){
	    double lat = Double.parseDouble(latitude);
	    double lon = Double.parseDouble(longitude);
	    plotter.setPenColor(Color.GREEN);

	    plotter.moveTo(lon - busConst,lat - busConst);
	    plotter.drawLineTo(lon + busConst,lat - busConst);
        plotter.drawLineTo(lon + busConst,lat + busConst);
        plotter.drawLineTo(lon - busConst,lat + busConst);
        plotter.drawLineTo(lon - busConst,lat - busConst);
    }

	/**
	 * Draws a point where Milwaukee city hall is located.
	 */
    private void drawCityHall() {
	    plotter.setPenColor(Color.GRAY);
	    plotter.drawPointAt(chLon,chLat);
    }
}