import math
from math import floor


def build_max_heap(A):
    for i in range(floor(len(A) / 2), -1, -1):
        _max_heapify(A, i)


def heap_extract_max(A):
    if len(A) < 1:
        raise OverflowError  # chose overflow as there is not standard underflow error
    max = A[0]
    A.insert(0, A[len(A) - 1])
    A.pop(1)
    A.pop()
    _max_heapify(A, 0)
    return max


# def heapsort(A):
#     build_max_heap(A)
#     for i in range(len(A) - 1, 1, -1):
#         tmp = A[0]
#         A[0] = A[i]
#         A[i] = tmp
#         _max_heapify(A, i)
def heapsort(A):
    heap = A[:]
    build_max_heap(heap)
    for i in range(len(A)):
        A[len(A) - i - 1] = heap_extract_max(heap)


def max_heap_insert(A, key):
    # print(A)
    A.append(-math.inf)
    _heap_increase_key(A, len(A) - 1, key)


def _heap_increase_key(A, i, key):
    if key < A[i]:
        raise Exception("New key is smaller than current key")
    A[i] = key
    while i > 0 and A[_parent(i)] < A[i]:
        tmp = A[i]
        A[i] = A[_parent(i)]
        A[_parent(i)] = tmp
        i = _parent(i)


def _max_heapify(A, i):
    l = _left(i)
    r = _right(i)

    largest = i
    if l <= (len(A) - 1) and A[l] > A[largest]:
        largest = l
    if r <= (len(A) - 1) and A[r] > A[largest]:
        largest = r
    if largest != i:
        tmp = A[i]
        A[i] = A[largest]
        A[largest] = tmp
        _max_heapify(A, largest)


def _parent(i):
    return int(i // 2)


def _left(i):
    return 2 * i + 1


def _right(i):
    return 2 * i + 2
