import time as t
import random as ran
from sorting import max_heap_insert, build_max_heap, heap_extract_max


def threaded_insert(num_elements: int, best_case: [], random_case: [], worst_case: []):
    random_numbers = [ran.random() for i in range(num_elements)]

    sorted_list = random_numbers[:]
    sorted_list.sort()

    reversed_list = sorted_list[:]
    reversed_list.sort(reverse=True)

    array = []

    start = t.clock()
    for i in range(0, len(sorted_list)):
        max_heap_insert(array, sorted_list[i])
    end = t.clock()
    elapsed = end - start
    best_case.append(elapsed)

    array.clear()
    start = t.clock()
    for i in range(0, len(random_numbers)):
        max_heap_insert(array, sorted_list[i])
    end = t.clock()
    elapsed = end - start
    random_case.append(elapsed)

    array.clear()
    start = t.clock()
    for i in range(0, len(reversed_list)):
        max_heap_insert(array, reversed_list[i])
    end = t.clock()
    elapsed = end - start
    worst_case.append(elapsed)


def threaded_build_heap(num_elements: int, best_case: [], random_case: [], worst_case: []):
    random_numbers = [ran.random() for i in range(num_elements)]

    sorted_list = random_numbers[:]
    sorted_list.sort()

    reversed_list = sorted_list[:]
    reversed_list.sort(reverse=True)

    start = t.clock()
    build_max_heap(sorted_list)
    end = t.clock()
    elapsed = end - start
    best_case.append(elapsed)

    start = t.clock()
    build_max_heap(random_numbers)
    end = t.clock()
    elapsed = end - start
    random_case.append(elapsed)

    start = t.clock()
    build_max_heap(reversed_list)
    end = t.clock()
    elapsed = end - start
    worst_case.append(elapsed)


def threaded_extract_max(num_elements: int, random_case: []):
    random_numbers = [ran.random() for i in range(num_elements)]

    start = t.clock()
    for i in random_numbers:
        heap_extract_max(random_numbers)
    end = t.clock()
    elapsed = end - start
    random_case.append(elapsed)
