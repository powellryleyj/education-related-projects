# Ryley Powell
# CS3581 - 021
# Lab 7 - Friend Recommendations (BFS)

from collections import deque
import copy
import sys


class DiGraph:
    """
    Implements a directed graph.

    Vertices can be any hashable object (e.g., number, string, tuple).

    The graph is stored using a dictionary.  The vertices are keys.
    The value for each vertex is a set of vertices.  An edge from vertex
    u to vertex v is present in the graph if v in self._edges[u].
    """

    def __init__(self):
        self._edges = dict()

    def add_vertex(self, v):
        """
        Adds the given vertex v. If v is already in the
        graph, it isn't added again.
        """
        if not self.vertex_exists(v):
            self._edges[v] = set()

    def add_edge(self, u, v):
        """
        Adds an edge from vertex u to vertex v.  If the edge is
        already in the graph, if it isn't added again.  If u or
        v are not present in the graph, they are added.
        """
        self.add_vertex(u)
        self.add_vertex(v)

        if not self.edge_exists(u, v):
            self._edges[u].add(v)

    def edge_set(self):
        """
        This method will loop through all of the edges, create a 2-tuple of the vertices
        involved in each edge for each edge, and add the tuples to a set.
        The set will then be returned.
        """
        edge_set = set()
        for u in self._edges.keys():
            for v in self._edges[u]:
                edge_set.add((u, v))
        return edge_set

    def vertex_exists(self, u):
        """
        Returns true if u is in the graph, false otherwise
        """
        return True if u in self._edges.keys() else False

    def edge_exists(self, u, v):
        """
        Returns true if there is an edge from u to v in the graph, false otherwise.
        If u or v are not in the graph, false is returned.
        """
        if self.vertex_exists(u) and self.vertex_exists(v):
            return True if v in self._edges[u] else False
        else:
            return False

    def get_outgoing_edges(self, u):
        """
        Returns a collection of edges starting at vertex u.
        """
        return self._edges[u]

    def count_vertices(self):
        """
        Counts the number of vertices in the graph
        """
        return len(self._edges.keys())

    def count_edges(self):
        """
        Counts the number of edges in the graph
        """
        count = 0
        for i in self._edges:
            count += len(self._edges[i])
        return count


class Vertex:
    """
    Models vertices in a graph.  The pi, color, and d attributes
    are used to store information as part of a breadth-first search.
    The name attribute is used for a unique identifier for each
    vertex.
    """

    def __init__(self, pi=None, color="WHITE", d=sys.maxsize, name=None):
        self.pi = pi
        self.color = color
        self.d = d
        self.name = name


def load_data(training_flname, testing_flname):
    """
    Loads the training and testing set data. Returns
    a pair of graphs corresponding to the two data sets.
    """
    # We want to avoid creating duplicate Vertex objects
    # for the same user.  We will check the dictionary for
    # an existing Vertex object before creating another
    vertices = dict()
    with open(training_flname) as fl:
        G1 = DiGraph()
        for ln in fl:
            cols = ln.split()
            if cols[0] not in vertices:
                vertices[cols[0]] = Vertex(name=cols[0])
            if cols[1] not in vertices:
                vertices[cols[1]] = Vertex(name=cols[1])
            u = vertices[cols[0]]
            v = vertices[cols[1]]
            G1.add_edge(u, v)

    with open(testing_flname) as fl:
        G2 = DiGraph()
        for ln in fl:
            cols = ln.split()
            if cols[0] not in vertices:
                vertices[cols[0]] = Vertex(name=cols[0])
            if cols[1] not in vertices:
                vertices[cols[1]] = Vertex(name=cols[1])
            u = vertices[cols[0]]
            v = vertices[cols[1]]
            G2.add_edge(u, v)

    return G1, G2


def precision(recommendations, testing_set):
    """
    Precision measures the fraction of positive predictions
    were found in the test set (were true positives).

    A precise algorithm rarely makes false positive predictions.
    """
    rec_edges = recommendations.edge_set()
    test_edges = testing_set.edge_set()
    intersection = rec_edges.intersection(test_edges)

    if len(rec_edges) == 0:
        return 0.0

    return float(len(intersection)) / len(rec_edges)


def recall(recommendations, testing_set):
    """
    Recall measures the fraction of test set that
    were predicted positively.
    """
    rec_edges = recommendations.edge_set()
    test_edges = testing_set.edge_set()
    intersection = rec_edges.intersection(test_edges)

    if len(test_edges) == 0:
        return 0.0

    return float(len(intersection)) / len(test_edges)


def bfs(G, s):
    """
    Performs a breadth-first search of the graph G, starting at vertex s.
    
    Hints:
     * sys.maxsize can be used in place of the INFTY constant.
     * The Python data structure deque is a double-ended queue. You can
       use q.append() to add the back of the queue, q.popleft() to remove
       items from the front of the queue, and len(q) to check if the queue
       is empty (len(q) == 0).
    """
    for v in G._edges.keys():
        v.color = VertexColor.WHITE
        v.d = sys.maxsize
        v.pi = None

    s.color = VertexColor.GRAY
    s.d = 0
    s.pi = None

    q = deque()
    q.append(s)

    while len(q) != 0:
        u = q.popleft()
        for v in G._edges[u]:
            if v.color == VertexColor.WHITE:
                v.color = VertexColor.GRAY
                v.d = u.d + 1
                v.pi = u
                q.append(v)
        u.color = VertexColor.BLACK


def recommend_friends_for_user(G, s, max_depth):
    """
    Performs a breadth-first search of the graph G, starting at vertex s.
    Does not traverse vertices with d > max_depth.
    Returns a list of all vertices encountered.
    
    Hints:
     * sys.maxsize can be used in place of the INFTY constant.
     * The Python data structure deque is a double-ended queue. You can
       use q.append() to add the back of the queue, q.popleft() to remove
       items from the front of the queue, and len(q) to check if the queue
       is empty (len(q) == 0).
    """
    for v in G._edges.keys():
        v.color = VertexColor.WHITE
        v.d = sys.maxsize
        v.pi = None

    s.color = VertexColor.GRAY
    s.d = 0
    s.pi = None

    q = deque()
    q.append(s)

    recommendations = list()
    while len(q) != 0:
        u = q.popleft()
        for v in G._edges[u]:
            if v.color == VertexColor.WHITE:
                v.color = VertexColor.GRAY
                v.d = u.d + 1
                v.pi = u
                q.append(v)
                if v not in recommendations and v.d <= max_depth:
                    recommendations.append(v)
        if u.d <= max_depth:
            u.color = VertexColor.BLACK
    return recommendations


def recommend_all_friends(G, max_depth):
    """
    Generates recommendations for all users by performing
    a depth-limited breadth-first search for each user.
    
    The resulting recommendations are stored as a DiGraph.
    """
    H = DiGraph()
    for u in G._edges.keys():
        targets = recommend_friends_for_user(G, u, max_depth)
        for v in targets:
            if not G.edge_exists(v, u):
                H.add_edge(u, v)
                H.add_edge(v, u)
    return H


class VertexColor:
    WHITE = "WHITE"
    BLACK = "BLACK"
    GRAY = "GRAY"
