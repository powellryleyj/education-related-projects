import sys
import threading


def insertion_sort(lst, p, r):
    """
    Sorts the list lst in place using insertion_sort.
    """
    for n in range(p + 1, r):
        key = lst[n]
        j = n - 1

        while j >= p and lst[j] > key:
            lst[j + 1] = lst[j]
            j -= 1

        lst[j + 1] = key


def merge_sort(lst, p, r):
    """
    Sorts the list lst in place using merge_sort.

    Args:
        lst: list to be sorted
        p: starting index of the list, i.e 0
        r: ending index of the list, i.e. len(lst) - 1
    """
    if p < r:
        q = int((p + r) / 2)
        merge_sort(lst, p, q)
        merge_sort(lst, q + 1, r)

        merge(lst, p, q, r)


def merge(lst, p, q, r):
    """
    Merges two sorted partitions of lst.  This is
    performed in-place. It is assumed that the
    two partitions are contiguous in the list.

    Args:
        lst: list that is being sorted
        p: starting index of first partition
        q: ending index of first partition
        r: ending index of second partition
    """
    i = j = 0
    k = p
    left = lst[p:q + 1]
    right = lst[q + 1:r + 1]

    while i < len(left) and j < len(right):
        if left[i] < right[j]:
            lst[k] = left[i]
            i += 1
        else:
            lst[k] = right[j]
            j += 1
        k += 1

    while i < len(left):
        lst[k] = left[i]
        i += 1
        k += 1

    while j < len(right):
        lst[k] = right[j]
        j += 1
        k += 1


class ParallelMergeSortV1:
    """
    This version of parallel merge sort is first-attempt implementation without consulting external sources
    """

    @staticmethod
    def parallel_merge_sort(lst, p, r, t=sys.maxsize):
        """
            Sorts the list lst in place using merge_sort.

            Args:
                lst: list to be sorted
                p: starting index of the list, i.e 0
                r: ending index of the list, i.e. len(lst) - 1
                t: maximum number of threads
        """
        if p < r:
            q = int((p + r) / 2)
            th = None
            if threading.active_count() < t:
                # print("Number of threads active: ", threading.active_count())
                th = threading.Thread(target=ParallelMergeSortV1.parallel_merge_sort, args=(lst, p, q, t))
                th.start()
            else:
                ParallelMergeSortV1.parallel_merge_sort(lst, p, q)

            ParallelMergeSortV1.parallel_merge_sort(lst, q + 1, r)

            if th is not None:
                th.join()

            ParallelMergeSortV1.parallel_merge(lst, p, q, r)

    @staticmethod
    def parallel_merge(lst, p, q, r):
        """
            Merges two sorted partitions of lst.  This is
            performed in-place. It is assumed that the
            two partitions are contiguous in the list.

            Args:
                lst: list that is being sorted
                p: starting index of first partition
                q: ending index of first partition
                r: ending index of second partition
        """
        i = j = 0
        k = p
        left = lst[p:q + 1]
        right = lst[q + 1:r + 1]

        while i < len(left) and j < len(right):
            if left[i] < right[j]:
                lst[k] = left[i]
                i += 1
            else:
                lst[k] = right[j]
                j += 1
            k += 1

        while i < len(left):
            lst[k] = left[i]
            i += 1
            k += 1

        while j < len(right):
            lst[k] = right[j]
            j += 1
            k += 1


class ParallelMergeSortV2:
    """
    This implementation uses the idea from GeeksForGeek in which the number of threads are decided
    """

    @staticmethod
    def parallel_merge_sort2(lst, p, r, t=4):
        """
        Sorts the list lst in place using merge_sort.

        Args:
            lst: list to be sorted
            p: starting index of the list, i.e 0
            r: ending index of the list, i.e. len(lst) - 1
            t: number of threads to use
        """
        size = len(lst)
        if t == 0:
            merge_sort(lst, 0, len(lst) - 1)
        else:
            threads = []
            for i in range(t):
                p = int(i * (len(lst) / t))
                r = int((i + 1) * (len(lst) / t) - 1)
                # print("left: ", p, "; right: ", r)
                threads.append(threading.Thread(target=merge_sort, args=(lst, p, r)))

            for j in threads:
                j.start()

            for k in threads:
                k.join()

        merge(lst, 0, int((size / 2 - 1) / 2), int(size / 2 - 1))
        merge(lst, int(size / 2), int(size / 2 + (size - 1 - size / 2) / 2), int(size - 1))
        merge(lst, 0, int((size - 1) / 2), int(size - 1))


class ParallelMergeSortV3:
    num_threads = 0
    lock = threading.Lock()

    @staticmethod
    def parallel_merge_sort3(lst, p, r, t=sys.maxsize):
        """
            Sorts the list lst in place using merge_sort.

            Args:
                lst: list to be sorted
                p: starting index of the list, i.e 0
                r: ending index of the list, i.e. len(lst) - 1
                t: maximum number of threads
        """
        if p < r:
            q = int((p + r) / 2)
            th = None

            ParallelMergeSortV3.lock.acquire(True)
            # print("Max Number of Threads: ", t)
            if ParallelMergeSortV3.num_threads < t:
                ParallelMergeSortV3.num_threads += 1
                # print("Number of Threads: ", ParallelMergeSortV3.num_threads)
                ParallelMergeSortV3.lock.release()
                # print("Number of threads active: ", threading.active_count())
                th = threading.Thread(target=ParallelMergeSortV3.parallel_merge_sort3, args=(lst, p, q, t))
                th.start()
            else:
                ParallelMergeSortV3.lock.release()
                ParallelMergeSortV3.parallel_merge_sort3(lst, p, q, t)

            ParallelMergeSortV3.parallel_merge_sort3(lst, q + 1, r, t)

            if th is not None:
                th.join()

            merge(lst, p, q, r)


class ParallelMergeSortV4:

    @staticmethod
    def parallel_merge_sort4(lst, p, r, t=sys.maxsize):
        """
            Sorts the list lst in place using merge_sort.

            Args:
                lst: list to be sorted
                p: starting index of the list, i.e 0
                r: ending index of the list, i.e. len(lst) - 1
                t: maximum number of elements to sort per thread
        """
        if p < r:
            q = int((p + r) / 2)
            tl, tr = None, None
            # print("Elements Per Thread: ", t)
            # print("Left Partition Size: ", (q - p))
            # print("Right Partition Size: ", (r - q))
            if (len(lst[p:q])) >= t:
                # print("Starting Left Thread")
                # print("Threads Active: ", threading.active_count())
                tl = threading.Thread(target=ParallelMergeSortV4.parallel_merge_sort4, args=(lst, p, q, t))
                tl.start()
            else:
                ParallelMergeSortV4.parallel_merge_sort4(lst, p, q, t)

            if (len(lst[q + 1:r])) >= t:
                # print("Starting Right Thread")
                tr = threading.Thread(target=ParallelMergeSortV4.parallel_merge_sort4, args=(lst, q + 1, r, t))
                tr.start()
            else:
                ParallelMergeSortV4.parallel_merge_sort4(lst, q + 1, r, t)
            # ParallelMergeSortV4.parallel_merge_sort4(lst, q + 1, r, t)

            if tl is not None:
                tl.join()

            if tr is not None:
                tr.join()

            merge(lst, p, q, r)


class ParallelMergeInsertionSort:
    num_threads = 0
    lock = threading.Lock()

    @staticmethod
    def parallel_merge_insertion_sort(lst, p, r, n=0, t=sys.maxsize):
        """
            Sorts the list lst in place using merge_sort.
            Switches to Insertion Sort when the number of indices between p and r are less than n

            Args:
                lst: list to be sorted
                p: starting index of the list, i.e 0
                r: ending index of the list, i.e. len(lst) - 1
                n: the number of indices
                t: maximum number of threads
        """
        if (r - p) <= n:
            insertion_sort(lst, p, r)
        # elif p < r:
        else:
            q = int((p + r) / 2)
            th = None

            ParallelMergeInsertionSort.lock.acquire(True)
            # print("Max Number of Threads: ", t)
            if ParallelMergeInsertionSort.num_threads < t:
                ParallelMergeInsertionSort.num_threads += 1
                # print("Number of Threads: ", ParallelMergeSortV3.num_threads)
                ParallelMergeInsertionSort.lock.release()
                # print("Number of threads active: ", threading.active_count())
                th = threading.Thread(target=ParallelMergeInsertionSort.parallel_merge_insertion_sort, args=(lst, p, q, n, t))
                th.start()
            else:
                ParallelMergeInsertionSort.lock.release()
                ParallelMergeInsertionSort.parallel_merge_insertion_sort(lst, p, q, n, t)

            ParallelMergeInsertionSort.parallel_merge_insertion_sort(lst, q + 1, r, n, t)

            if th is not None:
                th.join()

            merge(lst, p, q, r)
