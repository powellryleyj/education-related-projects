import numpy as num
import random as ran
import time as t


def benchmark(function, num_elements, max_threads=None):
    """
    This function serves as a central test-bed for benchmarking merge sort and parallel merge sort
    Will run the given function 10 times
    :param function: the function signature
    :param num_elements: the number of elements to be sorted in a given trial
    :param max_threads: if provided, this will run the parallel version of merge sort, limiting the number of active threads to the passed value
    :return: returns 3-tuple of average run times over 10 trials
    """
    exe_times = []
    for i in range(10):
        # create array with random values
        random_list = [ran.random() for i in range(num_elements)]

        # benchmark random case
        start = t.clock()
        if max_threads is not None:
            function(random_list, 0, len(random_list) - 1, max_threads)
        else:
            function(random_list, 0, len(random_list) - 1)
        stop = t.clock()
        exe_times.append(stop - start)

    return num.average(exe_times)
