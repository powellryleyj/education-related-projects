import unittest
import random
import time
from sorting import *

N_NUMBERS = 100
N_THREADS = 10
N_ELEMENTS = 25


class TestMergeSort(unittest.TestCase):

    def test_merge_sort_random(self):
        """
        Tests merge sort using a list of randomly-ordered
        numbers.
        """

        numbers = [random.random() for i in range(N_NUMBERS)]

        # make a copy of the list
        observed = numbers[:]
        start = time.clock()
        merge_sort(observed, 0, len(observed) - 1)
        end = time.clock()
        print('Exe Time: ', end - start)

        # make a copy of the list
        # and sort using built-in function
        expected = numbers[:]
        expected.sort()

        self.assertListEqual(observed, expected)

    def test_merge_sort_sorted(self):
        """
        Tests merge sort using a list of sorted
        numbers. Ensures that insertion sort maintains
        the sorted property.
        """

        numbers = [random.random() for i in range(N_NUMBERS)]
        numbers.sort()

        # make a copy of the list
        expected = numbers[:]

        # make a copy of the list
        observed = numbers[:]
        merge_sort(observed, 0, len(observed) - 1)

        self.assertListEqual(observed, expected)

    def test_merge(self):
        """
        Tests that the merge operation successfully
        merges two sorted lists into a single sorted list.
        """

        lst1 = [random.random() for i in range(N_NUMBERS)]
        lst1.sort()

        lst2 = [random.random() for i in range(N_NUMBERS)]
        lst2.sort()

        merged = lst1 + lst2
        merge(merged, 0, len(lst1) - 1, len(merged) - 1)

        expected = lst1 + lst2
        expected.sort()

        self.assertListEqual(expected, merged)


class TestParallelMergeSortV1(unittest.TestCase):
    def test_parallel_merge_sort_random(self):
        """
        Tests merge sort using a list of randomly-ordered
        numbers.
        """

        numbers = [random.random() for i in range(N_NUMBERS)]

        # make a copy of the list
        observed = numbers[:]
        ParallelMergeSortV1.parallel_merge_sort(observed, 0, len(observed) - 1, N_THREADS)

        # make a copy of the list
        # and sort using built-in function
        expected = numbers[:]
        expected.sort()

        self.assertListEqual(observed, expected)

    def test_parallel_merge_sort_sorted(self):
        """
        Tests merge sort using a list of sorted
        numbers. Ensures that insertion sort maintains
        the sorted property.
        """

        numbers = [random.random() for i in range(N_NUMBERS)]
        numbers.sort()

        # make a copy of the list
        expected = numbers[:]

        # make a copy of the list
        observed = numbers[:]
        ParallelMergeSortV1.parallel_merge_sort(observed, 0, len(observed) - 1, N_THREADS)

        self.assertListEqual(observed, expected)

    def test_parallel_merge(self):
        """
        Tests that the merge operation successfully
        merges two sorted lists into a single sorted list.
        """

        lst1 = [random.random() for i in range(N_NUMBERS)]
        lst1.sort()

        lst2 = [random.random() for i in range(N_NUMBERS)]
        lst2.sort()

        merged = lst1 + lst2
        ParallelMergeSortV1.parallel_merge(merged, 0, len(lst1) - 1, len(merged) - 1)

        expected = lst1 + lst2
        expected.sort()

        self.assertListEqual(expected, merged)


class TestParallelMergeSortV2(unittest.TestCase):

    def test_parallel_merge_sort_random2(self):
        """
        Tests merge sort using a list of randomly-ordered
        numbers.
        """

        numbers = [random.random() for i in range(N_NUMBERS)]

        # make a copy of the list
        observed = numbers[:]
        ParallelMergeSortV2.parallel_merge_sort2(observed, 0, len(observed) - 1, 4)

        # make a copy of the list
        # and sort using built-in function
        expected = numbers[:]
        expected.sort()

        self.assertListEqual(observed, expected)

    def test_parallel_merge_sort_sorted2(self):
        """
        Tests merge sort using a list of sorted
        numbers. Ensures that insertion sort maintains
        the sorted property.
        """

        numbers = [random.random() for i in range(N_NUMBERS)]
        numbers.sort()

        # make a copy of the list
        expected = numbers[:]

        # make a copy of the list
        observed = numbers[:]
        ParallelMergeSortV2.parallel_merge_sort2(observed, 0, len(observed) - 1, 4)

        self.assertListEqual(observed, expected)


class TestParallelMergeSortV3(unittest.TestCase):

    def test_parallel_merge_sort_random3(self):
        """
        Tests merge sort using a list of randomly-ordered
        numbers.
        """

        numbers = [random.random() for i in range(N_NUMBERS)]

        # make a copy of the list
        observed = numbers[:]
        ParallelMergeSortV3.parallel_merge_sort3(lst=observed, p=0, r=int(len(observed) - 1), t=N_THREADS)

        # make a copy of the list
        # and sort using built-in function
        expected = numbers[:]
        expected.sort()

        self.assertListEqual(observed, expected)

    def test_parallel_merge_sort_sorted3(self):
        """
        Tests merge sort using a list of sorted
        numbers. Ensures that insertion sort maintains
        the sorted property.
        """

        numbers = [random.random() for i in range(N_NUMBERS)]
        numbers.sort()

        # make a copy of the list
        expected = numbers[:]

        # make a copy of the list
        observed = numbers[:]
        ParallelMergeSortV3.parallel_merge_sort3(lst=observed, p=0, r=int(len(observed) - 1), t=N_THREADS)

        self.assertListEqual(observed, expected)


class TestParallelMergeSortV4(unittest.TestCase):

    def test_parallel_merge_sort_random4(self):
        """
        Tests merge sort using a list of randomly-ordered
        numbers.
        """

        numbers = [random.random() for i in range(N_NUMBERS)]

        # make a copy of the list
        observed = numbers[:]
        start = time.clock()
        ParallelMergeSortV4.parallel_merge_sort4(lst=observed, p=0, r=int(len(observed) - 1), t=N_ELEMENTS)
        end = time.clock()
        print("Exe Time: ", end - start)

        # make a copy of the list
        # and sort using built-in function
        expected = numbers[:]
        expected.sort()

        self.assertListEqual(observed, expected)

    def test_parallel_merge_sort_sorted4(self):
        """
        Tests merge sort using a list of sorted
        numbers. Ensures that insertion sort maintains
        the sorted property.
        """

        numbers = [random.random() for i in range(N_NUMBERS)]
        numbers.sort()

        # make a copy of the list
        expected = numbers[:]

        # make a copy of the list
        observed = numbers[:]
        ParallelMergeSortV4.parallel_merge_sort4(lst=observed, p=0, r=int(len(observed) - 1), t=N_ELEMENTS)

        self.assertListEqual(observed, expected)


class TestInsertionSort(unittest.TestCase):
    def test_insertion_sort_random(self):
        """
        Tests insertion sort using a list of randomly-ordered
        numbers.
        """

        numbers = [random.random() for i in range(N_NUMBERS)]

        # make a copy of the list
        observed = numbers[:]
        insertion_sort(observed, 0, len(observed))

        # make a copy of the list
        # and sort using built-in function
        expected = numbers[:]
        expected.sort()

        self.assertEqual(observed, expected)

    def test_insertion_sort_sorted(self):
        """
        Tests insertion sort using a list of sorted
        numbers.  Ensures that insertion sort maintains
        the sorted property.
        """

        numbers = [random.random() for i in range(N_NUMBERS)]
        numbers.sort()

        # make a copy of the list
        expected = numbers[:]

        # make a copy of the list
        observed = numbers[:]
        insertion_sort(observed, 0, len(observed))

        self.assertEqual(observed, expected)


class TestParallelMergeInsertionSort(unittest.TestCase):

    def test_parallel_merge_insertion_sort_random(self):
        """
        Tests merge sort using a list of randomly-ordered
        numbers.
        """

        numbers = [random.random() for i in range(N_NUMBERS)]

        # make a copy of the list
        observed = numbers[:]
        ParallelMergeInsertionSort.parallel_merge_insertion_sort(lst=observed, p=0, r=int(len(observed) - 1), n=10, t=N_THREADS)

        # make a copy of the list
        # and sort using built-in function
        expected = numbers[:]
        expected.sort()

        self.assertListEqual(observed, expected)

    def test_parallel_merge_insertion_sort_sorted3(self):
        """
        Tests merge sort using a list of sorted
        numbers. Ensures that insertion sort maintains
        the sorted property.
        """

        numbers = [random.random() for i in range(N_NUMBERS)]
        numbers.sort()

        # make a copy of the list
        expected = numbers[:]

        # make a copy of the list
        observed = numbers[:]
        ParallelMergeInsertionSort.parallel_merge_insertion_sort(lst=observed, p=0, r=int(len(observed) - 1), n=10, t=N_THREADS)

        self.assertListEqual(observed, expected)


if __name__ == "__main__":
    unittest.main()
