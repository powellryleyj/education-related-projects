from sorting import insertion_sort, merge_sort
import random as ran
import time as t


def insertion_thread(num_elements: int, best_case: [], random_case: [], worst_case: []):
    random_numbers = [ran.random() for i in range(num_elements)]
    print(random_numbers)
    sorted_list = random_numbers[:]
    sorted_list.sort()
    print(sorted_list)

    reversed_list = sorted_list[:]
    sorted_list.sort(reverse=True)
    print(reversed_list)

    start = t.clock()
    insertion_sort(sorted_list)
    end = t.clock()
    elapsed = end - start
    best_case.append(elapsed)

    start = t.clock()
    insertion_sort(random_numbers)
    end = t.clock()
    elapsed = end - start
    random_case.append(elapsed)

    start = t.clock()
    insertion_sort(reversed_list)
    end = t.clock()
    elapsed = end - start
    worst_case.append(elapsed)


def merge_thread(num_elements: int, best_case: [], random_case: [], worst_case: []):
    random_numbers = [ran.random() for i in range(num_elements)]

    sorted_list = random_numbers[:]
    sorted_list.sort()

    reversed_list = sorted_list[:]
    sorted_list.sort(reverse=True)

    start = t.clock()
    merge_sort(sorted_list, 0, len(sorted_list) - 1)
    end = t.clock()
    elapsed = end - start
    best_case.append(elapsed)

    start = t.clock()
    merge_sort(random_numbers, 0, len(random_numbers) - 1)
    end = t.clock()
    elapsed = end - start
    random_case.append(elapsed)

    start = t.clock()
    merge_sort(reversed_list, 0, len(reversed_list) - 1)
    end = t.clock()
    elapsed = end - start
    worst_case.append(elapsed)