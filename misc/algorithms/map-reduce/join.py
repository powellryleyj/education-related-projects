import MapReduce
import sys

"""
Ryley Powell
CS3860
Lab8 - MapReduce
Relational Join for Problem 2

Directions: Just paste this into the directory with given MapReduce.py
Note: Only works on files that specify table name as the first column
and key to join on as the second column.
"""

mr = MapReduce.MapReduce()


# =============================
# Do not modify above this line

def mapper(record):
    # key: order_id
    # value: table row
    key = record[1]
    value = record
    mr.emit_intermediate(key, value)


def reducer(key, values):
    # key: order_id - not used in this function
    # values: all rows with order_id
    selected = values[0]
    for v in values:
        if selected[0] != v[0]:
            tmp = selected + v
            mr.emit(tmp)


# Do not modify below this line
# =============================
if __name__ == '__main__':
    inputdata = open(sys.argv[1], 'r')
    mr.execute(inputdata, mapper, reducer)
