def insertion_sort(lst):
    """
    Sorts the list lst in place using insertion_sort.
    """
    for n in range(1, len(lst)):
        key = lst[n]
        j = n - 1

        while j >= 0 and lst[j] > key:
            lst[j + 1] = lst[j]
            j -= 1

        lst[j + 1] = key
