import urllib.request
from PIL import Image

file = urllib.request.urlretrieve('http://www.hacker.org/challenge/img/redline.png')[0]
image = Image.open(file)

print(''.join([hex(image.getpixel((x ,0))[0])[2:] for x in range(image.size[0])]))
image.close()