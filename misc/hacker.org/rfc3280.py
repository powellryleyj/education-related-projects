# used as a mapping of word -> number of occurrences
words = dict()
with open('rfc3280.txt', 'r') as file:
    # the first for loop retrieves each line delimited by newline
    for line in file:
        # the second loop splits each line in to it's component strings, delimited by whitespace
        for word in line.split():
            # if the length of the current string is not exactly equal to 9, ignore it.
            if len(word) == 9:
                # convert all characters to lower-case to prevent duplicate keys of the same word with different letter-cases
                word.lower()
                # if the current string is not already in the mapping, add it
                if word not in words.keys():
                    words[word] = 0
                # increment the count by 1
                words[word] += 1

common = None
# iterate over the mappings to evaluate which word is most common.
for word in words.keys():
    if common is None:
        common = word
    elif words[common] < words[word]:
        common = word

print(common)
