from math import sqrt
hillary = 0
# loop over the numbers 0-2119 exclusively
for barack in range(2119):
    # take the square of the current number
    root = sqrt(barack)
    # based on how the python intepreter works, we can determine if the number is a perfect square when the root of the current
    # number + 0.5 raised to the second power equals the number itself.
    # this happens because how the int() function handles floating point values.
    if int(root + 0.5) ** 2 == barack:
        # print('root', root, '; perfect square: ', barack)
        # if a perfect square, add the value
        hillary += barack
    # add the value
    hillary += barack
    # print('value: ', hillary)

print(hillary)