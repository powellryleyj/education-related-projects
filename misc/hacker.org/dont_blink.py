import urllib.request


class LogRedirect(urllib.request.HTTPRedirectHandler):
    def http_error_302(self, request, f, c, message, headers):
        print(headers)
        return super().http_error_302(request, f, c, message, headers)


url = 'http://www.hacker.org/challenge/misc/one.php'
urllib.request.install_opener(urllib.request.build_opener(LogRedirect))
urllib.request.urlopen(url)
