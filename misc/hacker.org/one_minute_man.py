import urllib.request
import time


contents = set()
flag = True
while flag:
    url = urllib.request.urlopen('http://www.hacker.org/challenge/misc/minuteman.php').read().decode()

    if url not in contents:
        contents.add(url)
        print(url)
        print()

    time.sleep(30)
