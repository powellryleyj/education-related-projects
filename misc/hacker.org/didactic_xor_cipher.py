cipher = ['3d', '2e', '21', '2b', '20', '22', '6f', '3c', '2a', '2a', '2b']
key = 79
plain = ''
for i in cipher:
    dec = int(i, 16)
    print('dec: ', dec)
    xor = dec ^ key
    print('xor: ', xor)
    char = chr(xor)
    print('char: ', char)
    plain += char

print(plain)
