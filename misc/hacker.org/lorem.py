# used as a mapping of word -> number of occurrences
words = dict()
with open('lorem.txt', 'r') as file:
    # the first for loop retrieves each line delimited by newline
    for line in file:
        # the second loop splits each line in to it's component strings, delimited by whitespace
        for word in line.split():
            # convert all characters to lower-case to prevent duplicate keys of the same word with different letter-cases
            word.lower()
            # if the current string is not already in the mapping, add it
            if word not in words.keys():
                words[word] = 0
            # increment the count by 1
            words[word] += 1

# print out the mapping that has a value of exactly 1
for word in words.keys():
    if words[word] == 1:
        print(word)
