cipher = '8776459cf37d459fbb7b5ecfbb7f5fcfb23e478aaa3e4389f378439aa13e4e96a77b5fc1f358439df36a4486a03e4381b63e5580a66c0c8ebd6d5b8aa13e459cf34e4d9fa67f02cf90714288a17f589abf7f5886bc705fcfbc700c96bc6b5ecfb7775f8cbc68499daa3f'
# break cipher into each byte
cipher_array = [cipher[i:i + 2] for i in range(0, len(cipher), 2)]
decimal_array = []
# convert hex values into decimal values
for j in cipher_array:
    decimal_array.append(int(j, 16))

# convert the string using key values 0-255
cipher_counter = 0
key_part = 0
for key in range(2147483648):
    key_bytes = hex(key)
    key_bytes = key_bytes[2:]
    if len(key_bytes) < 8:
        bytes_to_prepend = 8 - len(key_bytes)
        affix = ''
        for i in range(bytes_to_prepend):
            affix = affix + '0'
        key_bytes = str(affix) + key_bytes

    key_bytes_array = [key_bytes[i:i+2] for i in range(0, len(key_bytes), 2)]

    output = []
    cipher_counter = 0
    key_part = 0
    while cipher_counter < len(decimal_array):
        if key_part == 4:
            key_part = 0

        c = int(key_bytes_array[key_part], 16) ^ decimal_array[cipher_counter]
        # print(c)
        output.append(c)

        key_part += 1
        cipher_counter += 1

    flag = True
    for ch in output:
        if 32 < ch < 122:
            flag = False

    if flag:
        ascii_out = ''
        for ch in output:
            ascii_out += chr(ch)

        print(key)
        print(ascii_out)




