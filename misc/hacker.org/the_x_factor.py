import math

n = 7393913335919140050521110339491123405991919445111971

while n % 2 == 0:
    print(2)
    n = n / 2

for i in range(int(math.sqrt(n)) + 1, 3, -2):
    while n % i == 0:
        print(i)
        n = n / i

if n > 2:
    print(n)

