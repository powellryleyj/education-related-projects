cipher = 'e5534adac53023aaad55518ac42671f8a1471d94d8676ce1b11309c1c27a64b1ae1f4a91c73f2bfce74c5e8e826c27e1f74c4f8081296ff3ee4519968a6570e2aa0709c2c4687eece44a1589903e79ece75117cec73864eebe57119c9e367fefe9530dc1 '
as_hex = bytes.fromhex(cipher)

key = 0
plaintext = ''
for i in range(0, len(as_hex), 4):
    part = int.from_bytes(as_hex[i:i + 4], byteorder='big')
    c = key ^ part
    plaintext += ''.join(map(chr, c.to_bytes(4, byteorder='big')))
    key = part

print('string: ', plaintext)
