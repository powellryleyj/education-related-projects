cipher = '310a7718781f734c31425e775a314f3b40132c5122720599b2dfb790fd8ff894add2a4bdc5d1a6e987a0ed8eee94adcfbb94ee88f382127819623a404d3f'
as_hex = bytes.fromhex(cipher)

for x in range(256):
    for k in range(256):
        plaintext = ''
        for i in range(len(as_hex)):
            c = k ^ as_hex[i]
            plaintext += chr(c)
            k = (as_hex[i] + x) % 0x100

        if 'I think' in plaintext:
            print('x: ', x, '; k: ', k, '; text: ', plaintext)
        else:
            plaintext = ''
