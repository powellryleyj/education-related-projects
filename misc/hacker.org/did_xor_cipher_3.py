import string

cipher = '31cf55aa0c91fb6fcb33f34793fe00c72ebc4c88fd57dc6ba71e71b759d83588'

as_hex = bytes.fromhex(cipher)
for b in range(256):
    for x in range(256):
        plaintext = ''
        for i in as_hex:
            plaintext += chr(i ^ b)
            b = (b + x) % 256

        if all(map(lambda c: c in string.printable, plaintext)):
            print('b: ', b, '; x: ', x, '; text: ', plaintext)
        else:
            plaintext = ''
