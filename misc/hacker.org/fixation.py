import urllib.request
from PIL import Image

file = urllib.request.urlretrieve('http://www.hacker.org/challenge/img/swirl.gif')[0]
image = Image.open(file)

while True:
    try:
        image.seek(image.tell() + 1)
    except EOFError:
        break
    image.show()

image.close()