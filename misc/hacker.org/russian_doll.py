import gzip
import urllib.request

file = urllib.request.urlopen('http://www.hacker.org/challenge/misc/doll.bin').read()

while True:
    try:
        file = gzip.decompress(file)
    except:
        break

print(file.decode())