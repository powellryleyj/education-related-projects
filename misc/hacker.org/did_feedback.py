cipher = '751a6f1d3d5c3241365321016c05620a7e5e34413246660461412e5a2e412c49254a24'

as_hex = bytes.fromhex(cipher)

for key in range(256):
    plaintext = ''
    for i in range(len(as_hex)):
        c = key ^ as_hex[i]
        plaintext += chr(c)
        key = as_hex[i]
    if 'your' in plaintext:
        print('key: ', key, '; string: ', plaintext)
    else:
        plaintext = ''


