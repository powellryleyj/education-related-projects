import urllib.request
from PIL import Image

file = urllib.request.urlretrieve('http://www.hacker.org/challenge/img/greenline.png')[0]
image = Image.open(file)

print(''.join([chr(image.getpixel((x, 0))[1]) for x in range(image.size[0])]))
image.close()
