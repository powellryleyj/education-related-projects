cipher = '948881859781c4979186898d90c4c68c85878f85808b8b808881c6c4828b96c4908c8d97c4878c858888818a8381'
# break cipher into each byte
cipher_array = [cipher[i:i + 2] for i in range(0, len(cipher), 2)]
decimal_array = []
# convert hex values into decimal values
for j in cipher_array:
    decimal_array.append(int(j, 16))

# convert the string using key values 0-255
for key in range(256):
    output = ''
    for c in decimal_array:
        output += chr(c ^ key)

    print('key :', key)
    print('message: ', output)
