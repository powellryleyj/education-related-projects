//
// Checkout.java: implementation of simple checkout module for a cashier system
//

package implementation;

import java.util.ArrayList;

public class Checkout {
  
    private ArrayList<Fruit> fruits;
  
    public Checkout() {
      fruits = new ArrayList<Fruit>();
    }

    public void setPrice(String item_name, int price) {
      int index = checkFruit(item_name);
      if(index == -1) {
        fruits.add(new Fruit(item_name, price));
      } else {
        fruits.get(index).setPrice(price);
      }
    }

    public void add(String item_name, int count) {
      int index = checkFruit(item_name);
      if(index == -1) {
        System.out.println("Fruit does not exist");
      } else {
        fruits.get(index).add(count);
      }
    }

    public int total() {
      int total = 0;
      for(int i = 0; i < fruits.size(); i++) {
        total += (fruits.get(i).getAmount() * fruits.get(i).getPrice());
      }
      return total;
    }
    
    private int checkFruit(String fruitName) {
      int index = 0;
      while (index < fruits.size()
        && fruitName != fruits.get(index).getName()) {
          index++;
        }
      if(index >= fruits.size()) {
        return -1;
      } else {
        return index;
      }
    }
    
    private class Fruit {
  
    private String fName;
    private int price;
    private int amount;
  
    public Fruit(String name, int price) {
      fName = name;
      this.price = price;
      amount = 0;
    }
  
    public void add(int numberOfFruit) {
      amount += numberOfFruit;
    }
  
    public String getName(){
      return fName;
    }
  
    public int getPrice(){
      return price;
    }
    
    public void setPrice(int price) {
      this.price = price;
    }
  
    public int getAmount() {
      return amount;
    }
}
}

