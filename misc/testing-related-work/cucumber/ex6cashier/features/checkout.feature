#
# checkout.feature: Cucumber script for cashier system
#

Feature: checking it out

  Scenario: Checking out a banana
    Given the price of a banana is 42c
    When I checkout 1 banana
    Then the total price should be 42c



  Scenario: Checkint out a banana of different price
    Given the price of a banana is 43c
    When I checkout 1 banana
    Then the total price should be 43c
    

  Scenario: Checkout two bananas
    Given the price of a banana is 42c
    When I checkout 2 banana
    Then the total price should be 84c

  Scenario Outline: checkout different product types
    Given the price of a <fruit> is <amount>c
    When I checkout 1 <fruit>
    Then the total price should be <amount>c
  Examples:
  | fruit   | amount |
  | apple   | 109    |
  | kumquat | 99     |
  | corn    | 2      |
  
  Scenario Outline: checkout different amounts of product types
    Give the price of a <fruit> is <price>c
    When I checkout <amount> <fruit>
    Then the total price should be <total>c
  Examples:
  | fruit | price | amount | total |
  | apple | 109   | 2      | 218   |
  | grape | 10    | 15     | 150   |
  | corn  | 2     | 152    | 304   |