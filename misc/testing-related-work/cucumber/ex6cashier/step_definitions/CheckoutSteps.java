//
// CheckoutSteps.java: step file implementing tests in checkout.features
// 

package step_definitions;

import cucumber.api.java8.En;
import cucumber.api.PendingException;
import static org.junit.Assert.*;

import implementation.Checkout;

public class CheckoutSteps implements En {
  
  private Checkout checkOut;
  
  public CheckoutSteps() {
    
    checkOut = new Checkout();
    
    Given("^the price of a (\\w+) is (\\d+)c$", (String fruit, Integer priceInCents) -> {
      checkOut.setPrice(fruit, priceInCents);
    });
    
    When("^I checkout (\\d+) (\\w+)$", (Integer numberOfItems, String fruit) -> {
      checkOut.add(fruit, numberOfItems);
    });
    
    Then("^the total price should be (\\d+)c$", (Integer totalPrice) -> {
      assertEquals(totalPrice.intValue(), checkOut.total());
    });
    
  }
  
}