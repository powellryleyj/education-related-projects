/**
 * Weather Station weather data generator. This class generates simulated temperature
 * and wind speed data whose values change periodically.
 */
package weatherData;

import java.util.Collection;
import java.util.LinkedList;

import javax.swing.SwingUtilities;

import weatherStation.Observer;
import weatherStation.Subject;
import weatherStation.TemperatureDisplay;
import weatherStation.WeatherStationMainDisplay;

/**
 * @author hornick
 */
public class WeatherDataGeneratorSimulator implements Subject {
   private double temp;
   private double windSpeed;
   private Collection<Observer> observers;

   /**
    * constructor; start the simulator
    */
   public WeatherDataGeneratorSimulator() {
      observers = new LinkedList<Observer>();

      new Thread(                // create a secondary Thread that generates weather data
            new Runnable() {     // create an object that can be run by the Thread (must implement Runnable)
               public void run() { // implement a run() method (makes the object a Runnable)
                  acquireDataFromSensors();
               }
            }
      ).start();                     // start the Thread, which causes the method above to execute off the main thread
   }

   /**
    * @return the temp
    */
   public double getTemp() {
      return temp;
   }

   /**
    * @return the windSpeed
    */
   public double getWindSpeed() {
      return windSpeed;
   }


   // simulate the acquisition of the current temp and wind speed
   private void acquireDataFromSensors() {
      while (true) {
         try {
            Thread.sleep(1000);
         } catch (InterruptedException e) {
         }
         temp = 74.0 + Math.random();   // vary temp from 74 to 75 degrees
         windSpeed = 4.8 + Math.random() * 0.4;      // vary wind speed from 4.8 to 5.2 MPH
         System.out.printf("T: %4.1f, S: %4.1f \n", temp, windSpeed);
         Runnable r = new Runnable() {
            public void run() {   // what thread does run() execute on?
               for (Observer o : observers) {
                  o.notifyObserver();
               }
            }
         };
         SwingUtilities.invokeLater(r);
      }
   }

   @Override
   // called when an Observer wants to subscribe to notifications
   public void attach(Observer o) {// what thread calls this method?
      observers.add(o);
   }

   @Override
   public void detach(Observer o) { // what thread calls this method?
      observers.remove(o);
   }

   @Override
   public void notifyObservers() {
      for (Observer o : observers) {
         o.notifyObserver();
      }
   }

} // end of WeatherDataGeneratorSimulator class
