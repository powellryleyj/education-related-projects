package weatherData;

// This interface defines the standard behaviors
// of a generic Weather Data provider
public interface WeatherDataProvider {
   /**
    * @return the temp
    */
   public double getTemp();

   /**
    * @return the wind speed
    */
   public double getWindSpeed();
}
