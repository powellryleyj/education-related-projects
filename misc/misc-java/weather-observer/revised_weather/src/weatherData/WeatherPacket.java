package weatherData;

// This class represents a consistent packet of weather data
public class WeatherPacket implements WeatherDataProvider {
   private double ws;
   private double temp;

   public WeatherPacket(double ws, double temp) {
      this.ws = ws;
      this.temp = temp;
   }

   public double getTemp() {
      return temp;
   }

   public double getWindSpeed() {
      return ws;
   }

}
