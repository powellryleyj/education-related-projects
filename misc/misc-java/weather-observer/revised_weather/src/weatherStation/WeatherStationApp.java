/**
 * Weather Station application
 */
package weatherStation;

import weatherData.WeatherDataGeneratorSimulator;

/**
 * @author hornick
 */
public class WeatherStationApp {

   /**
    * @param args - not used
    */
   public static void main(String[] args) {


      WeatherDataGeneratorSimulator wdg = new WeatherDataGeneratorSimulator();
      new WeatherStationMainDisplay(wdg);
      new TemperatureDisplay(wdg);
   }
}
