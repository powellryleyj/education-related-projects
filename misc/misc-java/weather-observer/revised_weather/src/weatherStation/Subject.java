package weatherStation;

public interface Subject {
   public void attach(Observer o); // called by Observer to subscribe to notifications

   public void detach(Observer o); // called by Observer to un-subscribe

   public void notifyObservers();   // called when Observer wants an immediate update
}
