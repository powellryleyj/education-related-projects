package weatherStation;

public interface Observer {
   public void notifyObserver(); // called by Subject to let the Observer know that new data is available
}
