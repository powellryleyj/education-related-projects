/**
 * Weather Station weather data generator. This class generates simulated temperature
 * and wind speed data whose values change periodically.
 */
package weatherData;

/**
 * @author hornick
 */
public class WeatherDataGeneratorSimulator {
   private double temp;
   private double windSpeed;

   /**
    * constructor; start the simulator
    */
   public WeatherDataGeneratorSimulator() {
      new Thread(                     // create a secondary Thread that generates weather data
            new Runnable() {         // create an object that can be run by the Thread (must implement Runnable)
               public void run() {      // implement a run() method (makes the object a Runnable)
                  acquireDataFromSensors();
               }
            }
      ).start();                     // start the Thread, which causes the method above to execute off the main thread
   }

   /**
    * @return the temp
    */
   public double getTemp() {
      return temp;
   }

   /**
    * @return the windSpeed
    */
   public double getWindSpeed() {
      return windSpeed;
   }


   // simulate the acquisition of the current temp and wind speed
   private void acquireDataFromSensors() {
      while (true) {
         try {
            Thread.sleep(1000);
         } catch (InterruptedException e) {
         }
         temp = 74.0 + Math.random();   // vary temp from 74 to 75 degrees
         windSpeed = 5.0 + Math.random() * 0.2;      // vary wind speed from 5 to 7 MPH
         System.out.printf("T: %4.1f, S: %4.1f \n", temp, windSpeed);
      }
   }

} // end of WeatherDataGeneratorSimulator class