/**
 * Weather Station temperature display UI
 */
package weatherStation;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import weatherData.WeatherDataGeneratorSimulator;

/**
 * @author hornick
 */
public class TemperatureDisplay extends JFrame {

	private WeatherDataGeneratorSimulator wdg;	// data source

	TemperatureDisplayUIListener eventListener;     // listener for UI events

	private JLabel txtTemperature;				// field for display of temp


	/**
	 * construct the UI
	 * @param wdg - WeatherDataGeneratorSimulator reference
	 */
	public TemperatureDisplay( WeatherDataGeneratorSimulator wdg ) {

		this.wdg = wdg; // save the reference to the WeatherDataGenerator object

		// create the nested event handler that will respond to incoming events
		eventListener = new TemperatureDisplayUIListener();

		createUserInterface();
	}

	/*
	 * Updates the label field in the UI that displays the result
	 * of getting the latest weather data.
	 */
	private void updateDisplay() {
		double temp = wdg.getTemp();
		DecimalFormat df = new DecimalFormat("0.0");
		txtTemperature.setText( df.format(temp) + " F" );
	}

	// build the UI
	private void createUserInterface() {

		// create the containing window and set its size etc
		this.setTitle("Temp");
		setSize(210, 210);
		setLocation(800, 600);
		setResizable(true);
		setVisible(true);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

		// Retrieves the window's contentPane and sets the background color to white
		Container contentPane = getContentPane();
		contentPane.setLayout(null);
		contentPane.setBackground(Color.BLUE);

		//Creation of "Calculate" button
		createButton("Update", "update", 10, 130, 170, 30, eventListener, contentPane);

		//Creation of "Current temp" label
		txtTemperature = new JLabel ("");
		Font font = new Font("Arial", Font.BOLD, 48 );
		txtTemperature.setFont(font);
		txtTemperature.setForeground(Color.WHITE);
		txtTemperature.setBounds(10, 10, 170, 100);
		contentPane.add(txtTemperature);

		contentPane.validate();
		contentPane.repaint();

	} // end of Constructor method

	/**
	 * Creates a JButton and adds it to the UI.
	 * @param name Name to appear on the button
	 * @param actionCmd Action command associated with the button
	 * @param x Horizontal coordinate of the upper left corner of the button
	 * @param y Vertical coordinate of the upper left corner of the button
	 * @param width Width of the button
	 * @param height Height of the button
	 * @param listener ActionListener to be added as a listener
	 * @param pane Container where the button will be added
	 */
	private void createButton(String name, String actionCmd, int x, int y, int width, int height, ActionListener listener, Container pane) {
		JButton btn;
		btn = new JButton (name); 
		btn.setActionCommand(actionCmd);
		btn.setBounds(x, y, width, height);
		btn.addActionListener(listener);	// subscribe to events
		pane.add(btn);		
	}

	/**
	 * Adds a JTextField and adds it to the UI.
	 * @param txtField Reference for the JTextField
	 * @param actionCmd Action command associated with the button
	 * @param x Horizontal coordinate of the upper left corner of the button
	 * @param y Vertical coordinate of the upper left corner of the button
	 * @param width Width of the button
	 * @param height Height of the button
	 * @param listener ActionListener to be added as a listener
	 * @param pane Container where the button will be added
	 */
	private JTextField createTextField(String actionCmd, int x, int y, int width, int height, ActionListener listener, Container pane) {
		JTextField txtField = new JTextField();
		txtField.setBounds(x, y, width, height);
		txtField.setActionCommand(actionCmd);
		txtField.addActionListener(listener);
		txtField.setEditable(false);
		pane.add(txtField);
		return txtField;
	}


	/**
	 * This is a nested inner class that handles events
	 * for the WeatherStationUI  
	 * @author hornick
	 */
	private class TemperatureDisplayUIListener implements ActionListener {

		/**
		 * Constructor; does nothing.
		 */ 
		private TemperatureDisplayUIListener() {
		}

		/**
		 * ActionListener event handler for this class
		 * @param event - the ActionEvent object that caused the event
		 */
		public void actionPerformed(ActionEvent event) {

			String cmd = event.getActionCommand(); 	// get the Action Command that caused this event

			// checks whether the "update" button was pressed
			if ( cmd.equals("update") )
			{
				updateDisplay();	// delegate the actual handling of the event to another method
			}
		}

	}	// end of TemperatureDisplayUIListener inner class

}  // end of TemperatureDisplay class
