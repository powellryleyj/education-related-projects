/**
 * Main UI for Weather Station
 */
package weatherStation;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import weatherData.WeatherDataGeneratorSimulator;

/**
 * @author hornick
 */
public class WeatherStationMainDisplay extends JFrame {

	private WeatherDataGeneratorSimulator wdg;		// data source

	private WeatherStationUIListener eventListener; // UI event listener

	private JTextField txtTemperature;	// temperature display field

	private JTextField txtWindSpeed;	// wind speed display field

	/**
	 * construct the UI
	 * @param wdg - WeatherDataGeneratorSimulator reference
	 */
	public WeatherStationMainDisplay( WeatherDataGeneratorSimulator wdg ) {

		this.wdg = wdg; // save the reference to the WeatherDataGenerator object

		// create the nested event handler that will respond to incoming UI events
		this.eventListener = new WeatherStationUIListener();

		createUserInterface();
	} // end of Constructor method

	/*
	 * Updates the label field in the UI that displays the result
	 * of getting the latest weather data.
	 */
	private void updateDisplay() {
		// retrieve data from the source and update the UI
		double temp = wdg.getTemp();
		double speed = wdg.getWindSpeed();

		DecimalFormat df = new DecimalFormat("0.0");
		txtTemperature.setText( df.format(temp) + " F" );
		txtWindSpeed.setText( df.format(speed) + " MPH");
	}

	// build the UI
	private void createUserInterface() {
		// create the containing window and set its size etc
		setTitle("Weather center");
		setSize(300, 200);
		setLocation(400, 400);
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Retrieves the window's contentPane and sets the background color to white
		Container contentPane = getContentPane();
		contentPane.setLayout(null);
		contentPane.setBackground(Color.BLACK);

		//Creation of "Calculate" button
		createButton("Update", "update", 10, 130, 170, 30, eventListener, contentPane);

		//Creation of "Quit" button
		createButton("Quit", "close", 190, 130, 95, 30, eventListener, contentPane);

		Font font = new Font("Arial", Font.BOLD, 24 );

		//Creation of "Current temp" label
		JLabel lblTemperature = new JLabel ("Current temp:");
		lblTemperature.setBounds(10, 10, 170, 30);
		lblTemperature.setForeground(Color.WHITE);
		contentPane.add(lblTemperature);
		//Creation of text field
		txtTemperature = createTextField("temp", 150, 10, 135, 30, eventListener, contentPane );
		txtTemperature.setFont(font);
		txtTemperature.setForeground(Color.WHITE);
		txtTemperature.setBackground(Color.BLACK);

		//Creation of "End Date" label
		JLabel lblWindSpeed = new JLabel ("Current wind speed:");
		lblWindSpeed.setBounds(10, 50, 170, 30);
		lblWindSpeed.setForeground(Color.WHITE);
		contentPane.add(lblWindSpeed);
		//Creation of text field
		txtWindSpeed = createTextField("temp", 150, 50, 135, 30, eventListener, contentPane );
		txtWindSpeed.setFont(font);
		txtWindSpeed.setForeground(Color.WHITE);
		txtWindSpeed.setBackground(Color.BLACK);

		contentPane.validate();
		contentPane.repaint();
	}


	/**
	 * Creates a JButton and adds it to the UI.
	 * @param name Name to appear on the button
	 * @param actionCmd Action command associated with the button
	 * @param x Horizontal coordinate of the upper left corner of the button
	 * @param y Vertical coordinate of the upper left corner of the button
	 * @param width Width of the button
	 * @param height Height of the button
	 * @param listener ActionListener to be added as a listener
	 * @param pane Container where the button will be added
	 */
	private void createButton(String name, String actionCmd, int x, int y, int width, int height, ActionListener listener, Container pane) {
		JButton btn;
		btn = new JButton (name); 
		btn.setActionCommand(actionCmd);
		btn.setBounds(x, y, width, height);
		btn.addActionListener(listener);	// subscribe to events
		pane.add(btn);		
	}

	/**
	 * Adds a JTextField and adds it to the UI.
	 * @param txtField Reference for the JTextField
	 * @param actionCmd Action command associated with the button
	 * @param x Horizontal coordinate of the upper left corner of the button
	 * @param y Vertical coordinate of the upper left corner of the button
	 * @param width Width of the button
	 * @param height Height of the button
	 * @param listener ActionListener to be added as a listener
	 * @param pane Container where the button will be added
	 */
	private JTextField createTextField(String actionCmd, int x, int y, int width, int height, ActionListener listener, Container pane) {
		JTextField txtField = new JTextField();
		txtField.setBounds(x, y, width, height);
		txtField.setActionCommand(actionCmd);
		txtField.addActionListener(listener);
		txtField.setEditable(false);
		pane.add(txtField);
		return txtField;
	}


	/**
	 * This is a nested inner class that handles events
	 * for the WeatherStationUI  
	 * @author hornick
	 */
	private class WeatherStationUIListener implements ActionListener {

		/**
		 * Constructor; does nothing useful
		 */ 
		private WeatherStationUIListener() {
		}

		/**
		 * ActionListener event handler for this class
		 * @param event - the ActionEvent object that caused the event
		 */
		public void actionPerformed(ActionEvent event) {

			String cmd = event.getActionCommand(); 	// get the Action Command that caused this event

			if (cmd.equals("close")) 	// the "Quit" button was pressed
			{
				System.exit(0); 		// Closes the entire program
			}

			// checks whether the "update" button is pressed
			if ( cmd.equals("update") )
			{
				updateDisplay();	// delegate the actual handling of the event to another method
			}
		}

	}	// end of WeatherStationUIListener inner class

} // end of the WeatherStationUI class