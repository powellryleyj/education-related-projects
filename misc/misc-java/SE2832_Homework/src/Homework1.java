import java.util.List;

/**
 * Created by Ryley on 4/6/2017.
 */
public class Homework1 {

    public Homework1(){

    }

    public static int generateParityBit(List<Integer> array, int type){
        int index;
        int sum = 0;
        int retValue = 0;

        for (index = 1; index < array.size(); index++){
            sum = (sum + array.get(index)) % 2;
        }

        if (sum == type) {
            retValue = 0;
        } else {
            retValue = 1;
        }

        return retValue;
    }

    public static int lastZero(int[] x) throws NullPointerException{
        if (x == null){
            throw new NullPointerException("The array should not be null");
        }

        for(int i = 0; i < x.length; i++){
            if(x[i] == 0){
                return i;
            }
        }
        return -1;
    }
}
