<%--
  User: Ryley
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>CmdServlet 2.0</title>
  </head>
  <body>
  <p>Choose a Command to execute: </p>
  <form action="/CmdServlet">
    <select name ="cmd">
      <option value="time">Time</option>
      <option name="weather" value="weather">Weather</option>
      <option value="clientdata">Client Data</option>
    </select>
    <input type="submit" value="Submit">
  </form>

  </body>
</html>
