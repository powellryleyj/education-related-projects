<%@ page import="command.Time" %><%--
  User: Ryley
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% Time time = (Time) request.getAttribute("time"); %>
<html>
<head>
    <title>CmdServlet 2.0</title>
</head>
<body>
    <h1> Date: </h1>
    <p><% out.println(time.getDate());%> </p>
    <h1> Time: </h1>
    <p>
        <% out.println(time.getTime());
        out.println(time.getMeridiem());%>
    </p>

</body>
</html>
