<%@ page import="command.ClientData" %><%--
  User: Ryley
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% ClientData cd = (ClientData) request.getAttribute("clientData");%>
<html>
<head>
    <title>CmdServlet 2.0</title>
</head>
<body>
    <h1>Your Client Data: </h1>
    <p>
        <% out.println(cd.getAccept()); %>
        <br>
        <% out.println(cd.getUser_agent()); %>
        <br>
        <% out.println(cd.getAccept_charset()); %>
        <br>
        <% out.println(cd.getAccept_language()); %>
        <br>
        <% out.println(cd.getX_wap_profile()); %>
        <br>
        <% out.println(cd.getProfile()); %>
        <br>
    </p>

</body>
</html>
