<%@ page import="command.Weather" %><%--
  User: Ryley
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page session="false" %>
<% Weather weather = (Weather) request.getAttribute("weather");
    Weather.WeatherData weatherData = weather.getWeatherData();
%>

<html>
<head>
    <title>CmdServlet 2.0</title>
</head>
<body>
<div>
    <h2>Search by City Name (currently only support one word names)</h2>
    <form onsubmit="">
        <input title="Location" name="loc" value=<%= weather.getLocation()%> type="text">
        <input type="hidden" name="cmd" value="weather">
        <button type="submit">Search</button>
    </form>
</div>
<div>
    <h1>Weather in
    <% out.println( " " + weatherData.getName() + ", " + weatherData.getSys().getCountry());%>
    </h1>
    <h3>Temperature Conditions</h3>
    <p>
        Current:
        <% out.println(" " + Weather.kelvinToFahrenheit(weatherData.getMain().getTemp())
                + "\u00b0F");%>
        <br/>
        Low:
        <% out.println(" " + Weather.kelvinToFahrenheit(weatherData.getMain().getTemp_min())
                + "\u00b0F");%>
        <br/>
        High:
        <% out.println(" " + Weather.kelvinToFahrenheit(weatherData.getMain().getTemp_max())
                + "\u00b0F");%>
        <br/>
        Humidity:
        <% out.println(" " + weatherData.getMain().getHumidity() + "%");%>
    </p>
    <h3>MISC Data</h3>
    <p>
        Wind Speed:
        <% out.println(" " + weatherData.getWind().getSpeed() + " m/s");%>
        <br/>
        Wind Direction:
        <% out.println(" " + Weather.degreesToCardinal(weatherData.getWind().getDeg()));%>
        <br/>
        Pressure:
        <% out.println(" " + weatherData.getMain().getPressure() + " hPa");%>
        <br/>
    </p>
</div>

</body>
</html>
