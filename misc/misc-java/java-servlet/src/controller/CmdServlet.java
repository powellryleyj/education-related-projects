package controller;

import command.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;


@WebServlet(name = "CmdServlet", displayName = "CmdServlet", urlPatterns = {"/CmdServlet"}, loadOnStartup = 1)
public class CmdServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private HashMap<String, Command> commandHashMap;
    private final String jspdir = "/";

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        initCommands();
    }

    private void initCommands(){
        commandHashMap = new HashMap<>();
        commandHashMap.putIfAbsent("time", new Time());
        commandHashMap.putIfAbsent("weather", new Weather());
        commandHashMap.putIfAbsent("clientdata", new ClientData());
    }

    private Command lookupCommand(String commandName) {
        return commandHashMap.getOrDefault(commandName.toLowerCase().trim(), null);
    }

    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String next = "index";
        try {
            Command cmd = lookupCommand(request.getParameter("cmd"));
            next = cmd.execute(request, response);
            System.out.println("CmdServlet: cmd = " + cmd + ", next = " + next );
        } catch (CommandException e) {
            e.printStackTrace();
        }
        RequestDispatcher rd;
        rd = getServletContext().getRequestDispatcher(jspdir + next + ".jsp");
        rd.forward(request, response);

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /*String cmd = request.getParameter("cmd");

        String accept = request.getHeader("accept");
        String user_agent = request.getHeader("user-agent");
        String accept_charset = request.getHeader("accept-charset");
        String accept_language = request.getHeader("accept-language");
        String x_wap_profile = request.getHeader("x-wap-profile");
        String profile = request.getHeader("profile");
        response.setContentType("text/html");

        java.io.PrintWriter output = response.getWriter();
        output.println("<html>");
        output.println("<head><title>User Agent</title></title>");
        output.println("<body>");
        output.write("accept " + accept + "<br>");
        output.write("user_agent " + user_agent + "<br>");
        output.write("accept_charset " + accept_charset + "<br>");
        output.write("accept_language " + accept_language + "<br>");
        output.write("x_wap_profile " + x_wap_profile + "<br>");
        output.write("profile " + profile + "<br>");
        output.println("</body></html>");
        output.close();*/
    }
}
