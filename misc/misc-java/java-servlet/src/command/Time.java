package command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;

public class Time implements Command {

    private final String CMD_NAME = "time";
    private LocalDateTime localDateTime;
    private String date;
    private String time;
    private String meridiem;

    /**
     * writes a formatted response to the provided HttpServletResponse to be displayed upon a GET request
     * @param request - the request made by the user
     * @param response - the response to be sent back
     * @return - the name of this Time command
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        refreshTime();
        meridiem = "a.m.";
        date = correctMonthLetterCase(localDateTime.getMonth().name())
                + " "
                + localDateTime.getDayOfMonth()
                + ", "
                + localDateTime.getYear();

        int hour = localDateTime.getHour();
        if(hour > 12) {
            hour -= 12;
            meridiem = "p.m.";
        }

        time = hour + ":" + localDateTime.getMinute();
        request.setAttribute("time", this);
        /*
        try {
            java.io.PrintWriter output = response.getWriter();
            output.println("<html>");
            output.println("<head><title>Current Time</title></title>");
            output.println("<body>");
            output.println("<h1>Date: </h1><br><h2> " + correctMonthLetterCase(localDateTime.getMonth().name())
                    + " " + localDateTime.getDayOfMonth()
                    + ", " + localDateTime.getYear() + "</h2><br>");
            int hour = localDateTime.getHour();
            if(hour > 12) {
                hour -= 12;
                meridiem = "p.m.";
            }
            output.println("<h1>Time:</h1> <br><h2>" + hour + ":" + localDateTime.getMinute()
                    + " " + meridiem + "</h2><br>");
            output.println("</body></html>");
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        */
        return CMD_NAME;
    }

    public String getMeridiem() {
        return meridiem;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    /**
     * refreshes the inner LocalDateTime object such that the time is most current time is reflected when this Time is called
     * by the CmdServlet
     */
    private void refreshTime() {
        localDateTime = LocalDateTime.now();
    }

    /**
     * simple worker the corrects the letter case of the String returned by LocalDateTime.getMonth().name()
     * @param month - the String name of the month to be corrected
     * @return - the corrected String
     */
    public static String correctMonthLetterCase(String month) {
        String firstLetter = month.substring(0 , 1);
        String restOfWord = month.substring(1, month.length());
        return  firstLetter + restOfWord.toLowerCase();
    }

    /**
     * used for printing out this Time object
     * @return - a String in the form of "time = MONTH, DAY YEAR T HH:MM"
     */
    @Override
    public String toString() {
        return CMD_NAME + " = " + date + " T " + time;
    }
}
