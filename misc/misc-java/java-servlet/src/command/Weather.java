package command;

import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class Weather implements Command {

    private String location;
    private WeatherData weatherData;

    private final String CMD_NAME = "weather";
    private final String DEFAULT_LOC = "Milwaukee";
    private final String API_KEY = "&APPID=e1229280aad1c7aae1c1a6cd9ba95400";
    private final String API_BASE_URL = "http://api.openweathermap.org/data/2.5/weather?q=";


    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String urlString = getApiUrl(request);
        try {
            String rawWeatherData = getRawWeatherData(urlString, request);
            Gson gson = new Gson();
            weatherData = gson.fromJson(rawWeatherData, WeatherData.class);
            request.setAttribute("weather", this);
        } catch (CommandException c) {
            try {
                request.getRequestDispatcher("/CityNotFound.jsp").forward(request, response);
            } catch (IOException | ServletException i) {
                //SHOULDN'T GET HERE
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return CMD_NAME;
    }

    /**
     * worker function that translates Kelvin to Fahrenheit, both of which as a String
     * @param kelvin - the value in Kelvin
     * @return - the value in Fahrenheit
     */
    public static String kelvinToFahrenheit(String kelvin) {
        final double CONSTANT = 459.67;
        double k = Double.parseDouble(kelvin);
        double f = ((k * 9) / 5) - CONSTANT;
        long rounded = Math.round(f);
        return Long.toString(rounded);
    }

    /**
     * worker function that converts Degrees to a Cardinal Direction
     * @param degrees - degrees between the values of [0, 360)
     * @return - the Cardinal Direction
     */
    public static String degreesToCardinal(String degrees) {
        double deg = Double.parseDouble(degrees);
        String ret;
        if(deg == 0) {
            ret = "East";
        } else if(deg == 90) {
            ret = "North";
        } else if (deg == 180) {
            ret = "West";
        } else if (deg == 270) {
            ret = "South";
        } else if (deg > 0 && deg < 90) {
            ret = "North-East";
        } else if (deg > 90 && deg < 180) {
            ret = "North-West";
        } else if (deg > 180 && deg < 270) {
            ret = "South-West";
        } else if (deg > 270 && deg < 360) {
            ret = "South-East";
        } else {
            ret = "This method doesn't support degree less than 0 or greater than or equal to 360";
        }
        return ret;
    }

    public WeatherData getWeatherData() {
        return weatherData;
    }

    private String getRawWeatherData(String url, HttpServletRequest request) throws IOException, CommandException {

        String weatherData;

        URL apiURL = new URL(url);
        HttpURLConnection apiConnection = (HttpURLConnection) apiURL.openConnection();
        apiConnection.setRequestMethod("GET");
        apiConnection.setRequestProperty("User-Agent", request.getHeader("user-agent"));

        int responseCode = apiConnection.getResponseCode();

        if (responseCode == HttpURLConnection.HTTP_OK) {
            BufferedReader responseMessageReader = new BufferedReader(new InputStreamReader(apiConnection.getInputStream()));
            StringBuilder stringBuffer = new StringBuilder();
            String tempLine;
            while ((tempLine = responseMessageReader.readLine()) != null) {
                stringBuffer.append(tempLine);
            }
            responseMessageReader.close();
            weatherData = stringBuffer.toString();
        } else {
            throw new CommandException("City could not be found.");
        }

        return weatherData;
    }

    private String getApiUrl(HttpServletRequest request) {
        String url = null;
        try {
            location = request.getParameter("loc");
            if(location.equals("")) {
                location = DEFAULT_LOC;
            } else {
                normalizeLocation();
            }
            url = API_BASE_URL + location + API_KEY;
        } catch (NullPointerException e) {
            location = DEFAULT_LOC;
            url = API_BASE_URL + DEFAULT_LOC + API_KEY;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return url;
    }

    private void normalizeLocation() {
        String firstLetter = location.substring(0, 1);
        String restOfWord = location.substring(1, location.length());
        location = firstLetter.toUpperCase() + restOfWord.toLowerCase();
    }

    @Override
    public String toString() {
        return CMD_NAME + " location -> " + location;
    }

    /**
     * used to dynamically fill the input field in weather.jsp
     * @return
     */
    public String getLocation() {
        return location;
    }

    /**
     * inner class that helps with parsing the JSON response from OpenWeatherMap.org
     */
    public class WeatherData {

        public String getName() {
            return name;
        }

        public WeatherMain getMain() {
            return main;
        }

        public WeatherWind getWind() {
            return wind;
        }

        public WeatherSys getSys() {
            return sys;
        }

        String name;
        //WeatherDescription weather; // not supported because of parsing error
        WeatherMain main;
        WeatherWind wind;
        WeatherSys sys;

        public String toString() {
            return "Location: " + name + ", " + sys.country
                    //+ "\r\nDescription: " + weather.description
                    + "\r\n<br>Temp: " + main.temp
                    + "\r\n<br>Temp Low: " + main.temp_min
                    + "\r\n<br>Temp High: " + main.temp_max
                    + "\r\n<br>Humidity: " + main.humidity
                    + "\r\n<br>Pressure: " + main.pressure
                    + "\r\n<br>Wind Speed: " + wind.speed
                    + "\r\n<br>Wind Direction: " + wind.deg;
        }

        /**
         * currently unsupported due to parsing
         */
        public class WeatherDescription {
            public String getId() {
                return id;
            }

            public String getMain() {
                return main;
            }

            public String getDescription() {
                return description;
            }

            String id;
            String main;
            String description;
        }

        /**
         * used for parsing
         */
        public class WeatherMain {
            String temp;
            String pressure;
            String humidity;
            String temp_min;
            String temp_max;

            public String getTemp() {
                return temp;
            }

            public String getPressure() {
                return pressure;
            }

            public String getHumidity() {
                return humidity;
            }

            public String getTemp_min() {
                return temp_min;
            }

            public String getTemp_max() {
                return temp_max;
            }
        }

        /**
         * used for parsing
         */
        public class WeatherWind {
            String speed;
            String deg;

            public String getSpeed() {
                return speed;
            }

            public String getDeg() {
                return deg;
            }
        }

        /**
         * used for parsing
         */
        public class WeatherSys {
            String country;
            String sunrise;
            String sunset;

            public String getCountry() {
                return country;
            }

            public String getSunrise() {
                return sunrise;
            }

            public String getSunset() {
                return sunset;
            }
        }

    }


}
