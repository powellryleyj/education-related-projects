package command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ClientData implements Command {


    private final String CMD_NAME = "clientData";

    private String accept;
    private String user_agent;
    private String accept_charset;
    private String accept_language;
    private String x_wap_profile;
    private String profile;

    public String getAccept() {
        return accept;
    }

    public String getUser_agent() {
        return user_agent;
    }

    public String getAccept_charset() {
        return accept_charset;
    }

    public String getAccept_language() {
        return accept_language;
    }

    public String getX_wap_profile() {
        return x_wap_profile;
    }

    public String getProfile() {
        return profile;
    }

    public String getCMD_NAME() {

        return CMD_NAME;
    }

    /**
     * dumps the user's client data to a POJO
     * @param request - the incoming HTTP request
     * @param response - the HTTP response
     * @return - the name of this command object
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        accept = request.getHeader("accept");
        user_agent = request.getHeader("user-agent");
        accept_charset = request.getHeader("accept-charset");
        accept_language = request.getHeader("accept-language");
        x_wap_profile = request.getHeader("x-wap-profile");
        profile = request.getHeader("profile");

        request.setAttribute("clientData", this);

        return CMD_NAME;
    }

    /**
     *
     * @return - pre-formatted dump string
     */
    @Override
    public String toString() {
        return "accept: " + accept
                + "\r\nuser_agent:" + user_agent
                + "\r\naccept_charset: " + accept_charset
                + "\r\naccept_language: " + accept_language
                + "\r\nx_wap_profile: " + x_wap_profile
                + "\r\nprofile: " + profile;
    }
}
