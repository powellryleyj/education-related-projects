/******************************************************************************************************************
 * CoinFlipper.java
 * Adapted from Dean & Dean, ch 9.
 * 
 * This program generates a histogram of coin flips. Suppose you have 5 coins and flip them 100000 times. 
 * Some flips will result in all heads, some with all tails, and others with some combination of heads and tails.
 * The histogram displays the number of times each combination occurred over all flips.
 ******************************************************************************************************************/
public class CoinFlipper {

	/**
	 * Kicks off the execution of the program, and also measures how long it takes to execute.
	 * @param args not used
	 */
	public static void main(String[] args) {

		final int NUM_OF_COINS = 5;		// number of coins to flip
		final int NUM_OF_REPS = 100000;	// repetitions, or the number of times the coins are flipped
		
		// The frequency array holds the number of times a particular number of heads occurred:
		// frequency[0] holds the number of times 0 heads occurred 
		// frequency[1] holds the number of times 1 head occurred
		// ...
		// frequency[NUM_OF_COINS] holds the number of times all heads occurred
		int[] frequency = new int[NUM_OF_COINS+1];

		long executionTime = System.currentTimeMillis(); // current system time snapshot

		flipCoins(frequency, NUM_OF_COINS, NUM_OF_REPS); // flip NUM_OF_COINS coins NUM_OF_REPS times
		printHistogram(frequency, NUM_OF_COINS, NUM_OF_REPS); // display the resulting histogram
		
		executionTime = System.currentTimeMillis()-executionTime; // compute elapsed time 
		System.out.println("Coin Flipper Time: " + executionTime + "ms");
	}

	/**
	 * This method flips a specified number of coins a specified number of times,
	 * and gathers the number of times a certain number of heads occurred in each flip into an array.
	 * @param frequency the array of "bins", each bin containing a specific number of heads that occurred for all flips
	 * @param numCoins the number of coins to flip each time
	 * @param numReps the number of times to flip the coins
	 */
	public static void flipCoins(int[] frequency, int numCoins, int numReps) {
		// This loop fills up the frequency bins. Each iteration simulates one group of numCoins coin flips. 
		// For example, with a group of flips of 3 coins, heads may come up 0, 1, 2, or 3 times.
		for( int rep=0; rep<numReps; rep++ ) {
			// perform a single flip of NUM_OF_COINS coins
			int heads = doSingleFlip(numCoins);
			frequency[heads]++; // update appropriate bin
		}
	}
	
	/**
	 * This method flips a specified number of coins and returns the number heads that occurred in the flip.
	 * It makes use of a random number generator to randomly generate heads or tails for each coin flipped.
	 * @param numCoins the number of coins to flip
	 * @return the number of heads that occurred in the flip
	 */
	public static int doSingleFlip(int numCoins) {
		int heads = 0;
		for( int i=0; i<numCoins; i++ ) { // flip each coin
			heads += (int)(Math.random() * 2); // computed random int value is either 0 or 1 (tails or heads)
		}
		return heads; // number of heads that came up
	}
	
	/**
	 * This method prints a histogram of the number of heads that occurred for a specified number of flips
	 * @param frequency the array of "bins", each bin containing a specific number of heads that occurred for all flips
	 * @param numCoins the number of coins that were flipped each time
	 * @param numReps the number of times that coins were flipped
	 * Notes: The output generated for numCoins=5 and numReps=100000 may look something like this:
	 *
	 * Number of times each head count occurred:
 	 * 0  3076  ***
	 * 1  15792  ****************
	 * 2  31348  *******************************
	 * 3  31197  *******************************
	 * 4  15552  ****************
	 * 5  3035  ***
	 */
	public static void printHistogram(int[] frequency, int numCoins, int numReps) {
		System.out.println("Number of times each head count occurred:");

		// This loop prints the histogram. Each iteration prints one histogram bar.
		for( int heads=0;heads<=numCoins; heads++ ) {
			System.out.print( " " + heads + "  " + frequency[heads] + "  " );
			double fractionOfReps = (float) frequency[heads] / numReps;
			int numOfAsterisks = (int) Math.round(fractionOfReps * 100);

			for( int i=0; i<numOfAsterisks; i++ ) {
				System.out.print("*");
			}
			System.out.println();
		}
	}
}