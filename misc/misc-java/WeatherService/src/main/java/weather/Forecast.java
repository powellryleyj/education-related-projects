package main.java.weather;


import com.sun.jersey.api.view.Viewable;
import org.json.JSONObject;
import org.json.JSONTokener;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

@Path("/forecast")
public class Forecast {

    private final String DEFAULT_LOC = "Milwaukee";
    private final String API_KEY = "&APPID=e1229280aad1c7aae1c1a6cd9ba95400";
    private final String API_BASE_URL = "http://api.openweathermap.org/data/2.5/weather?q=";

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Viewable getDefaultForecast() {
        JSONObject weatherData = getWeatherData(DEFAULT_LOC);
        System.out.println(weatherData);

        return new Viewable("/forecast", weatherData);
    }

    private JSONObject getWeatherData(String location) {
        String weatherData;
        JSONObject ret = null;
        String url = API_BASE_URL + location + API_KEY;
        try {
            URL apiURL = new URL(url);
            HttpURLConnection apiConnection = (HttpURLConnection) apiURL.openConnection();
            apiConnection.setRequestMethod("GET");
            int responseCode = apiConnection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader responseMessageReader = new BufferedReader(new InputStreamReader(apiConnection.getInputStream()));
                StringBuilder stringBuffer = new StringBuilder();
                String tempLine;
                while ((tempLine = responseMessageReader.readLine()) != null) {
                    stringBuffer.append(tempLine);
                }
                responseMessageReader.close();
                weatherData = stringBuffer.toString();
                JSONTokener tokener = new JSONTokener(weatherData);
                ret = new JSONObject(tokener);
            }
        } catch (IOException i) {
            i.printStackTrace();
        }
        return ret;
    }
}
