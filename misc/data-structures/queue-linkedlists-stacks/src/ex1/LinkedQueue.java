package ex1;

/**
 * Ryley Powell (powellr)
 * SE2811 - 031
 * In Class Activity #1
 * Created on 11/28/17
 */

public class LinkedQueue implements GenericQueue{

    private SimpleLinkedList<String> list;

    public LinkedQueue() {
        list = new SimpleLinkedList<>();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public void add(String nextItem) {
        list.addLast(nextItem);
    }

    @Override
    public String get() {
        return list.first().toString();
    }

    public static void main(String[] args) {
        GenericQueue todo = new LinkedQueue();
        todo.add("this");
        if ( todo.isEmpty() )
            System.out.println("Expected non-empty list.");
        todo.add("that");
        todo.add(".");
        if ( ! todo.get().equals("this") )
            System.out.println("Expected `this'");
        if ( ! todo.get().equals("that") )
            System.out.println("Expected `that'");
        if ( ! todo.get().equals(".") )
            System.out.println("Expected `.'");
        if ( ! todo.isEmpty() )
            System.out.println("Expected empty list.");
        System.out.println("All tests passed.");
    }
}
