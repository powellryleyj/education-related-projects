package ex1;

public interface GenericQueue {
    public boolean isEmpty();

    public void add(String nextItem);

    public String get();
}