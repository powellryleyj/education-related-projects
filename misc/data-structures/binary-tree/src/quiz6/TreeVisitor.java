package quiz6;

public interface TreeVisitor {
    public void visit(Node node);
    public long result();
}

