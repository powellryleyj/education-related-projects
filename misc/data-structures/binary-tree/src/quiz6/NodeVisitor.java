package quiz6;

public class NodeVisitor implements TreeVisitor {

    private long result;

    @Override
    public long result() {
        return result;
    }

    @Override
    public void visit(Node node) {
        if(node != null) {
            if(node.left != null) {
                visit(node.left);
            }
            if(node.right != null) {
                visit(node.right);

            }

            result += 1;
        }
    }
}
