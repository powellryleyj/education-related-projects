package quiz6;

/**
 * A node in a binary search tree; in practice this would be a private inner class of
 * BinarySearchTree, but making it a regular class simplifies the visitor code.
 */
public class Node {
    protected long data;
    protected Node left, right;

    /**
     * Create a binary search tree node with given data and left, right subtrees
     * @param data value to store in node
     * @param l left subtree, can be null
     * @param r right subtree, can be null
     */
    public Node(long data, Node l, Node r) {
        this.data = data;
        this.left = l;
        this.right = r;
    }

    /**
     * Create a leaf node containing the given data
     * @param data value to store in leaf
     */
    public Node(long data) {
        this(data, null, null);
    }

    /**
     * apply the visitor to the current node and all children
     * @param visitor
     */
    public void accept(TreeVisitor visitor) {
        if (left != null)
            left.accept(visitor);
        visitor.visit(this);
        if (right != null)
            right.accept(visitor);
    }
}

