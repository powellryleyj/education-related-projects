package quiz6;

import java.util.Scanner;

public class Main {
    private static final int[] SIMPLE_NUMS = {10, 5, 8, 1, 12};
    private static final int[] TEST_NUMS = {
            14, 5, 25, 8, 3, 10, 12, 11, 21, 99, 53, 183, 7, 98, 71, 13, 9,
            13, // intentional repeat
            15, 29};
    private static final int[] UNBALANCED = {5, 6, 7, 9, 8, 10};

    /**
     * Construct a binary search tree given an array of integers
     * @param nums Values to store in the tree nodes
     * @return a valid binary search tree
     */
    private static BinarySearchTree makeTree(int nums[]) {
        BinarySearchTree result = new BinarySearchTree();
        for (int n : nums)
            result.add(n);
        return result;
    }

    /**
     * Compute and print a statistic (as computed by a visitor)
     * @param name Statistic description
     * @param tree Tree to process
     * @param visitor Visitor object to apply
     */
    private static void printStatistic(String name, BinarySearchTree tree,
                                       TreeVisitor visitor) {
        tree.accept(visitor);
        System.out.println(name + ": " + visitor.result());
    }

    /**
     * Print three statistics for a given tree.
     * @param tree tree to process, must not be null
     */
    private static void printStatisticsFor(BinarySearchTree tree) {
        printStatistic("Node count", tree, new NodeVisitor());
        //printStatistic("Even value sum", tree, null);
        //printStatistic("Nodes with two children", tree, null);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String test = in.next();
        BinarySearchTree simple = makeTree(SIMPLE_NUMS);
        BinarySearchTree complex = makeTree(TEST_NUMS);
        BinarySearchTree unbalanced = makeTree(UNBALANCED);
        switch (test) {
            case "a":
                System.out.println("Statistics for simple tree:");
                printStatisticsFor(simple);
                break;
            case "b":
                System.out.println("Statistics for complex tree:");
                printStatisticsFor(complex);
                break;
            case "c":
                System.out.println("Statistics for unbalanced tree:");
                printStatisticsFor(unbalanced);
                break;
            default:
                System.out.println("Statistics for all trees:");
                printStatisticsFor(simple);
                printStatisticsFor(complex);
                printStatisticsFor(unbalanced);
        }
        System.out.println("End of test.");
    }
}
