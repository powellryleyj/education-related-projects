package quiz6;

//
// BinarySearchTree.java: binary search tree containing longs and test-bed main
//
// Compiling from the command line:
//      javac -target 1.8 BinarySearchTree.java
//
// Running from the command line:
//      java -ea BinarySearchTree
//
// Author: Robert W. Hasker
//

public class BinarySearchTree {
    protected Node root = null;

    public BinarySearchTree() {
    }

    // erase all nodes from tree
    public void clear() {
        root = null;
    }

    // return true if tree is empty
    public boolean isEmpty() {
        return root == null;
    }

    // return true if TARGET in tree
    public boolean contains(long target) {
        return contains(root, target);
    }

    // auxiliary function: return true if TARGET in subtree CURRENT
    protected boolean contains(Node current, long target) {
        if (current == null)
            return false;
        else {
            if (target == current.data)
                return true;
            else if (target < current.data)
                return contains(current.left, target);
            else
                return contains(current.right, target);
        }
    }

    // insert X into tree; does nothing if value already in tree
    public void add(long x) {
        if (root == null)
            root = new Node(x);
        else
            add(x, root);
    }

    // auxiliary add: insert X into tree rooted at NODE
    // precondition: node is not null
    private void add(long x, Node node) {
        if (x == node.data)
            return;                   // item already in tree
        else if (x < node.data) {
            if (node.left == null)
                node.left = new Node(x);
            else
                add(x, node.left);
        } else // x > node.data
        {
            if (node.right == null)
                node.right = new Node(x);
            else
                add(x, node.right);
        }
    }

    public void accept(TreeVisitor visitor) {
        if (root != null)
            root.accept(visitor);
    }
}
